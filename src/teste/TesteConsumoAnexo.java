package teste;

import com.aseg.email.service.EmailService;
import com.aseg.seguranca.Usuario;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;

import java.io.InputStream;
import java.util.*;

public class TesteConsumoAnexo {

	/**
	 * Metodo criado para testar o consumo de um anexo de E-mail especifico, 
	 * nesse caso anexo da crefisa chamado ultimatum
	 * 
	 * @author mmiola
	 * @author alecindro
	 * 
	 */
	public  void main(String[] args) {
		//criar email service
		//ler email
		//carregar xml
		//createXmlParser
		//getListChildsFromXQL
		
		try{
			EmailService es = new EmailService();
			UtilServiceImpl usi = new UtilServiceImpl();
			GenericVO vo = new GenericVO(usi);
			Usuario usuario = new Usuario();
			vo.put("serverType", "imap");
			vo.put("server", "imap.gmail.com");
			vo.put("username", "ultimatum@matiasrezende.com.br");
			vo.put("password", "ultimatum");
			vo.put("SEGURO", "true");
			vo.put("emptyTrash", "true");
			vo.put("deleteread", "false");
			List list = es.getMessages(vo,usuario );
			for(Object ob : list){
				Map mmOb = (Map)ob;
				InputStream in = (InputStream) mmOb.get("ATT-1");
				ParseXmlService pxs =  usi.createXMLParser(in);
				Collection<Map<String,String>> maps = pxs.getListChildsFromXQL("/Ultimatum/Publicacoes", null, new Usuario());
				Iterator<Map<String,String>> imap = maps.iterator();
				while(imap.hasNext()){
					Map<String,String> map = imap.next();
					Set<String> sets = map.keySet();
					for(String s : sets){
						System.out.print(s);
						System.out.print("--");
						System.out.print(map.get(s));
						System.out.println("===");
					}
					System.out.println("==Prxima iterao=");
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	
	
		

	}
	
	

}
