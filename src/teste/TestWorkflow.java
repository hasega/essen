package teste;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.util.workflow.AdvancedWorkflow;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.InvalidRoleException;
import com.opensymphony.workflow.WorkflowException;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class TestWorkflow extends TestCase {

	public TestWorkflow() {
		super();
		this.setName("Sistema de Workflows");
	}

	@Override
	@Before
	public void setUp() {

	}

	@Test
	public void testaCarregamentoDeUmWorkflow(Usuario user) {
		AdvancedWorkflow wf = new AdvancedWorkflow(new Long(22), user);
		assertNotNull(wf);
	}

	@Test
	public void testaCriacaoDeUmaInstanciaDeWorkflow(Usuario user) {

		Long instanceId = null;
		AdvancedWorkflow wf = new AdvancedWorkflow(new Long(22), user);

		try {
			instanceId = wf.initialize(100, null);
		} catch (InvalidRoleException e) {
			LogUtil.exception(e, user);
		} catch (InvalidInputException e) {
			LogUtil.exception(e, user);
		} catch (WorkflowException e) {
			LogUtil.exception(e, user);
		}

		assertNotNull(instanceId);

	}
	
	

}
