package com.aseg.config;

import java.util.Arrays;
import java.util.List;

/**
 * @author asega
 * 
 */

public interface Constants {
	String IN_SEQUENCE_VALUES = "_#:SV:";
	String true_str = "true";
	String false_str = "false";
	String bi_filter = "F#{FILTRO}";
	String ACTION_BASE = "Action";
	String ACTION_SUFIX = ".do";
	String ACTION_TITLE = "***ACTION_TITLE***";
	String ActionID = ACTION_BASE + "Id";
	String ActionList = "Lista" + ACTION_BASE;
	String ActionName = ACTION_BASE + Constants.name;
	String ActionParam = "METHOD";
	String ALL_FIELDS = "ALL_FIELDS";
	String ALL_METHODS = "ALL_METHODS";
	String ASP_SUFIX = ".asp";
	String BUTTONS = "BUTTONS";
	String CACHE = "CACHE_KEY";
	String CACHE_SEPARATOR = "*";
	String CALLS = "CALLS_INJECT";
	String CHILD_CONTEXTS = "CHILD_CONTEXTS";
	String CONTEXT_KEY = "CONTEXT_KEY";
	String CONTEXT_MASTER_KEY = "CONTEXT_MASTER_KEY";
	String CONTROLER = "CONTROLLER_CLASS_BASE";
	String custom = "custom";
	String customAttribute = "Custom";
	String DEFAULT_CRUD = "DEFAULT_CRUD";
	String DEFAULT_SEARCH = "DEFAULT_SEARCH";
	String FILTER_KEY = "FILTRO";
	String QUERY_KEY = "query";
	String IGNORE_PERMISSION = String.valueOf(String.valueOf("1gn0R3_p3RMlssI0n".hashCode()).hashCode());

	String DEFAULT_SORT = "DEFAULT_SORT";
	// String RAIN_VO = "RAIN_VO";
	// String encode = "ISO-8859-1";
	// String encode = "UTF-8";
	// tamanho padrao da listagem dos grids
	String gridSize = "20";
	String KeyCollection = "MULTIPLEID";
	String NEXT_KEY = "IS_NEXT";
	String LIST_DATA = "LIST_DATA";
	String LIST_FIELDS = "LIST_FIELDS";
	String MAIN_ACTION = "MAIN_ACTION";
	String MAIN_FIELD = "MAIN_FIELD";
	String MASTERVOKey = "masterVO";
	String NO_FILTER = "NO_FILTER";
	String NO_HELP = "NO_HELP";
	String NOME_CHAVE = "NOME_CHAVE";
	String PAGE_CONTEXT = "***PAGE_CONTEXT***";
	// chave do usuario logado
	String RAIN_USER = "user";
	String RELATION_KEY = "RELATION_KEY";
	String REQUEST = "request";

	String REQUEST_KEYS = "req_keys";
	String REQUEST_EVALUATION_CONTEXT = "REQUEST_EVALUATION_CONTEXT";

	String RequestProcessed = "RequestProcessed";
	// String PROCESS_REQUEST = "PROCESS_REQUEST_?";
	String RESPONSE = "RESPONSE_KEY";
	String SCRIPTS = "SCRIPTS";
	String SEARCH_BY = "SEARCH_BY";
	String SERVER_URL = "ServerUrl";
	String SERVICE_BASE = "Service";
	String uncheckedSufix = "UncheckedValue";
	String UploadList = "CommonsUploadList";
	boolean use_icons = true;
	String VOKey = "VO";
	String VOName = "FormName";
	String WINDOW_SIZE = "WINDOW_SIZE";
	String NO_WINDOW_SIZE = "NO_WINDOW_SIZE";
	String name = "name";
	String key = "key";
	String labelValue = "labelValue";
	String component = "component";
	String SERVICE_KEY = "***SERVICE_IMPL***";
	String COLUMN_NAME = "COLUMN_NAME";
	String join = "join_name";
	String real_name = "real_name";
	String lazyList = "lazyList";
	// boolean DISABLE_WORKFLOW_LISTENER = true;
	boolean production_env = false;
	String TOKEN = "_TOKEN_REQ_KEY_";
	String CRUD_NATURE = "CRUD_NATURE";
	List CRUD_METHODS = Arrays.asList(new String[] { "lista", "inclui", "obtem", "remove", "altera", "obtemLista" });
	String CONTAINER = "***CONTAINER***";
	String FUW = "***FUW***";
	String DAO_SUFIX = "DAO";
	String PROCESS_CALCULATED = "PROCESS_CALCULATED";
	Object type = "type";
	String LIST_HEADER_FIELDS = "LIST_HEADER_FIELDS";
	String MODEL_AND_VIEW = "MODEL_AND_VIEW";

	String COR_DE_FUNDO = "corDeFundo";
	String NEGRITO = "negrito";
	String ITALICO = "italico";
	String FONTE = "fonte";
	String COR_DA_FONTE = "corDaFonte";
	String COR_DE_FUNTO = "corDeFundo";
	String TAMANHO_DA_FONTE = "tamanhoDaFonte";
	String ESCAPE_CHAR = "*";
	String FIELD_CONTEXT_PREFIX = "FIELD_CONTEXT_PREFIX";
	String IS_CONTEXT_FIELD = "IS_CONTEXT_FIELD";
	String ORIGINAL_NAME = "ORIGINAL_NAME";
	int REQUEST_TIMEOUT = 1000000;
	String subject_key = "ID_USUARIO"; // ID_USUARIO
	String subject_service = "UsuarioService";
	String Unit_ID = "ID_UNIDADE";
	String Unit_Service = "UnidadeService";
	String EXPRESSION_PREFIX = "${";
	String EXPRESSION_SUFIX = "}";
	String staticRequest = "__staticRequest";

    String MAIN_REQUEST_ACTION = "MAIN_REQUEST_ACTION" ;
};