package com.aseg.exceptions;

public class ConfigurationException extends GenericRuntimeException {

	public static enum TYPE {
		CANNOT_ROUT_CONNECTION, DATABASE_EMPTY, DB_PARAMS, FIELD_NOT_FOUND, IMPORT_ERROR, LABEL_NOT_FOUND, LANGUAGE_EMPTY, MASTER_USER_REQUIRED, METHOD_NOT_FOUND, PARAM_NOT_FOUND, SYSTEM_NOT_FOUND, USER_EMPTY
	};

	private final TYPE type;

	public ConfigurationException(final String arg0, final Object value, final TYPE type) {
		super(arg0, value);
		this.type = type;
	}

	public ConfigurationException(final String arg0, final String extra, final TYPE type) {
		super(arg0, extra);
		this.type = type;
	}

	public ConfigurationException(final String arg0, final Throwable arg1, final Object value, final TYPE type) {
		super(arg0, arg1, value);
		this.type = type;
	}

	public ConfigurationException(final String arg0, final Throwable arg1, final TYPE type) {
		super(arg0, arg1);
		this.type = type;
	}

	public ConfigurationException(final String message, final TYPE type) {
		super(message);
		this.type = type;
	}

	public ConfigurationException(final Throwable arg0, final TYPE type) {
		super(arg0);
		this.type = type;
	}

	public TYPE getType() {
		return this.type;
	}
}
