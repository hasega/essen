package com.aseg.exceptions;

public class StreamingException extends GenericRuntimeException {

	public StreamingException() {
	}

	public StreamingException(String arg0) {
		super(arg0);
	}

	public StreamingException(Throwable arg0) {
		super(arg0);
	}

	public StreamingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public StreamingException(String arg0, Object value) {
		super(arg0, value);
	}

	public StreamingException(String arg0, Throwable arg1, Object value) {
		super(arg0, arg1, value);

	}

	public StreamingException(String arg0, String extra) {
		super(arg0, extra);
		this.extra = extra;
	}

	String extra;

	@Override
	public String getExtra() {
		return this.extra;
	}

}
