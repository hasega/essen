package com.aseg.exceptions;

public class BusinessException extends GenericRuntimeException {

	public BusinessException() {
	}

	public static final String REGISTRO_REMOVIDO = "error.registro.removido";

	public BusinessException(String arg0) {
		super(arg0);
	}

	public BusinessException(Throwable arg0) {
		super(arg0);
	}

	public BusinessException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public BusinessException(String arg0, Object value) {
		super(arg0, value);
	}

	public BusinessException(String arg0, Throwable arg1, Object value) {
		super(arg0, arg1, value);

	}

	public BusinessException(String arg0, String extra) {
		super(arg0, extra);
	}

}
