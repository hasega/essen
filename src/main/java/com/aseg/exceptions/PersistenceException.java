package com.aseg.exceptions;

public class PersistenceException extends GenericRuntimeException {

	public PersistenceException() {
	}

	public PersistenceException(String arg0) {
		super(arg0);
	}

	public PersistenceException(Throwable arg0) {
		super(arg0);
	}

	public PersistenceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public PersistenceException(String arg0, Object value) {
		super(arg0, value);
	}

	public PersistenceException(String arg0, Throwable arg1, Object value) {
		super(arg0, arg1, value);
	}

}
