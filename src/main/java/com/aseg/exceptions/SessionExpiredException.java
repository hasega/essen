/*
 * SessionExpiredException.java
 *
 * Created on 8 de Dezembro de 2005, 18:07
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.aseg.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;

import com.aseg.seguranca.Usuario;

/**
 * 
 * @author asega
 */
public class SessionExpiredException extends GenericRuntimeException {

	/** Creates a new instance of SessionExpiredException */
	public SessionExpiredException() {
		super();
	}

	public SessionExpiredException(Usuario user) {
		super();

	}

	public SessionExpiredException(Usuario user, String ex) {
		super(ex);

	}

	public void printStackTrace() {
		// LogUtil.debug("SISTEMA NO LOGADO, REDIRECIONANDO 3");
	}

	public void printStackTrace(PrintStream p) {
		// LogUtil.debug("SISTEMA NO LOGADO, REDIRECIONANDO 2");
	}

	public void printStackTrace(PrintWriter p) {
		// LogUtil.debug("SISTEMA NO LOGADO, REDIRECIONANDO ");
	}

}
