package com.aseg.exceptions;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;

import com.aseg.logauditoria.service.LogUtil;

public class LogException {

	// static Logger log = Logger.getLogger("com.aseg");
	ResourceBundle SequenceResource = ResourceBundle.getBundle("ExceptionLog");

	public LogException() {
		abrirExceptionFile();
	}

	public void gravarNoLog(String codigoExcecao) throws IOException {

		String file;
		if (System.getProperty("java.io.tmpdir")
				.charAt(System.getProperty("java.io.tmpdir").length() - 1) == File.separatorChar)
			file = System.getProperty("java.io.tmpdir");
		else
			file = System.getProperty("java.io.tmpdir") + File.separator;

		file = file + SequenceResource.getString("fileOut.File");

		String pattern = SequenceResource.getString("fileOut.layout.ConversionPattern");

		try {

			if (abrirExceptionFile() == 0) {
				/*
				 * if(log.isDebugEnabled()){ PatternLayout patternLayout = new
				 * PatternLayout(pattern); FileAppender fileAppender = new
				 * FileAppender(patternLayout,file);
				 * BasicConfigurator.configure(fileAppender); log.debug(" Codigo
				 * da classe : " + codigoExcecao ); }
				 */
			}
			fecharExceptionFile();
		} catch (Exception ex) {
			// log.error("Oops, deu erro: " + ex.getMessage());
			LogUtil.exception(ex, null);
			throw new GenericRuntimeException(ex);
		}
	}

	private char abrirExceptionFile() {
		char abrir = 1;

		return abrir;
	}

	private char fecharExceptionFile() {
		char fechar = 0;

		return fechar;
	}

}