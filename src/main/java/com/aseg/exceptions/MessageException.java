package com.aseg.exceptions;

public class MessageException extends GenericRuntimeException {

	public MessageException() {
	}

	public MessageException(String arg0) {
		super(arg0);
	}

	public MessageException(Throwable arg0) {
		super(arg0);
	}

	public MessageException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public MessageException(String arg0, Object value) {
		super(arg0, value);
	}

	public MessageException(String arg0, Throwable arg1, Object value) {
		super(arg0, arg1, value);

	}
}
