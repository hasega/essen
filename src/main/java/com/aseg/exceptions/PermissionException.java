package com.aseg.exceptions;

public class PermissionException extends GenericRuntimeException {

	public PermissionException() {
		super();
	}

	public PermissionException(String arg0) {
		super(arg0);
	}

	public PermissionException(String arg0, Object value) {
		super(arg0, value);
	}

	public PermissionException(Throwable arg0) {
		super(arg0);
	}

	public PermissionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public PermissionException(String arg0, Throwable arg1, Object value) {
		super(arg0, arg1, value);
	}

}
