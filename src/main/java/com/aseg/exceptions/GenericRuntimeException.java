package com.aseg.exceptions;

import java.util.ArrayList;
import java.util.List;

public class GenericRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private List<Object> values = new ArrayList<Object>();
	private String extra = "";
	private boolean close = false;

	public void setValues(List<Object> values) {
		this.values = values;
	}

	public GenericRuntimeException() {
		super();
	}

	public GenericRuntimeException(String arg0) {
		super(arg0);
	}

	public GenericRuntimeException(String arg0, Object value) {
		this(arg0);
		values.add(value);
	}

	public GenericRuntimeException(Throwable arg0) {
		super(arg0);
		setAttributes(arg0);
	}

	public GenericRuntimeException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		setAttributes(arg1);
	}

	public GenericRuntimeException(String arg0, Throwable arg1, Object value) {
		super(arg0, arg1);
		setAttributes(arg1);
		values.add(value);
	}

	public void addValues(Object value) {
		values.add(value);
	}

	public Object[] getValues() {
		return values.toArray();
	}

	public GenericRuntimeException(String arg0, String extra) {
		super(arg0);
		this.extra = extra;
	}

	private void setAttributes(Throwable arg0) {
		if (arg0 instanceof GenericRuntimeException) {
			GenericRuntimeException e = (GenericRuntimeException) arg0;
			if (e.values != null) {
				this.values = e.values;
			}
			this.setClose(e.isClose());
		}
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public boolean isClose() {
		return close;
	}

	public void setClose(boolean close) {
		this.close = close;
	}

}
