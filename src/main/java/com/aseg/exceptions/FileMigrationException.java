package com.aseg.exceptions;

public class FileMigrationException extends Exception {

	public FileMigrationException() {

	}

	public FileMigrationException(String arg0) {
		super(arg0);
	}

	public FileMigrationException(Throwable arg0) {
		super(arg0);
	}

	public FileMigrationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
