/**
 * O Usuario mantem em memoria os atributos necessarios para se identificar
 * um Usuario, e ter acesso a suas informacoes de maneira mais simples e mais eficiente.
 */

package com.aseg.seguranca;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import com.aseg.config.Constants;
import com.aseg.exceptions.BusinessException;
import com.aseg.exceptions.SessionExpiredException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.util.ConnectionManager;
import com.aseg.util.DataFormat;
import com.aseg.util.factory.SpringFactory;
import com.aseg.util.service.ParseXml;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.util.sql.Dialeto;
import com.aseg.vo.GenericVO;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class Usuario implements Comparable<Usuario>, Serializable, UserDetails {

    public static final String USER_MESSAGES = "USER_MESSAGES";

    private static Map perfis = new HashMap();

    private static transient SpringFactory factory;

    public static SpringFactory getFactory() {
        return factory;
    }

    public Map vars = new HashMap();

    // public static void setFactory(SpringFactory factory) {
    // Usuario.factory = factory;
    // }

    private Object conRoute = null;

    private String email = null;
    private String serverUrl = null;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return getSenha();
    }

    @Override
    public String getUsername() {
        return getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return !getTrocaSenha();
    }

    @Override
    public boolean isAccountNonLocked() {
        return !isBloqueado();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !getTrocaSenha();
    }

    @Override
    public boolean isEnabled() {
        return !isDeletado();
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    private transient UtilServiceImpl util = new UtilServiceImpl();

    public String getEmail() {
        return email;
    }

    private Date dataExpiracaoSenha;

    private Timestamp dataFinal;

    private Map<Object, Object> filtros = new HashMap<Object, Object>();

    private String hostOrigem;

    private long idUnidadeOrganizacional;

    private String tipoUnidade;

    private String matricula;

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public boolean isSuperUser() {
        return superUser;
    }

    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }

    public String getTipoUnidade() {
        return tipoUnidade;
    }

    public void setTipoUnidade(String tipoUnidade) {
        this.tipoUnidade = tipoUnidade;
    }

    private Long idUsuario;

    private String ipOrigem;

    private boolean isBloqueado;
    private boolean isDeletado;

    private Long lang;

    private boolean ldap;

    private String login;

    private List<String> messages = new ArrayList<String>();

    private boolean mobile;

    private String nome; // O nome do usuario

    private int perfil;

    private boolean superUser = false;

    static transient ResourceBundle langResouces = ResourceBundle.getBundle("ApplicationResources");

    static transient ResourceBundle build = ResourceBundle.getBundle("Build");

    private Collection<Object> pRelevantes;

    private String senha;

    private String sessionId;

    private String system;

    private long tempoValidadeSenha;

    private boolean trocaSenha;

    private String schema;

    private transient LogUtil logger = new LogUtil();

    private String port;

    private transient HttpSession session;

    private String loginConectado;

    private String diasExpiracao;

    private boolean processListeners;

    private Long[] unidadesVisiveis = new Long[]{};

    private Long[] unidadesInvisiveis = new Long[]{};

    private String indexAction;
    private String mainAction;

    public boolean isForceConnect(String login) {
        boolean r = (loginConectado != null && loginConectado.equals(login));
        loginConectado = null;
        return r;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

    public Usuario(final String lang, UtilServiceImpl util) {
        if (lang != null) {
            this.setLang(new Long(lang));

        }
        this.util = util;

    }

    public Long[] getUnidadesInvisiveis() {
        return unidadesInvisiveis;
    }

    public Long[] getUnidadesVisiveis() {
        return unidadesVisiveis;
    }

    public void setUnidadesInvisiveis(Long[] unidadesInvisiveis) {
        this.unidadesInvisiveis = unidadesInvisiveis;
    }

    public void setUnidadesVisiveis(Long[] unidadesVisiveis) {
        this.unidadesVisiveis = unidadesVisiveis;
    }

    public Usuario(Usuario user) {
        this.setLang(user.getLang());
        this.setSystem(user.getSystem());
        this.setConnectionRoute((String) user.getConnectionRoute());
        this.util = user.getUtil();

    }

    public Usuario() {
        // TODO Auto-generated constructor stub
        System.out.println("NOVO USUARIO");
    }

    public void addMessage(final String message, final boolean unique) {

        if (!unique || !this.messages.contains(message)) {
            this.messages.add(message);
        }
    }

    public void addActionMessage(final ServletRequest req, final String message, final String extra,
                                 final boolean close) {
        Map msg = new HashMap();
        msg.put("MESSAGE", message);
        msg.put("EXTRA", extra);
        msg.put("CLOSE", close);

        ((LinkedHashSet) this.getContextVar(req, USER_MESSAGES, LinkedHashSet.class)).add(msg);
    }

    private Object getContextVar(final ServletRequest req, String key, Class c) {
        // TODO Auto-generated method stub
        Object r = getUtil().Evaluate(key, getRequestEvaluationContext(req));
        if (r == null)
            try {
                getRequestEvaluationContext(req).setVariable(key, Class.forName(c.getName()));
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        return r;
    }

    public StandardEvaluationContext getRequestEvaluationContext(final ServletRequest req) {
        if (req.getAttribute(Constants.REQUEST_EVALUATION_CONTEXT) == null) {
            final SpELEnvironmentRoot root = new SpELEnvironmentRoot(req, this);
            final StandardEvaluationContext ctx = new StandardEvaluationContext(root);
            ctx.setBeanResolver(new BeanFactoryResolver(Usuario.getFactory().getBeanFactory()));
            ctx.setVariable("user", this);
            req.setAttribute(Constants.REQUEST_EVALUATION_CONTEXT, ctx);
        }
        return (StandardEvaluationContext) req.getAttribute(Constants.REQUEST_EVALUATION_CONTEXT);

    }

    /**
     * Metodo compareTo
     *
     * @param
     * @return int
     */

    @Override
    public int compareTo(final Usuario user) {
        return getNome().compareTo(user.getNome());
    }

    public String getConnectionRoute() {
        return (String) (conRoute == null ? getSystem() : conRoute);
    }

    public Date getDataExpiracaoSenha() {
        return dataExpiracaoSenha;
    }

    public Timestamp getDataFinal() {

        if (dataFinal == null)
            dataFinal = new DataFormat().nowTimestamp();

        return dataFinal;
    }

    public String getDescricao(final String string) {
        try {
            LogUtil.debug(string, this);
            final ServiceImpl s = (ServiceImpl) getService("ItemLinguagemService");
            final GenericVO v = s.createVO(this)
                    .putKey("NOME_CONFIG_SISTEMA", getLang() + Constants.CACHE_SEPARATOR + string)
                    .searchBy(Constants.CACHE);
            v.setLazyList(true);
            return s.obtem(v, this).get("DESCRICAO_CONFIG_SISTEMA").toString();
        } catch (final Exception e) {
            // LogUtil.exception(e, user);

            // LogUtil.debug("label Faltando :" + string);
            return string;
        }
    }

    public Throwable getRootCause(Throwable th) {
        return ExceptionUtils.getRootCause(th);
    }

    public File getSystemPropertyFile(final String string) {

        if (string == "") {
            return null;
        }
        try {

            final File f = File.createTempFile(getSchema() + string, ".tmp");
            final ByteArrayInputStream fileInputStream = new ByteArrayInputStream(getSystemPropertyByteArray(string));
            int i;
            final FileOutputStream out = new FileOutputStream(f);
            while ((i = fileInputStream.read()) != -1) {
                out.write(i);
            }
            out.flush();
            out.close();

            return f;
        } catch (final Exception e) {

        }
        return null;
    }

    public File getFile(final String string) {

        if (string == "") {
            return null;
        }
        try {
            LogUtil.debug(string, this);
            final ServiceImpl s = (ServiceImpl) getService("ItemLinguagemService");
            final GenericVO v = s.createVO(this)
                    .putKey("NOME_CONFIG_SISTEMA", getLang() + Constants.CACHE_SEPARATOR + string)
                    .searchBy(Constants.CACHE);
            v.setLazyList(true);

            final File f = File.createTempFile(string, ".tmp");
            final ByteArrayInputStream fileInputStream = new ByteArrayInputStream(
                    (byte[]) s.obtem(v, this).get("ARQUIVO_CONFIG_SISTEMA"));
            int i;
            final FileOutputStream out = new FileOutputStream(f);
            while ((i = fileInputStream.read()) != -1) {
                out.write(i);
            }
            out.flush();
            out.close();

            return f;
        } catch (final Exception e) {

        }
        return null;
    }

    public Map<Object, Object> getFiltros() {
        return filtros;
    }

    public String getHelp(final String string) {
        try {
            LogUtil.debug(string, this);
            final ServiceImpl s = (ServiceImpl) getService("ItemLinguagemService");
            final GenericVO v = s.createVO(this)
                    .putKey("NOME_CONFIG_SISTEMA", getLang() + Constants.CACHE_SEPARATOR + string)
                    .searchBy(Constants.CACHE);
            v.setLazyList(true);
            return s.obtem(v, null).get("HELP_CONFIG_SISTEMA").toString();
        } catch (final Exception e) {
            LogUtil.exception(e, this);
            // LogUtil.debug("label Faltando :" + string);
            return string;
        }
    }

    public String getHostOrigem() {
        if (hostOrigem == null) {
            return "null";
        } else {
            return hostOrigem;
        }
    }

    public String getIcon(final String string) {
        try {
            LogUtil.debug(string, this);
            final ServiceImpl s = (ServiceImpl) getService("ItemLinguagemService");
            final GenericVO v = s.createVO(this)
                    .putKey("NOME_CONFIG_SISTEMA", getLang() + Constants.CACHE_SEPARATOR + string)
                    .searchBy(Constants.CACHE);
            v.setLazyList(true);
            return s.obtem(v, this).get("ARQUIVO_CONFIG_SISTEMA").toString();
        } catch (final Exception e) {
            // LogUtil.exception(e, this);
            LogUtil.debug("icon Faltando :" + string, this);
            return null;
        }
    }

    public byte[] getIconByteArray(final String string) {

        if (string == "") {
            return null;
        }
        try {
            LogUtil.debug(string, this);
            final ServiceImpl s = (ServiceImpl) getService("ItemLinguagemService");
            final GenericVO v = s.createVO(this)
                    .putKey("NOME_CONFIG_SISTEMA", getLang() + Constants.CACHE_SEPARATOR + string)
                    .searchBy(Constants.CACHE);
            v.setLazyList(true);
            return (byte[]) s.obtem(v, this).get("ARQUIVO_CONFIG_SISTEMA");

        } catch (final Exception e) {
            return null;
        }

    }

    public long getIdUnidadeOrganizacional() {
        return this.idUnidadeOrganizacional;
    }

    public Long getIdUsuario() {

        return this.idUsuario;

    }

    public String getIp_origem() {
        if (ipOrigem == null) {
            return "null";
        } else {
            return ipOrigem;
        }
    }

    public boolean isBloqueado() {
        return this.isBloqueado;
    }

    public String getLabel(String string) {
        try {
            return langResouces.getString(string);
        } catch (Exception e) {
        }

        if (string == "") {
            return string;
        }
        final ServiceImpl s = (ServiceImpl) getService("ItemLinguagemService");
        if (s == null)
            return string;
        final GenericVO v = s.createVO(this)
                .putKey("NOME_CONFIG_SISTEMA", getLang() + Constants.CACHE_SEPARATOR + string)
                .searchBy(Constants.CACHE);
        v.setLazyList(true);
        Object o = null;
        try {
            o = s.obtem(v, this).get("VALOR_CONFIG_SISTEMA");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (o == null)// LogUtil.exception(e, this);
        {

            if (false)
                return getUtil()
                        .clearForJSON("<a href=javascript:labelRepair(\\'" + string + "\\'" + ")>" + string + "</a>");
            else
                return getUtil().clearForJSON(string);

        } else {
            return getUtil().clearForJSON(o.toString());
        }
    }

    public String getThemeItem(String string) {

        if (string == "") {
            return string;
        }
        try {
            final ServiceImpl s = (ServiceImpl) getService("ItemTemaService");
            final GenericVO v = s.createVO(this)
                    .putKey("NOME_CONFIG_SISTEMA", getLang() + Constants.CACHE_SEPARATOR + string)
                    .searchBy(Constants.CACHE);
            v.setLazyList(true);
            string = s.obtem(v, this).get("VALOR_CONFIG_SISTEMA").toString();
        } catch (final Exception e) {
            // LogUtil.exception(e, this);
            if (string != null && langResouces.containsKey(string)) {
                return langResouces.getString(string);
            }

        }
        return getUtil().clearForJSON(string);
    }

    public void be(String msg, boolean close) {
        BusinessException e = new BusinessException(msg);
        e.setClose(close);
        throw e;
    }

    public Long getLang() {

        if (this.lang == null || this.lang.equals("null")) {

            this.lang = 1L;
            // try {
            // final ServiceImpl s = (ServiceImpl)
            // getService("LinguagemService", this);
            // if (s == null)
            // return null;
            // final GenericVO v =
            // s.createVO(this).putKey("NOME_CONFIG_SISTEMA", "PT-BR")
            // .searchBy("NOME_CONFIG_SISTEMA");
            // v.setLazyList(true);
            // Long l = (Long) s.obtem(v, this).get("ID_CONFIG_SISTEMA");
            // if (l == null)
            // return l;
            // this.lang = new Long(l.toString());
            // } catch (final Exception e) {
            // LogUtil.exception(e, this);
            // return null;
            // }

        }
        return new Long(this.lang);

    }

    public String getLogin() {
        return this.login;
    }

    public List<String> getMessages() {
        return messages;
    }

    public boolean getMobile() {
        return mobile;
    }

    public String getNome() {
        return this.nome;
    }

    public int getPerfil() {
        return perfil;
    }

    public int[] getPermissoes() {
        return (int[]) getPerfis().get(system + Constants.CACHE_SEPARATOR + getPerfil());
    }

    public Collection<Object> getProcessosRelevantes() {
        return this.pRelevantes;
    }

    public String getSchema() {
        if (schema != null)
            return schema; //TODO REMOVE GETFACTORY FROM USER
        if (getFactory().getSystems().get(system) != null) {
            schema = getFactory().getSystems().get(system);
        } else {
            schema = system;
        }
        return schema;
    }

    public String getSenha() {
        return senha;
    }

    public String getDual() {
        return Dialeto.getInstance().getDialeto(this).fromDual();
    }

    /**
     * Retorna o valor da variavel sessionId
     *
     * @return String
     */
    public String getSessionId() throws SessionExpiredException {
        try {
            if (this.sessionId == null) {
                return "null";
            } else {
                return this.sessionId;
            }
        } catch (final Exception e) {
            throw new SessionExpiredException(this);
        }
    }

    public String getSystem() {

        return system;

    }

    public String getSystemPath() {
        if (system == null)
            return null;

        return "/" + system;
    }

    public String getSystemUserProperty(String value) {
        GenericService aspConfigService = (GenericService) util.getService("VariaveisDoUsuarioService", this);
        GenericVO aspConfigVO = aspConfigService.createVO(this);
        aspConfigVO.setFiltro("{gA:[" + "{c:{f:'TIPO_CONFIG_SISTEMA',o:'=',v1:'USUARIO'}}, "
                + "{c:{f:'ID_USUARIO_CADASTRO',o:'=',vc:'" + this.getIdUsuario() + "'}},"
                + "{c:{f:'NOME_CONFIG_SISTEMA',o:'=',v1:'" + value + "'}}" + "]}");

        try {
            aspConfigVO = aspConfigService.obtem(aspConfigVO, this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return aspConfigVO.get("VALOR_CONFIG_SISTEMA") == null ? null
                : aspConfigVO.get("VALOR_CONFIG_SISTEMA").toString();
    }

    public String getSystemProperty(String string) {
        String r = null;
        if (string == "") {
            return null;
        } else {

            final List c = getFactory().getSystemProperty(getSystem());
            List k = getUtil().match(c, "NOME_CONFIG_SISTEMA", string);
            if (k.size() > 0)
                r = (String) ((Map) k.get(0)).get("VALOR_CONFIG_SISTEMA");
            else
                r = null;

        }
        return r == null ? r : getUtil().clearForJSON(r);
    }

    public byte[] getSystemPropertyByteArray(final String string) {

        if (string == "") {
            return null;
        }
        byte[] s;
        try {
            final List c = getFactory().getSystemProperty(getSystem());

            s = (byte[]) ((Map) getUtil().match(c, "NOME_CONFIG_SISTEMA", string).get(0)).get("ARQUIVO_CONFIG_SISTEMA");

        } catch (final Exception e) {
            return null;
        }

        return s;
    }

    public void refreshSystem(String s) {
        try {
            if (s == null)
                return;
            getFactory().refreshSystem(this, s);
        } catch (Exception e) {
            LogUtil.exception(e, this);
        }
    }

    public Object getService(final String name) {
        // beans defined on xml
        if (getFactory() == null)
            return null;

        return getFactory().getSpringBean(name, this);
    }

    public List matchTemplatesEmail(final String string) {

        if (string == "") {
            return null;
        }
        try {
            LogUtil.debug(string, this);
            final ServiceImpl s = (ServiceImpl) getService("TemplateEmailService");
            final GenericVO v = s.createVO(this);

            v.setFiltro("{gA:[{c:{f:'NOME_CONFIG_SISTEMA',o:'%?',v1:'" + string + "'}}]}");
            // v.putKey("NOME_CONFIG_SISTEMA",
            // string);
            v.put("ID_PAI", getLang());
            v.setLazyList(true);
            return s.lista(v, this);
        } catch (final Exception e) {
            LogUtil.exception(e, this);
        }
        return null;
    }

    public Map getTemplateEmail(final String string) {

        if (string == "") {
            return null;
        }
        try {
            LogUtil.debug(string, this);
            final ServiceImpl s = (ServiceImpl) getService("TemplateEmailService");
            final GenericVO v = s.createVO(this).putKey("NOME_CONFIG_SISTEMA", string);
            v.put("ID_PAI", getLang());
            v.setLazyList(true);
            return s.obtem(v, this);
        } catch (final Exception e) {
            LogUtil.exception(e, this);
        }
        return null;
    }

    public long getTempoValidadeSenha() {
        return tempoValidadeSenha;
    }

    public boolean getTrocaSenha() {
        return this.trocaSenha;
    }

    public boolean isLdap() {
        return ldap;
    }

    public boolean isMobile() {
        return mobile;
    }

    public void reset() {

        // conRoute = null;
        // system = null;

        dataExpiracaoSenha = null;

        dataFinal = null;

        filtros = new HashMap<Object, Object>();

        hostOrigem = null;

        idUnidadeOrganizacional = -1;
        unidadesInvisiveis = new Long[]{};
        unidadesVisiveis = new Long[]{};

        idUsuario = null;

        ipOrigem = null;

        isBloqueado = false;

        lang = null;

        ldap = false;

        login = null;

        messages = new ArrayList<String>();

        mobile = false;

        nome = null; // O nome do usuario, por extenso

        perfil = -1;

        senha = null;

        sessionId = null;

        tempoValidadeSenha = -1;

        trocaSenha = false;

    }

    public void setConnectionRoute(final String route) {
        this.conRoute = route;
    }

    public void setDataExpiracaoSenha(final Date data_expiracao_senha) {
        if (this.isSuperUser()) {
            this.dataExpiracaoSenha = new Date(System.currentTimeMillis() + 10000000000L);
        } else {
            this.dataExpiracaoSenha = data_expiracao_senha;
        }
    }

    public void setDataFinal(final Timestamp DataFinal) {
        this.dataFinal = DataFinal;
    }

    public void setFiltros(final Map<Object, Object> filtros) {
        this.filtros = filtros;
    }

    public void setHostOrigem(final String hostOrigem) {
        this.hostOrigem = hostOrigem;
    }

    public void setIdUnidadeOrganizacional(final long idUnidadeOrganizacional) {
        List unidades = new ArrayList();

        if (this.idUnidadeOrganizacional != idUnidadeOrganizacional) {
            setUnidadesVisiveis((Long[]) unidades.toArray(new Long[]{}));
        }

        this.idUnidadeOrganizacional = idUnidadeOrganizacional;

        {
            unidades.add(idUnidadeOrganizacional);

        }

        Object[] perm = (Object[]) getUnidadesVisiveis();
        if (perm != null)
            for (int i = 0; i < perm.length; i++) {
                unidades.add(new Long(perm[i].toString()));
            }

        setUnidadesVisiveis((Long[]) unidades.toArray(new Long[]{}));
        unidades.clear();

        Object[] rest = (Object[]) getUnidadesInvisiveis();
        if (rest != null)
            for (int i = 0; i < rest.length; i++) {
                unidades.addAll(getIdsUnidadesDown(rest[i]));
                unidades.add(new Long(rest[i].toString()));
            }

        setUnidadesInvisiveis((Long[]) unidades.toArray(new Long[]{}));

    }

    public List<Long> getIdsUnidadesDown(Object idUnidade) {
        ServiceImpl unServ = (ServiceImpl) getService(Constants.Unit_Service);
        GenericVO unVO = unServ.createVO(this);
        unVO.put("SEGMENTBY", "DOWN");
        unVO.putKey(Constants.Unit_ID, new Long("" + idUnidade));
        unVO.setFiltro("");
        unVO.setPaginate(false);

        try {
            unVO = unServ.obtem(unVO, this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Long> longResult = new ArrayList<Long>();
        Object[] unFilhos = (Object[]) unVO.get("ID_UNIDADE_FILHO");
        if (unFilhos != null) {
            for (Object o : unFilhos) {
                longResult.add(new Long("" + o));
            }
        }
        return longResult;
    }

    public void setIdUsuario(final Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setIpOrigem(final String ipOrigem) {
        this.ipOrigem = ipOrigem;
    }

    public void setIsBloqueado(final boolean isBloqueado) {
        this.isBloqueado = isBloqueado;
    }

    public void setLang(final Long lang) {
        if (lang != null) {
            this.lang = lang;
        }
    }

    public void setLdap(final boolean b) {
        ldap = b;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public void setMobile(final boolean isMobile) {
        this.mobile = isMobile;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setPerfil(final int perfil) {
        try {
            if (getPerfis().get(perfil) == null) {
                refreshPerfil(String.valueOf(perfil));
            }
        } catch (final Exception e) {
            LogUtil.exception(e, this);
        }
        this.perfil = perfil;
    }

    public void refreshPerfil(final String perfil) throws Exception {
        final ServiceImpl s = (ServiceImpl) getService("PerfilService");
        final GenericVO v = s.createVO(this).putKey("ID_PERFIL", perfil);
        v.setLazyList(true);
        String[] p = null;

        // TODO Esse tryCatch e temporario, ate todos os clientes estiverem
        // com o novo padrao de serializacao de permissoes
        GenericVO perfisIdDB = s.obtem(v, this);
        try {
            p = (String[]) perfisIdDB.get("ID_PERMISSAO");
        } catch (Exception e) {
            p = (String[]) loadPermissionsHash(perfisIdDB.get("ID_PERMISSAO").toString());
        }

        if (p == null)
            return;
        int[] per = new int[p.length];

        for (int i = 0; i < p.length; i++) {
            per[i] = Integer.parseInt(p[i]);
        }
        Arrays.sort(per);
        synchronized (getPerfis()) {
            getPerfis().put(system + Constants.CACHE_SEPARATOR + new Long(perfil).intValue(), per);
        }

    }

    private String[] loadPermissionsHash(String p) {
        List<String> res = new ArrayList<String>();
        if (!p.isEmpty()) {
            String[] items = p.split("#");
            for (String i : items) {
                if (getUtil().JSONtoMap(getUtil().replace(i, "$", ",")).get("value") != null
                        && !getUtil().JSONtoMap(getUtil().replace(i, "$", ",")).get("value").equals("\"null\"")) {
                    res.add(getUtil().JSONtoMap(getUtil().replace(i, "$", ",")).get("value").toString());
                }
            }
        }
        return res.toArray(new String[]{});
    }

    public void setPerfilVar(String name, Object value) {

        final ServiceImpl s = (ServiceImpl) getService("PerfilService");

        try {
            if (getPerfis()
                    .get(system + Constants.CACHE_SEPARATOR + name + Constants.CACHE_SEPARATOR + perfil) == null) {

                getPerfis().put(system + Constants.CACHE_SEPARATOR + name + Constants.CACHE_SEPARATOR + perfil, value);
            }
        } catch (final Exception e) {
            LogUtil.exception(e, this);
        }
        this.perfil = perfil;
    }

    public Object getPerfilVar(String name) {
        return getPerfis().get(system + Constants.CACHE_SEPARATOR + name + Constants.CACHE_SEPARATOR + perfil);
    }

    public void setSchema(final String schema) {
        this.schema = schema;
    }

    public void setSenha(final String senha) {
        this.senha = senha;
    }

    public void setSessionId(final String id) {
        this.sessionId = id;
    }

    public void setSystem(final String system) {
        if (this.system != null && !this.system.equals(system)) {
            this.reset();
        }

        this.system = system;
    }

    public void setTempoValidadeSenha(final long tempoValidadeSenha) {
        this.tempoValidadeSenha = tempoValidadeSenha;
    }

    public void setTrocaSenha(final boolean senha) {
        this.trocaSenha = senha;
    }

    public String getSegmentBy(String service, String alias, String direction) {

        if (ConnectionManager.getInstance(this).getBancoConectado(this) == ConnectionManager.ORACLE) {
            GenericService s = (GenericService) getService(service);

            return s.getDAO().getSegmentBy(this, direction, alias, s.createVO(this))[1];
        } else {
            GenericService s = (GenericService) getService(service);
            GenericVO v = s.createVO(this);

            return s.getDAO().getSegmentBy(this, "DOWN-ARRAY", alias, v)[1];
        }
    }

    public boolean verificaPermisao(final int permissionView) {
        if (isSuperUser())
            return true;

        try {
            if (getPermissoes() == null || getPermissoes().length == 0) {
                return true;
            }
            if (permissionView == 0) {
                return true;
            }
            if (Arrays.binarySearch(getPermissoes(), permissionView) > -1) {
                return false;
            } else {
                return true;
            }
        } catch (final Exception e) {
            LogUtil.exception(e, this);
            return true;
        }

    }

    public boolean verificaVisibilidadeTotal() {
        return this.isSuperUser();
        // return verificaPermisao("100");
    }

    public void setEmail(String string) {
        this.email = string;
    }

    public Map toMap() {
        try {
            return new GenericVO().putAllAndGet(BeanUtils.describe(this), true);
        } catch (IllegalAccessException e) {
            LogUtil.exception(e, this);
        } catch (InvocationTargetException e) {
            LogUtil.exception(e, this);
        } catch (NoSuchMethodException e) {
            LogUtil.exception(e, this);
        }
        return null;
    }

    public LinkedHashSet getActionMessages(ServletRequest req) {

        return ((LinkedHashSet) this.getContextVar(req, USER_MESSAGES, LinkedHashSet.class));

    }

    public String getSystemVersion() {
        return build.getString("versao");

    }

    public String getDataCompilacao() {
        return build.getString("data_compilacao");

    }

    public boolean SystemPropertyIsTrue(String string) {
        String sp = getSystemProperty(string);
        if (sp != null && sp.equalsIgnoreCase("true"))
            return true;
        return false;
    }

    public String setConfigUsuario(String parametro, Object value) {
        final ServiceImpl s = (ServiceImpl) getService("UsuarioConfigService");
        final GenericVO v = s.createVO(this);

        v.put("NOME_CONFIG_SISTEMA", parametro);
        v.put("VALOR_CONFIG_SISTEMA", value);

        try {
            s.merge(v, this);
            return value.toString();
        } catch (Exception e) {
            LogUtil.exception(e, this);
        }
        return null;

    }

    public UtilServiceImpl getUtil() {
        return this.util;
    }

    public LogUtil getLogger() {
        return this.logger;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public HttpSession getSession() {
        return session;
    }

    public void setForceConnect(String b) {
        loginConectado = b;

    }

    public Map getInfo() {

        Map a = new HashMap();
        a.put("NOME", getNome());

        a.put("ID_SESSAO", getSessionId());
        a.put("LOGIN", getLogin());
        a.put("IP_ORIGEM", getIp_origem());
        return a;
    }

    public void setDiasExpiracao(String d) {
        diasExpiracao = d;

    }

    public String getDiasExpiracao() {
        return diasExpiracao;
    }

    public String getConfigUsuario(String parametro) {
        final ServiceImpl s = (ServiceImpl) getService("UsuarioConfigService");
        GenericVO v = s.createVO(this);

        v.putKey("NOME_CONFIG_SISTEMA", parametro);
        v.setLazyList(true);
        try {
            v = s.obtem(v, this);
            return v.get("VALOR_CONFIG_SISTEMA") == null ? "" : v.get("VALOR_CONFIG_SISTEMA").toString();
        } catch (Exception e) {
            LogUtil.exception(e, this);
        }
        return null;
    }

    public boolean isDeletado() {
        return isDeletado;
    }

    public void setDeletado(boolean isDeletado) {
        this.isDeletado = isDeletado;
    }

    public boolean isProcessListeners() {
        return processListeners;
    }

    public void setProcessListeners(boolean b) {
        this.processListeners = b;
    }

    public String getLoginLogado() {
        return loginConectado;
    }

    public ParseXml getSysParser() {
        return (ParseXml) getService("ParseSys");
    }

    public String getBoolLabel(String v) {
        if (v != null && v.equals("T"))
            return getLabel("label.sim");
        else
            return getLabel("label.nao");
    }

    public String getLabel(String key, final Object[] values) {
        if (key == null) {
            return "null";
        }

        if (key != null && key.startsWith("Wrapped"))
            key = key.substring(key.indexOf(":") + 1, key.indexOf("(<"));
        String message = this.getLabel(key);
        if (message == null) {
            return "Sem Mensagem";
        }
        message = message.replaceAll("\"", "");
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                try {
                    message = util.replace(message.toString(), "{" + i + "}", this.getLabel(values[i].toString()));
                } catch (final Exception e) {
                    continue;
                }

            }
        }
        return message == null ? "" : message;
    }

    public String getEncode() {
        String e = getSystemProperty("USER_ENCODE");
        if (e == null)
            e = "ISO-8859-1";
        return e;
    }

    public static Map getPerfis() {
        return perfis;
    }

    public static void setPerfis(Map perfis) {
        Usuario.perfis = perfis;
    }

    @Override
    public String toString() {
        return "{" + "nome : '" + nome + "'" + ", login : '" + login + "'" + ", conRoute : '" + conRoute + "'"
                + ", email : '" + email + "'" + ", serverUrl : '" + serverUrl + "'" + ", hostOrigem : '" + hostOrigem
                + "'" + ", idUnidadeOrganizacional : '" + idUnidadeOrganizacional + "'" + ", idUsuario : '" + idUsuario
                + "'" + ", ipOrigem : '" + ipOrigem + "'" + ", isBloqueado : '" + isBloqueado + "'" + ", isDeletado : '"
                + isDeletado + "'" + ", lang : '" + lang + "'" + ", superUser : '" + superUser + "'" + ", sessionId : '"
                + sessionId + "'" + ", system : '" + system + "'" + ", schema : '" + schema + "'"
                + ", diasExpiracao : '" + diasExpiracao + "'" + "}";
    }

    public String getDefaultSystemFont() {
        // TODO Auto-generated method stub
        String f = getSystemProperty("MISC_DEFAULT_FONT_NAME");
        return f == null ? "Arial" : f;
    }

    public static void setFactory(SpringFactory springFactory) {
        // TODO Auto-generated method stub
        factory = springFactory;
    }

    public static Usuario getSystemUser() {
        // TODO Auto-generated method stub
        return new Usuario();
    }

    @SuppressWarnings("unused")
    private static class SpELEnvironmentRoot {

        private final ServletRequest request;
        private final Usuario user;
        private final GenericVO VO;
        private final GenericVO masterVO;

        /**
         * Create a new SpEL environment root for use in a SpEL evaluation
         * context.
         *
         * @param request
         *            web request
         */
        private SpELEnvironmentRoot(ServletRequest request, Usuario user) {
            this.request = request;
            this.user = user;
            this.VO = (GenericVO) request.getAttribute(Constants.VOKey);
            this.masterVO = (GenericVO) request.getAttribute(Constants.MASTERVOKey);

        }

        /**
         * Get the request associated with this environment root.
         */
        public ServletRequest getRequest() {
            return request;
        }

        /**
         * The person associated with this environment root
         */
        public Usuario getUser() {
            return this.user;
        }

        public GenericVO getVO() {
            return this.VO;
        }

        public GenericVO getmasterVO() {
            return this.masterVO;
        }

    }

    public String getIndexAction() {
        // TODO Auto-generated method stub
        return this.indexAction;
    }

    public String getMainAction() {
        // TODO Auto-generated method stub
        return this.mainAction;
    }

    public void setIndexAction(String string) {
        // TODO Auto-generated method stub
        this.indexAction = string;
    }

    public void setMainAction(String string) {
        // TODO Auto-generated method stub
        this.mainAction = string;
    }
}
