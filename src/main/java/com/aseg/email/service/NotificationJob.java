package com.aseg.email.service;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.sql.Dialeto;
import com.aseg.util.sql.InterfaceDialeto;
import com.aseg.vo.GenericVO;

public class NotificationJob extends ServiceImpl implements StatefulJob {

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {
		// TODO Auto-generated method stub
		final Usuario buser = (Usuario) ctx.getJobDetail().getJobDataMap().get("user");

		Iterator ix = Dialeto.getDialetos().keySet().iterator();
		String cn = buser.getConnectionRoute();
		while (ix.hasNext()) {
			try {

				String cr = (String) ix.next();
				buser.setConnectionRoute(cr);
				Usuario user = null;
				try {
					user = Usuario.getSystemUser();

				} catch (Exception e) {
					LogUtil.exception(e, buser);
				}

				if (user.SystemPropertyIsTrue("MAIL_PRECEDENT_NOTIFICATION")) {

					final List<Map<String, String>> c = (List) user.getSysParser().getListValuesFromXQL(

							"/systems/system[@name='" + user.getSchema()
									+ "']/module/bean/property[contains(@notifyMe,'dayField')]",
							"order", user, true, true);
					for (Iterator iterator = c.iterator(); iterator.hasNext();) {
						Map<String, String> map = (Map<String, String>) iterator.next();
						if (!ParseXmlService.canAccess(map, user, true))
							continue;

						Map nm = user.getUtil().JSONtoMap(map.get("notifyMe"));

						GenericService ser = (GenericService) getServiceById(map.get("parent_id"), user);

						if (ser == null)
							return;

						GenericVO v = ser.createVO(user);
						v.setProcess(false);
						v.put("NO_SCRIPT_PROCESS", true);

						// TO_DATE( DATA -
						// DIAS_ANTECEDENCIA_EMAIL,'dd/MM/yyyy')<=
						// TO_DATE(
						// SYSDATE,'dd/MM/yyyy')
						InterfaceDialeto d = Dialeto.getInstance().getDialeto(user);
						String t = ser.getProperties().get("table") + ".";
						String fc = d.getTruncDate(
								d.subtractDaysFromDate(t + nm.get("dayField").toString(), t + map.get(Constants.name)))
								+ " <= " + d.getTruncDate(d.getSysdate());

						fc = user.getUtil().replace(fc, "'", "^");

						String filtro = "{gA:[{c:{f:'#C',o:'=',v1:'" + fc + "'}}";

						if (nm.get("listenerField") != null)
							filtro += ", {c:{f:'" + nm.get("listenerField") + "',o:'=',v1:'T'}}";

						if (nm.get("flagField") != null)
							filtro += ", {c:{f:'" + nm.get("flagField") + "',o:'=',v1:'F'}}";

						filtro += "]}";
						v.setFiltro(filtro);
						v.setAnulate(false);
						v.setProcess(false);
						v.setLazyList(false);
						v.setPaginate(false);
						try {
							List items = ser.lista(v, user);
							EmailService emailService = ser.getEmailService(user);
							Usuario stubUser = new Usuario(user);
							GenericService userService = (GenericService) ser.getService("UsuarioService", user);
							for (Iterator iterator2 = items.iterator(); iterator2.hasNext();) {
								GenericVO vo = (GenericVO) iterator2.next();
								vo = ser.obtem(vo, user);

								String[] uf = nm.get("userMailField").toString().split(">");

								String[] users = null;
								try {
									users = (String[]) vo.get(uf[0]);
								} catch (Exception e) {
									users = new String[] { vo.get(uf[0]).toString() };
								}
								for (int i = 0; i < users.length; i++) {
									stubUser.reset();
									GenericVO uvo = userService.createVO(user);
									Long uid = new Long(users[i]);
									if (uf.length > 1) {
										try {
											Map p = ser.getProperty(uf[0], stubUser);
											GenericService s = (GenericService) getService((String) p.get("service"),
													stubUser);
											GenericVO vc = s.createVO(stubUser);
											vc.putKey(s.getPrimaryKey().get("name"), uid);
											uid = (Long) s.obtem(vc, stubUser).get(uf[1]);
										} catch (Exception e) {
											LogUtil.exception(e, user);
										}
									}

									uvo.putKey("ID_USUARIO", uid);
									uvo = userService.obtem(uvo, stubUser);
									bindUser(stubUser, uvo, false);

									GenericVO evo = emailService.createVO(user);
									evo.put(EmailJob.EMAIL_TEMPLATE, nm.get("template"));
									evo.put(EmailJob.EMAIL_PARA, stubUser.getEmail());
									evo.putAll(vo);
									// evo.put(EmailJob.FILE_ATTACHMENT, att);
									emailService.send(evo, stubUser);
								}
								if (nm.get("flagField") != null) {
									Object key = vo.get(vo.getKey());
									vo.clear();
									vo.putKey(vo.getKey(), key);
									vo.put((String) nm.get("flagField"), "T");
									vo.setAnulate(false);
									ser.altera(vo, user);
								}

							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							LogUtil.exception(e, user);
						}

					}
				}
				if (user.SystemPropertyIsTrue("MAIL_NOTIFICATION")) {
					GenericService s = (GenericService) user.getService("PoolEmailService");
					GenericVO nvo = s.createVO(user);

					try {
						nvo.setPaginate(false);
						String filtro = "{gA:[{c:{f:'PROCESSED',o:'=',v1:'F'}}]}";
						nvo.setFiltro(filtro);
						List a = s.lista(nvo, user);
						for (Iterator iterator = a.iterator(); iterator.hasNext();) {
							GenericVO object = (GenericVO) iterator.next();
							object = s.obtem(object, user);
							MimeMessage message = null;
							try {
								message = new MimeMessage(createSession(user),
										new ByteArrayInputStream(object.get("MSG").toString().getBytes()));
								Transport.send(message);
							} catch (Exception e) {
								object.put("PROCESSED", "T");
								object.put("ERROR", user.getUtil().getStackTrace(e));
								s.altera(object, user);
								continue;

							}
							s.remove(object, user);
							user.getLogger().log(user, new HashMap(), "NotificacaoEmailService",
									"Enviado email com titulo " + message.getSubject() + " para "
											+ message.getSubject().toString() + " em " + user.getUtil().now(),
									new HashMap(), new HashMap()

									, "envio correto", getProperties().get("id").toString(), "0");

						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						LogUtil.exception(e, user);
					}
				}

			} catch (Exception e) {
				LogUtil.exception(e, buser);
			} finally {
				buser.setConnectionRoute(cn);
			}
		}

	}

	static class Autenticacao extends Authenticator {

		private String username = null;

		private String password = null;

		public Autenticacao(final String user, final String pwd) {
			this.username = user;
			this.password = pwd;

		}

		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}

		public String getPwd() {
			return this.password;
		}

		public String getUser() {
			return this.username;
		}

		public void setPwd(final String password) {
			this.password = password;
		}

		public void setUser(final String username) {
			this.username = username;
		}

	}

	public static Session createSession(Usuario user) throws NamingException {

		Map ses = new HashMap();
		String JNDI_NAME = null, host = "", port = null;
		final Properties props = new Properties();
		Autenticacao auth = null;
		String tipo = null;
		String servidor = null;
		String usuario = null;
		String senha = null;
		String segura = null;

		tipo = user.getSystemProperty("MAIL_SMTP_TIPO");
		if (tipo == null) {
			LogUtil.error("Configuraes de EMAIL nao configuradas ");
			return null;
		}
		servidor = user.getSystemProperty("MAIL_SMTP_SERVIDOR");
		usuario = user.getSystemProperty("MAIL_USUARIO_SMTP");
		senha = user.getSystemProperty("MAIL_SENHA_SMTP");
		segura = user.getSystemProperty("MAIL_SSL_SMTP");
		if (segura == null)
			segura = "F";

		String de = user.getSystemProperty("MAIL_FROM");

		Session session = null;
		if (tipo.equalsIgnoreCase("R")) {
			JNDI_NAME = servidor;

		/*	final InitialContext context = new InitialContext();
			try {

				final Context envCtx = (Context) context.lookup("java:comp/env");
				session = (Session) envCtx.lookup(JNDI_NAME);

			} catch (final Exception e) {
				LogUtil.debug("No java:comp/env", user);
				try {
					session = (Session) context.lookup("java:comp/" + JNDI_NAME);
				} catch (final Exception e1) {
					LogUtil.debug("No java:comp/" + JNDI_NAME, user);
					try {
						session = (Session) context.lookup("java:" + JNDI_NAME);
					} catch (final Exception e2) {
						LogUtil.debug("No java:" + JNDI_NAME, user);
						try {
							// Context envCtx = (Context)
							// context.lookup("java:comp/env");
							// session = (Session)
							// envCtx.lookup("mail/Session");
							session = (Session) context.lookup(JNDI_NAME);
						} catch (final Exception e3) {
							LogUtil.debug("No " + JNDI_NAME, user);
							try {
								session = (Session) context.lookup("java:comp/env/" + JNDI_NAME);
							} catch (final Exception e4) {
								LogUtil.debug("No  java:comp/env/" + JNDI_NAME, user);
								LogUtil.exception(e4, user);
							}
						}
					}
				}
			} */
		} else {
			// Configuraes para formar a mensagem de envio
			try {
				final String[] ser = servidor.split(":");
				host = ser[0];
				try {
					port = ser[1];
					props.put("mail.smtp.port", port);
				} catch (final Exception e) {

				}
				// props.put("mail.debug", Constants.true_str);
				props.setProperty("mail.transport.protocol", "smtp");
				props.put("mail.host", host);
				props.put("de", de);
				// props.put("assunto", assunto);
				// props.put("mensagem", mens);
				// props.put("cabecalho", cabecalho);

				if (usuario != null && usuario.length() > 0) {
					auth = new Autenticacao(usuario, senha);
					props.put("mail.smtp.auth", Constants.true_str);

					props.put("mail.user", usuario);
					props.put("mail.password", senha);
					// session = Session.getDefaultInstance(props, auth);
				} else {
					props.put("mail.smtp.auth", Constants.false_str);

				}

				if (port != null) {
					props.put("mail.smtp.port", port);
					if (segura.equalsIgnoreCase("T")) {
						props.put("mail.smtp.socketFactory.port", port);
					}
				}
				if (segura.equalsIgnoreCase("T")) {
					props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
					props.put("mail.smtp.socketFactory.fallback", Constants.false_str);
				}

				// SecurityManager security = System.getSecurityManager();

				session = Session.getInstance(props, auth);

				// // sessions.put(user.getConnectionRoute(), ses);

			} catch (final Exception e) {
				LogUtil.exception(e, user);
			}
		}

		return session;
	}
}
