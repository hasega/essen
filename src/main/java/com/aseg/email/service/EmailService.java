package com.aseg.email.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.ContentType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.ParseException;
import javax.mail.search.FlagTerm;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.util.service.ParseXmlService;
import com.aseg.vo.GenericVO;
import com.sun.mail.util.BASE64DecoderStream;

public class EmailService extends ServiceImpl {

	public void enviarEmail(final GenericVO vo, final Usuario user) throws Exception {

		send(vo, user);
	}

	/**
	 * Monta Mensagem
	 * 
	 * @throws Exception
	 */

	public void send(final GenericVO vo, final Usuario user) throws Exception {
		/*
		 * try { String millisId = "" + System.currentTimeMillis(); String
		 * emailId = "" + millisId.hashCode(); String groupId = "emailGroup::" +
		 * millisId; Trigger trigger = TriggerUtils.makeImmediateTrigger(0, 0);
		 * trigger.setName("EmailService::Hash(" + emailId + ")::User(" +
		 * user.getIdUsuario() + ")::Millis(" + millisId + ")");
		 * trigger.setGroup(groupId); JobDetail job = new JobDetail("Email(" +
		 * emailId + ")::Millis(" + millisId + ")", groupId, EmailJob.class);
		 * final Map param = job.getJobDataMap(); param.put("VO", vo);
		 * param.put("user", user); final StdScheduler std = (StdScheduler)
		 * getService("EmailSheduler", user); std.scheduleJob(job, trigger); }
		 * catch (final Exception e) { e.printStackTrace(); }
		 */

		EmailJob j = new EmailJob();
		try {
			List a = j.getMessages(vo, user, Session.getDefaultInstance(new Properties()));
			for (Iterator iterator = a.iterator(); iterator.hasNext();) {
				MimeMessage message = (MimeMessage) iterator.next();
				GenericService s = (GenericService) user.getService("PoolEmailService");
				GenericVO nvo = s.createVO(user);
				nvo.put("ID_EMAIL", message.hashCode());
				nvo.put("EMAIL", parseAddr(message.getRecipients(Message.RecipientType.TO)));
				ByteArrayOutputStream byte1 = new ByteArrayOutputStream();
				message.writeTo(byte1);

				nvo.put("MSG", byte1.toString());
				s.inclui(nvo, user);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}

	}

	private Object parseAddr(Address[] recipients) {
		// TODO Auto-generated method stub
		String a = "";
		for (int i = 0; i < recipients.length; i++) {
			Address l = recipients[i];
			a += l.toString() + ";";
		}
		return a;
	}

	public void sendSync(final GenericVO vo, final Usuario user) throws Exception {
		EmailJob j = new EmailJob();
		List a = j.getMessages(vo, user, NotificationJob.createSession(user));
		for (Iterator iterator = a.iterator(); iterator.hasNext();) {
			MimeMessage object = (MimeMessage) iterator.next();

			Transport.send(object);
		}

	}

	public void sendMeSync(final GenericVO vo, final Usuario user) throws Exception {
		EmailJob j = new EmailJob();
		GenericService s = (GenericService) user.getService("PoolEmailService");
		MimeMessage m = new MimeMessage(NotificationJob.createSession(user),
				new ByteArrayInputStream(vo.get("MSG").toString().getBytes()));
		Transport.send(m);
		vo.setMethod("remove");
		s.remove(vo, user);

	}

	public List getMessages(GenericVO v, Usuario user) {
		Store store = null;
		Folder inbox = null;
		List r = new ArrayList();

		try {
			// -- Get hold of the default session --
			Properties props = System.getProperties();
			String type = "";
			if (v.get("SERVERTYPE").toString().equals("pop"))
				type = "pop3";
			else if (v.get("SERVERTYPE").toString().equals("imap"))
				type = "imap";
			if (v.get("SEGURO") != null && v.get("SEGURO").equals("true")) {
				// Properties to connect as secure
				props.setProperty("mail." + v.get("SERVERTYPE") + ".socketFactory.class",
						"javax.net.ssl.SSLSocketFactory");
				props.setProperty("mail." + v.get("SERVERTYPE") + ".socketFactory.fallback", "false");
				type += "s";
			}
			Session session = Session.getDefaultInstance(props, null);
			// -- Get hold of a POP3/IMAP message store, and connect to it --
			store = session.getStore(type);
			store.connect(v.get("SERVER").toString(), v.get("USERNAME").toString(), v.get("PASSWORD").toString());
			if (!store.isConnected()) {
				store.connect();
			}
			// -- Try to get hold of the default folder --
			inbox = store.getDefaultFolder();
			if (inbox == null)
				throw new Exception("No default folder");
			// -- ...and its INBOX --
			inbox = inbox.getFolder("INBOX");
			if (inbox == null)
				throw new Exception("No INBOX folder");
			// -- Open the folder for read only --
			// if (delete || emptyTrash) inbox.open(Folder.READ_WRITE);
			// else inbox.open(Folder.READ_ONLY);

			// To check the email as SEEN we need to open folder as READ_WRITE
			inbox.open(Folder.READ_WRITE);

			// -- Get ONLY NOT SEEN messages and process them --
			FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
			Message messages[] = inbox.search(ft);
			for (Message msg : messages) {
				if (v.get("ONLYONE") != null && v.get("ONLYONE").equals("true")) {
					if (r.size() == 1)
						return r;
				}
				r.add(print(msg, user));
				if (v.get("DELETEREAD") != null && v.get("DELETEREAD").equals("true")) {
					msg.setFlag(Flags.Flag.SEEN, true);
					msg.setFlag(Flags.Flag.DELETED, true);
				} else {
					msg.setFlag(Flags.Flag.SEEN, true);
					msg.setFlag(Flags.Flag.DELETED, false);
				}
			}
		} catch (Exception ex) {
			LogUtil.exception(ex, user);
		} finally {
			// -- Close down nicely --
			try {
				if (inbox != null)
					inbox.close(v.get("EMPTYTRASH") != null && v.get("EMPTYTRASH").equals("true"));
				if (store != null)
					store.close();
			} catch (Exception ex2) {
				LogUtil.exception(ex2, user);
			}
		}
		return r;
	}

	public GenericVO print(Message msg, Usuario user) throws MessagingException, IOException {
		// Get the header information

		GenericVO v = new GenericVO();
		String from = ((InternetAddress) msg.getFrom()[0]).getPersonal();
		if (from == null) {
			from = ((InternetAddress) msg.getFrom()[0]).getAddress();
		}
		v.put("FROM", from);
		String subject = msg.getSubject();
		v.put("SUBJECT", subject);
		// -- Get the message part (i.e. the message itself) --

		Object content = null;
		try {
			content = msg.getContent();
		} catch (Exception e) {
			// TODO: handle exception
			LogUtil.exception(e, user);
			throw new MessagingException("No foi possvel ler contedo do email.", e);
		}
		// -- or its first body part if it is a multipart message --
		if (content instanceof Multipart) {
			Multipart multipart = (Multipart) content;
			for (int i = 0, n = multipart.getCount(); i < n; i++) {
				Part part = multipart.getBodyPart(i);
				String ct = part.getContentType();
				try {
					ContentType contentType = new ContentType(ct);
					v.put("CONTENT-TYPE", contentType.toString());
				} catch (ParseException pex) {
					LogUtil.exception(pex, user);
					v.put("BAD CONTENT-TYPE", ct);
				}
				if (!Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {

					if (part.isMimeType("multipart/*")) {
						Multipart mp = (Multipart) part.getContent();
						int count = mp.getCount();
						for (int j = 0; j < count; j++) {
							Part p = mp.getBodyPart(j);

							if (p.getDisposition() == Part.ATTACHMENT) {
								v.put("ATT-" + j, p.getInputStream());
								v.put("ATT-FILENAME-" + j, p.getFileName());

							} else {
								v.put("BODY", readTextFile(p));
								v.put("BODY-STREAM", mp.getBodyPart(j).getInputStream());
							}
						}

						continue;
					}
					if (part.isMimeType("text/plain")) {
						v.put("BODY-STREAM", new ByteArrayInputStream(readTextFile(part).getBytes()));
						v.put("BODY", readTextFile(part));
						continue;
					}
				} else if (part.getDisposition().equalsIgnoreCase(Part.ATTACHMENT)) {
					// Save the text file. We can read it without saving.

					if (part.getInputStream() instanceof BASE64DecoderStream) {
						v.put("ATT-" + i,
								new ByteArrayInputStream(ParseXmlService.convertStreamToByte(part.getInputStream())));
					} else {
						v.put("ATT-" + i, part.getInputStream());
					}

					v.put("ATT-FILENAME-" + i, part.getFileName());

					// Leitura do body do email. Pode ser feito um parser.
					// Alguns servers transformam o email em HTML, assim eles
					// repetem o BODY como TEXT e HTML
					// Read the text file.
					// readTextFile(part);
					continue;
				}
			}
		} else {
			v.put("BODY", content.toString());
			v.put("BODY-STREAM", new ByteArrayInputStream(content.toString().getBytes()));

		}
		return v;
	}

	// public static void printMultipart(Part part) throws MessagingException,
	// IOException {
	// String ct = part.getContentType();
	// try {
	// System.out.println("CONTENT-TYPE: " + (new ContentType(ct)).toString());
	// } catch (ParseException pex) {
	// System.out.println("BAD CONTENT-TYPE: " + ct);
	// }
	//
	// /*
	// * Using isMimeType to determine the content type avoids
	// * fetching the actual content data until we need it.
	// * CAN BE REMOVED!!!!
	// */
	// if (part.isMimeType("text/plain") || part.isMimeType("text/html") ||
	// part.isMimeType("text/XML")) {
	// System.out.println("This is plain/html text");
	// System.out.println("---------------------------");
	// } else if (part.isMimeType("multipart/*")) {
	// System.out.println("This is a Multipart");
	// System.out.println("---------------------------");
	// Multipart mp = (Multipart)part.getContent();
	// int count = mp.getCount();
	// for (int i = 0; i < count; i++) printMultipart(mp.getBodyPart(i));
	// } else if (part.isMimeType("message/rfc822")) {
	// System.out.println("This is a Nested Message");
	// System.out.println("---------------------------");
	// printMultipart((Part)part.getContent());
	// } else {
	// /*
	// * If we actually want to see the data, and it's not a
	// * MIME type we know, fetch it and check its Java type.
	// */
	// Object o = part.getContent();
	// if (o instanceof String) {
	// System.out.println("This is a string");
	// System.out.println("---------------------------");
	// System.out.println((String)o);
	// } else if (o instanceof InputStream) {
	// System.out.println("This is just an input stream");
	// System.out.println("---------------------------");
	// InputStream is = (InputStream)o;
	// int c;
	// while ((c = is.read()) != -1)
	// System.out.write(c);
	// } else {
	// System.out.println("This is an unknown type");
	// System.out.println("---------------------------");
	// System.out.println(o.toString());
	// }
	// }
	// // just a separator
	// System.out.println("---------------------------");
	//
	//
	// }

	// public static void saveFile(Part part) throws MessagingException {
	// // many mailers don't include a Content-Disposition
	// String disp = part.getDisposition();
	// if (disp != null && disp.equalsIgnoreCase(Part.ATTACHMENT)) {
	// int attnum = 1;
	// String filename = part.getFileName();
	// String extensao = "";
	// if (filename != null) {
	// System.out.println("FILENAME: " + filename);
	// extensao = filename.substring(filename.lastIndexOf(".")+1,
	// filename.length());
	// filename = filename.substring(0, filename.lastIndexOf("."));
	// } else filename = "Attachment";
	// try {
	// File f = new File(filename+"."+extensao);
	// // Do not overwrite existing files. Try different filenames.
	// while (f.exists()) {
	// f = new File(filename + (++attnum) + "." + extensao);
	// }
	// // Save the file
	// ((MimeBodyPart)part).saveFile(f);
	// System.out.println("Saving attachment to file " + filename + "" +
	// attnum++);
	// } catch (IOException ex) {
	// System.out.println("Failed to save attachment: " + ex);
	// }
	// System.out.println("---------------------------");
	// }
	// }

	public String readTextFile(Part part) {
		StringBuilder r = new StringBuilder();
		try {
			InputStream is = part.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String thisLine = reader.readLine();
			while (thisLine != null) {
				r.append(thisLine);
				thisLine = reader.readLine();
			}
		} catch (Exception e) {
			LogUtil.exception(e, null);
			// LogUtil.exception(e, user);
		}
		return r.toString();
	}

}