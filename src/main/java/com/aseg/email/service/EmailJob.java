package com.aseg.email.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.StringUtils;

import com.aseg.config.Constants;
import com.aseg.exceptions.BusinessException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;

public class EmailJob extends ServiceImpl
// implements StatefulJob
{

	public static final String SESSION = "SESSION";
	public static final String DE = "DE";
	public static final String EMAIL_ASSUNTO = "EMAIL_ASSUNTO";
	public static final String EMAIL_CORPO = "EMAIL_CORPO";
	public static final String EMAIL_PARA = "EMAIL_PARA";
	public static final String FILE_ATTACHMENT = "FILE_ATTACHMENT";
	public static final String EMAIL_TEMPLATE = "EMAIL_TEMPLATE";
	private static final String FILE_NAME_ATTACHMENT = "FILE_NAME_ATTACHMENT";
	private static final String LIST_FILE_ATTACHMENT = "LIST_FILE_ATTACHMENT";

	/*
	 * @Override public void execute(final JobExecutionContext ctx) throws
	 * JobExecutionException {
	 * 
	 * final GenericVO vo = (GenericVO) ctx.getJobDetail().getJobDataMap()
	 * .get("VO"); vo.setProcess(false); final Usuario user = (Usuario)
	 * ctx.getJobDetail().getJobDataMap() .get("user"); try { send(vo, user); }
	 * catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); }
	 * 
	 * }
	 */
	public List getMessages(final GenericVO vo, final Usuario user, Session ses) throws Exception {
		final ServiceImpl service = (ServiceImpl) vo.get(Constants.SERVICE_KEY);
		List msgs = new ArrayList();
		if (vo.get(EMAIL_PARA) != null)
			vo.put(EMAIL_PARA, user.getUtil().replace(vo.get(EMAIL_PARA).toString(), ";", ","));
		String assunto = null;
		String corpo = null;
		MimeMessage mensagem = null;
		final HashMap evento = new HashMap();

		if (vo.containsKey(EMAIL_ASSUNTO) && vo.containsKey(EMAIL_CORPO)) {

			LogUtil.info("VO contem EMAIL_ASSUNTO e EMAIL_CORPO, ignorando templates...", user);
			try {
				if (vo.get(EMAIL_ASSUNTO) != null) {
					assunto = (String) vo.get(EMAIL_ASSUNTO);
				} else {
					assunto = "Sem Assunto";
				}
				if (vo.get(EMAIL_CORPO) != null) {
					corpo = (String) vo.get(EMAIL_CORPO);
				} else {
					corpo = "Sem Corpo";
				}

			} catch (Exception e) {
				throw new BusinessException(
						"Nao foi possivel retornar os valores da VO para preencher ASSUNTO e CORPO do email.");
			}

		} else {

			try {

				String t = "";
				if (vo.get(EMAIL_TEMPLATE) != null) {
					t = (String) vo.get(EMAIL_TEMPLATE);
				} else {
					String c = convertMethods(vo.getMethod());
					if (c == null || c.trim().length() == 0)
						return msgs;
					t = "email." + service.getName() + "." + c;
				}

				List tem = user.matchTemplatesEmail(t);
				if (tem.size() == 0) {
					return msgs;
				} else {
					corpo = (String) ((Map) tem.get(0)).get("HELP_CONFIG_SISTEMA");
					assunto = (String) ((Map) tem.get(0)).get("DESCRICAO_CONFIG_SISTEMA");

				}
			} catch (final Exception e1) {
				LogUtil.exception(e1, user);
				throw new BusinessException("* * * * * * EmailService-enviarEmail: Erro ao compor Email * * * * * * ",
						user);
			}

		}

		try {

			mensagem = new MimeMessage(ses);
			// Formato da mensagem: Texto
			mensagem.setContent("EssenWEB", "text/plain; charset=" + user.getEncode());
			// Corpo da mensagem

			// Cabealho da mensagem
			// mensagem.addHeaderLine(cabecalho);
			Address addressFROM = null;
			String mailFromName = user.getSystemProperty("MAIL_FROM_NAME");
			try {
				addressFROM = new InternetAddress((String) user.getSystemProperty("MAIL_FROM")); // Endereo
																									// de
				((InternetAddress) addressFROM).setPersonal(mailFromName);
			} catch (Exception e) {
				// TODO: handle exception
				LogUtil.debug("VARIAVEL DE EMAIL NAO CADASTRADA 'MAIL_FROM'", user);
				return msgs;
			}
			mensagem.setFrom(addressFROM);
			Iterator itr = null;
			String msg = null;
			if (service != null && vo.get(EMAIL_PARA) == null) {
				try {
					boolean is = user.isSuperUser();
					user.setSuperUser(true);
					try {
						itr = getUsuarios(service, vo, user).iterator();
					} catch (Exception e) {
						// TODO: handle exception
					} finally {
						user.setSuperUser(is);
					}
					boolean un = user.SystemPropertyIsTrue("MAIL_UNIQUE_MAIL");
					while (itr.hasNext()) {
						final Map usuarios = (Map) itr.next();
						corpo = (String) usuarios.get("CORPO");

						msg = replaceVarsInMail(corpo, (StandardEvaluationContext) usuarios.get(Constants.CONTEXT_KEY),
								user.getUtil());
						if (usuarios.get("MAIL_RULE") != null && !un) {
							msg = msg + "\n " + user.getLabel("D_USUARIO_NOTIFICACAO.NOME") + ":"
									+ usuarios.get("MAIL_RULE");
						}
						mensagem.setText(msg, user.getEncode());
						// Assunto da mensagem
						assunto = (String) usuarios.get("ASSUNTO");
						mensagem.setSubject(replaceVarsInMail(assunto,
								(StandardEvaluationContext) usuarios.get(Constants.CONTEXT_KEY), user.getUtil()),
								user.getEncode());
						InternetAddress[] addresses = null;
						try {
							addresses = new InternetAddress()
									.parse((String) usuarios.get("EMAIL").toString().replaceAll(";", ","));
							for (Address ad : addresses) {
								((InternetAddress) ad).setPersonal(mailFromName);
							}
							mensagem.setRecipients(Message.RecipientType.TO, addresses);
						} catch (Exception e) {
							LogUtil.debug(" ERRO PARSEANDO EMAIL " + (String) usuarios.get("EMAIL") + " DO USUARIO "
									+ usuarios, user);
							continue;
							// TODO: handle exception
						}

						LogUtil.debug(" ENVIANDO MENSAGEM PARA " + (String) usuarios.get("EMAIL"), user);
						try {
							// Transport.send(mensagem);
							msgs.add(mensagem);
						} catch (Exception e) {
							LogUtil.exception(e, user);
							continue;
							// TODO: handle exception
						}
					}
				} catch (Exception e5) {
					LogUtil.exception(e5, user);
					throw new BusinessException(e5.getMessage());

				}
			} else {

				if (vo.containsKey(LIST_FILE_ATTACHMENT) && vo.get(LIST_FILE_ATTACHMENT) != null) {
					List a = (List) vo.get(LIST_FILE_ATTACHMENT);
					MimeBodyPart mbp1 = new MimeBodyPart();
					mbp1.setText(corpo);
					// create the Multipart and add its parts to it
					Multipart mp = new MimeMultipart();
					mp.addBodyPart(mbp1);

					for (Iterator iterator = a.iterator(); iterator.hasNext();) {
						Map object = (Map) iterator.next();

						// create the second message part
						MimeBodyPart mbp2 = new MimeBodyPart();
						// attach the file to the message
						FileDataSource fds = null;
						try {
							fds = new FileDataSource((File) object.get(FILE_ATTACHMENT));

						} catch (Exception e) {
							// TODO: handle exception
							fds = new FileDataSource(user.getUtil().writeToTmp((byte[]) object.get(FILE_ATTACHMENT),
									(String) object.get(FILE_NAME_ATTACHMENT)));

						}
						if (fds.getFile() != null) {
							DataHandler d = new DataHandler(fds);

							mbp2.setDataHandler(d);
							mbp2.setFileName((String) object.get(FILE_NAME_ATTACHMENT));
							mp.addBodyPart(mbp2);
						}
						// mbp2.setHeader("Content-Type","application/octet-stream");

					}
					mensagem.setSubject(assunto, user.getEncode());

					// add the Multipart to the message
					mensagem.setContent(mp);
					InternetAddress[] addresses = parseInetAdress(vo, user);
					mensagem.setRecipients(Message.RecipientType.TO, addresses);
					// set the Date: header
					mensagem.setSentDate(new Date());
				}

				else if (vo.containsKey(FILE_ATTACHMENT) && vo.get(FILE_ATTACHMENT) != null) {
					MimeBodyPart mbp1 = new MimeBodyPart();
					mbp1.setText(corpo);

					// create the second message part
					MimeBodyPart mbp2 = new MimeBodyPart();
					// attach the file to the message
					FileDataSource fds = null;
					try {
						fds = new FileDataSource((File) vo.get(FILE_ATTACHMENT));

					} catch (Exception e) {
						// TODO: handle exception
						fds = new FileDataSource(user.getUtil().writeToTmp((byte[]) vo.get(FILE_ATTACHMENT),
								(String) vo.get(FILE_NAME_ATTACHMENT)));

					}

					mbp2.setDataHandler(new DataHandler(fds));
					mbp2.setFileName(fds.getName());

					// create the Multipart and add its parts to it
					Multipart mp = new MimeMultipart();
					mp.addBodyPart(mbp1);
					mp.addBodyPart(mbp2);

					mensagem.setSubject(assunto, user.getEncode());

					// add the Multipart to the message
					mensagem.setContent(mp);
					InternetAddress[] addresses = parseInetAdress(vo, user);
					mensagem.setRecipients(Message.RecipientType.TO, addresses);
					// set the Date: header
					mensagem.setSentDate(new Date());
				}

				else {
					mensagem.setText(user.getUtil().EvaluateInString(corpo, user.getUtil().createContext(vo, user)),
							user.getEncode());
					// Assunto da mensagem
					mensagem.setSubject(
							user.getUtil().EvaluateInString(assunto, user.getUtil().createContext(vo, user)),
							user.getEncode());

					InternetAddress[] addresses = parseInetAdress(vo, user);

					mensagem.setRecipients(Message.RecipientType.TO, addresses);

				}
				LogUtil.debug(" ENVIANDO MENSAGEM PARA " + (String) vo.get(EMAIL_PARA), user);

				// Transport.send(mensagem);
				msgs.add(mensagem);
			}

		} catch (final javax.mail.SendFailedException se) {
			try {
				LogUtil.debug(mensagem.getSubject(), user);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				LogUtil.exception(e, user);
			}
			try {
				LogUtil.debug(mensagem.getContent().toString(), user);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				LogUtil.exception(e, user);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				LogUtil.exception(e, user);
			}
			LogUtil.exception(se, user);
			throw new BusinessException(se.getMessage() == null ? se.toString() : se.getMessage());

		} catch (final Exception e) {
			LogUtil.exception(e, user);
			throw new BusinessException(e.getMessage() == null ? e.toString() : e.getMessage());

		}
		return msgs;
	}

	private InternetAddress[] parseInetAdress(final GenericVO vo, Usuario user) throws AddressException {
		try {
			InternetAddress[] addresses = new InternetAddress().parse((String) vo.get(EMAIL_PARA));
			for (Address ad : addresses) {
				((InternetAddress) ad).setPersonal(user.getSystemProperty("MAIL_FROM_NAME"));
			}
			return addresses;
		} catch (Exception e) {
			// TODO: handle exception

			LogUtil.debug(" ERRO  PARSEANDO ENDERECO DE EMAIL >> " + (String) vo.get(EMAIL_PARA), user);

			return null;
		}
	}

	private String replaceVarsInMail(String Action, StandardEvaluationContext ctx, UtilServiceImpl util) {
		try {
			if (Action == null) {
				return null;
			}
			Usuario user = null;
			while (Action.indexOf("${") > -1) {
				final String expr = Action
						.subSequence(Action.indexOf("${") + 2, Action.indexOf("}", Action.indexOf("${"))).toString();

				user = (Usuario) util.Evaluate("${user}", ctx);
				Map services = (Map) util.Evaluate("${SERVICE_VO_KEYS}", ctx);

				Iterator i = services.keySet().iterator();
				Object v = null;
				while (i.hasNext()) {
					String object = (String) i.next();

					Map var = new HashMap((Map) services.get(object));
					ctx.setVariable(object + Constants.VOKey, var);

					if (v != null && v.toString().trim().length() > 0) {
						Action = util.replace(Action, "${" + expr + "}", v.toString());
						continue;
					}

					GenericService s = (GenericService) getService(object, user);
					Map cmp = util.getCmp(expr, s, user);
					if (expr.equals("ID_USUARIO_CADASTRO")) {
						cmp = new HashMap();
						cmp.put("ID_USUARIO_CADASTRO", user.getIdUsuario());
						cmp.put(Constants.name, "ID_USUARIO_CADASTRO");
						cmp.put("xtype", "property");
						cmp.put("label", "ID_USUARIO_CADASTRO");
						cmp.put(Constants.component, "select");
						cmp.put("key", "ID_USUARIO");
						cmp.put("labelValue", "NOME");
						cmp.put("service", "UsuarioService");
						cmp.put("serviceParams", "{SEGMENTBY:'false'}");
						cmp.put("type", "num");
					}
					if (cmp != null && cmp.get("service") != null) {
						cmp.putAll(s.createVO(user).putAllAndGet(var, false));
						// **EVSER
						v = "**EVSER"; // util.evalService(cmp, expr, user);
					}
				}

				if (v != null && v.toString().trim().length() > 0) {
					Action = util.replace(Action, "${" + expr + "}", v.toString());
				} else {
					Action = util.replace(Action, "${" + expr + "}", "#{" + expr + "}");
				}
			}
			Action = util.replace(Action, "#{", "${");
			// TODO Auto-generated method stub
			return util.EvaluateInString(Action, ctx);
		} catch (Exception e) {
			throw new BusinessException("Erro no parser do email ou do template de email para esta operao");
		}
	}

	private List getUsuarios(ServiceImpl service, GenericVO vo, final Usuario user) throws Exception {
		final List users = new ArrayList();

		final ServiceImpl sm = (ServiceImpl) getService("NotificacaoService", user);
		String singleMail = null;

		if (user.SystemPropertyIsTrue("MAIL_UNIQUE_MAIL"))
			singleMail = "";

		if (sm == null)
			return users;
		final GenericVO v = sm.createVO(user);
		v.setSegmentBy(false);

		if (false) {

			v.setPaginate(false);
			// /v.setFiltro(filtro);
			return sm.lista(v, user);

		} else {
			v.setProcess(false);
			final List tem = user
					.matchTemplatesEmail("email." + service.getName() + "." + convertMethods(vo.getMethod()));
			if (tem == null) {
				return users;

			}

			String filtro = "{gA:[{c:{f:'ID_EVENTOS',o:'in',vc:'" + StringUtils.arrayToDelimitedString(
					user.getUtil().extractArrayFromList(tem, "ID_CONFIG_SISTEMA", true), ",") + "'}}]}";
			v.setFiltro(filtro);
			v.setPaginate(false);
			v.setSegmentBy(false);
			List usr = sm.lista(v, user);

			boolean m = true;
			GenericService s = null;
			GenericVO mvo = null;

			final ServiceImpl us = (ServiceImpl) getService("UsuarioService", user);

			Map ctxAll = new HashMap();
			for (Iterator iterator = usr.iterator(); iterator.hasNext();) {
				GenericVO vu = (GenericVO) iterator.next();
				vu.setProcess(false);
				Map object = sm.obtem(vu, user);
				String[] ids = (String[]) object.get("ID_EVENTOS");
				if (m) {
					s = service;
					mvo = vo;
					Object vx[] = matchObject(user, object, s, mvo, ctxAll);
					// mvo = (GenericVO) vx[0];
					// s = (GenericService) vx[1];
					m = false;
				}
				GenericVO uvo = us.createVO(user).putKey("ID_USUARIO", object.get("ID_USUARIO"));
				// uvo.setSegmentBy(false);
				GenericVO data = us.obtem(uvo, user);
				for (int i = 0; i < ids.length; i++) {
					ServiceImpl sd = (ServiceImpl) user.getFactory().getSpringBean("TemplateEmailService",
							user);
					GenericVO vd = sd.createVO(user).putKey("ID_CONFIG_SISTEMA", new Long(ids[i]));
					vd = sd.obtem(vd, user);
					if (vd.get("NOME_CONFIG_SISTEMA").toString()
							.startsWith("email." + service.getName() + "." + convertMethods(vo.getMethod()))) {
						data.put("CORPO", vd.get("HELP_CONFIG_SISTEMA"));
						data.put("ASSUNTO", vd.get("DESCRICAO_CONFIG_SISTEMA"));

					}

				}

				Usuario StubUser = new Usuario(user);
				bindUser(StubUser, data, false);

				if (object.get("VALOR_FILTRO") != null) {
					mvo.setFiltro((String) object.get("VALOR_FILTRO"));
					Map mc = null;
					try {
						mvo.put("FOR_TEMPLATE", true);
						mvo.setProcess(false);
						mc = s.obtem(mvo, StubUser);
						if (mc.get(s.getPrimaryKey().get(Constants.name)) == null)
							continue;
						ctxAll.putAll(mc);
					} catch (Exception e) {
						continue;
					}
					if (mc == null) {
						continue;
					}
					if (mc.get(s.getPrimaryKey().get(Constants.name)) != null) {
						data.put(Constants.CONTEXT_KEY, StubUser.getUtil().createContext(ctxAll, StubUser));
						data.put("MAIL_RULE", object.get("NOME"));
						users.add(data);
					}

				}

				if (singleMail != null) {
					String email = StubUser.getEmail() == null ? "erro@" + StubUser.getIdUsuario()
							: StubUser.getEmail();
					singleMail += " \"" + StubUser.getNome() + "\" <" + email + ">;";
					data.put("EMAIL", singleMail);
				}

			}
			// users.add(sm.obtem(v, user));
			if (singleMail != null && users.size() > 1)
				return users.subList(users.size() - 1, users.size());

			return users;
		}
	}

	private Object[] matchObject(final Usuario user, Map object, GenericService s, GenericVO mvo, Map ctx)
			throws Exception {
		boolean isMaster = s.getProperties().get("type").equals("M");

		Map voCtx = new HashMap();
		boolean isP = mvo.isProcess();
		mvo.setProcess(false);
		if (!isMaster) {
			voCtx.put(s.getName(), mvo);
		}
		while (!isMaster) {
			if (!s.getParent(user).getName().equalsIgnoreCase("SystemService")) {
				s = s.getParent(user);
			} else {
				isMaster = true;
				continue;
			}

			if (s == null) {
				isMaster = true;
				continue;
			} else {
				Object k = mvo.get(s.getPrimaryKey().get(Constants.name));
				mvo = s.createVO(user);
				mvo.putKey(s.getPrimaryKey().get(Constants.name), k);

				mvo = s.obtem(mvo, user);
				voCtx.put(s.getName(), mvo);
				isMaster = s.getProperties().get("id").equals(object.get("CONTEXTO"));
				continue;
			}

		}
		if (ctx != null) {

			ctx.putAll(s.obtem(mvo, user));
		}
		ctx.put("SERVICE_VO_KEYS", voCtx);
		mvo.setProcess(isP);
		return new Object[] { mvo, s, ctx };
	}

	private String convertMethods(final String method) {

		if (method.equals("obtemLista")) {
			return "lista";
		}
		return method;
	}

}