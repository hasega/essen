package com.aseg.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleScriptContext;

import org.w3c.dom.NamedNodeMap;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;

public class DataBaseInitializator extends ServiceImpl {

	private Usuario superUser;
	private UtilServiceImpl util;
	private SeleniumTestConfig testConfig;
	private ConfigSisAux configSisAux;
	private SeleniumReport r;
	private ScriptEngine scriptEngine = new ScriptEngineManager().getEngineByName("js");
	private static DataBaseInitializator instance;

	public static String[][] insertFieldsFilter = {
			{ "type", "primaryKey", "relationKey", "xtype", "xtype", "component", "type", "type", "component",
					"component" },
			{ "hidden", "true", "true", "container", "method", "fake", "scriptRule", "sqlrule", "grid", "message" } };

	public static DataBaseInitializator getInstance(Usuario user, SeleniumReport r) {
		if (instance == null) {
			instance = new DataBaseInitializator(user, r);
		}
		return instance;
	}

	public DataBaseInitializator(Usuario user, SeleniumReport rep) {
		this.superUser = user;
		util = user.getUtil();
		testConfig = SeleniumTestConfig.getInstance(user, true);
		configSisAux = ConfigSisAux.getInstance(user);
		r = rep;
	}

	public void initMasters() {
		// Realiza insert em todos os contextos masters, e suas dependncias.
		String mastersFilter = "";
		if (testConfig.getNodeListFromTestConfig("/configTest").item(0).getAttributes()
				.getNamedItem("mastersFilter") != null) {
			mastersFilter = testConfig.getNodeListFromTestConfig("/configTest").item(0).getAttributes()
					.getNamedItem("mastersFilter").getNodeValue();
		} else {
			mastersFilter = "@type='M'";
		}
		List<Map<String, String>> beansMasterToInit = Collections
				.unmodifiableList((List) superUser.getSysParser().getListValuesFromXQL(
						"/systems/system[@name='" + superUser.getSystem() + "']/module/bean[" + mastersFilter + "]",
						"order", superUser, true, false));
		for (Map<String, String> m : beansMasterToInit) {
			if (testConfig.isToTestThisBean(null, m) && configSisAux.canAcess(m)
					&& configSisAux.beanIsValidToDefaultCrudTest(m)) {
				initMaster(m);
			}
		}
		r.appendItemList("</br>", null, "");
	}

	public Object initMaster(Map<String, String> master) {
		// Cadastra um master, e os seus relacionamentos.

		idsInsert = new HashMap<String, Object>();
		relationalTables = new HashMap<String, Object>();

		Object result = new Object();
		r.appendItemList("Preparando o bean: " + master.get("name") + " para o incio do teste.", null, "");
		List<Map<String, String>> props = util.filter(configSisAux.getItemsOfBean(master), insertFieldsFilter[0],
				insertFieldsFilter[1]);
		GenericService mServ = (GenericService) getServiceById(master.get("id"), superUser);
		GenericVO mVO = mServ.createVO(superUser);

		mVO = getVoWithInsertValues(props, master, mServ);

		for (Entry<String, Object> rl : relationalTables.entrySet()) {

			Map<String, String> propRel = (Map<String, String>) rl.getValue();
			Map<String, String> ba = configSisAux.getBeanById(propRel.get("idBean"));
			Map<String, String> bb = configSisAux.getBeanByName(propRel.get("service").replaceAll("Service", ""));

			ServiceImpl serv = (ServiceImpl) getService(ba.get("name") + "Service", superUser);
			GenericVO vo = serv.createVO(superUser);

			if (idsInsert.containsKey(ba.get("table"))) {
				vo.putKey(configSisAux.getPrimaryKeyOfBean(ba).get("name"), idsInsert.get(ba.get("table")));
				try {
					vo = serv.obtem(vo, superUser);
					if (idsInsert.containsKey(bb.get("table"))) {
						vo.put(propRel.get("name"), new String[] { String.valueOf(idsInsert.get(bb.get("table"))) });
					} else {
						vo.put(propRel.get("name"), new String[] { String.valueOf(insertAux(bb)) });
					}
					// e.put(getMainFieldBean(ba).get("name"),
					// obt.get(getMainFieldBean(ba).get("name")));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			vo.setMethod("altera");
			try {
				serv.altera(vo, superUser);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		try {
			result = mServ.inclui(mVO, superUser);
		} catch (Exception e) {
			e.printStackTrace();
			r.appendItemList("Erro na insero de preparao de testes do bean: " + master.get("name") + " Exception :"
					+ e.toString(), SeleniumReport.FontColor.RED, "");
		}
		r.appendItemList("Finalizada a preparao o bean: " + master.get("name") + " para o incio do teste.", null, "");
		return result;
	}

	public Object trySelectElseInsert(String serviceName) {
		// Este mtodo tenta selecionar e retorna a ID de um registro do
		// serviceName, caso no consiga, insere e retorna a ID.
		GenericService sm = (GenericService) getService(serviceName, superUser);
		GenericVO vo = sm.createVO(superUser);
		List<GenericVO> listRes = new ArrayList<GenericVO>();
		try {
			listRes = sm.lista(vo, superUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (listRes.isEmpty()) {
			return insertAux(configSisAux.getBeanByName(serviceName.replaceAll("Service", "")));
		} else {
			return listRes.get(0).get(sm.getPrimaryKey().get("name"));
		}
	}

	private Map<String, Object> idsInsert = new HashMap<String, Object>();
	private Map<String, Object> relationalTables = new HashMap<String, Object>();

	public Object insertAux(Map<String, String> bean) {
		// Mtodo auxiliar para recurso de inseres.

		if (idsInsert.containsKey(bean.get("table")) && bean.get("table") != null) {
			return idsInsert.get(bean.get("table"));
		}

		Object res = null;
		List<Map<String, String>> props = null;
		if (configSisAux.canAcess(bean)) {
			GenericService aServ = (GenericService) getServiceById(bean.get("id"), superUser);
			;
			GenericVO aVO = aServ.createVO(superUser);
			aVO.setLazyList(false);
			props = util.filter(configSisAux.getItemsOfBean(bean), insertFieldsFilter[0], insertFieldsFilter[1]);

			aVO = getVoWithInsertValues(props, bean, aServ);

			try {
				res = Integer.parseInt(aServ.inclui(aVO, superUser));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (Exception e) {
				try {
					res = new Long(((Map<String, Long>) aServ.lista(aVO, superUser).get(0))
							.get(aServ.getPrimaryKey().get("name")));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}

		if (res != null && bean.get("table") != null) {
			idsInsert.put(bean.get("table"), res);
		}

		return res;
	}

	public GenericVO getVoWithInsertValues(List<Map<String, String>> props, Map<String, String> context,
			GenericService bServ) {
		// Este mtodo gera uma VO com valores para cadastro, padro, ou definidos
		// no config-test.
		GenericVO aVO = bServ.createVO(superUser);
		for (Map<String, String> p : props) {

			if (p.get("relational_table") != null && !p.get("relational_table").isEmpty()) {
				if (idsInsert.containsKey(
						configSisAux.getBeanByName(p.get("service").replace("Service", "")).get("table"))) {
					aVO.put(p.get("name"), idsInsert
							.get(configSisAux.getBeanByName(p.get("service").replace("Service", "")).get("table")));
				} else {
					p.put("idBean", context.get("id"));
					relationalTables.put(p.get("relational_table"), p);
				}
			}

			String value = testConfig.getValueToField(context, p, ConfigSisAux.ServiceMethod.PREPARATION, -1);
			if ((p.get("component") != null && !p.get("component").equals(ConfigSisAux.PropertyComponents.HIDDEN.value))
					&& !(p.get("component") != null
							&& p.get("component").equals(ConfigSisAux.PropertyComponents.SELECT.value)
							&& p.get("service").equals(bServ.getName())
							// && (p.get("required") == null ||
							// (p.get("required") != null &&
							// p.get("required").equals("false")))
							&& p.get("serviceMethod") == null)
					&& (p.get("calculated") == null
							|| (p.get("caculated") != null && p.get("calculated").equals("false")))) {

				if (p.get("service") != null
						&& testConfig.getPreparationScript(context.get("id"), p.get("name"), false) != null) {
					// execuo, de scripts de preparao, para inserir registros
					// com devidos relacionamentos, e recuperar a sua ID, para
					// inserir na tabela corrente.
					try {
						String filter = ((NamedNodeMap) testConfig.getPreparationScript(context.get("id"),
								p.get("name"), false)).getNamedItem("filter").getNodeValue();
						if (filter != null && !filter.isEmpty()) {
							String script = "";
							if (testConfig.getPreparationScript(context.get("id"), p.get("name"), true) != null) {
								script = testConfig.getPreparationScript(context.get("id"), p.get("name"), true)
										.toString();
							}
							// Verificao para saber se o script j foi rodado,
							// baseando-se no filtro.
							if (getIdByFilter(filter, p) == null && !script.isEmpty()) {
								getResultOfTestFunction(context, script);
							}
							aVO.put(p.get("name"), getIdByFilter(filter, p));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else if (p.get("service") != null
						&& ((p.get("component").equals(ConfigSisAux.PropertyComponents.SELECT.value))
								|| (p.get("component").equals(ConfigSisAux.PropertyComponents.RADIO_GROUP.value)
										&& !p.get("service").equals(bServ.getName())))) {

					if (p.get("service").equals(bServ.getName()) && p.get("serviceMethod") != null || (configSisAux
							.getBeanByName(p.get("service").replaceAll("Service", "")).get("type").equals("F"))) {
						aVO.put(p.get("name"),
								configSisAux.getValuesOfMethodList(context, p).entrySet().iterator().next().getKey());

					} else {
						if (value == null || value.isEmpty()) {
							aVO.put(p.get("name"),
									insertAux(configSisAux.getBeanByName(p.get("service").replaceAll("Service", ""))));
						} else {
							aVO.put(p.get("name"), getIdByFilter(value, p));
						}
					}
				} else {
					if (p.get("type") != null && configSisAux.canAcess(p)) {
						if (p.get("component") != null
								&& p.get("component").equals(ConfigSisAux.PropertyComponents.CONTEXT.value)) {
							if (p.get("nature") != null && p.get("nature").equals("dependent")) {
								Map<String, String> cBean = configSisAux
										.getBeanByName(p.get("service").replaceAll("Service", ""));
								GenericService cServ = (GenericService) getServiceById(cBean.get("id"), superUser);
								GenericVO cVO = cServ.createVO(superUser);
								List<Map<String, String>> cProps = util.filter(configSisAux.getItemsOfBean(cBean),
										insertFieldsFilter[0], insertFieldsFilter[1]);
								cVO = getVoWithInsertValues(cProps, cBean, cServ);

								for (Entry<String, Object> vi : cVO.entrySet()) {
									aVO.put(p.get("name") + "_" + vi.getKey(), vi.getValue());
								}
							}
						} else {
							aVO.put(p.get("name"), getValueToField(context, p));
						}
					}
				}
			}
		}
		return aVO;
	}

	public String getValueToField(Map<String, String> bean, Map<String, String> p) {
		// Retorna valor sugerido para uma property, que no seja CONTEXT
		GenericService bServ = (GenericService) getService(bean.get("name") + "Service", superUser);
		String definedValue = testConfig.getValueToField(bean, p, ConfigSisAux.ServiceMethod.PREPARATION, -1);

		if (definedValue != null && !definedValue.isEmpty()) {
			if (p.get("type").equals(ConfigSisAux.PropertyTypes.ALFA.value) && p.get("service") == null) {
				if (p.get("maxlength") != null && definedValue.length() > Integer.parseInt(p.get("maxlength"))) {
					return definedValue.substring(0, Integer.parseInt(p.get("maxlength")));
				}
			} else {
				return definedValue;
			}
		}

		String value = "";
		if (p.get("type").equals(ConfigSisAux.PropertyTypes.ALFA.value) && p.get("service") == null) {
			value = ConfigSisAux.ValuesToFields.TEXT.values[0];
		} else if (p.get("type").equals(ConfigSisAux.PropertyTypes.BIT.value)) {
			value = ConfigSisAux.ValuesToFields.BIT.values[0];
		} else if (p.get("type").equals(ConfigSisAux.PropertyTypes.DATE.value)) {
			value = ConfigSisAux.ValuesToFields.DATA.values[0];
		} else if (p.get("type").equals(ConfigSisAux.PropertyTypes.FILE.value)) {
			value = ConfigSisAux.ValuesToFields.FILE.values[0];
		} else if (p.get("type").equals(ConfigSisAux.PropertyTypes.MONEY.value)) {
			value = ConfigSisAux.ValuesToFields.MONEY.values[0];
		} else if (p.get("type").equals(ConfigSisAux.PropertyTypes.NUM.value)
				&& (p.get("component") != null
						&& !p.get("component").equals(ConfigSisAux.PropertyComponents.CONTEXT.value))
				&& !(p.get("relationKey") != null && p.get("relationKey").equals("true"))) {

			value = ConfigSisAux.ValuesToFields.NUM.values[0];
		} else if (p.get("type").equals(ConfigSisAux.PropertyTypes.TIME.value)) {
			value = ConfigSisAux.ValuesToFields.TIME.values[0];
		} else if ((p.get("service") != null && p.get("service").equals(bServ.getName())
				&& p.get("component").equals(ConfigSisAux.PropertyComponents.RADIO_GROUP.value))
				|| (p.get("service") != null && p.get("service").equals(bServ.getName())
						&& p.get("component").equals(ConfigSisAux.PropertyComponents.SELECT.value))) {
			value = configSisAux.getValuesOfMethodList(bean, p).entrySet().iterator().next().getKey();
		}
		// coloca o name da property antes do valor quando for String
		if (!p.get("type").equals(ConfigSisAux.PropertyTypes.NUM.value)
				&& !p.get("type").equals(ConfigSisAux.PropertyTypes.BIT.value)
				&& !p.get("type").equals(ConfigSisAux.PropertyTypes.MONEY.value)
				&& !p.get("type").equals(ConfigSisAux.PropertyTypes.DATE.value) && !(p.get("maxlength") != null
						&& (p.get("name").toLowerCase() + value).length() > Integer.parseInt(p.get("maxlength")))) {

			value = p.get("name").toLowerCase() + value;
		}
		if (p.get("maxlength") != null && value.length() > Integer.parseInt(p.get("maxlength"))) {
			value = value.substring(0, Integer.parseInt(p.get("maxlength")));
		}
		return value;
	}

	public Object getIdByFilter(String filter, Map<String, String> property) {
		GenericService gServ = (GenericService) getService(property.get("service"), superUser);
		GenericVO gVo = gServ.createVO(superUser);
		gVo.setFiltro(filter);
		Object res = null;
		try {
			res = gServ.obtem(gVo, superUser).get(gServ.getPrimaryKey().get("name"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public String getResultOfTestFunction(Map<String, String> bean, String func) {
		try {
			ScriptContext ctx = new SimpleScriptContext();

			final Bindings bindings = ctx.getBindings(ScriptContext.ENGINE_SCOPE);

			bindings.put("sm", ((GenericService) getServiceById(bean.get("id"), superUser)));
			bindings.put("VO", ((GenericService) getServiceById(bean.get("id"), superUser)).createVO(superUser));
			bindings.put("user", superUser);
			bindings.put("util", util);
			bindings.put("df", superUser.getUtil().df);
			bindings.put("sysNavi", this);
			bindings.put("bean", bean);

			Object res = null;
			try {
				res = scriptEngine.eval("importClass(Packages." + this.getClass().getCanonicalName() + ");" + func,
						ctx);
				if (res != null && res.getClass().equals(String.class)) {
					return res.toString();
				}
			} catch (ScriptException e) {
				LogUtil.exception(e, superUser);
			}
		} catch (Exception ex) {
			LogUtil.exception(ex, superUser);
		}
		return "";

	}

	public String getReport() {
		return r.toString();
	}

}
