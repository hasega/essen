package com.aseg.tests;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.service.importation.XMLImporter;
import com.aseg.vo.GenericVO;

public class DataImporter extends ServiceImpl {
	private SeleniumTestConfig testConfig;
	private Usuario user;
	private ConfigSisAux configSisAux;
	List<Map<String, String>> allBeans;

	public DataImporter(Usuario user) {
		testConfig = SeleniumTestConfig.getInstance(user, true);
		this.user = user;
		configSisAux = ConfigSisAux.getInstance(user);
		allBeans = configSisAux.getAllBeans();
	}

	public void importAllDocuments() {
		if (!testConfig.getImportDataSrc().isEmpty()) {
			File docsDir = new File(testConfig.getImportDataSrc());
			List<File> files = Arrays.asList(docsDir.listFiles());

			for (File f : files) {
				if (f.isFile() && isValidDocument(f.getName())) {
					ServiceImpl service = (ServiceImpl) getService(f.getName().replace(".xml", "") + "Service", user);
					GenericVO vo = service.createVO(user);

					try {
						new XMLImporter(service, user).process(new FileInputStream(f), vo);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}

		}
	}

	public boolean isValidDocument(String docName) {
		if (!docName.endsWith(".xml")) {
			return false;
		} else {
			for (Map<String, String> b : allBeans) {
				if (b.get("name").equals(docName.replace(".xml", ""))) {
					return true;
				}
			}
		}
		return false;
	}

}
