package com.aseg.tests;

import java.util.Map;

public class SeleniumReport {

	private StringBuilder r = new StringBuilder();

	static String cssFontRed = "color:red;";
	static String cssFontGreen = "color:green;";
	static String cssFontBlue = "color:blue;";
	static String cssFontWhite = "color:white;";

	private boolean showErrors = true;
	private boolean showWarnings = true;

	public SeleniumReport(boolean err, boolean warn) {
		showErrors = err;
		showWarnings = warn;
	}

	public void appendInitList() {
		r.append("<ul>");
	}

	public void appendFimList() {
		r.append("</ul>");
	}

	public void appendItemList(String item, FontColor c, String extraStyle) {
		String fontColor = "";
		if (c != null) {
			switch (c) {
			case BLUE:
				fontColor = cssFontBlue;
				break;
			case RED:
				fontColor = cssFontRed;
				break;
			case GREEN:
				fontColor = cssFontGreen;
				break;
			case WHITE:
				fontColor = cssFontWhite;
				break;
			}
		}

		r.append("<li style='" + fontColor + extraStyle + "'>" + item + "</li>");
	}

	public void initLogBean(Map<String, String> bean) {
		String result = "";
		if (bean.get("type").equals("M")) {
			result = "<li><h1># " + bean.get("name") + " < 'Master' > </h1></li>";
		} else if (bean.get("type").equals("D")) {
			result = "<li><h3># " + bean.get("name") + " < 'Detail' > </h3></li>";
		} else if (bean.get("type").equals("A")) {
			result = "<li><h4># " + bean.get("name") + " < 'Auxilixar' > </h4></li>";
		}
		r.append(result);
	}

	public void appendResultTest(Prefix p, Method m, Map<String, String> bean, String extraMsg) {
		String result = "";
		if (bean != null && m != null) {
			if ((p.value.equals(Prefix.INI.value) || p.value.equals(Prefix.FIM.value)) && showWarnings) {
				if (m != Method.METHOD) {
					result = "<li style='" + cssFontGreen + "'>" + p.value + " o(no) teste de " + m.value
							+ " do bean : " + bean.get("name") + "</li>";
				} else {
					result = "<li style=" + cssFontGreen + "'>" + p.value + " o(no) teste do " + m.value + " : "
							+ bean.get("name") + "</li>";
				}
			} else if (p.value.equals(Prefix.ERRO.value) && showErrors) {
				if (m != Method.METHOD) {
					result = "<li style=" + cssFontRed + "'>" + p.value + " o(no) teste de " + m.value
							+ " do bean : " + bean.get("name") + "<ul><li>" + ((extraMsg != null && !extraMsg.isEmpty()
									? extraMsg : "Causa do erro desconhecida, favor verificar manualmente."))
							+ "</li></ul>";
				} else {
					result = "<li style=" + cssFontRed + "'>" + p.value + " o(no) teste do " + m.value
							+ " : " + bean.get("name") + "<ul><li>" + ((extraMsg != null && !extraMsg.isEmpty()
									? extraMsg : "Causa do erro desconhecida, favor verificar manualmente."))
							+ "</li></ul>";
				}
			}
		} else {
			result = "<li style=" + cssFontRed + "'>" + p.value + " o(no) teste de " + m.value + " do bean : "
					+ bean.get("name") + "<ul><li>" + ((extraMsg != null && !extraMsg.isEmpty() ? extraMsg
							: "Causa do erro desconhecida, favor verificar manualmente."))
					+ "</li></ul>";
		}
		r.append(result);
	}

	public enum Prefix {
		INI("Iniciado"), FIM("Finalizado"), ERRO("ERRO");
		private Prefix(String v) {
			this.value = v;
		}

		private String value;
	}

	public enum Method {
		INSERT("Insert"), UPDATE("Update"), DELETE("Delete"), METHOD("Method");
		private Method(String v) {
			this.value = v;
		}

		private String value;
	}

	public enum FontColor {
		RED, GREEN, BLUE, WHITE;
	}

	@Override
	public String toString() {
		return r.toString();
	}
}
