//package com.aseg.tests;
//
//import java.util.Calendar;
//import java.util.Collections;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.concurrent.Callable;
//
//import javax.script.Bindings;
//import javax.script.ScriptContext;
//import javax.script.ScriptEngine;
//import javax.script.ScriptEngineManager;
//import javax.script.ScriptException;
//import javax.script.SimpleScriptContext;
//import javax.servlet.ServletRequest;
//import javax.servlet.http.HttpServletRequest;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.NoSuchElementException;
//import org.openqa.selenium.StaleElementReferenceException;
//import org.openqa.selenium.TimeoutException;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.support.ui.ExpectedCondition;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.w3c.dom.NamedNodeMap;
//
//import com.aseg.config.Constants;
//import com.aseg.logauditoria.service.LogUtil;
//import com.aseg.seguranca.Usuario;
//import com.aseg.service.GenericService;
//import com.aseg.service.ServiceImpl;
//import com.aseg.tests.ConfigSisAux.CmpToIdentify;
//import com.aseg.tests.ConfigSisAux.OperadoresFiltro;
//import com.aseg.tests.ConfigSisAux.PropertyComponents;
//import com.aseg.tests.ConfigSisAux.PropertyTypes;
//import com.aseg.tests.ConfigSisAux.ServiceMethod;
//import com.aseg.tests.ConfigSisAux.ValuesToFields;
//import com.aseg.util.service.UtilServiceImpl;
//import com.aseg.vo.GenericVO;
//import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
//
//public class SeleniumTester extends ServiceImpl implements Callable<String> {
//
//	private ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
//	private ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("js");
//	private String URL = "";
//	private String SYSTEM;
//	private Usuario user = null;
//	private UtilServiceImpl util = null;
//	private WebDriver driver = null;
//	private final int TIME_TO_WAIT_PAGE_LOAD = 100;
//	private SeleniumReport r;
//	private ServletRequest request;
//	private SeleniumTestConfig testConfig;
//	private Usuario superUser = null;
//	private String userSenha = "";
//	private ConfigSisAux configSisAux;
//	private DataBaseInitializator dataBaseInit;
//
//	public SeleniumTester(Usuario user, ServletRequest request, SeleniumTestConfig testConfig) {
//		this.request = request;
//		this.testConfig = testConfig;
//		URL = testConfig.getSystemUrl();
//		loadWebDrivers();
//		SYSTEM = (String) ((HashMap<String, String>) Collections.unmodifiableList(
//				(List) user.getSysParser().getListValuesFromXQL("/systems/system[@name]", "order", user, true, false))
//				.get(0)).get("name");
//		this.user = user;
//		this.util = user.getUtil();
//		r = new SeleniumReport(testConfig.reportIsToShow(SeleniumTestConfig.ReportItems.ERROR),
//				testConfig.reportIsToShow(SeleniumTestConfig.ReportItems.WARNING));
//
//		userSenha = user.getSenha();
//		createValidUser((HttpServletRequest) request);
//
//		superUser =Usuario.getSystemUser();
//		if (user == null) {
//			user = (Usuario) request.getAttribute(Constants.RAIN_USER);
//			util = user.getUtil();
//		}
//		this.configSisAux = ConfigSisAux.getInstance(superUser);
//		this.dataBaseInit = new DataBaseInitializator(superUser, r);
//	}
//
//	public void loadWebDrivers() {
//		System.setProperty("webdriver.chrome.driver",
//				testConfig.getDriverLocation(SeleniumTestConfig.BrowsersWebDrivers.CHROME));
//	}
//
//	public void initTest() {
//
//		driver = new ChromeDriver();
//		r.appendInitList();
//
//		// Acessa a url configurada.
//		driver.get(URL);
//
//		loginInSystem(request, driver);
//		try {
//			testAllSystem();
//		} catch (Exception e) {
//			e.printStackTrace();
//			// r.appendItemList("Ocorreu um erro grave durante o teste do
//			// sistema.: "+e.toString(), FontColor.RED, "");
//		}
//		r.appendFimList();
//	}
//
//	public void testAllSystem() {
//		String xpath = testConfig.getXpathOfSelectBeans();
//		if (!xpath.isEmpty()) {
//			xpath = "and " + xpath;
//		}
//		List<Map<String, String>> beansMaster = Collections.unmodifiableList((List) user.getSysParser()
//				.getListValuesFromXQL("/systems/system[@name='" + SYSTEM + "']/module/bean[@type='M' " + xpath + "]",
//						"order", user, true, false));
//
//		for (Map<String, String> bean : beansMaster) {
//			try {
//				testBean(bean);
//			} catch (Exception e) {
//				continue;
//			}
//		}
//		driver.close();
//	}
//
//	public void testBean(Map<String, String> bean) {
//		if (configSisAux.beanIsValidToDefaultCrudTest(bean) && testConfig.isToTestThisBean(null, bean)) {
//			try {
//				navigateAndTestCrudOfContext(bean, true);
//			} catch (Exception e) {
//				e.printStackTrace();
//				waitLoadingModalHide();
//				try {
//					navigateAndTestCrudOfContext(bean, true);
//				} catch (Exception e2) {
//					e2.printStackTrace();
//				}
//			}
//		} else if (testConfig.isToAutoNavigateInLinks(bean) && testConfig.isToTestThisBean(null, bean)) {
//			autoNavigateCategoryLinks(bean);
//		}
//	}
//
//	public void navigateAndTestCrudOfContext(Map<String, String> context, boolean testCrud) {
//		if (!executeTestScript(context, null, null, SeleniumTestConfig.ScriptTypes.NAVIGATION_SCRIPT)) {
//			waitLoadingModalHide();
//			r.initLogBean(context);
//			try {
//				menuClick(context);
//			} catch (Exception e) {
//				// e.printStackTrace();
//				waitLoadItemById("itemFakeToForceWait", 15);
//				okClick();
//				menuClick(context);
//			}
//
//			waitForCategoriaLinks(context);
//
//			executeTestScript(context, SeleniumTestConfig.ScriptNature.BEFORE_LIST, null,
//					SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//			if (!categoriaLinkClick(context, configSisAux.getTodosLinkLabel(context), true)) {
//				try {
//					waitLoadItemById("itemFaceToForceWait", 5);
//				} catch (Exception e) {
//				}
//				okClick();
//				categoriaLinkClick(context, configSisAux.getTodosLinkLabel(context), true);
//			}
//			waitForListGrid(context);
//			executeTestScript(context, SeleniumTestConfig.ScriptNature.AFTER_LIST, null,
//					SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//			if (testCrud) {
//				if (testConfig.getCrudTestQtd(context) == -1) {
//					testCrudOfContext(context, null);
//				} else {
//					for (int i = 0; i < testConfig.getCrudTestQtd(context); i++) {
//						context.put("testIndex", "" + i);
//						testCrudOfContext(context, null);
//					}
//				}
//			}
//		}
//	}
//
//	public void testCrudOfContext(Map<String, String> context, Map<String, String> master) {
//
//		if (testConfig.isToTestThisOperation(ServiceMethod.SEARCH) && context.get("type").equals("M")) {
//			testAdvancedSearch(context);
//			closeAllTabsOfContext(context);
//			navigateAndTestCrudOfContext(context, false);
//		}
//
//		boolean insertOk = testInsert(context);
//		if (insertOk || getRowsOfGrid(context).size() > 0) {
//
//			if (!insertOk) {
//				testDetail(context, 0);
//			} else {
//				if (configSisAux.thisBeanDetailAfterInsert(context)
//						&& testConfig.isToTestThisOperation(ServiceMethod.INSERT)) {
//					try {
//						waitForDetail(context);
//					} catch (Exception e) {
//						testDetail(context, 0);
//					}
//				}
//			}
//
//			if (testConfig.isToTestThisOperation(ServiceMethod.METHOD)
//					&& !configSisAux.getBeanMethods(context, true, "[@type != 'L']").isEmpty()) {
//				if (context.get("type").equals("M")) {
//					driver.findElement(By.ById.id(getIdentifier(context, null, null, CmpToIdentify.TAB_LISTA, null)))
//							.click();
//				} else {
//					tabDetailNaviClick(master, context);
//				}
//				testMethods(context);
//				if (context.get("type").equals("M")) {
//					driver.findElement(By.ById.id(getIdentifier(context, null, null, CmpToIdentify.TAB_LISTA, null)))
//							.click();
//				} else {
//					tabDetailNaviClick(master, context);
//				}
//				while (tabNavigateClose(null, context, CmpToIdentify.TAB_DETAIL_MASTER, false)) {
//					;
//				}
//			}
//			try {
//				if (testConfig.isToTestThisOperation(ServiceMethod.METHOD)) {
//					testDetail(context, 0);
//				} else {
//					if (configSisAux.thisBeanDetailAfterInsert(context)) {
//						waitForDetail(context);
//					} else {
//						testDetail(context, 0);
//					}
//				}
//			} catch (Exception e) {
//				testDetail(context, 0);
//			}
//
//			List<Map<String, String>> detailBeans = configSisAux.getDetailBeans(context);
//			testDetailBeans(context, detailBeans);
//			if (detailBeans != null && !detailBeans.isEmpty()) {
//				r.appendItemList("<br/>", null, "");
//			}
//			waitLoadingModalHide();
//			waitWindowClose();
//			try {
//				driver.findElement(By.ById.id(getIdentifier(context, null, null, CmpToIdentify.TAB_RESUMO, null)))
//						.click();
//				waitLoadItemById(getIdentifier(context, null, null, CmpToIdentify.TAB_RESUMO_CONTENT, null), 10);
//			} catch (Exception e) {
//			}
//
//			waitLoadingModalHide();
//			testUpdate(context);
//			testDelete(context);
//		}
//		closeAllTabsOfContext(context);
//		waitLoadingModalHide();
//
//	}
//
//	public void closeAllTabsOfContext(Map<String, String> context) {
//		try {
//			tabNavigateClose(null, context, CmpToIdentify.TAB_DETAIL_MASTER, false);
//			tabNavigateClose(null, context, CmpToIdentify.TAB_LISTA, false);
//			tabNavigateClose(null, context, CmpToIdentify.TAB_NAVIGATION, false);
//		} catch (NoSuchElementException nsel) {
//			try {
//				tabNavigateClose(null, context, CmpToIdentify.TAB_LISTA, false);
//				tabNavigateClose(null, context, CmpToIdentify.TAB_NAVIGATION, false);
//			} catch (Exception e) {
//				try {
//					tabNavigateClose(null, context, CmpToIdentify.TAB_NAVIGATION, false);
//				} catch (Exception e2) {
//				}
//
//			}
//		}
//	}
//
//	public void autoNavigateCategoryLinks(Map<String, String> bean) {
//		if (!executeTestScript(bean, null, null, SeleniumTestConfig.ScriptTypes.NAVIGATION_SCRIPT)) {
//			setReqAttr(request,"node", bean.get("id"));
//			r.initLogBean(bean);
//
//			List<Map<String, String>> categorias = null;
//			try {
//				categorias = Collections
//						.unmodifiableList((List) user.getSysParser().getListValuesFromXQL(
//								util.EvaluateInString(
//										((net.sf.json.JSONObject) ((JSONArray) util.JSONtoMap(bean.get("links"))
//												.get("Q")).get(0)).get("e").toString(),
//										util.createContext(request)),
//								"order", user, true, false));
//			} catch (JSONException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//
//			boolean groupMaster = configSisAux.linksOfBeanHaveGroupMaster(bean, request);
//
//			if (groupMaster) {
//				menuClick(bean);
//				Map<String, String> groups = configSisAux.getGroupsByMaster(bean);
//
//				for (String it : groups.keySet()) {
//					if (testConfig.isToTestThisBean(null, configSisAux.getBeanById(it))) {
//						List<Map<String, String>> auxs = null;
//						try {
//							categoriaLinkClick(bean, configSisAux.getBeanById(it).get("label"), false);
//							auxs = Collections.unmodifiableList((List) user.getSysParser()
//									.getListValuesFromXQL("/systems/system[@name='" + SYSTEM
//											+ "']/module/bean[@master='" + it + "' and @type='A']", "order", user, true,
//											false));
//							waitLoadingModalHide();
//
//						} catch (Exception e) {
//							continue;
//						}
//						for (Map<String, String> a : auxs) {
//							if (testConfig.isToTestThisBean(bean, a)) {
//								waitLoadingModalHide();
//								try {
//									r.initLogBean(a);
//									String xpath = "//div[@id='" + SYSTEM
//											+ "/SystemVO_*_show_tab']//div[@id='categoria-links']//dd/div/h4[contains( text(),'"
//											+ user.getLabel(user.getLabel(a.get("table"))) + "')]";
//
//									waitLoadItemByXpath(xpath, 10);
//									try {
//										driver.findElement(By.ByXPath.xpath(xpath)).click();
//									} catch (Exception e) {
//										tabNavigateClick("centerpanel__" + SYSTEM + "/SystemVO_*_show_tab");
//										driver.findElement(By.ByXPath.xpath(xpath)).click();
//									}
//
//									waitForListGrid(a);
//									try {
//										if (testInsert(a)) {
//											if (configSisAux.thisBeanDetailAfterInsert(a)) {
//												waitForDetail(a);
//											} else {
//												testDetail(a, 0);
//											}
//											testDetailBeans(a, configSisAux.getDetailBeans(a));
//											if (!configSisAux.getDetailBeans(a).isEmpty()) {
//												try {
//													driver.findElement(By.ById.id(getIdentifier(a, null, null,
//															CmpToIdentify.TAB_RESUMO, null))).click();
//													waitLoadItemById(getIdentifier(a, null, null,
//															CmpToIdentify.TAB_RESUMO_CONTENT, null), 10);
//												} catch (Exception e) {
//												}
//											}
//											testUpdate(a);
//											testDelete(a);
//										}
//									} catch (Exception e) {
//										continue;
//									}
//
//									try {
//										tabNavigateClick("centerpanel__" + SYSTEM + "/SystemVO_*_show_tab");
//									} catch (Exception e) {
//										tabNavigateClick(a);
//									}
//									waitLoadingModalHide();
//									while (tabNavigateClose(bean, a, CmpToIdentify.TAB_DETAIL_REGISTRO, false)) {
//										;
//									}
//									for (;;) {
//										try {
//											driver.findElement(By.ById.id("centerpanel__" + SYSTEM + "/" + a.get("name")
//													+ "VO_" + getPkValueOfContext(a, false) + "_obtem_tab"))
//													.findElement(By.ByClassName.className("x-tab-strip-close")).click();
//										} catch (Exception e) {
//											break;
//										}
//									}
//
//									tabNavigateClose(bean, a, CmpToIdentify.TAB_LISTA, false);
//								} catch (Exception e) {
//									continue;
//								}
//							}
//							waitLoadingModalHide();
//							tabNavigateClick(bean);
//						}
//					}
//				}
//			} else {
//				menuClick(bean);
//				for (Map<String, String> c : categorias) {
//					try {
//						if (testConfig.isToAutoNavigateInLinks(c) && testConfig.isToTestThisBean(bean, c)) {
//							autoNavigateCategoryLinks(c);
//						} else {
//							if (testConfig.isToTestThisBean(bean, c)) {
//								r.initLogBean(c);
//								executeTestScript(c, SeleniumTestConfig.ScriptNature.BEFORE_LIST, null,
//										SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//								if (!categoriaLinkClick(bean, user.getLabel(c.get("label")), false)) {
//									String xpath = "//div[@id='" + SYSTEM
//											+ "/SystemVO_*_show_tab']//div[@id='categoria-links']//dd/div/h4[contains( text(),'"
//											+ user.getLabel(user.getLabel(c.get("label"))) + "')]";
//									waitLoadItemByXpath(xpath, 10);
//									driver.findElement(By.ByXPath.xpath(xpath)).click();
//								}
//								executeTestScript(c, SeleniumTestConfig.ScriptNature.AFTER_LIST, null,
//										SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//								waitLoadingModalHide();
//								waitForListGrid(c);
//								try {
//									testCrudOfContext(c, null);
//								} catch (Exception e) {
//									continue;
//								}
//								tabNavigateClick(bean);
//								waitLoadingModalHide();
//							}
//						}
//					} catch (Exception e) {
//						tabNavigateClick(bean);
//						continue;
//					}
//				}
//			}
//		}
//	}
//
//	public boolean testInsert(Map<String, String> context) {
//		boolean okControl = false;
//		if (testConfig.isToTestThisOperation(ServiceMethod.INSERT)) {
//			executeTestScript(context, SeleniumTestConfig.ScriptNature.BEFORE_INSERT, null,
//					SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//			r.appendResultTest(SeleniumReport.Prefix.INI, SeleniumReport.Method.INSERT, context, "");
//			List<Map<String, String>> propsContext = configSisAux.getItemsOfBean(context);
//			waitLoadItemById(getIdentifier(context, null, null, CmpToIdentify.B_ADICIONAR, ServiceMethod.INSERT), 10);
//			adicionarClick(context);
//			analiseAndSetValueInPropertyFields(context, null, ServiceMethod.INSERT);
//			salvarClick(context, ServiceMethod.INSERT);
//			okControl = okControl(context, SeleniumReport.Method.INSERT);
//			waitForInsertWindowHide(context);
//			r.appendResultTest(SeleniumReport.Prefix.FIM, SeleniumReport.Method.INSERT, context, "");
//			executeTestScript(context, SeleniumTestConfig.ScriptNature.AFTER_INSERT, null,
//					SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//		}
//		if (!testConfig.isToTestThisOperation(ServiceMethod.INSERT)) {
//			return true;
//		} else {
//			return okControl;
//		}
//	}
//
//	public boolean testUpdate(Map<String, String> context) {
//		boolean okControl = false;
//		if (testConfig.isToTestThisOperation(ServiceMethod.UPDATE)) {
//			ServiceMethod sm;
//			try {
//				waitLoadingModalHide();
//				try {
//					driver.findElements(By.ByXPath
//							.xpath("//div[@id='" + getIdentifier(context, null, null, CmpToIdentify.GRID_LIST, null)
//									+ "']//div[@class='x-grid3-row-checker']"))
//							.get(0).click();
//					sm = ServiceMethod.LIST;
//				} catch (Exception e) {
//					sm = ServiceMethod.OBTEM;
//				}
//				executeTestScript(context, SeleniumTestConfig.ScriptNature.BEFORE_UPDATE, null,
//						SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//				r.appendResultTest(SeleniumReport.Prefix.INI, SeleniumReport.Method.UPDATE, context, "");
//				alterarClick(context, sm);
//				waitLoadItemById(getIdentifier(context, null, null, CmpToIdentify.ALTERA_WINDOW, null), 15);
//				analiseAndSetValueInPropertyFields(context, null, ServiceMethod.UPDATE);
//				salvarClick(context, ServiceMethod.UPDATE);
//				okControl = okControl(context, SeleniumReport.Method.UPDATE);
//				waitLoadingModalHide();
//			} catch (Exception e) {
//				r.appendResultTest(SeleniumReport.Prefix.ERRO, SeleniumReport.Method.UPDATE, context, "");
//			}
//			r.appendResultTest(SeleniumReport.Prefix.FIM, SeleniumReport.Method.UPDATE, context, "");
//			executeTestScript(context, SeleniumTestConfig.ScriptNature.AFTER_UPDATE, null,
//					SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//		}
//		return okControl;
//	}
//
//	public void testDelete(Map<String, String> context) {
//		if (testConfig.isToTestThisOperation(ServiceMethod.DELETE)) {
//			waitLoadingModalHide();
//			try {
//				ServiceMethod sm;
//				try {
//					driver.findElements(By.ByXPath
//							.xpath("//div[@id='" + getIdentifier(context, null, null, CmpToIdentify.GRID_LIST, null)
//									+ "']//div[@class='x-grid3-row-checker']"))
//							.get(0).click();
//					sm = ServiceMethod.LIST;
//				} catch (Exception e) {
//					waitForDetail(context);
//					sm = ServiceMethod.OBTEM;
//				}
//				executeTestScript(context, SeleniumTestConfig.ScriptNature.BEFORE_REMOVE, null,
//						SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//				r.appendResultTest(SeleniumReport.Prefix.INI, SeleniumReport.Method.DELETE, context, "");
//				removeClick(context, sm);
//				simClick();
//				okControl(context, SeleniumReport.Method.DELETE);
//				r.appendResultTest(SeleniumReport.Prefix.FIM, SeleniumReport.Method.DELETE, context, "");
//				executeTestScript(context, SeleniumTestConfig.ScriptNature.AFTER_REMOVE, null,
//						SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//				waitLoadingModalHide();
//			} catch (Exception ex) {
//				return;
//			}
//		}
//	}
//
//	public void testMethods(Map<String, String> bean) {
//		while (tabNavigateClose(null, bean, CmpToIdentify.TAB_DETAIL_MASTER, false)) {
//			;
//		}
//		;
//		if (testConfig.isToTestThisOperation(ServiceMethod.METHOD)) {
//			try {
//				List<Map<String, String>> methods = configSisAux.getBeanMethods(bean, true, "[@type != 'L']");
//				waitForListGrid(bean);
//				waitLoadingModalHide();
//				int gridRows = getRowsOfGrid(bean).size();
//				for (Map<String, String> m : methods) {
//					String errorMsg = "";
//					try {
//						if (!m.get("type").equals("C") && gridRows > 0) {
//							selectCheckItem(bean, null, true, 0, ServiceMethod.LIST, null);
//						}
//					} catch (Exception e) {
//					}
//
//					/*
//					 * if(m.get("type").equals("C")){
//					 * getJavaScriptExecutor().executeScript(
//					 * "Ext.getCmp('"+getIdentifier(bean, null, null,
//					 * CmpToIdentify.GRID_LIST, ServiceMethod.LIST)+
//					 * "').getSelectionModel().clearSelections()"); }
//					 */
//
//					if (methodClick(bean, m)) {
//						executeTestScript(bean, SeleniumTestConfig.ScriptNature.BEFORE_METHOD, m.get("name"),
//								SeleniumTestConfig.ScriptTypes.TEST_SCRIPT);
//						r.appendResultTest(SeleniumReport.Prefix.INI, SeleniumReport.Method.METHOD, m, null);
//						if (!m.get("type").equals("R")) {
//							if (m.get("type").equalsIgnoreCase("A")) {
//								simClick();
//							} else {
//								analiseAndSetValueInPropertyFields(bean, m, ServiceMethod.METHOD);
//								bean.remove("fieldPrefix");
//								bean.remove("fieldSufix");
//								driver.findElement(By.ById
//										.id(getIdentifier(bean, m, null, CmpToIdentify.B_SALVAR, ServiceMethod.METHOD)))
//										.click();
//								errorMsg = waitLoadingModalHide();
//							}
//
//							if (!errorMsg.isEmpty() || !okControl(bean, SeleniumReport.Method.METHOD)) {
//								r.appendResultTest(SeleniumReport.Prefix.ERRO, SeleniumReport.Method.METHOD, bean,
//										errorMsg);
//								bean.put("fieldSufix", m.get("name"));
//								driver.findElement(By.ById.id(
//										getIdentifier(bean, m, null, CmpToIdentify.B_CANCELAR, ServiceMethod.METHOD)))
//										.click();
//								bean.remove("fieldSufix");
//							}
//						}
//						r.appendResultTest(SeleniumReport.Prefix.FIM, SeleniumReport.Method.METHOD, m, null);
//						waitLoadingModalHide();
//						if (bean.get("type").equals("D")) {
//							// waitForTabResumoClickAndShow(configSisAux.getBeanById(bean.get("master")));
//							tabDetailNaviClick(configSisAux.getBeanById(bean.get("master")), bean);
//						}
//						if (m.get("type").equals("C") && errorMsg.isEmpty()) {
//							try {
//								waitForDetail(bean);
//							} catch (Exception ex) {
//							}
//							driver.findElement(
//									By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.TAB_LISTA, null))).click();
//						}
//					}
//				}
//			} catch (Exception e) {
//			}
//		}
//	}
//
//	public boolean menuClick(Map<String, String> bean) {
//		if (menuClick(bean.get("label"))) {
//			waitForCategoriaLinks(bean);
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	public boolean menuClick(String label) {
//		try {
//			driver.findElement(By.ByXPath.xpath("//*[text()='" + user.getLabel(label) + "']")).click();
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public boolean tabNavigateClick(Map<String, String> bean) {
//		try {
//			tabNavigateClick(getIdentifier(bean, null, null, CmpToIdentify.TAB_NAVIGATION, null));
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public boolean tabGridListClick(Map<String, String> bean) {
//		try {
//			driver.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.TAB_LISTA, null))).click();
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public boolean tabNavigateClick(String id) {
//		try {
//			driver.findElement(By.ById.id(id)).click();
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public boolean tabNavigateClose(Map<String, String> master, Map<String, String> bean, CmpToIdentify cmp,
//			boolean isSystem) {
//		try {
//			if (isSystem) {
//				driver.findElement(By.ById.id("centerpanel__" + SYSTEM + "/SystemVO_*_show_tab"))
//						.findElement(By.ByClassName.className("x-tab-strip-close")).click();
//			} else {
//				switch (cmp) {
//				case TAB_NAVIGATION:
//					driver.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.TAB_NAVIGATION, null)))
//							.findElement(By.ByClassName.className("x-tab-strip-close")).click();
//					break;
//				case TAB_DETAIL_MASTER:
//					driver.findElement(
//							By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.TAB_DETAIL_MASTER, null)))
//							.findElement(By.ByClassName.className("x-tab-strip-close")).click();
//					break;
//				case TAB_LISTA:
//					driver.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.TAB_LISTA, null)))
//							.findElement(By.ByClassName.className("x-tab-strip-close")).click();
//					break;
//				case TAB_DETAIL:
//					driver.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.TAB_NAVIGATION, null)))
//							.findElement(By.ByClassName.className("x-tab-strip-close")).click();
//				case TAB_DETAIL_REGISTRO:
//					driver.findElement(
//							By.ById.id(getIdentifier(master, null, bean, CmpToIdentify.TAB_DETAIL_REGISTRO, null)))
//							.findElement(By.ByClassName.className("x-tab-strip-close")).click();
//					break;
//				}
//			}
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public boolean adicionarClick(Map<String, String> bean) {
//		try {
//			driver.findElement(
//					By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.B_ADICIONAR, ServiceMethod.LIST))).click();
//			waitLoadItemById("window", -1);
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public boolean methodClick(Map<String, String> bean, Map<String, String> method) {
//		try {
//
//			if (driver.findElement(By.ById.id(getIdentifier(bean, method, null, CmpToIdentify.B_METHOD, null)))
//					.isEnabled()
//					&& !(new Boolean(getJavaScriptExecutor()
//							.executeScript("return Ext.getCmp('"
//									+ getIdentifier(bean, method, null, CmpToIdentify.B_METHOD, null) + "').disabled")
//							.toString()))) {
//
//				driver.findElement(By.ById.id(getIdentifier(bean, method, null, CmpToIdentify.B_METHOD, null))).click();
//				if (!method.get("type").equalsIgnoreCase("A")) {
//					try {
//						waitLoadItemById("window", 10);
//					} catch (Exception e) {
//
//					}
//				}
//				return true;
//			}
//		} catch (Exception e) {
//			return false;
//		}
//		return false;
//	}
//
//	public boolean removeClick(Map<String, String> bean, ServiceMethod m) {
//		try {
//			driver.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.B_EXCLUIR, m))).click();
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public boolean alterarClick(Map<String, String> bean, ServiceMethod m) {
//		try {
//			driver.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.B_ALTERAR, m))).click();
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public boolean salvarClick(Map<String, String> context, ServiceMethod m) {
//		try {
//			driver.findElement(By.ById.id(getIdentifier(context, null, null, CmpToIdentify.B_SALVAR, m))).click();
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public boolean cancelarClick(Map<String, String> bean, ServiceMethod m) {
//		try {
//			driver.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.B_CANCELAR, m))).click();
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public String okClick() {
//		String msg = "";
//		try {
//			if (driver.findElement(By.ByXPath.xpath("//*[text()='OK']")).isDisplayed()) {
//				try {
//					msg = getJavaScriptExecutor().executeScript("return jQuery('.ext-mb-content').text()").toString();
//				} catch (Exception e) {
//				}
//				driver.findElement(By.ByXPath.xpath("//*[text()='OK']")).click();
//				if (msg.isEmpty()) {
//					msg = "#";
//				}
//				return msg;
//			}
//		} catch (Exception e) {
//			return "";
//		}
//		return "";
//	}
//
//	public boolean categoriaLinkClick(Map<String, String> bean, String label, boolean waitForGrid) {
//		try {
//			driver.findElements(By.ByXPath.xpath("//div[@id='"
//					+ getIdentifier(bean, null, null, CmpToIdentify.ID_SHOW_CONTEXT, null)
//					+ "']//div[@id='categoria-links']//dd/div/h4[contains( text(),'" + user.getLabel(label) + "')]"))
//					.get(0).click();
//			if (waitForGrid) {
//				waitForListGrid(bean);
//			} else {
//				waitForCategoriaLinks(bean);
//			}
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public boolean waitForCategoriaLinks(Map<String, String> bean) {
//		try {
//			waitLoadItemByXpath("//div[@id='" + getIdentifier(bean, null, null, CmpToIdentify.ID_SHOW_CONTEXT, null)
//					+ "']//div[@id='categoria-links']//dd", -1);
//			return true;
//		} catch (TimeoutException to) {
//			return false;
//		}
//	}
//
//	public void waitForListGrid(Map<String, String> bean) {
//		int wait = -1;
//		if (bean.get("type").equals("A")) {
//			wait = 7;
//		}
//		try {
//			waitLoadItemById(getIdentifier(bean, null, null, CmpToIdentify.GRID_LIST, null), wait);
//		} catch (Exception e) {
//			return;
//		}
//	}
//
//	public void waitForDetail(Map<String, String> bean) throws TimeoutException {
//		waitLoadItemByXpath(
//				"//div[@id='" + SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_obtem']",
//				10);
//	}
//
//	public void waitForUpdateWindowHide(Map<String, String> bean) {
//		while (true) {
//			try {
//				driver.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.ALTERA_WINDOW, null)));
//			} catch (Exception e) {
//				break;
//			}
//		}
//	}
//
//	public void waitForInsertWindowHide(Map<String, String> bean) {
//		while (true) {
//			try {
//				driver.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.INSERT_WINDOW, null)));
//			} catch (Exception e) {
//				break;
//			}
//		}
//	}
//
//	public void waitLoadItemById(final String item, int timeOut) {
//		waitLoadItem(By.ById.id(item), timeOut);
//	}
//
//	public void waitLoadItemByName(final String item, int timeOut) {
//		waitLoadItem(By.ByName.name(item), timeOut);
//	}
//
//	public void waitLoadItem(final By by, int timeOut) {
//		if (timeOut == -1) {
//			timeOut = TIME_TO_WAIT_PAGE_LOAD;
//		}
//		// esperar algum componente da tela carregar, para realizar alguma
//		// operao.
//		(new WebDriverWait(driver, timeOut)).until(new ExpectedCondition<WebElement>() {
//			@Override
//			public WebElement apply(WebDriver d) {
//				return d.findElement(by);
//			}
//		});
//	}
//
//	public void waitLoadItemByXpath(final String item, int timeOut) throws TimeoutException {
//		waitLoadItem(By.ByXPath.xpath(item), timeOut);
//	}
//
//	public void waitWindowClose() {
//		while (true) {
//			try {
//				driver.findElement(By.ById.id("window"));
//			} catch (NoSuchElementException e) {
//				break;
//			}
//		}
//	}
//
//	public void selectComboItem(String id, ServiceMethod sm, String value) {
//		int valueIdx = 0;
//		if (sm == ServiceMethod.UPDATE) {
//			valueIdx = 1;
//		}
//		if (value != null && !value.isEmpty()) {
//			driver.findElement(By.ById.id(id)).sendKeys(Keys.ARROW_DOWN);
//			driver.findElement(By.ById.id(id)).click();
//			waitLoadItemById(id + "_selectItem", 6);
//			getJavaScriptExecutor().executeScript("Ext.getCmp('" + id + "').setValue(" + value + ")");
//		} else {
//			try {
//				driver.findElement(By.ById.id(id)).sendKeys(Keys.ARROW_DOWN);
//				driver.findElement(By.ById.id(id)).click();
//				waitLoadItemById(id + "_selectItem", 6);
//				try {
//					driver.findElements(By.ById.id(id + "_selectItem")).get(valueIdx).click();
//				} catch (Exception e) {
//					driver.findElement(By.ById.id(id + "_selectItem")).click();
//				}
//			} catch (Exception e) {
//			}
//		}
//	}
//
//	public void selectRadioGroupItem(Map<String, String> context, Map<String, String> radio, ServiceMethod sm,
//			String value, Map<String, String> propContext) {
//		if (value.isEmpty()) {
//			if ((context.get("name") + "Service").equals(radio.get("service"))) {
//				selectRadioGroupItem(context, radio, user.getLabel(
//						configSisAux.getValuesOfMethodList(context, radio).entrySet().iterator().next().getKey()), sm,
//						propContext);
//			} else {
//				// tem que buscar as opes em outro contexto
//			}
//		} else {
//			selectRadioGroupItem(context, radio, value, sm, propContext);
//		}
//	}
//
//	public void selectRadioGroupItem(Map<String, String> context, Map<String, String> radio, String value,
//			ServiceMethod sm, Map<String, String> propContext) {
//		try {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, radio, CmpToIdentify.FIELD, sm)))
//					.findElement(By.ByCssSelector.cssSelector("input[value='" + value + "']")).click();
//		} catch (Exception e) {
//
//		}
//	}
//
//	public void setTimeValue(Map<String, String> context, Map<String, String> propContext, Map<String, String> prop,
//			ServiceMethod sm, String value) {
//		int valueIdx = 0;
//		if (sm == ServiceMethod.UPDATE) {
//			valueIdx = 1;
//		}
//		if (value.isEmpty()) {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//					.sendKeys(ValuesToFields.TIME.values[valueIdx]);
//		} else {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//					.sendKeys(value);
//		}
//	}
//
//	public void setMoneyValue(Map<String, String> context, Map<String, String> propContext, Map<String, String> prop,
//			ServiceMethod sm, String value) {
//		int valueIdx = 0;
//		if (sm == ServiceMethod.UPDATE) {
//			valueIdx = 1;
//		}
//		if (value.isEmpty()) {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//					.sendKeys(ValuesToFields.MONEY.values[valueIdx]);
//		} else {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//					.sendKeys(value);
//		}
//	}
//
//	public void dropFromGridToGrid(Map<String, String> bean, Map<String, String> propContext, Map<String, String> prop,
//			ServiceMethod sm, String values) {
//		prop.put("listType", "FROM");
//		String fromId = getIdentifier(bean, propContext, prop, CmpToIdentify.FIELD, sm);
//		prop.put("listType", "TO");
//		String toId = getIdentifier(bean, propContext, prop, CmpToIdentify.FIELD, sm);
//
//		// TODO Definir como configurar os itens a serem arrastados de um grid
//		// para o outro...
//		getJavaScriptExecutor().executeScript("try{" + "Ext.getCmp('" + toId + "').getStore().add(" + "Ext.getCmp('"
//				+ fromId + "').getStore().data.items[0]);" + "}catch(ex){}");
//
//	}
//
//	public void setNumValue(Map<String, String> context, Map<String, String> propContext, Map<String, String> prop,
//			ServiceMethod sm, String value) {
//		int valueIdx = 0;
//		if (sm == ServiceMethod.UPDATE) {
//			valueIdx = 1;
//		}
//		driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm))).clear();
//		if (value.isEmpty()) {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//					.sendKeys(ValuesToFields.NUM.values[valueIdx]);
//		} else {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//					.sendKeys(value);
//		}
//	}
//
//	public void setDateValue(Map<String, String> context, Map<String, String> propContext, Map<String, String> prop,
//			ServiceMethod sm, String value) {
//		int valueIdx = 0;
//		if (sm == ServiceMethod.UPDATE) {
//			valueIdx = 1;
//		}
//		if (value.isEmpty()) {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm))).clear();
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//					.sendKeys(ValuesToFields.DATA.values[valueIdx]);
//		} else {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm))).clear();
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//					.sendKeys(value);
//		}
//	}
//
//	public void setTextValue(Map<String, String> context, Map<String, String> propContext, Map<String, String> prop,
//			ServiceMethod sm, String value) {
//		int valueIdx = 0;
//		if (sm == ServiceMethod.UPDATE) {
//			valueIdx = 1;
//		}
//		if (value.isEmpty()) {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm))).clear();
//			if (prop.get("mask") != null && prop.get("mask").equals("duracao")) {
//				driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//						.sendKeys(ValuesToFields.TEXT_DURACAO.values[valueIdx]);
//			} else {
//				driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//						.sendKeys(ValuesToFields.TEXT.values[valueIdx]);
//			}
//		} else {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm))).clear();
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//					.sendKeys(value);
//		}
//	}
//
//	public void selectCheckItem(Map<String, String> context, Map<String, String> check, boolean isList, int idx,
//			ServiceMethod m, String value) {
//		if (isList) {
//			waitLoadItemByXpath("//div[@id='" + getIdentifier(context, null, null, CmpToIdentify.GRID_LIST, null)
//					+ "']//div[@class='x-grid3-row-checker']", -1);
//			getJavaScriptExecutor()
//					.executeScript("Ext.getCmp('" + getIdentifier(context, null, null, CmpToIdentify.GRID_LIST, m)
//							+ "').getSelectionModel().selectRow(" + idx + ")");
//		} else {
//			if (!value.isEmpty()) {
//				if (value.trim().equals("T") || value.trim().equals("F")) {
//					if (value.trim().equals("T")) {
//						value = "true";
//					} else {
//						value = "false";
//					}
//				}
//				if (value.equals("true") || value.equals("false")) {
//					getJavaScriptExecutor()
//							.executeScript("Ext.getCmp('" + getIdentifier(context, null, check, CmpToIdentify.FIELD, m)
//									+ "').setValue(" + value + ")");
//				}
//			} else {
//				driver.findElement(By.ById.id(getIdentifier(context, null, check, CmpToIdentify.FIELD, m))).click();
//			}
//		}
//	}
//
//	public JavascriptExecutor getJavaScriptExecutor() {
//		return ((JavascriptExecutor) driver);
//	}
//
//	public String getIdentifier(Map<String, String> bean, Map<String, String> fieldContext,
//			Map<String, String> field_or_tab_or_beanDetail, CmpToIdentify cmpToId, ServiceMethod method) {
//
//		// bean = Contexto ou um master para identificar um elemento
//
//		// field_or_tab_or_beanDetail = Pode ser um field, ou uma tab, ou um
//		// bean detail que deve ser acompanhado do seu master passado como
//		// 'bean'
//
//		// fieldContext = a property com component context, e o field deve vir
//		// em 'field_or_tab_or_beanDetail', acompanhado do bean e se nescessrio
//		// utilizar um atributo 'fieldSufix' colocado no bean, para
//		// ser inserido como sufixo da identificao, e fieldPrefix para o
//		// contrrio.
//
//		String result = "";
//		switch (cmpToId) {
//		case FIELD:
//			if (method == ServiceMethod.METHOD) {
//				result = field_or_tab_or_beanDetail.get("name") + SYSTEM + "/" + bean.get("name") + "VO_"
//						+ getPkValueOfContext(bean, false);
//				if (bean.get("fieldSufix") == null || bean.get("fieldSufix").isEmpty()) {
//					result += "_" + fieldContext.get("name");
//				} else {
//					result += "_" + bean.get("fieldSufix");
//				}
//			} else {
//				if (bean.get("type").equals("M") || bean.get("type").equals("A")
//						|| (field_or_tab_or_beanDetail.get("component").equals(PropertyComponents.LIST.value))) {
//					if (field_or_tab_or_beanDetail.get("component") != null
//							&& field_or_tab_or_beanDetail.get("component").equals(PropertyComponents.LIST.value)
//							&& field_or_tab_or_beanDetail.get("listType") != null) {
//						if (method == ServiceMethod.INSERT) {
//							result = SYSTEM + "/" + bean.get("name") + "VO_*_inclui"
//									+ field_or_tab_or_beanDetail.get("name") + "_"
//									+ field_or_tab_or_beanDetail.get("listType") + "_grid";
//						} else if (method == ServiceMethod.UPDATE) {
//							result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false)
//									+ "_altera" + field_or_tab_or_beanDetail.get("name") + "_"
//									+ field_or_tab_or_beanDetail.get("listType") + "_grid";
//						}
//					} else {
//						if (method == ServiceMethod.INSERT) {
//							result = (fieldContext == null || fieldContext.get("name") == null ? ""
//									: fieldContext.get("name") + "_") + field_or_tab_or_beanDetail.get("name") + SYSTEM
//									+ "/" + bean.get("name") + "VO_*_inclui";
//						} else if (method == ServiceMethod.UPDATE) {
//							result = (fieldContext == null || fieldContext.get("name") == null ? ""
//									: fieldContext.get("name") + "_") + field_or_tab_or_beanDetail.get("name") + SYSTEM
//									+ "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_altera";
//						}
//					}
//				} else if (bean.get("type").equals("D")) {
//					if (method == ServiceMethod.INSERT) {
//						result = (fieldContext != null ? fieldContext.get("name") : "")
//								+ field_or_tab_or_beanDetail.get("name") + SYSTEM + "/" + bean.get("name") + "VO_"
//								+ getPkValueOfContext(bean, true) + "_inclui";
//					} else if (method == ServiceMethod.UPDATE) {
//						result = (fieldContext != null ? fieldContext.get("name") : "")
//								+ field_or_tab_or_beanDetail.get("name") + SYSTEM + "/" + bean.get("name") + "VO_"
//								+ getPkValueOfContext(bean, false) + "_altera";
//					}
//				}
//			}
//			break;
//		case FIELD_IN_DETAIL:
//			result = field_or_tab_or_beanDetail.get("name") + SYSTEM + "/" + bean.get("name") + "VO_"
//					+ getPkValueOfContext(bean, false) + "_obtem";
//			break;
//		case B_ADICIONAR:
//			if (bean.get("type").equals("M")) {
//				result = "label_inclui-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_*_lista";
//			} else if (bean.get("type").equals("D")) {
//				result = "label_inclui-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_"
//						+ getPkValueOfContext(bean, true) + "_lista";
//			} else if (bean.get("type").equals("A")) {
//				result = "label_inclui-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_*_lista";
//			}
//			break;
//		case B_ALTERAR:
//			if (bean.get("type").equals("M") || bean.get("type").equals("A")) {
//				if (method == ServiceMethod.LIST) {
//					result = "label_altera-FORONE_" + SYSTEM + "/" + bean.get("name") + "VO_*_lista";
//				} else if (method == ServiceMethod.OBTEM) {
//					result = "label_altera-FORONE_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, false) + "_obtem";
//				}
//			} else if (bean.get("type").equals("D")) {
//				if (method == ServiceMethod.LIST) {
//					result = "label_altera-FORONE_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, true) + "_lista";
//				} else if (method == ServiceMethod.OBTEM) {
//					result = "label_altera-FORONE_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, false) + "_obtem";
//				}
//			}
//			break;
//		case B_CANCELAR:
//
//			if (method == ServiceMethod.METHOD) {
//				result = "label_cancelar-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_"
//						+ getPkValueOfContext(bean, false) + "_" + fieldContext.get("name");
//			}
//			if (bean.get("type").equals("M") || bean.get("type").equals("A")) {
//				if (method == ServiceMethod.INSERT) {
//					result = "label_cancelar-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_*_inclui";
//				} else if (method == ServiceMethod.UPDATE) {
//					result = "cancelar-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, false) + "_altera";
//				}
//			} else if (bean.get("type").equals("D")) {
//				if (method == ServiceMethod.INSERT) {
//					result = "label_cancelar-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, true) + "_inclui";
//				} else if (method == ServiceMethod.UPDATE) {
//					result = "label_cancelar-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, false) + "_altera";
//				}
//			}
//			break;
//		case B_EXCLUIR:
//			if (bean.get("type").equals("M") || bean.get("type").equals("A")) {
//				if (method == ServiceMethod.LIST) {
//					result = "label_remove-FORALL_" + SYSTEM + "/" + bean.get("name") + "VO_*_lista";
//				} else if (method == ServiceMethod.OBTEM) {
//					result = "label_remove-FORALL_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, false) + "_obtem";
//				}
//			} else if (bean.get("type").equals("D")) {
//				if (method == ServiceMethod.LIST) {
//					result = "label_remove-FORALL_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, true) + "_lista";
//				} else if (method == ServiceMethod.OBTEM) {
//					result = "label_remove-FORALL_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, false) + "_obtem";
//				}
//			}
//			break;
//		case B_METHOD:
//			String actionFor = "FORALL";
//
//			if (fieldContext.get("type") != null && fieldContext.get("type").equalsIgnoreCase("A")
//					|| fieldContext.get("type").equalsIgnoreCase("D")) {
//				actionFor = "CONDITION";
//			} else if (fieldContext.get("type").toString().equalsIgnoreCase("R")
//					|| fieldContext.get("type").toString().equalsIgnoreCase("L")
//					|| fieldContext.get("type").toString().equalsIgnoreCase("C")) {
//				actionFor = "STATIC";
//				if (fieldContext.get("nature") != null
//						&& fieldContext.get("nature").toString().equalsIgnoreCase("persistent")) {
//					actionFor = "FORONE";
//				}
//			}
//			if (fieldContext.get("nature") != null && fieldContext.get("nature").toString().equalsIgnoreCase("U")) {
//				actionFor = "FORONE";
//			}
//
//			if (bean.get("type").equals("D")) {
//				result = bean.get("table") + "_" + fieldContext.get("name") + "-" + actionFor + "_" + SYSTEM + "/"
//						+ bean.get("name") + "VO_" + getPkValueOfContext(bean, true) + "_lista";
//			} else if (bean.get("type").equals("M")) {
//				result = bean.get("table") + "_" + fieldContext.get("name") + "-" + actionFor + "_" + SYSTEM + "/"
//						+ bean.get("name") + "VO_*_lista";
//			}
//			break;
//		case B_SALVAR:
//			if (method == ServiceMethod.METHOD) {
//				result = "label_salvar-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_"
//						+ getPkValueOfContext(bean, false) + "_" + fieldContext.get("name");
//			}
//			if (bean.get("type").equals("M") || bean.get("type").equals("A")) {
//				if (method == ServiceMethod.INSERT) {
//					result = "label_salvar-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_*_inclui";
//				} else if (method == ServiceMethod.UPDATE) {
//					result = "label_salvar-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, false) + "_altera";
//				}
//			} else if (bean.get("type").equals("D")) {
//				if (method == ServiceMethod.INSERT) {
//					result = "label_salvar-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, true) + "_inclui";
//				} else if (method == ServiceMethod.UPDATE) {
//					result = "label_salvar-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_"
//							+ getPkValueOfContext(bean, false) + "_altera";
//				}
//			}
//			break;
//		case ALTERA_WINDOW:
//			result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_altera";
//			break;
//		case INSERT_WINDOW:
//			if (bean.get("type").equals("M") || bean.get("type").equals("A")) {
//				return SYSTEM + "/" + bean.get("name") + "VO_*_inclui";
//			} else if (bean.get("type").equals("D")) {
//				return SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, true) + "_inclui";
//			}
//			break;
//		case GRID_LIST:
//			if (bean.get("type").equals("M")) {
//				result = SYSTEM + "/" + bean.get("name") + "VO_*_lista_grid";
//			} else if (bean.get("type").equals("D")) {
//				result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, true) + "_lista_grid";
//			} else if (bean.get("type").equals("A")) {
//				result = SYSTEM + "/" + bean.get("name") + "VO_*_lista_grid";
//			}
//			break;
//		case TAB_DETAIL:
//			result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(field_or_tab_or_beanDetail, true)
//					+ "_obtem_tab__" + field_or_tab_or_beanDetail.get("name") + SYSTEM + "/" + bean.get("name") + "VO_"
//					+ getPkValueOfContext(field_or_tab_or_beanDetail, true) + "_obtem";
//			break;
//		case TAB_INSERT_UPDATE:
//			if (method == ServiceMethod.METHOD) {
//				result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_"
//						+ bean.get("fieldSufix") + "_tab__" + bean.get("fieldPrefix") + SYSTEM + "/" + bean.get("name")
//						+ "VO_" + getPkValueOfContext(bean, false) + "_" + bean.get("fieldSufix");
//			} else {
//				if (bean.get("type").equals("M") || bean.get("type").equals("A")) {
//					if (method == ServiceMethod.INSERT) {
//						result = SYSTEM + "/" + bean.get("name") + "VO_*_inclui_tab__"
//								+ field_or_tab_or_beanDetail.get("name") + SYSTEM + "/" + bean.get("name")
//								+ "VO_*_inclui";
//					} else if (method == ServiceMethod.UPDATE) {
//						result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false)
//								+ "_altera_tab__" + field_or_tab_or_beanDetail.get("name") + SYSTEM + "/"
//								+ bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_altera";
//					}
//				} else if (bean.get("type").equals("D")) {
//					if (method == ServiceMethod.INSERT) {
//						result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, true)
//								+ "_inclui_tab__" + field_or_tab_or_beanDetail.get("name") + SYSTEM + "/"
//								+ bean.get("name") + "VO_" + getPkValueOfContext(bean, true) + "_inclui";
//					} else if (method == ServiceMethod.UPDATE) {
//						result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false)
//								+ "_altera_tab__" + field_or_tab_or_beanDetail.get("name") + SYSTEM + "/"
//								+ bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_altera";
//					}
//				}
//			}
//			break;
//		case TAB_RESUMO:
//			if (bean.get("type").equals("M") || bean.get("type").equals("A")) {
//				result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_obtem_tab__"
//						+ SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_obtem";
//			} else if (bean.get("type").equals("D")) {
//				result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_obtem_tab__"
//						+ SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_obtem";
//			}
//			break;
//		case TAB_RESUMO_CONTENT:
//			result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_obtem";
//			break;
//		case ID_SHOW_CONTEXT:
//			result = "DETAIL" + SYSTEM + "/" + bean.get("name") + "VO_*_show";
//			break;
//		case TAB_NAVIGATION:
//			result = "centerpanel__" + SYSTEM + "/" + bean.get("name") + "VO_*_show_tab";
//			break;
//		case TAB_DETAIL_MASTER:
//			result = "centerpanel__" + SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false)
//					+ "_detail_master_tab";
//			break;
//		case TAB_LISTA:
//			result = "centerpanel__" + SYSTEM + "/" + bean.get("name") + "VO_*_lista_tab";
//			break;
//		case TAB_DETAIL_REGISTRO:
//			result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_obtem_tab__"
//					+ SYSTEM + "/" + field_or_tab_or_beanDetail.get("name") + "VO_"
//					+ getPkValueOfContext(field_or_tab_or_beanDetail, false) + "_obtem_tab";
//		case B_ADVANCED_SEARCH:
//			if (bean.get("type").equals("M")) {
//				result = "label_search-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_*_lista";
//			} else {
//				result = "label_search-STATIC_" + SYSTEM + "/" + bean.get("name") + "VO_"
//						+ getPkValueOfContext(bean, true) + "_lista";
//			}
//			break;
//		case FILTRO_TREE_PANEL:
//			if (bean.get("type").equals("M")) {
//				result = "FILTRO" + SYSTEM + "/" + bean.get("name") + "VO_*_lista_tree";
//			} else {
//				result = "FILTRO" + SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, true)
//						+ "_lista_tree";
//			}
//			break;
//		case B_BUSCAR:
//			if (bean.get("type").equals("M")) {
//				result = "buscarFILTRO" + SYSTEM + "/" + bean.get("name") + "VO_*_lista_tree";
//			} else {
//				result = "buscarFILTRO" + SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, true)
//						+ "_lista_tree";
//			}
//			break;
//		case HEADER_DETAIL:
//			result = SYSTEM + "/" + bean.get("name") + "VO_" + getPkValueOfContext(bean, false) + "_detail_fs";
//			break;
//		default:
//			break;
//		}
//
//		if (cmpToId != CmpToIdentify.TAB_INSERT_UPDATE) {
//			if (bean.get("fieldPrefix") != null && !bean.get("fieldPrefix").isEmpty()) {
//				result = bean.get("fieldPrefix") + "_" + result;
//			}
//			if (bean.get("fieldSufix") != null && !bean.get("fieldSufix").isEmpty()
//					&& !result.endsWith(bean.get("fieldSufix"))) {
//				result += "_" + bean.get("fieldSufix");
//			}
//		}
//
//		return result;
//	}
//
//	public String getPkValueOfContext(Map<String, String> bean, boolean loadMaster) {
//		String result = "";
//		if (loadMaster) {
//			try {
//				bean = (HashMap<String, String>) Collections
//						.unmodifiableList(
//								(List) user.getSysParser()
//										.getListValuesFromXQL("/systems/system[@name='" + SYSTEM
//												+ "']/module/bean[@id='" + bean.get("master") + "']", "order", user,
//												true, false))
//						.get(0);
//			} catch (Exception e) {
//				LogUtil.exception(e, user);
//			}
//		}
//		try {
//			String pkName = ((GenericService) util.getServiceById(bean.get("id"), user)).getPrimaryKey().get("name")
//					.toString();
//			waitLoadItemByName(pkName, 15);
//			for (WebElement el : driver.findElements(By.ByXPath.xpath("//input[@name='" + pkName + "' ]"))) {
//				if (!el.getAttribute("value").isEmpty()) {
//					result = el.getAttribute("value");
//				}
//			}
//		} catch (Exception e) {
//		}
//
//		return (result.isEmpty() ? "*" : result);
//	}
//
//	public void simClick() {
//		getJavaScriptExecutor()
//				.executeScript("jQuery('button:contains(\"" + user.getLabel("label.sim") + "\")').click()");
//	}
//
//	public void naoClick() {
//		getJavaScriptExecutor()
//				.executeScript("jQuery('button:contains(\"" + user.getLabel("label.nao") + "\")').click()");
//	}
//
//	public void analiseAndSetValueInPropertyFields(Map<String, String> context, Map<String, String> method,
//			ServiceMethod sm) {
//
//		// analisa e insere valores em campos do contexto, navega pelas abas, e
//		// chama outro mtodo
//		// analiseAndSetValueInPropertyFieldsOfContext' para resolver campos
//		// context.
//
//		List<Map<String, String>> items = null;
//		if (sm == ServiceMethod.METHOD) {
//			items = configSisAux.getPropertysOfMethod(context, method.get("name"), true);
//		} else {
//			items = configSisAux.getItemsOfBean(context);
//		}
//
//		for (Map<String, String> prop : items) {
//			if (configSisAux.canAcess(prop)) {
//				try {
//					if ((fieldStateIsValid(context, null, prop, sm) || fieldStateIsValid(context, method, prop, sm))
//							&& !prop.get("type").equals("openTab")
//							&& !prop.get("component").equals(PropertyComponents.CONTEXT.value)) {
//						try {
//							if (sm == ServiceMethod.METHOD) {
//								setValueInFieldUi(context, method, prop, sm);
//							} else {
//								setValueInFieldUi(context, null, prop, sm);
//							}
//						} catch (Exception e) {
//							continue;
//						}
//					} else if (prop.get("type") != null && prop.get("type").equals("openTab")) {
//						try {
//							driver.findElement(
//									By.ById.id(getIdentifier(context, null, prop, CmpToIdentify.TAB_INSERT_UPDATE, sm)))
//									.click();
//						} catch (Exception e) {
//							continue;
//						}
//					} else if (prop.get("component") != null
//							&& prop.get("component").equals(PropertyComponents.CONTEXT.value)) {
//						if (sm == ServiceMethod.METHOD) {
//							putFieldPrefix(context, prop.get("key"));
//							context.put("fieldSufix", method.get("name"));
//						}
//						analiseAndSetValueInPropertyFieldsOfContext(context, prop, method, sm);
//						context.remove("fieldPrefix");
//						context.remove("fieldSufix");
//					}
//				} catch (Exception e) {
//					continue;
//				}
//			}
//		}
//	}
//
//	public void analiseAndSetValueInPropertyFieldsOfContext(Map<String, String> bean, Map<String, String> propContext,
//			Map<String, String> method, ServiceMethod sm) {
//		// esse mtodo separado do outro ( analiseAndSetValueInPropertyFields ) ,
//		// pois ele deve concatenar os prefixos dos campos, para poder conseguir
//		// identifica-los, aqui inserido valores, em
//		// uma tela j aberta que contm campos que vm de um context, e se esse
//		// context tb tem um context, ele vai concatenando o prefix, para gerar
//		// o mtodo getIdentifier
//		// retornar as ids, corretamente.
//		List<Map<String, String>> cItems = configSisAux.getPropsOfContext(propContext.get("service"));
//		// String atualTab = "";
//		for (Map<String, String> cProp : cItems) {
//			if (!cProp.get("type").equals("openTab") && cProp.get("component") != null
//					&& !cProp.get("component").equals(PropertyComponents.CONTEXT.value)
//					&& fieldStateIsValid(bean, propContext, cProp, sm)) {
//				setValueInFieldUi(bean, propContext, cProp, sm);
//			} else if (cProp.get("type") != null && cProp.get("type").equals("openTab")
//					|| cProp.get("type") != null && cProp.get("type").equals("closeTab")) {
//				bean.put("atualTab", cProp.get("name"));
//				putFieldPrefix(bean, cProp.get("name"));
//				bean.put("fieldPrefix", bean.get("fieldPrefix").replaceAll("__", "_"));
//				try {
//					driver.findElement(
//							By.ById.id(getIdentifier(bean, propContext, cProp, CmpToIdentify.TAB_INSERT_UPDATE, sm)))
//							.click();
//				} catch (Exception e) {
//				}
//				removeFieldPrefix(bean, cProp.get("name"));
//			} else if (cProp.get("component") != null
//					&& cProp.get("component").equals(PropertyComponents.CONTEXT.value)) {
//				putFieldPrefix(bean, cProp.get("name"));
//				analiseAndSetValueInPropertyFieldsOfContext(bean, cProp, method, sm);
//				removeFieldPrefix(bean, cProp.get("name"));
//				bean.remove("atualTab");
//			}
//		}
//	}
//
//	public void putFieldPrefix(Map<String, String> bean, String value) {
//		// vai concatenando o prefixo de um field, para identificar os campos,
//		// quando h recurso.
//		// Por exemplo, quando um Bean que tem uma property='context' chamando
//		// outro bean,
//		// e esse outro bean tambm tem um context, e assim por diante.
//		String oldPrefix = "";
//		if (bean.get("fieldPrefix") != null && !bean.get("fieldPrefix").isEmpty()) {
//			oldPrefix = bean.get("fieldPrefix");
//			bean.put("fieldPrefix", oldPrefix + "_" + value);
//		} else {
//			bean.put("fieldPrefix", value);
//		}
//	}
//
//	public void removeFieldPrefix(Map<String, String> bean, String value) {
//		if (bean.get("fieldPrefix") != null) {
//			String oldPrefix = bean.get("fieldPrefix");
//			oldPrefix = oldPrefix.replace(value, "");
//			if (oldPrefix.endsWith("_")) {
//				oldPrefix = oldPrefix.substring(0, oldPrefix.lastIndexOf("_"));
//			}
//			if (oldPrefix.contains("__")) {
//				oldPrefix = oldPrefix.substring(0, oldPrefix.lastIndexOf("__") + 1);
//			}
//			bean.put("fieldPrefix", oldPrefix);
//		}
//	}
//
//	public void setValueInFieldUi(Map<String, String> context, Map<String, String> propContext,
//			Map<String, String> prop, ServiceMethod sm) {
//		int testIndex = (context.get("testIndex") == null ? -1 : Integer.parseInt(context.get("testIndex")));
//		String value = "";
//		try {
//			if (propContext == null) {
//				value = testConfig.getValueToField(context, prop, sm, testIndex);
//			} else if (propContext.get("xtype").equals("property")) {
//				value = testConfig.getValueToField(
//						configSisAux.getBeanByName(propContext.get("service").replaceAll("Service", "")), prop, sm,
//						testIndex);
//			} else if (propContext.get("xtype").equals("method")) {
//				context.put("methodName", propContext.get("name"));
//				value = testConfig.getValueToField(context, prop, sm, testIndex);
//			}
//
//			if (prop.get("component").equals(PropertyComponents.SELECT.value) && value != null && !value.isEmpty()
//					&& prop.get("service") != null && !prop.get("service").equals(context.get("name") + "Service")) {
//				// Faz um select no banco com o filtro definido, para obter a
//				// id, para inserir no component select.
//				GenericService pServ = (GenericService) getService(prop.get("service"), superUser);
//				GenericVO pVO = pServ.createVO(superUser);
//				pVO.setFiltro(value);
//				value = String.valueOf(pServ.obtem(pVO, superUser).get(pServ.getPrimaryKey().get("name")));
//			}
//
//		} catch (Exception e) {
//		}
//
//		if (prop.get("type").equals(PropertyTypes.DATE.value)
//				&& !prop.get("component").equals(PropertyComponents.SELECT.value)) {
//			setDateValue(context, propContext, prop, sm, value);
//		} else if ((prop.get("type").equals(PropertyTypes.ALFA.value) || prop.get("type").equals("text"))
//				&& (prop.get("component").equals(PropertyTypes.ALFA.value)
//						|| prop.get("component").equals(PropertyComponents.MEMO.value))) {
//
//			if (prop.get("maxlength") == null) {
//				setTextValue(context, propContext, prop, sm, value);
//			} else {
//				if (!value.isEmpty()) {
//					setTextValue(context, propContext, prop, sm,
//							(value.length() > Integer.parseInt(prop.get("maxlegth").toString()))
//									? value.substring(0, Integer.parseInt(prop.get("maxlength").toString())) : value);
//				} else {
//					setTextValue(context, propContext, prop, sm,
//							(ValuesToFields.TEXT.values[0].length() > Integer
//									.parseInt(prop.get("maxlength").toString()))
//											? ValuesToFields.TEXT.values[0].substring(0,
//													Integer.parseInt(prop.get("maxlength").toString()))
//											: ValuesToFields.TEXT.values[0]);
//				}
//			}
//
//		} else if (prop.get("component").equals(PropertyComponents.SELECT.value)) {
//			selectComboItem(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm), sm, value);
//		} else if (prop.get("component").equals(PropertyComponents.RADIO_GROUP.value)) {
//			selectRadioGroupItem(context, prop, sm, value, propContext);
//		} else if (prop.get("component").equals(PropertyComponents.CHECK_BOX.value)) {
//			selectCheckItem(context, prop, false, -1, sm, value);
//		} else if (prop.get("component").equals(PropertyComponents.TIME.value)
//				|| prop.get("type").equals(PropertyTypes.TIME.value)) {
//			setTimeValue(context, propContext, prop, sm, value);
//		} else if (prop.get("type").equals(PropertyTypes.FILE.value)) {
//			driver.findElement(By.ById.id(getIdentifier(context, propContext, prop, CmpToIdentify.FIELD, sm)))
//					.sendKeys("C:\\Windows\\System32\\aaclient.dll");
//		} else if (prop.get("type").equals(PropertyTypes.NUM.value)) {
//			setNumValue(context, propContext, prop, sm, value);
//		} else if (prop.get("component").equals(PropertyTypes.MONEY.value)) {
//			setMoneyValue(context, propContext, prop, sm, value);
//		} else if (prop.get("component").equals(PropertyTypes.LIST.value)) {
//			dropFromGridToGrid(context, propContext, prop, sm, value);
//		}
//	}
//
//	public String getResultOfTestFunction(Map<String, String> bean, String func) {
//		try {
//			ScriptContext ctx = new SimpleScriptContext();
//
//			final Bindings bindings = ctx.getBindings(ScriptContext.ENGINE_SCOPE);
//			bindings.put("ID_REGISTRO", getPkValueOfContext(bean, false));
//			bindings.put("sm", ((GenericService) getServiceById(bean.get("id"), user)));
//			bindings.put("VO", ((GenericService) getServiceById(bean.get("id"), user)).createVO(user));
//			bindings.put("user", user);
//			bindings.put("superUser", superUser);
//			bindings.put("util", util);
//			bindings.put("df", user.getUtil().df);
//			bindings.put("sysNavi", this);
//			bindings.put("configSisAux", configSisAux);
//			bindings.put("bean", bean);
//
//			Object res = null;
//			try {
//				res = scriptEngine.eval("importClass(Packages." + this.getClass().getCanonicalName() + ");" + func,
//						ctx);
//				if (res != null && res.getClass().equals(String.class)) {
//					return res.toString();
//				}
//			} catch (ScriptException e) {
//				LogUtil.exception(e, user);
//			}
//		} catch (Exception ex) {
//			LogUtil.exception(ex, user);
//		}
//		return "";
//
//	}
//
//	public String waitLoadingModalHide() {
//		String okMsg = "";
//		try {
//			while (driver.findElement(By.ByXPath.xpath("//*[text()='" + user.getLabel("label.carregando") + "']"))
//					.isDisplayed()
//					&& driver.findElement(By.ByXPath.xpath("//*[text()='" + user.getLabel("label.carregando") + "']"))
//							.isEnabled()) {
//				try {
//					if (okMsg.isEmpty() || okMsg.equals("#")) {
//						okMsg = okClick();
//					} else {
//						okClick();
//					}
//				} catch (Exception e) {
//					try {
//						driver.findElement(By.ByXPath.xpath("//*[text()='" + user.getLabel("label.carregando") + "']"))
//								.isDisplayed();
//					} catch (Exception ex) {
//						break;
//					}
//				}
//			}
//
//		} catch (Exception e) {
//		}
//
//		if (okMsg != null && okMsg.isEmpty() || okMsg.equals("#")) {
//			okMsg = okClick();
//		} else {
//			okClick();
//		}
//		if (okMsg == null) {
//			okMsg = "";
//		}
//		return okMsg;
//	}
//
//	public void testAdvancedSearch(Map<String, String> context) {
//
//		List<Map<String, String>> properties = util.filter(configSisAux.getItemsOfBean(context),
//				DataBaseInitializator.insertFieldsFilter[0], DataBaseInitializator.insertFieldsFilter[1]);
//		properties.addAll(configSisAux.getAllPropertysOfAllMethods(context));
//
//		waitLoadingModalHide();
//
//		// Click no boto 'Busca avanada' para iniciar a busca avanada
//		driver.findElement(By.ById.id(getIdentifier(context, null, null, CmpToIdentify.B_ADVANCED_SEARCH, null)))
//				.click();
//		try {
//			waitLoadItemById("itemFakeToFoceWait", 1);
//		} catch (Exception e) {
//		}
//		;
//
//		// Num
//		testFieldsInAdvancedSearch(configSisAux.filterProperties(properties,
//				new String[] { "type", "component", "component" }, new String[] { "num", "!select", "!context" }),
//				context, PropertyTypes.NUM.value);
//		// CheckBox - boolean
//		testFieldsInAdvancedSearch(configSisAux.filterProperties(properties, new String[] { "type", "component" },
//				new String[] { "bit", "checkbox" }), context, PropertyTypes.BIT.value);
//		// Date
//		testFieldsInAdvancedSearch(
//				configSisAux.filterProperties(properties, new String[] { "type" }, new String[] { "date" }), context,
//				PropertyTypes.DATE.value);
//		// fields de relacionamento normalmente com lts
//		testFieldsInAdvancedSearch(configSisAux.filterProperties(properties, new String[] { "type", "component" },
//				new String[] { "num", "select" }), context, PropertyComponents.SELECT.value);
//		// Money
//		testFieldsInAdvancedSearch(
//				configSisAux.filterProperties(properties, new String[] { "type" }, new String[] { "money" }), context,
//				PropertyTypes.MONEY.value);
//		// radioGroups
//		testFieldsInAdvancedSearch(
//				configSisAux.filterProperties(properties, new String[] { "component" },
//						new String[] { PropertyComponents.RADIO_GROUP.value }),
//				context, PropertyComponents.SELECT.value);
//		// Contexts
//		testFieldsInAdvancedSearch(configSisAux.filterProperties(properties, new String[] { "component" },
//				new String[] { PropertyComponents.CONTEXT.value }), context, PropertyComponents.SELECT.value);
//
//		waitLoadingModalHide();
//
//	}
//
//	public void testFieldsInAdvancedSearch(List<Map<String, String>> fields, Map<String, String> context,
//			String opType) {
//		String idReg = "*";
//		if (context.get("type").equals("D")) {
//			idReg = getPkValueOfContext(context, true);
//		}
//		for (Map<String, String> f : fields) {
//			if (f.get("component") != null && f.get("component").equals(PropertyComponents.CONTEXT.value)
//					&& f.get("labelValue") == null) {
//				// so props context que no possuem labelValue, ou seja no
//				// aparecem diretamente no detalhe do contexto master.
//				continue;
//			}
//			if (comboSelectFieldAdvancedSearchClick(context, f)) {
//				if (opType.equals(PropertyTypes.BIT.value)) {
//					// Testa campo boolean com T
//					String pv = setOrSelectItemInValueOfAdvancedSearch(f, context, "T", null);
//					buscarAdvancedSearchClick(context);
//					validaResultOfAdvancedSearch(context, f, pv, OperadoresFiltro.E_IGUAL_A);
//					waitLoadingModalHide();
//					// Testa campo boolean com F
//					pv = setOrSelectItemInValueOfAdvancedSearch(f, context, "F", null);
//					buscarAdvancedSearchClick(context);
//					validaResultOfAdvancedSearch(context, f, pv, OperadoresFiltro.E_IGUAL_A);
//				} else {
//					// for(int i=0;i<1;i++){
//					for (OperadoresFiltro op : configSisAux.getOperadoresOfType(opType)) {
//						String pv = "";
//						selectOperadorAdvancedSearch(op, context, idReg);
//						if (!configSisAux.isOperatorValue(op)) {
//							pv = setOrSelectItemInValueOfAdvancedSearch(f, context, null, op);
//							buscarAdvancedSearchClick(context);
//						}
//						validaResultOfAdvancedSearch(context, f, pv, op);
//					}
//				}
//			} else {
//				continue;
//			}
//		}
//	}
//
//	public List<WebElement> getRowsOfGrid(Map<String, String> bean) {
//		return driver.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.GRID_LIST, null)))
//				.findElements(By.ByClassName.className("x-grid3-row"));
//	}
//
//	public void validaResultOfAdvancedSearch(Map<String, String> bean, Map<String, String> fieldSel, String value,
//			OperadoresFiltro op) {
//		boolean result = false;
//		waitLoadingModalHide();
//		List<WebElement> gridRows = getRowsOfGrid(bean);
//
//		r.appendItemList("Validando o Filtro: " + fieldSel.get("name").toLowerCase() + " " + op.toString().toLowerCase()
//				+ " " + value + ".", null, "");
//
//		if (gridRows.isEmpty()) {
//			r.appendItemList("O Filtro no teve resultados.", SeleniumReport.FontColor.BLUE, "");
//		} else {
//			for (int i = 0; i < gridRows.size(); i++) {
//				// for(int i=0; i<1; i++){ // para visualizar apenas o primeiro
//				// item.
//				tabGridListClick(bean);
//				testDetail(bean, i);
//				String valueFieldDetail = "";
//
//				try {
//					WebElement fieldElDetail = driver.findElement(
//							By.ById.id(getIdentifier(bean, null, fieldSel, CmpToIdentify.FIELD_IN_DETAIL, null)));
//
//					try {
//						valueFieldDetail = fieldElDetail.findElement(By.ByClassName.className("detalhe_valor"))
//								.getText();
//					} catch (StaleElementReferenceException ex) {
//						fieldElDetail = driver.findElement(
//								By.ById.id(getIdentifier(bean, null, fieldSel, CmpToIdentify.FIELD_IN_DETAIL, null)));
//						valueFieldDetail = fieldElDetail.findElement(By.ByClassName.className("detalhe_valor"))
//								.getText();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//				} catch (NoSuchElementException e) {
//					valueFieldDetail = " No foi possvel verificar o valor do campo.";
//				}
//
//				// checkBox
//				if (fieldSel.get("type").equals(PropertyTypes.BIT.value) && fieldSel.get("component") != null
//						&& fieldSel.get("component").equals(PropertyComponents.CHECK_BOX.value)) {
//
//					if (value.equals("T")) {
//						result = valueOfDetailIsValid(op, user.getLabel("label.sim"), valueFieldDetail, fieldSel);
//					} else if (value.equals("F")) {
//						result = valueOfDetailIsValid(op, user.getLabel("label.nao"), valueFieldDetail, fieldSel);
//					}
//					// Outros componenetes
//				} else {
//					result = valueOfDetailIsValid(op, value, valueFieldDetail, fieldSel);
//				}
//
//				if (result) {
//					r.appendItemList("Registro vlido de acordo com o filtro.", SeleniumReport.FontColor.GREEN, "");
//				} else {
//					r.appendItemList("Registro invlido de acordo com o filtro. Valor do registro:" + valueFieldDetail,
//							SeleniumReport.FontColor.RED, "");
//				}
//				tabNavigateClose(null, bean, CmpToIdentify.TAB_DETAIL_MASTER, false);
//			}
//		}
//
//	}
//
//	public boolean valueOfDetailIsValid(OperadoresFiltro op, String vFiltro, String vField, Map<String, String> field) {
//		boolean result = false;
//
//		GregorianCalendar gcField = null;
//		GregorianCalendar gcFiltro = null;
//		GregorianCalendar dataAtual = new GregorianCalendar();
//		dataAtual.setTime(new Date());
//
//		if (field.get("type").equals(PropertyTypes.DATE.value)) {
//			// vField tem que ser maior que vFiltro
//			gcField = new GregorianCalendar(Integer.parseInt(vField.split("/")[2]),
//					Integer.parseInt(vField.split("/")[1]) - 1, Integer.parseInt(vField.split("/")[0]));
//			if (!vFiltro.isEmpty() && !vFiltro.contains("#")) {
//				gcFiltro = new GregorianCalendar(Integer.parseInt(vFiltro.split("/")[2]),
//						Integer.parseInt(vFiltro.split("/")[1]) - 1, Integer.parseInt(vFiltro.split("/")[0]));
//			}
//		}
//
//		switch (op) {
//		case COMECA_COM:
//			if (vField.startsWith(vFiltro)) {
//				result = true;
//			}
//			break;
//		case CONTEM:
//			if (vField.contains(vFiltro)) {
//				result = true;
//			}
//			break;
//		case E_NULO:
//			if (vField.isEmpty()) {
//				result = true;
//			}
//			break;
//		case NAO_NULO:
//			if (!vField.isEmpty()) {
//				result = true;
//			}
//			break;
//		case NAO_CONTEM:
//			if (!vField.contains(vFiltro)) {
//				result = true;
//			}
//			break;
//		case E_IGUAL_A:
//			if (field.get("type").equals(PropertyTypes.DATE.value)) {
//				result = gcField.equals(gcFiltro);
//			} else {
//				if (vFiltro.equals(vField)) {
//					result = true;
//				}
//			}
//			break;
//		case E_DIFERENTE_DE:
//			if (field.get("type").equals(PropertyTypes.DATE.value)) {
//				result = !gcField.equals(gcFiltro);
//			} else {
//				if (!vFiltro.equals(vField)) {
//					result = true;
//				}
//			}
//			break;
//		case ESTA_ENTRE:
//			GregorianCalendar fd1 = new GregorianCalendar(Integer.parseInt(vFiltro.split("#")[0].split("/")[2]),
//					Integer.parseInt(vFiltro.split("#")[0].split("/")[1]) - 1,
//					Integer.parseInt(vFiltro.split("#")[0].split("/")[0]));
//
//			GregorianCalendar fd2 = new GregorianCalendar(Integer.parseInt(vFiltro.split("#")[1].split("/")[2]),
//					Integer.parseInt(vFiltro.split("#")[1].split("/")[1]) - 1,
//					Integer.parseInt(vFiltro.split("#")[1].split("/")[0]));
//
//			if ((gcField.after(fd1) || gcField.equals(fd1)) && (gcField.before(fd2) || gcField.equals(fd2))) {
//				result = true;
//			}
//
//			break;
//		case E_MAIOR_QUE:
//			if (field.get("type").equals(PropertyTypes.DATE.value)) {
//				result = gcField.after(gcFiltro);
//			} else {
//				if (vField.compareTo(vFiltro) > 0) {
//					result = true;
//				}
//			}
//			break;
//		case E_MENOR_QUE:
//			if (field.get("type").equals(PropertyTypes.DATE.value)) {
//				result = gcField.before(gcFiltro);
//			} else {
//				if (vFiltro.compareTo(vField) > 0) {
//					result = true;
//				}
//			}
//			break;
//		case ANO_ANTERIOR:
//			result = dataAtual.get(Calendar.YEAR) - 1 == gcField.get(Calendar.YEAR);
//			break;
//		case ANO_ATUAL:
//			result = dataAtual.get(Calendar.YEAR) == gcField.get(Calendar.YEAR);
//			break;
//		case ESTA_NO_FUTURO:
//			result = dataAtual.before(gcField);
//			break;
//		case ESTA_NO_PASSADO:
//			result = dataAtual.after(gcField);
//			break;
//		case ULTIMOS_60_DIAS:
//			if (daysIntervalInDates(dataAtual, gcField) >= -60) {
//				result = true;
//			}
//			break;
//		case ULTIMOS_360_DIAS:
//			if (daysIntervalInDates(dataAtual, gcField) >= -360) {
//				result = true;
//			}
//			break;
//		case ULTIMOS_30_DIAS:
//			if (daysIntervalInDates(dataAtual, gcField) >= -30) {
//				result = true;
//			}
//			break;
//		case ULTIMOS_180_DIAS:
//			if (daysIntervalInDates(dataAtual, gcField) >= -180) {
//				result = true;
//			}
//			break;
//		case SEMANA_PASSADA:
//			// volta at o ultimo domingo
//			while (dataAtual.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
//				dataAtual.add(Calendar.DAY_OF_MONTH, -1);
//			}
//			GregorianCalendar dateAux = new GregorianCalendar(dataAtual.get(Calendar.YEAR),
//					dataAtual.get(Calendar.MONTH), dataAtual.get(Calendar.DAY_OF_MONTH));
//			// retorna 7 dias...
//			dateAux.add(Calendar.DAY_OF_WEEK, -7);
//			if ((dateAux.before(gcField) || dateAux.equals(gcField))
//					&& (dataAtual.after(gcField) || dataAtual.equals(gcField))) {
//				result = true;
//			}
//			break;
//		case PROXIMOS_60_DIAS:
//			if (daysIntervalInDates(dataAtual, gcField) <= 60) {
//				result = true;
//			}
//			break;
//		case PROXIMOS_360_DIAS:
//			if (daysIntervalInDates(dataAtual, gcField) <= 360) {
//				result = true;
//			}
//			break;
//		case PROXIMOS_30_DIAS:
//			if (daysIntervalInDates(dataAtual, gcField) <= -30) {
//				result = true;
//			}
//			break;
//		case PROXIMOS_180_DIAS:
//			if (daysIntervalInDates(dataAtual, gcField) <= 180) {
//				result = true;
//			}
//			break;
//		case PROXIMO_ANO:
//			result = dataAtual.get(Calendar.YEAR) + 1 == gcField.get(Calendar.YEAR);
//			break;
//		case ONTEM:
//			result = dataAtual.get(Calendar.DAY_OF_MONTH) - 1 == gcField.get(Calendar.DAY_OF_MONTH);
//			break;
//		case NESTA_SEMANA:
//			break;
//		case HOJE:
//			result = dataAtual.equals(gcField);
//			break;
//		case MES_ATUAL:
//			result = dataAtual.get(Calendar.MONTH) == gcField.get(Calendar.MONTH);
//			break;
//		case MES_PASSADO:
//			result = dataAtual.get(Calendar.MONTH) - 1 == gcField.get(Calendar.YEAR);
//			break;
//		case MES_PROXIMO:
//			result = dataAtual.get(Calendar.MONTH) + 1 == gcField.get(Calendar.YEAR);
//			break;
//
//		}
//		return result;
//	}
//
//	static final long ONE_HOUR = 60 * 60 * 1000L;
//
//	public long daysIntervalInDates(GregorianCalendar atual, GregorianCalendar date) {
//		return ((date.getTime().getTime() - atual.getTime().getTime() + ONE_HOUR) / (ONE_HOUR * 24));
//	}
//
//	public boolean comboSelectFieldAdvancedSearchClick(Map<String, String> context, Map<String, String> field) {
//		buscarAdvancedSearchClick(context);
//		try {
//			driver.findElement(By.ById.id(getIdentifier(context, null, null, CmpToIdentify.FILTRO_TREE_PANEL, null)))
//					.findElement(By.ByClassName.className("filtro_campo")).click();
//			try {
//				waitLoadItemById("itemFakeToFoceWait", 1);
//			} catch (Exception e) {
//			}
//			;
//			if (field != null) {
//				return selectFieldInAdvancedSearch(field, context);
//			}
//		} catch (Exception e) {
//			buscarAdvancedSearchClick(context);
//			return false;
//		}
//		return true;
//	}
//
//	public void selectOperadorAdvancedSearch(OperadoresFiltro op, Map<String, String> context, String idReg) {
//		waitLoadingModalHide();
//		driver.findElement(By.ById.id(getIdentifier(context, null, null, CmpToIdentify.FILTRO_TREE_PANEL, null)))
//				.findElement(By.ByXPath.xpath("//span[@id!='g-ID-nodeFILTRO" + SYSTEM + "/" + context.get("name")
//						+ "VO_" + idReg + "_lista_tree' and @class='filtro_operador']"))
//				.click();
//		driver.findElement(By.ByXPath.xpath("//*[text() = '" + user.getLabel(op.getValue()) + "']")).click();
//
//	}
//
//	public boolean selectFieldInAdvancedSearch(Map<String, String> field, Map<String, String> bean) {
//		List<WebElement> comboFields = driver.findElements(By.ByName.name("item_filtro"));
//		String fieldLabel = user.getLabel(bean.get("table") + "." + field.get("name"));
//		for (WebElement el : comboFields) {
//			if (el.getText().equals(fieldLabel)) {
//				el.click();
//				return true;
//			}
//		}
//		return false;
//	}
//
//	public void printScreen(String path) {
//		new WebDriverBackedSelenium(driver, URL).captureScreenshot(path);
//	}
//
//	public String setOrSelectItemInValueOfAdvancedSearch(Map<String, String> field, Map<String, String> bean,
//			String definedValue, OperadoresFiltro op) {
//		String placedValue = "";
//
//		// recupera um valor definido para o campo+operador
//		if (testConfig.getValueToFieldInAdvancedSearch(bean, field, op) != null
//				&& !testConfig.getValueToFieldInAdvancedSearch(bean, field, op).isEmpty()) {
//			if (field.get("component").equals(PropertyComponents.SELECT.value)
//					&& !field.get("service").equals(bean.get("name") + "Service")) {
//				placedValue = String.valueOf(
//						dataBaseInit.getIdByFilter(testConfig.getValueToFieldInAdvancedSearch(bean, field, op), field));
//			} else {
//				placedValue = testConfig.getValueToFieldInAdvancedSearch(bean, field, op);
//			}
//		} else {
//			// tenta carregar o item definido em preparationScript
//			try {
//				if (field.get("component").equals(PropertyComponents.SELECT.value)
//						&& !field.get("service").equals(bean.get("name") + "Service")) {
//					placedValue = String.valueOf(dataBaseInit
//							.getIdByFilter(placedValue = ((NamedNodeMap) testConfig.getPreparationScript(bean.get("id"),
//									field.get("name"), false)).getNamedItem("filter").getNodeValue(), field));
//				} else {
//					placedValue = ((NamedNodeMap) testConfig.getPreparationScript(bean.get("id"), field.get("name"),
//							false)).getNamedItem("filter").getNodeValue();
//				}
//
//			} catch (Exception e) {
//			}
//		}
//
//		// campos select e context
//		if (field.get("component") != null && field.get("component").equals(PropertyComponents.CONTEXT.value)
//				|| field.get("component").equals(PropertyComponents.SELECT.value)) {
//			if (definedValue != null && !definedValue.isEmpty() || placedValue != null && !placedValue.isEmpty()) {
//				if (definedValue != null && !definedValue.isEmpty()) {
//					placedValue = definedValue;
//				}
//				setValueInAdvancedSearch(placedValue, SeachValueFields.V1, bean);
//			} else if (field.get("component").equals(PropertyComponents.CONTEXT.value)) {
//				List<Map<String, String>> propCtx = configSisAux.getPropsOfContext(field.get("service"));
//				// procura a property no bean que est em service da property
//				// context
//				for (Map<String, String> p : propCtx) {
//					if (field.get("labelValue").equals(p.get("name"))) {
//						placedValue = setOrSelectItemInValueOfAdvancedSearch(p, bean, "", op);
//						break;
//					}
//				}
//				// Procura a property nos methods
//				if (placedValue.isEmpty()) {
//					List<Map<String, String>> mts = configSisAux
//							.getBeanMethods(field.get("service").replaceAll("Service", ""), false, "[@type != 'L']");
//					for (Map<String, String> m : mts) {
//						if (!placedValue.isEmpty()) {
//							break;
//						}
//						List<Map<String, String>> mtProps = configSisAux.getPropertysOfMethod(
//								field.get("service").replaceAll("Service", ""), m.get("name"), false);
//						for (Map<String, String> mtp : mtProps) {
//							if (mtp.get("name").equals(field.get("labelValue"))) {
//								placedValue = setOrSelectItemInValueOfAdvancedSearch(mtp, bean, "", op);
//								break;
//							}
//						}
//					}
//				}
//			} else {
//				GenericService gServ = (GenericService) getService(field.get("service"), superUser);
//				GenericVO gVO = gServ.createVO(superUser);
//				List<GenericVO> list = null;
//				if (field.get("filtro") != null && !field.get("filtro").isEmpty()) {
//					gVO.setFiltro(field.get("filtro"));
//				}
//				try {
//					list = gServ.lista(gVO, superUser);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				if (list.size() > 0) {
//					placedValue = "" + list.get(0).get(field.get("labelValue"));
//					setValueInAdvancedSearch(placedValue, SeachValueFields.V1, bean);
//				}
//			}
//			// Campos Bit ou boolean
//		} else if (field.get("type").equals(PropertyTypes.BIT.value)) {
//			placedValue = definedValue;
//			getJavaScriptExecutor().executeScript(
//					"Ext.getCmp('" + getIdentifier(bean, null, null, CmpToIdentify.FILTRO_TREE_PANEL, null)
//							+ "').getRootNode().childNodes[0].attributes.o = '" + placedValue + "'");
//		} else {
//			// outros campos
//			if (field.get("type").equals(PropertyTypes.DATE.value) && op == OperadoresFiltro.ESTA_ENTRE) {
//				if (placedValue != null && !placedValue.isEmpty() && placedValue.contains("#")) {
//					String d1 = placedValue.split("#")[0];
//					String d2 = placedValue.split("#")[1];
//					setValueInAdvancedSearch(d1, SeachValueFields.V1, bean);
//					setValueInAdvancedSearch(d2, SeachValueFields.V2, bean);
//				} else {
//					setValueInAdvancedSearch(ValuesToFields.DATA.values[0], SeachValueFields.V1, bean);
//					setValueInAdvancedSearch(ValuesToFields.DATA.values[1], SeachValueFields.V2, bean);
//				}
//			} else if (field.get("component").equals(PropertyComponents.RADIO_GROUP.value)) {
//				if (placedValue == null || placedValue.isEmpty()) {
//					// aqui no utilizado getValueToField, pq eu preciso da Key
//					// pra busca e do value pra validao.
//					Iterator itr = configSisAux.getValuesOfMethodList(bean, field).entrySet().iterator();
//					Entry<String, Object> obj = (Entry<String, Object>) itr.next();
//					placedValue = user.getLabel((String) obj.getValue());
//					setValueInAdvancedSearch(obj.getKey(), SeachValueFields.VC, bean);
//				} else {
//					setValueInAdvancedSearch(placedValue, SeachValueFields.VC, bean);
//				}
//			} else {
//				if (definedValue != null && !definedValue.isEmpty()) {
//					placedValue = definedValue;
//				} else {
//					placedValue = dataBaseInit.getValueToField(bean, field);
//				}
//				setValueInAdvancedSearch(placedValue, SeachValueFields.V1, bean);
//			}
//		}
//		return placedValue;
//	}
//
//	public void setValueInAdvancedSearch(String value, SeachValueFields sv, Map<String, String> bean) {
//		if (sv == SeachValueFields.V1) {
//			getJavaScriptExecutor().executeScript(
//					"Ext.getCmp('" + getIdentifier(bean, null, null, CmpToIdentify.FILTRO_TREE_PANEL, null)
//							+ "').getRootNode().childNodes[0].attributes.v1 = '" + value + "'");
//		} else if (sv == SeachValueFields.V2) {
//			getJavaScriptExecutor().executeScript(
//					"Ext.getCmp('" + getIdentifier(bean, null, null, CmpToIdentify.FILTRO_TREE_PANEL, null)
//							+ "').getRootNode().childNodes[0].attributes.v2 = '" + value + "'");
//		} else if (sv == SeachValueFields.VC) {
//			getJavaScriptExecutor().executeScript(
//					"Ext.getCmp('" + getIdentifier(bean, null, null, CmpToIdentify.FILTRO_TREE_PANEL, null)
//							+ "').getRootNode().childNodes[0].attributes.vc = '" + value + "'");
//		}
//	}
//
//	public enum SeachValueFields {
//		V1, V2, VC;
//	}
//
//	public void buscarAdvancedSearchClick(Map<String, String> context) {
//		try {
//			driver.findElement(By.ById.id(getIdentifier(context, null, null, CmpToIdentify.B_BUSCAR, null))).click();
//			waitLoadingModalHide();
//		} catch (Exception e) {
//		}
//	}
//
//	public void testDetailBeans(Map<String, String> context, List<Map<String, String>> beans) {
//		try {
//			for (Map<String, String> bean : beans) {
//				if (configSisAux.beanIsValidToDefaultCrudTest(bean) && testConfig.isToTestThisBean(context, bean)) {
//					if (testConfig.getCrudTestQtd(bean) == -1) {
//						if (tabDetailNaviClick(context, bean)) {
//							testDetailBean(context, bean);
//						}
//					} else {
//						for (int i = 0; i < testConfig.getCrudTestQtd(bean); i++) {
//							bean.put("testIndex", "" + i);
//							if (tabDetailNaviClick(context, bean)) {
//								testDetailBean(context, bean);
//							}
//						}
//					}
//				}
//			}
//		} catch (Exception e) {
//			LogUtil.exception(e, user);
//		}
//	}
//
//	public void testDetailBean(Map<String, String> master, Map<String, String> bean) {
//		if (!executeTestScript(bean, null, null, SeleniumTestConfig.ScriptTypes.NAVIGATION_SCRIPT)) {
//			try {
//				r.initLogBean(bean);
//				testCrudOfContext(bean, master);
//			} catch (Exception e) {
//				// LogUtil.exception(e, user);
//				waitLoadingModalHide();
//			}
//			waitLoadingModalHide();
//		}
//	}
//
//	public void waitForTabResumoClickAndShow(Map<String, String> bean) {
//		try {
//			long initWhile = new Date().getTime();
//			while (!driver
//					.findElement(By.ById.id(getIdentifier(bean, null, null, CmpToIdentify.TAB_RESUMO_CONTENT, null)))
//					.isDisplayed()) {
//				if (((new Date().getTime() - initWhile) / 1000) > 10) {
//					break;
//				}
//				int a = 1 + 1;
//			}
//
//		} catch (Exception e) {
//		}
//	}
//
//	public boolean tabDetailNaviClick(Map<String, String> context, Map<String, String> bean) {
//		try {
//			driver.findElement(By.ById.id(getIdentifier(context, null, bean, CmpToIdentify.TAB_DETAIL, null))).click();
//			try {
//				waitLoadItemById(getIdentifier(bean, null, null, CmpToIdentify.GRID_LIST, null), 10);
//			} catch (Exception e) {
//			}
//			driver.findElement(By.ById.id(getIdentifier(context, null, bean, CmpToIdentify.TAB_DETAIL, null))).click();
//			return true;
//		} catch (Exception e) {
//			return false;
//		}
//	}
//
//	public String getReport() {
//		return this.r.toString();
//	}
//
//	public boolean executeTestScript(Map<String, String> bean, SeleniumTestConfig.ScriptNature nature, String method,
//			SeleniumTestConfig.ScriptTypes type) {
//		String script = "";
//		try {
//			if (method == null) {
//				script = testConfig.getTestScript(bean.get("id"), nature, null, type);
//			} else {
//				script = testConfig.getTestScript(bean.get("id"), nature, method, type);
//			}
//			if (script != null && !script.isEmpty()) {
//				String resFunc = getResultOfTestFunction(bean, script);
//				if (resFunc.isEmpty()) {
//					r.appendItemList(
//							"<div " + SeleniumReport.cssFontGreen + "> O sistema passou no teste do script.</div>",
//							null, "");
//				} else {
//					r.appendItemList("<div " + SeleniumReport.cssFontRed + "> Falha no Script de teste, retorno: "
//							+ resFunc + " </div>", null, "");
//				}
//				return true;
//			}
//		} catch (Exception e) {
//			return false;
//		}
//		return false;
//	}
//
//	public void testDetail(Map<String, String> context, int idx) {
//		if (getRowsOfGrid(context).size() > 0) {
//			try {
//
//				getJavaScriptExecutor().executeScript(
//						"Ext.getCmp('" + getIdentifier(context, null, null, CmpToIdentify.GRID_LIST, null)
//								+ "').getSelectionModel().clearSelections();");
//				waitLoadItemByXpath("//div[@id='" + getIdentifier(context, null, null, CmpToIdentify.GRID_LIST, null)
//						+ "']//table[@class='x-grid3-row-table']", -1);
//				List<WebElement> rows = driver
//						.findElement(By.ById.id(getIdentifier(context, null, null, CmpToIdentify.GRID_LIST, null)))
//						.findElements(By.ByClassName.className("x-grid3-row"));
//
//				try {
//					for (WebElement el : rows.get(idx).findElements(By.ByClassName.className("x-grid3-cell-inner"))) {
//						try {
//							el.click();
//						} catch (Exception ex) {
//							continue;
//						}
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				waitForDetail(context);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
//
//	public boolean okControl(Map<String, String> context, SeleniumReport.Method m) {
//		String okMsg = waitLoadingModalHide();
//		ServiceMethod sm = null;
//
//		if (m != null) {
//			sm = m.equals(SeleniumReport.Method.UPDATE) ? ServiceMethod.UPDATE : ServiceMethod.INSERT;
//		}
//
//		if (!okMsg.isEmpty() || okMsg.equals("#")) {
//			try {
//				waitLoadingModalHide();
//				if (context != null && sm != null) {
//					cancelarClick(context, sm);
//				}
//			} catch (Exception e) {
//			}
//		}
//		okMsg += okClick();
//		if (!okMsg.isEmpty() || !getJavaScriptExecutor().executeScript("return jQuery('.ext-mb-content').text()")
//				.toString().isEmpty()) {
//			getJavaScriptExecutor()
//					.executeScript("jQuery('button:contains(\"" + user.getLabel("label.ok") + "\")').click()");
//			// getJavaScriptExecutor().executeScript("jQuery('.ext-mb-content').remove()");
//		}
//		if (context != null && sm != null) {
//			cancelarClick(context, sm);
//		}
//
//		if (okMsg.contains("Confirma")) {
//			getJavaScriptExecutor().executeScript("jQuery('.ext-mb-content').remove()");
//			return true;
//		}
//
//		if (!okMsg.isEmpty() && !okMsg.contains("Confirma")) {
//			r.appendResultTest(SeleniumReport.Prefix.ERRO, m, context, okMsg);
//		} else {
//			okMsg = "";
//		}
//		if (okMsg.equals("#")) {
//			okMsg += getJavaScriptExecutor().executeScript("return jQuery('.ext-mb-content').text()").toString();
//		}
//		// getJavaScriptExecutor().executeScript("jQuery('.ext-mb-content').remove()");
//		return okMsg.isEmpty();
//	}
//
//	public boolean fieldStateIsValid(Map<String, String> context, Map<String, String> fieldContext,
//			Map<String, String> prop, ServiceMethod sm) {
//		boolean result = false;
//		try {
//			if (driver.findElements(By.ById.id(getIdentifier(context, fieldContext, prop, CmpToIdentify.FIELD, sm)))
//					.isEmpty() || prop.get("component").equals(PropertyComponents.HIDDEN.value)
//					|| prop.get("type").equals("openFieldset") || prop.get("type").equals("closeFieldset")) {
//				result = false;
//			} else {
//				result = driver
//						.findElement(By.ById.id(getIdentifier(context, fieldContext, prop, CmpToIdentify.FIELD, sm)))
//						.isDisplayed()
//						&& driver
//								.findElement(
//										By.ById.id(getIdentifier(context, fieldContext, prop, CmpToIdentify.FIELD, sm)))
//								.isEnabled();
//			}
//		} catch (Exception e) {
//			result = false;
//		}
//
//		if (!result && context.get("atualTab") != null && !context.get("atualTab").isEmpty()
//				&& context.get("fieldPrefix") != null) {
//			String oldFieldPrefix = context.get("fieldPrefix");
//			String newFieldPrefix = "";
//			newFieldPrefix = oldFieldPrefix.replace(fieldContext.get("key"), context.get("atualTab"));
//			context.put("fieldPrefix", newFieldPrefix.replaceAll("__", "_"));
//			try {
//				result = driver
//						.findElement(By.ById.id(getIdentifier(context, fieldContext, prop, CmpToIdentify.FIELD, sm)))
//						.isDisplayed()
//						&& driver
//								.findElement(
//										By.ById.id(getIdentifier(context, fieldContext, prop, CmpToIdentify.FIELD, sm)))
//								.isEnabled();
//			} catch (Exception e) {
//			}
//			if (!result) {
//				context.put("fieldPrefix", oldFieldPrefix);
//			}
//		}
//
//		return result;
//	}
//
//	public void createValidUser(HttpServletRequest request) {
//		try {
//			user.setIsLogin(true);
//			// user.setLogin(LOGIN);
//			user.setSchema(SYSTEM);
//			user.setServerUrl("http://localhost:8080/" + SYSTEM + "/LoginAction.do");
//			user.setLdap(false);
//			user.setSession(request.getSession());
//			user.setSessionId(request.getSession().getId());
//			user.setSuperUser(true);
//			user.setSystem(SYSTEM);
//			user = login(user.getLogin(), user.getSenha(), user, false);
//		} catch (Exception e) {
//			LogUtil.exception(e, user);
//		}
//	}
//
//	public void loginInSystem(ServletRequest request, WebDriver driver) {
//		driver.findElement(By.ByTagName.name("LOGIN")).sendKeys(user.getLogin());
//		driver.findElement(By.ByTagName.name("SENHA")).sendKeys(userSenha);
//		driver.findElement(By.ByXPath.xpath("//*[@id=\"label_ok-FORALL_" + SYSTEM + "/LoginVO_*_login\"]")).click();
//		// okControl(null, null);
//		r.appendItemList(" #### Incio SeleniumTester com o Usurio : " + user.getLogin(), SeleniumReport.FontColor.WHITE,
//				"background-color:#3C82B9;margin-left:-40px;");
//		createValidUser((HttpServletRequest) request);
//
//		List<Map<String, String>> autoShowItems = configSisAux.getAutoShowItems();
//		for (Map<String, String> as : autoShowItems) {
//			if (configSisAux.canAcess(as)) {
//				try {
//					waitLoadItemById("MULTIPLEID" + SYSTEM + "/" + as.get("name") + "VO_*_lista", 40);
//				} catch (Exception e) {
//				}
//			}
//		}
//		if (autoShowItems.isEmpty()) {
//			waitLoadingModalHide();
//		}
//	}
//
//	@Override
//	public String call() throws Exception {
//		initTest();
//		return this.getReport();
//	}
//
//}
