package com.aseg.tests;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.aseg.seguranca.Usuario;
import com.aseg.tests.ConfigSisAux.OperadoresFiltro;
import com.aseg.tests.ConfigSisAux.ServiceMethod;
import com.aseg.util.service.UtilServiceImpl;

public class SeleniumTestConfig {
	private static SeleniumTestConfig instance;
	private Document testConfig;
	private XPath xql;
	private UtilServiceImpl util = new UtilServiceImpl();
	private Usuario user;

	public static SeleniumTestConfig getInstance(Usuario user, boolean forceNew) {
		if (instance == null || forceNew) {
			instance = new SeleniumTestConfig(user);
		}
		return instance;
	}

	private SeleniumTestConfig(Usuario user) {
		this.user = user;
		xql = javax.xml.xpath.XPathFactory.newInstance().newXPath();

		try {
			testConfig = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(getTestConfig());
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public NodeList getNodeListFromTestConfig(String query) {
		try {
			return (NodeList) xql.evaluate(query, testConfig, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public NodeList getTestConfigOfBean(String id) {
		return getNodeListFromTestConfig("/configTest/testBeanConfig[@idBean='" + id + "']");
	}

	public boolean reportIsToShow(ReportItems it) {
		try {
			switch (it) {
			case CONSOLE_TEXT:
				return new Boolean(getNodeListFromTestConfig("/configTest/reportConfig").item(0).getAttributes()
						.getNamedItem("consoleText").getNodeValue());
			case ERROR:
				return new Boolean(getNodeListFromTestConfig("/configTest/reportConfig").item(0).getAttributes()
						.getNamedItem("showErrors").getNodeValue());
			case WARNING:
				return new Boolean(getNodeListFromTestConfig("/configTest/reportConfig").item(0).getAttributes()
						.getNamedItem("showWarnings").getNodeValue());
			}

		} catch (Exception e) {
			return true;
		}
		return true;
	}

	public enum ReportItems {
		ERROR, WARNING, CONSOLE_TEXT
	}

	public String getValueToField(Map<String, String> bean, Map<String, String> prop, ServiceMethod m, int indx) {
		String res = "";
		if (indx == -1) {
			indx = 0;
		}
		try {
			if (m == ServiceMethod.PREPARATION) {
				res = getNodeListFromTestConfig(
						"/configTest/testBeanConfig[@idBean='" + bean.get("id") + "']/valuesConfig/setPropValue[@name='"
								+ prop.get("name") + "' and @serviceMethod='preparation']").item(0).getAttributes()
										.getNamedItem("value").getNodeValue().toString();
			} else if (m == ServiceMethod.INSERT) {
				res = getNodeListFromTestConfig(
						"/configTest/testBeanConfig[@idBean='" + bean.get("id") + "']/valuesConfig/setPropValue[@name='"
								+ prop.get("name") + "' and @serviceMethod='insert' and @index='" + indx + "']").item(0)
										.getAttributes().getNamedItem("value").getNodeValue().toString();
			} else if (m == ServiceMethod.UPDATE) {
				res = getNodeListFromTestConfig(
						"/configTest/testBeanConfig[@idBean='" + bean.get("id") + "']/valuesConfig/setPropValue[@name='"
								+ prop.get("name") + "' and @serviceMethod='update' and @index='" + indx + "']").item(0)
										.getAttributes().getNamedItem("value").getNodeValue().toString();
			} else if (m == ServiceMethod.METHOD) {
				res = getNodeListFromTestConfig(
						"/configTest/testBeanConfig[@idBean='" + bean.get("id") + "']/valuesConfig/setPropValue[@name='"
								+ prop.get("name") + "' and @serviceMethod='method' and @index='" + indx
								+ "' and @methodName='" + bean.get("methodName") + "']").item(0).getAttributes()
										.getNamedItem("value").getNodeValue().toString();
			}
		} catch (Exception e) {
		}
		return res;
	}

	public int getCrudTestQtd(Map<String, String> bean) {
		try {
			return Integer
					.parseInt(getNodeListFromTestConfig("/configTest/testBeanConfig[@idBean='" + bean.get("id") + "']")
							.item(0).getAttributes().getNamedItem("crudTestQtd").getNodeValue().toString());
		} catch (Exception e) {
			return -1;
		}
	}

	public SelectBeansTypes getSelectBeansType() {
		try {
			String v = getNodeListFromTestConfig("/configTest/selectBeans").item(0).getAttributes().getNamedItem("type")
					.getNodeValue();
			if (v.equals("only")) {
				return SelectBeansTypes.ONLY;
			} else if (v.equals("ignore")) {
				return SelectBeansTypes.IGNORE;
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	public String getSelectedBeans() {
		return getNodeListFromTestConfig("/configTest/selectBeans").item(0).getAttributes().getNamedItem("idBeans")
				.getNodeValue();
	}

	public String getXpathOfSelectBeans() {
		try {
			return getNodeListFromTestConfig("/configTest/selectBeans").item(0).getAttributes().getNamedItem("xpath")
					.getNodeValue();
		} catch (Exception e) {
			return "";
		}
	}

	public boolean haveSelectBeanConfig() {
		try {
			if (getNodeListFromTestConfig("/configTest/selectBeans").item(0) != null
					&& getNodeListFromTestConfig("/configTest/selectBeans").item(0).getAttributes()
							.getNamedItem("idBeans") != null
					&& !getNodeListFromTestConfig("/configTest/selectBeans").item(0).getAttributes()
							.getNamedItem("idBeans").getNodeValue().isEmpty()
					&& !getNodeListFromTestConfig("/configTest/selectBeans").item(0).getAttributes()
							.getNamedItem("idBeans").getNodeValue().trim().equals("{}")) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public String getTestScript(String idBean, ScriptNature nature, String method, ScriptTypes type) {
		if ((method == null || method.isEmpty()) && nature != null) {
			return getNodeListFromTestConfig(
					"/configTest/testBeanConfig[@idBean='" + idBean + "']/script[@nature='" + nature.value + "']")
							.item(0).getTextContent();
		} else if (method != null && !method.isEmpty()) {
			return getNodeListFromTestConfig("/configTest/testBeanConfig[@idBean='" + idBean + "']/script[@nature='"
					+ nature.value + "' and @methodName = '" + method + "']").item(0).getTextContent();
		} else if (nature == null && method == null) {
			return getNodeListFromTestConfig("/configTest/testBeanConfig[@idBean='" + idBean + "']/script[@type = '"
					+ ScriptTypes.NAVIGATION_SCRIPT.value + "']").item(0).getTextContent();
		}
		return "";
	}

	public Object getPreparationScript(String idBean, String propName, boolean getScript) {
		try {
			if (getScript) {
				return getNodeListFromTestConfig(
						"/configTest/testBeanConfig[@idBean='" + idBean + "']/valuesConfig/script[@type = '"
								+ ScriptTypes.PREPARATION_SCRIPT.value + "' and @propertyName='" + propName + "']")
										.item(0).getTextContent();
			} else {
				return getNodeListFromTestConfig(
						"/configTest/testBeanConfig[@idBean='" + idBean + "']/valuesConfig/script[@type = '"
								+ ScriptTypes.PREPARATION_SCRIPT.value + "' and @propertyName='" + propName + "']")
										.item(0).getAttributes();
			}

		} catch (Exception e) {
			return null;
		}
	}

	public String getValueToFieldInAdvancedSearch(Map<String, String> bean, Map<String, String> field,
			OperadoresFiltro op) {
		String result = "";
		try {
			if (op != OperadoresFiltro.ESTA_ENTRE) {
				result = getNodeListFromTestConfig("/configTest/testBeanConfig[@idBean='" + bean.get("id")
						+ "']/advancedSearchConfig/field[@propertyName='" + field.get("name") + "' and @op='" + op
						+ "']").item(0).getAttributes().getNamedItem("v1").getNodeValue();
			} else {
				result = getNodeListFromTestConfig("/configTest/testBeanConfig[@idBean='" + bean.get("id")
						+ "']/advancedSearchConfig/field[@propertyName='" + field.get("name") + "' and @op='" + op
						+ "']").item(0).getAttributes().getNamedItem("v1").getNodeValue();
				result += "#" + getNodeListFromTestConfig("/configTest/testBeanConfig[@idBean='" + bean.get("id")
						+ "']/advancedSearchConfig/field[@propertyName='" + field.get("name") + "' and @op='" + op
						+ "']").item(0).getAttributes().getNamedItem("v2").getNodeValue();
			}
		} catch (Exception e) {
			return "";
		}
		return result;
	}

	public String getDriverLocation(BrowsersWebDrivers br) {
		try {
			return getNodeListFromTestConfig("/configTest/" + br.value).item(0).getAttributes().getNamedItem("src")
					.getNodeValue();
		} catch (Exception e) {
			return "";
		}
	}

	public String getSystemUrl() {
		try {
			return getNodeListFromTestConfig("/configTest").item(0).getAttributes().getNamedItem("systemUrl")
					.getNodeValue();
		} catch (Exception e) {
			return "";
		}
	}

	public boolean isToInitMasters() {
		try {
			return new Boolean(getNodeListFromTestConfig("/configTest").item(0).getAttributes()
					.getNamedItem("initMasters").getNodeValue().toString());
		} catch (Exception e) {
			return false;
		}
	}

	public enum ScriptNature {
		AFTER_INSERT("afterInsert"), BEFORE_INSERT("beforeInsert"),

		AFTER_UPDATE("afterUpdate"), BEFORE_UPDATE("beforeUpdate"),

		AFTER_REMOVE("afterRemove"), BEFORE_REMOVE("beforeRemove"),

		AFTER_LIST("afterList"), BEFORE_LIST("beforeList"),

		AFTER_METHOD("afterMethod"), BEFORE_METHOD("beforeMethod");

		private ScriptNature(String v) {
			this.value = v;
		}

		String value;

	}

	public enum SelectBeansTypes {
		ONLY("only"), IGNORE("ignore");
		private SelectBeansTypes(String v) {
			this.value = v;
		}

		private String value;

		public String getValue() {
			return this.value;
		}
	}

	public enum BrowsersWebDrivers {
		CHROME("chromeDriver"), FIREFOX("firefoxDriver"), INTERNET_EXPLORER("internetExplorerDriver");
		private BrowsersWebDrivers(String v) {
			value = v;
		}

		private String value;

		public String getValue() {
			return this.value;
		}

	}

	public enum ScriptTypes {
		TEST_SCRIPT("testScript"), NAVIGATION_SCRIPT("navigationScript"), PREPARATION_SCRIPT("preparationScript");
		private ScriptTypes(String v) {
			this.value = v;
		}

		String value;

		public String getValue() {
			return this.value;
		}
	}

	public boolean isToTestThisOperation(ServiceMethod sm) {
		try {
			String operations = getNodeListFromTestConfig("/configTest/selectBeans").item(0).getAttributes()
					.getNamedItem("operations").getNodeValue();
			if (operations == null || operations.isEmpty()) {
				return true;
			}
			switch (sm) {
			case INSERT:
				if (operations.contains("C") || operations.contains("c")) {
					return true;
				}
				break;
			case METHOD:
				if (operations.contains("M") || operations.contains("m")) {
					return true;
				}
				break;
			case UPDATE:
				if (operations.contains("U") || operations.contains("u")) {
					return true;
				}
				break;
			case DELETE:
				if (operations.contains("R") || operations.contains("r")) {
					return true;
				}
				break;
			case SEARCH:
				if (operations.contains("S") || operations.contains("s")) {
					return true;
				}
				break;
			}
		} catch (Exception e) {
			return true;
		}
		return false;
	}

	public InputStream getTestConfig() {
		ClassLoader cl = getClass().getClassLoader();
		try {
			URLConnection uc = cl.getResource("../config-test.xml").openConnection();
			uc.setUseCaches(false);
			return uc.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean isToTestThisBean(Map<String, String> master, Map<String, String> bean) {
		ConfigSisAux configAux = ConfigSisAux.getInstance(user);
		boolean result = false;
		Map<String, Object> sel = util.JSONtoMap(getSelectedBeans());

		if (sel.isEmpty()) {
			return true;
		}

		if (master == null) {
			if (sel.containsKey(bean.get("id"))) {
				result = true;
			}
		} else {
			Map<String, String> topMaster = bean;
			while (!topMaster.get("type").equals("M")) {
				topMaster = configAux.getMasterOfBean(topMaster);
			}
			if (sel.containsKey(topMaster.get("id")) || sel.containsKey(master.get("id"))) {
				if ((sel.get(topMaster.get("id")) != null && sel.get(topMaster.get("id")).toString().equals("{}"))
						|| (sel.get(master.get("id")) != null && sel.get(master.get("id")).toString().equals("{}"))) {
					result = true;
				} else {

					String it = "";
					if (sel.get(topMaster.get("id")) != null) {
						it = sel.get(topMaster.get("id")).toString();
					} else if (sel.get(master.get("id")) != null) {
						it = sel.get(master.get("id")).toString();
					}

					result = haveThisBeanInJson(it, bean.get("id"));
				}
			}
		}
		if (result == true && getSelectBeansType() == SelectBeansTypes.IGNORE) {
			result = false;
		}
		if (result == false && getSelectBeansType() == SelectBeansTypes.IGNORE) {
			result = true;
		}
		return result;
	}

	public boolean isToAnulateTestAll(String selItem) {
		boolean result = false;
		try {
			Entry<String, Object> e = util.JSONtoMap(selItem).entrySet().iterator().next();
			if (e.getValue().equals("{}")) {
				result = false;
			} else if (e.getKey().equals("x") && e.getValue().equals("x")) {
				result = true;
			}
		} catch (Exception e) {
			return false;
		}
		return result;
	}

	public boolean haveThisBeanInJson(String json, String idBean) {
		boolean result = false;

		if (isToAnulateTestAll(json)) {
			return false;
		}

		Map<String, Object> map = util.JSONtoMap(json);
		for (Entry<String, Object> e : map.entrySet()) {
			if (e.getKey().equals(idBean)) {
				result = true;
			} else if (!e.getValue().toString().equals("{}")) {
				result = haveThisBeanInJson(e.getValue().toString(), idBean);
			}
		}
		return result;
	}

	public String getImportDataSrc() {
		try {
			return getNodeListFromTestConfig("/configTest/importData/@src").item(0).getTextContent();
		} catch (Exception e) {
			return "";
		}
	}

	public boolean isToImportData() {
		try {
			return new Boolean(getNodeListFromTestConfig("/configTest/@importData").item(0).getTextContent());
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isToAutoNavigateInLinks(Map<String, String> bean) {
		try {
			if (getTestConfigOfBean(bean.get("id")).item(0).getAttributes().getNamedItem("navigateInLinks")
					.getNodeValue().equals("true")) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}
	}

}
