package com.aseg.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.service.UtilServiceImpl;

import net.sf.json.JSONArray;

public class ConfigSisAux {

	private Usuario user;
	private UtilServiceImpl util;
	private SeleniumReport r;
	private String SYSTEM;
	private SeleniumTestConfig testConfig;
	private static ConfigSisAux instance;

	public static String[][] notShowInSeach = {
			{ "component", "relationKey", "xtype", "xtype", "component", "type", "type", "component", "component",
					"component", "type" },
			{ "hidden", "true", "container", "method", "fake", "scriptRule", "sqlrule", "grid", "message", "context",
					"forwardRule" } };

	public static ConfigSisAux getInstance(Usuario user) {
		if (instance == null) {
			instance = new ConfigSisAux(user);
		}
		return instance;
	}

	private ConfigSisAux(Usuario user) {
		this.user = user;
		/// testConfig = SeleniumTestConfig.getInstance(user, true);
		util = user.getUtil();
		SYSTEM = getSystemName();
	}

	public enum PropertyComponents {
		SELECT("select"), MEMO("memo"), LIST("list"), HIDDEN("hidden"), RADIO_GROUP("radioGroup"), CHECK_BOX(
				"checkbox"), TIME("time"), COLOR("color"), MULTI_BOX("multibox"), LABEL("label"), DUAL_GRID(
						"dualGrid"), HIDDEN_CONTEXT("hidden_context"), CONTEXT("context"), DATA_VIEW(
								"dataview"), GRID("grid"), PROPERTY_GRID("propertygrid"), MESSAGE("message");
		String value = null;

		private PropertyComponents(String v) {
			this.value = v;
		}
	}

	public enum PropertyTypes {
		ALFA("alfa"), NUM("num"), ARRAY("array"), MONEY("money"), DATE("date"), TIME("time"), TEXT("text"), BIT(
				"bit"), FILE("file"), SQL_RULE(
						"sqlrule"), SCRIPT_RULE("scriptRule"), FORWARD_RULE("forwardRule"), LIST("list");
		String value = null;

		private PropertyTypes(String v) {
			this.value = v;
		}
	}

	public enum CmpToIdentify {
		B_ADICIONAR, B_ALTERAR, B_EXCLUIR, B_SALVAR, B_CANCELAR, B_METHOD, GRID_LIST, FIELD, FIELD_IN_DETAIL, INSERT_WINDOW, ALTERA_WINDOW, HEADER_DETAIL, // Identifica
																																							// o
																																							// cabealho
																																							// do
																																							// item
																																							// detalhado.
		B_ADVANCED_SEARCH, FILTRO_TREE_PANEL, B_BUSCAR,

		TAB_DETAIL_MASTER, TAB_LISTA,

		ID_SHOW_CONTEXT, // identifica, a div me das categorias, depois de
							// realizar um click em um item do menu.
		TAB_NAVIGATION, TAB_RESUMO, // tab de resumo do master
		TAB_RESUMO_CONTENT, // contedo da tab de resumo do master
		TAB_DETAIL, // Representa a Tab de resumo de um registro.
		TAB_INSERT_UPDATE, TAB_DETAIL_REGISTRO // Representa a tab de detalhe de
												// um registro de um bean Detail

	}

	public enum ServiceMethod {
		INSERT("inclui"), UPDATE("altera"), OBTEM("obtem"), LIST("lista"), METHOD("method"), DELETE(
				"remove"), PREPARATION("preparation"), SEARCH("search");

		private ServiceMethod(String v) {
			this.value = v;
		}

		String value;

	}

	public List<OperadoresFiltro> getOperadoresOfType(String type) {
		List<OperadoresFiltro> res = new ArrayList<OperadoresFiltro>();
		OperadoresFiltro[] ops = OperadoresFiltro.values();

		for (OperadoresFiltro o : ops) {
			if (o.type.contains(type)) {
				res.add(o);
			}
		}

		/*
		 * List<OperadoresFiltro> aux = new ArrayList<OperadoresFiltro>(){};
		 * aux.add(OperadoresFiltro.E_IGUAL_A); return aux;
		 */

		return res;
	}

	public enum OperadoresFiltro {

		// LT
		// E_SEMELHANTE("label.filtro.semelhante",
		// PropertyComponents.SELECT.value),

		// Num | Money
		E_IGUAL_A("label.filtro.igual",
				PropertyTypes.NUM.value + ";" + PropertyComponents.SELECT.value + ";" + PropertyTypes.MONEY.value),

		E_MAIOR_QUE("label.filtro.maior", PropertyTypes.DATE.value + ";" + PropertyTypes.MONEY.value), E_MENOR_QUE(
				"label.filtro.menor", PropertyTypes.DATE.value + ";" + PropertyTypes.MONEY.value),

		COMECA_COM("label.filtro.comeca", PropertyTypes.NUM.value + ";" + PropertyComponents.SELECT.value), CONTEM(
				"label.filtro.contem",
				PropertyTypes.NUM.value + ";" + PropertyComponents.SELECT.value), NAO_CONTEM("label.filtro.nao.contem",
						PropertyTypes.NUM.value + ";" + PropertyComponents.SELECT.value), NAO_NULO(
								"label.filtro.nao.nulo",
								PropertyTypes.NUM.value + ";" + PropertyComponents.SELECT.value + ";"
										+ PropertyTypes.MONEY.value), E_NULO(
												"label.filtro.nulo",
												PropertyTypes.NUM.value + ";" + PropertyComponents.SELECT.value + ";"
														+ PropertyTypes.MONEY.value), E_DIFERENTE_DE(
																"label.filtro.diferente",
																PropertyTypes.NUM.value + ";"
																		+ PropertyComponents.SELECT.value + ";"
																		+ PropertyTypes.MONEY.value),

		// Data
		ANO_ANTERIOR("label.filtro.ano.anterior", PropertyTypes.DATE.value), ANO_ATUAL("label.filtro.ano.atual",
				PropertyTypes.DATE.value), PROXIMO_ANO("label.filtro.ano.proximo",
						PropertyTypes.DATE.value), PROXIMOS_180_DIAS("label.filtro.dias.180proximos",
								PropertyTypes.DATE.value), ULTIMOS_180_DIAS("label.filtro.dias.180ultimos",
										PropertyTypes.DATE.value), PROXIMOS_30_DIAS("label.filtro.dias.30proximos",
												PropertyTypes.DATE.value), ULTIMOS_30_DIAS(
														"label.filtro.dias.30ultimos",
														PropertyTypes.DATE.value), PROXIMOS_360_DIAS(
																"label.filtro.dias.360proximos",
																PropertyTypes.DATE.value), ULTIMOS_360_DIAS(
																		"label.filtro.dias.360ultimos",
																		PropertyTypes.DATE.value), PROXIMOS_60_DIAS(
																				"label.filtro.dias.60proximos",
																				PropertyTypes.DATE.value), ULTIMOS_60_DIAS(
																						"label.filtro.dias.60ultimos",
																						PropertyTypes.DATE.value), ESTA_ENTRE(
																								"label.filtro.esta.entre",
																								PropertyTypes.DATE.value), ESTA_NO_FUTURO(
																										"label.filtro.esta.futuro",
																										PropertyTypes.DATE.value), ESTA_NO_PASSADO(
																												"label.filtro.esta.passado",
																												PropertyTypes.DATE.value), HOJE(
																														"label.filtro.hoje",
																														PropertyTypes.DATE.value), MES_ATUAL(
																																"label.filtro.mes.atual",
																																PropertyTypes.DATE.value), MES_PASSADO(
																																		"label.filtro.mes.passado",
																																		PropertyTypes.DATE.value), MES_PROXIMO(
																																				"label.filtro.mes.proximo",
																																				PropertyTypes.DATE.value), ONTEM(
																																						"label.filtro.ontem",
																																						PropertyTypes.DATE.value), NESTA_SEMANA(
																																								"label.filtro.semana.nesta",
																																								PropertyTypes.DATE.value), SEMANA_PASSADA(
																																										"label.filtro.semana.passada",
																																										PropertyTypes.DATE.value),

		// Boolean

		TRUE("label.filtro.sim", PropertyTypes.BIT.value), FALSE("label.filtro.nao", PropertyTypes.BIT.value);

		private OperadoresFiltro(String val, String ptype) {
			value = val;
			type = ptype;
		}

		public String getValue() {
			return this.value;
		}

		private String value;
		private String type;

	}

	public enum ValuesToFields {
		// Lista de valores predefinidos para preencher os campos.
		DATA(new String[] { "11/11/2011", "12/12/2011" }), TEXT(
				new String[] { "CDefaultValue", "UDefaultValue" }), TIME(new String[] { "00:00", "00:15" }), NUM(
						new String[] { "25", "50" }), MONEY(new String[] { "10,00", "50,00" }), TEXT_DURACAO(
								new String[] { "12345", "54321" }), FILE(new String[] {
										"[112, 114, 105, 110, 116, 108, 110, 40, 39, 82, 111, 100, 97, 110, 100, 111, 44, 32, 114,"
												+ "111, 100, 97, 110, 100, 111, 111, 111, 32, 104, 105, 32, 104, 105, 105, 105, 39, 41]",

										"[112, 114, 105, 110, 116, 108, 110, 40, 39, 82, 111, 100, 97, 110, 100, 111, 44, 32, 114, 111, "
												+ "100, 97, 110, 100, 111, 111, 111, 32, 104, 105, 32, 104, 105, 105, 105, 39, 41]"

		}), BIT(new String[] { "T", "F" });

		String[] values = null;

		private ValuesToFields(String[] v) {
			this.values = v;
		}

	}

	public List<Map<String, String>> filterProperties(List<Map<String, String>> items, String[] keys, String[] values) {
		// retorna apenas os items que casam com a condio passada em KEYS e
		// VALUES
		for (int i = 0; i < keys.length; i++) {
			items = filterProperties(items, keys[i], values[i]);
		}
		return items;
	}

	public List<Map<String, String>> filterProperties(List<Map<String, String>> items, String key, String value) {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		for (Map<String, String> it : items) {
			if (value.startsWith("!")) {
				if (it.get(key) != null && !it.get(key).equals(value.replaceAll("!", "")) && canAcess(it)) {
					result.add(it);
				}
			} else {
				if (it.get(key) != null && it.get(key).equals(value) && canAcess(it)) {
					result.add(it);
				}
			}
		}
		return result;
	}

	public Map<String, String> getPrimaryKeyOfBean(Map<String, String> bean) {
		return filterProperties(getItemsOfBean(bean), "primaryKey", "true").get(0);
	}

	public Map<String, String> getMainFieldBean(Map<String, String> bean) {
		return filterProperties(getItemsOfBean(bean), "mainField", "true").get(0);
	}

	public boolean linksOfBeanHaveGroupMaster(Map<String, String> bean, ServletRequest request) {
		if (((net.sf.json.JSONObject) ((JSONArray) util.JSONtoMap(bean.get("links")).get("Q")).get(0))
				.get("g") == null) {
			return false;
		} else if (((net.sf.json.JSONObject) ((JSONArray) util.JSONtoMap(bean.get("links")).get("Q")).get(0)).get("g")
				.equals("master")) {
			return true;
		}
		return false;
	}

	public Map<String, String> getMasterOfBean(Map<String, String> bean) {
		try {
			return (Map<String, String>) Collections.unmodifiableList((List) user.getSysParser().getListValuesFromXQL(
					"/systems/system[@name='" + SYSTEM + "']/module/bean[@id='" + bean.get("master") + "']", "order",
					user, true, false)).get(0);
		} catch (Exception e) {
			return null;
		}
	}

	public List<Map<String, String>> getItemsOfBean(Map<String, String> context) {
		return (List) user.getSysParser().getListValuesFromXQL(
				"/systems/system[@name='" + SYSTEM + "']/module/bean[@id='" + context.get("id") + "']/*", "order", user,
				true, true);
	}

	public List<Map<String, String>> getItemsOfBean(String beanName) {
		return (List) user.getSysParser().getListValuesFromXQL("/systems/system[@name='" + SYSTEM
				+ "']/module/bean[@id='" + getBeanByName(beanName).get("id") + "']/*", "order", user, true, true);
	}

	public Map<String, String> getGroupsByMaster(Map<String, String> bean) {

		List<Map<String, String>> auxiliares = Collections
				.unmodifiableList((List) user.getSysParser()
						.getListValuesFromXQL(
								util.EvaluateInString(
										((net.sf.json.JSONObject) ((JSONArray) util.JSONtoMap(bean.get("links"))
												.get("Q")).get(0)).get("e").toString(),
										util.createContext(new HashMap<String, String>(), user)),
								"order", user, true, false)); // ?? request

		Map<String, String> groups = new HashMap<String, String>();
		for (Map<String, String> a : auxiliares) {
			groups.put(a.get("master"), "");
		}
		return groups;
	}

	public boolean beanIsValidToDefaultCrudTest(Map<String, String> bean) {
		if ((bean.get("delegateViewList") != null && bean.get("delegateViewList").equals("true"))
				|| testConfig.isToAutoNavigateInLinks(bean)
				|| (bean.get("autoShow") != null && bean.get("autoShow").equals("true")) || !canAcess(bean)
				|| (bean.get("main") != null && bean.get("main").equals("true"))) {
			return false;
		}
		return true;
	}

	public Map<String, Object> getValuesOfMethodList(Map<String, String> bean, Map<String, String> field) {
		// retorma um map com os valores de um method do type L, com uma
		// listagem de items para um
		String query = "";
		if (field.get("service").equals(bean.get("name") + "Service")) {
			query = "/systems/system[@name='" + SYSTEM + "']/module/bean[@id='" + bean.get("id") + "']/method[@name='"
					+ field.get("serviceMethod") + "']/property";
		} else {
			query = "/systems/system[@name='" + SYSTEM + "']/module/bean[@name='"
					+ field.get("service").replaceAll("Service", "") + "']/method[@name='" + field.get("serviceMethod")
					+ "']/property";
		}

		return util.JSONtoMap(Collections.unmodifiableList(
				(List<Map<String, String>>) user.getSysParser().getListValuesFromXQL(query, "order", user, true, false))
				.get(0).get("value"));
	}

	public boolean isOperatorValue(OperadoresFiltro op) {
		// Identifica aqueles operadores que no precisam de valor, em que eles
		// mesmos so o valor definido.
		boolean result = true;
		switch (op) {
		case ANO_ANTERIOR:
			break;
		case ANO_ATUAL:
			break;
		case ESTA_NO_FUTURO:
			break;
		case ESTA_NO_PASSADO:
			break;
		case HOJE:
			break;
		case MES_ATUAL:
			break;
		case MES_PASSADO:
			break;
		case MES_PROXIMO:
			break;
		case PROXIMO_ANO:
			break;
		case PROXIMOS_180_DIAS:
			break;
		case PROXIMOS_30_DIAS:
			break;
		case PROXIMOS_360_DIAS:
			break;
		case PROXIMOS_60_DIAS:
			break;
		case NESTA_SEMANA:
			break;
		case NAO_NULO:
			break;
		case E_NULO:
			break;
		case ULTIMOS_180_DIAS:
			break;
		case ULTIMOS_30_DIAS:
			break;
		case ULTIMOS_360_DIAS:
			break;
		case ULTIMOS_60_DIAS:
			break;
		case ONTEM:
			break;
		case SEMANA_PASSADA:
			break;
		default:
			result = false;
			break;
		}
		return result;
	}

	public boolean canAcess(Map<String, String> it) {
		return ((ParseXmlService) user.getSysParser()).canAccess(it, user, true);
	}

	public String getTodosLinkLabel(Map<String, String> bean) {
		return ((net.sf.json.JSONObject) ((JSONArray) util.JSONtoMap(bean.get("links")).get("L")).get(0)).get("l")
				.toString();
	}

	public List<Map<String, String>> getPropsOfContext(String context) {
		return (List<Map<String, String>>) ((GenericService) util.getService(context, user)).getProperties()
				.get("ALL_FIELDS");
	}

	public List<Map<String, String>> getAutoShowItems() {
		return Collections.unmodifiableList((List<Map<String, String>>) user.getSysParser().getListValuesFromXQL(
				"/systems/system[@name='" + SYSTEM + "']/module/bean[@autoShow='true']", "order", user, true, false));
	}

	public boolean thisBeanDetailAfterInsert(Map<String, String> bean) {
		if (bean.get("actions") != null
				&& bean.get("actions").replaceAll(" ", "").contains("detailAfterInsert:false")) {
			return false;
		} else {
			return true;
		}
	}

	public Map<String, String> getBeanById(String id) {
		return (Map<String, String>) Collections.unmodifiableList((List) user.getSysParser().getListValuesFromXQL(
				"/systems/system[@name='" + SYSTEM + "']/module/bean[@id='" + id + "']", "order", user, true, false))
				.get(0);
	}

	public List<Map<String, String>> getBeanMethods(Map<String, String> bean, boolean byId, String methodXpath) {
		return getBeanMethods(bean.get("id"), byId, methodXpath);
	}

	public List<Map<String, String>> getBeanMethods(String idBean, boolean byId, String methodXpath) {
		if (methodXpath == null) {
			methodXpath = "";
		}
		if (byId) {
			return Collections.unmodifiableList((List) user.getSysParser().getListValuesFromXQL(
					"/systems/system[@name='" + SYSTEM + "']/module/bean[@id='" + idBean + "']/method" + methodXpath,
					"order", user, true, false));
		} else {
			return Collections.unmodifiableList((List) user.getSysParser().getListValuesFromXQL(
					"/systems/system[@name='" + SYSTEM + "']/module/bean[@name='" + idBean + "']/method" + methodXpath,
					"order", user, true, false));
		}
	}

	public List<Map<String, String>> getAllPropertysOfAllMethods(Map<String, String> bean) {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		List<Map<String, String>> methods = getBeanMethods(bean, true, null);
		for (Map<String, String> m : methods) {
			result.addAll(getPropertysOfMethod(bean, m.get("name"), true));
		}
		return util.filter(result, notShowInSeach[0], notShowInSeach[1]);
	}

	public List<Map<String, String>> getPropertysOfMethod(Map<String, String> bean, String methodName, boolean byId) {
		if (byId) {
			return getPropertysOfMethod(bean.get("id"), methodName, byId);
		} else {
			return getPropertysOfMethod(bean.get("name"), methodName, byId);
		}
	}

	public List<Map<String, String>> getPropertysOfMethod(String idBean, String methodName, boolean byId) {
		if (byId) {
			return Collections
					.unmodifiableList(
							(List) user.getSysParser()
									.getListValuesFromXQL(
											"/systems/system[@name='" + SYSTEM + "']/module/bean[@id='" + idBean
													+ "']/method[@name='" + methodName + "']/*",
											"order", user, true, false));
		} else {
			return Collections
					.unmodifiableList(
							(List) user.getSysParser()
									.getListValuesFromXQL(
											"/systems/system[@name='" + SYSTEM + "']/module/bean[@name='" + idBean
													+ "']/method[@name='" + methodName + "']/*",
											"order", user, true, false));
		}

	}

	public Map<String, String> getBeanByName(String id) {
		return (Map<String, String>) Collections.unmodifiableList((List) user.getSysParser().getListValuesFromXQL(
				"/systems/system[@name='" + SYSTEM + "']/module/bean[@name='" + id + "']", "order", user, true, false))
				.get(0);
	}

	public List<Map<String, String>> getTabsOfContext(Map<String, String> context) {
		return Collections
				.unmodifiableList(
						(List) user.getSysParser()
								.getListValuesFromXQL("/systems/system[@name='" + SYSTEM + "']/module/bean[@id='"
										+ context.get("id") + "']/container[@type='openTab']", "order", user, true,
										false));
	}

	public String getSystemName() {
		return (String) ((HashMap<String, String>) Collections.unmodifiableList(
				(List) user.getSysParser().getListValuesFromXQL("/systems/system[@name]", "order", user, true, false))
				.get(0)).get("name");
	}

	public List<Map<String, String>> getAllBeans() {
		return Collections.unmodifiableList((List) user.getSysParser()
				.getListValuesFromXQL("/systems/system[@name='" + SYSTEM + "']//bean", "order", user, true, false));
	}

	public List<Map<String, String>> getDetailBeans(Map<String, String> master) {
		return Collections.unmodifiableList((List) user.getSysParser().getListValuesFromXQL(
				"/systems/system[@name='" + SYSTEM + "']/module/bean[@master='" + master.get("id") + "' and @type='D']",
				null, user, true, false));
	}

}
