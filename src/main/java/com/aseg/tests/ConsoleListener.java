package com.aseg.tests;

import java.io.PrintStream;

public class ConsoleListener extends PrintStream {

	private static final String newLine = System.getProperty("line.separator");

	private final StringBuilder text = new StringBuilder();
	private final PrintStream original;

	public ConsoleListener(PrintStream original) {
		super(original);
		this.original = original;
	}

	public void print(double d) {
		text.append(d);
		original.print(d);
	}

	public void print(String s) {
		text.append(s);
		original.print(s);
	}

	public void println(String s) {
		text.append(s).append(newLine);
		original.println(s);
	}

	public void println() {
		text.append(newLine);
		original.println();
	}

	public PrintStream printf(String s, Object... args) {
		text.append(String.format(s, args));
		original.printf(s, args);
		return original;
	}

	public String getAllText() {
		return text.toString();
	}

}
