//package com.aseg.tests;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.Callable;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.Future;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.w3c.dom.NodeList;
//
//import com.aseg.config.Constants;
//import com.aseg.logauditoria.service.LogUtil;
//import com.aseg.seguranca.Usuario;
//
//public class SeleniumServlet extends HttpServlet {
//	private static final long serialVersionUID = 1L;
//
//	List<Map<String, String>> users;
//	Usuario user;
//	SeleniumTestConfig testConfig;
//
//	public SeleniumServlet() {
//		super();
//	}
//
//	protected void responds(HttpServletRequest request, HttpServletResponse response) {
//		user = (Usuario) request.getAttribute(Constants.RAIN_USER);
//		testConfig = SeleniumTestConfig.getInstance(user, true);
//		loadUsers();
//		ExecutorService execSvc = (ExecutorService) Executors.newFixedThreadPool(users.size());
//		List<Callable<String>> itensPool = new ArrayList<Callable<String>>();
//		List<Future<String>> reports = null;
//
//		Usuario testUser = null;
//		SeleniumTester selTest = null;
//		for (int i = 0; i < users.size(); i++) {
//			if (getUsersQtd() == i) {
//				break;
//			}
//			testUser = new Usuario();
//			testUser.setLogin(users.get(i).get("login"));
//			testUser.setSenha(users.get(i).get("senha"));
//			selTest = new SeleniumTester(testUser, request, SeleniumTestConfig.getInstance(testUser, true));
//			itensPool.add(selTest);
//		}
//
//		ConsoleListener console = null;
//		if (testConfig.reportIsToShow(SeleniumTestConfig.ReportItems.CONSOLE_TEXT)) {
//			console = new ConsoleListener(System.out);
//			System.setOut(console);
//		}
//		DataBaseInitializator dbInit = null;
//		if (testConfig.isToInitMasters()) {
//			selTest.createValidUser(request);
//			dbInit = new DataBaseInitializator(
//					Usuario.getSystemUser(),
//					new SeleniumReport(testConfig.reportIsToShow(SeleniumTestConfig.ReportItems.ERROR),
//							testConfig.reportIsToShow(SeleniumTestConfig.ReportItems.WARNING)));
//			dbInit.initMasters();
//		}
//		DataImporter dataImporter = null;
//		if (testConfig.isToImportData() && !testConfig.getImportDataSrc().isEmpty()) {
//			dataImporter = new DataImporter(testUser);
//			dataImporter.importAllDocuments();
//		}
//
//		try {
//			reports = execSvc.invokeAll(itensPool);
//			for (Future<String> report : reports) {
//				response.getWriter().write(report.get());
//			}
//			if (testConfig.reportIsToShow(SeleniumTestConfig.ReportItems.CONSOLE_TEXT)) {
//				response.getWriter().write(console.getAllText());
//			}
//			if (dbInit != null) {
//				response.getWriter().write(dbInit.getReport());
//			}
//			response.getWriter().close();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		} catch (Exception e) {
//			LogUtil.exception(e, user);
//		}
//	}
//
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		responds(request, response);
//	}
//
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		responds(request, response);
//	}
//
//	public void loadUsers() {
//		users = new ArrayList<Map<String, String>>();
//		NodeList ndl = testConfig.getNodeListFromTestConfig("/configTest/user");
//		for (int i = 0; i < ndl.getLength(); i++) {
//			Map<String, String> u = new HashMap<String, String>();
//			u.put("login", ndl.item(i).getAttributes().getNamedItem("login").getNodeValue());
//			u.put("senha", ndl.item(i).getAttributes().getNamedItem("senha").getNodeValue());
//			u.put("browser", ndl.item(i).getAttributes().getNamedItem("browser").getNodeValue());
//			users.add(u);
//		}
//	}
//
//	public int getUsersQtd() {
//		return Integer.parseInt(testConfig.getNodeListFromTestConfig("/configTest").item(0).getAttributes()
//				.getNamedItem("userQtd").getNodeValue());
//	}
//}
