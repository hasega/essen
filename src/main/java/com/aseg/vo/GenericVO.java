package com.aseg.vo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.springframework.util.StringUtils;

import com.aseg.config.Constants;
import com.aseg.exceptions.BusinessException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.Paginator;
import com.aseg.util.DateUtil;
import com.aseg.util.service.UtilServiceImpl;


public class GenericVO extends HashMap<String, Object> implements
        Map<String, Object>, Paginator {

    private static final long serialVersionUID = 1L;
    public static final List staticProps = java.util.Arrays
            .asList(new String[]{"MULTIPLEID", "START", "LIMIT", "TOTAL",
                    "SORT", "DIR", Constants.ActionParam, "PAGINATE",
                    "SEGMENTBY", Constants.FILTER_KEY});
    private boolean encapsulate = true;
    private boolean ignoreCache = false;
    private boolean ignoreCase = false;
    private String key;
    private boolean lazyList = false;
    private boolean ignoreIdCondition = false;
    private boolean isImport = false;
    private boolean isExport = false;

    public boolean isExport() {
        return isExport;
    }

    public void setExport(boolean isExport) {
        this.isExport = isExport;
    }

    public boolean isImport() {
        return isImport;
    }

    public void setImport(boolean isImport) {
        this.isImport = isImport;
    }

    private boolean forceDistinct = false;

    // criado para scripts que realizam a chamada de um m�todo que est�
    // contido em seu serviceMethod, ex: um script q tenha serviceMethod="R" e
    // que no c�digo realiza a chamada de um obtem() gerando um recurs�o
    // infinita.
    private List<String> ignoredScripts;
    private static UtilServiceImpl util = new UtilServiceImpl();
    private String searchBy;
    private List<Object> validationErrors = new ArrayList<Object>();
    private final List<Object> validationProperties = new ArrayList<Object>();
    private boolean preserv;
    private GenericVO old_VO;
    private boolean process = false;
    private boolean segmentBy = true;
    private boolean commitSingle;
    private String matcher = "";
    private boolean anulate = true; // nao altera valores nullos se false
    private int updatedRows = 0;
    private Map aliases = new HashMap();
    private boolean controll = true;

    public void setUpdatedRows(int updatedRows) {
        this.updatedRows = updatedRows;
    }

    public GenericVO() {

        initialize();
    }

    private void initialize() {
        // TODO Auto-generated method stub
        this.put("START", "0");
        this.put("LIMIT", "10");
        this.put("TOTAL", "");
        this.put("SORT", "");
        this.put("DIR", "");
        this.put(Constants.ActionParam, "");
        this.put("PAGINATE", Constants.true_str);
        this.ignoredScripts = new ArrayList<String>() {
            @Override
            public boolean add(String obj) {
                if (!super.contains(obj)) {
                    super.add(obj);
                    return true;
                }
                return false;
            }
        };
        matcher = "";
        this.put(Constants.FILTER_KEY, null);
        setMultipleId(null);
        aliases.clear();
    }

    @Override
    public void clear() {

        super.clear();
        initialize();

    }

    public void setProcess(boolean process) {
        this.process = process;
    }

    public String getMatcher() {
        if (this.get("MATCHER") != null) {
            return "" + this.get("MATCHER");
        }
        return matcher;
    }

    public void setMatcher(String matcher) {
        this.matcher = matcher;
    }

    public GenericVO cloneConfig(final GenericVO vo) {
        this.put("START", vo.getStart());
        this.put("LIMIT", vo.getLimit());
        this.put("TOTAL", vo.getTotal());
        this.put("DIR", vo.getDir());
        this.put(Constants.ActionParam, "");
        this.put("PAGINATE", vo.getPaginate());
        this.put(Constants.FILTER_KEY, vo.getFiltro());
        this.put("CONTEXTO", vo.get("CONTEXTO"));
        if (vo.get("SERVICE") != null) {
            this.put("SERVICE", vo.get("SERVICE"));
        }
        if (vo.get("SERVICEMETHOD") != null) {
            this.put("SERVICEMETHOD", vo.get("SERVICEMETHOD"));
        }
        if (vo.get("KEY") != null) {
            this.put("KEY", vo.get("KEY"));
        }
        if (vo.get("LABELVALUE") != null) {
            this.put("LABELVALUE", vo.get("LABELVALUE"));
        }
        if (vo.get("SEGMENTBY") != null) {
            this.put("SEGMENTBY", vo.get("SEGMENTBY"));
        }
        final Iterator i = vo.keySet().iterator();
        while (i.hasNext()) {
            final String object = (String) i.next();
            if (this.containsKey(object) && vo.get(object) != null) {
                this.put(object, vo.get(object));
            }
        }

        return this;
    }

    public GenericVO format(final Usuario user) {

        final Iterator i = this.keySet().iterator();

        while (i.hasNext()) {
            final String object = (String) i.next();
            try {
                this.put(object, this.getFormated(object, user));
            } catch (final Exception e) {
                LogUtil.exception(e, user);
            }

        }

        return this;
    }

    @Override
    public Object get(final Object pk) {
        if (pk == null) {
            return null;
        }
        return super.get(pk.toString().toUpperCase());
    }

    public Object getICase(final Object pk) {
        if (pk == null) {
            return null;
        }
        return super.get(pk);
    }

    public Object get(final Object pk, final int i) {
        try {
            return ((Object[]) get(pk.toString()))[i];
        } catch (final Exception e) {
            return get(pk.toString());
        }

    }

    @Override
    public String getAlias() {
        return (String) this.get("ALIAS");
    }

    public String getDataLog() {
        final StringBuffer out = new StringBuffer();
        Iterator elem = null;
        elem = this.keySet().iterator();
        Object obj;
        if (elem != null) {
            while (elem.hasNext()) {
                obj = elem.next();
                // || ((dados.get(obj) instanceof String [] ) ))

                if (this.get(obj) instanceof Object[]) {
                    final Object[] a = (Object[]) this.get(obj);
                    out.append(obj.toString()).append(" : [");
                    for (final Object element : a) {
                        out.append(
                                element != null ? element.toString() : "null")
                                .append(",");
                    }
                    out.append(" ] ");
                } else if (this.get(obj) instanceof java.util.AbstractCollection) {
                    {
                        final Iterator um = ((java.util.AbstractCollection<?>) this
                                .get(obj)).iterator();
                        out.append(obj != null ? obj.toString() : "null")
                                .append(" : [");
                        while (um.hasNext()) {
                            final Object a = um.next();
                            out.append(a.toString()).append(",");

                        }

                        out.append(" ] ");
                    }
                } else {
                    out.append(obj.toString()).append(" : ")
                            .append(getString(obj.toString())).append(",");
                }
            }
        } else {
            out.append("empty");
        }

        return out.toString();
    }

    @Override
    public String getDir() {
        return (String) this.get("DIR");
    }

    public List<Object> getErrors() {
        return validationErrors;
    }

    @Override
    public String getFiltro() {

        return (String) this.get(Constants.FILTER_KEY);
    }

	/*
     * (non-Javadoc)
	 * 
	 * @see com.aseg.view.Paginator#getFiltro()
	 */

    public Object getFormated(final String name, final Usuario user) {

        Map properties = null;
        final List mp = util.match(getValidationProperties(), Constants.name,
                name.toString());
        if (mp.size() > 0) {
            properties = (Map) mp.get(0);
        }
        Object value = this.get(name);
        if (properties == null) {
            return value;
        }

        if (value == null || value.equals("null")) {
            return "";
        }
        if (this.containsKey("FORCE_*_TEXT")) {
            return value;
        }
        final GenericVO x = new GenericVO();
        x.clear();
        x.putAllAndGet(this, true);
        x.putAllAndGet(properties, true);

        // **EVSER
        // if (properties.get("service") != null &&
        // properties.get("serviceMethod")!=null && !util.isDefaultCRUD((String)
        // properties.get("serviceMethod"))) {
        //
        // util.matchService(x, name, value, user);
        // value = util.evalService(x, name, user);
        // }

        if (properties.get(Constants.component) != null) {
            if (properties.get(Constants.component).toString().equals("money")
                    && !(this.isImport && !this.isExport)) {
                return util.formatoMonetario((BigDecimal) value, 2);
            }
            if (properties.get(Constants.component).toString().equals("time")) {
                if (properties.get("mask") != null
                        && properties.get("mask").equals("H:i"))
                    return value.toString().substring(0, 5);
                return value.toString();
            }
            if (properties.get(Constants.component).toString().equals("object")) {
                final byte[] br = (byte[]) value;

                ObjectInputStream objectIn = null;
                if (br != null)
                    try {
                        objectIn = new ObjectInputStream(
                                new ByteArrayInputStream(br));
                    } catch (IOException e) {
                        LogUtil.exception(e, user);
                    }

                try {
                    return objectIn.readObject();
                } catch (IOException e) {
                    LogUtil.exception(e, user);
                } catch (ClassNotFoundException e) {
                    LogUtil.exception(e, user);
                }
            }
            if (properties.get(Constants.component).toString().equals("indice")) {
                return util.formatoMonetario((BigDecimal) value, 6);
            }
            if (properties.get(Constants.component).toString().equals("date")) {
                // if (properties.get("mask") != null
                // && properties.get("mask").equals("datetime")) {
                // return util.df
                // .fromTimestampcomHoraptBR((Timestamp) value);
                // } else {
                try {
                    return DateUtil.fromTimestamp((Timestamp) value);
                } catch (final Exception e) {
                    if (value.toString().equalsIgnoreCase("now")) {
                        return DateUtil.fromTimestamp(new Timestamp(System
                                .currentTimeMillis()));
                    }
                }
                // }
            }

            if (properties.get(Constants.component).toString()
                    .equals("imageSelector")
                    && !isImport) {
                String pre = "/images/";
                if (value.toString().startsWith(
                        user.getSystemPath() + "/ItemLinguagemAction" + Constants.ACTION_SUFIX + "")) {
                    pre = "";
                }
                if (value.toString().trim().length() == 0) {
                    value = "s.gif";
                }

                // TODO As vezes o SYSTEM do usu�rio � perdido e aqui ele
                // corrige,
                // colocando novamente /system/restoDoLink para acessar o
                // itemDeLinguagem
                if (value != null
                        && !value.toString().startsWith("/" + user.getSystem())) {
                    value = "/" + user.getSystem() + value.toString();
                }

                // pegar o label do value e colocar na descricao
                return "<img border='0' src ='" + pre + value.toString()
                        + "' title='"
                        + user.getDescricao((String) properties.get("label"))
                        + "' />";
            }
            if (properties.get(Constants.component).toString()
                    .equals("checkbox")
                    && !isImport) {
                if (value == null) {
                    return "";
                }
                if (value.toString().equalsIgnoreCase("T")) {
                    return "<img border=\'0\' src =\'/images/check.gif\' />";
                } else {
                    return "";
                }

            }
        }

        return value;

    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aseg.view.Paginator#setFiltro(java .lang.String)
	 */

    public String getKey() {
        return key;
    }

    @Override
    public String getLimit() {
        return (String) this.get("LIMIT");
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aseg.view.Paginator#setMultipleId( java.lang.String[])
	 */

    public String getMapping() {
        return (String) this.get("MAPPING");

    }

    public String getMethod() {
        return (String) this.get(Constants.ActionParam);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.aseg.view.Paginator#getMultipleId()
     */
    public String[] getMultipleId() {
        String[] m = new String[]{""};
        try {
            m = (String[]) this.get("MULTIPLEID");
            if (m != null && m.length == 1 && m[0] != null
                    && m[0].toString().indexOf(",") > -1) {
                return m[0].split(",");

                // pegar o id do do bean caso esteja vazio
                // ex: config_sis/BLOQUEAR_SCR ou DESBLOQUEAR_SCR (method que
                // esta rodando no detail e
                // assim n�o estava retornado nada, pq n�o selecionava nenhum
                // checked).
            } else if (m == null || m.length == 1
                    && (m[0] == null || m[0].equals(""))) {
                return new String[]{this.get(this.getKey()).toString()};
            }

            return m;
        } catch (final Exception e) {
            return m;
        }

    }

    public String getMultipleIdString() {
        try {
            final String[] m = (String[]) this.get("MULTIPLEID");

            return StringUtils.arrayToCommaDelimitedString(m);
        } catch (final Exception e) {
            return null;
        }

    }

    public GenericVO getOld_VO() {
        return old_VO;
    }

    public boolean getPaginate() {

        if (this.get("PAGINATE") != null
                && this.get("PAGINATE").toString().equals(Constants.true_str)) {
            return true;
        }
        return false;
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aseg.view.Paginator#getFiltro()
	 */

    public String getSearchBy() {
        return searchBy;
    }

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aseg.view.Paginator#setFiltro(java .lang.String)
	 */

    @Override
    public String getSort() {
        return (String) this.get("SORT");
    }

    @Override
    public String getStart() {
        if (this.get("START") == null
                || this.get("START").toString().trim().length() == 0) {
            setStart("0");
        }
        return (String) this.get("START");
    }

    public String getString(final String nomeChave) {
        return this.get(nomeChave) == null ? "" : this.get(nomeChave)
                .toString();
    }

    @Override
    public String getTotal() {

        if (this.get("TOTAL") == null
                || this.get("TOTAL").toString().trim().length() == 0) {
            return "0";
        }
        return (String) this.get("TOTAL");
    }

    public List<Object> getValidationErrors() {
        return validationErrors;
    }

    public List<Object> getValidationProperties() {

        return new ArrayList<Object>(validationProperties);
    }

    @Override
    public String getWhere() {

        return (String) this.get("WHERE");
    }

    public boolean ignoreCache() {

        return ignoreCache;
    }

    public GenericVO ignoreCase() {

        ignoreCase = true;
        return this;
    }

    public boolean isEncapsulate() {
        return encapsulate;
    }

    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    public boolean isLazyList() {
        return lazyList
                || this.get(Constants.lazyList) != null
                && this.get(Constants.lazyList).toString()
                .equalsIgnoreCase(Constants.true_str);
    }

    public boolean isPaginate() {

        if (this.get("PAGINATE") != null
                && this.get("PAGINATE").toString().equals(Constants.true_str)) {
            return true;
        }
        return false;
    }

    public boolean isPreserv() {
        return preserv;
    }

    public GenericVO mapping(final Map d) {
        final Iterator i = d.keySet().iterator();
        while (i.hasNext()) {
            final String element = (String) i.next();
            final Object val = d.get(element);
            if (!this.containsKey(element)) {
                continue; // SO COLOCA NA VO O QUE PERTENCE AO DOMINIO DELA
            }
            if (val != null && val instanceof List) {
                // TODO ADAPTAO PARA RESOLVER O PROBLEMA DO TRATAMENTO DE TIPOS
                // DE
                // LISTA RECEBIDOS NA CAMADA DE APRESENTAO (EXTJS -
                // TAGDUALLIST) COMO
                // STRING E POPULA-LOS NA VO COMO ARRAY DE STRING

                try {
                    this.put(element.toUpperCase(), ((List) val).toArray());
                } catch (final Exception e) {
                    this.put(element.toUpperCase(), val);
                }

            } else {
                this.put(element.toUpperCase(), val);
            }

        }
        return this;
    }

    public GenericVO match(final Map p) {
        if (p == null) {
            return this;
        }
        // atencao somente sobrescreve valores que nao sao nulos na VO
        for (final Iterator iterator = p.keySet().iterator(); iterator
                .hasNext(); ) {
            final String type = (String) iterator.next();
            if (this.containsKey(type)) {
                this.put(type, p.get(type));
            }

        }

        return this;

    }

    /**
     * Retorna o mesmo VO, apenas com os valores no nulos e no "string vazia" e
     * no string "null" e no array length=0.
     *
     * @return
     */
    public GenericVO notNulls() {
        final GenericVO result = (GenericVO) this.clone();
        final Set<String> keys = this.keySet();
        final Iterator<String> it = keys.iterator();
        for (int i = 0; it.hasNext(); i++) {
            final String key = it.next();
            final Object item = this.get(key);
            if (item instanceof Object[]) {
                final Object[] array = (Object[]) item;
                if (array.length > 0) {
                    result.put(key, item);
                }
            } else if (item != null && item.toString().length() > 0
                    && !item.toString().equalsIgnoreCase("null")) {
                result.put(key, item);
            }
        }

        return result;
    }

    @Override
    public boolean paginate() {
        try {
            if (isPaginate()) {
                return true;
            }
            // this.getDynaClass().getName();
            return false;
            // return false;
        } catch (final Exception e) {
            return false;
        }
    }

    public void preservOld() {
        this.preserv = true;

    }

    public GenericVO processKeyBinder(final GenericVO bindVO) {

        if (bindVO == null) {
            return this;
        }

        List mp = util.match(getValidationProperties(), Constants.component,
                "hidden");
        mp = util.filter(mp, "primaryKey", Constants.true_str);
        for (final Iterator iterator = mp.iterator(); iterator.hasNext(); ) {
            final Map object = (Map) iterator.next();
            if (bindVO.get(object.get(Constants.name)) != null) {
                this.put((String) object.get(Constants.name),
                        bindVO.get(object.get(Constants.name)));
            }
        }

        return this;
    }

    public GenericVO putICase(final String idx, final Object data) {

        try {
            super.put(idx, solveAmp(data));
        } catch (final Exception e) {
            LogUtil.exception(e, null);
        }
        return this;
        // else
        // super.put(idx.toString().toUpperCase(),"");
        // return data;
    }

    // --------------------------------------

    @Override
    public GenericVO put(final String idx, final Object data) {
        try {
            super.put(idx.toUpperCase(), solveAmp(data));
        } catch (final Exception e) {
            LogUtil.exception(e, null);
        }
        return this;
        // else
        // super.put(idx.toString().toUpperCase(),"");
        // return data;
    }

    private Object solveAmp(Object data) {
        // TODO Auto-generated method stub
        if (data instanceof String)
            return util.replace(data.toString(), "#--e#c#o#m--#", "&");
        if (data instanceof org.json.JSONArray)
            return ((org.json.JSONArray) data);
        if (data instanceof org.json.JSONObject)
            return ((Map) data);

        return data;
    }

    public GenericVO putDiff(final Map p) {
        if (p == null) {
            return this;
        }
        // atencao somente sobrescreve valores que nao sao nulos na passada
        for (final Iterator iterator = p.keySet().iterator(); iterator
                .hasNext(); ) {
            final String type = (String) iterator.next();
            if (!staticProps.contains(type.toUpperCase())
                    && p.get(type) != null) {
                this.put(type, p.get(type));
            }

        }

        return this;

    }

    public GenericVO putAllAndGet(final Map p, final boolean override) {
        if (p == null || p == this) {
            return this;
        }
        // atencao somente sobrescreve valores que nao sao nulos na VO atual
        for (final Iterator iterator = p.keySet().iterator(); iterator
                .hasNext(); ) {
            final String type = (String) iterator.next();
            if (override) {
                this.put(type, p.get(type));
            } else if (type != null
                    && !staticProps.contains(type.toUpperCase())
                    && (this.get(type) == null || this
                    .get(type)
                    .toString()
                    .startsWith(
                            Constants.EXPRESSION_PREFIX))) {
                this.put(type, p.get(type));
            }

        }

        return this;

    }

    public GenericVO putKey(final Object idx, Object data) {

        if (getSearchBy() != null
                && getSearchBy().toString().equals(Constants.CACHE)) {
            if (data != null
                    && !data.toString().startsWith(Constants.CACHE_SEPARATOR)) {
                data = Constants.CACHE_SEPARATOR + data;
            }
        } else {
            this.searchBy(idx != null ? idx.toString().toUpperCase() : null);
        }
        this.key = (String) idx;
        super.put(idx != null ? idx.toString().toUpperCase() : null, data);
        return this;
        // else
        // super.put(idx.toString().toUpperCase(),"");
        // return data;
    }

    public void remove(final String elem) {
        if (this.get(elem) != null) {
            super.remove(elem.toUpperCase());
        }
    }

    public void removeAll(final Map m) {

        final Iterator i = m.keySet().iterator();
        while (i.hasNext()) {
            final Object object = i.next();
            this.remove(object);
        }

    }

    public GenericVO searchBy(final Object idx) {
        if (idx != null && idx == Constants.CACHE) {
            if (this.get(getKey()) != null
                    && !this.get(getKey()).toString()
                    .startsWith(Constants.CACHE_SEPARATOR)) {
                this.putKey(getKey(),
                        Constants.CACHE_SEPARATOR + this.get(getKey()));
            }
        }
        this.searchBy = (String) idx;
        return this;
    }

    @Override
    public void setAlias(final String alias) {
        this.put("ALIAS", alias);

    }

    @Override
    public void setDir(final String dir) {
        this.put("DIR", dir);
    }

    public void setEncapsulate(final boolean b) {

        encapsulate = b;
    }

    @Override
    public void setFiltro(final String filtro) {
        if (filtro != null) {
            super.put(Constants.FILTER_KEY, filtro);
        }
    }

    public void setIgnoreCache(final boolean b) {

        ignoreCache = b;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public void setLazyList(final boolean lazyList) {
        this.lazyList = lazyList;
    }

    @Override
    public void setLimit(final String limit) {
        this.put("LIMIT", limit);
    }

    public void setMapping(final String map) {
        if (map != null) {
            super.put("MAPPING", map);
        }
    }

    public void setMethod(final String method) {
        this.put(Constants.ActionParam, method);
    }

    @Override
    public void setMultipleId(final String[] multipleId) {
        if (multipleId == null) {
            this.put("MULTIPLEID", new String[]{});
        } else {
            this.put("MULTIPLEID", multipleId);
        }
    }

    public void setOld_VO(GenericVO old_VO) {
        this.old_VO = old_VO;
    }

    public void setPaginate(final boolean paginate) {
        this.put("PAGINATE", String.valueOf(paginate));
    }

    public void setPreserv(boolean preserv) {
        this.preserv = preserv;
    }

    @Override
    public void setSort(final String sort) {
        this.put("SORT", sort);
    }

    @Override
    public void setStart(final String start) {
        this.put("START", start.trim().length() == 0 ? "0" : start);
    }

    @Override
    public void setTotal(final String total) {
        this.put("TOTAL", total);
    }

    @Override
    public void setSTime(final String total) {
        this.put("STIME", total);
    }

    public void setValidation_errors(final List<Object> validationErrors) {
        this.validationErrors = validationErrors;
    }

    public void setValidationProperties(final List<Object> validationProperties) {
        for (Object element : validationProperties) {

            this.validationProperties.add((Map) element);
        }

    }

    @Override
    public void setWhere(final String where) {
        this.put("WHERE", where);
    }

    /**
     * Cria um array de Object a partir do VO, APENAS com os valores no nulos e
     * no "string vazia" e no string "null" e no array length=0.
     *
     * @return
     */
    public Object[] toArrayNotNull() {
        final Object[] todos = this.values().toArray();
        final List filtrados = new ArrayList();
        for (int i = 0; i < todos.length; i++) {
            if (todos[i] instanceof Object[]) {
                final Object[] array = (Object[]) todos[i];
                if (array.length > 0) {
                    filtrados.add(todos[i]);
                }
            } else if (todos[i] != null && todos[i].toString().length() > 0
                    && !todos[i].toString().equalsIgnoreCase("null")) {
                filtrados.add(todos[i]);
            }
        }

        final Object[] result = filtrados.toArray();
        return result;
    }

    public GenericVO validate(Usuario user) throws BusinessException {

        final Iterator i = ((Map) this.clone()).keySet().iterator();

        while (i.hasNext()) {
            final String object = (String) i.next();
            if (staticProps.contains(object))
                continue;
            this.put(object, convert(object, this.get(object), user));

        }

        return this;
    }

    public Object convert(final String name, final Object value, Usuario user)
            throws BusinessException {
        Map properties = null;
        try {

            List mp = util.match(getValidationProperties(), Constants.name,
                    name.toString());
            if (mp.size() == 1) {
                properties = (Map) mp.get(0);
            } else if (mp.size() > 1) {
                List mc = util.match(mp, "parent_id", getMethod());
                if (mc.size() > 0)
                    properties = (Map) mc.get(0);
                else
                    properties = (Map) mp.get(0);
            }

            if (value == null) {
                if (properties != null
                        && properties.get("type") != null
                        && properties.get("type").toString()
                        .equalsIgnoreCase("array")) {
                    return new String[]{};
                }

                return value;
            } else if (value.toString().equalsIgnoreCase("null")) {
                return null;
            }

            if (value.toString().startsWith(
                    Constants.EXPRESSION_PREFIX)) {
                return value;
            }

            if (properties == null) {
                if (value instanceof String) {
                    return value != null
                            && util.decode((String) value, user) == null ? value
                            : util.decode((String) value, user);
                } else {
                    return value; // Streams
                }
            }

            if (value.toString().trim().length() == 0) {
                return null;
            }

            if (properties.get("type").toString().equalsIgnoreCase("file")) {
                return value;

            }
            if (user != null
                    && properties.get("maxlength") != null
                    && (properties.get("type").toString()
                    .equalsIgnoreCase("alfa") && properties
                    .get("transient") == null)
                    && value.toString().length() > (Integer.parseInt(properties
                    .get("maxlength").toString())))
                throw new BusinessException(user.getLabel(properties.get(
                        "label").toString())
                        + " "
                        + user.getLabel("label.excedeu")
                        + " "
                        + properties.get("maxlength")
                        + " "
                        + user.getLabel("label.chars"));
            // fazendo o decode dos elementos do request
            if (properties.get("type").toString().equalsIgnoreCase("memo")) {
                if (user != null && value.toString().length() > 4000)
                    throw new BusinessException(user.getLabel(properties.get(
                            "label").toString())
                            + " " + user.getLabel("label.excedeu") + " 4000 "

                            + user.getLabel("label.chars"));
                return util.clearForJSON((String) value);
            }
            if (properties.get("type") == null
                    || properties.get("type").toString()
                    .equalsIgnoreCase("alfa")) {
                return value;
            }
            if (properties.get("type").toString().equalsIgnoreCase("num")) {
                try {
                    return new Long(util.removePontos(value.toString()));
                } catch (final Exception e) {
                    return value;
                }
            }
            if (properties.get("type").toString().equalsIgnoreCase("money")
                    && !isImport) {
                if (value instanceof BigDecimal) {
                    return value;
                } else {
                    return DateUtil.currencyToBigDecimal(value.toString());
                }
            }
            if (properties.get("type").toString().equalsIgnoreCase("date")) {
                if (value.toString().equalsIgnoreCase("now")) {
                    return new Timestamp(System.currentTimeMillis());
                }
                return DateUtil.stringToTimestamp(value.toString());
            }
            if (properties.get("type").toString().equalsIgnoreCase("time")) {
                if (value.toString().equalsIgnoreCase("now")) {
                    return new java.sql.Time(System.currentTimeMillis());
                }
                return DateUtil.fromStringHora(value.toString());
            }
            if (properties.get("type").toString().equalsIgnoreCase("checkbox")) {
                return value == null ? "F" : "T";
            }
            if (properties.get("type").toString().equalsIgnoreCase("array")) {
                if (value.toString().indexOf(",") > -1) {
                    return value.toString().split(",");
                }
                return value;
            }
            return value;
        } catch (final BusinessException e) {
            throw e;
        } catch (final Exception e) {
            return value;
        }

    }

    public boolean isProcess() {
        // TODO Auto-generated method stub
        return process;
    }

    public String getRelationKey() {
        GenericService s = (GenericService) get(Constants.SERVICE_KEY);

        String keyname = (String) s.getProperties().get(Constants.RELATION_KEY);
        if (keyname == null) {
            if (s.getPrimaryKey() != null) {
                keyname = (String) s.getPrimaryKey().get(Constants.name);
            }
        }
        return keyname;
    }

    public GenericVO getMasterVO(Usuario user) {
        // TODO Auto-generated method stub

        Object key = null;
        final GenericService s = ((GenericService) get(Constants.SERVICE_KEY))
                .getParent(user);
        if (s != null && s.getPrimaryKey() != null) {
            key = get(getRelationKey());
            if (key != null) {
                key = key.toString();
            }
            final GenericVO vo = s.createVO(user);
            if (key != null) {
                try {
                    vo.putKey(vo.getKey(), key);
                    vo.setLazyList(true);
                    vo.putAllAndGet(s.obtem(vo, user), true);
                } catch (final Exception e1) {
                    LogUtil.exception(e1, user);
                }
            }
            return vo;

        }
        return null;
    }

    public Map getData() {
        GenericVO n = new GenericVO().putAllAndGet(this, true);
        for (int i = 0; i < staticProps.size(); i++) {
            n.remove(staticProps.get(i));
        }
        // TODO Auto-generated method stub
        return n;
    }

    public boolean isCommitSingle() {
        return commitSingle;
    }

    public void setCommitSingle(boolean commitSingle) {
        this.commitSingle = commitSingle;
    }

    public String getSTime() {
        if (this.get("STIME") == null
                || this.get("STIME").toString().trim().length() == 0) {
            return "0";
        }
        return (String) this.get("STIME");
    }

    public void setSegmentBy(boolean b) {
        segmentBy = b;

    }

    public boolean isSegmentBy() {
        return segmentBy;
    }

    public void setAnulate(boolean b) {
        // TODO Auto-generated method stub
        anulate = b;
    }

    public boolean isAnulate() {
        return anulate;
    }

    public int getUpdatedRows() {
        // TODO Auto-generated method stub
        return updatedRows;
    }

    public GenericVO cloneDiff(GenericVO vo) {
        // TODO Auto-generated method stub
        final Iterator i = vo.keySet().iterator();
        while (i.hasNext()) {
            final String object = (String) i.next();
            if (!this.containsKey(object) && vo.getICase(object) != null) {
                super.put(object, vo.getICase(object));
            }
        }

        return this;

    }

    public String toJSON() {
        org.json.JSONObject j = new org.json.JSONObject();
        Iterator i = this.keySet().iterator();

        while (i.hasNext()) {
            String object = (String) i.next();
            try {
                j.put(object, this.get(object));
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return j.toString();
    }

    public void addIgnoredScript(String scriptName) {
        ignoredScripts.add(scriptName);
    }

    public void removeIgnoredScript(String scriptName) {
        ignoredScripts.remove(scriptName);
    }

    public boolean isIgnoredScript(String scriptName) {
        if (ignoredScripts.contains(scriptName)) {
            return true;
        }
        return false;
    }

    public Map getAliases() {
        return aliases;
    }

    public boolean isForceDistinct() {
        if (this.get("FORCEDISTINCT") != null
                && Boolean.valueOf("" + this.get("FORCEDISTINCT"))) {
            return true;
        }
        return forceDistinct;
    }

    public void setForceDistinct(boolean forceDistinct) {
        this.forceDistinct = forceDistinct;
    }

    public boolean isIgnoreIdCondition() {
        return ignoreIdCondition;
    }

    public void setIgnoreIdCondition(boolean ignoreIdCondition) {
        this.ignoreIdCondition = ignoreIdCondition;
    }

    public boolean getControlCollection() {
        // TODO Auto-generated method stub
        return this.controll;
    }

    public void setControlCollection(boolean controll) {
        this.controll = controll;
    }

	public List<String> getStaticprops() {
		// TODO Auto-generated method stub
		return staticProps;
	}

	public Object getConverted(String string, Usuario user) {
		// TODO Auto-generated method stub
		return convert(string, get(string), user);
	}

}