package com.aseg.upload;

import java.io.File;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

public class ExtendedMultiPartRequestHandler implements MultipartRequest {

	public static final long DEFAULT_SIZE_MAX = 999999999;

	public static final int DEFAULT_SIZE_THRESHOLD = 256 * 1024;

	private Hashtable elementsAll;

	private Hashtable elementsFile;

	private Hashtable elementsText;

	public Hashtable getTextElements() {
		return this.elementsText;
	}

	public Hashtable getFileElements() {
		return this.elementsFile;
	}

	public Hashtable getAllElements() {
		return this.elementsAll;
	}

	protected String getBaseFileName(String filePath) {

		String fileName = new File(filePath).getName();

		int colonIndex = fileName.indexOf(":");
		if (colonIndex == -1) {

			colonIndex = fileName.indexOf("\\\\");
		}
		int backslashIndex = fileName.lastIndexOf("\\");

		if (colonIndex > -1 && backslashIndex > -1) {

			fileName = fileName.substring(backslashIndex + 1);
		}

		return fileName;
	}

	public MultipartFile getFile(String arg0) {

		return null;
	}

	public Map getFileMap() {

		return null;
	}

	public Iterator getFileNames() {

		return null;
	}

	public List<MultipartFile> getFiles(String arg0) {

		return null;
	}

	public MultiValueMap<String, MultipartFile> getMultiFileMap() {

		return null;
	}

	@Override
	public String getMultipartContentType(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}
