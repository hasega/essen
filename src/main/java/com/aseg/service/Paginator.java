package com.aseg.service;

public interface Paginator {

	public abstract String getAlias();

	public abstract String getDir();

	/**
	 * Reset all properties to their default values.
	 * 
	 * @param mapping
	 *            The ActionMapping used to select this instance.
	 * @param request
	 *            The HTTP Request we are processing.
	 */

	public abstract String getFiltro();

	public abstract String getLimit();

	public abstract String getSort();

	public abstract String getStart();

	public abstract String getTotal();

	public abstract String getWhere();

	public abstract boolean paginate();

	public abstract void setAlias(String alias);

	public abstract void setDir(String dir);

	public abstract void setFiltro(String filtro);

	public abstract void setLimit(String limit);

	public abstract void setMultipleId(String[] multipleId);

	public abstract void setSort(String sort);

	public abstract void setStart(String start);

	public abstract void setTotal(String total);

	public abstract void setWhere(String where);

	public abstract void setSTime(String time);

}