package com.aseg.service;

import com.aseg.config.Constants;
import com.aseg.dao.DAOImpl;
import com.aseg.dao.GenericDAO;
import com.aseg.email.service.EmailService;
import com.aseg.exceptions.BusinessException;
import com.aseg.exceptions.ConfigurationException;
import com.aseg.exceptions.PersistenceException;
import com.aseg.exceptions.SessionExpiredException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.exportation.Exporter;
import com.aseg.service.exportation.GraphMLExporter;
import com.aseg.service.exportation.XMLExporter;
import com.aseg.util.ConnectionManager;
import com.aseg.util.DAOTemplate;
import com.aseg.util.apresentacao.JSONBuilder;
import com.aseg.util.service.ParseXml;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.sql.Dialeto;
import com.aseg.util.workflow.AdvancedWorkflow;
import com.aseg.util.workflow.WorkflowCommunication;
import com.aseg.vo.GenericVO;
import com.opensymphony.workflow.StoreException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.script.*;
import javax.servlet.ServletRequest;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.*;
import java.util.Map.Entry;

//import org.eclipse.wst.jsdt.debug.rhino.debugger.RhinoDebugger;
@Service
public class ServiceImpl implements GenericService {

	private static boolean debugJS = false;

	@Override
	public boolean isDefaultCRUD(final String serviceMethod) {
		return Constants.CRUD_METHODS.contains(serviceMethod);
	}

	public List<Map<String, Object>> markSelected(final List list, final String field, final Object value) {
		final List<Map<String, Object>> ret = new LinkedList<Map<String, Object>>();
		for (final Iterator it = list.iterator(); it.hasNext();) {
			final Map<String, Object> obj = (Map<String, Object>) it.next();
			if (obj.get(field) != null && obj.get(field).toString().equals(value.toString())) {
				obj.put("selected", Constants.true_str);
			}
			ret.add(obj);
		}

		return ret;
	}

	boolean cached = false;
	private int check_code;
	public GenericDAO DAO = new DAOImpl();
	public ScriptEngineManager manager = new ScriptEngineManager();
	public ScriptEngine engine = manager.getEngineByName("js");
	private final ArrayList explorer = new ArrayList();
	// private boolean ldap_up = true;
	// Rhino manager = new ScriptEngineManager();

	private Object debugger = null;
	private Map mf;
	private GenericVO mVO;
	protected String name;
	private ServiceImpl parent;
	private Map lk;
	private Map pk;

	protected Map<String, Object> properties = new HashMap();
	private final WorkflowCommunication wfc = new WorkflowCommunication();

	public WorkflowCommunication getWfc() {
		return wfc;
	}

	private static Map<String, Map> wfListeners = new HashMap();

	public ServiceImpl() {
		this.properties.put(Constants.ALL_FIELDS, new ArrayList());
	}

	public void updateListeners(Usuario user) {

		wfListeners.remove(user.getSystem());
		registerListeners(user);

	}

	public void registerListeners(Usuario user) {

		if (this.getProperties().get("forceWorkflowListener") != null)
			return;
		if (wfListeners.containsKey(user.getSystem()))
			return;

		ServiceImpl ser = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowService", user);

		GenericVO vo = ser.createVO(user);

		try {
			vo.setPaginate(false); // sem isso se tivermos mais de 20 nao viria
			List wf = ser.lista(vo, user);
			for (Iterator iterator = wf.iterator(); iterator.hasNext();) {
				Map w = (Map) iterator.next();
				AdvancedWorkflow aworkflow = new AdvancedWorkflow(new Long(w.get("ID_WORKFLOW").toString()), user);

				if (aworkflow.getWorkflowDescriptor().getMetaAttributes().get("listeners") != null) {
					String l = (String) aworkflow.getWorkflowDescriptor().getMetaAttributes().get("listeners");
					user.setProcessListeners(false);
					try {
						String[] wl = l.split(",");
						for (int i = 0; i < wl.length; i++) {

							String[] wli = wl[i].split("\\.");

							((GenericService) ser.getService(wli[0].trim(), user)).registerListener(wli[1].trim(),
									user);
						}
					} catch (Exception e) {
						// TODO: handle exception
					}
					user.setProcessListeners(true);
				}
			}
			if (!wfListeners.containsKey(user.getSystem()))
				wfListeners.put(user.getSystem(), new HashMap());
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
	}

	public String merge(final GenericVO vo, final Usuario user) throws Exception {
		final DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(
				ConnectionManager.getInstance(user).getDataSource(user));
		transactionManager
				.setTransactionSynchronization(DataSourceTransactionManager.SYNCHRONIZATION_ON_ACTUAL_TRANSACTION);
		final TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
		transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		final DAOTemplate tem = new DAOTemplate(transactionManager.getDataSource(), user, createVO(user),
				user.getUtil());
		return String.valueOf(transactionTemplate.execute(new TransactionCallback() {

			public Object doInTransaction(final TransactionStatus status) throws RuntimeException {

				try {
					Object value = obtem(vo, user).get(getPrimaryKey().get(Constants.name));
					if (value != null) {
						vo.put((String) getPrimaryKey().get(Constants.name), value);
						return altera(vo, user);
					} else
						try {
							return inclui(vo, user);
						} catch (Exception e) {
							LogUtil.exception(e, user);
						}
				} catch (Exception e) {
					LogUtil.exception(e, user);
				}
				return null;

			}
		}));

	}

	@SuppressWarnings("unchecked")
	@Override
	public String altera(final GenericVO vo, final Usuario user) throws Exception {
		final DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(
				ConnectionManager.getInstance(user).getDataSource(user));
		transactionManager
				.setTransactionSynchronization(DataSourceTransactionManager.SYNCHRONIZATION_ON_ACTUAL_TRANSACTION);
		final TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
		transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		final DAOTemplate tem = new DAOTemplate(transactionManager.getDataSource(), user, createVO(user),
				user.getUtil());
		return String.valueOf(transactionTemplate.execute(new TransactionCallback() {
			@Override
			public Object doInTransaction(final TransactionStatus status) throws RuntimeException {
				vo.setProcess(true);

				GenericVO antiga = null;

				try {
					boolean p = vo.isProcess();
					vo.setProcess(false);
					antiga = obtem(vo, user);
					vo.setOld_VO(antiga);
					vo.setProcess(p);
				} catch (final Exception e1) {

					LogUtil.exception(e1, user);
				}

				vo.validate(user);
				try {
					if (!processCalls(vo, user, "U", "before", getCalls(user, vo.getMethod()))) {

						return null;
					}
					if (!processScripts(vo, user, getScripts(user, vo.getMethod()), "U", "precedent")) {
						return vo;
					}

					List all = (List) getProperties().get(Constants.ALL_FIELDS);
					if (vo.getMethod() != null && !vo.getMethod().equalsIgnoreCase("altera")) {

						final Map m = getMethod(vo.getMethod(), user);

						if (m != null && m.get("nature") != null
								&& (m.get("nature").toString().equalsIgnoreCase("persistent")
										|| m.get("nature").toString().equalsIgnoreCase("dependent"))) {
							all = (List) m.get(Constants.ALL_FIELDS); // somente
							// campos
							// context
							// para
							// methods
							// persistentes
							for (final Iterator iterator = all.iterator(); iterator.hasNext();) {
								final Map cmp = (Map) iterator.next();

								final String[] s = cmp.get("service").toString().split("\\.");
								final ServiceImpl ser = (ServiceImpl) getService(s[0], user);
								final GenericVO v = ser.createVO(user);
								unBindVOContext(vo, v, (String) cmp.get(Constants.name), user);
								// v = v.putAllAndGet(vo, false);
								if (v.get(ser.getPrimaryKey().get(Constants.name)) == null)
									ser.invoke("inclui", v, user);
								else
									ser.invoke("altera", v, user);

							}
							return "";

						}

						if (m != null && m.get("type").toString().equalsIgnoreCase("A")) {
							all = (List) m.get(Constants.ALL_FIELDS);
							// Anulate
							final String[] s = m.get("service").toString().split("\\.");
							final ServiceImpl ser = (ServiceImpl) getService(s[0], user);
							List pr = (List) ser.getMethod(s[1], user).get(Constants.ALL_FIELDS);
							List mt = user.getUtil().filter(pr, new String[] { "type", "volatile" },
									new String[] { "array", null });
							GenericVO v = ser.createVO(user);
							v = v.putAllAndGet(vo, false);

							if (vo.getKey() != null) {
								v.putKey(vo.getKey(), vo.get(vo.getKey()));
							}

							v.setMultipleId(vo.getMultipleId());
							v.setMethod(s[1]);
							if (mt.size() > 0) {

								for (final Iterator iterator = mt.iterator(); iterator.hasNext();) {
									final Map object = (Map) iterator.next();
									v.put(object.get(Constants.name).toString(), null);
								}
							}
							Object invokeRes = ser.invoke(s[1], v, user);

							if (!processScripts(v, user, getScripts(user, vo.getMethod()), "U", "dependent")) {
								return vo;
							}

							return invokeRes;
						}
					}

					final String pkf = (String) pk.get(Constants.name);
					List hc = null;

					hc = user.getUtil().match(all, new String[] { Constants.component, "nature" },
							new String[] { "hidden_context", "precedent" });
					Iterator ih = hc.iterator();
					while (ih.hasNext()) {
						final Map cam = (Map) ih.next();
						InvokeRecursiveMethod(cam, vo, false, null, null, user);
					}

					hc = user.getUtil().match(all, new String[] { Constants.component, "nature" },
							new String[] { "context", "null" });
					ih = hc.iterator();
					while (ih.hasNext()) {

						final Map cam = (Map) ih.next();

						if (cam.get("disableWhenExpression") != null) {

							if (user.getUtil().EvaluateBoolean((String) cam.get("disableWhenExpression"),
									user.getUtil().createContext(vo, user)))
								continue;

						}
						InvokeRecursiveMethod(cam, vo, true, vo.getMethod(), null, user);
					}

					hc = user.getUtil().match(all, Constants.component, "rule");

					for (final Iterator iterator = hc.iterator(); iterator.hasNext();) { // tratando
																							// rules
																							// apenas
																							// de
						// union de campos por
						// enquanto
						final Map object = (Map) iterator.next();
						final String[] f = object.get("value").toString().split("\\|");
						for (final String element : f) {
							if (vo.get(f) != null) {
								vo.put(object.get(Constants.name).toString(), vo.get(f));
							}
						}

					}
					LogUtil.debug("$$$ VO $$$ " + vo, user);
					vo.put(pkf, getDAO().altera(vo, user));
					if (vo.isCommitSingle())
						transactionManager.commit(status);
					hc = user.getUtil().match(all, new String[] { Constants.component, "nature" },
							new String[] { "hidden_context", "dependent" });
					ih = hc.iterator();
					while (ih.hasNext()) {
						final Map cam = (Map) ih.next();
						if (cam.get("disableWhenExpression") != null) {

							if (user.getUtil().EvaluateBoolean((String) cam.get("disableWhenExpression"),
									user.getUtil().createContext(vo, user)))
								continue;

						}
						InvokeRecursiveMethod(cam, vo, false, null, null, user);
					}

					hc = user.getUtil().match(all, new String[] { Constants.component, "nature" },
							new String[] { "context", "dependent" });
					ih = hc.iterator();
					while (ih.hasNext()) {
						final Map cam = (Map) ih.next();
						if (cam.get("disableWhenExpression") != null) {

							if (user.getUtil().EvaluateBoolean((String) cam.get("disableWhenExpression"),
									user.getUtil().createContext(vo, user)))
								continue;

						}
						if (cam.get("printWhenExpression") != null) {

							if (!user.getUtil().EvaluateBoolean((String) cam.get("printWhenExpression"),
									user.getUtil().createContext(vo, user)))
								continue;

						}
						InvokeRecursiveMethod(cam, vo, true, vo.getMethod(), null, user);
					}
					GenericVO nova = null;

					if (isLogable(vo)) {
						try {
							boolean p = vo.isProcess();
							vo.setProcess(false);
							nova = obtem(vo, user);
							vo.setProcess(p);
						} catch (final Exception e1) {

							LogUtil.exception(e1, user);
						}
						user.getLogger()
								.log(user,
										new GenericVO().putAllAndGet(user.getUtil().filterMap(nova,
												new String[] { (String) getMainField(vo.getMethod(), user)
														.get(Constants.name) }),
												false),
										getName(), getProperties().get("table").toString(), antiga, nova,
										vo.getMethod(), getProperties().get("id").toString(),
										String.valueOf(tem.queryForObject(
												Dialeto.getInstance().getDialeto(user).getTransactionID(),
												String.class)));
					}

					processCalls(vo, user, "U", "after", getCalls(user, vo.getMethod()));
					if (!processScripts(vo, user, getScripts(user, vo.getMethod()), "U", "dependent")) {

						return vo;
					}

					return vo.get(pkf);

				} catch (final Exception e) {
					LogUtil.exception(e, user);
					throw new BusinessException(e.getMessage(), e);
				}
			}

		}));
	}

//	public Usuario bindUserBySessionIdAndLogin(Usuario user) {
//		try {
//
//			GenericService sessoes = (GenericService) getService("SessoesService", user);
//			GenericVO voSessionUserBD = null;
//			GenericVO voSess = null;
//
//			try {
//				voSess = sessoes.createVO(user);
//
//			} catch (Exception e) {
//				return user;
//			}
//
//			StringBuilder filtro = new StringBuilder();
//
//			filtro.append("{gA:[{c:{f:'ID_SESSAO', o:'=', v1:'" + user.getSessionId() + "'}}");
//
//			if (user.getLogin() != null && !user.getLogin().equals("")) {
//				filtro.append(",{c:{f:'LOGIN', o:'=', v1:'" + user.getLogin() + "'}}");
//			}
//
//			filtro.append("]}");
//			voSess.setFiltro(filtro.toString());
//
//			voSessionUserBD = sessoes.obtem(voSess, user);
//
//			GenericService servUsuario = (GenericService) getService("UsuarioService", user);
//			if (servUsuario == null)
//				return user;
//			GenericVO voUserBD = servUsuario.createVO(user);
//			GenericVO voUsuario = null;
//			voUserBD.setFiltro("{gA:[" +
//			// "{c:{f:'NOME', o:'=',
//			// v1:'"+voSessionUserBD.get("NOME")+"'}}, "
//			// +
//			// "{c:{f:'ID_USUARIO_CADASTRO', o:'=',
//			// v1:'"+voSessionUserBD.get("ID_USUARIO_CADASTRO")+"'}},"
//			// +
//					"{c:{f:'LOGIN', o:'=', v1:'" + voSessionUserBD.get("LOGIN") + "'}}" + "]" + "}");
//
//			voUsuario = servUsuario.obtem(voUserBD, user);
//
//			return bindUser(user, voUsuario, false);
//		} catch (Exception e) {
//			LogUtil.exception(e, user);
//		}
//		return user;
//	}

	public List<Long> getIdsUnidadesDown(Object idUnidade, Usuario user) {
		ServiceImpl unServ = (ServiceImpl) getService("UnidadeService", user);
		GenericVO unVO = unServ.createVO(user);
		unVO.put("SEGMENTBY", "DOWN");
		unVO.putKey("ID_UNIDADE_ORGANIZACIONAL", new Long("" + idUnidade));
		unVO.setFiltro("");
		unVO.setPaginate(false);

		try {
			unVO = unServ.obtem(unVO, user);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<Long> longResult = new ArrayList<Long>();
		Object[] unFilhos = (Object[]) unVO.get("ID_UNIDADE_FILHO");
		if (unFilhos != null) {
			for (Object o : unFilhos) {
				longResult.add(new Long("" + o));
			}
		}
		return longResult;
	}

	public Usuario bindUser(final Usuario user, final GenericVO valores, boolean RealEevent) {
		if (valores == null || valores.get("LOGIN") == null) {
			return user;
		}
		user.setIdUsuario(new Long(valores.get("ID_USUARIO").toString()));
		user.setSenha((String) valores.get("SENHA"));
		user.setMatricula((String) valores.get("MATRICULA"));
		user.setLogin((String) valores.get("LOGIN"));
		if (valores.get("BLOQUEADO") == null) {
			user.setIsBloqueado(false);
		} else {
			user.setIsBloqueado(valores.get("BLOQUEADO").toString().equalsIgnoreCase("T") ? true : false);
		}

		if (valores.get("DELETADO") == null) {
			user.setDeletado(false);
		} else {
			user.setDeletado(valores.get("DELETADO").toString().equalsIgnoreCase("T") ? true : false);
		}

		if (valores.get("SUPER_USER") == null) {
			user.setSuperUser(false);
		} else {
			user.setSuperUser(valores.get("SUPER_USER").toString().equalsIgnoreCase("T") ? true : false);
		}

		user.setLdap(
				valores.get("LDAP") != null && valores.get("LDAP").toString().equalsIgnoreCase("T") ? true : false);

		user.setNome(valores.get("NOME").toString());
		user.setMatricula((String) valores.get("MATRICULA"));
		user.setEmail(valores.get("EMAIL").toString());

		List unidades = new ArrayList();

		unidades.add(new Long(valores.get(Constants.Unit_ID).toString()));

		user.setIdUnidadeOrganizacional(new Long(valores.get(Constants.Unit_ID).toString()).longValue());

		Object[] perm = (Object[]) valores.get("ID_UNIDADE_CONCESSAO");
		if (perm != null)
			for (int i = 0; i < perm.length; i++) {
				unidades.add(new Long(perm[i].toString()));
			}

		user.setUnidadesVisiveis((Long[]) unidades.toArray(new Long[] {}));
		unidades.clear();

		Object[] rest = (Object[]) valores.get("ID_UNIDADE_RESTRICAO");
		if (rest != null)
			for (int i = 0; i < rest.length; i++) {
				unidades.addAll(getIdsUnidadesDown(rest[i], user));
				unidades.add(new Long(rest[i].toString()));
			}

		user.setUnidadesInvisiveis((Long[]) unidades.toArray(new Long[] {}));

		GenericService s = (GenericService) getService(Constants.Unit_Service, user);
		GenericVO v = s.createVO(user);
		v.putKey(Constants.Unit_ID, valores.get(Constants.Unit_ID));
		try {
			v = s.obtem(v, user);
			if (v.get("TIPO") == null) {
				user.setTipoUnidade("J");
			} else {
				user.setTipoUnidade(v.get("TIPO").toString());
			}
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		if (valores.get("NIVEL_PERMISSAO") != null) {
			try {
				user.setPerfil(Integer.parseInt(valores.get("NIVEL_PERMISSAO").toString()));
			} catch (Exception e) {
			}
		}
		user.setDataExpiracaoSenha(new Date(((Timestamp) valores.get("DATA_EXPIRACAO_SENHA")).getTime()));

		if (valores.get("TROCA_SENHA") == null) {
			user.setTrocaSenha(false);
		} else {
			user.setTrocaSenha(valores.get("TROCA_SENHA").toString().equalsIgnoreCase("T"));
		}
		try {
			if (RealEevent) {
				valores.setProcess(RealEevent);
				processCalls(valores, user, "L", "after", getCalls(user, "login"));
			}
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		GenericService us = (GenericService) getService("UsuarioFiltroService", user);
		GenericVO vfs = us.createVO(user);
		vfs.putKey("ID_USUARIO", user.getIdUsuario());
		vfs.setPaginate(false);

		try {
			List a = us.lista(vfs, user);
			for (Iterator iterator = a.iterator(); iterator.hasNext();) {
				Map object = (Map) iterator.next();
				List it = (List) user.getFiltros().get(object.get("CONTEXTO"));
				if (it == null)
					it = new ArrayList();
				it.add(object.get("VALOR_FILTRO"));
				user.getFiltros().put(object.get("CONTEXTO"), it);

			}
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		us = (GenericService) getService("PerfilFiltroService", user);
		vfs = us.createVO(user);
		vfs.putKey("ID_PERFIL", new Long(user.getPerfil()));
		vfs.setPaginate(false);

		try {
			List a = us.lista(vfs, user);
			for (Iterator iterator = a.iterator(); iterator.hasNext();) {
				Map object = (Map) iterator.next();
				List it = (List) user.getFiltros().get(object.get("CONTEXTO"));
				if (it == null)
					it = new ArrayList();
				it.add(object.get("VALOR_FILTRO"));
				user.getFiltros().put(object.get("CONTEXTO"), it);

			}
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		return user;
	}

	@Override
	public GenericVO bindVOContext(final GenericVO vo, final String object, final Usuario user) {
		final Iterator i = ((GenericVO) vo.clone()).keySet().iterator();
		while (i.hasNext()) {
			final Object object2 = i.next();
			if (this.createVO(user).containsKey(object2) && !GenericVO.staticProps.contains(object2)) {
				vo.put(object + "_" + object2, vo.get(object2));
				vo.remove(object2);
			} else {
				vo.remove(object2);
			}
		}

		return vo;
	}

	private Map createNode(final Object id, final Object master, final Object params, final Object text,
			final Object url, final Object titulo, final Object desc, final Object leaf, final Object model,
			final Object for_checked, final Object modal, final Object type, final Object label, final Object icon,
			final Usuario user) {
		final HashMap<String, Object> map_link = new HashMap<String, Object>();
		map_link.put("id", id);
		map_link.put("master", master);
		map_link.put("url", url);
		map_link.put("params", params);
		map_link.put("text", text);
		map_link.put("titulo", titulo);
		map_link.put("desc", desc);
		map_link.put("leaf", leaf);
		map_link.put("model", model);
		if (for_checked != null) {
			map_link.put("checked", for_checked);
		}

		map_link.put("modal", modal);
		map_link.put("type", type);
		map_link.put("label", label);
		final String ic = (String) (icon == null ? map_link.get("label") : icon);

		if (user.getIcon(ic) != null) {
			map_link.put("" + "icon",
					"ItemLinguagemAction.do?" + Constants.ActionParam + "=icon&amp;NOME_CONFIG_SISTEMA="
							+ user.getLang() + Constants.CACHE_SEPARATOR + ic
							+ "&amp;sz=32&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
		}
		return map_link;
	}

	@Override
	public GenericVO createVO(final Usuario user) {
		if (this.mVO == null) {
			List cam = (List) getProperties().get(Constants.ALL_FIELDS);
			final List dsor = user.getUtil().filter(cam, "defaultSort", null);
			String sor = null;
			boolean desc = false;
			if (!dsor.isEmpty()) {

				if (((Map) dsor.get(0)).get("defaultSort").equals("desc")) {
					desc = true;
				}
				if (((Map) dsor.get(0)).get("CDATA") != null) {
					sor = ((Map) dsor.get(0)).get("CDATA").toString().trim();
				} else {
					sor = (String) ((Map) dsor.get(0)).get(Constants.name);
				}
			}

			cam = user.getUtil().filter(cam, new String[] { "type", "type" },
					new String[] { "sqlrule", "imageSelector" });// order

			final List ds = user.getUtil().match(cam, "defaultSearch", Constants.true_str);
			String search = null;
			if (!ds.isEmpty()) {
				search = (String) ((Map) ds.get(0)).get(Constants.name);
			}

			if (sor == null && getMainField("lista", user) != null) {
				sor = (String) getMainField("lista", user).get(Constants.name);
			}

			String pkf = "";
			if (pk != null) {
				pkf = (String) pk.get(Constants.name);
			}
			final GenericVO vo = new GenericVO();
			vo.setKey(pkf);
			vo.searchBy(search);
			vo.setSort(sor);
			if (desc) {
				vo.setDir("DESC");
			} else {
				vo.setDir("ASC");
			}

			for (final Iterator iterator = cam.iterator(); iterator.hasNext();) {
				final Map e = (Map) iterator.next();
				if (e.get(Constants.component) != null
						&& e.get(Constants.component).toString().equalsIgnoreCase("context")) {
					GenericService s = (GenericService) getService(e.get("service").toString(), user);
					if (s == null) {
						continue;
					}
					vo.putAll(user.getUtil().renameKeys(s.createVO(user), e.get(Constants.name).toString()));

					if (e.get("contextParams") != null) {
						vo.putAll(
								user.getUtil()
										.renameKeys(
												user.getUtil()
														.JSONtoMap(user.getUtil().replace(
																user.getUtil()
																		.removeInvalidItemsFromContextParams(
																				e.get("contextParams"))
																		.toString(),
																"!", "")),
												e.get(Constants.name).toString()));
					}
				}
				if (e.get("value") != null && !e.get("value").toString().startsWith("${")) {
					vo.put(e.get(Constants.name).toString(), e.get("value").toString());
				} else {
					if (e.get(Constants.type) != null && e.get(Constants.type).equals("bit"))
						vo.put(e.get(Constants.name).toString(), "F");
					else
						vo.put(e.get(Constants.name).toString(), null);
				}

			}
			if (cached) {
				vo.searchBy(Constants.CACHE);
			}
			this.mVO = vo;
			this.mVO.setValidationProperties((List) getProperties().get(Constants.ALL_FIELDS));
		}
		final GenericVO v = (GenericVO) this.mVO.clone();

		return v;
	}

	public GenericVO createEmptyVO(Usuario user) {
		GenericVO emptyVO = this.createVO(user);
		for (Entry<String, Object> v : emptyVO.entrySet()) {
			if (!v.getKey().equals(Constants.ActionParam)) {
				emptyVO.put(v.getKey(), null);
			}
		}
		return emptyVO;
	}

	public void loopLista(final GenericVO vo, String src, final Usuario user) {
		vo.setPaginate(true);
		vo.setProcess(false);
		Paginator p = vo;
		List a = null;
		Long time = System.currentTimeMillis();
		try {
			a = lista(vo, user);
		} catch (ConfigurationException e) {
			LogUtil.exception(e, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		final Compilable compilingEngine = (Compilable) engine;
		CompiledScript eng = null;
		if (!debugJS) {
			try {
				eng = compilingEngine.compile(src);
			} catch (ScriptException e) {
				LogUtil.error(src);
				LogUtil.exception(e, user);
			}
		}

		if (Integer.parseInt(p.getLimit()) > a.size())
			p.setLimit(String.valueOf(a.size()));

		while (Integer.parseInt(p.getTotal()) >= Integer.parseInt(p.getStart()) + Integer.parseInt(p.getLimit())) {
			LogUtil.debug("Interando bloco com inicio " + p.getStart() + " para " + p.getLimit()
					+ " registros  de um total " + p.getTotal() + " tempo total "
					+ user.getUtil().millisecondsToMinutes(System.currentTimeMillis() - time), user);
			final Bindings bindings = engine.createBindings();
			bindings.put("user", user);
			bindings.put("sm", this);
			bindings.put("util", user.getUtil());
			bindings.put("df", user.getUtil().df);
			// bindings.put("cmp", new TagInputExt());
			for (Iterator iterator = a.iterator(); iterator.hasNext();) {
				Object object = iterator.next();
				LogUtil.debug("Interando > " + object, user);
				bindings.put(Constants.VOKey, object);

				Object r = null;
				try {
					if (debugJS) {
						// engine.setContext((ScriptContext) ctx.e);
						r = engine.eval(src, bindings);
					} else {
						r = eng.eval(bindings);
					}
				} catch (final ScriptException e) {
					LogUtil.exception(e, user);

					continue;

				}

			}
			p.setStart("" + (Integer.parseInt(p.getStart()) + Integer.parseInt(p.getLimit())));
			try {
				a = lista(vo, user);
			} catch (ConfigurationException e) {
				LogUtil.exception(e, user);
			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

		}

	}

	@Override
	public List describe(final GenericVO vo, final Usuario user) {

		final ServletRequest req = (ServletRequest) vo.get(Constants.RequestProcessed);

		// vo.setEncapsulate(false);
		String item = (String) vo.get("NODE");
		final String Type = (String) vo.get("LABELPROPERTY");
		final String navigate = (String) vo.get("MODEL");
		boolean for_checked = false;

		if (Type != null && Type.toString().startsWith("filter")) {
			return user.getSysParser().describe(vo, user);
		}
		if (vo.get("FOR_CHECKED") != null && vo.get("FOR_CHECKED").toString().equalsIgnoreCase(Constants.true_str)) {
			for_checked = true;
		}
		if (Type != null && Type.toString().startsWith("bean") && navigate.equalsIgnoreCase("navigate")) {
			return navigateBean(vo, user);
		} else if (navigate.equals("explorer")) { // usado para explorar o
			// sistema ex permissoes e
			// help
			String ck = ",checked:false";
			String id = item.split("_")[0];
			GenericService s = (GenericService) getServiceById(id, user);
			if (s == null) {
				s = (GenericService) getService("SystemService", user);
				item = (String) s.getProperties().get("id");
			}
			if (Type.startsWith("bean")) {
				final ArrayList l = new ArrayList();
				if (s.getParent(user) != s && item.endsWith("props")) {
					List fields = (List) s.getProperties().get(Constants.ALL_FIELDS);
					fields = user.getUtil().filter(fields, "volatile", "true");
					// if (for_checked) {
					fields = user.getUtil().filter(fields,
							new String[] { Constants.component, "required", "xtype", "type", "type", "mainField" },
							new String[] { "hidden", Constants.true_str, "container", "sqlrule", "scriptrule",
									Constants.true_str });
					// }
					for (final Iterator f = fields.iterator(); f.hasNext();) {
						final Map field = (Map) f.next();

						if (field.get("label") == null) {
							continue;
						}
						l.add(user.getUtil()
								.JSONtoMap("{id: '" + field.get("label") + "',master :'" + field.get("parent_id")
										+ "' ,url :'" + item + "',desc :'" + item + "',params :'" + item + "',text :'"
										+ user.getLabel((String) field.get("label")) + "',titulo :'" + item
										+ "',type :'" + item + "' ,value:'" + field.get("label").hashCode() + "', "
										+ getIcon(user, "") + " " + ck + " , leaf:true}"));

					}
				} else if (item.endsWith("methods")) {
					List methods = s.getMethods(user);
					methods = user.getUtil().filter(methods, new String[] { "type", "type" },
							new String[] { "L", "R" });
					for (final Iterator m = methods.iterator(); m.hasNext();) {
						final Map met = (Map) m.next();

						if (met.get("label") == null) {
							continue;
						}
						l.add(user.getUtil()
								.JSONtoMap("{id: '" + met.get("label") + "',master :'" + item + "' ,url :'" + item
										+ "',desc :'" + item + "',params :'" + item + "',text :'"
										+ user.getLabel((String) met.get("label")) + "',titulo :'" + item + "',type :'"
										+ item + "',value:'" + met.get("label").hashCode() + "', " + getIcon(user, "")
										+ " " + ck + " , leaf:true}"));

					}

					String[] defauts = new String[] { "inclui", "altera", "remove", "importa", "exporta" };

					if (s.getProperties().get("table") != null) {
						for (String defaut : defauts) {

							l.add(user.getUtil()
									.JSONtoMap("{id: '" + s.getProperties().get("table") + "." + defaut + "',master :'"
											+ item + "' ,url :'" + item + "',desc :'" + item + "',params :'" + item
											+ "',text :'" + user.getLabel("label." + defaut) + "',titulo :'" + item
											+ "',type :'" + item + "',value:'"
											+ (s.getProperties().get("table") + "." + defaut).hashCode() + "', "
											+ getIcon(user, "") + " " + ck + " , leaf:true}"));

						}
					}
				} else

				{

					List modules = s.getChilds(user);

					if (!item.endsWith("modules") && Type.equals("bean") && s.getParent(user) != s) {
						ck = "";
						l.add(user.getUtil()
								.JSONtoMap("{id: '" + s.getProperties().get("id") + "_props',disabled:true,master :'"
										+ item + "' ,url :'" + item + "',desc :'" + item + "',params :'" + item
										+ "',text :'" + user.getLabel("label.propriedades") + "',titulo :'" + item
										+ "',type :'bean_fields', " + getIcon(user, "folder") + " ,value:'' " + ck
										+ "  }"));
						l.add(user.getUtil().JSONtoMap("{id: '" + s.getProperties().get("id")
								+ "_methods', disabled:true,   master :'" + item + "' ,url :'" + item + "',desc :'"
								+ item + "',params :'" + item + "',text :'" + user.getLabel("label.metodos")
								+ "',titulo :'" + item + "',type :'bean_methods', " + getIcon(user, "folder")
								+ " ,value:''" + ck + "   }"));
						l.add(user.getUtil().JSONtoMap("{id: '" + s.getProperties().get("id")
								+ "_modules' ,disabled:true ,master :'" + item + "' ,url :'" + item + "',desc :'" + item
								+ "',params :'" + item + "',text :'" + user.getLabel("label.sub_modulos")
								+ "',titulo :'" + item + "',type :'bean_modules', " + getIcon(user, "folder")
								+ " ,value:'' " + ck + "  }"));

					}

					else if (l.size() == 0 && modules.size() > 0) {

						for (final Iterator iterator = modules.iterator(); iterator.hasNext();) {
							final ServiceImpl m = (ServiceImpl) iterator.next();

							if (m.getProperties().get("label") == null) {
								continue;
							}
							l.add(user.getUtil().JSONtoMap("{id: '" + m.getProperties().get("id") + "',master :'"
									+ m.getProperties().get("id") + "_modules" + "' ,url :'" + item + "',desc :'" + item
									+ "',params :'" + m.getProperties().get("master") + "',text :'"
									+ user.getLabel((String) m.getProperties().get("label")) + "',titulo :'" + item
									+ "',type :'" + item + "_modules', " + getIcon(user, "") + " ,value:'"
									+ m.getProperties().get("label").hashCode() + "'" + ck + "}"));

						}

					}
				}

				return l;
			}
			if (Type.equals("properties")) {
				return user.getUtil().mappingCollection(
						(List<Map<String, String>>) s.getProperties().get(Constants.ALL_FIELDS),
						"{parent_id:'master', label:'text',false:'checked',true:'leaf',property:'type'}", "L", true,
						user);
			}
		}
		final List cam = user.getUtil().filter((List) properties.get(Constants.ALL_FIELDS), new String[] { "fake" },
				new String[] { Constants.true_str });
		vo.setTotal("" + cam.size());
		return cam;

	}

	/**
	 * Verifica se um registro existe no banco. Recebe uma VO com as
	 * chaves:valor (chaves!)
	 */
	@Override
	public boolean existe(final GenericVO chavesDoRegistro, final Usuario user) throws Exception {

		try {

			final GenericVO vo = this.obtem(chavesDoRegistro, user);

			final Iterator iterador = vo.keySet().iterator();

			while (iterador.hasNext()) {
				final Object o = iterador.next();
				if (vo.get(o) == null || vo.get(o).equals("null") || vo.get(o).equals("")) {
					return false;
				}
			}

			return true;

		} catch (final StoreException e) {
			LogUtil.exception(e, user);
		} catch (final Exception e) {
			LogUtil.exception(e, user);
		}

		return false;
	}

	public void exportBean(final GenericVO vo, final Usuario user, final Writer writer) throws Exception {
		final String beanName = getProperties().get(Constants.name).toString();
		final Exporter exporter = new XMLExporter(this, user, writer, user.getEncode());
		vo.setLazyList(false);
		vo.setPaginate(false);
		vo.setExport(true);
		List<GenericVO> vos = lista(vo, user);
		for (Iterator iterator = vos.iterator(); iterator.hasNext();) {
			GenericVO genericVO = (GenericVO) iterator.next();
			// Fazendo obtem para trazer todos os campos inclusive as MMs
			genericVO = obtem(genericVO, user);
			genericVO.setExport(true);
			exporter.export(beanName, genericVO);
		}

		exporter.flush();
	}

	public void exportListBean(final List<GenericVO> lista, final Usuario user, final Writer writer) throws Exception {
		final String beanName = getProperties().get(Constants.name).toString();
		final Exporter exporter = new XMLExporter(this, user, writer, user.getEncode());
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			GenericVO genericVO = (GenericVO) iterator.next();
			exporter.export(beanName, genericVO);
		}

		exporter.flush();
	}

	public Usuario findUsuario(final String login, final Usuario user) throws Exception {
		ServiceImpl s = (ServiceImpl) getService("ConfiguracaoSistemaService", user);

		final String sn = ((ParseXml) user.getSysParser()).getValueFromXQL(
				"/systems/system[@name='" + user.getSchema() + "']/module/bean[@auth='true']", Constants.name, user,
				true, false) + Constants.SERVICE_BASE;
		s = (ServiceImpl) getService(sn, user);
		final GenericVO v = s.createVO(user);
		v.setLazyList(true);
		v.setSegmentBy(false);
		final GenericVO valores = s.obtem(v.putKey("LOGIN", login).ignoreCase(), user);

		return bindUser(user, valores, true);
	}

	public Map getUsuario(final Usuario user) throws Exception {
		ServiceImpl s = (ServiceImpl) getService("ConfiguracaoSistemaService", user);

		final String sn = ((ParseXml) user.getSysParser()).getValueFromXQL(
				"/systems/system[@name='" + user.getSchema() + "']/module/bean[@auth='true']", Constants.name, user,
				true, false) + Constants.SERVICE_BASE;
		s = (ServiceImpl) getService(sn, user);
		final GenericVO v = s.createVO(user);
		v.setLazyList(true);

		return s.obtem(v.putKey("ID_USUARIO", user.getIdUsuario()), user);

	}

	// (Map) properties.get(Constants.SCRIPTS)
	public List getCalls(final Usuario user, final String met) {
		if (isDefaultCRUD(met)) {
			if (properties.get(Constants.CALLS) == null) {
				final List m = (List) ((ParseXml) user.getSysParser()).getListValuesFromXQL("/systems/system[@name='"
						+ user.getSchema() + "']/module/bean[@id='" + properties.get("id") + "']/call", null, user,
						false, true);

				properties.put(Constants.CALLS, m);
			}
			return (List) properties.get(Constants.CALLS);
		} else {
			if (getMethod(met, user) == null) {
				return null;
			}
			return (List) getMethod(met, user).get(Constants.CALLS);
		}
	}

	public int getCheck_code() {
		return check_code;
	}

	@Override
	public List getChilds(final Usuario user) {
		final Collection<Map<String, String>> n = user.getSysParser()
				.getListValuesFromXQL("/systems/system[@name='" + user.getSchema()
						+ "']/module/bean[@type != 'F' and @master='" + properties.get("id") + "']", Constants.name,
						user, true, true);
		final List c = new ArrayList<GenericService>();
		for (final Object element : n) {
			final Map object = (Map) element;
			c.add(getServiceById((String) object.get("id"), user));
		}
		return c;
	}

	@Override
	public GenericDAO getDAO() {
		return DAO;
	}

	public EmailService getEmailService(final Usuario user) {
		return (EmailService) getService("emailService", user);
	}

	@Override
	public List getExplorerPropertyes(final Usuario user) {
		if (explorer.size() > 0) {
			return explorer;
		} else if (getParent(user) != null) {
			return getParent(user).getExplorerPropertyes(user);
		} else {
			return explorer;
		}
	}

	private String getIcon(final Usuario user, final String label) {
		if (user.getIcon(label) != null) {
			return "" + "icon:" + "'ItemLinguagemAction.do?" + Constants.ActionParam + "=icon&amp;NOME_CONFIG_SISTEMA="
					+ user.getLang() + Constants.CACHE_SEPARATOR + label
					+ "&amp;sz=32&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache'";
		} else {
			return "iconCls:'no-icon'";
		}
	}

	@Override
	public Map getMainField(final String method, final Usuario user) {

		if (isDefaultCRUD(method)) {
			return mf;
		} else {
			try {
				final Map t = (Map) user.getUtil().match((List) getMethod(method, user).get(Constants.ALL_FIELDS),
						"mainField", Constants.true_str).get(0);
				return t;
			} catch (final Exception e) {
				return mf;
			}
		}
	}

	@Override
	public Map getMethod(final String name, final Usuario user) {
		try {
			return (Map) user.getUtil().match(getMethods(user), Constants.name, name).get(0);
		} catch (final Exception e) {
			return null;
		}
	}

	// Global Refactoring
	// Se possui um service Master resgata seu attributo, se nao procura o Sei
	// Atributo

	@Override
	public List<Map<String, Object>> getMethods(final Usuario user) {

		if (properties.get(Constants.ALL_METHODS) == null && user != null) {

			final List<Map<String, Object>> methods = (List) user.getSysParser()
					.getListValuesFromXQL("/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='"
							+ properties.get("id") + "']/method[contains(@layers,'S')]", null, user, true, true);

			for (final Map<String, Object> element : methods) {
				element.put(Constants.ALL_FIELDS,
						getDAO().whitExtensions((List) ((ParseXml) user.getSysParser()).getListValuesFromXQL(
								"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='"
										+ properties.get("id") + "']/method[@name='" + element.get(Constants.name)
										+ "']/*[name() = \"property\" or name() = \"container\"]",
								null, user, true, true), user));
				List sc = (List) element.get(Constants.ALL_FIELDS);

				List ctx = user.getUtil().match(sc, new String[] { "component" }, new String[] { "context" });

				for (Iterator iterator = ctx.iterator(); iterator.hasNext();) {
					Map object = (Map) iterator.next();
					sc.addAll(user.getUtil()
							.match((List) ((GenericService) getService(object.get("service").toString(), user))
									.getProperties().get(Constants.ALL_FIELDS), new String[] { "type" },
									new String[] { "scriptRule" }));
				}

				final List scripts = user.getUtil().match(sc, new String[] { "type" }, new String[] { "scriptRule" });
				final Map src = new HashMap();

				for (final Iterator iterato = scripts.iterator(); iterato.hasNext();) {
					final Map objec = (Map) iterato.next();

					final Compilable compilingEngine = (Compilable) engine;

					try {
						final Map srcvalue = new HashMap();
						if (debugJS) {
							srcvalue.put("src", objec.get("CDATA"));
						} else {
							srcvalue.put("src", objec.get("CDATA"));

							srcvalue.put("src_cmp", compilingEngine.compile((String) objec.get("CDATA")));
						}
						srcvalue.put("nature", objec.get("nature"));
						srcvalue.put("serviceMethod", objec.get("serviceMethod"));

						src.put(objec.get(Constants.name), srcvalue);
					} catch (final ScriptException e) {
						LogUtil.exception(e, user);
					}
				}
				element.put(Constants.SCRIPTS, src);
				element.put(Constants.CALLS, ((ParseXml) user.getSysParser()).getListValuesFromXQL(
						"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + properties.get("id")
								+ "']/method[@name='" + element.get(Constants.name) + "']/call",
						null, user, true, true));
				createVO(user).setValidationProperties((List<Object>) element.get(Constants.ALL_FIELDS));
			}
			properties.put(Constants.ALL_METHODS, methods);
		}

		List retu = new ArrayList();

		List all = (List) properties.get(Constants.ALL_METHODS);

		for (Iterator iterator = all.iterator(); iterator.hasNext();) {
			Map object = (Map) iterator.next();
			if (ParseXmlService.canAccess(object, user, true)) {
				retu.add(object);
			}

		}

		return retu;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public ServiceImpl getParent(final Usuario user) {

		return (ServiceImpl) getServiceById((String) getProperties().get("master"), user);
	}

	@Override
	public Map getPrimaryKey() {
		return pk;
	}

	@Override
	public Map getListKey() {
		return lk;
	}

	@Override
	public Map<String, Object> getProperties() {
		return new HashMap<String, Object>(properties);
	}

	@Override
	public Map getProperty(final String object, Usuario user) {

		return (Map) user.getUtil()
				.match((List) getAllFields(user), new String[] { Constants.name }, new String[] { object }).get(0);
	}

	// (Map) properties.get(Constants.SCRIPTS)
	public Map getScripts(final Usuario user, final String met) {
		if (isDefaultCRUD(met) || met == null || met.trim().length() == 0) {
			if (properties.get(Constants.SCRIPTS) == null) {
				final List scripts = user.getUtil().match((List) this.properties.get(Constants.ALL_FIELDS),
						new String[] { "type" }, new String[] { "scriptRule" });
				final Map src = new HashMap();
				for (final Iterator iterator = scripts.iterator(); iterator.hasNext();) {
					final Map object = (Map) iterator.next();

					final Compilable compilingEngine = (Compilable) engine;

					try {
						final Map srcvalue = new HashMap();
						if (debugJS) {
							srcvalue.put("src", object.get("CDATA"));
						} else {
							srcvalue.put("src", object.get("CDATA"));

							srcvalue.put("src_cmp", compilingEngine.compile((String) object.get("CDATA")));
						}
						srcvalue.put("nature", object.get("nature"));
						srcvalue.put("serviceMethod", object.get("serviceMethod"));

						src.put(object.get(Constants.name), srcvalue);

					} catch (final ScriptException e) {
						LogUtil.fatal("ERRO AO COMPILAR SCRIPT " + object.get("CDATA"));
						LogUtil.exception(e, user);
					}
				}
				this.properties.put(Constants.SCRIPTS, src);
			}
			try {
				return (Map) properties.get(Constants.SCRIPTS);
			} catch (Exception e) {
				return null;
			}
		} else {
			if (getMethod(met, user) == null) {
				return null;
			}
			return (Map) getMethod(met, user).get(Constants.SCRIPTS);
		}
	}

	@Override
	public Object getService(final String name, final Usuario user) {
		LogUtil.debug("Service \"" + name + "\" foi requerido " + parseActualUser(user) + "", user);

		return user.getFactory().getSpringBean(name, user);
	}

	@Override
	public Object getServiceById(final String name, final Usuario user) {
		final String Name = ((ParseXml) user.getSysParser()).getValueFromXQL(
				"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + name + "']", Constants.name,
				user, true, false);
		try {
			if (Name.length() == 0) {
				return null;
			}
			return user.getFactory().getSpringBean(Name + "Service", user);
		} catch (final Exception e) {
			return null;
		}
	}

	public String getServiceProperty(final String service, final String property, final String id, final Usuario user) {
		try {
			return (String) ((GenericService) getService(service, user)).obtem(null, user).get(property);
		} catch (final Exception e) {
			return null;
		}
	}

	@Override
	public String inclui(final GenericVO vo, final Usuario user) throws Exception {

		final DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(
				ConnectionManager.getInstance(user).getDataSource(user));
		transactionManager.setTransactionSynchronization(transactionManager.SYNCHRONIZATION_ON_ACTUAL_TRANSACTION);
		final TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
		transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		final DAOTemplate tem = new DAOTemplate(transactionManager.getDataSource(), user, createVO(user),
				user.getUtil());
		return (String) transactionTemplate.execute(new TransactionCallback() {
			@Override
			public Object doInTransaction(final TransactionStatus status) throws RuntimeException {
				final String pkf = (String) pk.get(Constants.name);
				vo.setProcess(true);
				vo.validate(user);

				try {
					if (!processCalls(vo, user, "C", "before", getCalls(user, vo.getMethod()))) {
						return null;
					}
					if (!processScripts(vo, user, getScripts(user, vo.getMethod()), "C", "precedent")) {
						GenericVO genericVO = vo;

						if (vo.get(pkf) != null) {
							return vo.get(pkf).toString();
						} else {
							return vo.get(pkf);
						}
					}

					List all = (List) getProperties().get(Constants.ALL_FIELDS);

					if (vo.getMethod() != null && !vo.getMethod().equalsIgnoreCase("inclui")) {
						final Map m = getMethod(vo.getMethod(), user);

						if (m != null && m.get("nature") != null
								&& m.get("nature").toString().equalsIgnoreCase("persistent")) {
							all = (List) m.get(Constants.ALL_FIELDS); // somente
							// campos
							// context
							// para
							// methods
							// persistentes
							for (final Iterator iterator = all.iterator(); iterator.hasNext();) {
								final Map cmp = (Map) iterator.next();

								final String[] s = cmp.get("service").toString().split("\\.");
								final ServiceImpl ser = (ServiceImpl) getService(s[0], user);
								final GenericVO v = ser.createVO(user);
								unBindVOContext(vo, v, (String) cmp.get(Constants.name), user);
								// v = v.putAllAndGet(vo, false);

								ser.invoke("inclui", v, user);

							}
							return "";

						}
					}

					List hc = user.getUtil().match(all, new String[] { Constants.component, "nature" },
							new String[] { "hidden_context", "precedent" });
					Iterator ih = hc.iterator();
					while (ih.hasNext()) {
						final Map cam = (Map) ih.next();
						if (cam.get("disableWhenExpression") != null) {

							if (user.getUtil().EvaluateBoolean((String) cam.get("disableWhenExpression"),
									user.getUtil().createContext(vo, user)))
								continue;

						}
						InvokeRecursiveMethod(cam, vo, false, null, null, user);
					}

					hc = user.getUtil().match(all, new String[] { Constants.component, "nature" },
							new String[] { "context", "precedent" });
					ih = hc.iterator();
					while (ih.hasNext()) {
						final Map cam = (Map) ih.next();
						if (cam.get("disableWhenExpression") != null) {

							if (user.getUtil().EvaluateBoolean((String) cam.get("disableWhenExpression"),
									user.getUtil().createContext(vo, user)))
								continue;

						}
						InvokeRecursiveMethod(cam, vo, true, "inclui", null, user);
					}
					hc = user.getUtil().match(all, Constants.component, "rule");

					// tratando rules apenas de union de campos por enquanto
					for (final Iterator iterator = hc.iterator(); iterator.hasNext();) {
						final Map object = (Map) iterator.next();
						final String[] f = object.get("value").toString().split("\\|");
						for (final String element : f) {
							if (vo.get(element.trim()) != null) {
								vo.put(object.get(Constants.name).toString(), vo.get(element.trim()));
							}
						}

					}

					vo.put(pkf, getDAO().inclui(vo, user));
					hc = user.getUtil().match(all, new String[] { Constants.component, "nature" },
							new String[] { "hidden_context", "dependent" });
					ih = hc.iterator();
					while (ih.hasNext()) {
						final Map cam = (Map) ih.next();
						if (cam.get("disableWhenExpression") != null) {

							if (user.getUtil().EvaluateBoolean((String) cam.get("disableWhenExpression"),
									user.getUtil().createContext(vo, user)))
								continue;

						}
						InvokeRecursiveMethod(cam, vo, false, null, null, user);
					}

					if (!vo.isLazyList()) {
						hc = user.getUtil().match(all, new String[] { Constants.component, "nature" },
								new String[] { "context", "dependent" });
						ih = hc.iterator();
						boolean modified = false;
						while (ih.hasNext()) {
							final Map cam = (Map) ih.next();
							if (cam.get("disableWhenExpression") != null) {

								if (user.getUtil().EvaluateBoolean((String) cam.get("disableWhenExpression"),
										user.getUtil().createContext(vo, user)))
									continue;

							}
							final Object a = InvokeRecursiveMethod(cam, vo, true, "inclui", null, user);
							if (vo.containsKey(cam.get(Constants.name)) && (cam.get("volatile") == null
									|| cam.get("volatile").toString().equalsIgnoreCase(Constants.false_str))) {
								modified = true;
								vo.put(cam.get(Constants.name).toString(), a);
							}
						}

						if (modified) {
							vo.setMethod("altera");
							getDAO().altera(vo, user);
							vo.setMethod("inclui");
						}
					}

					if (isLogable(vo)) {
						boolean b = vo.isProcess();
						vo.setProcess(false);
						Map nova = obtem(vo, user);
						vo.setOld_VO((GenericVO) nova);
						vo.setProcess(b);
						String idt = String.valueOf(tem.queryForObject(
								Dialeto.getInstance().getDialeto(user).getTransactionID(), String.class));
						if (idt != null)
							user.getLogger()
									.log(user,
											new GenericVO()
													.putAllAndGet(
															user.getUtil()
																	.filterMap(nova,
																			new String[] { (String) getMainField(
																					vo.getMethod(), user)
																							.get(Constants.name) }),
															false),
											getName(), getProperties().get("table").toString(), new HashMap(), nova,
											vo.getMethod(), getProperties().get("id").toString(), idt);
					}

					processCalls(vo, user, "C", "after", getCalls(user, vo.getMethod()));

					if (!processScripts(vo, user, getScripts(user, vo.getMethod()), "C", "dependent")) {
						if (vo.get(pkf) == null)
							return "";
						return vo.get(pkf).toString();
					}

					if (vo.get(pkf) == null)
						return "";
					return vo.get(pkf).toString();
				} catch (final BusinessException e) {
					if (LogUtil.debugEnabled(user)) {
						LogUtil.exception(e, user);
					}
					throw e;
				} catch (final Exception ex) {
					if (LogUtil.debugEnabled(user)) {
						LogUtil.exception(ex, user);
					}
					throw new BusinessException(ex);
				}
			}

		});
	}

	@Override
	public Object invoke(final String method, final GenericVO vo, final Usuario user) throws Exception {

		LogUtil.debug("AGORA INVOKE >>>>" + getName() + " >> " + method, user);
		Method m = ReflectionUtils.findMethod(this.getClass(), method, new Class[] { GenericVO.class, Usuario.class });

		if (m == null) // criando metodos injetados.
		{
			final List ma = user.getUtil().match(getMethods(user), Constants.name, method.trim());
			if (ma != null && ma.size() > 0) {
				final Map<String, String> j = (Map<String, String>) ma.get(0);
				j.put("master", (String) properties.get("id"));
				vo.setMethod(method);
				return resolvInjectMethod(j, user, vo);
			}
		}
		try {
			return ReflectionUtils.invokeMethod(m, this, new Object[] { vo, user });
		}

		catch (final BusinessException e) {
			throw e;
		}

		catch (final Exception e) {

			LogUtil.debug("Erro na chamada do methodo  > " + getProperties().get(Constants.name) + "Service." + method,
					user);
			LogUtil.exception(e, user);
			return null;
		}
	}

	private Object InvokeRecursiveMethod(final Map cam, final GenericVO vo, final boolean bindVOforContext,
			final String method, final String params, final Usuario user) throws Exception {

		GenericService service = null;
		try {
			service = (GenericService) user.getFactory().getSpringBean(cam.get("service").toString(), user);
		} catch (final Exception e) {
			LogUtil.exception(e, user);
		}
		if (service == null) {
			return null;
		}

		GenericVO nvo = service.createVO(user);

		boolean maped = true;
		if (params != null || cam.get("contextParams") != null) {
			final Map par = user.getUtil().JSONtoMap(params == null ? user.getUtil().replace(
					user.getUtil().removeInvalidItemsFromContextParams(cam.get("contextParams")).toString(), "!", "")
					: params);
			final Iterator p = par.keySet().iterator();
			while (p.hasNext()) {
				final Object object = p.next();
				if (object.toString().equalsIgnoreCase(Constants.SEARCH_BY)) {
					maped = false;
					nvo.searchBy(user.getUtil().replace(par.get(object).toString(), "|", ","));
				} else {
					nvo.put(object.toString(), par.get(object));
				}

			}

		}

		nvo = processBinder(nvo, vo, user);
		Object m = null;

		if (maped) {
			final Object v = vo.get(cam.get(Constants.name));

			// o campo context é volatil devemos buscar o valor da key
			if (v == null && cam.get("contextParams") != null) {
				Map p = user.getUtil()
						.JSONtoMap((String) user.getUtil().replace(
								user.getUtil().removeInvalidItemsFromContextParams(cam.get("contextParams").toString()),
								"!", ""));
				nvo.setFiltro(user.getUtil().mergeFiltro(nvo.getFiltro(), user.getUtil().createCriteria(p, "=", "A")));
			} else {
				nvo.putKey((String) cam.get("key") == null ? cam.get(Constants.name) : cam.get("key"), v);
			}
		}

		if (bindVOforContext) {
			nvo = service.unBindVOContext(vo, nvo, cam.get(Constants.name).toString(), user);
		}
		if (m == null) {
			String met = method == null ? cam.get("serviceMethod").toString() : method;

			if (!Constants.CRUD_METHODS.contains(met)) {
				if (nvo.get(service.getPrimaryKey().get("name")) == null) { // vai
																			// realizar
																			// inclusão
					met = "inclui";
				} else { // vai realizar alteração
					met = "altera";
				}
			}

			nvo.setMethod(met);
			m = service.invoke(met, nvo, user);
		}

		if (m == null) {
			return m;
		}
		if (m instanceof String) {
			vo.put(cam.get(Constants.name).toString(), m);
		} else {
			vo.putAllAndGet(service.bindVOContext(service.createVO(user).putAllAndGet((Map) m, true),
					cam.get(Constants.name).toString(), user), false);
		}

		return m;

	}

	private boolean keysetItemLike(Map vo, String searchValue) {

		boolean result = false;

		Object keySet[] = vo.keySet().toArray();

		for (Object element : keySet) {
			if (((String) element).startsWith(searchValue)) {
				result = true;
				break;
			}
		}

		return result;
	}

	private Usuario ldap(Usuario user, final String login, final String senha)
			throws UnknownHostException, IOException, Exception {

		final ServiceImpl usu = (ServiceImpl) getService("UsuarioService", user);
		GenericVO vo = null;

		try {
			user = findUsuario(login, user);
		} catch (final Exception e1) {
			LogUtil.exception(e1, user);
		}

		if (user.SystemPropertyIsTrue("LDAP_AUTH")) {

			try {
				if (user.SystemPropertyIsTrue("LDAP_FORCE_HOST_VERIFY")) {
					testLdapHostIsReachable(user);
				}

				vo = usu.createVO(user);
				vo.setLazyList(true);
				final Hashtable environment = prepareEnvironment(user);

				// Create the initial directory context
				LdapContext ctx = createLdapContext(user, environment);

				// Create the search controls
				final SearchControls searchCtls = new SearchControls();

				// Specify the search scope
				searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

				// specify the LDAP search filterLDAP_SEARCH_FILTER
				final String searchFilter = user.getUtil().replace(user.getSystemProperty("LDAP_SEARCH_FILTER"), "#?#",
						login);

				// Specify the Base for the search
				final String searchBase = user.getSystemProperty("LDAP_SEARCH_BASE");

				// initialize counter to total the group members
				int totalResults = 0;

				// Specify the attributes to return
				final String returnedAtts[] = { "*" };
				searchCtls.setReturningAttributes(new String[] { "*" });

				// Search for objects using the filter
				final NamingEnumeration answer = ctx.search(searchBase, searchFilter, searchCtls);
				boolean s = false;
				if (!answer.hasMoreElements()) {
					throw new BusinessException("label.login.invalido");
				}
				searchCtls.setReturningAttributes(returnedAtts);

				String realName = "displayName";

				if (user.getSystemProperty("LDAP_LOGIN_ATTR") != null)
					realName = user.getSystemProperty("LDAP_LOGIN_ATTR");

				// Loop through the search results
				while (answer.hasMoreElements()) {
					s = true;
					final SearchResult sr = (SearchResult) answer.next();
					String dn = sr.getAttributes().get(realName).toString();
					LogUtil.debug(">>>" + dn, user);
					final String n = dn.replaceAll(realName + ":", "");
					environment.put(Context.SECURITY_PRINCIPAL, n.trim());
					environment.put(Context.SECURITY_CREDENTIALS, senha);
					// Print out the groups

					final LdapContext ctxL = new InitialLdapContext(environment, null);
					final NamingEnumeration answerL = ctxL.search(searchBase, searchFilter, searchCtls);
					if (!answerL.hasMoreElements()) {
						throw new BusinessException("label.login.invalido");
					}
					while (answerL.hasMoreElements()) {
						final SearchResult srd = (SearchResult) answerL.next();
						final Attributes attrs = srd.getAttributes();
						if (attrs != null) {

							String u = "VALOR_CONFIG_SISTEMA";

							if (user.getIdUsuario() != null) {
								u = "DESCRICAO_CONFIG_SISTEMA";
								vo.putAll(getUsuario(user));
							}

							final String Mapp = user.getSystemProperty("LDAP_MAPPING");
							final Map m = user.getUtil().JSONtoMap(Mapp);
							final Map r = user.getUtil().reverseMap(m);

							vo.putAll(m);
							vo.put("SENHA", user.getUtil().hash(senha, 20));
							vo.put("BLOQUEADO", "F");
							vo.put("LDAP", "T");
							try {
								for (final NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
									final Attribute attr = (Attribute) ae.next();

									LogUtil.debug("Attribute: " + attr.getID(), user);
									if (r.containsKey(attr.getID())) {
										for (final NamingEnumeration e = attr.getAll(); e.hasMore(); totalResults++) {
											final String val = (String) e.next();
											vo.put(r.get(attr.getID()).toString(), val);
											LogUtil.debug(" " + totalResults + ". " + val, user);
										}

									}

								}

							} catch (final NamingException e) {
								System.err.println("Problem listing membership: " + e);
								throw new BusinessException("label.login.invalido");
							}

							if (user.getIdUsuario() != null) {
								vo.put("ID_USUARIO", user.getIdUsuario());
								vo.setProcess(true);
								vo.setMethod("altera");
								vo.setAnulate(false);
								vo.put("NIVEL_PERMISSAO", null);
								vo.put("SUPER_USER", null);
								vo.put("DATA_EXPIRACAO_SENHA", user.getUtil().getDataExpiracaoSenha(user));
								vo.put("ID_UNIDADE_CONCESSAO", null);
								vo.put("ID_UNIDADE_RESTRICAO", null);
								vo.put("ID_UNIDADE_ORGANIZACIONAL", null);
								usu.altera(vo, user);
								vo.setProcess(false);
							} else {
								vo.setMethod("inclui");
								vo.put("DATA_EXPIRACAO_SENHA", user.getUtil().nowTimestamp());
								vo.setProcess(true);
								usu.inclui(vo, Usuario.getSystemUser());
								vo.setProcess(false);
							}
						}
					}

					LogUtil.debug("Total groups: " + totalResults, user);
					ctx.close();
				}
				vo = usu.obtem(vo, user);
				LogUtil.debug("LOGIN LDAP COM SUCESSO", user);
				return bindUser(user, vo, true);
			}

			catch (final NamingException e) {
				LogUtil.exception(e, user);
				LogUtil.debug("Problem searching directory: " + e, user);
				throw new BusinessException("label.login.invalido");
			} catch (final BusinessException ex) {
				throw ex;
			} catch (final Exception ex) {
				LogUtil.debug("FALTANDO ATTRIBUTOS DE CONFIGURACAO LDAP: " + ex, user);
				return user;
			}

		}

		return user;
	}

	public LdapContext createLdapContext(Usuario user, final Hashtable environment) {
		LdapContext ctx = null;
		try {
			ctx = new InitialLdapContext(environment, null);
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			throw new BusinessException("label.LDAP_ADMIN_AUTH_ERROR");
		}
		return ctx;
	}

	public void testLdapHostIsReachable(Usuario user) throws UnknownHostException, IOException {
		InetAddress end = null;
		if (user.getUtil().isIPAddress(user.getSystemProperty("LDAP_SERVER"))) {
			byte[] b = new byte[4];
			String[] t = user.getSystemProperty("LDAP_SERVER").toString().split("\\.");
			b[0] = new Integer(t[0]).byteValue();
			b[1] = new Integer(t[1]).byteValue();
			b[2] = new Integer(t[2]).byteValue();
			b[3] = new Integer(t[3]).byteValue();
			// Returns an InetAddress object given the raw IP
			// address .

			end = InetAddress.getByAddress(b);
		} else
			end = InetAddress.getByName(user.getSystemProperty("LDAP_SERVER"));

		if (!end.isReachable(1000))
			throw new BusinessException("label.ldap.inacessivel");
	}

	public Hashtable prepareEnvironment(Usuario user) {
		final Hashtable environment = new Hashtable();
		final String adminName = user.getSystemProperty("LDAP_USER");

		final String adminPassword = user.getSystemProperty("LDAP_PASS");// senha;
		String ldapURL = urlLdap(user);
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		// set security credentials, note using simple cleartext
		// authentication
		environment.put(Context.SECURITY_AUTHENTICATION, user.getSystemProperty("LDAP_AUTH_METHOD"));
		environment.put(Context.SECURITY_PRINCIPAL, adminName);
		environment.put(Context.SECURITY_CREDENTIALS, adminPassword);

		// connect to my domain controller
		environment.put(Context.PROVIDER_URL, ldapURL);
		return environment;
	}

	private String urlLdap(Usuario user) {
		String ldapURL = "ldap://" + user.getSystemProperty("LDAP_SERVER");

		if (user.getSystemProperty("LDAP_PORT") != null)
			ldapURL += ":" + user.getSystemProperty("LDAP_PORT");
		return ldapURL;
	}

	@Override
	public List lista(final GenericVO vo, final Usuario user) throws ConfigurationException, Exception {
		List listar = null;
		boolean cont = false;
		final List retornar = new ArrayList();
		LogUtil.debug("AGORA INVOKE >>>>" + getName() + " >> lista ", user);

		if (!processCalls(vo, user, "L", "before", getCalls(user, vo.getMethod()))) {
			return (List) vo.get(Constants.LIST_DATA);
		}

		// Processar script Antes de chamar qualquer query de listagem
		// correo para erro que estava acontecendo em Service="Pool"
		if (vo.get("NO_SCRIPT_PROCESS") == null)
			processScripts(vo, user, getScripts(user, vo.getMethod()), "L", "precedent");
		if (vo.containsKey(Constants.LIST_DATA)) {
			return listar = (List) vo.get(Constants.LIST_DATA);
		}

		vo.validate(user);

		if (cached && vo.getSearchBy() != null && vo.getSearchBy().toString().equalsIgnoreCase(Constants.CACHE)) {
			if (properties.get(Constants.RELATION_KEY) != null) {
				vo.searchBy(properties.get(Constants.RELATION_KEY));
			} else if (properties.get(Constants.DEFAULT_SEARCH) != null) {
				vo.searchBy(properties.get(Constants.DEFAULT_SEARCH));
			} else {
				vo.searchBy(properties.get(Constants.NOME_CHAVE));
			}
		}

		try {
			if (properties.get("type").toString().equalsIgnoreCase("D")
					&& properties.get(Constants.RELATION_KEY) == null) {
				final String k = (String) ((Map) user.getUtil()
						.filter((List) getParent(user).getProperties().get(Constants.ALL_FIELDS), "primaryKey", null)
						.get(0)).get(Constants.name);
				Object[] p = null;
				if (vo.get(k) != null) {
					p = new Object[] { vo.get(k) };
					vo.putKey(k, vo.get(k));
				}
				listar = getDAO().Listagem(p, vo, user);
			} else {
				if (properties.get(Constants.RELATION_KEY) != null) {
					final String k = (String) getPrimaryKey().get(Constants.name);
					if (vo.get(k) != null) {
						vo.putKey(properties.get(Constants.RELATION_KEY), vo.get(k));
					} else {
						vo.putKey(properties.get(Constants.RELATION_KEY),
								vo.get(properties.get(Constants.RELATION_KEY)));
					}
					Object[] params = new Object[] {};
					if (vo.get(vo.getKey()) != null)
						params = new Object[] { vo.get(vo.getKey()) };
					listar = getDAO().Listagem(params, vo, user);
				} else {
					listar = getDAO().Listagem(null, vo, user);
				}
			}
		} catch (final PersistenceException e) {
			LogUtil.exception(e, user);
		} catch (final ConfigurationException e) {
			throw e;
		}
		if (listar == null) {
			return retornar;
		}

		if (vo.get("NO_SCRIPT_PROCESS") == null)
			processScripts(vo, user, getScripts(user, vo.getMethod()), "L", "precedent");

		if (vo.containsKey(Constants.LIST_DATA)) {
			listar = (List) vo.get(Constants.LIST_DATA);
			if (!cont) {
				return listar;
			}
		}

		for (final Iterator iterator = listar.iterator(); iterator.hasNext();) {
			final Map object = (Map) iterator.next();
			final GenericVO v = createVO(user);
			v.setProcess(vo.isProcess());

			if (vo.get("NO_SCRIPT_PROCESS") == null)
				processScripts(v, user, getScripts(user, vo.getMethod()), "R", "precedent");

			v.putAllAndGet(object, true);

			if (vo.get("NO_SCRIPT_PROCESS") == null)
				processScripts(v, user, getScripts(user, vo.getMethod()), "R", "dependent");

			v.setLazyList(true);
			processRetrieve(v, user);
			retornar.add(v);
		}
		vo.put(Constants.LIST_DATA, retornar);
		processCalls(vo, user, "L", "after", getCalls(user, vo.getMethod()));
		vo.remove(Constants.LIST_DATA);
		if (vo.getMapping() == null) {
			return retornar;
		} else {
			return user.getUtil().mappingCollection(retornar, vo.getMapping(), "U", true, user);
		}
	}

	@Override
	public void logout(Usuario user) throws Exception {
		Map nova = new HashMap();
		user.getLogger().log(user, nova, "UsuarioService", "", nova, nova

				, "logout", getProperties().get("id").toString(), "0");

	}

	public synchronized Usuario login(final String login, final String senha, Usuario indefinedUser, boolean hash)
			throws Exception {

		// logados.clean();

		try {
			// maxUsuariosLogadosOk(indefinedUser)
			if (true) {
				// LOGIN propriamente dito!

				indefinedUser = ldap(indefinedUser, login, senha);

				if (indefinedUser.isBloqueado()) {
					// users.remove(indefinedUser);
					throw new BusinessException("label.usuario.bloqueado");
				}

				if (indefinedUser.isDeletado()) {
					// users.remove(indefinedUser);
					throw new BusinessException("label.usuario.deletado");
				}

				indefinedUser = processLogin(login, senha, indefinedUser.getHostOrigem(), hash, indefinedUser);

				if (indefinedUser != null) { // Login bem sucedido
					indefinedUser.setLang(indefinedUser.getLang());
					indefinedUser.setDataFinal(indefinedUser.getUtil().nowTimestamp());

					indefinedUser.setDataFinal(indefinedUser.getUtil().nowTimestamp());

					ServiceImpl idInstanciaServ = (ServiceImpl) getService("ASPConfigService", indefinedUser);
					if (idInstanciaServ != null) {
						GenericVO idInstanciaVO = createVO(indefinedUser);
						idInstanciaVO.setFiltro("{gA:[{c:{f:'NOME_CONFIG_SISTEMA',o:'=',v1:'"
								+ indefinedUser.getSystem() + "'}},{c:{f:'ID_PAI',o:'nl'}}]}");
						idInstanciaVO = idInstanciaServ.obtem(idInstanciaVO, indefinedUser);

						if (idInstanciaVO.get("ID_CONFIG_SISTEMA") != null
								&& !idInstanciaVO.get("ID_CONFIG_SISTEMA").equals("")) {
							indefinedUser.getSession().setAttribute("ID_INSTANCIA",
									idInstanciaVO.get("ID_CONFIG_SISTEMA"));
						}
					}

				} else { // Login nao efetuado com sucesso
					throw new BusinessException("label.login.entrar");
				}

			} else {
				throw new BusinessException("label.login.limiteUsuario");
			}
			Map nova = new HashMap();

			if (getProperties().get("id") != null) {
				indefinedUser.getLogger().log(indefinedUser, nova, "UsuarioService", "", nova, nova

						, "login", getProperties().get("id").toString(), "0");
			}
			return indefinedUser;

		} catch (final SessionExpiredException e) {
			throw e;

		} catch (final BusinessException e) {
			if (LogUtil.debugEnabled(indefinedUser))
				LogUtil.exception(e, indefinedUser);
			throw new BusinessException(e.getMessage(), e.getExtra());
		} catch (final Exception e) {
			if (LogUtil.debugEnabled(indefinedUser))
				LogUtil.exception(e, indefinedUser);
			throw new BusinessException(e.getMessage(),
					"alert(\"Email para administrador ERRO >>>" + e.getMessage() + " <<<\")");
		}
	}

	private boolean isLogable(GenericVO vo) {
		if (
		// !vo.isProcess()
		// ||
		(getProperties().get("logable") != null
				&& getProperties().get("logable").toString().equalsIgnoreCase("false"))) {
			return false;
		}
		return true;
	}

	private boolean isEmailListener(GenericVO vo) {
		if (
		// !vo.isProcess()
		// ||
		getProperties().get("emailListener") != null
				&& getProperties().get("emailListener").toString().equalsIgnoreCase("false")) {
			return false;
		}
		return true;
	}

	private List navigateBean(final GenericVO vo, final Usuario user) {
		final List links = new ArrayList<Map>();
		final List actions = new ArrayList<Map>();
		final List others = new ArrayList<Map>();

		String labelOthers = "";

		final ServletRequest req = (ServletRequest) vo.get(Constants.RequestProcessed);
		final StandardEvaluationContext ctx = user.getRequestEvaluationContext(req);
		String item = (String) vo.get("NODE");
        ctx.setVariable("node",item);
		String group = null;
		final String[] sp = item.split("_");
		item = sp[0];
		if (sp.length > 1) {
			group = sp[1];
		}
        ctx.setVariable("group",group);


        final String Type = (String) vo.get("LABELPROPERTY");
        ctx.setVariable("type",Type);

		final String navigate = (String) vo.get("MODEL");
        ctx.setVariable("navigate",navigate);

		boolean icons = Constants.use_icons;
		boolean for_checked = false;
		boolean grouped = true;
		if (vo.get("TREE") == null) {
			icons = true;
		}
		if (vo.get("FOR_CHECKED") != null && vo.get("FOR_CHECKED").toString().equalsIgnoreCase(Constants.true_str)) {
			for_checked = true;
		}
		final String k = "links";
		final GenericService serviceTarget = (GenericService) getServiceById(item, user);
		Map jsObj = null;
		try {
			jsObj = user.getUtil().JSONtoMap((String) serviceTarget.getProperties().get(k));
		} catch (final Exception e) {

		}
		if (jsObj != null) {
			final Iterator i = jsObj.keySet().iterator();
			while (i.hasNext()) {
				final Object object = i.next();
				if (object.toString().equalsIgnoreCase("Q")) {
					final List it = (List) jsObj.get(object);
					for (final Iterator iterator = it.iterator(); iterator.hasNext();) {
						final Map qr = (Map) iterator.next();
						List<Map<String, String>> c = (List) user.getSysParser().getListValuesFromXQL(
								user.getUtil().EvaluateInString(qr.get("e").toString(), ctx), (String) qr.get("sort"),
								user, true, true);
						labelOthers = user.getLabel((String) qr.get("l"));
						if (qr.get("g") != null) // agrupar
						{

							if (group == null) {
								group = (String) qr.get("g");
								c = user.getUtil().groupCollection(c, qr.get("g"));
								c = user.getUtil().mappingCollection(c,
										"{" + qr.get("g") + ":'label', "
												+ this.getName().replaceAll(Constants.SERVICE_BASE, "") + ":'name'  }",
										"L", false, user);
							} else {
								c = user.getUtil().match(c, qr.get("g"), group);
								group = null;
								grouped = false;
							}

						}
						for (final Map map_filho : c) {
							if (map_filho.get("ignoreLayers") != null
									&& map_filho.get("ignoreLayers").toString().contains("V")) {
								continue;
							}

							final String id_f = (String) map_filho.get("id");
							String label_f = "";
							label_f = user.getLabel((String) map_filho.get("label"));
							String desc_f = user.getDescricao((String) map_filho.get("label"));

							try {
								if (!item.equalsIgnoreCase(id_f) || group != null) {
									if (group == null) {
										final GenericVO v = createVO(user);
										v.put("NODE", id_f);
										v.put("LABELPROPERTY", Type);
										v.put("MODEL", navigate);
										v.put(Constants.RequestProcessed, vo.get(Constants.RequestProcessed));

										final List cf = navigateBean(v, user);
										map_filho.put("leaf", cf.size() == 0);
									} else {
										map_filho.put("leaf", false);
									}
									map_filho.put("model", navigate);
									if (for_checked) {
										map_filho.put("checked", false);

									}
								} else {
									map_filho.put("leaf", true);
									map_filho.put("model", navigate);
									if (for_checked) {
										map_filho.put("checked", false);
									}
								}
							} catch (final Exception e) {
								LogUtil.exception(e, user);
								map_filho.put("leaf", true);
								map_filho.put("model", navigate);
								if (for_checked) {
									map_filho.put("checked", false);
								}
							}

							if (group == null) {
								map_filho.put("id", id_f);
							} else {
								map_filho.put("id", item + "_" + map_filho.get(group));
								if (getServiceById(label_f, user) == null) {
									continue;
								}
								map_filho.put("label",
										((ServiceImpl) getServiceById(label_f, user)).getProperties().get("label"));

								label_f = user.getLabel((String) ((ServiceImpl) getServiceById(label_f, user))
										.getProperties().get("label")); // exclusivo
								// para o
								// group de tabelas
								// auxiliares
								desc_f = user.getDescricao((String) ((ServiceImpl) getServiceById(desc_f, user))
										.getProperties().get("label")); // exclusivo
								// para o
								// group de tabelas
								// auxiliares
							}

							String action_f = (String) map_filho.get("action");
							if (action_f == null || action_f != null && action_f.equals("")) {

								action_f = user.getUtil()
										.encode(user.getUtil().getActionStringAndParams(map_filho,
												!new Boolean(String.valueOf(map_filho.get("leaf"))), false, req)[0]
														.toString(),
												user.getSessionId(), "", false, user);
							}

							String params_f = (String) map_filho.get("params");
							if (params_f == null || params_f != null && params_f.equals("''")) {
								params_f = "''";
							} else {
								params_f = "{" + params_f + "}";
							}

							map_filho.put("master", item);
							map_filho.put("url", action_f);
							map_filho.put("desc", desc_f);
							map_filho.put("params", params_f);
							map_filho.put("text", label_f);
							map_filho.put("titulo", label_f);
							map_filho.put("type", "M");

							if (user.getIcon(map_filho.get("label").toString()) != null && icons) {
								map_filho.put("" + "icon", "ItemLinguagemAction.do?" + Constants.ActionParam
										+ "=icon&amp;NOME_CONFIG_SISTEMA=" + user.getLang() + Constants.CACHE_SEPARATOR
										+ map_filho.get("label")
										+ "&amp;sz=32&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
							} else {
								map_filho.put("iconCls", "no-icon");
							}
							if (ParseXmlService.canAccess(map_filho, user, true)) {
								others.add(map_filho);
							}

						}
					}
				} else if (object.toString().equalsIgnoreCase("L")) {

					final List it = (List) jsObj.get(object);

					for (final Iterator iterator = it.iterator(); iterator.hasNext();) {

						final Map ql = (Map) iterator.next();

						final String label_link = user.getLabel(ql.get("l").toString());

						final String desc_link = user.getDescricao(ql.get("l").toString());

						final String id_link = item + "_lin_" + ql.get("l").toString().hashCode();
						Map po = new HashMap(((GenericService) getServiceById(item, user)).getProperties());

						if (ql.get("m") != null) {
							po.put(Constants.ActionParam, ql.get("m"));
						}

						final String action_link = user.getUtil().encode(
								user.getUtil().getActionStringAndParams(po, false, false, req)[0].toString(),
								user.getSessionId(), "", false, user);

						final JSONBuilder params_link = new JSONBuilder()
								.addPropertyType(Constants.FILTER_KEY,
										user.getUtil().encode(ql.get("f").toString(), user))
								.addPropertyType("texto", user.getUtil().encode(label_link, user));

						final HashMap<String, Object> map_link = new HashMap<String, Object>();
						map_link.put("id", id_link);
						map_link.put("master", item);
						map_link.put("url", action_link);
						map_link.put("params", params_link);
						map_link.put("text", label_link);
						map_link.put("desc", desc_link);
						map_link.put("titulo", label_link);
						map_link.put("leaf", true);
						map_link.put("model", navigate);
						if (for_checked) {
							map_link.put("checked", false);
						}
						map_link.put("type", "LINK");
						map_link.put("label", ql.get("l").toString());
						ql.put("label", ql.get("l").toString());

						if (user.getIcon(map_link.get("label").toString()) != null && icons) {
							map_link.put("" + "icon",
									"ItemLinguagemAction.do?" + Constants.ActionParam + "=icon&amp;NOME_CONFIG_SISTEMA="
											+ user.getLang() + Constants.CACHE_SEPARATOR + map_link.get("label")
											+ "&amp;sz=32&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
						} else {
							map_link.put("iconCls", "no-icon");
						}
						if (ParseXmlService.canAccess(ql, user, true)) {
							links.add(map_link);
						}
					}
				} else if (object.toString().equalsIgnoreCase("A")) {
					final List it = (List) jsObj.get(object);
					for (final Iterator iterator = it.iterator(); iterator.hasNext();) {
						final Map ql = (Map) iterator.next();

						if (ql.get("printWhenExpression") == null || (ql.get("printWhenExpression") != null
								&& user.getUtil().EvaluateBoolean(ql.get("printWhenExpression").toString(), ctx))) {

							final Map p = new HashMap(((GenericService) getServiceById(item, user)).getProperties());

							String itemTableName = null;
							if (ql.get("s") != null) {
								// metodos injetados de outro service, para
								// verificar a permissoes e necesserio o NAME da
								// table definida.
								GenericService s = ((ServiceImpl) getService(ql.get("s") + Constants.SERVICE_BASE,
										user));
								if (s == null)
									continue;

								itemTableName = (String) s.getProperties().get("table");
							} else {
								// metodos do proprio bean
								itemTableName = "" + p.get("table");
							}

							if (!user.verificaPermisao((itemTableName + "." + ql.get("m")).hashCode())) {
								continue;
							}
							final String label_link = user.getLabel(ql.get("l").toString());

							final String desc_link = user.getDescricao(ql.get("l").toString());

							final String id_link = item + "_act_" + ql.get("l").toString().hashCode();

							p.put(Constants.ActionParam, ql.get("m"));
							if (ql.get("s") != null)
								p.put(Constants.name, ql.get("s"));

							final String action_link = user.getUtil().encode(
									user.getUtil().getActionStringAndParams(p, false, false, req)[0].toString(),
									user.getSessionId(), "", false, user);

							final JSONBuilder params_link = new JSONBuilder();
							if (ql.get("f") != null) {
								params_link
										.addPropertyType("FILTRO", user.getUtil().encode(ql.get("f").toString(), user))
										.addPropertyType("texto", user.getUtil().encode(label_link, user));
							}

							final HashMap<String, Object> map_link = new HashMap<String, Object>();
							map_link.put("id", id_link);
							map_link.put("master", item);

							if (ql.get("url") == null || ql.get("url").equals("")) {
								map_link.put("url", action_link);
							} else {
								map_link.put("url", ql.get("url"));
							}

							if (ql.get("params") == null || ql.get("params").equals("")) {
								map_link.put("params", params_link);
							} else {
								map_link.put("params", ql.get("params"));
							}

							map_link.put("text", label_link);
							map_link.put("titulo", label_link);
							map_link.put("desc", desc_link);
							map_link.put("leaf", true);

							if (ql.get("model") == null || ql.get("model").equals("")) {
								map_link.put("model", navigate);
							} else {
								map_link.put("model", ql.get("model"));
							}

							if (for_checked) {
								map_link.put("checked", false);
							}
							map_link.put("modal", true);

							if (ql.get("type") == null || ql.get("type").equals("")) {
								map_link.put("type", "LINK");
							} else {
								map_link.put("type", ql.get("type"));
							}

							map_link.put("label", ql.get("l").toString());
							ql.put("label", ql.get("l").toString());
							if (user.getIcon(map_link.get("label").toString()) != null && icons) {
								map_link.put("" + "icon", "ItemLinguagemAction.do?" + Constants.ActionParam
										+ "=icon&amp;NOME_CONFIG_SISTEMA=" + user.getLang() + Constants.CACHE_SEPARATOR
										+ map_link.get("label")
										+ "&amp;sz=32&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
							}

							if (ParseXmlService.canAccess(ql, user, true)) {
								actions.add(map_link);
							}

						}
					}
				}

			}
		}
		if (vo.get("MAPPING") == null) { // tree
			links.addAll(actions);
			links.addAll(others);
			if (serviceTarget != null && !serviceTarget.getProperties().get("type").equals("F")
					&& serviceTarget.getProperties().get("links") != null && group == null && grouped) {
				final List<Map<String, String>> c = (List) user.getSysParser().getListValuesFromXQL(
						user.getUtil()
								.EvaluateInString("/systems/system[@name='" + user.getSchema()
										+ "']/module/bean[contains(@sharing,'context')]", ctx),
						"order", user, true, true);
				Map ac = user.getUtil().JSONtoMap((String) serviceTarget.getProperties().get("acceptSharing"));
				if (ac.size() > 0) {
					for (final Object element : c) {
						final Map<String, String> map = (Map<String, String>) element;
						if (!ac.containsKey(map.get("id"))) {
							continue;
						}
						map.put("model", (String) serviceTarget.getProperties().get("id"));
						Map sh = user.getUtil().JSONtoMap(map.get("sharing"));

						if (ParseXmlService.canAccess(map, user, true)) {
							links.add(
									createNode(
											map.get("label").toString() + "_" + serviceTarget.getProperties().get("id"),
											item, "{CONTEXTO:'" + serviceTarget.getProperties().get("id") + "'}",
											user.getLabel(map.get("label").toString()),
											user.getUtil()
													.encode(user.getUtil().getActionStringAndParams(map, true, false,
															req)[0].toString(), user.getSessionId(), "", false, user),
											"", "", true, null, null, true, "sharing", map.get("label").toString(),
											null, user));
						}

					}
				}
			}
			return links;
		}
		final List m = new ArrayList();
		final List master = new ArrayList();
		final HashMap x = new HashMap();
		if (others.size() > 0) {

			x.put("title", labelOthers);
			x.put("data", others);
			m.add(x.clone());
			x.clear();
		}
		if (links.size() > 0) {

			x.put("title", user.getLabel("label.links"));
			x.put("data", links);
			m.add(x.clone());
			x.clear();
		}

		if (actions.size() > 0) {
			x.put("title", user.getLabel("label.atalhos"));
			x.put("data", actions);
			m.add(x.clone());
			x.clear();
		}

		if (!serviceTarget.getProperties().get("type").equals("F") && serviceTarget.getProperties().get("links") != null
				&& group == null && grouped) {
			final List<Map<String, String>> c = (List) user.getSysParser()
					.getListValuesFromXQL(
							user.getUtil()
									.EvaluateInString("/systems/system[@name='" + user.getSchema()
											+ "']/module/bean[contains(@sharing,'context')]", ctx),
							"order", user, true, true);

			Map ac = user.getUtil().JSONtoMap((String) serviceTarget.getProperties().get("acceptSharing"));
			if (ac.size() > 0) {
				for (final Object element : c) {
					final Map<String, String> map = (Map<String, String>) element;
					if (!ac.containsKey(map.get("id"))) {
						continue;
					}

					x.put("title", user.getLabel(map.get("label")));

					final ServiceImpl sha = (ServiceImpl) getService(map.get(Constants.name) + "Service", user);
					final GenericVO sv = sha.createVO(user);
					sv.put("CONTEXTO", serviceTarget.getProperties().get("id"));
					sv.put("MODULO", serviceTarget.getProperties().get("id"));
					map.put("CONTEXTO", (String) serviceTarget.getProperties().get("id"));
					req.setAttribute("model", sv.get("CONTEXTO"));
					try {
						final Map params = user.getUtil().JSONtoMap(user.getUtil().EvaluateInString(map.get("sharing"),
								user.getUtil().createContext(sv, user)));
						final Map mapping = (Map) params.get("mapping");
						final Map spar = (Map) params.get("serviceParams");
						// sv.setLazyList(true);
						sv.setPaginate(false);
						if (spar != null) {
							sv.putAll(user.getUtil().encodeMap(spar, user));
						}
						final List it = sha.lista(sv, user);
						if (it.size() > 0 && mapping != null) {
							final List s = new ArrayList();
							for (final Iterator iterator = it.iterator(); iterator.hasNext();) {
								boolean lr = true;
								String url = "";
								final Map object = (Map) iterator.next();
								if (params.get("method") != null) {
									map.put(Constants.ActionParam, (String) params.get("method"));
									lr = false;

									url = user.getUtil().getActionStringAndParams(map, lr, false, req)[0].toString();

									Map p = user.getUtil().filterMap(object, mapping.values().toArray());
									Iterator i = p.keySet().iterator();
									while (i.hasNext()) {
										String object2 = (String) i.next();
										url += "&" + object2 + "=" + p.get(object2);

									}

									url = user.getUtil().encode(url, user.getSessionId(), "", false, user);
								} else {
									url = user.getUtil().encode(
											user.getUtil().getActionStringAndParams(map, lr, false, req)[0].toString(),
											user.getSessionId(), "", false, user);
								}
								boolean modal = true;
								if (params.get("is_win") != null) {
									modal = Boolean.valueOf(params.get("is_win").toString());
								}

								s.add(createNode(
										object.get(mapping.get("id")).toString() + "_"
												+ serviceTarget.getProperties().get("id"),
										item, "", object.get(mapping.get("label")), url, "", "", true, null, null,
										modal, "sharing", object.get(mapping.get("label")).toString().toString(),
										sha.getProperties().get("label"), user));

							}
							x.put("data", s);
							m.add(x.clone());
							x.clear();
						}
					} catch (final ConfigurationException e) {

						LogUtil.exception(e, user);
					} catch (final Exception e) {

						LogUtil.exception(e, user);
					}

				}
			}
		}

		final HashMap base = new HashMap();
		final ServiceImpl s = (ServiceImpl) getServiceById(item, user);
		base.put("BASE", m);
		base.put("" + "IMG",
				"ItemLinguagemAction.do?" + Constants.ActionParam
						+ "=icon&amp;key=NOME_CONFIG_SISTEMA&amp;NOME_CONFIG_SISTEMA=" + user.getLang()
						+ Constants.CACHE_SEPARATOR + s.getProperties().get("label")
						+ ".show&amp;sz=none&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");

		base.put("DESC", user.getDescricao((String) s.getProperties().get("label")));
		base.put("TITULO", user.getLabel((String) s.getProperties().get("label")));

		master.add(base);
		return master;
	}

	@Override
	public GenericVO obtem(final GenericVO vo, final Usuario user) throws Exception {
		GenericVO vor = createVO(user);
		LogUtil.debug("AGORA INVOKE >>>>" + getName() + " >> obtem ", user);

		try {

			if (!this.cached) {
				if (!processCalls(vo, user, "R", "before", getCalls(user, vo.getMethod()))) {
					return vo;
				}

				if ((vo.get("NO_SCRIPT_PROCESS") == null)
						&& !processScripts(vo, user, getScripts(user, vo.getMethod()), "R", "precedent")) {
					return vo;
				}
			}
			vo.validate(user);
			if (vo.get(vo.getSearchBy()) != null && vo.get(vo.getSearchBy()).toString().trim().length() > 0
					|| (vo.getSearchBy() != null && vo.getSearchBy().indexOf(",") > -1)) {
				vor = (GenericVO) getDAO().obtem(vo, vo.getSearchBy(), user);
			} else if (vo.getSearchBy() == Constants.CACHE
					&& vo.get(getProperties().get(Constants.DEFAULT_SEARCH)) != null
					&& vo.get(getProperties().get(Constants.DEFAULT_SEARCH)).toString()
							.startsWith(Constants.CACHE_SEPARATOR)) {
				vor = (GenericVO) getDAO().obtem(vo, vo.get(getProperties().get(Constants.DEFAULT_SEARCH)).toString(),
						user);
			} else if (vo.get(vo.getKey()) != null) {
				if (vo.getSearchBy() != null && vo.getSearchBy().equalsIgnoreCase(Constants.CACHE)) {
					vo.searchBy(getProperties().get(Constants.DEFAULT_SEARCH));
				}
				vor = (GenericVO) getDAO().obtem(vo, vo.getKey(), user);
			} else {
				if (properties.get("type") != null && properties.get("type").toString().equalsIgnoreCase("D")
						&& properties.get(Constants.RELATION_KEY) == null) {
					final String k = (String) getParent(user).getPrimaryKey().get(Constants.name);
					vo.putKey(k, vo.get(k));
					vor = (GenericVO) getDAO().obtem(vo, vo.getKey(), user);
				} else {
					if (properties.get(Constants.RELATION_KEY) != null) {
						final String k = (String) ((Map) user.getUtil()
								.filter((List) getParent(user).getProperties().get(Constants.ALL_FIELDS), "primaryKey",
										null)
								.get(0)).get(Constants.name);
						if (vo.get(k) != null)
							vo.putKey(properties.get(Constants.RELATION_KEY), vo.get(k));

						vor = (GenericVO) getDAO().obtem(vo, vo.getKey(), user);
					} else {
						vor = (GenericVO) getDAO().obtem(vo, null, user);
					}
				}
			}

			if (vor != null) {
				processRetrieve(vor, user);
			}
		} catch (final Exception e) {
			LogUtil.debug("Erro em " + getProperties() + " VO > > > " + vo, user);
			LogUtil.exception(e, user);
		}

		if (vor != null) {
			if (vo.isPreserv()) {
				vor.setOld_VO(vo);
			}
			vor.setLazyList(vo.isLazyList());
			vor.setMethod(vo.getMethod());
			vor.cloneDiff(vo);
			if (vo.get("NO_SCRIPT_PROCESS") != null)
				vor.put("NO_SCRIPT_PROCESS", true);
		}

		if (!this.cached) {
			processCalls(vor == null ? vo : vor, user, "R", "after", getCalls(user, vo.getMethod()));
			if ((vo.get("NO_SCRIPT_PROCESS") == null) && !processScripts(vor == null ? vo : vor, user,
					getScripts(user, vo.getMethod()), "R", "dependent")) {
				return vor;
			}
		}

		return vor == null ? createVO(user) : vor;
	}

	private String parseActualUser(final Usuario user) {
		if (user != null) {
			return "pelo usuario \"" + user.getLogin() + "\" do sistema " + user.getSchema();
		} else {
			return "por um usuario \"sem Sessao\"";
		}

	}

	@Override
	public boolean prepare(GenericVO vo, Usuario user) {
		try {
			if (!processCalls(vo, user, "P", "before", getCalls(user, vo.getMethod()))) {
				return false;
			}
			if (!processScripts(vo, user, getScripts(user, vo.getMethod()), "P", "precedent")) {
				return false;
			}
			processCalls(vo, user, "P", "after", getCalls(user, vo.getMethod()));
		} catch (BusinessException e) {
			throw e;
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		return true;
	}

	private GenericVO processBinder(final GenericVO nvo, final GenericVO vo, final Usuario user) throws Exception {
		if (nvo.getSearchBy() == null) {
			nvo.searchBy(vo.getKey());
		}
		if (nvo.get("BINDER") != null) {
			final String binder = (String) nvo.get("BINDER");

			final String binding[] = binder.split(";");

			for (final String element : binding) {
				final String[] b = element.split("\\|");
				if (b[1] != null && b[1].toString().equalsIgnoreCase("now()")) {
					nvo.put(b[0], user.getUtil().now());
				} else {
					nvo.put(b[0], vo.get(b[1]));
				}
				nvo.put("BINDER", b[0]);
			}

		}
		if (nvo.get("SEARCH_BY") != null) {
			final String sb = (String) nvo.get("SEARCH_BY");

			final String sbing[] = sb.split("\\|");
			nvo.searchBy(StringUtils.arrayToDelimitedString(sbing, ","));

		}

		// final Iterator i = nvo.keySet().iterator();
		// while (i.hasNext()) {
		// final Object type = i.next();
		// if (vo.get(type) != null
		// && nvo.get(type) == null
		// && !GenericVO.staticProps.contains(type.toString()
		// .toUpperCase())) {
		// nvo.put(type.toString(), vo.get(type));
		// }
		// }

		return nvo;
	}

	private boolean processCalls(final GenericVO vo, final Usuario user, final String nature, final String when,
			final List calls) throws Exception {

		if (isEmailListener(vo) && vo != null && vo.isProcess() && when.equals("after")) {
			vo.put(Constants.SERVICE_KEY, this);
			EmailService s = getEmailService(user);
			if (s != null)
				s.enviarEmail((GenericVO) vo.clone(), user);
			vo.remove(Constants.SERVICE_KEY);
		}

		if (calls != null && calls.size() > 0) {
			for (final Iterator iterator = calls.iterator(); iterator.hasNext();) {
				final Map object = (Map) iterator.next();
				if (object.get("nature") != null
						&& object.get("nature").toString().toUpperCase().indexOf(nature) == -1) {
					continue;
				}

				if (object.get("when") != null && !object.get("when").toString().equalsIgnoreCase(when)) {
					continue;
				}

				if (object.get("singleton") != null
						&& object.get("singleton").toString().equalsIgnoreCase(Constants.true_str)) {
					final Object c = ReflectionUtils
							.findMethod(Class.forName(object.get("service").toString()), "getInstance",
									new Class[] { Usuario.class })
							.invoke(Class.forName(object.get("service").toString()), new Object[] { user });

					final Method f = ReflectionUtils.findMethod(c.getClass(), object.get("serviceMethod").toString(),
							new Class[] { GenericVO.class, Usuario.class });
					if (f != null) {
						try {
							return (Boolean) f.invoke(c, new Object[] { vo, user });
						} catch (InvocationTargetException ex) {
							LogUtil.exception(ex, user);
							if (ex.getCause() != null && ex.getCause() instanceof BusinessException) {
								return false;
							}
						}

					}
				} else {
					return (Boolean) ((ServiceImpl) getService(object.get("service").toString(), user))
							.invoke(object.get("serviceMethod").toString(), vo, user);
				}
			}
		}

		// TODO processWorkflowTriggers deve acontecer apos uma alteracao,
		// inclusao, ou
		// remocao de dados no sistema. Agora ele esta executando antes, entao
		// nao ha nenhuma action disponivel para execucao, ja que o banco de
		// dados continua o mesmo. Ex: Ao aprovar um contrato. Ex2: Ao alterar
		// um contrato.
		try {

			if (vo != null && vo.isProcess() && when.equals("after")) {
				vo.setProcess(false);
				GenericVO mvo = obtem(vo, user);
				// um bug de transacao muito serio a ser estudado
				if (vo.containsKey("ID_WORKFLOW_ATIVIDADE")) {
					mvo = vo;
				}
				processWorkflowTriggers(mvo, user, nature, when, calls);
				vo.setProcess(true);
			}
		} catch (Exception e) {
		}

		return true;

	}

	private Usuario processLogin(final String login, final String senha, final String ip, final boolean hash,
			Usuario user) throws Exception {
		// Usuarios users = (Usuarios) getService("Usuarios", user);

		if (user.getSenha() != null) {
			// Sem essa verificao um usurio que se loge numa sesso
			// ativa com
			// login diferente e mesma senha se autentica

			// if (users.isRegistred(login, user) == false) {
			// throw new BusinessException("label.login.usuario.inexistente");
			// }
			if (!user.getLogin().equalsIgnoreCase(login)) {
				throw new BusinessException("label.login.invalido");
			}
			// if (users.getLock() == true) {
			// throw new BusinessException("label.manut.provisionamento");
			// }
			if (senha == null) {
				throw new BusinessException("errors.senha.required");
			}
			if (!hash) {
				if (!user.getSenha().equals(user.getUtil().hash(senha, 20))) {
					// users.remove(user);
					throw new BusinessException("label.login.invalido");
				}
			} else {
				if (!user.getSenha().equals(senha)) {
					// users.remove(user);
					throw new BusinessException("label.login.invalido");
				}
			}

			/*
			 * if (user.getDataExpiracaoSenha() != null) { final long d =
			 * user.getDataExpiracaoSenha().getTime(); final long di = (d - new
			 * java.util.Date().getTime()) / (1000 * 60 * 60 * 24) + 1;
			 * user.setTempoValidadeSenha(di); }
			 * 
			 * if (user.getTrocaSenha()) { user.setTempoValidadeSenha(-1); }
			 * else { user.setTempoValidadeSenha(0); }
			 */
		} else {
			// users.remove(user);
			throw new BusinessException("label.login.invalido");
		}
		// users.inserirUsuario(user);

		return user;
	}

	private GenericVO processRetrieve(final GenericVO vo, final Usuario user) {

		// if (!Constants.JoinSuport && false) {
		// final List cam = user.getUtil().match(
		// (List) properties.get(Constants.ALL_FIELDS),
		// new String[] { Constants.component },
		// new String[] { "context" });
		//
		// try {
		// final Iterator ih = cam.iterator();
		// while (ih.hasNext()) {
		// final Map ca = (Map) ih.next();
		// InvokeRecursiveMethod(ca, vo, true, "obtem", null, user);
		// }
		// } catch (final Exception e) {
		// e.printStackTrace();
		// }
		// }
		final List hc = user.getUtil().match((List) properties.get(Constants.ALL_FIELDS), Constants.component, "rule");

		for (final Iterator iterator = hc.iterator(); iterator.hasNext();) {
			// tratando rules apenas de union de campos por enquanto
			final Map object = (Map) iterator.next();
			final String[] f = object.get("bind").toString().split(">");
			final String[] v = object.get("value").toString().split("\\|");
			final String[] cv = f[1].split(":");
			if (f[0].equalsIgnoreCase("union")) {
				final String val = (String) vo.get(cv[0]);
				final String[] vn = cv[1].toString().split("\\|");
				for (int i = 0; i < vn.length; i++) {
					if (val == vn[i]) {
						vo.put(v[i], vo.get(object.get(Constants.name)));
					}
				}
			}

		}

		return vo;
	}

	public void invokeScript(String name, GenericVO vo, Usuario user) {
		executeScript(getScript(name, user, vo), vo, user);
	}

	private Object executeScript(Map script, GenericVO vo, Usuario user) {
		CompiledScript eng = null;
		if (!debugJS) {
			eng = (CompiledScript) script.get("src_cmp");
		}

		Object r = null;
		try {
			if (debugJS) {
				r = engine.eval(script.get("src").toString(), getBindings(user, vo));
			} else {
				r = eng.eval(getBindings(user, vo));
			}
		} catch (final ScriptException e) {
			LogUtil.error(
					"Erro no script com source " + script.get("name") + " no service " + getProperties().get("name"));
			if (e.getMessage().indexOf("BusinessException") > -1) {
				if (e.getCause().getCause().getMessage() != null)
					throw new BusinessException(e.getCause().getCause().getMessage().trim(), e.getCause().getCause());
				else
					throw new BusinessException(e.getCause().getMessage().trim(), e.getCause());
			}
			if (script.get("CDATA") != null) {
				LogUtil.error(script.get("CDATA").toString());
			}
			if (LogUtil.exceptionEnabled(user)) {
				LogUtil.exception(e, user);
			}
		}
		return r;
	}

	private Map getScript(String name, Usuario user, GenericVO vo) {
		Map allScripts = getScripts(user, vo.getMethod());

		final Iterator i = allScripts.keySet().iterator();
		while (i.hasNext()) {

			String scriptName = (String) i.next();

			if (scriptName.equals(name)) {
				final Map script = (Map) allScripts.get(scriptName);
				if (script.get("name") == null && (scriptName != null && !scriptName.isEmpty())) {
					script.put(Constants.name, scriptName);
				}
				return script;
			}

		}
		return null;
	}

	public boolean processScripts(final GenericVO vo, final Usuario user, final Map scripts, final String method,
			final String nature) {

		if (scripts == null) {
			return true;
		}

		final Iterator i = scripts.keySet().iterator();

		while (i.hasNext()) {

			String scriptName = (String) i.next();
			final Map object = (Map) scripts.get(scriptName);
			if (object.get("name") == null && (scriptName != null && !scriptName.isEmpty())) {
				object.put(Constants.name, scriptName);
			}

			if (object != null && object.get("name") != null) {
				if (vo.isIgnoredScript(object.get("name").toString())) {
					continue;
				}
			}

			if ((object.get("serviceMethod") != null
					&& object.get("serviceMethod").toString().toUpperCase().indexOf(method) == -1)
					|| (object.get("nature") != null && !object.get("nature").toString().equalsIgnoreCase(nature))) {
				continue;
			}

			LogUtil.debug("EXECUTANDO JS SCRIPT PROPERTY NO SERVICE " + getName() + " >>> METHOD = " + method + ">> "
					+ vo.getMethod() + " >> " + object.get("name") + " src  = " + object.get("src"), user);

			Object r = executeScript(object, vo, user);

			try {
				return (Boolean) r;
			} catch (Exception e) {
			}

		}

		return true;
	}

	public Bindings getBindings(Usuario user, GenericVO vo) {
		Bindings bindings = engine.createBindings();
		bindings.put("user", user);
		bindings.put("sm", this);
		bindings.put(Constants.VOKey, vo);
		bindings.put("util", user.getUtil());
		bindings.put("df", user.getUtil().df);
		// bindings.put("cmp", new TagInputExt());
		bindings.put("LogUtil", new LogUtil());
		return bindings;
	}

	private void processWorkflowTriggers(final GenericVO vo, final Usuario user, final String nature, final String when,
			final List calls) {

		if (user.SystemPropertyIsTrue("DISABLE_WORKFLOW_LISTENER")) {
			return;
		}

		if (getProperties().get("forceWorkflowListener") == null
				|| getProperties().get("forceWorkflowListener").equals("false")) {
			if (wfListeners.containsKey(user.getSystem())
					&& !wfListeners.get(user.getSystem()).containsKey(getName() + "." + vo.getMethod()))
				return;
		}

		try {

			boolean isMaster = false;
			Map ids = new HashMap();
			ServiceImpl parent = null;
			if (properties.get("type") == null) {
				return;
			}

			ids.put(getProperties().get("id"),
					getProperties().get("id") + "-" + vo.get(this.getPrimaryKey().get(Constants.name)));
			if (!properties.get("type").equals("M")) {
				while (!isMaster) {
					if (parent == null) {
						parent = getParent(user);
					} else {
						parent = parent.getParent(user);
					}
					if (parent == null || parent.getPrimaryKey() == null) {
						isMaster = true;
						continue;
					}
					ids.put(parent.getProperties().get("id"), parent.getProperties().get("id") + "-"
							+ vo.get(parent.getPrimaryKey().get(Constants.name))); // em
					// 3
					// nivel
					// ex
					// objeto
					// parcela
					// pode
					// ser
					// que
					// a
					// vo
					// ntao
					// tenha
					// o
					// id_processo
					if (parent.getProperties().get("type").equals("M") && !properties.get("type").equals("A")
							&& !properties.get("type").equals("F")) {
						;
						isMaster = true;
					} else {
						isMaster = true;
					}
				}
			}

			if (properties.get(Constants.name).equals("WorkFlowAtividade") && vo.getMethod().equals("decisao")
					&& keysetItemLike(vo, "ACAO_") == true) {
				// acho que vai sumir
				vo.setProcess(false); // nao posso ter workflow em cima de
				// atividades de workflow
				wfc.executarInstancia(vo, user);
			} else
			// if (!primaryKey.equals("ID_WORKFLOW")
			// && vo.get(primaryKey) != null)
			{
				if (vo.containsKey(Constants.ActionParam) && ids.size() > 0) {
					String voMethod = (String) vo.get(Constants.ActionParam);
					if (!voMethod.equals("")) {
						if (wfc.getWorkflowsByBeanId((String) getProperties().get("id"), user).size() > 0) {
							if (voMethod.equals("inclui")) { // retirar isso
								// sempre
								// tentar
								// criar, e
								// na GLOBAL
								// CONDITION
								// VALIDAR
								// METHOD E
								// VALORES
								// DE VO COM
								// RHINO
								wfc.createInstancesForBeanId((String) getProperties().get("id"),
										(String) ids.get(getProperties().get("id")), user, vo);

							}

						}
						wfc.executarInstanciasParaObjeto(ids.values().toArray(), vo, user);

					}
				}

			}

		} catch (Exception e) {
			// TODO tratar melhor esse erro.
			LogUtil.error("Nao foi possivel executar acoes de instancias de workflow.");
			LogUtil.exception(e, user);
		}

	}

	public void redefinirSenha(final GenericVO vo, final Usuario user) throws Exception {
		if (!vo.get("NOVA_SENHA").equals(vo.get("CONFIRMA_SENHA"))) {
			throw new BusinessException("senhanova.diferentes");
		}

		if (vo.get("NOVA_SENHA").toString().length() < 8) {
			throw new BusinessException("errors.minlength.senha", "label.senha_nova");
		}

		final String[] users = vo.get("SENHA").toString().split(",");
		for (final String user2 : users) {
			vo.put("ID_USUARIO", user2);
			// getEmailService().enviarSenhaEmail(vo, false, false, user);
		}
	}

	@Override
	public String remove(final GenericVO mvo, final Usuario user) throws Exception {
		final DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(
				ConnectionManager.getInstance(user).getDataSource(user));
		transactionManager
				.setTransactionSynchronization(DataSourceTransactionManager.SYNCHRONIZATION_ON_ACTUAL_TRANSACTION);
		final TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
		transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		final DAOTemplate tem = new DAOTemplate(transactionManager.getDataSource(), user, createVO(user),
				user.getUtil());
		return String.valueOf(transactionTemplate.execute(new TransactionCallback() {
			@Override
			public Object doInTransaction(final TransactionStatus status) throws RuntimeException {
				try {

					GenericVO vo = null;
					try {
						vo = obtem(mvo, user);
						vo.setMultipleId(mvo.getMultipleId());
					} catch (Exception e) {
						return null;
					}
					if (vo.get(vo.getKey()) == null)
						vo = mvo;

					String contextKey = "";
					if (mvo.containsKey(Constants.CONTEXT_KEY)) {
						contextKey = "" + mvo.get(Constants.CONTEXT_KEY);
					}

					if (!vo.isProcess()) {
						if (!processScripts(vo, user, getScripts(user, vo.getMethod()), "D", "precedent")) {
							return null;
						}
					}

					String ret = null;
					final List co = user.getUtil().match((List) properties.get(Constants.ALL_FIELDS),
							Constants.component, "context");

					// user.getUtil().filter(, "nature", "dependent");
					// final List po =
					// user.getUtil().match(user.getUtil().match(
					// (List) properties.get(Constants.ALL_FIELDS),
					// Constants.component, "context"), "nature",
					// "dependent");
					String p = null;
					String pkf = vo.getKey();
					if (pkf == null) {
						p = (String) pk.get(Constants.name);
					} else {
						p = pkf;
					}
					String[] ids = mvo.getMultipleId();

					if (ids.length == 0 && vo.get(pkf) != null) {
						ids = new String[] { vo.get(pkf).toString() };
					}
					if (ids.length == 0 && user.getUtil().valid(mvo.getFiltro())) // removendo
																					// apenas
																					// com
																					// filtro
																					// sem
																					// chaves
					{
						ids = user.getUtil().extractArrayFromList(lista(mvo, user),
								(String) getPrimaryKey().get(Constants.name), true);
					}

					for (final String id : ids) {
						final Iterator ix = co.iterator();
						if (id == null) {
							continue;
						}
						final GenericVO rvo = createVO(user).putKey(pkf, id);
						if (!processCalls(rvo, user, "D", "before", getCalls(user, vo.getMethod()))) {
							continue;

						}
						while (ix.hasNext()) {
							final Map c = (Map) ix.next();
							GenericService service = null;
							if ("true".equals(c.get("volatile"))) {
								continue;
							}

							try {
								service = (GenericService) user.getFactory().getSpringBean(c.get("service").toString(),
										user);
							} catch (final Exception e) {

							}

							String a = "";
							rvo.setPaginate(false);
							rvo.match(vo);
							rvo.setSort("");
							final List data = lista(rvo, user);

							final List l = user.getUtil()
									.match(user.getUtil().filter(
											(List) service.getProperties().get(Constants.ALL_FIELDS), "volatile",
											Constants.true_str), Constants.name, pkf);
							if (l.size() > 0) {
								rvo.setKey(p);
								rvo.setMultipleId(new String[] { id });
								a = (String) service.invoke("remove", rvo, user);
							} else {
								// remocao de contextos sem a chave do
								// Pai
								final GenericVO fvo = service.createVO(user);
								vo.setKey((String) c.get(Constants.name));
								vo.setMultipleId(user.getUtil().extractArrayFromList(data,
										c.get(Constants.name).toString(), true));
								a = (String) service.invoke("remove", fvo, user);
							}

						}

						final ArrayList chil = (ArrayList) ((ArrayList) getProperties().get(Constants.CHILD_CONTEXTS))
								.clone();

						// inverte a orderm comecando de contextos menos
						// importantes para os mais, provavelmente
						// removendo
						// contextos que usam chaves de outros antes
						user.getUtil().SortCollection(chil, "order");

						for (final Iterator iterator = chil.iterator(); iterator.hasNext();) {
							final Map object = (Map) iterator.next();
							GenericService service = null;
							if ("volatile".equals(object.get("nature"))) {
								continue;
							}
							try {
								service = (GenericService) user.getFactory()
										.getSpringBean(object.get(Constants.name).toString() + "Service", user);
							} catch (final Exception e) {

							}
							if (service != null && id != null) {
								final GenericVO svo = service.createVO(user);
								svo.match(vo);
								svo.put(p, id);
								svo.put(Constants.CONTEXT_KEY, contextKey);
								svo.setMultipleId(new String[] { id });
								svo.setPaginate(false);

								List a = new ArrayList();
								try {
									a = service.lista(svo, user);
								} catch (Exception e) {

								}
								for (final Iterator iterator2 = a.iterator(); iterator2.hasNext();) {
									final Map object2 = (Map) iterator2.next();

									svo.setKey((String) service.getPrimaryKey().get(Constants.name));
									svo.setMultipleId(new String[] {
											object2.get(service.getPrimaryKey().get(Constants.name)).toString() });
									service.invoke("remove", svo, user);
								}

							}
						}

						// removendo items que dependem deste
						final Collection<Map<String, String>> deps = user.getSysParser()
								.getListValuesFromXQL("/systems/system[@name='" + user.getSchema()
										+ "']/module/bean/property[@service='" + getName() + "' and @cyclic='true']",
										null, user, false, false);

						for (final Object element : deps) {
							final Map<String, String> map = (Map<String, String>) element;
							if (map.get("parent_node") != null && map.get("parent_node").equals("bean")) {
								final GenericService s = (GenericService) getServiceById(
										map.get("parent_id").toString(), user);
								GenericVO v1 = s.createVO(user);
								v1.putAll(map);
								v1.putKey(map.get(Constants.name), id);
								s.getDAO().cleanArrayField(v1, user);
							}
						}
						if (id != null) {
							ret = String.valueOf(getDAO().remove(id, p, vo, user));

							if (isLogable(vo)) {

								user.getLogger()
										.log(user,
												new GenericVO()
														.putAllAndGet(
																user.getUtil()
																		.filterMap(new HashMap(),
																				new String[] { (String) getMainField(
																						vo.getMethod(), user)
																								.get(Constants.name) }),
																false),
												getName(), getProperties().get("table").toString(), vo, new HashMap(),
												vo.getMethod(), getProperties().get("id").toString(),
												String.valueOf(tem.queryForObject(
														Dialeto.getInstance().getDialeto(user).getTransactionID(),
														String.class)));
							}

						}

						if (!processScripts(vo, user, getScripts(user, vo.getMethod()), "D", "dependent")) {
							return null;
						}

						// ix = po.iterator();
						// while (ix.hasNext()) {
						// final Map c = (Map) ix.next();
						// GenericService service = null;
						// try {
						// service = (GenericServiceutil.getFactory()
						// .getSpringBean(c.get("service")
						// .toString(), user);
						// } catch (final Exception e) {
						//
						// }
						//
						// final int a = service.invoke("remove", p,
						// new String[] { id }, user);
						//
						// }
						rvo.setProcess(true);
						processCalls(rvo, user, "D", "after", getCalls(user, "remove"));
					}

					return ret;
				} catch (DataIntegrityViolationException dte) {
					LogUtil.exception(dte, user);
					throw new BusinessException("label.registro.sendoUtilizado", "label.registro.sendoUtilizado");
				} catch (final Exception e) {
					LogUtil.exception(e, user);
					throw new BusinessException(e.getMessage(), e);
				}
			}
		}));
	}

	@Override
	public void removeAll(final Usuario user, boolean log) {

		if (log) {
			final GenericVO vo = createVO(user);
			vo.setProcess(false);
			vo.setPaginate(false);
			try {
				final List a = lista(vo, user);
				vo.setMultipleId(
						user.getUtil().extractArrayFromList(a, (String) getPrimaryKey().get(Constants.name), true));
				vo.setKey((String) getPrimaryKey().get(Constants.name));
				remove(vo, user);
			} catch (final ConfigurationException e) {
				LogUtil.exception(e, user);
			} catch (final Exception e) {
				LogUtil.exception(e, user);
			}
		} else {
			DAO.removeAll(user);
		}

	}

	@Override
	public String requiredObject(final GenericVO vo, final Usuario user) throws Exception {

		final DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(
				ConnectionManager.getInstance(user).getDataSource(user));
		transactionManager.setTransactionSynchronization(transactionManager.SYNCHRONIZATION_ON_ACTUAL_TRANSACTION);

		final TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
		transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		final DAOTemplate tem = new DAOTemplate(transactionManager.getDataSource(), user, createVO(user),
				user.getUtil());
		return (String) transactionTemplate.execute(new TransactionCallback() {
			@Override
			public Object doInTransaction(final TransactionStatus status) throws RuntimeException {
				try {
					final GenericVO ob = obtem(vo, user);
					if (ob.get(getPrimaryKey().get(Constants.name)) == null) {
						return inclui(vo, user);
					} else {
						final List all = (List) getProperties().get(Constants.ALL_FIELDS);
						// [@component='hidden_context']

						return ob.get(pk.get(Constants.name)).toString();
					}
				} catch (final Exception e) {
					LogUtil.exception(e, user);
					throw new BusinessException(e.getMessage(), e);
				}
			}
		});
	}

	private Object resolvInjectMethod(final Map object, final Usuario user, final GenericVO vo) throws Exception {
		final List pro = (List) object.get(Constants.ALL_FIELDS);
		// Listagens
		if (object.get("type").toString().equalsIgnoreCase("L")) {
			List<Map<String, String>> ret = new ArrayList<Map<String, String>>();
			final Iterator ix = pro.iterator();
			if ((vo.get("NO_SCRIPT_PROCESS") == null)
					&& !processScripts(vo, user, (Map) object.get(Constants.SCRIPTS), "L", "precedent")) {
				return vo;
			}

			try {
				vo.put(Constants.ALL_FIELDS, pro);
				if (!processCalls(vo, user, "L", "before", (List) object.get(Constants.CALLS))) {
					return null;
				}
			} catch (final IllegalArgumentException e) {
				LogUtil.exception(e, user);
			} catch (final IllegalAccessException e) {
				LogUtil.exception(e, user);
			} catch (final InvocationTargetException e) {
				LogUtil.exception(e, user);
			} catch (final ClassNotFoundException e) {
				LogUtil.exception(e, user);
			}

			while (ix.hasNext()) {
				final Map object3 = (Map) ix.next();
				if (object3.get(Constants.component) == null)
					continue;
				if (object3.get(Constants.component).equals("labelTransform")
						|| object3.get(Constants.component).equals("iconTransform") && vo.containsKey("FORCE_*_TEXT")) {
					final Map values = user.getUtil().JSONtoMap((String) object3.get("value"));
					final Iterator i = values.keySet().iterator();
					final String[] comp = object3.get(Constants.name).toString().split(":");
					String bind = (String) object3.get("bind");
					if (bind == null) {
						bind = "";
					}
					final String binder[] = bind.split(":");

					boolean returnnow = false;
					while (i.hasNext()) {
						final String object2 = (String) i.next();
						final Map<String, String> put = new HashMap<String, String>();
						put.put(comp[0], object2);
						put.put(comp[1], user.getLabel((String) values.get(object2)));
						if (binder.length > 0 && binder[0].equals("step")) {
							ret.add(put);
							if ((vo.get(binder[1]) == null || vo.get(binder[1]).equals(object2)) && !returnnow) {
								returnnow = true;
								continue;
							}
							if (returnnow) {
								return ret;
							}

						} else {
							ret.add(put);
						}

					}
					if (vo != null) {
						vo.setTotal(String.valueOf(ret.size()));
						if (ret.size() > Integer.parseInt(vo.getLimit()) && vo.isPaginate()) {
							return ret.subList(Integer.valueOf(vo.getStart()), Integer.valueOf(vo.getLimit()));
						}
					}
					return ret;
				} else if (object3.get(Constants.component).equals("iconTransform")) {
					final Map values = user.getUtil().JSONtoMap((String) object3.get("value"));
					final Iterator i = values.keySet().iterator();
					final String[] comp = object3.get(Constants.name).toString().split(":");
					String sz = "";
					if (object3.get("maxlength") != null) {
						sz = "&amp;sz=" + object3.get("maxlength");
					}
					while (i.hasNext()) {
						final String object2 = (String) i.next();
						final Map<String, String> put = new HashMap<String, String>();
						put.put(comp[0], object2);
						put.put(comp[1],
								user.getSystemPath() + "/ItemLinguagemAction.do?" + Constants.ActionParam
										+ "=icon&amp;NOME_CONFIG_SISTEMA=" + user.getLang() + Constants.CACHE_SEPARATOR
										+ (String) values.get(object2)
										+ "&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache" + sz);
						ret.add(put);

					}
				}

			}

			// Chamada do DAO se necessario por enquanto so resolvendo
			// calls

			if ((vo.get("NO_SCRIPT_PROCESS") == null)
					&& !processScripts(vo, user, (Map) object.get(Constants.SCRIPTS), "L", "dependent")) {
				return vo;
			}

			try {
				processCalls(vo, user, "L", "after", (List) object.get(Constants.CALLS));
			} catch (final IllegalArgumentException e) {
				LogUtil.exception(e, user);
			} catch (final IllegalAccessException e) {
				LogUtil.exception(e, user);
			} catch (final InvocationTargetException e) {
				LogUtil.exception(e, user);
			} catch (final ClassNotFoundException e) {
				LogUtil.exception(e, user);
			}
			if (ret.size() == 0)
				ret = (List<Map<String, String>>) vo.get(Constants.LIST_DATA);
			if (ret == null) {
				ret = new ArrayList<Map<String, String>>();
			}

			vo.remove(Constants.LIST_DATA);
			vo.remove(Constants.ALL_FIELDS);

			if (vo != null && vo.getPaginate()) {
				vo.setTotal(String.valueOf(ret.size()));
				if (ret.size() > Integer.valueOf(Constants.gridSize)) {
					return ret.subList(Integer.valueOf(vo.getStart()), Integer.valueOf(vo.getLimit()));
				}
			}
			return ret;

		}
		if (object.get("type").toString().equalsIgnoreCase("U")) {
			vo.setMethod((String) object.get(Constants.name));
			return altera(vo, user);

		}
		if (object.get("type").toString().equalsIgnoreCase("C")) {

			return inclui(vo, user);

		}

		if (object.get("type").toString().equalsIgnoreCase("R")) {

			return obtem(vo, user);

		}

		if (object.get("type").toString().equalsIgnoreCase("S")) {

			final GraphMLExporter exporter = new GraphMLExporter(this, user, (Writer) vo.get("WRITER"),
					user.getEncode());
			vo.setLazyList(true);
			vo.setPaginate(false);
			vo.put("EXPORTER", exporter);
			if (!processScripts(vo, user, (Map) object.get(Constants.SCRIPTS), "S", "precedent")) {
				return vo;
			}

			processCalls(vo, user, "S", "before", (List) object.get(Constants.CALLS));

			processCalls(vo, user, "S", "after", (List) object.get(Constants.CALLS));
			if (!processScripts(vo, user, (Map) object.get(Constants.SCRIPTS), "S", "dependent")) {
				return vo;
			}
			return null;

		}
		return null;
	}

	public void setCheck_code(final int check_code) {
		this.check_code = check_code;
	}

	public void setDAO(final GenericDAO dao) {
		DAO = dao;
	}

	// private call only for implemented classes
	public void setProperties(final Map properties) {

		this.properties.putAll(properties);
		Usuario u = new Usuario();
		setProperties(this.properties, u);
	}

	public void setProperties(final Map properties, final Usuario user) {

		if (debugger == null && debugJS) {
			debugger = null; // new
								// RhinoDebugger("transport=socket,suspend=y,address=9000");
			try {
				// debugger.start();
			} catch (Exception e1) {
				LogUtil.exception(e1, user);
			}
	
		}

		this.properties = new HashMap(properties);
		name = this.properties.get(Constants.name) + "Service";
		// engine.setContext((ScriptContext) org.mozilla.javascript.Context.);
		// // manager.getEngineByName("js"); //.setContext();
		this.mVO = null;
		if (this.properties != null) {
			if (this.properties.get("cached") != null

					&& this.properties.get("cached").equals(Constants.true_str)) {
				this.cached = true;
			}
			try {
				List all = (List) properties.get(Constants.ALL_FIELDS);
				if (all == null) {
					all = new ArrayList();
				}
				all = DAO.whitExtensions(all, user);

				all.addAll(
						((ParseXml) user.getSysParser()).getListValuesFromXQL(
								"/systems/system[@name='" + user.getSchema() + "']/module/bean[@name='"
										+ this.properties.get(Constants.name)
										+ "']/*[name() = \"property\" or name() = \"container\"]",
								null, user, true, true));
				if (properties.get("extends") != null) {
					final String[] extensions = properties.get("extends").toString().split(",");
					for (final String extension : extensions) {
						all.addAll(((ParseXml) user.getSysParser()).getListValuesFromXQL(
								"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + extension
										+ "']/*[name() = \"property\" or name() = \"container\"]",
								null, user, true, true));
					}

				}

				if (all.size() == 0 && this.properties.containsKey(Constants.ALL_FIELDS)) {
					all = (List) this.properties.get(Constants.ALL_FIELDS);
				}
				this.properties.put(Constants.ALL_FIELDS, all);
				final List primaryKey = user.getUtil().filter(all, "primaryKey", null);

				if (primaryKey.size() > 0) {
					pk = (Map) primaryKey.get(0);
					try {
						this.properties.put(Constants.NOME_CHAVE, pk.get(Constants.name));
					} catch (final Exception e) {

					}
				}

				final List listKey = user.getUtil().filter(all, "listKey", null);

				if (listKey.size() > 0) {
					lk = (Map) listKey.get(0);
				}

				try {
					this.properties.put(Constants.RELATION_KEY,
							user.getUtil().extractArrayFromList(user.getUtil().filter(all, "relationKey", null),
									Constants.name, true)[0]);
				} catch (final Exception e) {

				}

				try {
					this.properties
							.put(Constants.DEFAULT_SEARCH,
									user.getUtil().extractArrayFromList(
											user.getUtil().filter(all, "defaultSearch", null), Constants.name,
											true)[0]);
				} catch (final Exception e) {

				}
				final List aM = user.getUtil().filter(all, "mainField", null);
				String n = "";
				if (aM.isEmpty()) {
					this.properties.put(Constants.MAIN_FIELD, n);
				} else {
					final Map a = (Map) aM.get(0);
					mf = a;
					if (a.get(Constants.component) != null
							&& a.get(Constants.component).toString().equalsIgnoreCase("context")) {
						n = a.get(Constants.name).toString() + "_"
								+ ((ServiceImpl) getServiceById(a.get("service").toString().replaceAll("Service", ""),
										user)).getMainField("lista", user).get(Constants.name);

					} else {
						n = a.get(Constants.name).toString();
					}
					this.properties.put(Constants.MAIN_FIELD, n);
				}

				final List aS = user.getUtil().filter(all, "defaultSort", null);
				if (aS.isEmpty()) {

					this.properties.put(Constants.DEFAULT_SORT, this.properties.get(Constants.MAIN_FIELD));
				} else {
					final Map a = (Map) aS.get(0);

					if (a.get(Constants.component).toString().equalsIgnoreCase("context")) {
						n = ((ParseXml) user.getSysParser())
								.getValueFromXQL(
										"/systems/system[@name='" + user.getSchema() + "']/module/bean[@name='"
												+ a.get("service").toString().replaceAll("Service", "")
												+ "']/property[@defaultSort='true']",
										Constants.name, null, true, false);
						if (n == null) {
							n = (String) ((ServiceImpl) getServiceById(
									a.get("service").toString().replaceAll("Service", ""), user))
											.getMainField("lista", user).get(Constants.name);
						}
					} else {
						n = a.get(Constants.name).toString();
					}
					this.properties.put(Constants.DEFAULT_SORT, n);
				}
				this.properties
						.put(Constants.CHILD_CONTEXTS,
								((ParseXml) user.getSysParser()).getListValuesFromXQL(
										"/systems/system[@name='" + user.getSchema() + "']/module/bean[@master='"
												+ properties.get("id") + "' and @type='D']",
										Constants.name, user, true, true));

			} catch (final Exception e) {
				LogUtil.exception(e, user);
				this.properties.put(Constants.ALL_FIELDS, new ArrayList());
			}

			final List scripts = user.getUtil().match((List) this.properties.get(Constants.ALL_FIELDS),
					new String[] { "type" }, new String[] { "scriptRule" });
			final Map src = new HashMap();
			for (final Iterator iterator = scripts.iterator(); iterator.hasNext();) {
				final Map object = (Map) iterator.next();

				final Compilable compilingEngine = (Compilable) engine;

				try {
					final Map srcvalue = new HashMap();
					if (debugJS) {
						srcvalue.put("src", object.get("CDATA"));
					} else {
						srcvalue.put("src_cmp", compilingEngine.compile((String) object.get("CDATA")));
						srcvalue.put("src", object.get("CDATA"));

					}
					srcvalue.put("nature", object.get("nature"));
					srcvalue.put("name", object.get("name"));
					srcvalue.put("serviceMethod", object.get("serviceMethod"));

					src.put(object.get(Constants.name), srcvalue);

				} catch (final ScriptException e) {
					LogUtil.fatal("ERRO AO COMPILAR SCRIPT " + object.get("CDATA"));
					LogUtil.exception(e, user);
				}
			}
			this.properties.put(Constants.SCRIPTS, src);
			explorer.addAll(
					((ParseXml) user.getSysParser()).getListValuesFromXQL(
							"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='"
									+ this.properties.get("id") + "']/container[@type='explorer']/property",
							Constants.name, user, true, true));
			explorer.size();
		}

	}

	public void trocaSenha(final GenericVO vo, final Usuario user) throws Exception {
		if (vo.get("SENHA").equals(vo.get("NOVA_SENHA"))) {
			throw new BusinessException("senha.diferentes");
		}

		if (!vo.get("NOVA_SENHA").equals(vo.get("CONFIRMA_SENHA"))) {
			throw new BusinessException("senhanova.diferentes");
		}

		if (vo.get("NOVA_SENHA").toString().length() < 8) {
			throw new BusinessException("senhanova.pequena");
		}

		if (vo.get("NOVA_SENHA").toString().indexOf(" ") >= 0) {
			throw new BusinessException("senhanova.espacobranco");
		}

		// primeiro e ultimo caracter - NAO pode ser numero
		try {
			Integer.parseInt(vo.get("NOVA_SENHA").toString().substring(0, 1));
			throw new BusinessException("senhanova.numero_primeiro_ultimo");
		} catch (final NumberFormatException e) {
			// Nao faz nada - validacao OK
		}

		try {
			final String str = vo.get("NOVA_SENHA").toString();
			Integer.parseInt(str.substring(str.length() - 1, str.length()));
			throw new BusinessException("senhanova.numero_primeiro_ultimo");
		} catch (final NumberFormatException e) {
			// Nao faz nada - validacao OK
		}

		String id = (String) vo.get("ID_MEMBRO");
		if (id == null) {
			id = (String) vo.get("ID_USUARIO");
		}

	}

	@Override
	public GenericVO unBindVOContext(final GenericVO vo, final GenericVO newVO, final String reference,
			final Usuario user) {
		// List tmp = new ArrayList();
		final Iterator i = ((GenericVO) vo.clone()).keySet().iterator();
		while (i.hasNext()) {
			final Object object2 = i.next();
			boolean calc = (object2.toString().startsWith("FILE_NAME_" + reference + "_")
					|| object2.toString().startsWith("CONTENT_TYPE_" + reference + "_"));
			final String nname = object2.toString().replaceFirst(reference + "_", "");
			if (object2.toString().startsWith(reference + "_") || calc || nname.equals(vo.getKey())) {
				// vo.remove(object2);
				if (vo.get(object2) != null && (newVO.containsKey(nname) || calc)
				// em obs 16/10/2010 && newVO.get(nname) == null

				) { // valor default da VO deve
					// ser carregado
					// se a vo possui o valor do campo da vo amarrada, ele
					// resgata este valor, senao usa o valor do proprio campo
					// newVO.remove(object2)
					newVO.put(nname, vo.get(object2) != null && vo.get(object2).toString().length() > 0
							? vo.get(object2) : vo.get(nname));
				}
			}

		}

		return newVO;
	}

	@Override
	public String translate(String evaluate, Usuario user, GenericVO vo) {
		return getDAO().translate(evaluate, user, vo);
	}

	@Override
	public String getServiceId() {
		return (String) properties.get("id");
	}

	@Override
	public Object calc(GenericVO vo, Usuario user) {
		return getDAO().calc(vo, user);
	}

	@Override
	public void registerListener(String string, Usuario user) {
		synchronized (wfListeners) {
			Map dl = wfListeners.get(user.getSystem());
			if (dl == null)
				dl = new HashMap();
			dl.put(getName() + "." + string, string);
			wfListeners.put(string, dl);
		}
	}

	public List getAllFields(Usuario user) {
		List f = new ArrayList();
		f.addAll((Collection) getProperties().get(Constants.ALL_FIELDS));
		List m = getMethods(user);
		for (Iterator iterator = m.iterator(); iterator.hasNext();) {
			Map object = (Map) iterator.next();
			if (object.get(Constants.ALL_FIELDS) != null) {
				f.addAll((Collection) object.get(Constants.ALL_FIELDS));
			}
		}
		return f;
	}

}
