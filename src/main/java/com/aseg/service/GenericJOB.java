package com.aseg.service;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.aseg.config.Constants;
import com.aseg.seguranca.Usuario;

public abstract class GenericJOB implements StatefulJob {
	private long totalSize = 0;
	private long bytesRead = 0;
	private long elapsedTime = 0;
	private String status = "done";
	private int fileIndex = 0;
	private boolean inProgress = true;

	public void setInProgress(boolean b) {
		inProgress = b;
	}

	public long getBytesRead() {
		return bytesRead;
	}

	public long getElapsedTime() {
		return elapsedTime;
	}

	public int getFileIndex() {
		return fileIndex;
	}

	public String getStatus() {
		return status;
	}

	public long getTotalSize() {
		return totalSize;
	}

	public boolean isInProgress() {
		return inProgress;
	}

	public void setBytesRead(final long bytesRead) {
		this.bytesRead = bytesRead;
		this.status = "Processando " + bytesRead + " de " + getTotalSize();
	}

	public void setElapsedTime(final long elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public void setFileIndex(final int fileIndex) {
		this.fileIndex = fileIndex;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public void setTotalSize(final long totalSize) {
		this.totalSize = totalSize;
	}

	@Override
	public String toString() {
		return "[UploadInfo]\n" + " totalSize= " + totalSize + "\n" + " bytesRead= " + bytesRead + "\n"
				+ " elapsedTime= " + elapsedTime + "\n" + " status= '" + status + "'\n" + " fileIndex= " + fileIndex
				+ "\n" + "[/ UploadInfo]\n";
	}

	public Object getServiceById(final String name, final Usuario user) {
		final String Name = user.getSysParser().getValueFromXQL(
				"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + name + "']", Constants.name,
				user, true, false);
		try {
			if (Name.length() == 0) {
				return null;
			}
			return user.getFactory().getSpringBean(Name + "Service", user);
		} catch (final Exception e) {
			return null;
		}
	}

	@Override
	public abstract void execute(JobExecutionContext arg0) throws JobExecutionException;

}
