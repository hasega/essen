package com.aseg.service;

import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.aseg.dao.GenericDAO;
import com.aseg.email.service.EmailService;
import com.aseg.seguranca.Usuario;
import com.aseg.vo.GenericVO;

public interface GenericService

{
	public String altera(GenericVO vo, Usuario user) throws Exception;

	public GenericVO bindVOContext(GenericVO putAllAndGet, String string, Usuario user);

	public GenericVO createVO(Usuario user);

	public List describe(GenericVO vo, Usuario user);

	public boolean existe(GenericVO vo, Usuario user) throws Exception;

	public List getChilds(Usuario user);

	public GenericDAO getDAO();

	public List getExplorerPropertyes(Usuario user);

	public Map getMainField(String method, final Usuario user);

	public Map getMethod(String name, Usuario user);

	public List<Map<String, Object>> getMethods(Usuario user);

	public String getName();

	public ServiceImpl getParent(Usuario user);

	public Map getPrimaryKey();

	public Object calc(GenericVO vo, Usuario user);

	public Map<String, Object> getProperties();

	public Map getProperty(String object, Usuario user);

	public Object getService(String name, Usuario user);

	public Object getServiceById(String name, Usuario user);

	public String inclui(GenericVO vo, Usuario user) throws Exception;

	public Object invoke(String method, GenericVO vo, Usuario user) throws Exception;

	public List lista(GenericVO vo, Usuario user) throws Exception;

	public GenericVO obtem(GenericVO vo, Usuario user) throws Exception;

	public String remove(GenericVO vo, Usuario user) throws Exception;

	public void removeAll(Usuario user, boolean log);

	public String requiredObject(final GenericVO vo, final Usuario user) throws Exception;

	public GenericVO unBindVOContext(GenericVO vo, GenericVO nvo, String string, Usuario user);

	public String merge(final GenericVO vo, final Usuario user) throws Exception;

	public Usuario bindUser(final Usuario user, final GenericVO valores, boolean RealEevent);

	public boolean prepare(GenericVO vo, Usuario user);

	public String translate(String evaluate, Usuario user, GenericVO vo);

	public Object getServiceId();

	public void logout(Usuario user) throws Exception;

	public boolean isDefaultCRUD(String string);

	public Map<String, String> getListKey();

	public void registerListener(String string, Usuario user);

	public EmailService getEmailService(Usuario user);

	public void exportListBean(final List<GenericVO> lista, final Usuario user, final Writer writer) throws Exception;

	public void exportBean(final GenericVO vo, final Usuario user, final Writer writer) throws Exception;

	public void loopLista(final GenericVO vo, String src, final Usuario user);

	public GenericVO createEmptyVO(Usuario user);

	public List getAllFields(Usuario user);
}
