package com.aseg.service.fileMigration;

public class FileMigratorModel {

	private boolean toDataBase = false;
	private boolean toFileSystem = false;
	private String directory;
	private boolean force = false;
	private boolean useFileName = false;
	private String oldConfig;

	public boolean isToDataBase() {
		return toDataBase;
	}

	public void setToDataBase(boolean toDataBase) {
		this.toDataBase = toDataBase;
	}

	public boolean isToFileSystem() {
		return toFileSystem;
	}

	public void setToFileSystem(boolean toFileSystem) {
		this.toFileSystem = toFileSystem;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public boolean isForce() {
		return force;
	}

	public void setForce(boolean force) {
		this.force = force;
	}

	public String getOldConfig() {
		return oldConfig;
	}

	public void setOldConfig(String oldConfig) {
		this.oldConfig = oldConfig;
	}

	public boolean isUseFileName() {
		return useFileName;
	}

	public void setUseFileName(boolean useFileName) {
		this.useFileName = useFileName;
	}

}
