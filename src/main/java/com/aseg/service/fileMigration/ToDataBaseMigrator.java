package com.aseg.service.fileMigration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.jdbc.support.lob.DefaultLobHandler;

import com.aseg.config.Constants;
import com.aseg.dao.DAOImpl;
import com.aseg.exceptions.FileMigrationException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;

public class ToDataBaseMigrator extends AFileMigrator {

	private LinkedHashSet<String> validationMsgs = new LinkedHashSet<String>();
	private boolean migrationLocked = false;
	private List<SqlUpdate> sqlUpdates = new ArrayList<SqlUpdate>();

	public ToDataBaseMigrator(JdbcTemplate jdbc, FileMigratorModel model, Usuario user) {
		super(jdbc, model, user);
	}

	@Override
	public boolean migrate(List<ServiceImpl> services) throws FileMigrationException {

		String fileDir = getClientFileDir(model.getOldConfig(), user);

		for (ServiceImpl s : services) {
			List<Map<String, String>> propsToMigrate = getFileFields(s, user);
			for (Map<String, String> propFile : propsToMigrate) {
				if (propFile.containsKey("storage")) {
					String propFileDir = getFileDirToBeanProperty(fileDir, propFile);
					File beanDir = new File(propFileDir);
					if (beanDir.exists()) {
						for (File f : beanDir.listFiles()) {
							try {
								migrateBeanFiles(model, s, user, propFile, f);
							} catch (IOException e) {
								throw new FileMigrationException(
										user.getLabel(
												"lb.file.migration.processo.de.entrada.e.ou.saida.de.dados.foi.interrompido"),
										e);
							}
						}
					}
				}
			}
		}

		if (validationMsgs.isEmpty() || model.isForce()) {
			try {
				FileUtils.deleteDirectory(new File(getClientFileDir(model.getOldConfig(), user)));
			} catch (IOException e) {
				LogUtil.exception(e, user);
			}
		}

		return validationMsgs.isEmpty();
	}

	private void migrateBeanFiles(FileMigratorModel model, ServiceImpl service, Usuario user,
			Map<String, String> propFile, File file) throws IOException, FileMigrationException {
		File[] beanFiles = file.listFiles();
		int count = 1;
		for (File bf : beanFiles) {
			LogUtil.debug("Migrando : " + service.getName() + " - item : " + count + " de : " + beanFiles.length
					+ " - arquivo : " + bf.getName(), user);
			try {
				getIdOfFile(bf);
			} catch (FileMigrationException e) {
				// Esta condio alcanada caso seja lanada uma exception pelo
				// mtodo getIdOfFile,
				// que realiza uma validao no nome de arquivo, caso no passe,
				// retornada esta exceo.
				if (!model.isForce()) {
					throw e;
				}
				continue;
			}

			if (!migrationIsLocked(model, service, bf, user)) {
				byte[] byteFile = fileToByteArray(bf);
				if (byteFile.length > 0) {
					String sqlUpdate = getSqlUpdate(service, propFile, bf);
					SqlUpdate update = new SqlUpdate(jdbc.getDataSource(), sqlUpdate);
					final DefaultLobHandler lobHandler = new DefaultLobHandler();
					SqlLobValue lobValue = new SqlLobValue(byteFile, lobHandler);
					update.declareParameter(new SqlParameter(propFile.get(Constants.name), Types.BLOB));
					sqlUpdates.add(update);
					doInTransaction(user, update, new Object[] { lobValue });
				}
			}
			count++;
		}
	}

	private String getSqlUpdate(ServiceImpl service, Map<String, String> propFile, File file)
			throws FileMigrationException {
		String servicePk = "" + service.getPrimaryKey().get(Constants.name);
		return "UPDATE " + service.getDAO().getEntity().get("table") + " set " + propFile.get(Constants.name)
				+ " = ? WHERE " + servicePk + " = "
				+ prepareFieldValueToSql(service.getPrimaryKey(), getIdOfFile(file), ((DAOImpl) service.getDAO()));
	}

	private String getSqlToBeanFile(ServiceImpl service, File file) throws FileMigrationException {
		String servicePk = "" + service.getPrimaryKey().get(Constants.name);
		String beanFileId = prepareFieldValueToSql(service.getPrimaryKey(), getIdOfFile(file),
				((DAOImpl) service.getDAO()));
		return "SELECT " + servicePk + " FROM " + service.getDAO().getEntity().get("table") + " WHERE " + servicePk
				+ " = " + beanFileId;

	}

	// Retorna apenas a ID do registro que possui este arquivo, removendo
	// pjw(???) e o nome do arquivo se houver...
	private String getIdOfFile(File file) throws FileMigrationException {
		String fileId = file.getName().replace("pjw(", "");
		fileId = fileId.substring(0, fileId.indexOf(")"));

		if (!fileId.matches("^\\d+$")) {
			throw new FileMigrationException("lb.file.migration.invalid.file.name " + file.getName());
		}

		return fileId;
	}

	private boolean migrationIsLocked(FileMigratorModel model, ServiceImpl service, File file, Usuario user)
			throws IOException, FileMigrationException {
		String servicePk = "" + service.getPrimaryKey().get(Constants.name);
		String sql = getSqlToBeanFile(service, file);
		if (model.isForce()) {
			Map<String, Object> queryRes = jdbc.queryForMap(sql);
			if (queryRes.get(servicePk) == null) {
				FileUtils.deleteDirectory(file);
			}
			return false;
		} else {
			Map<String, Object> queryRes = jdbc.queryForMap(sql);
			if (queryRes.get(servicePk) == null) {
				return migrationLocked = true;
			}
		}
		return migrationLocked;
	}

	@Override
	public void validate(List<ServiceImpl> services) {
	}

	private byte[] fileToByteArray(File file) {
		InputStream is = null;
		byte[] buffer = null;
		try {
			is = new FileInputStream(file);
			buffer = new byte[is.available()];
			is.read(buffer);
			is.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return buffer;
	}

	@Override
	public void rollBack() {
		for (SqlUpdate u : sqlUpdates) {
			doInTransaction(user, u, new Object[] { null });
		}
	}

	@Override
	public LinkedHashSet<String> getValidationMsgs() {
		return this.validationMsgs;
	}

}
