package com.aseg.service.fileMigration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import com.aseg.exceptions.BusinessException;
import com.aseg.exceptions.ConfigurationException;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.util.ConnectionManager;
import com.aseg.vo.GenericVO;

public class FileMigratorService {

	private static FileMigratorService instance;

	private FileMigratorService() {
	}

	public static FileMigratorService getInstance(Usuario user) {
		if (instance == null) {
			instance = new FileMigratorService();
		}
		return instance;
	}

	// Realiza a migrao de arquivos
	public boolean fileMigration(GenericVO vo, Usuario user) throws BusinessException, Exception {

		FileMigratorModel model = populateMigrationModel(vo);
		IFileMigrator migrator = getFileMigrator(model, user);
		List<ServiceImpl> servicesTmg = getServicesToMigrate(user);

		try {
			try {
				if (migrator.migrate(servicesTmg)) { // Migrao no realizada com
														// sucesso.
					updateSystemConfig(model, user);
					//user.addActionMessage("lb.file.migration.migracao.realizada.com.sucesso", "", true);
				} else { // Ocorreu algum problema durante a migrao, e no para
							// forar
					throw new BusinessException(getAllValidationMessages(migrator.getValidationMsgs()));
				}
			} catch (BusinessException e) {
				throw e;
			} catch (Exception e) {
				throw new BusinessException(e.getMessage());
			}
		} catch (BusinessException ex) {
			migrator.rollBack();
			throw ex;
		}

		return false;
	}

	// Retorna todas as mensagens da validao, formatadas para serem exibidas ao
	// usurio
	private String getAllValidationMessages(LinkedHashSet<String> messages) {
		String msgs = "";
		for (String m : messages) {
			msgs += m + "</br>";
		}
		return msgs;
	}

	// Atualiza a configurao do sistema ( LOCALIZACAO_SERVIDOR ) de acordo com
	// as opes inseridas pelo usurio.
	private void updateSystemConfig(FileMigratorModel model, Usuario user) throws Exception {
		String valorConfig = null;
		if (model.isToFileSystem()) {
			valorConfig = model.getDirectory();
		}
		ServiceImpl gedServ = (ServiceImpl) user.getUtil().getService("GEDConfigItemService", user);
		GenericVO gedVO = gedServ.createVO(user);
		gedVO.searchBy(null);
		gedVO.putKey("NOME_CONFIG_SISTEMA", "LOCALIZACAO_SERVIDOR");
		gedVO.put("VALOR_CONFIG_SISTEMA", valorConfig);
		gedVO.put("TIPO_CONFIG_SISTEMA", "IASP");
		gedVO.put("ID_PAI", getUserSystemVO(user).get("ID_CONFIG_SISTEMA"));
		gedServ.merge(gedVO, user);
		if (model.isToFileSystem()) {
			gedVO.putKey("NOME_CONFIG_SISTEMA", "LOCALIZACAO_USE_FILE_NAME");
			gedVO.put("VALOR_CONFIG_SISTEMA", model.isUseFileName());
			gedServ.merge(gedVO, user);
		}
		gedServ.merge(gedVO, user);
		user.refreshSystem(user.getSystem());
	}

	private GenericVO getUserSystemVO(Usuario user) {
		ServiceImpl aspServ = (ServiceImpl) user.getUtil().getService("ASPConfigService", user);
		GenericVO aspVO = aspServ.createVO(user);
		aspVO.putKey("NOME_CONFIG_SISTEMA", user.getSchema());
		try {
			aspVO = aspServ.obtem(aspVO, user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aspVO;
	}

	private static JdbcTemplate getJdbcTemplate(Usuario user) {
		try {
			return new JdbcTemplate(ConnectionManager.getInstance(user).getDataSource(user));
		} catch (ConfigurationException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	// Retorna a instncia do migrador de arquivos
	private IFileMigrator getFileMigrator(FileMigratorModel model, Usuario user) {
		if (model.isToFileSystem()) {
			return new ToFileSystemMigrator(getJdbcTemplate(user), model, user);
		} else if (model.isToDataBase()) {
			return new ToDataBaseMigrator(getJdbcTemplate(user), model, user);
		}
		return null;
	}

	// Transforma os valores da VO em um objeto FileMigrationModel
	private FileMigratorModel populateMigrationModel(GenericVO vo) {
		FileMigratorModel model = new FileMigratorModel();
		if (vo.get("TIPO_DE_REPOSITORIO") != null && vo.get("TIPO_DE_REPOSITORIO").equals("F")) {
			model.setToFileSystem(true);
			if (vo.get("DIRETORIO") != null) {
				model.setDirectory(String.valueOf(vo.get("DIRETORIO")));
			}
			if (vo.get("USE_FILE_NAME") != null) {
				if (vo.get("USE_FILE_NAME").equals("T")) {
					model.setUseFileName(true);
				} else {
					model.setUseFileName(false);
				}
			}
		} else {
			model.setToDataBase(true);
		}
		if (vo.get("FORCE") != null) {
			if (vo.get("FORCE").equals("T")) {
				model.setForce(true);
			} else {
				model.setForce(false);
			}
		}
		if (vo.get("OLD_CONFIG") != null) {
			model.setOldConfig("" + vo.get("OLD_CONFIG"));
		}
		return model;
	}

	// Retorna todos os services que possuem alguma property type="file", com
	// storage
	private List<ServiceImpl> getServicesToMigrate(Usuario user) {
		List<Map<String, String>> propertyFiles = (List<Map<String, String>>) user.getSysParser()
				.getListValuesFromXQL("//property[@type='file' and @storage]", null, user, false, false);
		List<ServiceImpl> servicesTmg = new ArrayList<ServiceImpl>();

		for (Map<String, String> prop : propertyFiles) {
			if (prop.get("parent_node").equals("bean")) {
				servicesTmg.add((ServiceImpl) user.getUtil().getServiceById(prop.get("parent_id"), user));
			} else if (prop.get("parent_node").equals("method")) {
				// Recuperar o bean...
			}
		}

		return servicesTmg;
	}

}