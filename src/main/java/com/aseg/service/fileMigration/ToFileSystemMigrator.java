package com.aseg.service.fileMigration;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StringUtils;

import com.aseg.config.Constants;
import com.aseg.dao.DAOImpl;
import com.aseg.exceptions.FileMigrationException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;

public class ToFileSystemMigrator extends AFileMigrator {

	private boolean migrationLocked = false;
	LinkedHashSet<String> validationMsgs = new LinkedHashSet<String>();

	public ToFileSystemMigrator(JdbcTemplate jdbc, FileMigratorModel model, Usuario user) {
		super(jdbc, model, user);
	}

	@Override
	public boolean migrate(List<ServiceImpl> services) throws FileMigrationException {
		LogUtil.debug("Iniciando migrao.", user);
		validate(services);
		LogUtil.debug("Validao realizada com sucesso!", user);
		if (validationMsgs.isEmpty()) {
			try {
				if (isFromDataBaseToFileSystem()) { // from : dataBase, to :
													// fileSystem
					migrateFromDataBaseToFileSystem(services, user);
				} else { // from : fileSystem, to : fileSystem
					migratePath(user);
				}
			} catch (Exception e) {
				throw new FileMigrationException(e.getMessage(), e);
			}
		}
		return validationMsgs.isEmpty();
	}

	private boolean isFromDataBaseToFileSystem() {
		return model.getOldConfig() == null || model.getOldConfig().isEmpty();
	}

	private void migrateFromDataBaseToFileSystem(List<ServiceImpl> services, Usuario user)
			throws FileMigrationException, IOException {
		LogUtil.debug("Realizando migrao da base de dados para fileSystem ...", user);
		String fileDir = getClientFileDir(model.getDirectory(), user);
		List<String> updateToNullSql = new ArrayList<String>();
		for (ServiceImpl s : services) {
			List<Map<String, String>> propsToMigrate = getFileFields(s, user);
			List<Map<String, Object>> listItems = jdbc.queryForList(getSelectFields(s, null, user, null));
			updateToNullSql.add(getUpdateFieldsToNull(s, propsToMigrate, user));
			int count = 1;
			for (Map<String, Object> dbItem : listItems) {
				for (Map<String, String> propFile : propsToMigrate) {
					if (propFile.containsKey("storage") && !migrationIsLocked(s, dbItem, propFile, user)) {
						dbItem = jdbc.queryForMap(getSelectFields(s, propsToMigrate, user,
								"" + dbItem.get(s.getPrimaryKey().get(Constants.name))));
						if (dbItem.get(propFile.get(Constants.name)) != null) {
							try {
								if (((byte[]) dbItem.get(propFile.get(Constants.name))).length > 0) {
									ByteArrayInputStream byteArrayIS = new ByteArrayInputStream(
											(byte[]) dbItem.get(propFile.get(Constants.name)));
									String fileOutDir = getFileDirToBeanItem(fileDir, s, propFile, dbItem);
									String fileSuffix = "";
									if (model.isUseFileName()) {
										Map<String, String> propFileName = getPropertyFileName(s, propFile, user);
										if (propFileName != null
												&& dbItem.get(propFileName.get(Constants.name)) != null) {
											fileSuffix = "-" + dbItem.get(propFileName.get(Constants.name));
										}
									}
									new File(fileOutDir).mkdirs();
									String fileName = "pjw(" + dbItem.get(s.getPrimaryKey().get(Constants.name)) + ")"
											+ fileSuffix;
									LogUtil.debug("Migrando : " + s.getName() + " - item : " + count + " de : "
											+ listItems.size() + " arquivo : " + fileName, user);
									user.getUtil().writeToDisk(byteArrayIS, fileName, fileOutDir);
								}
							} catch (FileNotFoundException e) {
								FileUtils.deleteDirectory(new File(fileDir));
								throw new FileMigrationException(
										user.getLabel("lb.file.migration.arquivo.ou.pasta.nao.encontrado(a)"), e);
							} catch (IOException e) {
								FileUtils.deleteDirectory(new File(fileDir));
								throw new FileMigrationException(
										user.getLabel(
												"lb.file.migration.processo.de.entrada.e.ou.saida.de.dados.foi.interrompido"),
										e);
							}
							count++;
						}
					}
				}
			}
		}
		if (validationMsgs.isEmpty() || model.isForce()) {
			for (String sql : updateToNullSql) {
				doInTransaction(user, sql);
			}
		}
	}

	private boolean migrationIsLocked(ServiceImpl service, Map<String, Object> dbItem, Map<String, String> propertyFile,
			Usuario user) {
		Map<String, String> propertyFileName = getPropertyFileName(service, propertyFile, user);
		if (model.isForce()) {
			if (propertyFileName != null && (dbItem.get(propertyFile.get(Constants.name)) == null
					&& (dbItem.get(propertyFileName.get(Constants.name)) != null
							&& !dbItem.get(propertyFileName.get(Constants.name)).equals("")))) {
				String servicePk = String.valueOf(service.getPrimaryKey().get(Constants.name));
				String table = service.getDAO().getEntity().get("table");
				doInTransaction(user, "UPDATE " + table + " SET " + propertyFileName.get(Constants.name)
						+ " = '' WHERE " + servicePk + " = " + dbItem.get(servicePk));
			}
			return false;
		} else {
			if (propertyFileName != null && (dbItem.get(propertyFile.get(Constants.name)) == null
					&& (dbItem.get(propertyFileName.get(Constants.name)) != null
							&& !dbItem.get(propertyFileName.get(Constants.name)).equals("")))) {
				validationMsgs.add(" - "
						+ user.getLabel("lb.file.migrator.encontrado.nome.sem.arquivo.contexto.{0}.nome.arquivo.{1}",
								new String[] { service.getName().replace(Constants.SERVICE_BASE, ""),
										String.valueOf(dbItem.get(propertyFileName.get(Constants.name))), }));
				return migrationLocked = true;
			}
		}
		return migrationLocked;
	}

	private void migratePath(Usuario user) throws IOException {
		File oldPath = new File(model.getOldConfig());
		File newPath = new File(model.getDirectory());
		FileUtils.copyDirectory(oldPath, newPath);
		try {
			FileUtils.deleteDirectory(new File(getClientFileDir(model.getOldConfig(), user)));
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
	}

	// Cria um select a id do service + os campos passados em props, e caso seja
	// passado uma ID inserida no WHERE...
	private String getSelectFields(ServiceImpl service, List<Map<String, String>> props, Usuario user, String id) {
		List<String> fields = new ArrayList<String>();
		String where = "";
		String servicePk = "" + service.getPrimaryKey().get(Constants.name);
		if (props != null) {
			if (id != null) {
				where = " WHERE " + servicePk + " = "
						+ prepareFieldValueToSql(service.getPrimaryKey(), id, ((DAOImpl) service.getDAO()));
			}
			for (Map<String, String> p : props) {
				Map<String, String> fileNameProp = getPropertyFileName(service, p, user);
				// Incluindo nome do arquivo no select, para validaes
				// posteriores
				if (fileNameProp != null) {
					fields.add(fileNameProp.get(Constants.name));
				}
				fields.add(p.get(Constants.name));
			}
		}
		List<Map<String, String>> fk = user.getUtil().match(service.getAllFields(user), "relationKey", "true");
		if (!fk.isEmpty()) {
			if (fields.isEmpty()) {
				fields.add(fk.get(0).get(Constants.name));
			} else {
				fields.add(fk.get(0).get(Constants.name));
			}
		}
		String selectFields = "";
		if (!fields.isEmpty()) {
			selectFields = "," + StringUtils.arrayToDelimitedString(fields.toArray(), ",");
		}
		return "SELECT " + servicePk + selectFields + " FROM " + service.getDAO().getEntity().get("table") + where;
	}

	// Atualiza campos type="file" do service para null where serviceId =
	// idValue
	private String getUpdateFieldsToNull(ServiceImpl service, List<Map<String, String>> props, Usuario user) {
		String setFieldsToNull = "";
		for (Map<String, String> p : props) {
			setFieldsToNull += p.get(Constants.name) + " = null,";
		}
		setFieldsToNull = user.getUtil().replaceLastString(setFieldsToNull, ",", "");

		return "UPDATE " + service.getDAO().getEntity().get("table") + " SET " + setFieldsToNull;
	}

	@Override
	public void validate(List<ServiceImpl> services) {
		LinkedHashSet<String> validationMsgs = new LinkedHashSet<String>();
		validadeFileDirectory(user);
	}

	// realiza validaes no fileSytem ( canWrite, canRead,etc... )
	private void validadeFileDirectory(Usuario user) {
		String fileDir = getClientFileDir(model.getDirectory(), user);
		File f = new File(fileDir);
		if (!f.exists()) {
			if (!f.mkdirs()) {
				validationMsgs.add(user.getLabel("lb.file.migration.nao.foi.possivel.criar.diretorio"));
			} else {
				if (!f.canRead()) {
					validationMsgs.add(user.getLabel("lb.file.migration.nao.e.possivel.ler.arquivos.no.diretorio"));
				} else {
					if (!f.canWrite()) {
						validationMsgs.add(user.getLabel("lb.file.migration.nao.e.possivel.gravar.no.diretorio"));
					}
				}
			}
		}
	}

	@Override
	public void rollBack() {
		try {
			FileUtils.deleteDirectory(new File(getClientFileDir(model.getDirectory(), user)));
		} catch (IOException e) {
			LogUtil.exception(e, user);
		}
	}

	@Override
	public LinkedHashSet<String> getValidationMsgs() {
		return this.validationMsgs;
	}

}
