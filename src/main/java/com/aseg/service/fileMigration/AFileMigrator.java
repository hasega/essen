package com.aseg.service.fileMigration;

import java.io.File;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.aseg.config.Constants;
import com.aseg.dao.DAOImpl;
import com.aseg.exceptions.ConfigurationException;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.util.ConnectionManager;

public abstract class AFileMigrator implements IFileMigrator {

	protected JdbcTemplate jdbc;
	protected FileMigratorModel model;
	protected Usuario user;

	@Override
	public abstract boolean migrate(List<ServiceImpl> services) throws Exception;

	@Override
	public abstract void validate(List<ServiceImpl> services);

	public AFileMigrator(FileMigratorModel model, Usuario user) {
		this.model = model;
	}

	public AFileMigrator(JdbcTemplate jdbc, FileMigratorModel model, Usuario user) {
		this.jdbc = jdbc;
		this.model = model;
		this.user = user;
	}

	public void doInTransaction(Usuario user, final SqlUpdate sqlUpdate, final Object[] params) {
		doInTransaction(user, new TransactionCallback() {
			@Override
			public Object doInTransaction(TransactionStatus arg0) {
				return sqlUpdate.update(params);
			}
		});
	}

	public void doInTransaction(Usuario user, final String sql) {
		doInTransaction(user, new TransactionCallback() {
			@Override
			public Object doInTransaction(TransactionStatus arg0) {
				return jdbc.update(sql);
			}
		});
	}

	private void doInTransaction(Usuario user, TransactionCallback callBack) {
		DataSourceTransactionManager transactionManager;
		try {
			transactionManager = new DataSourceTransactionManager(
					ConnectionManager.getInstance(user).getDataSource(user));
			transactionManager.setTransactionSynchronization(transactionManager.SYNCHRONIZATION_ON_ACTUAL_TRANSACTION);
			final TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
			transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
			transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
			transactionTemplate.execute(callBack);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// Retorna a String do diretrio de arquivos do cliente.
	protected String getClientFileDir(String fileDir, Usuario user) {
		return fileDir + File.separatorChar + user.getSystemProperty("SIGLA_CLIENTE");
	}

	// Retorna o diretrio da property do bean, com base no atributo storage
	protected String getFileDirToBeanProperty(String fileDir, Map<String, String> propFile) {
		String[] storage = propFile.get("storage").split("/");
		return fileDir + File.separatorChar + storage[0].toLowerCase();
	}

	// Retorna o diretrio para um registro do bean
	protected String getFileDirToBeanItem(String fileDir, ServiceImpl service, Map<String, String> propertyFile,
			Map<String, Object> dbItem) {
		String[] storage = propertyFile.get("storage").split("/");
		if (dbItem.get(storage[1]) == null) {
			String servicePk = String.valueOf(service.getPrimaryKey().get(Constants.name));
			String table = service.getDAO().getEntity().get("table");
			dbItem.putAll(jdbc.queryForMap(
					"SELECT " + storage[1] + " FROM " + table + " WHERE " + servicePk + " = " + dbItem.get(servicePk)));
		}
		return getFileDirToBeanProperty(fileDir, propertyFile) + File.separatorChar + dbItem.get(storage[1]);
	}

	// Retorna todos os campos type="file" do service
	protected List<Map<String, String>> getFileFields(ServiceImpl service, Usuario user) {
		return user.getUtil().match(service.getAllFields(user), new String[] { "type" }, new String[] { "file" });
	}

	// Retorna a property (se houver), que responsvel por guardar o nome do
	// arquivo, com base no labelValue da propety type="file"
	protected Map<String, String> getPropertyFileName(ServiceImpl service, Map<String, String> propertyFile,
			Usuario user) {
		String fileLabelValue = propertyFile.get(Constants.labelValue);
		Map<String, String> fileNameProperty = null;
		if (fileLabelValue != null) {
			List<Map<String, String>> labelValueProps = user.getUtil().match(service.getAllFields(user), Constants.name,
					fileLabelValue);
			if (!labelValueProps.isEmpty()) {
				fileNameProperty = labelValueProps.get(0);
			}
		}
		return fileNameProperty;
	}

	protected String prepareFieldValueToSql(Map<String, String> field, String value, DAOImpl dao) {
		if (dao.getType(field, user) == Types.VARCHAR) {
			return "'" + value + "'";
		}
		return value;
	}

}
