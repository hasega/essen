package com.aseg.service.fileMigration;

import java.util.LinkedHashSet;
import java.util.List;

public interface IFileMigrator {

	boolean migrate(List<com.aseg.service.ServiceImpl> services) throws Exception;

	LinkedHashSet<String> getValidationMsgs();

	void validate(List<com.aseg.service.ServiceImpl> services);

	void rollBack();

}