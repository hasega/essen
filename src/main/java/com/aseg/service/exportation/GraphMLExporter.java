package com.aseg.service.exportation;

import java.io.Writer;
import java.util.Map;
import java.util.Properties;

import com.aseg.config.Constants;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.util.service.UtilServiceImpl;
import com.jamesmurty.utils.XMLBuilder;

public class GraphMLExporter extends ServiceImpl {
	private final String encoding;

	private XMLBuilder root;

	private final Writer writer;

	private final UtilServiceImpl util = new UtilServiceImpl();

	public GraphMLExporter(final GenericService service, final Usuario user, final Writer writer, final String encoding)
			throws Exception {
		this.writer = writer;
		this.encoding = encoding;

		try {
			this.root = XMLBuilder.create("graphml");
			root.a("xmlns", "http://graphml.graphdrawing.org/xmlns");
			root.a("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			root.a("xmlns:y", "http://www.yworks.com/xml/graphml");
			root.a("xsi:schemaLocation",
					"http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd");

			root.e("key").a("for", "graphml").a("id", "d0").a("yfiles.type", "resources");
			root.e("key").a("for", "node").a("id", "d1").a("attr.type", "string").a("attr.name", "url");
			root.e("key").a("for", "node").a("id", "d2").a("attr.type", "string").a("attr.name", "description");
			root.e("key").a("for", "node").a("id", "d3").a("yfiles.type", "nodegraphics");
			root.e("key").a("for", "edge").a("id", "d6").a("yfiles.type", "edgegraphics");
		} catch (final Exception e) {
			throw e;
		}
	}

	public XMLBuilder addAxis(final Map properties) {
		XMLBuilder r = root.e("edge").a("id", (String) properties.get("id"))
				.a("source", properties.get("source").toString()).a("target", properties.get("target").toString());
		r = r.e("data").a("key", "d6");
		final XMLBuilder e = r.e("y:PolyLineEdge");
		e.e("y:Path").a("sx", "0").a("sy", "0").a("tx", "0").a("ty", "0");
		e.e("y:Point").a("x", "0").a("y", "0");
		e.e("y:Point").a("x", "0").a("y", "0");

		e.e("y:LineStyle").a("color", "#000000").a("type", "line").a("width", "1.0");
		e.e("y:Arrows").a("source", "none").a("target", "standart");
		e.e("y:BendStyle").a("smoothed", Constants.false_str);

		return root;
	}

	public XMLBuilder addNode(final Map properties) {
		if (properties.get("id") == null) {
			return root;
		}
		XMLBuilder r = root.e("graph").a("edgedefault", "directed").e("node").a("id", properties.get("id").toString());
		r.e("data").a("key", "d1");
		r.e("data").a("key", "d2");
		r = r.e("data").a("key", "d3");
		final XMLBuilder e = r.e("y:UMLClassNode");
		e.e("y:Geometry").a("heigth", "100").a("width", String.valueOf(properties.get("text").toString().length() * 10))
				.a("x", "0").a("y", "0");
		e.e("y:Fill").a("color", "#99CCFF").a("transparent", Constants.false_str);
		e.e("y:BorderStyle").a("color", "#000000").a("type", "line").a("width", "1.0");
		e.e("y:NodeLabel").a("alignment", "center").a("autoSizePolicy", "content").a("fontFamily", "Dialog")
				.a("fontSize", "13").a("fontStyle", "bold").a("hasBackgroundColor", Constants.false_str)
				.a("hasLineColor", Constants.false_str)
				.a("height", "20" + 20 * util.countLines(properties.get("desc").toString())).a("modelName", "internal")
				.a("modelPosition", "t").a("textColor", "#000000").a("visible", Constants.true_str).a("y", "100")
				.a("width", "180").a("x", "0").text((String) properties.get("text"));
		final XMLBuilder u = e.e("y:UML").a("clipContent", Constants.true_str).a("constraint", "")
				.a("omitDetails", Constants.false_str).a("stereotype", "").a("use3DEffect", Constants.true_str);
		u.e("y:AttributeLabel").text((String) properties.get("desc"));
		u.e("y:MethodLabel");
		return root;
	}

	public void flush() throws Exception {
		final Properties properties = new Properties();
		properties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
		properties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
		properties.put("{http://xml.apache.org/xslt}indent-amount", "2");
		properties.put(javax.xml.transform.OutputKeys.ENCODING, this.encoding);

		String s = this.root.toString();

		this.root.toWriter(this.writer, properties);
	}

}
