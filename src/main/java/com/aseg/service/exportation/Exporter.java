package com.aseg.service.exportation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.aseg.config.Constants;
import com.aseg.exceptions.ConfigurationException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.tests.ConfigSisAux;
import com.aseg.util.service.ParseXml;
import com.aseg.vo.GenericVO;

public abstract class Exporter {

	private final GenericService service;
	private final Usuario user;
	private final ParseXml parseXml;
	private final Set<String> idList;

	public Exporter(final GenericService service, final Usuario user) {
		this.service = service;
		this.user = user;
		this.parseXml = this.user.getSysParser();
		this.idList = new HashSet<String>();
	}

	public void export(final String beanName, final GenericVO vo) throws Exception {

		export(null, null, null, null, beanName, vo);

	}

	private void export(final String parentBean, final String parentId, final String propertyName,
			final String relationkey, final String beanName, final GenericVO vo) throws Exception {

		LogUtil.debug("parentBean = " + parentBean + ", parentId = " + parentId + ", propertyName = " + propertyName
				+ ", beanName = " + beanName, user);

		final Map<String, Object> voMap = new TreeMap<String, Object>();
		final List<Map<String, String>> services = new ArrayList<Map<String, String>>();
		Object ke = vo.get(vo.getKey());
		if (ke == null)
			return;
		final String id = ke.toString();
		final boolean exported = exported(beanName, vo);

		if (exported) {
			filterVo(beanName, vo, voMap, relationkey);

		} else {
			filterVo(beanName, vo, voMap, services, id);
		}

		if (parentBean != null && parentId != null) {
			generateSubVO(parentBean, parentId, propertyName, relationkey, beanName, id, voMap, exported);
		} else {
			generateVO(beanName, id, voMap, exported);
		}

		if (!exported) {
			exportServices(beanName, vo, services);
			exportDetails(beanName, vo);
		}
	}

	private void exportDetail(final String parentBeanName, final GenericService parentService, final GenericVO parentVo,
			final Map<String, String> subBean) throws ConfigurationException, Exception {

		final String primaryKey = getPrimaryKey(parentService);
		final String parentId = parentVo.get(primaryKey).toString();
		final String beanName = subBean.get(Constants.name);
		final GenericService service = getService(beanName + "Service");
		final GenericVO search = service.createVO(this.user);

		search.put(primaryKey, parentId);
		search.setPaginate(false);
		search.setLazyList(false);
		search.setExport(true);
		final List<GenericVO> beans = service.lista(search, this.user);

		String relationkey;

		for (final GenericVO vo : beans) {
			relationkey = getRelationKey(service);

			if (relationkey == null) {
				relationkey = primaryKey;
			}
			export(parentBeanName, parentId, null, relationkey, beanName, vo);
		}
	}

	private void exportDetails(final String beanName, final GenericVO vo) throws ConfigurationException, Exception {

		final GenericService service = getService(beanName + "Service");
		final String beanId = service.getProperties().get("id").toString();

		final Collection<Map<String, String>> beans = parseXml.getListValuesFromXQL("/systems/system[@name='"
				+ this.user.getSchema() + "']/module/bean[@type='D' and @master='" + beanId + "']", "", this.user, true,
				true);
		// Collections.reverse((List) beans);
		for (final Map<String, String> subBean : beans) {
			exportDetail(beanName, service, vo, subBean);
		}
	}

	private boolean exported(final String beanName, final GenericVO vo) {
		try {
			final GenericService service = getService(beanName + "Service");
			final String primaryKey = getPrimaryKey(service);
			final String id = vo.get(primaryKey).toString();
			return this.idList.contains(beanName + id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private void exportService(final String parentBean, final GenericVO parentVO, final Map<String, String> property)
			throws Exception {

		final String serviceName = property.get("service");
		final String propertyName = property.get(Constants.name);
		final String key = property.get("key") == null ? propertyName : property.get("key").toString();
		final GenericService service = getService(serviceName);

		final String parentId = parentVO.get(parentVO.getKey()).toString();

		if (property.get("type").equals("array") && !service.getProperties().get("type").equals("A")
				&& parentVO.get(propertyName) instanceof String[]) {

			for (final String id : (String[]) parentVO.get(propertyName)) {
				exportService(parentBean, serviceName, propertyName, key, service, id, parentId);
			}

		} else {
			if (propertyIsServiceToExport(property)) {
				final String id = parentVO.get(propertyName).toString();
				exportService(parentBean, serviceName, propertyName, key, service, id, parentId);
			}
		}
	}

	public boolean propertyIsServiceToExport(Map<String, String> property) {
		if (property.get("service") == null || property.get("service").isEmpty()) {
			return false;
		} else if (property.get("serviceMethod") != null && !property.get("serviceMethod").isEmpty()) {
			ConfigSisAux configSisAux = ConfigSisAux.getInstance(user);
			List<Map<String, String>> methods = configSisAux.getBeanMethods(
					configSisAux.getBeanByName(property.get("service").replace(Constants.SERVICE_BASE, "")), true,
					null);
			for (Map<String, String> m : methods) {
				if (m.get("name").equals(property.get("serviceMethod")) && m.get("type").equals("L")) {
					return false;
				}
			}
		}
		return true;
	}

	private void exportService(final String parentBean, final String serviceName, final String propertyName,
			final String key, final GenericService service, final String value, final String parentId)
			throws Exception {

		final String beanName = serviceName.replaceAll(Constants.SERVICE_BASE, "");
		GenericVO vo = null;

		final GenericVO search = service.createVO(this.user);

		// if (util.match((List)
		// service.getProperties().get(Constants.ALL_FIELDS),Constants.name,key).size()==0
		// ) { // pode ser simplesmente um metodo configurado no service entao
		// mudando search.containsKey(key) para o valor do if atual
		// throw new BusinessException("Chave primria " + key
		// + " no existe no bean do service " + serviceName
		// + " configurado");
		// }
		search.setLazyList(true);
		search.putKey(key, value);
		vo = service.obtem(search, user);

		if (vo != null && vo.get(key) != null) {
			vo.setKey(key);
			export(parentBean, parentId, propertyName, null, beanName, vo);
		}
	}

	private void exportServices(final String parentBean, final GenericVO vo, final List<Map<String, String>> properties)
			throws Exception {

		for (final Map<String, String> property : properties) {
			exportService(parentBean, vo, property);
		}
	}

	private void filterVo(final String beanName, final GenericVO vo, final Map<String, Object> voMap,
			final List<Map<String, String>> services, final String id) {

		this.idList.add(beanName + id);
		final Collection<Map<String, String>> properties = getProperties(beanName);

		String name, serviceName;

		for (final Map<String, String> property : properties) {

			if (!isIgnoredProperty(property)) {
				name = property.get(Constants.name);

				if (vo.get(name) != null) {
					serviceName = property.get("service");

					// TODO: Verificar se existe um "method" padro para
					// exportar
					if (serviceName != null && !isIgnoredService(property)) {
						services.add(property);
					}
					voMap.put(name, vo.get(name));
				}
			}
		}
	}

	private void filterVo(final String beanName, final GenericVO vo, final Map<String, Object> voMap,
			final String relationkey) {

		final GenericService service = getService(beanName + "Service");
		final String primaryKey = getPrimaryKey(service);
		final String mainField = service.getMainField("lista", this.user).get(Constants.name).toString();

		voMap.put(primaryKey, vo.get(primaryKey));
		voMap.put(mainField, vo.get(mainField));

		if (relationkey != null) {
			voMap.put(relationkey, vo.get(relationkey));
		}
	}

	public abstract void flush() throws Exception;

	protected abstract void generateSubVO(String parentBean, String parentId, String propertyName, String masterField,
			String beanName, String beanId, Map<String, Object> vo, boolean exported);

	protected abstract void generateVO(String beanName, String beanId, Map<String, Object> vo, boolean exported);

	private String getPrimaryKey(final GenericService service) {
		return service.getPrimaryKey().get(Constants.name).toString();
	}

	private Collection<Map<String, String>> getProperties(final String beanName) {
		return (Collection<Map<String, String>>) getService(beanName + "Service").getProperties()
				.get(Constants.ALL_FIELDS);
	}

	private String getRelationKey(final GenericService service) {
		final Object relationKey = service.getProperties().get(Constants.RELATION_KEY);
		return relationKey == null ? null : relationKey.toString();
	}

	private GenericService getService(final String serviceName) {
		return (GenericService) this.service.getService(serviceName, this.user);
	}

	private boolean isIgnoredProperty(final Map<String, String> property) {

		final String name = property.get(Constants.name);

		if (name.equals("ID_USUARIO_CADASTRO") || name.equals("ULTIMA_MODIFICACAO") || name.equals("DATA_CADASTRO")) {
			return true;
		}

		final String type = property.get("type");

		if (type.equals("sqlrule")) {
			return true;
		}

		final String propVolatile = property.get("volatile");

		if (propVolatile != null && propVolatile.equals(Constants.true_str) && !type.equals("array")) {
			return true;
		}
		return false;
	}

	private boolean isIgnoredService(final Map<String, String> property) {
		final String serviceName = property.get("service");
		return serviceName.indexOf(".") != -1 || serviceName.equals("UtilService") || property.get("method") != null;
	}
}
