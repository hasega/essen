package com.aseg.service.exportation;

import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.util.XMLUtil;
import com.jamesmurty.utils.XMLBuilder;

public class XMLExporter extends Exporter {

	private static final char DELIMITER = '_';
	private XMLBuilder root;
	private final Map<String, XMLBuilder> elements;
	private final Writer writer;
	private final String encoding;

	public XMLExporter(final GenericService service, final Usuario user, final Writer writer, final String encoding)
			throws Exception {
		super(service, user);
		this.writer = writer;
		this.elements = new HashMap<String, XMLBuilder>();
		this.encoding = encoding;

		try {
			this.root = XMLBuilder.create("export");
		} catch (final Exception e) {
			throw e;
		}
	}

	@Override
	public void flush() throws Exception {
		final Properties properties = new Properties();
		properties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
		properties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
		properties.put("{http://xml.apache.org/xslt}indent-amount", "2");
		properties.put(javax.xml.transform.OutputKeys.ENCODING, this.encoding);
		this.root.toWriter(this.writer, properties);
	}

	private XMLBuilder generate(final XMLBuilder parent, final String beanName, final String beanId,
			final Map<String, Object> vo, final boolean exported) {

		final XMLBuilder xmlBuilder = parent.e(beanName);

		if (!exported) {
			this.elements.put(beanName + beanId, xmlBuilder);
		}

		if (exported) {
			xmlBuilder.a("recursive", Constants.true_str);
		}

		try {
			XMLUtil.vo2Xml(vo, xmlBuilder, DELIMITER, false);
		} catch (final Exception e) {
			LogUtil.exception(e, null);
		}
		return xmlBuilder;
	}

	@Override
	protected void generateSubVO(final String parentBean, final String parentId, final String propertyName,
			final String masterField, final String beanName, final String beanId, final Map<String, Object> vo,
			final boolean exported) {

		final XMLBuilder xmlBuilder = this.elements.get(parentBean + parentId);
		final XMLBuilder subNode = generate(xmlBuilder, beanName, beanId, vo, exported);

		if (propertyName != null) {
			subNode.a("ref", propertyName);
		}

		if (masterField != null) {
			subNode.a("master", masterField);
		}
	}

	@Override
	protected void generateVO(final String beanName, final String beanId, final Map<String, Object> vo,
			final boolean exported) {
		generate(this.root, beanName, beanId, vo, exported);
	}
}
