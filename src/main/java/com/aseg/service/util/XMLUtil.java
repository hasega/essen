package com.aseg.service.util;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.aseg.vo.GenericVO;
import com.jamesmurty.utils.XMLBuilder;
import com.thoughtworks.xstream.core.util.Base64Encoder;

public final class XMLUtil {

	public static final char DELIMITER_DEFAULT = '-';

	private static final String TYPE = "type";
	private static final String TYPE_BYTES = "binary";

	private static void attribute2Tag(final XMLBuilder xmlBuilder, final String tagName, final Object value) {

		if (value instanceof String[]) {
			XMLBuilder multipleIdNode;
			String[] multipleId;
			multipleId = (String[]) value;

			if (multipleId.length != 0) {

				multipleIdNode = xmlBuilder.e(tagName);

				for (final String element : multipleId) {
					multipleIdNode.e("id").t(element);
				}
			}

		} else {

			if (value instanceof byte[]) {
				xmlBuilder.e(tagName).d((byte[]) value).a(TYPE, TYPE_BYTES);

			} else if (value != null && !value.toString().equals("")) {
				xmlBuilder.e(tagName).t(value.toString());
			}
		}
	}

	private static String[] loadIds(final Node node) {
		final NodeList nodes = node.getChildNodes();
		Node nodeChild;
		final List<String> list = new ArrayList<String>();

		for (int i = 0; i < nodes.getLength(); i++) {
			nodeChild = nodes.item(i);

			if (nodeChild.getNodeType() == Node.ELEMENT_NODE) {
				list.add(nodeChild.getTextContent());
			}
		}
		return list.toArray(new String[list.size()]);
	}

	private static void noteToAttribute(final GenericVO vo, final Node node, final String nodeName) {

		final NamedNodeMap attrs = node.getAttributes();
		final Node attrType = attrs.getNamedItem(TYPE);

		if (attrType != null && attrType.getTextContent().equals(TYPE_BYTES)) {
			final byte[] data = new Base64Encoder().decode(node.getTextContent());
			vo.put(nodeName, new ByteArrayInputStream(data));
		} else {
			String[] ids;
			ids = loadIds(node);
			// Verifica se foi passado um array de ids
			if (ids.length > 0) {
				vo.put(nodeName, ids);
			} else {
				vo.put(nodeName, node.getTextContent());
			}
		}
	}

	private static final void processMultipleId(final GenericVO vo, final Node multipleNode) {
		vo.setMultipleId(loadIds(multipleNode));
	}

	/**
	 * Converte a string para um nome de tag padronizado, tudo minsculo e
	 * separado por "-".<br>
	 * Exemplo: "NomeDaTag" ou "NOME_DA_TAG" para "nome-da-tag".
	 * 
	 * @param name
	 * @return Nome para a tag padronizado
	 */
	public final static String toTagName(final String name) {
		return toTagName(name, DELIMITER_DEFAULT);
	}

	public final static String toTagName(final String name, final char delimiter) {
		return name.replaceAll("[^a-zA-Z]", String.valueOf(delimiter))
				.replaceAll("([a-z])([A-Z])", "$1" + delimiter + "$2").toLowerCase();
	}

	public final static String toVoName(final String name) {
		final String[] part = name.split("[^a-zA-Z]");
		String beanName = "";

		for (final String partName : part) {
			beanName += partName.substring(0, 1).toUpperCase() + partName.substring(1);
		}
		return beanName;
	}

	public final static String toVoPropertyName(final String name) {
		return name.replaceAll("[^a-zA-Z]", "_").toUpperCase();
	}

	public final static XMLBuilder vo2Xml(final Map<String, Object> vo, final String beanName)
			throws ParserConfigurationException, FactoryConfigurationError {

		return vo2Xml(vo, beanName, DELIMITER_DEFAULT);
	}

	public final static XMLBuilder vo2Xml(final Map<String, Object> vo, final String beanName, final char delimiter)
			throws ParserConfigurationException, FactoryConfigurationError {

		final XMLBuilder xmlBuilder = XMLBuilder.create(toTagName(beanName, delimiter));
		vo2Xml(vo, xmlBuilder);
		return xmlBuilder.root();

	}

	public final static void vo2Xml(final Map<String, Object> vo, final XMLBuilder xmlBuilder) {

		vo2Xml(vo, xmlBuilder, DELIMITER_DEFAULT, true);
	}

	public final static void vo2Xml(final Map<String, Object> vo, final XMLBuilder xmlBuilder, final char delimiter,
			final boolean formatted) {

		String tagName;

		for (final Map.Entry<String, Object> entry : vo.entrySet()) {
			tagName = formatted ? toTagName(entry.getKey(), delimiter) : entry.getKey();
			attribute2Tag(xmlBuilder, tagName, entry.getValue());
		}
	}

	public final static void xml2Vo(final Node nodeBean, final GenericVO vo, final boolean formatted) {

		final NodeList nodes = nodeBean.getChildNodes();
		Node node;
		String attrName, searchBy = null, searchKey = null;

		for (int i = 0; i < nodes.getLength(); i++) {
			node = nodes.item(i);

			if (formatted) {
				attrName = toVoPropertyName(node.getNodeName());
			} else {
				attrName = node.getNodeName();
			}

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				if (attrName.equals("MULTIPLEID")) {
					processMultipleId(vo, node);

				} else if (attrName.equals("BY")) {
					searchBy = node.getTextContent();

				} else if (attrName.equals("KEY")) {
					searchKey = node.getTextContent();

				} else if (vo.containsKey(attrName)) {
					noteToAttribute(vo, node, attrName);
				}
			}
		}

		if (searchBy != null && searchKey != null) {
			vo.putKey(searchBy, searchKey);
		}
	}
}
