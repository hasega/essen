package com.aseg.service.importation;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.service.util.XMLUtil;
import com.aseg.vo.GenericVO;

public class XMLImporter extends Importer {

	private long start;
	private long count;

	public XMLImporter(final ServiceImpl service, final Usuario user) {
		super(service, user);
	}

	private GenericVO createVO(final Node node, GenericVO bindVO) {
		final String beanName = getBeanName(node);
		final GenericVO vo = createVO(beanName);

		XMLUtil.xml2Vo(node, vo, false);
		cleanReferences(beanName, vo);
		vo.processKeyBinder(bindVO);
		return vo;
	}

	private String getAttribute(final Node node, final String attribute) {
		final Node master = node.getAttributes().getNamedItem(attribute);
		return master == null ? null : master.getTextContent();

	}

	private String getBeanName(final Node node) {
		return node.getNodeName();
	}

	private String getMasterField(final Node node) {
		return getAttribute(node, "master");
	}

	private String getReferenceId(final Node node) {
		return getAttribute(node, "ref");
	}

	private boolean isRequiredBean(final Node node) {
		return node.getAttributes().getNamedItem("ref") != null;
	}

	@Override
	public void process(final InputStream is, GenericVO genericVO) throws Exception { // A
																						// vo
																						// esta
																						// sendo
																						// passada
																						// agora
																						// para
																						// fazer
																						// o
																						// bind
																						// do
																						// contexto
																						// master
																						// nos
																						// arquivos
																						// de
																						// import
		LogUtil.debug("Iniciando importao...", user);

		start = System.currentTimeMillis();

		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		final Document document = factory.newDocumentBuilder().parse(is);
		final Element export = document.getDocumentElement();
		processBeans(export.getChildNodes(), genericVO);

		LogUtil.debug("Importao concluda em " + (System.currentTimeMillis() - start) + " ms", user);

		is.close();
	}

	private boolean processAfter(final Node node) {
		return node.getAttributes().getNamedItem("master") != null;
	}

	private String processBean(final Node node, final GenericVO vo, GenericVO genericVO) throws Exception {

		final String beanName = getBeanName(node);
		final NodeList nodeList = node.getChildNodes();

		final List<Node> detailNodes = new ArrayList<Node>();
		Node child;

		// LogUtil.debug("Processando " + beanName + " " + vo.getKey() + " = "
		// + vo.get(vo.getKey()));

		final int total = nodeList.getLength();

		for (int i = 0; i < total; i++) {
			child = nodeList.item(i);

			if (child.getNodeType() == Node.ELEMENT_NODE) {

				if (isRequiredBean(child)) {
					processRequiredBean(beanName, vo, child, genericVO);

				} else if (processAfter(child)) {
					// Bean do tipo "detail" devem ser inseridos depois do bean
					// "master"
					detailNodes.add(child);
				}
			}
		}
		final String beanId = insert(beanName, vo, getMasterField(node), genericVO);

		if (detailNodes.size() > 0) {
			GenericVO subVO;

			// Cria os beans do tipo "D" (detail) depois da sua "M" (master)
			for (final Node detailNode : detailNodes) {
				subVO = createVO(detailNode, genericVO);
				injectMasterId(getBeanName(detailNode), subVO, beanId);
				processBean(detailNode, subVO, genericVO);
			}
		}
		return beanId;
	}

	private void processBeans(final NodeList nodeList, GenericVO genericVO) {

		Node node;
		final int total = nodeList.getLength();
		final List<Integer> erros = new ArrayList<Integer>();
		int logCount = 0;

		for (int i = 0; i < total; i++) {

			node = nodeList.item(i);

			if (node.getNodeType() == Node.ELEMENT_NODE) {

				try {
					processBean(node, createVO(node, genericVO), genericVO);

				} catch (final Exception e) {
					erros.add(i + 1);
					LogUtil.debug("Falha ao importar registro " + (i + 1), user);
					LogUtil.exception(e, user);
				}
				logCount++;
				count++;

				if (logCount == 10) {
					logCount = 0;
					LogUtil.debug(count + " em " + (System.currentTimeMillis() - start) + " ms", user);
				}

			}
		}
		// erros.isEmpty();
	}

	private void processRequiredBean(final String beanName, final GenericVO vo, final Node child, GenericVO bindVO)
			throws Exception {

		final String id = processBean(child, createVO(child, bindVO), bindVO);

		// Injeta o id do bean no bean dependente
		putId(vo, getReferenceId(child), id);
	}
}
