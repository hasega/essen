package com.aseg.service.importation;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.aseg.config.Constants;
import com.aseg.exceptions.BusinessException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;

public abstract class Importer extends ServiceImpl {

	protected final Map<String, Map<Object, String>> cacheId;
	private final ServiceImpl service;
	protected Usuario user;

	public Importer(final ServiceImpl service, final Usuario user) {
		this.service = service;
		this.user = user;
		this.cacheId = new HashMap<String, Map<Object, String>>();
	}

	private final UtilServiceImpl util = new UtilServiceImpl();

	protected void cleanReferences(final String beanName, final GenericVO vo) {

		final GenericService service = getService(beanName);
		final List<Map<String, String>> properties = util
				.match((List) service.getProperties().get(Constants.ALL_FIELDS), "type", "array");

		for (final Map<String, String> property : properties) {
			vo.put(property.get(Constants.name), new String[] {});
		}
	}

	protected GenericVO createVO(final String beanName) {
		GenericVO mvo = getService(beanName).createVO(this.user);
		mvo.setImport(true);
		List m = getService(beanName).getMethods(this.user);
		for (Iterator iterator = m.iterator(); iterator.hasNext();) {
			Map object = (Map) iterator.next();
			if (object.get(Constants.ALL_FIELDS) != null) {
				List f = (List) object.get(Constants.ALL_FIELDS);
				for (Iterator iterator2 = f.iterator(); iterator2.hasNext();) {
					Map fil = (Map) iterator2.next();
					if (!fil.containsKey("volatile"))
						mvo.put((String) fil.get(Constants.name), null);

				}
			}

		}

		return mvo;
	}

	private String findOldId(final ServiceImpl service, final String mainField, final Object mainFieldValue,
			final GenericVO mVO, final GenericVO bindVO) throws Exception {

		Map<Object, String> cache = this.cacheId.get(service.getName());

		if (cache == null) {
			cache = new HashMap<Object, String>();
			this.cacheId.put(service.getName(), cache);

		} else if (cache.get(mainFieldValue) != null) {
			return cache.get(mainFieldValue);
		}

		final GenericVO search = service.createVO(this.user);
		if (service.getProperties().get("type").equals("D")) {
			String k = (String) service.getParent(user).getPrimaryKey().get(Constants.name);
			search.put(k, mVO.get(k));
		}
		search.ignoreCase();
		search.searchBy(mainField);
		search.put(mainField, mainFieldValue);
		search.setLazyList(true);
		search.setIgnoreCache(true);
		search.setPaginate(false);
		search.setCommitSingle(true);
		search.setSegmentBy(false);
		if (bindVO != null) {
			search.processKeyBinder(bindVO);
		}

		final GenericVO vo = service.obtem(search, this.user);
		String id = null;

		if (vo.get(vo.getKey()) != null) {
			id = vo.get(vo.getKey()).toString();
			cache.put(mainFieldValue, id);
		}
		return id;
	}

	private String findOldId(final ServiceImpl service, final String mainField, final String mainFieldValue,
			final String key, final String value, final GenericVO mVO, final GenericVO bindVO) throws Exception {

		Map<Object, String> cache = this.cacheId.get(service.getName());
		final String cacheId = mainFieldValue + "_" + value;

		if (cache == null) {
			cache = new HashMap<Object, String>();
			this.cacheId.put(service.getName(), cache);

		} else if (cache.get(cacheId) != null) {
			return cache.get(cacheId);
		}

		final String filtro = "{gA:[{c:{f:'" + mainField + "',o:'=',v1:'" + mainFieldValue + "'}},{c:{f:'" + key
				+ "',o:'=',vc:'" + value + "'}}]}";

		final GenericVO search = service.createVO(this.user);
		if (service.getProperties().get("type").equals("D")) {
			String k = (String) service.getParent(user).getPrimaryKey().get(Constants.name);
			search.put(k, mVO.get(k));
		}
		search.put(mainField, mainFieldValue);
		search.put(key, value);
		search.setFiltro(filtro);
		search.setLazyList(true);
		search.setIgnoreCache(true);
		search.setPaginate(false);
		search.setCommitSingle(true);
		search.setSegmentBy(false);

		if (bindVO != null) {
			search.processKeyBinder(bindVO);

		}

		final List voList = service.lista(search, this.user);

		// if (voList.size() > 1) {
		// throw new BusinessException(
		// "Esperado apenas um resultado para busca: " + mainField
		// + " = " + mainFieldValue + ", " + key + " = "
		// + value);
		// }

		if (voList.size() >= 1) {
			final GenericVO vo = (GenericVO) voList.get(0);
			final Object id = vo.get(vo.getKey());

			if (id != null) {
				cache.put(cacheId, id.toString());
				return id.toString();
			}
		}
		return null;
	}

	private ServiceImpl getService(final String beanName) {
		return (ServiceImpl) this.service.getService(beanName + Constants.SERVICE_BASE, this.user);
	}

	protected void injectMasterId(final String beanName, final GenericVO subVO, final String beanId) {
		final ServiceImpl service = getService(beanName);
		final String relationKey = service.getProperties().get(Constants.RELATION_KEY).toString();
		subVO.put(relationKey, beanId);
	}

	protected String insert(final String beanName, final GenericVO vo, final String masterField, final GenericVO bindVO)
			throws Exception {

		final ServiceImpl service = getService(beanName);
		Map mf = service.getMainField("lista", this.user);
		final String mainField = (String) mf.get(Constants.name);

		if (vo.get(mainField) == null && !mf.containsKey("calculated")) {
			throw new BusinessException("Identificador nico (" + mainField + ") no pode ser nulo.");
		}

		final String mainFieldValue = (String) vo.get(mainField);

		vo.setImport(true);
		vo.format(user);

		LogUtil.debug("IMPORTANDO " + beanName + " >> " + mainFieldValue, user);
		String id;

		if (masterField != null) {

			final String value = vo.get(masterField).toString();
			id = findOldId(service, mainField, mainFieldValue, masterField, value, vo, bindVO);

		} else {
			id = findOldId(service, mainField, mainFieldValue, vo, bindVO);
		}

		if (id != null) {
			LogUtil.debug("J existe " + beanName + "[" + id + "] " + mainField + " = " + vo.get(mainField), user);
			// TODO: Implementar atualiao (merge)
			return id;
		} else {
			LogUtil.debug("Inserindo " + beanName + " " + mainField + " " + vo.get(mainField), user);
			// Removendo id que veio junto com a importao
			id = vo.get(vo.getKey()).toString();
			vo.put(vo.getKey(), null);
			vo.setLazyList(true);
			vo.setIgnoreCache(true);
			vo.setCommitSingle(true);
			vo.setMethod("inclui");
			String idn = service.inclui(vo, this.user);

			Map<Object, String> cache = this.cacheId.get(service.getName());
			final String cacheId = mainFieldValue + "_" + id;
			cache.put(cacheId, idn.toString());

			return idn;
		}
	}

	public abstract void process(InputStream is, GenericVO genericVO) throws Exception;

	protected void putId(final GenericVO vo, final String key, final String id) {

		if (vo.get(key) != null && vo.get(key) instanceof String[]) {
			final String[] ids = (String[]) vo.get(key);
			final String[] newIds = Arrays.copyOf(ids, ids.length + 1);
			newIds[ids.length] = id;

			vo.put(key, newIds);
		} else {
			vo.put(key, id);
		}
	}

}
