package com.aseg.service.setup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aseg.config.Constants;
import com.aseg.dao.DAOUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.util.service.ParseXmlService;

public class SemanticValidation {

	public enum ERROR_TYPE {
		PROPERTY_TYPE, PROPERTY_COMPONENT, PROPERTY_MAXLENGTH, NUMBER_MAXLENGTH, COMPONENT_CHECKBOX, PRIMARYKEY_TOTAL, PRIMARYKEY_NAME, RESERVED_NAME
	}

	private List<Map<String, String>> errors;
	private Collection<Map<String, String>> beans;

	private Collection<String> tables;

	private void add(final Map<String, String> property, final ERROR_TYPE errorType) {
		final Map<String, String> error = add(property.get("parent_id"), errorType);
		error.put("PROPERTY", property.get(Constants.name));
	}

	private Map<String, String> add(final String beanId, final ERROR_TYPE errorType) {

		final Map<String, String> error = new HashMap<String, String>();
		error.put("BEAN", beanId);
		error.put("ERROR", errorType.name());

		this.errors.add(error);

		return error;
	}

	private Collection<Map<String, String>> getBeans(final ParseXmlService parse, final Usuario user) {

		if (this.beans == null) {
			final String system = user.getSchema();
			this.beans = parse.getListValuesFromXQL(
					"/systems/system[@name='" + system + "']/module/bean[@table and not(@extends)]", "table", user,
					false, false);
		}
		return this.beans;
	}

	private List<Map<String, String>> getProperties(final ParseXmlService parse, final String beanParam,
			final String propertyParam, final Usuario user) {

		final String beanWhere = beanParam == null ? "" : "[" + beanParam + "]";
		final String propertyWhere = "[not(@volatile)" + (propertyParam == null ? "" : " and " + propertyParam) + "]";

		final String system = user.getSchema();

		final String xpath = "/systems/system[@name='" + system + "']/module/bean[not(@extends)]" + beanWhere;

		return (List<Map<String, String>>) parse.getListValuesFromXQL(
				new String[] { xpath + "/property" + propertyWhere, xpath + "/method/property" + propertyWhere },
				Constants.name, user, false, false);
	}

	private Collection<String> getTables(final ParseXmlService parse, final Usuario user) {

		final String system = user.getSchema();

		if (this.tables == null) {
			this.tables = parse.getListFromXQL("/systems/system[@name = '" + system + "']/module/bean/@table", user);
		}
		return this.tables;
	}

	public List<Map<String, String>> validate(final ParseXmlService parse, final Usuario user) {

		this.errors = new ArrayList<Map<String, String>>();

		validateTable(parse, user);
		validatePrimaryKey(parse, user);
		validateNumberMaxlength(parse, user);
		validateComponentCheckbox(parse, user);
		validateReservedNames(parse, user);

		return this.errors;
	}

	private void validateComponent(final Map<String, String> entry, final Map<String, String> property) {

		if (!property.get(Constants.component).equals(entry.get(Constants.component))) {
			add(property, ERROR_TYPE.PROPERTY_COMPONENT);
		}
	}

	private void validateComponentCheckbox(final ParseXmlService parse, final Usuario user) {
		for (final Map<String, String> property : getProperties(parse, null,
				"@component = 'checkbox' and (@type != 'bit' and @type != 'sqlrule' and @type != 'scriptRule')",
				user)) {
			add(property, ERROR_TYPE.COMPONENT_CHECKBOX);
		}
	}

	private void validateMaxlength(final Map<String, String> entry, final Map<String, String> property) {
		String value;
		value = property.get("maxlength");

		if (value == null) {

			if (entry.get("maxlength") != null) {
				add(property, ERROR_TYPE.PROPERTY_MAXLENGTH);
			}

		} else if (!value.equals(entry.get("maxlength"))) {
			add(property, ERROR_TYPE.PROPERTY_MAXLENGTH);
		}
	}

	private void validateNumberMaxlength(final ParseXmlService parse, final Usuario user) {
		for (final Map<String, String> property : getProperties(parse, null, "@type = 'num' and @maxlength > 20",
				user)) {
			add(property, ERROR_TYPE.NUMBER_MAXLENGTH);
		}
	}

	private void validatePrimaryKey(final ParseXmlService parse, final Usuario user) {
		List<Map<String, String>> properties;
		String idName;

		for (final Map<String, String> bean : getBeans(parse, user)) {

			properties = getProperties(parse, "@id = '" + bean.get("id") + "'", "@primaryKey = 'true'", user);

			if (properties.size() == 1) {

				if (bean.get("table") != null) {
					idName = DAOUtil.getPrimaryKeyName(bean.get("table"), parse, user);

					if (!idName.equals(properties.get(0).get(Constants.name))) {
						add(properties.get(0), ERROR_TYPE.PRIMARYKEY_NAME);
					}
				}

			} else {
				add(bean.get("id"), ERROR_TYPE.PRIMARYKEY_TOTAL);
			}
		}

	}

	private void validateReservedNames(final ParseXmlService parse, final Usuario user) {

		final String reserved = "@name ='START' or @name='LIMIT' or @name='TOTAL' or @name='SORT' or @name='DIR' or @name='MULTIPLEID' or @name='METHOD' or @name='PAGINATE' or @name='FILTRO' ";

		for (final Map<String, String> property : getProperties(parse, null, reserved, user)) {

			add(property, ERROR_TYPE.RESERVED_NAME);
		}
	}

	private void validateTable(final ParseXmlService parse, final Usuario user) {

		for (final String tableName : getTables(parse, user)) {
			validateTable(tableName, getProperties(parse, "@table= '" + tableName + "'", null, user));
		}
	}

	private void validateTable(final String tableName, final List<Map<String, String>> properties) {
		final Map<String, Map<String, String>> store = new HashMap<String, Map<String, String>>();
		Map<String, String> entry;
		String propertyName;

		for (final Map<String, String> property : properties) {
			propertyName = property.get(Constants.name);
			entry = store.get(propertyName);

			if (entry == null) {
				entry = new HashMap<String, String>();
				entry.put("type", property.get("type"));
				entry.put(Constants.component, property.get(Constants.component));
				entry.put("maxlength", property.get("maxlength"));

				store.put(propertyName, entry);

			} else {
				validateType(entry, property);
				validateComponent(entry, property);
				validateMaxlength(entry, property);
			}
		}
	}

	private void validateType(final Map<String, String> entry, final Map<String, String> property) {

		if (!property.get("type").equals(entry.get("type"))) {
			add(property, ERROR_TYPE.PROPERTY_TYPE);
		}

	}
}
