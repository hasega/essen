package com.aseg.service.protocol;

import com.aseg.exceptions.GenericRuntimeException;

public class ServiceException extends GenericRuntimeException {

	public ServiceException() {
		super();
	}

	public ServiceException(Throwable throwable) {
		super(throwable);
	}

	public ServiceException(String message) {
		super(message);
	}

}
