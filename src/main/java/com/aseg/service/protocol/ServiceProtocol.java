package com.aseg.service.protocol;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.aseg.vo.GenericVO;

public interface ServiceProtocol {

	public enum Method {
		START_SESSION, END_SESSION, INCLUDE, UPDATE, GET, REMOVE, LIST
	}

	public void processRequest(HttpServletRequest request);

	public String generateResponse(GenericVO vo);

	public String generateResponse(String response);

	public String generateResponse(List<Map<String, Object>> list);

	public String generateFault(String message);

	public Method getMethod();

	public boolean hasVO();

	public void populateVO(GenericVO vo);

	public String getLogin();

	public String getPassword();

	public String getSessionId();

	public boolean isPasswordHash();

	public void setServiceMethod(String serviceMethod);

	public String getServiceMethod();
}
