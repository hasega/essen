package com.aseg.service.protocol;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.util.XMLUtil;
import com.aseg.vo.GenericVO;
import com.jamesmurty.utils.XMLBuilder;

public class SoapProtocol implements ServiceProtocol {

	private static final String XMLNS_SOAP_URL = "http://schemas.xmlsoap.org/soap/envelope/";
	private static final String XMLNS_PROJURIS_URL = "http://webservices.essen.aseg.com.br";

	private static final String XMLNS = "xmlns:";
	private static final String XMLNS_SOAP = "soapenv";
	private static final String XMLNS_PROJURIS = "impl";

	private final String beanName;
	private final String encoding;
	private final List<Map<String, Object>> methods;

	private Method method;
	private Node methodNode;

	private String serviceMethod;

	public SoapProtocol(final String beanName, final String encoding, final List<Map<String, Object>> methods) {
		this.beanName = XMLUtil.toTagName(beanName);
		this.encoding = encoding;
		this.methods = methods;
	}

	private XMLBuilder createBody() throws ParserConfigurationException, FactoryConfigurationError {
		return XMLBuilder.create(XMLNS_SOAP + ":Envelope").a(XMLNS + XMLNS_SOAP, XMLNS_SOAP_URL)
				.a(XMLNS + XMLNS_PROJURIS, XMLNS_PROJURIS_URL).e(XMLNS_SOAP + ":Body");
	}

	private XMLBuilder createResponse(final String method)
			throws ParserConfigurationException, FactoryConfigurationError {

		final String response = this.serviceMethod != null ? this.serviceMethod : method;
		return createBody().e(XMLNS_PROJURIS + ":" + response + "Response");
	}

	private XMLBuilder createResult(final String method, final String tag, final String result)
			throws ParserConfigurationException, FactoryConfigurationError {
		return createResponse(method).e(tag).t(result);
	}

	private Node findNode(final NodeList nodes, final String name) {
		Node node;

		for (int i = 0; i < nodes.getLength(); i++) {
			node = nodes.item(i);

			if (node.getNodeType() == Node.ELEMENT_NODE && node.getLocalName().equals(name)) {
				return node;
			}
		}
		return null;
	}

	private Node findNodeElement(final NodeList nodes) {
		Node node;

		for (int i = 0; i < nodes.getLength(); i++) {
			node = nodes.item(i);

			if (node.getNodeType() == Node.ELEMENT_NODE) {
				return node;
			}
		}
		return null;
	}

	private Node findNodeEndsWith(final NodeList nodes, final String text) {
		Node node;

		for (int i = 0; i < nodes.getLength(); i++) {
			node = nodes.item(i);

			if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().endsWith(text)) {
				return node;
			}
		}
		return null;
	}

	@Override
	public String generateFault(final String message) {
		try {
			final XMLBuilder xmlBuilder = createBody().e(XMLNS_SOAP + ":Fault");
			xmlBuilder.e("faultcode").t(XMLNS_SOAP + ":Server");
			xmlBuilder.e("faultstring").t(message);
			return generateResponse(xmlBuilder);
		} catch (final Exception e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public String generateResponse(final GenericVO vo) {

		try {
			final XMLBuilder xmlBuilder = createResponse("Obtem");
			XMLUtil.vo2Xml(vo, xmlBuilder.e(this.beanName));
			return generateResponse(xmlBuilder);

		} catch (final Exception e) {
			new ServiceException(e);
		}
		return null;
	}

	@Override
	public String generateResponse(final List<Map<String, Object>> list) {

		try {
			final XMLBuilder xmlBuilder = createResponse("Lista");
			final String beanName = this.serviceMethod != null ? this.serviceMethod : this.beanName;

			for (final Map<String, Object> bean : list) {
				XMLUtil.vo2Xml(bean, xmlBuilder.e(beanName));
			}
			return generateResponse(xmlBuilder);
		} catch (final Exception e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public String generateResponse(final String result) {
		XMLBuilder xmlBuilder;
		try {
			switch (this.method) {
			case START_SESSION:
				xmlBuilder = createResult("IniciarSessao", "session", result);
				break;
			case END_SESSION:
				xmlBuilder = createResult("EncerrarSessao", "result", result);
				break;
			case INCLUDE:
				xmlBuilder = createResult("Incluir", "id", result);
				break;
			case UPDATE:
				xmlBuilder = createResult("Alterar", "id", result);
				break;
			case REMOVE:
				xmlBuilder = createResult("Remover", "result", result);
				break;
			default:
				throw new ServiceException("No foi possvel gerar resposta");
			}
			return generateResponse(xmlBuilder);
		} catch (final Exception e) {
			throw new ServiceException(e);
		}
	}

	private String generateResponse(final XMLBuilder xmlBuilder) throws TransformerException {
		final Properties outputProperties = new Properties();
		outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
		outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
		outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
		outputProperties.put(javax.xml.transform.OutputKeys.ENCODING, this.encoding);

		return xmlBuilder.asString(outputProperties);
	}

	private Map<String, Object> getBeanMethod(final String methodName) {

		for (final Map<String, Object> method : this.methods) {

			if (methodName.startsWith(method.get(Constants.name).toString())) {
				return method;
			}
		}
		return null;
	}

	private String getChildContent(final NodeList nodes, final String name) {
		final Node node = findNode(nodes, name);

		if (node != null) {
			return node.getTextContent();
		}
		return null;
	}

	@Override
	public String getLogin() {
		return getChildContent(this.methodNode.getChildNodes(), "usuario");
	}

	@Override
	public Method getMethod() {
		return this.method;
	}

	@Override
	public String getPassword() {
		return getChildContent(this.methodNode.getChildNodes(), "senha");
	}

	@Override
	public String getServiceMethod() {
		return serviceMethod;
	}

	@Override
	public String getSessionId() {
		return getChildContent(this.methodNode.getParentNode().getChildNodes(), "session");
	}

	@Override
	public boolean hasVO() {
		return this.method != Method.START_SESSION && this.method != Method.END_SESSION;
	}

	@Override
	public boolean isPasswordHash() {
		return false;
	}

	@Override
	public void populateVO(final GenericVO vo) {

		if (hasVO()) {
			final Node nodeBean = findNodeElement(this.methodNode.getChildNodes());

			if (nodeBean == null) {
				throw new ServiceException("No existe bean nesse pacote.");
			}
			XMLUtil.xml2Vo(nodeBean, vo, true);

		} else {
			throw new ServiceException("No existe bean para esse tipo de pacote (" + this.method + ")");
		}
	}

	private void processBody(final Node bodyNode) {
		final NodeList nodes = bodyNode.getChildNodes();

		if (nodes.getLength() == 0) {
			throw new ServiceException("Envelope vazio.");
		}

		this.methodNode = findNodeEndsWith(nodes, "Request");
		setMethod(methodNode.getLocalName());
	}

	@Override
	public void processRequest(final HttpServletRequest request) {
		try {
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);

			final Document document = factory.newDocumentBuilder().parse(request.getInputStream());

			final NodeList nodes = document.getElementsByTagNameNS(XMLNS_SOAP_URL, "Body");

			if (nodes.getLength() > 0) {
				processBody(nodes.item(0));

			} else {
				throw new ServiceException("Tag 'body' no foi encontrado.");
			}

		} catch (final Exception e) {
			LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
			throw new ServiceException(e);
		}
	}

	private void setMethod(final String method) {

		if (method.equals("IniciarSessaoRequest")) {
			this.method = Method.START_SESSION;
		} else if (method.equals("EncerrarSessaoRequest")) {
			this.method = Method.END_SESSION;
		} else if (method.equals("IncluirRequest")) {
			this.method = Method.INCLUDE;
		} else if (method.equals("AlterarRequest")) {
			this.method = Method.UPDATE;
		} else if (method.equals("ObtemRequest")) {
			this.method = Method.GET;
		} else if (method.equals("RemoverRequest")) {
			this.method = Method.REMOVE;
		} else if (method.equals("ListaRequest")) {
			this.method = Method.LIST;
		} else {
			setMethod(method, getBeanMethod(method));
		}
	}

	private void setMethod(final String method, final Map<String, Object> beanMethod) {
		final String type = beanMethod.get("type").toString();

		if (type.equals("C")) {
			this.method = Method.INCLUDE;

		} else if (type.equals("R")) {
			this.method = Method.GET;

		} else if (type.equals("U")) {
			this.method = Method.UPDATE;

		} else if (type.equals("D")) {
			this.method = Method.REMOVE;

		} else if (type.equals("L")) {
			this.method = Method.LIST;
		}
		setServiceMethod(beanMethod.get(Constants.name).toString());
	}

	@Override
	public void setServiceMethod(final String serviceMethod) {
		this.serviceMethod = serviceMethod;
	}
}
