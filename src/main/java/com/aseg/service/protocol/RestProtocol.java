package com.aseg.service.protocol;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.aseg.config.Constants;
import com.aseg.service.util.XMLUtil;
import com.aseg.vo.GenericVO;
import com.jamesmurty.utils.XMLBuilder;

public class RestProtocol implements ServiceProtocol {

	private static final String AUTH_TOKEN = "Token";
	private static final String AUTH_LOGOUT = "Logout";
	private static final String AUTH_BASIC = "Basic";
	private static final String REQUEST_METHOD_DELETE = "DELETE";
	private static final String REQUEST_METHOD_POST = "POST";
	private static final String REQUEST_METHOD_PUT = "PUT";
	private static final String REQUEST_METHOD_GET = "GET";
	private static final String TAG_ERROR_MESSAGE = "error-message";
	private static final String TAG_ERROR = "error";
	private static final String TAG_LOGOUT = "logout";
	private static final String TAG_SESSION_ID = "session-id";
	private static final String HEADER_AUTHORIZATION = "authorization";
	private static final String PROPERTY_MULTIPLEID = "MULTIPLEID";

	private final String beanName;
	private final String encoding;

	private HttpServletRequest request;
	private Method method;

	private String login;
	private String password;
	private String sessionId;

	private String primaryKey;
	private String id;

	private boolean returnList;
	private String serviceMethod;

	public RestProtocol(final String beanName, final String encoding) {
		this.beanName = beanName;
		this.encoding = encoding;
	}

	private XMLBuilder createResult(final String property, final String result)
			throws ParserConfigurationException, FactoryConfigurationError {

		return createXMLBean().e(property).t(result);
	}

	private XMLBuilder createXMLBean() throws ParserConfigurationException, FactoryConfigurationError {

		return XMLBuilder.create(getResultBeanName());
	}

	@Override
	public String generateFault(final String message) {

		try {
			final XMLBuilder xmlBuilder = createXMLBean();

			if (this.method == Method.LIST) {
				xmlBuilder.a("type", "array");
			}

			xmlBuilder.e(TAG_ERROR).t(Constants.true_str);
			xmlBuilder.e(TAG_ERROR_MESSAGE).t(message);
			return generateResponse(xmlBuilder);
		} catch (final Exception e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public String generateResponse(final GenericVO vo) {
		try {
			final XMLBuilder xmlBuilder = createXMLBean();
			XMLUtil.vo2Xml(vo, xmlBuilder);
			return generateResponse(xmlBuilder);

		} catch (final Exception e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public String generateResponse(final List<Map<String, Object>> list) {

		try {
			final XMLBuilder xmlBuilder = createXMLBean().a("type", "array");
			final String tagName = getResultBeanName();

			for (final Map<String, Object> bean : list) {
				XMLUtil.vo2Xml(bean, xmlBuilder.e(tagName));
			}
			return generateResponse(xmlBuilder);

		} catch (final Exception e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public String generateResponse(final String result) {
		XMLBuilder xmlBuilder;
		try {
			switch (this.method) {
			case START_SESSION:
				xmlBuilder = createResult(TAG_SESSION_ID, result);
				break;
			case END_SESSION:
				xmlBuilder = createResult(TAG_LOGOUT, result);
				break;
			// TODO: Implementar outros mtodos
			// case INCLUDE:
			// xmlBuilder = createResult("id", result);
			// break;
			// case UPDATE:
			// xmlBuilder = createResult("id", result);
			// break;
			// case REMOVE:
			// xmlBuilder = createResult("Result", result);
			// break;
			default:
				throw new ServiceException("label.service.erro");
			}
			return generateResponse(xmlBuilder);
		} catch (final Exception e) {
			throw new ServiceException(e);
		}
	}

	private String generateResponse(final XMLBuilder xmlBuilder) throws TransformerException {
		final Properties outputProperties = new Properties();
		outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
		outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
		outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
		outputProperties.put(javax.xml.transform.OutputKeys.ENCODING, this.encoding);
		return xmlBuilder.asString(outputProperties);
	}

	@Override
	public String getLogin() {
		return this.login;
	}

	@Override
	public Method getMethod() {
		return this.method;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	private String getResultBeanName() {

		if (serviceMethod == null) {
			return XMLUtil.toTagName(this.beanName);
		} else {
			return XMLUtil.toTagName(this.beanName + "_" + serviceMethod);
		}
	}

	@Override
	public String getServiceMethod() {
		return serviceMethod;
	}

	@Override
	public String getSessionId() {
		return this.sessionId;
	}

	@Override
	public boolean hasVO() {
		return false;
	}

	/**
	 * TODO: Trocar para true quando tiver suporte a MD5. (Cliente e servidor
	 * gerar o mesmo hash)
	 */

	@Override
	public boolean isPasswordHash() {
		return false;
	}

	@Override
	public void populateVO(final GenericVO vo) {
		@SuppressWarnings("unchecked")
		final Enumeration<String> params = this.request.getParameterNames();
		String param, name, searchBy = null, searchKey = null;

		while (params.hasMoreElements()) {
			param = params.nextElement();
			name = param.toUpperCase().replace('-', '_');

			if (vo.containsKey(name)) {

				if (param.endsWith(PROPERTY_MULTIPLEID)) {
					vo.put(name, this.request.getParameterValues(param));

				} else {
					vo.put(name, this.request.getParameter(param));
				}
			} else if (name.equals("BY")) {
				searchBy = this.request.getParameter(param);
			} else if (name.equals("KEY")) {
				searchKey = this.request.getParameter(param);
			}
		}

		if (searchBy != null && searchKey != null) {
			vo.putKey(searchBy, searchKey);

		} else if (this.primaryKey != null && this.id != null) {
			vo.putKey(this.primaryKey, this.id);
		}
	}

	@Override
	public void processRequest(final HttpServletRequest request) {
		this.request = request;

		try {
			final String[] auth = this.request.getHeader(HEADER_AUTHORIZATION).split("[: ]");

			if (auth[0].equals(AUTH_BASIC)) {
				this.method = Method.START_SESSION;
				this.login = auth[1];
				this.password = auth[2];

			} else if (auth[0].equals(AUTH_LOGOUT)) {
				this.method = Method.END_SESSION;
				this.sessionId = auth[1];

			} else if (auth[0].equals(AUTH_TOKEN)) {
				this.sessionId = auth[1];
				final String method = this.request.getMethod();

				if (method.equals(REQUEST_METHOD_GET)) {

					if (this.returnList) {
						this.method = Method.LIST;
					} else {
						this.method = Method.GET;
					}

				} else if (method.equals(REQUEST_METHOD_PUT)) {
					this.method = Method.INCLUDE;

				} else if (method.equals(REQUEST_METHOD_POST)) {
					this.method = Method.UPDATE;

				} else if (method.equals(REQUEST_METHOD_DELETE)) {
					this.method = Method.REMOVE;
				}
			}
		} catch (final Exception e) {
			throw new ServiceException("label.login.necessario");
		}
	}

	public void setId(final String primaryKey, final String id) {
		this.primaryKey = primaryKey;
		this.id = id;
	}

	public void setReturnList(final boolean returnList) {
		this.returnList = returnList;
	}

	@Override
	public void setServiceMethod(final String serviceMethod) {
		this.serviceMethod = serviceMethod;
	}
}
