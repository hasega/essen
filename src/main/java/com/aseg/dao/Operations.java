package com.aseg.dao;

import java.util.HashMap;
import java.util.Map;

public enum Operations {
	LIKE("sem"), EQUAL("="), NOT_LIKE("!="), NOT_EQUAL("<>"), IN("in"), NOT_IN("notin"), MAX("max"), NULL(
			"nl"), NOT_NULL("nnl"), CONTAINS("%"), NOT_CONTAINS("!%"), BETWEEN("-"), GREATER_THAN(">"), LESS_THAN(
					"<"), TRUE("T"), FALSE("F"), MALE("MAS"), FEMALE("FEM"), IN_ORGANIZATIONAL_UNIT(
							"%U"), IN_SYSTEM_MODULE("%M"), TODAY("hoje"), YESTERDAY("ontem"), TOMORROW(
									"amanha"), LAST_WEEK("sp"), THIS_WEEK("sa"), NEXT_WEEK("ps"), LAST_MONTH(
											"mp"), THIS_MONTH("ma"), NEXT_MONTH("pm"), LAST_YEAR(
													"aan"), THIS_YEAR("aat"), NEXT_YEAR("pa"), LAST_30_DAYS(
															"u30"), LAST_60_DAYS("u60"), LAST_180_DAYS(
																	"u180"), LAST_360_DAYS("u360"), NEXT_30_DAYS(
																			"p30"), NEXT_60_DAYS("p60"), NEXT_180_DAYS(
																					"p180"), NEXT_360_DAYS(
																							"p360"), FUTURE("nf"), PAST(
																									"np"), YES("S"), NO(
																											"N"), BEGIN_WITH(
																													"%?"), END_WITH(
																															"?%");

	private final String operator;

	private static Map<String, Operations> lookup = new HashMap<String, Operations>();

	static {
		for (Operations localOperator : Operations.values()) {
			lookup.put(localOperator.getOperator(), localOperator);
		}
	}

	private Operations(String operator) {
		this.operator = operator;
	}

	public static Operations getByOperator(String operator) {
		return lookup.get(operator);
	}

	public String getOperator() {
		return operator;
	}

}
