package com.aseg.dao;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptException;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.util.StringUtils;

import com.aseg.config.Constants;
import com.aseg.exceptions.BusinessException;
import com.aseg.exceptions.ConfigurationException;
import com.aseg.exceptions.PersistenceException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.Paginator;
import com.aseg.service.ServiceImpl;
import com.aseg.util.ConnectionManager;
import com.aseg.util.DAOTemplate;
import com.aseg.util.DataFormat;
import com.aseg.util.service.ParseXml;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.sql.Dialeto;
import com.aseg.util.sql.InterfaceDialeto;
import com.aseg.vo.GenericVO;

import net.sf.ehcache.Cache;

public class DAOImpl implements GenericDAO {

	boolean logSQL = true;

	private List defaultDAOFields = Arrays
			.asList(new String[] { "DATA_CADASTRO", "ID_USUARIO_CADASTRO", "ULTIMA_MODIFICACAO" });

	private static final String[][] getFilters = { { "volatile", "type", "type", "component" },
			{ Constants.true_str, "array", "map", "fake" } };
	private static List<Map<String, String>> insertControlCollection = new ArrayList<Map<String, String>>();
	private static List<Map<String, String>> updateControlCollection = new ArrayList<Map<String, String>>();
	private static final String[][] updateFilters = {
			{ "volatile", "relationKey", "parent_node", "type", "xtype", Constants.name, Constants.name, "component" },
			{ Constants.true_str, Constants.true_str, "method", "sqlrule", "container", "DATA_CADASTRO",
					"ULTIMA_MODIFICACAO", "fake" } };

	private static final String[][] insertFilters = {
			{ "volatile", "parent_node", "type", "xtype", Constants.name, Constants.name, "component" },
			{ Constants.true_str, "method", "sqlrule", "container", "DATA_CADASTRO", "ULTIMA_MODIFICACAO", "fake" } };

	private static final String[][] updateMethodFilters = {
			{ "volatile", "relationKey", "type", "xtype", Constants.name, Constants.name, "component" },
			{ Constants.true_str, Constants.true_str, "sqlrule", "container", "DATA_CADASTRO", "ULTIMA_MODIFICACAO",
					"fake" } };

	private static final String[][] insertMethodFilters = {
			{ "volatile", "type", "xtype", Constants.name, Constants.name, "component" },
			{ Constants.true_str, "sqlrule", "container", "DATA_CADASTRO", "ULTIMA_MODIFICACAO", "fake" } };

	private static final String FIELD_REPLACE = ":$#";

	public Object getBean(final String name, final Usuario user) {

		return user.getFactory().getSpringBean(name, user);
	}

	boolean cached = false;
	private int check_code;
	Map<String, String> entity = null;
	DAOImpl parent = null;
	String primaryKey = null;
	String mainField = null;
	String paginateBy = null;
	Map<String, String> pkMap = null;
	private List<Map<String, String>> properties;
	private int cacheSize = 0;

	public void afterPropertiesSet(final Usuario user) throws Exception {

		if (getEntity() != null && properties == null) {
			final ParseXml parseXml = user.getSysParser();

			properties = Collections.unmodifiableList((List) parseXml.getListValuesFromXQL(
					new String[] {
							"/systems/system[@name='" + user.getSchema() + "']/module/bean[@name='"
									+ getEntity().get(Constants.name) + "']/property",
							"/systems/system[@name='" + user.getSchema() + "']/module/bean[@name='"
									+ getEntity().get(Constants.name) + "']/container/property",
							"/systems/system[@name='" + user.getSchema() + "']/module/bean[@name='"
									+ getEntity().get(Constants.name) + "']/method[@type!='L']/property",
							"/systems/system[@name='" + user.getSchema() + "']/module/bean[@name='"
									+ getEntity().get(Constants.name) + "']/method[@type!='L']/container/property" },
					"order", user, true, true));
		}

		if (getEntity() != null && properties != null && getEntity().get("cached") != null
				&& getEntity().get("cached").equals(Constants.true_str)) {
			this.cached = true;
			final GenericVO vo = new GenericVO();
			vo.setPaginate(false);
			final String[] keys = getEntity().get("cacheKey").toString().split(",");
			int i = 0;

			List<Map> l = Listagem(new Object[] {}, vo, user);
			for (final Map item : l) {
				String cacheKey = "";
				for (i = 0; i < keys.length; i++) {
					cacheKey = cacheKey + item.get(keys[i]) + Constants.CACHE_SEPARATOR;
				}
				cacheKey = cacheKey.substring(0, cacheKey.length() - 1);
				final net.sf.ehcache.Element element = new net.sf.ehcache.Element(
						user.getSystem() + Constants.CACHE_SEPARATOR + cacheKey, item);
				((Cache) getBean("dataCache", user)).put(element);
				// LogUtil.debug("cache " + element.getKey());
			}
			if (logSQL)
				LogUtil.debug("cache size  " + getEntity().get(Constants.name) + l.size(), user);
			cacheSize = l.size();
			try {
				primaryKey = user.getUtil().extractArrayFromList(user.getUtil().filter(properties, "primaryKey", null),
						Constants.name, true)[0];
			} catch (final Exception e) {

			}

		}

		if (insertControlCollection.size() == 0) {
			final HashMap<String, String> dc = new HashMap<String, String>();
			dc.put(Constants.name, "ULTIMA_MODIFICACAO");
			dc.put("type", "date");
			updateControlCollection.add((Map) dc.clone());
			insertControlCollection.add((Map) dc.clone());

			dc.put(Constants.name, "DATA_CADASTRO");
			insertControlCollection.add((Map) dc.clone());

			dc.put(Constants.name, "ID_USUARIO_CADASTRO");
			dc.put("type", "num");
			dc.put("service", Constants.subject_service);
			dc.put("key", Constants.subject_key);
			dc.put("labelValue", "NOME");
			insertControlCollection.add((Map) dc.clone());

		}
		runTagsData(user);
		runTagsDbProcedure(user);
	}

	public void runTagsDbProcedure(final Usuario user) {
		try {
			DAOTemplate tem = null;

			List<Map<String, String>> dbProcedures = (List) user.getSysParser()
					.getListValuesFromXQL("/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='"
							+ getEntity().get("id") + "']/dbProcedure", Constants.name, user, true, true);

			if (!dbProcedures.isEmpty()) {
				tem = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, null,
						user.getUtil());
			} else {
				return;
			}

			for (Map<String, String> p : dbProcedures) {
				if (p.get("for").equals(ConnectionManager.getInstance(user).getStringBancoConectado(user))
						&& p.get("CDATA") != null && !p.get("CDATA").isEmpty()
						&& !procedureExistsInDB(p.get("name"), user)) {
					tem.update(p.get("CDATA"));
				}
			}

		} catch (ConfigurationException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean procedureExistsInDB(String name, Usuario user) {
		try {
			ResultSet rs = ConnectionManager.getInstance(user).getConexao(user).getMetaData().getProcedures(null, "",
					"%");
			while (rs.next()) {
				if (rs.getString("PROCEDURE_NAME").equals(name)) {
					return true;
				}
			}
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		return false;
	}

	public void runTagsData(final Usuario user) {
		try {

			List<Map<String, String>> dataConfig = (List) user.getSysParser()
					.getListValuesFromXQL("/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='"
							+ getEntity().get("id") + "']/data", Constants.name, user, true, true);

			if (!dataConfig.isEmpty()) {

				GenericService pServ = (GenericService) getServiceById(getEntity().get("id"), user);
				GenericVO pVo = pServ.createVO(user);
				pVo.setPaginate(false);

				for (Map<String, String> d : dataConfig) {
					if (d.get("CDATA") != null) {
						if (d.get("filtro") != null && !d.get("filtro").isEmpty()) {
							pVo.setFiltro(d.get("filtro"));
						}
						List<GenericVO> result = pServ.lista(pVo, user);
						Map<String, Object> cdataMap = user.getUtil().JSONtoMap(d.get("CDATA"));
						try {
							boolean haveItem = false;
							if (d.get("filtro") != null && !d.get("filtro").isEmpty() && !result.isEmpty()) {
								haveItem = true;
							} else {
								for (GenericVO res : result) {
									if (res.get(getMainField(user)).equals(cdataMap.get(getMainField(user)))) {
										haveItem = true;
										break;
									}
								}
							}
							if (!haveItem) {
								for (java.util.Map.Entry<String, Object> field : cdataMap.entrySet()) {

									String value = field.getValue() != null ? field.getValue().toString() : "";

									if (value.contains("${")) {

										value = value.subSequence(value.indexOf("${") + 2, value.lastIndexOf("}"))
												.toString();

										Bindings bindings = user.getUtil().getJSEngine().createBindings();
										bindings.put("user", user);
										bindings.put("sm", pServ);
										bindings.put(Constants.VOKey, pVo);
										bindings.put("util", user.getUtil());
										bindings.put("df", new DataFormat());
									//	bindings.put("cmp", new TagInputExt());

										value = "" + user.getUtil().getJSEngine().eval(value, bindings);
									}

									pVo.put(field.getKey(), value);
								}
								pVo.setIgnoreCache(true);
								pServ.inclui(pVo, Usuario.getSystemUser());
							}
						} catch (Exception e) {
							LogUtil.exception(e, user);
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

	}

	@Override
	public Long altera(final GenericVO vo, final Usuario user) throws Exception {
		List cam = null;

		if (vo.getMethod().equals("") || vo.getMethod().equalsIgnoreCase("altera")) {
			cam = user.getUtil().filter(properties, updateFilters[0], updateFilters[1]);
			cam = user.getUtil().match(cam, "parent_id", getEntity().get("id").toString());
		} else {

			cam = user.getUtil().match(properties, "parent_id", vo.getMethod());

			List ctx = user.getUtil().match(cam, "component", "context");

			for (Iterator iterator = ctx.iterator(); iterator.hasNext();) {
				Map object = (Map) iterator.next();

				cam.addAll((Collection) ((GenericService) getService(object.get("service").toString(), user))
						.getProperties().get(Constants.ALL_FIELDS));

			}

			cam = user.getUtil().filter(cam, updateMethodFilters[0], updateMethodFilters[1]);

		}

		final List fpk = user.getUtil().filter(properties, "primaryKey", null);
		String pk = vo.getKey();

		final List data = user.getUtil().filter(cam, new String[] { "name", "type" }, new String[] { pk, "array" });

		final List datas = user.getUtil().filter(cam, new String[] { "type" }, new String[] { "array" });

		final DAOTemplate tem = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo,
				user.getUtil());

		Long r = null;
		try {
			for (Iterator iterator = datas.iterator(); iterator.hasNext();) {
				Map object = (Map) iterator.next();
				if (object.get("foneticSearch") != null
						&& object.get("foneticSearch").toString().equalsIgnoreCase("true")
						&& !object.get("name").toString().endsWith("_FONETIC")) {
					Map f = new HashMap(object);
					f.put("name", object.get("name") + "_FONETIC");
					data.add(f);
					vo.put((String) f.get("name"), user.getUtil().getSoundex(user).TextSound(

							(String) vo.get(object.get("name"))));

				}
			}
			r = altera(getEntity().get("table").toString(), data, pk, user, vo, true);
			final List a = user.getUtil().match(data, new String[] { "type", "unique" },
					new String[] { "bit", Constants.true_str });
			if (a.size() > 0) {

				for (final Iterator iterator = a.iterator(); iterator.hasNext();) {
					final Map object = (Map) iterator.next();
					List aux = new ArrayList();
					aux.add(object);

					if (object.get("filtro") != null) {
						final StandardEvaluationContext ctx = new StandardEvaluationContext();
						ctx.setVariable(Constants.VOKey, vo);
						vo.setFiltro(user.getUtil().EvaluateInString((String) object.get("filtro"), ctx));
					}
					if (vo.get(object.get(Constants.name)) != null && vo.get(object.get(Constants.name)).equals("T")) {
						vo.put(object.get(Constants.name).toString(), "F");
						if (getEntity().get("master") != null && getEntity().get("type").equals("D")) {
							pk += "!," + ((DAOImpl) getDAOById(getEntity().get("master"), user)).getPrimaryKey(user);
						} else {
							pk += "!,";
						}
						aux = user.getUtil().filter(aux, updateFilters[0], updateFilters[1]);
						altera(getEntity().get("table").toString(), aux, pk, user, vo, false);
					}
				}
			}
			pk = getPrimaryKey(user);

			processArrayField(vo, user, pk);

		} catch (final Exception e) {
			LogUtil.exception(e, user);
			throw new PersistenceException(e);
		}
		if (cached && !vo.ignoreCache()) {
			afterPropertiesSet(user);
		}
		if (getEntity().get("refresh") != null)
			((DAOImpl) getDAOById(getEntity().get("refresh"), user)).afterPropertiesSet(user);
		return r;

	}

	private Long altera(final String table, final List<Map<String, String>> campos, final String columId,
			final Usuario user, final GenericVO vo, boolean autocontrol) throws Exception {

		final ArrayList params = new ArrayList();
		final ArrayList storages = new ArrayList();

		final SqlUpdate su = new SqlUpdate();
		final DAOTemplate tem = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo,
				user.getUtil());
		su.setDataSource(tem.getDataSource());

		String uploadDir = null;
		try {
			uploadDir = user.getSystemProperty("LOCALIZACAO_SERVIDOR");
		} catch (Exception e1) {
			LogUtil.exception(e1, user);
		}

		// Adiciona e declara os valores dos campos de cadastro
		if (autocontrol) {
			campos.addAll(updateControlCollection);
		}
		vo.put("ULTIMA_MODIFICACAO", new DataFormat().nowTimestamp());
		StringBuffer sql = new StringBuffer("update ");
		sql.append(table + " set ");

		int j = 0;
		for (final Map<String, String> object : campos) {

			if (!ParseXmlService.canAccess(object, user, true) && !isToIgnorePermission(vo, object, user)) {
				continue;
			}

			if (!vo.isAnulate() && vo.get(object.get(Constants.name)) == null)
				continue;
			if (object.get("storage") != null && uploadDir != null) {
				storages.add(object);
				continue;
			}

			if (object.get("type") != null && object.get("type").equalsIgnoreCase("bit")) {
				if (vo.get(object.get("name")) == null)
					vo.put(object.get("name"), "F");
			}

			int type = getType(object, user);
			if (type == Types.BLOB || type == Types.JAVA_OBJECT || type == Types.CLOB) {

				InputStream is = null;
				long length = 0;
				if (vo.get(object.get(Constants.name)) != null) {
					if (type == Types.JAVA_OBJECT) {
						type = Types.BLOB;
						String path = dumpToFile(vo.get(object.get(Constants.name).toString()),
								user.getSessionId() + "-" + System.currentTimeMillis());
						is = new FileInputStream(path);
					} else if (type == Types.CLOB) {
						is = new ByteArrayInputStream(
								vo.get(object.get(Constants.name).toString()).toString().getBytes());
						length = is.available();
						if (length == 0) {
							continue;
						}

					} else
						try {
							is = (ByteArrayInputStream) vo.get(object.get(Constants.name).toString());
							length = is.available();
							if (length == 0) {
								continue;
							}
						} catch (final Exception e) {
							is = (FileInputStream) vo.get(object.get(Constants.name).toString());

							length = ((FileInputStream) vo.get(object.get(Constants.name).toString())).available();
							if (length == 0) {
								continue;
							}
						}
				}
				try {
					/*
					 * if (ConnectionManager.getBancoConectado() ==
					 * ConnectionManager.ORACLE && false) { OracleLobHandler
					 * lobHandler = new OracleLobHandler(); parameterValues[1] =
					 * new SqlLobValue(is, length, lobHandler); } else {
					 */
					final DefaultLobHandler lobHandler = new DefaultLobHandler();
					params.add(new SqlLobValue(is, new Long(length).intValue(), lobHandler));
					// }
				} catch (final Exception e) {
					LogUtil.exception(e, user);
					continue;

				}
			} else {
				if (object.get("calculated") != null && vo.get(object.get(Constants.name)) == null) {
					continue;
				}
				if (object.get(Constants.name).toString().equalsIgnoreCase("ULTIMA_MODIFICACAO")) {
					vo.put(object.get(Constants.name).toString(), new DataFormat().nowTimestamp());
				}

				params.add(vo.getConverted(object.get(Constants.name),user));
			}

			sql.append(object.get(Constants.name).toString() + " = ? ,");

			su.declareParameter(new SqlParameter(object.get(Constants.name).toString(), type));
			j++;
		}
		if (columId.indexOf(",") > -1) {
			sql.deleteCharAt(sql.lastIndexOf(","));
			if (!vo.isIgnoreIdCondition()) {
				sql.append(" where ");
				final String[] c = columId.split(",");
				for (String element : c) {
					String sig = " = ";
					if (element.indexOf("!") > -1) {
						sig = " <> ";
						element = user.getUtil().replace(element, "!", "");
					}
					params.add(vo.getConverted(element.trim(),user));
					List<Map<String, String>> fieldId = user.getUtil().match(properties, "name", element);
					if (!fieldId.isEmpty()) {
						su.declareParameter(new SqlParameter(element, getType(fieldId.get(0), user)));
					}
					sql.append(element + sig + " ? * ");
				}
				sql.deleteCharAt(sql.lastIndexOf("*"));
				sql = new StringBuffer(user.getUtil().replace(sql.toString(), "*", " AND "));
			}
			final String f = getSqlWhere(null, vo.get(Constants.FILTER_KEY), user, vo, null, false);
			if (f != null && f.trim().length() > 0) {
				if (!vo.isIgnoreIdCondition()) {
					sql.append(" AND ");
				} else {
					sql.append(" where ");
				}
				sql.append(f);
			}
		} else {
			sql.deleteCharAt(sql.lastIndexOf(","));
			if (!vo.isIgnoreIdCondition()) {
				List<Map<String, String>> fieldId = user.getUtil().match(properties, "name", columId);
				if (!fieldId.isEmpty()) {
					su.declareParameter(new SqlParameter(columId, getType(fieldId.get(0), user)));
				}
				params.add(vo.getConverted(columId,user));
				sql.append(" where " + columId + " = ?");
			}
			final String f = getSqlWhere(null, vo.get(Constants.FILTER_KEY), user, vo, null, false);
			if (f != null && f.trim().length() > 0) {
				if (!vo.isIgnoreIdCondition()) {
					sql.append(" AND ");
				} else {
					sql.append(" where ");
				}
				sql.append(f);
			}
		}

		if (!vo.getMethod().equalsIgnoreCase("altera") || getEntity().get("mapping") != null) // tratando
																								// mapping
																								// de
		// metodos
		{
			final GenericService s = getServiceById(getEntity().get("id").toString(), user);
			Map m = s.getMethod(vo.getMethod(), user);
			if (m == null) {
				m = getEntity();
			}

			if (m != null && m.get("mapping") != null) {
				final Map js = user.getUtil().JSONtoMap((String) m.get("mapping"));
				final Iterator i = js.keySet().iterator();
				while (i.hasNext()) {
					final String object = (String) i.next();
					sql = new StringBuffer(user.getUtil().replace(sql.toString(), object, (String) js.get(object)));

				}
			}
		}
		if (logSQL)
			LogUtil.debug("Query de alteracao >> " + sql + " >> " + params, user);

		su.setSql(sql.toString());
		Locale l = Locale.getDefault();
		vo.setUpdatedRows(su.update(params.toArray()));
		if (uploadDir != null) {
			for (Iterator iterator = storages.iterator(); iterator.hasNext();) {
				Map field = (Map) iterator.next();
				String[] st = field.get("storage").toString().split("/");

				if (!uploadDir.endsWith(String.valueOf(java.io.File.separatorChar)))
					uploadDir += String.valueOf(java.io.File.separatorChar);

				uploadDir += getDefaultStorePath(user, (String) vo.get(st[1]).toString(), st[0].toLowerCase());

				if (vo.get("FILE_NAME_" + field.get("name")) == null
						|| vo.get("FILE_NAME_" + field.get("name")).toString().trim().length() == 0)
					continue;

				// FormFile file = (FormFile) vo.get("File1");
				try {
					// boolean criar = (new File(uploadDir)).mkdirs();

					String[] dirList = new File(uploadDir).list();

					if (dirList != null) {
						for (int i = 0; i < dirList.length; i++) {
							String n = String.valueOf(dirList[i]);
							if (n.startsWith("pjw(" + vo.get(getPrimaryKey(user)).toString() + ")")) {

								File file = new File(uploadDir + n);
								file.delete();
							}
						}
					}
					String rad = "";
					if (user.getSystemProperty("LOCALIZACAO_USE_FILE_NAME") == null
							|| user.getSystemProperty("LOCALIZACAO_USE_FILE_NAME").equalsIgnoreCase("true"))
						rad = "-" + vo.get("FILE_NAME_" + field.get("name"));

					user.getUtil().writeToDisk((InputStream) vo.get(field.get("name")),
							"pjw(" + vo.get(getPrimaryKey(user)).toString() + ")" + rad, uploadDir);
				} catch (FileNotFoundException e) {
					LogUtil.exception(e, user);
				} catch (IOException e) {
					LogUtil.exception(e, user);
				}
			}
		}

		try {
			return new Long(vo.get(columId).toString());
		} catch (final Exception e) {
			return null; // so para as dependencias bit, unique por EXEMPLE
		}

	}

	private String getDefaultStorePath(Usuario user, String key, String radical) {
		String u = user.getSystemProperty("SIGLA_CLIENTE");
		if (u == null)
			throw new PersistenceException("VALOR DA VARIAVEL DE SISTEMA SIGLA CLIENTE INDEFINIDO NA INSTANCIA");
		String r = String.valueOf(java.io.File.separatorChar);
		// TODO Auto-generated method stub
		return r + u + r + radical + r + key + r;
	}

	private String build_permission(final Usuario user, final String q, final String sb, String direction,
			GenericVO vo) {
		// MembrosService ms = (MembrosService) getService("MembrosService",
		// user);
		String a = "( ";
		final Map mu = new HashMap();
		if (direction.indexOf(":") > -1) {
			try {
				mu.put(sb.split(".")[1], vo.get(direction.split(":")[1]).toString());
			} catch (Exception e) {
				return "";
			}

		} else {
			mu.put("ID_UNIDADE", String.valueOf(user.getIdUnidadeOrganizacional()));

		}

		Long[] mun = new Long[] {};
		try {
			mun = user.getUnidadesVisiveis();
		} catch (final Exception e) {

			LogUtil.exception(e, user);
		}
		String tmp = "";
		// mun.add(mu);
		int cont = 0;
		for (int i = 0; i < mun.length; i++) {

			tmp = user.getUtil().replace(q, ":#opr#:", " in ");
			if (a.trim().length() > 1 && cont != mun.length) {
				a += ") OR (";
			} else if (cont != mun.length) {
				a += "(  ";
			}
			a += user.getUtil().replace(tmp, "?", mun[i].toString());
			cont++;
		}
		a += " ) ";
		if (direction != null && direction.equalsIgnoreCase("down")) {
			mun = new Long[] {};
			try {
				mun = user.getUnidadesInvisiveis();
			} catch (final Exception e) {

				LogUtil.exception(e, user);
			}
			if (mun.length > 0) {
				a += " ) AND (" + sb + " not in (" + StringUtils.arrayToDelimitedString(mun, ",") + ")";
			}
		}
		return a + ") ";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aseg.dao.RainDAOIface#Listagem(int, int, java.lang.String,
	 * java.lang.Object[], java.lang.Object)
	 */

	@Override
	public String cont(String filtro_master, String filtro, final String join, final Usuario user, final GenericVO vo)
			throws Exception {

		if (filtro_master.toString().length() == 0) {
			filtro_master = filtro;
		}
		filtro_master = user.getUtil().decode(filtro_master, user);
		filtro = user.getUtil().decode(filtro, user);
		// LogUtil.debug(filtro);

		if (filtro_master.toString().equalsIgnoreCase(filtro)) {
			final String contexto = filtro_master.substring(0, 2);
			Map items = getEntity();

			if (items == null) {
				return "0";
			}
			items = (HashMap) items.get(contexto);
			final String[] sb = getSegmentBy(user, null, getEntity().get("table").toString(), vo);
			String sql = sb[0] + "select count(1) from " + items.get("table").toString() + " " + contexto + " WHERE "
					+ sb[1];
			final String f = getSqlWhere(contexto, filtro_master, user, vo, null, false);
			if (f.trim().length() > 0) {
				sql = sql + " AND " + f;
			}

			try {
				DAOTemplate jt = null;
				jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo,
						user.getUtil());
				return String.valueOf(jt.queryForObject(sql, Integer.class));

			} catch (final org.springframework.dao.IncorrectResultSizeDataAccessException se) {
				return "0";
			} catch (final Exception SQLe) {
				LogUtil.exception(SQLe, user);
				return "0";
			}
		} else {
			final String subcontexto = filtro.substring(0, 2);
			Map sub = null; // getHashValuesFromXQL("/systems/system[@name='"+user.getSystem()+"']/module/bean[@id='"
			// + subcontexto + "']", "id", user, true);
			sub = (HashMap) sub.get(subcontexto);

			String contexto = "";
			if (filtro_master.length() > 0) {
				contexto = filtro_master.substring(0, 2);
			} else {
				contexto = sub.get("parent").toString();
			}
			Map items = null; // getHashValuesFromXQL(
			// "/systems/system[@name='"+user.getSystem()+"']/module/bean[@id='"
			// + contexto + "']", "id", user,
			// true);
			if (items == null) {
				return "0";
			}
			items = (HashMap) items.get(contexto);

			if (join.equalsIgnoreCase("T")) {

				String f = getSqlWhere(contexto, filtro_master, user, vo, null, false);
				if (items == null) {
					f = "";
				}

				if (f.trim().length() > 0) {
					f = " AND " + f;
				}
				final String o = getSqlWhere(subcontexto, filtro, user, vo, null, false);
				if (o.trim().length() > 0) {
					f += " AND " + o;
				}
				final String[] sb = getSegmentBy(user, null, getEntity().get("table").toString(), vo);
				final String sql = sb[0] + "select distinct count(1) from " + items.get("table").toString() + " "
						+ contexto + "  inner join " + sub.get("table").toString() + " " + subcontexto + " on "
						+ contexto + "." + items.get("key").toString() + " = " + subcontexto + "."
						+ sub.get("parent_key").toString() + " WHERE " + sb[1] + f;

				try {
					DAOTemplate jt = null;
					jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo,
							user.getUtil());
					return String.valueOf(jt.queryForObject(sql, Integer.class));

				} catch (final org.springframework.dao.IncorrectResultSizeDataAccessException se) {
					return "0";
				} catch (final Exception SQLe) {
					LogUtil.exception(SQLe, user);
					return "0";
				}
			} else {

				String sql = "";
				String f = getSqlWhere(contexto, filtro_master, user, vo, null, false);
				if (items == null) {
					sql = "select distinct count(1) from " + sub.get("table").toString() + " " + subcontexto;
				} else {
					if (f.trim().length() > 0) {
						f = " AND " + f;
					}

					final String o = getSqlWhere(subcontexto, filtro, user, vo, null, false);
					if (o.trim().length() > 0) {
						f += " AND " + o;
					}
					final String sb[] = getSegmentBy(user, null, getEntity().get("table").toString(), vo);
					if (sub.get("table").equals(items.get("table"))) {
						sql = sb[0] + "select distinct count(1) from " + sub.get("table").toString() + " " + subcontexto
								+ " WHERE " + sb[1] + f;
					} else {
						sql = sb[0] + "select distinct count(1) from " + sub.get("table").toString() + " " + subcontexto
								+ " inner join " + items.get("table").toString() + " " + contexto + " on " + contexto
								+ "." + items.get("key").toString() + " = " + subcontexto + "."
								+ sub.get("parent_key").toString() + " WHERE " + sb[1] + f;
					}
				}
				try {
					DAOTemplate jt = null;
					jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo,
							user.getUtil());
					return String.valueOf(jt.queryForObject(sql, Integer.class));

				} catch (final org.springframework.dao.IncorrectResultSizeDataAccessException se) {
					return "0";
				} catch (final Exception SQLe) {
					LogUtil.exception(SQLe, user);
					return "0";
				}
			}

		}
	}

	// se o campo for volatil neste contexto temos que fazer o join na chave
	// e alterar o campo no in clause
	private String deleteLastCharAt(final String string) {

		final StringBuilder sb = new StringBuilder(string.trim());
		return sb.deleteCharAt(sb.toString().length() - 1).toString();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aseg.filtro.service.FiltroService#getSqlWhere(java .lang.String,
	 * java.user.getUtil().HashMap, java.lang.String, java.lang.String)
	 */

	public String dumpToFile(final Object reports, final String id) {
		try {
			final File f = File.createTempFile("serializer_file" + id, ".pjw");
			final ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(f));
			o.writeObject(reports);
			o.flush();
			o.close();
			return f.getAbsolutePath();
		} catch (final IOException ex) {
			LogUtil.exception(ex, null);
		}
		return null;
	}

	public String[] formataValorFromCampo(String Valor, String Campo, final int isLike, String dt, String ctx,
			final Usuario user) {
		// apenas para vale "+Constants.ESCAPE_CHAR+" de se pensar em anexar
		// essa funcionalidade na
		// leitura do xml de filtros
		if (ctx == null || ctx.trim().length() == 0)
			ctx = getEntity().get("id");
		if (dt == null) {
			dt = Campo;
		}

		if (dt.equals("num") || (dt.equals("array") && isLike == 0)) {
			if (isLike != 0) {

				if (isLike == 2) {
					return new String[] { " upper('%" + trataLike(Valor, user)
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "%') escape '" + Constants.ESCAPE_CHAR + "' ", " UPPER " };
				}
				if (isLike == 1) {
					return new String[] { " upper('%" + trataLike(Valor, user)
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "') escape '" + Constants.ESCAPE_CHAR + "'", " UPPER " };
				}
				if (isLike == 3) {
					return new String[] { " upper('" + trataLike(Valor, user)
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "%') escape '" + Constants.ESCAPE_CHAR + "'", " UPPER " };
				} else {
					return new String[] { " upper('" + Valor
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "') ", " UPPER " };

				}
			}
			return new String[] { " " + Valor + " ", "" };
		}

		if (dt.equals("alfa") || dt.equals("sqlrule") || (dt.equals("array") && isLike > 0)) {
			if (isLike == 2) {
				return new String[] {
						" upper('%" + trataLike(Valor, user) + "%') escape '" + Constants.ESCAPE_CHAR + "' ",
						" UPPER " };
			}
			if (isLike == 1) {
				return new String[] {
						" upper('%" + trataLike(Valor, user) + "') escape '" + Constants.ESCAPE_CHAR + "' ",
						" UPPER " };
			}
			if (isLike == 3) {
				return new String[] {
						" upper('" + trataLike(Valor, user) + "%') escape '" + Constants.ESCAPE_CHAR + "'", " UPPER " };
			} else {
				DAOImpl d = (DAOImpl) getDAOById(ctx, user);
				List a = user.getUtil().match(d.whitExtensions(d.properties, user), Constants.name, Campo);
				Map values = (Map) a.get(0);

				if (values.get("type").equals("num") || values.get("component").equals("fake")) {
					if (user.getUtil().isNumber(Valor))
						return new String[] { Valor, " " };
					else
						return new String[] { " upper('" + Valor + "') ", " UPPER " };
				} else {
					return new String[] { " upper('" + Valor + "') ", " UPPER " };
				}
			}
		}

		if (dt.equals("text")) {
			if (isLike == 2) {
				return new String[] {
						" upper('%" + trataLike(Valor, user) + "%') escape '" + Constants.ESCAPE_CHAR + "' ",
						" LIMIT " };
			}
			if (isLike == 1) {
				return new String[] {
						" upper('%" + trataLike(Valor, user) + "') escape '" + Constants.ESCAPE_CHAR + "' ",
						" LIMIT " };
			}
			if (isLike == 3) {
				return new String[] {
						" upper('" + trataLike(Valor, user) + "%') escape '" + Constants.ESCAPE_CHAR + "'", " LIMIT " };
			} else {
				DAOImpl d = (DAOImpl) getDAOById(ctx, user);
				List a = user.getUtil().match(d.whitExtensions(d.properties, user), Constants.name, Campo);
				Map values = (Map) a.get(0);

				if (values.get("type").equals("num") || values.get("component").equals("fake")) {
					if (user.getUtil().isNumber(Valor))

						return new String[] { Valor, " " };
					else
						return new String[] { " upper('" + Valor + "') ", " LIMIT " };
				} else {
					return new String[] { " upper('" + Valor + "') ", " LIMIT " };
				}
			}
		}

		if (dt.equals("time")) {
			if (isLike != 0) {
				if (isLike == 2) {
					return new String[] { " upper('%" + trataLike(Valor, user)
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "%') escape '" + Constants.ESCAPE_CHAR + "' ", " TIME_CHAR " };
				}
				if (isLike == 1) {
					return new String[] { " upper('%" + trataLike(Valor, user)
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "') escape '" + Constants.ESCAPE_CHAR + "'", " TIME_CHAR " };
				}
				if (isLike == 3) {
					return new String[] { " upper('" + trataLike(Valor, user)
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "%') escape '" + Constants.ESCAPE_CHAR + "'", " TIME_CHAR " };
				} else {
					return new String[] { " upper('" + Valor
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "') ", " TIME_CHAR " };

				}
			} else {

				return new String[] {

						Dialeto.getInstance().getDialeto(user).getToDate("HH:mm", Valor), " TRUNC_TIME " };

			}
		}

		if (dt.equals("date")) {
			if (isLike != 0) {
				if (isLike == 2) {
					return new String[] { " upper('%" + trataLike(Valor, user)
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "%') escape '" + Constants.ESCAPE_CHAR + "' ", " DATE_CHAR " };
				}
				if (isLike == 1) {
					return new String[] { " upper('%" + trataLike(Valor, user)
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "') escape '" + Constants.ESCAPE_CHAR + "'", " DATE_CHAR " };
				}
				if (isLike == 3) {
					return new String[] { " upper('" + trataLike(Valor, user)
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "%') escape '" + Constants.ESCAPE_CHAR + "'", " DATE_CHAR " };
				} else {
					return new String[] { " upper('" + Valor
							// + Dialeto.getInstance().getDialeto(user)
							// .getToChar(Valor,
							// java.sql.Types.NUMERIC)
							+ "') ", " DATE_CHAR " };

				}
			} else {

				return new String[] {

						Dialeto.getInstance().getDialeto(user).getToDate("DD/MM/YYYY", Valor), " TRUNC_DATE " };

			}
		}

		if (dt.equals("bit")) {
			return new String[] { "'" + Valor + "'", "" };
		}

		if (dt.equals("money")) {
			return new String[] { "" + user.getUtil().replace(user.getUtil().replace(Valor, ".", ""), ",", ".") + " ",
					"" };
		}

		return new String[] { Valor, "" };
	}

	private String trataLike(String valor, Usuario user) {
		// TODO Auto-generated method stub
		return user.getUtil().replace(valor,
				new String[] {
						// "%", coringa configuravel pelo usuario ai da
						"_" },
				new String[] {
						// ""+Constants.ESCAPE_CHAR+"%",
						"" + Constants.ESCAPE_CHAR + "_" });
	}

	@Override
	public void getArrayField(final GenericVO vo2, final Usuario user) {
		final List arrays = user.getUtil().match(properties, new String[] { "type", "volatile" },
				new String[] { "array", null });
		for (final Iterator iterator = arrays.iterator(); iterator.hasNext();) {
			final Map c = (Map) iterator.next();
			// as os arrays sao sempre de STRING
			if (((ParseXmlService) user.getSysParser()).canAccess(c, user, true)) {
				DAOTemplate jt = null;
				try {
					jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo2,
							user.getUtil());
				} catch (final ConfigurationException e) {
					LogUtil.exception(e, user);
				} catch (final SQLException e) {
					LogUtil.exception(e, user);
				}

				String sort = "";
				if (c.get("columns") != null) {
					Map col = user.getUtil().JSONtoMap((String) c.get("columns"));
					Iterator i = col.keySet().iterator();

					while (i.hasNext()) {
						String object = (String) i.next();
						Map g = (Map) col.get(object);
						if (g.containsKey("sortable")) {
							sort += object + ",";
						}
					}
				}
				if (sort.trim().length() > 0) {
					sort = " ORDER BY " + sort.substring(0, sort.length() - 1);
				}

				Object[] p = new Object[] { vo2.get(getPrimaryKey(user)) };

				if (c.get("cyclic") != null && vo2.get("SEGMENTBY") == null) { // o
					// default e selecionar sempre os pais se ORDER == DOWN ai
					// seleciona os filhos
					// default
					// "+Constants.ESCAPE_CHAR+"
					// selecionar
					// sempre
					// os
					// pais se ORDER == DOWN ai
					// seleciona os filhos

					String q = "select " + getPrimaryKey(user) + " from " + c.get("relational_table") + " where (1=1) "
							+ " AND " + c.get(Constants.name) + " = ? " + sort;

					LogUtil.debug("Query de MM  >> " + q + " >>> " + p, user);

					if (isToGenerateJsonResultInArrayField(c, user)) {
						getArrayFieldInJsonResult(jt, c, user, sort, vo2, p);
					} else {
						vo2.put((String) c.get(Constants.name),
								user.getUtil().extractArrayFromList(jt.queryForList(q, p), getPrimaryKey(user), false));

					}

				} else {
					String k = (String) c.get("key");
					if (k.equals(getPrimaryKey(user))) {
						k = (String) c.get(Constants.name);
					}

					if (isToGenerateJsonResultInArrayField(c, user)) {
						getArrayFieldInJsonResult(jt, c, user, sort, vo2, p);
					} else {
						String q = "select " + k + " from " + c.get("relational_table") + " where (1=1) " + " AND "
								+ getPrimaryKey(user) + " = ? " + sort;

						LogUtil.debug("Query de MM  >> " + q + " >>> " + p, user);
						vo2.put((String) c.get(Constants.name),
								user.getUtil().extractArrayFromList(jt.queryForList(q, p), k, false));
					}
				}
			}
		}
	}

	private boolean isToGenerateJsonResultInArrayField(Map<String, String> arrayField, Usuario user) {
		if (arrayField.get("serviceParams") != null) {
			Map<String, Object> servParams = user.getUtil().JSONtoMap(arrayField.get("serviceParams"));
			if (servParams.get("mmJsonResult") != null) {
				return Boolean.valueOf(servParams.get("mmJsonResult").toString());
			}
		}
		return false;
	}

	private void getArrayFieldInJsonResult(DAOTemplate pjTemplate, Map<String, String> campo, Usuario user,
			String sort, GenericVO vo2, Object[] pks) {

		Map<String, Object> columns = user.getUtil().JSONtoMap(campo.get("columns").toString());
		String columnsToSelect = ",";
		for (Entry<String, Object> cols : columns.entrySet()) {
			columnsToSelect += cols.getKey() + ",";
		}
		columnsToSelect = columnsToSelect.substring(0, columnsToSelect.lastIndexOf(","));
		String k = (String) campo.get("key");
		String q = "select " + k + columnsToSelect + " from " + campo.get("relational_table") + " where (1=1) "
				+ " AND " + getPrimaryKey(user) + " = ? " + sort;
		LogUtil.debug("Query de MM  >> " + q + " >>> " + pks, user);
		vo2.put((String) campo.get(Constants.name),
				user.getUtil().extractJsonFromList(pjTemplate.queryForList(q, (Object[]) pks[0]), k,
						columnsToSelect.substring(0, columnsToSelect.length())));
	}

	/*
	 * private boolean columnExistsInBean(Map<String, Object> columns){
	 * for(Entry<String, Object> c : columns.entrySet()){
	 * if(!columnExistsInBean(c.getKey())){ return false; } } return true; }
	 * 
	 * private boolean columnExistsInBean(String column){ for(Map<String,
	 * String> beanCol : properties ){
	 * if(beanCol.get(Constants.name).equals(column)){ return true; } } return
	 * false; }
	 */

	public int getCheck_code() {
		return check_code;
	}

	public String getConcat(final String type, boolean translate, Usuario user) {
		String g = type.equalsIgnoreCase("go") ? " OR " : " AND ";
		if (translate) {
			if (g.trim().length() == 0) {
				return "";
			}
			g = user.getLabel("label.filtro." + g.trim());
		}
		return g;
	}

	public Object getDAOById(final String id, final Usuario user) {
		final String name = user.getSysParser().getValueFromXQL(
				"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + id + "']", Constants.name, user,
				true, false);
		try {
			return user.getFactory().getSpringBean(name + Constants.DAO_SUFIX, user);
		} catch (final Exception e) {
			return null;
		}
	}

	private String getDelete(final String table, String where, Usuario user) {
		if (where != null && where.trim().length() > 0)
			where = " WHERE " + where;
		if (logSQL)
			LogUtil.debug("  DELETE " + " FROM " + table + where + "  ", user);
		return "  DELETE " + " FROM " + table + where + "  ";
	}

	@Override
	public Map<String, String> getEntity() {
		try {

			return entity;
		} catch (final Exception e) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aseg.dao.RainDAOIface#getForNamedQuery(java .lang.String,
	 * java.lang.Object[])
	 */
	public List getForNamedQuery(final String key, final Object[] params) {
		return null;

	}

	private String[] getIdQuery(Map values, String pk, final boolean codigo, String ctx, boolean sharing,
			final Usuario user, final List joins, final GenericVO vo, String TableAlias, String ope,
			boolean includeTrunc) {
		if (values == null) {
			return new String[] { "", "" };
		}

		Map t = new HashMap(getEntity());
		values.put("contextParams", user.getUtil().removeInvalidItemsFromContextParams(values.get("contextParams")));
		values = new HashMap(values);
		DAOImpl d = this;
		boolean context = false;

		if (values.get("relational_table") != null) {
			// verificar campos tipo array retirado dos dependents
			// causa:MM_MEMBRO_MEMBRO.ID_UNIDADE_FILHO
			t.put("table", values.get("relational_table"));
			values.remove("filtro");
			if (values.get("cyclic") == null) {
				values.put(Constants.name, values.get("key"));
			}
		}
		String buf = "";
		String lv = "";
		String fil = "";
		Object contextParams = user.getUtil().removeInvalidItemsFromContextParams(values.get("contextParams"));
		if (values.get("service") != null && values.get("service").toString().split("\\.")[0].endsWith("Service")
				|| !ctx.equals(getEntity().get("id"))) {

			if (!ctx.equals(getEntity().get("id"))) {
				context = true;
				d = (DAOImpl) getDAOById(ctx, user);
			} else {
				d = (DAOImpl) getBean(
						values.get("service").toString().split("\\.")[0].replaceAll("Service", Constants.DAO_SUFIX),
						user);
			}
			String fke = "";

			if (user.getUtil().match(d.whitExtensions(d.properties, user), Constants.name, getPrimaryKey(user))
					.size() > 0 && codigo && values.get(Constants.component) != null
					&& !values.get(Constants.component).toString().equalsIgnoreCase("context")) {
				fke = getPrimaryKey(user);
				if (values.get("labelValue") == null) {
					lv = getMainField(user);
				} else {
					if (values.get("cyclic") != null || codigo) {
						lv = values.get(Constants.name).toString();
					} else {
						lv = values.get("labelValue").toString();
					}
				}

				if (values.get("filtro") != null && values.get("filtro").toString().indexOf("${") == -1) {
					fil = d.getSqlWhere(null, values.get("filtro"), user, vo, null, includeTrunc);
					if (fil.indexOf("1=1") > -1)
						fil = "";
				}
			} else {
				String alias = null;
				if (values.get("volatile") == null || values.get("volatile") // TODO
						// MM_ERROR
						.equals(Constants.false_str)
				// && values.get("relational_table") == null
				) {
					alias = (String) t.get("table");
					if (context) {
						fke = getPrimaryKey(user);

					} else {

						fke = (String) values.get("name");

					}
				} else {
					if (values.get(Constants.key) != null && false) {
						// MM_ERROR
						fke = (String) values.get(Constants.key);
					} else {
						fke = getPrimaryKey(user);
					}
					alias = "volatile(" + (String) t.get("table") + "." + fke + ")";
				}

				// if(context)
				// {lv = getPrimaryKey(user) ;}
				// else
				// {
				if (values.get("labelValue") == null) {
					if (user.getUtil()
							.match(d.whitExtensions(d.properties, user), Constants.name, values.get(Constants.name))
							.size() == 0)
						lv = d.getMainField(user);
					else
						lv = (String) values.get(Constants.name);
				} else {
					if (codigo) {
						lv = values.get("key").toString();
						if (this.getField(lv, user).size() == 0 && values.get("volatile") == null) {
							lv = (String) values.get("name");
						}
					} else {
						lv = values.get("labelValue").toString();
					}
				}

				// }
				if (ctx.equals(d.getEntity().get("id"))) { // joins fazem isso
					if (values.get("filtro") != null && values.get("filtro").toString().indexOf("${") == -1) {
						fil = (String) values.get("filtro");

					}
					// TODO JOINALIAS
					fil = d.getSqlWhere(d.getEntity().get("table"), fil, user, vo, joins, includeTrunc);
				}
			}
			if (!codigo && !this.equals(d)) {
				List cmp = user.getUtil().match(d.whitExtensions(d.properties, user), Constants.name, lv);
				if (cmp.size() > 0) {
					Map com = new HashMap((Map) cmp.get(0));

					String oldFilter = (String) com.get("filtro");
					if (contextParams != null) {
						final Map par = user.getUtil().JSONtoMap((String) contextParams);

						com.put("filtro",
								user.getUtil().mergeFiltro(oldFilter, user.getUtil().createCriteria(par, "=", "A")));
						com.put("contextParams", contextParams);
						oldFilter = (String) com.get("filtro");

					}

					if (values.get("filtro") != null) {

						com.put("filtro", user.getUtil().mergeFiltro(oldFilter, (String) values.get("filtro")));
					}
					if (defaultDAOFields.contains(fke) || d.getField(fke, user).size() == 0)
						fke = d.getPrimaryKey(user);

					buf += d.getIdQuery(com, fke, codigo, // substituido valor
															// de codigo
							d.getEntity().get("id"), sharing, user, joins, (GenericVO) vo.clone(), TableAlias, ope,
							includeTrunc)[0];
				} else {
					cmp = user.getUtil().match(d.whitExtensions(d.properties, user), Constants.name,
							values.get(Constants.name));
					if (cmp.size() > 0) {
						Map com = new HashMap((Map) cmp.get(0));

						String oldFilter = (String) com.get("filtro");
						if (contextParams != null) {
							final Map par = user.getUtil().JSONtoMap((String) contextParams);
							com.put("filtro", user.getUtil().mergeFiltro(oldFilter,
									user.getUtil().createCriteria(par, "=", "A")));
							com.put("contextParams", contextParams);
							oldFilter = (String) com.get("filtro");
						}

						if (values.get("filtro") != null) {

							com.put("filtro", user.getUtil().mergeFiltro(oldFilter, (String) values.get("filtro")));
						}

						buf += d.getIdQuery(com, (String) values.get(Constants.name), codigo, d.getEntity().get("id"),
								sharing, user, joins, (GenericVO) vo.clone(), TableAlias, ope, includeTrunc)[0];
					}

				}
			}
		}

		DAOImpl fctx = this;

		if (values.containsKey("contextParams"))
			fctx = d;

		if (values.get("filtro") != null && values.get("filtro").toString().indexOf("${") == -1
				&& values.get("filtro").toString().indexOf("!{") == -1) {
			fil = (String) values.get("filtro");
			if (contextParams != null) {
				final Map par = user.getUtil().JSONtoMap((String) contextParams);
				fil = user.getUtil().mergeFiltro(((String) fil), user.getUtil().createCriteria(par, "=", "A"));
				fil = d.processIgnoreFilterClause(fil, par);
			}
			// TODO JOINALIAS
			fil = d.getSqlWhere(d.getEntity().get("table"), fil, user, vo, joins, includeTrunc);

		}

		String r = "";
		String sufix = "";
		String tp = "";
		String fk = "";
		if (codigo && values.get("service") != null) {
			fk = lv;
			tp = "num";
		}

		if (user.getUtil()
				.match(whitExtensions(properties, user), Constants.name, values.get(Constants.name).toString())
				.size() > 0 && (values.get("type").toString().equalsIgnoreCase("num") || !codigo)) {

			if (codigo && values.get(Constants.labelValue) != null) {

				List dx = ((List) user.getUtil().match(whitExtensions(d.properties, user),
						new String[] { Constants.name, "volatile" },
						new String[] { values.get(Constants.labelValue).toString(), null }));
				if (dx.size() > 0

						&& user.getUtil()
								.match(whitExtensions(properties, user), new String[] { Constants.name, "volatile" },
										new String[] { values.get(Constants.name).toString(), null })
								.size() == 0
						&& values.get("type").toString().equalsIgnoreCase("num")) {
					fk = values.get(Constants.labelValue).toString();
					if (codigo && "alfa".equals(((Map) dx.get(0)).get("type")))
						values.put("type", "alfaNoCode");
					else
						values.put("type",
								((Map) dx.get(0)).get("type") == null ? "alfa" : ((Map) dx.get(0)).get("type"));
					t = new HashMap(d.getEntity());
				}
			} else {
				if (values.get("volatile") != null && values.get("volatile").equals("true") && user.getUtil()
						.match(insertControlCollection, Constants.name, values.get(Constants.name)).size() == 0) {
					fk = getPrimaryKey(user);
				} else {
					fk = values.get(Constants.name).toString();
				}

			}

		}

		if (fk.length() == 0 && codigo && values.get("service") != null || context) {
			fk = pk;
		}
		boolean cascate = false;
		String original_service = null;
		if (fk.length() == 0 || values.get("relational_table") != null // TODO
				// MM_ERROR
				&& (values.get("cyclic") == null || (values.get("sharingKey") != null && sharing))) {
			original_service = (String) values.get("service");

			user.getUtil().matchService(values, (String) values.get(Constants.name), null, false, user);

			if ((original_service != null && !original_service.equalsIgnoreCase((String) values.get("service")))
					|| (values.get("sharingKey") != null))
				cascate = true;

			fk = values.get(Constants.name).toString();
		} else if (context) // getField(values.get(Constants.name).toString(),
							// user).size()>0)
		{
			t = values;
			fk = (String) values.get(Constants.name);

		}

		if (canFilter(t.get("table").toString(), fil, user))
			fil = " AND  " + fil;
		else
			fil = "";

		if (cascate) {
			Map p = ((GenericService) getService(original_service, user)).getProperties();
			if (values.get("sharingKey") != null)

			{
				if (codigo)
					sufix = "select " + t.get("table") + "." + fk + " from " + t.get("table") + " where "
							+ t.get("table") + "." + pk + " in ( select  " + p.get("table") + "." + pk + " from "
							+ p.get("table") + " where " + p.get("table") + "." + values.get("key") + " "
							+ FIELD_REPLACE + ") ";
				else
					sufix = "select " + t.get("table") + "." + fk + " from " + t.get("table") + " where "
							+ t.get("table") + "." + pk + " in ( select  " + p.get("table") + "." + pk + " from "
							+ p.get("table") + " where " + p.get("table") + "." + values.get("labelValue") + " "
							+ FIELD_REPLACE + ") ";
			} else
				sufix = "select " + t.get("table") + "." + pk + " from " + t.get("table") + " where " + t.get("table")
						+ "." + fk + " in ( select  " + p.get("table") + "." + fk + " from " + p.get("table")
						+ " where " + p.get("table") + "." + values.get("key") + " " + FIELD_REPLACE + ") ";
		} else {
			sufix = "select " + t.get("table") + "." + pk + " from " + t.get("table") + " where " + t.get("table") + "."
					+ fk + " " + FIELD_REPLACE + " ";
			String F = null;
			Object propertyId = t.get("id");
			if (propertyId == null) {
				propertyId = t.get("parent_id");
			}
			DAOImpl dx = ((DAOImpl) getDAOById(propertyId.toString(), user));
			if (dx != null && values.get("filtro") != null) {
				String f = values.get("filtro").toString();
				if (values.get("contextParams") != null)
					f = d.processIgnoreFilterClause(f.toString(), user.getUtil().JSONtoMap(user.getUtil()
							.removeInvalidItemsFromContextParams(values.get("contextParams").toString())));
				F = dx.getSqlWhere(t.get("table").toString(), f, user, vo, joins, includeTrunc);
			}
			if (F != null && F.trim().length() > 0)
				sufix += " AND " + F;
		}
		tp = values.get("type").toString();

		if (d != null && tp.equals("array")) {
			List<Map<String, String>> fields = user.getUtil().match(whitExtensions(d.getProperties(), user),
					Constants.name, values.get("key"));
			if (!fields.isEmpty()) {
				tp = (fields.get(0)).get("type").toString();
			}

		}

		if (values.get("CDATA") != null) {
			tp = "alfa";
			String data = values.get("CDATA").toString();

			if (data.indexOf("$FILTER$") > -1) {
				if (codigo)
					data = data.replaceAll("\\$FILTER\\$", values.get("key").toString());
				else {
					buf = buf.replaceAll(fk, values.get("labelValue").toString());
					data = data.replaceAll("\\$FILTER\\$", values.get("labelValue").toString());
				}
			}
			if (data.indexOf(FIELD_REPLACE) == -1)
				sufix = "select " + t.get("table") + "." + pk + " from " + t.get("table") + " where " + data + " " + " "
						+ FIELD_REPLACE + " ";
			else
				sufix = data;
			if (sufix.indexOf(" " + getEntity().get("table") + ".") > -1
					&& sufix.indexOf(" " + getEntity().get("table") + " ") == -1 && TableAlias != null
					&& TableAlias.trim().length() > 0 && getEntity().get("id").equalsIgnoreCase(TableAlias))
				sufix = user.getUtil().replace(sufix, " " + getEntity().get("table") + ".", " " + TableAlias + ".");
		}
		if (!codigo && tp.equals("num"))
			tp = "alfa";
		if (buf.length() == 0 || buf.trim().equals(sufix.trim())) {
			r = sufix + fil;
		} else {

			r = user.getUtil().replace(sufix, "" + FIELD_REPLACE + "", ope + " (" + buf + fil + " )");
		}

		return new String[] { r, tp };
	}

	private String processIgnoreFilterClause(String filtro, Map par) {
		// Usado para remover campos com mesmo nome em tabelas diferente com
		// finalidades diferente ex: D_PROCESSOPARTE.CLIENTE e
		// M_ENTIDADE.CLIENTE
		if (par.containsKey("FILTER_IGNORE_CLAUSE")) {
			String ignoreClause = (String) par.get("FILTER_IGNORE_CLAUSE");
			// par.remove("IGNORE_CLAUSE");
			String entityTable = getEntity().get("table");
			String table;
			String field;
			for (String conditions : ignoreClause.split(",")) {
				String[] split = conditions.trim().split(">");
				table = split[0].trim();
				field = split[1].trim();
				if (entityTable.equals(table)) {
					filtro = filtro.replaceAll(field, field + "_CLAUSE_IGNORED");
				}
			}

		}
		return filtro;
	}

	private boolean canFilter(String ctx, String fil, Usuario user) {
		// TODO Auto-generated method stub

		if (fil != null && fil.trim().length() > 0 && fil.indexOf(ctx) > -1)
			return true;
		return false;
	}

	public String getInClause(final String select) {
		return "( " + select + " )";
	}

	private String getLabelCampo(final String Campo, final String contexto, final Usuario user) {
		final List<Object> cam = user.getUtil().match(whitExtensions(properties, user), Constants.name, Campo);

		if (cam.size() == 0 || ((Map) cam.get(0)).get("label") == null) {
			return "";
		}

		return user.getLabel(((Map) cam.get(0)).get("label").toString());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aseg.filtro.service.FiltroService#getHashFromFiltro
	 * (java.lang.String, com.aseg.seguranca.Usuario)
	 */

	private String getMainField(Usuario user) {
		if (mainField == null) {
			try{
			mainField = user.getUtil().extractArrayFromList(
					user.getUtil().filter(whitExtensions(properties, user), "mainField", null), Constants.name,
					true)[0];
			}catch(Exception e)
			{
				throw new ConfigurationException("Cannot find mainfield on configuration",ConfigurationException.TYPE.PARAM_NOT_FOUND );
			}
		}
		return mainField;
	}

	private String getPaginateBy(Usuario user) {
		if (paginateBy == null) {
			try {
				Map c = (Map) user.getUtil().filter(whitExtensions(properties, user), "paginateBy", null).get(0);
				if (c.get("CDATA") != null) {
					paginateBy = (String) c.get("CDATA");
				} else {
					paginateBy = (String) c.get(Constants.name);
				}
			} catch (Exception e) {
				return null;
			}
		}
		return paginateBy;
	}

	private String[] getNomeCampo(final String Campo, final boolean codigo, final String contexto, boolean sharing,
			final Usuario user, final List joins, final GenericVO vo, String Talias, String ope) {
		String dt = "";
		String _SQL = "";
		Map values = null;

		try {
			if (sharing || contexto.equals(getEntity().get("id"))) {
				values = (Map) user.getUtil().match(whitExtensions(properties, user), Constants.name, Campo).get(0);
			} else {
				DAOImpl d = (DAOImpl) getDAOById(contexto, user);
				values = (Map) user.getUtil().match(d.whitExtensions(d.getProperties(), user), Constants.name, Campo)
						.get(0);
			}
		} catch (final Exception e) {

		}

		// if (codigo) {
		final String[] a = getIdQuery(values, getPrimaryKey(user), codigo, contexto, sharing, user, joins, vo, Talias,
				ope, true);
		_SQL = queryLowerCase(a[0]);
		dt = a[1];
		// if (_SQL.trim().length() == 0) {
		// a = getJoinQuery(values, user);
		// _SQL = queryLowerCase(a[0]);
		// dt = a[1];
		// }
		_SQL = user.getUtil().replaceFuncionDialeto(_SQL, user);
		return new String[] { _SQL, dt };
		/*
		 * } else { try { String[] a = getJoinQuery(values, user); _SQL =
		 * queryLowerCase(a[0]); dt = a[1]; } catch (Exception e) {
		 * e.printStackTrace(); String[] a =
		 * getIdQuery(values,getPrimaryKey(user) ,"", user); _SQL =
		 * queryLowerCase(a[0]); dt = a[1]; } }
		 */
		// _SQL =
		// user.getUtil().replaceFuncionDialeto(_SQL,
		// user);
		// return new String[] { _SQL, dt };

	}

	/**
	 * Retorna a parte do campo concatenada ao seu operador escolhido na
	 * interface do filtro (or ou and)
	 * 
	 * @return Retorna a clausula where para filtros
	 * @exception Exception
	 *                Descr"+Constants.ESCAPE_CHAR+""+Constants.ESCAPE_CHAR+"o
	 *                da exce"+Constants.ESCAPE_CHAR+""+Constants.ESCAPE_CHAR+"o
	 * @since 03/06/2004
	 */

	private boolean isToIgnoreCondition(GenericVO vo, String field, String valor1, String valor2) {
		if (valor1 != null) {
			if (valor1.equals("@binderValue@")) {
				return true;
			} else if (valor1.replaceAll(Constants.IN_SEQUENCE_VALUES, "").isEmpty()) {
				return true;
			}
		}
		if (vo != null && vo.get("FILTER_IGNORE_CLAUSE") != null) {
			List fieldsIgnored = Arrays.asList(vo.get("FILTER_IGNORE_CLAUSE").toString().split(","));
			return fieldsIgnored.contains(field);
		}
		return false;
	}

	public String getOperadorCampo(final String indice, final String Campo, String valor1, String valor2,
			boolean codigo, final String contexto, boolean sharing, final Usuario user, final GenericVO vo,
			final List joins, boolean translate, String Talias, String ope) {

		if (isToIgnoreCondition(vo, Campo, valor1, valor2)) {
			return "()";
		}

		final int i = indice.hashCode();

		if (valor1 != null && valor1.startsWith("${")) {
			valor1 = user.getUtil().EvaluateInString(valor1, user.getUtil().createContext(vo, user));
		}
		if (valor2 != null && valor2.startsWith("${")) {
			valor2 = user.getUtil().EvaluateInString(valor2, user.getUtil().createContext(vo, user));
		}

		// if(!valor1.equals("")){
		String _SQL1[] = null;
		String _SQL = "";
		Operations selectedOperator = Operations.getByOperator(indice);
		switch (selectedOperator) {

		// 113755

		case LIKE: // ""+Constants.ESCAPE_CHAR+" semelhante a =
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.semelhante") + " "
						+ valor1;
			}
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);

			final String[] tmp = user.getUtil().getSoundex(user).ParseWord(valor1);

			String[] s = _SQL1[0].split("where");

			s[1] = user.getUtil().replace(s[1], FIELD_REPLACE, "");

			for (int j = 0; j < tmp.length; j++) {

				if (j == 0)
					_SQL = s[0] + " where " + s[1].trim() + "_FONETIC " + " like '%"
							+ user.getUtil().getSoundex(user).Sound(tmp[j]) + "%' ";
				else
					_SQL = _SQL + " and " + s[1].trim() + "_FONETIC " + " like '%"
							+ user.getUtil().getSoundex(user).Sound(tmp[j]) + "%' ";
			}

			// //LogUtil.debug("goc >>>>"+_SQL);
			// return getInClause(getSelect(_SQL,con));
			return getInClause(_SQL);
		}

		case EQUAL: // ""+Constants.ESCAPE_CHAR+" semelhante a =
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.igual") + " " + valor1;
			}
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			if (contexto.equalsIgnoreCase("AU")
					&& (Campo.equalsIgnoreCase("AD") || Campo.equalsIgnoreCase("AN") || Campo.equalsIgnoreCase("NO"))) {
				// consulta em logs
				// de auditoria nos
				// campos de
				// informacao
				valor1 = ""; // LogUtil.InterpretaStringLog(valor1, Campo);
			}
			if (_SQL1[1].equalsIgnoreCase("alfaNoCode")) {
				valor1 = valor2;
				_SQL1[1] = "alfa";
			}

			final String[] tmp = formataValorFromCampo(valor1, Campo, 0, _SQL1[1], contexto, user);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " = " + tmp[0], tmp[1], Campo, null, vo, user);

			// //LogUtil.debug("goc >>>>"+_SQL);
			// return getInClause(getSelect(_SQL,con));
			return getInClause(_SQL);
		}
		case NOT_EQUAL: // "+Constants.ESCAPE_CHAR+" diferente de <>
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.diferente") + " "
						+ valor1;
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			if (contexto.equalsIgnoreCase("AU") && contexto.equalsIgnoreCase("AU")) {
				// consulta em logs
				// de auditoria
				valor1 = "";// LogUtil.InterpretaStringLog(valor1, Campo);
			}
			if (_SQL1[1].equalsIgnoreCase("alfaNoCode")) {
				valor1 = valor2;
				_SQL1[1] = "alfa";
			}
			final String[] tmp = formataValorFromCampo(valor1, Campo, 0, _SQL1[1], contexto, user);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " = " + tmp[0], tmp[1], Campo, null, vo, user);
			return getInClause(_SQL);
		}

		case NOT_LIKE: // "+Constants.ESCAPE_CHAR+" diferente de !=
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.diferente") + " "
						+ valor1;
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			if (_SQL1[1].equalsIgnoreCase("alfaNoCode")) {
				valor1 = valor2;
				_SQL1[1] = "alfa";
			}
			final String[] tmp = formataValorFromCampo(valor1, Campo, 0, _SQL1[1], contexto, user);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " = " + tmp[0], tmp[1], Campo, null, vo, user);
			return getInClause(_SQL);
		}
		case IN: // "IN " in ,
		{
			if (valor1 != null) {
				valor1 = valor1.replaceAll(Constants.IN_SEQUENCE_VALUES, "");
				valor1 = user.getUtil().replace(valor1, "^", "'");
			}
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.in") + " " + valor1;
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);

			if (_SQL1[1].equals("array")) {
				Map<String, String> arrayField = (Map) user.getUtil()
						.match(whitExtensions(properties, user), Constants.name, Campo).get(0);
				if (arrayField.get("columns") != null) {
					Map<String, Object> columns = user.getUtil().JSONtoMap(arrayField.get("columns"));
				}
			}

			if (valor1 == null || valor1.replaceAll("\"", "").equals("null")) {

				if (translate) {
					return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.in") + " " + "()";
				}

				_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " is  null ", "", Campo, null, vo, user);
			} else {
				if (valor1.toLowerCase().indexOf("select") == -1) {
					String[] values = valor1.split(",");
					if (_SQL1[1].equals("alfa")) {
						for (int j = 0; j < values.length; j++) {
							values[j] = "'" + values[j] + "'";
						}
					}
					valor1 = Arrays.toString(values);
				} else {
					valor1 = "(" + valor1 + ")";
				}
				if (translate) {
					return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.in") + " " + "("
							+ valor1 + ")";
				}
				_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", "  in ", "", Campo, valor1, vo, user);
			}

			return getInClause(_SQL);

		}
		case NOT_IN: // "Not In notin ,
		{
			if (valor1 != null) {
				valor1 = valor1.replaceAll(Constants.IN_SEQUENCE_VALUES, "");
				valor1 = user.getUtil().replace(valor1, "^", "'");
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			if (valor1 == null || valor1.replaceAll("\"", "").equals("null")) {

				if (translate) {
					return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.notin") + " "
							+ "()";
				}
				_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " is not null ", "", Campo, null, vo, user);
			} else {

				if (valor1.toLowerCase().indexOf("select") == -1) {
					String[] values = valor1.split(",");
					if (_SQL1[1].equals("alfa")) {
						for (int j = 0; j < values.length; j++) {
							values[j] = "'" + values[j] + "'";
						}
					}
					valor1 = Arrays.toString(values);
				} else {
					valor1 = "(" + valor1 + ")";
				}
				if (translate) {
					return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.notin") + " " + "("
							+ valor1 + ")";
				}
				_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " not in ", "", Campo, valor1, vo, user);
			}

			return getInClause(_SQL);

		}
		case MAX: // MAX ,
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.max");
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " = ", "",
					Campo, "(select max(" + Campo + ") from " + getEntity().get("table") + " d where d."
							+ getPrimaryKey(user) + " = " + getEntity().get("table") + "." + getPrimaryKey(user) + " )",
					vo, user);

			return getInClause(_SQL);

		}

		case NULL: // ""+Constants.ESCAPE_CHAR+" nulo " nl ,
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.nulo");
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " is null ", "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case NOT_NULL: // N"+Constants.ESCAPE_CHAR+"o "+Constants.ESCAPE_CHAR+"
						// nulo nnl
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.nao.nulo");
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " is not null ", "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case CONTAINS: // Contem %
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.contem") + " " + valor1;
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			final String[] tmp = formataValorFromCampo(valor1, Campo, 2, _SQL1[1], contexto, user);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " like  " + tmp[0] + "", tmp[1], Campo, null, vo,
					user);
			// _SQL =
			// "select M_PROCESSO.ID_PROCESSO from M_PROCESSO where UPPER (
			// TO_CHAR(M_PROCESSO.ID_PROCESSO, '999999999999999999')) like
			// upper('%2%') escape '"+Constants.ESCAPE_CHAR+"' ";
			return getInClause(_SQL);

		}
		case NOT_CONTAINS: // Nao contem !%
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.nao.contem");
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			final String[] tmp = formataValorFromCampo(valor1, Campo, 2, _SQL1[1], contexto, user);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " like  " + tmp[0] + "", tmp[1], Campo, null, vo,
					user);

			return getInClause(_SQL);

		}
		case BETWEEN: // Esta entre -
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.esta.entre") + " "
						+ valor1 + " " + user.getLabel("label.filtro.AND") + " " + valor2;
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			if (valor2.trim().length() == 0) {
				valor2 = new DataFormat().fromTimestamp(new Timestamp(System.currentTimeMillis()));
			}
			if (_SQL1[1].equals("date")) {

				_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " BETWEEN " +

						Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss", user.getUtil()

								.subtractDay(valor1) + " 23:59:59")
						+ " AND " + Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
								valor2 + " 23:59:59"),
						"", Campo, null, vo, user);
			} else {
				_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "",
						" BETWEEN " + formataValorFromCampo(valor1, Campo, 0, _SQL1[1], contexto, user)[0] + " AND "
								+ formataValorFromCampo(valor2, Campo, 0, _SQL1[1], contexto, user)[0],
						"", Campo, null, vo, user);
			}

			return getInClause(_SQL);

		}
		case GREATER_THAN: // E maior que >
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.maior") + " " + valor1;
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			final String[] tmp = formataValorFromCampo(valor1, Campo, 0, _SQL1[1], contexto, user);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " > " + tmp[0], tmp[1], Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case LESS_THAN: // E menor que <
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.menor") + " " + valor1;
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			final String[] tmp = formataValorFromCampo(valor1, Campo, 0, _SQL1[1], contexto, user);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " < " + tmp[0], tmp[1], Campo, null, vo, user);

			return getInClause(_SQL);

		}

		case TRUE: // E verdadeiro T
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.sim");
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " = 'T' ", "", Campo, null, vo, user);

			return getInClause(_SQL);

		}

		case FALSE: // E falso F
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.nao");
			}

			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " = 'F' ", "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case MALE: // E Masculino MAS
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.masculino");
			}
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " = 'M' ", "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case FEMALE: // E Feminino FEM
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.feminino");
			}
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " = 'F' ", "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		/*
		 * case 13: //Esta contido em uma unidade organizacional { PermissaoUO p
		 * = PermissaoUO.getInstance(); String grupo =
		 * p.getSQLWhere(Integer.parseInt(valor1) ); condicao =
		 * getNomeCampo(Campo,codigo)+" "+grupo.substring(26,grupo.length()) ;
		 * rs = fs.getSelect(_SQL+condicao); return getInClause(rs);
		 * 
		 * }
		 */
		case IN_ORGANIZATIONAL_UNIT: // Esta contido em uma unidade
										// organizacional %U
		{
			final String grupo = "";
			final String membro = "";
			// final HierarquiaUnidades p = HierarquiaUnidades.getInstance();
			try {
				if (codigo == false) {
					// membro = Global.findMembro(valor1, user);

					// grupo = p.getFilhos(membro, user).toString();
					throw new Exception("nao implementado");
				} else {
					// grupo = p.getFilhos(valor1, user).toString();
				}
			} catch (final Exception ex) {
				LogUtil.exception(ex, user);

			}
			codigo = true;
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			if (grupo.equals("[]")) {
				_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", "in (0)", "", Campo, null, vo, user);
			} else {
				_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "",
						"in (" + grupo.substring(1, grupo.length() - 1) + ")", "", Campo, null, vo, user);
			}

			return getInClause(_SQL);
		}

		case IN_SYSTEM_MODULE: // Esta contido em um Modulo do sistema
								// %M
		{
			String grupo = "";

			try {
				// if (codigo == false) {
				// final String cod = ((LogAuditoriaService) getBean(
				// "LogAuditoriaService", user)).getIdModulo(valor1,
				// user);
				// grupo = ((LogAuditoriaService) getBean(
				// "LogAuditoriaService", user)).retornaIdPai(cod,
				// user);
				// } else {
				// grupo = ((LogAuditoriaService) getBean(
				// "LogAuditoriaService", user)).retornaIdPai(valor1,
				// user);
				// }
			} catch (final Exception ex) {
				LogUtil.exception(ex, user);
			}

			codigo = true;
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			if (grupo.length() == 0) {
				_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", "in (0)", "", Campo, null, vo, user);
			} else {
				_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", "in (" + grupo + ")", "", Campo, null, vo,
						user);
			}
			// LogUtil.debug(_SQL);
			return getInClause(_SQL);
		}

		case TODAY: // "+Constants.ESCAPE_CHAR+" hoje hoje
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.hoje");
			}

			final Calendar ini = Calendar.getInstance();
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}

		case YESTERDAY: // ontem
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.ontem");
			}
			final Calendar ini = Calendar.getInstance();
			ini.add(Calendar.DAY_OF_MONTH, -1);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();
			fim.add(Calendar.DAY_OF_MONTH, -1);
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case TOMORROW: // amanha
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.amanha");
			}
			final Calendar ini = Calendar.getInstance();
			ini.add(Calendar.DAY_OF_MONTH, 1);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();
			fim.add(Calendar.DAY_OF_MONTH, 1);
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}

		case LAST_WEEK: // na semana passada sp
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.semana.passada");
			}
			final Calendar ini = Calendar.getInstance();
			ini.add(Calendar.WEEK_OF_MONTH, -2);
			ini.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = (Calendar) ini.clone();
			fim.add(Calendar.DAY_OF_MONTH, 6);
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case THIS_WEEK: // nesta semana ns
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.semana.nesta");
			}
			final Calendar ini = Calendar.getInstance();
			ini.add(Calendar.WEEK_OF_MONTH, -1);
			ini.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = (Calendar) ini.clone();
			fim.add(Calendar.DAY_OF_MONTH, 6);
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case NEXT_WEEK: // na semana que vem ps
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.semana.proxima");
			}
			final Calendar ini = Calendar.getInstance();
			// ini.add(Calendar.WEEK_OF_MONTH, 1);
			ini.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = (Calendar) ini.clone();
			fim.add(Calendar.DAY_OF_MONTH, 6);
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case LAST_MONTH: // no mes passado mp
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.mes.passado");
			}
			final Calendar ini = Calendar.getInstance();
			ini.add(Calendar.MONTH, -1);
			ini.set(Calendar.DAY_OF_MONTH, 1);

			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = (Calendar) ini.clone();
			fim.set(Calendar.DAY_OF_MONTH, fim.getActualMaximum(Calendar.DAY_OF_MONTH));
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case THIS_MONTH: // no mes atual ma
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.mes.atual");
			}
			final Calendar ini = Calendar.getInstance();
			// ini.add(Calendar.MONTH, -1);
			ini.set(Calendar.DAY_OF_MONTH, 1);

			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = (Calendar) ini.clone();
			fim.set(Calendar.DAY_OF_MONTH, fim.getActualMaximum(Calendar.DAY_OF_MONTH));
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}

		case NEXT_MONTH: // no proximo mes pm
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.mes.proximo");
			}
			final Calendar ini = Calendar.getInstance();
			ini.add(Calendar.MONTH, 1);
			ini.set(Calendar.DAY_OF_MONTH, 1);

			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = (Calendar) ini.clone();
			fim.set(Calendar.DAY_OF_MONTH, fim.getActualMaximum(Calendar.DAY_OF_MONTH));
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}

		case LAST_YEAR: // no ano anterior aan
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.ano.anterior");
			}
			final Calendar ini = Calendar.getInstance();
			ini.set(Calendar.MONTH, Calendar.JANUARY);
			ini.set(Calendar.DAY_OF_MONTH, ini.getActualMinimum(Calendar.DAY_OF_MONTH));
			ini.add(Calendar.YEAR, -1);

			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = (Calendar) ini.clone();
			fim.set(Calendar.MONTH, Calendar.DECEMBER);
			fim.set(Calendar.DAY_OF_MONTH, fim.getActualMaximum(Calendar.DAY_OF_MONTH));
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case THIS_YEAR: // no ano atual aat
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.ano.atual");
			}
			final Calendar ini = Calendar.getInstance();
			ini.set(Calendar.MONTH, Calendar.JANUARY);
			ini.set(Calendar.DAY_OF_MONTH, ini.getActualMinimum(Calendar.DAY_OF_MONTH));

			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = (Calendar) ini.clone();
			fim.set(Calendar.MONTH, Calendar.DECEMBER);
			fim.set(Calendar.DAY_OF_MONTH, fim.getActualMaximum(Calendar.DAY_OF_MONTH));
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case NEXT_YEAR: // no proximo ano pa
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.ano.proximo");
			}
			final Calendar ini = Calendar.getInstance();
			ini.set(Calendar.MONTH, Calendar.JANUARY);
			ini.set(Calendar.DAY_OF_MONTH, ini.getActualMinimum(Calendar.DAY_OF_MONTH));
			ini.add(Calendar.YEAR, 1);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = (Calendar) ini.clone();
			fim.set(Calendar.MONTH, Calendar.DECEMBER);
			fim.set(Calendar.DAY_OF_MONTH, fim.getActualMaximum(Calendar.DAY_OF_MONTH));
			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case LAST_30_DAYS: // nos ultimos 30 dias u30
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.dias.30ultimos");
			}
			final Calendar ini = Calendar.getInstance();

			ini.add(Calendar.DAY_OF_YEAR, -30);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();

			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case LAST_60_DAYS: // nos ultimos 60 dias u60
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.dias.60ultimos");
			}
			final Calendar ini = Calendar.getInstance();

			ini.add(Calendar.DAY_OF_YEAR, -60);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();

			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case LAST_180_DAYS: // nos ultimos 180 dias u180
		{

			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.dias.180ultimos");
			}
			final Calendar ini = Calendar.getInstance();

			ini.add(Calendar.DAY_OF_YEAR, -180);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();

			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case LAST_360_DAYS: // nos ultimos 360 dias u360
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.dias.360ultimos");
			}
			final Calendar ini = Calendar.getInstance();

			ini.add(Calendar.DAY_OF_YEAR, -360);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();

			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}

		case NEXT_30_DAYS: // nos proximos 30 dias p30
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.dias.30proximos");
			}
			final Calendar ini = Calendar.getInstance();

			ini.add(Calendar.DAY_OF_YEAR, 30);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();

			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case NEXT_60_DAYS: // nos proximos 60 dias p60
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.dias.60proximos");
			}
			final Calendar ini = Calendar.getInstance();

			ini.add(Calendar.DAY_OF_YEAR, 60);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();

			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case NEXT_180_DAYS: // nos proximos 180 dias p180
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.dias.180proximos");
			}
			final Calendar ini = Calendar.getInstance();

			ini.add(Calendar.DAY_OF_YEAR, 180);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();

			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case NEXT_360_DAYS: // nos proximos 360 dias p360
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.dias.360proximos");
			}
			final Calendar ini = Calendar.getInstance();

			ini.add(Calendar.DAY_OF_YEAR, 360);
			ini.set(Calendar.HOUR_OF_DAY, 0);
			ini.set(Calendar.MINUTE, 0);
			ini.set(Calendar.SECOND, 0);
			ini.set(Calendar.MILLISECOND, 0);
			final Calendar fim = Calendar.getInstance();

			fim.set(Calendar.HOUR_OF_DAY, 23);
			fim.set(Calendar.MINUTE, 59);
			fim.set(Calendar.SECOND, 59);
			fim.set(Calendar.MILLISECOND, 59);

			final String var = "  between "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(fim.getTime().getTime())))
					+ " and  "
					+ Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case FUTURE: // esta no futuro nf
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.esta.futuro");
			}
			final Calendar ini = Calendar.getInstance();

			final String var = "  > " +

					Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case PAST: // esta no passado np
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.esta.passado");
			}
			final Calendar ini = Calendar.getInstance();

			final String var = "  < " +

					Dialeto.getInstance().getDialeto(user).getToDate("dd/mm/yyyy hh24:mi:ss",
							new DataFormat().fromTimestampcomHoraptBR(new java.sql.Timestamp(ini.getTime().getTime())))
					+ " ";
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", var, "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case YES: // "Sim", S
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.sim");
			}
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " is not null ", "", Campo, null, vo, user);

			return getInClause(_SQL);

		}
		case NO: // N"+Constants.ESCAPE_CHAR+"o N
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.nao");
			}
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " is null ", "", Campo, null, vo, user);

			return getInClause(_SQL);
		}

		case BEGIN_WITH: // Comeca com %?
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.comeca") + " " + valor1;
			}
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			final String[] tmp = formataValorFromCampo(valor1, Campo, 3, _SQL1[1], contexto, user);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " like  " + tmp[0], tmp[1], Campo, null, vo,
					user);

			return getInClause(_SQL);
		}

		case END_WITH: // Comeca com ?%
		{
			if (translate) {
				return getLabelCampo(Campo, contexto, user) + " " + user.getLabel("label.filtro.termina") + " "
						+ valor1;
			}
			_SQL1 = getNomeCampo(Campo, codigo, contexto, sharing, user, joins, vo, Talias, ope);
			final String[] tmp = formataValorFromCampo(valor1, Campo, 1, _SQL1[1], contexto, user);
			_SQL = replaceForField(_SQL1[0], "" + FIELD_REPLACE + "", " like  " + tmp[0], tmp[1], Campo, null, vo,
					user);

			return getInClause(_SQL);
		}

		}

		return "";
	}

	private String getOrder(GenericVO mvo, final String sort, final String dir, Usuario user) {
		final String[] sortable = sort.split(",");
		String d = "";
		Map f = getField(sort, user);
		if (sort.indexOf(",") == -1 && f.get("useSort") != null) {
			Map ss = getField((String) f.get("useSort"), user);
			String s = "";
			if (ss.size() == 0)
				s = (String) f.get("useSort");
			else {
				if (ss.get("CDATA") != null)
					s = (String) ss.get("CDATA");
				else
					s = (String) ss.get(Constants.name);
			}
			sortable[0] = s;

			if (sortable[0].indexOf(",") > -1 && dir != null) {
				if (dir.equals("DESC")) {
					sortable[0] = sortable[0].replace("{DIR}", " DESC ");
				} else {
					sortable[0] = sortable[0].replace("{DIR}", " ASC ");
				}
			}

		}
		if (mvo.getFiltro() != null && mvo.getFiltro().trim().length() > 0
				&& !Dialeto.getInstance().getDialeto(user).requireOrder() && mvo.get("LABELVALUE") == null) {

			return "";
		}
		if (f.get("volatile") != null && f.get("volatile").toString().equalsIgnoreCase("true")) {
			return "";
		}

		if (dir.indexOf(",") > -1) {
			final String[] orders = dir.split(",");
			for (int i = 0; i < orders.length; i++) {
				try {
					if (sortable[i].indexOf("(") < 0
							&& user.getUtil().match(properties, Constants.name, sortable[i]).size() > 0) {
						d = getEntity().get("table") + ".";
					}
					sortable[i] = d + user.getUtil().shortenIdentifier(sortable[i]) + " " + orders[i];
				} catch (final Exception e) {

				}
			}
			d = StringUtils.arrayToCommaDelimitedString(sortable);
		} else {
			if (sortable[0].indexOf("(") < 0
					&& user.getUtil().match(properties, Constants.name, sortable[0]).size() > 0) {
				d = getEntity().get("table") + ".";
			}
			if (sortable[0].indexOf("(") < 0)
				d = d + user.getUtil().shortenIdentifier(sortable[0]) + " " + dir;
			else
				d = d + sortable[0] + " " + dir;
		}

		return " order by " + user.getUtil().replaceFuncionDialeto(d, user);

	}

	private String getPrimaryKey(Usuario user) {

		if (primaryKey == null) {
			try {
				primaryKey = user.getUtil().extractArrayFromList(user.getUtil().filter(properties, "primaryKey", null),
						Constants.name, true)[0];
			} catch (final Exception e) {

			}
		}
		return primaryKey;
	}

	private Map getPrimaryKeyMap(Usuario user) {

		if (pkMap == null) {
			try {
				pkMap = (Map<String, String>) user.getUtil().filter(properties, "primaryKey", null).get(0);
			} catch (final Exception e) {

			}
		}
		return pkMap;
	}

	@Override
	public List<Map<String, String>> getProperties() {

		if (properties == null) {
			return new ArrayList();
		}
		return new ArrayList(properties);
	}

	/**
	 * @param user
	 * @return
	 */

	@Override
	public String[] getSegmentBy(final Usuario user, final String direction, final String alias, final GenericVO vo) {

		if (user.verificaVisibilidadeTotal() || !vo.isSegmentBy() || entity.get("segmentBy") == null
				|| direction != null && direction.equalsIgnoreCase("false")) {
			return new String[] { "", "(1 = 1)" };

		} else {
			String[] hie = null;
			final InterfaceDialeto dialect = Dialeto.getInstance().getDialeto(user);

			if (direction == null || direction.toString().startsWith("DOWN")) {
				hie = dialect.getSqlHierarquia("?", Constants.Unit_ID, "ID_UNIDADE_FILHO", "MM_MEMBRO_MEMBRO", user);
			} else {
				if (direction.toString().startsWith("UP")) {
					hie = dialect.getSqlHierarquia("?", "ID_UNIDADE_FILHO", Constants.Unit_ID, "MM_MEMBRO_MEMBRO",
							user);
				}
			}
			if (hie == null || direction == null) {
				return new String[] { "", "(1 = 1)" };
			}

			final Map<String, String> entity = getEntity();

			final String recursive = "  union select  ? " + dialect.fromDual();
			// TODO autorecursao EX unidades sendo filhas delas mesmo // tratar
			// no nao queremos que a propria unidade fique disponivel
			/*
			 * if (user.getUtil().match(properties , new String[] { "cyclic",
			 * "key" }, new String[] { Constants.true_str, getPrimaryKey(user)
			 * }).size() > 0) { recursive = ""; }
			 */

			final String[] sb = entity.get("segmentBy").split(",");
			String p = "";
			int cont = 0;

			String item = "";

			try {
				if (direction.indexOf(":") > -1) {
					item = vo.get(direction.split(":")[1]).toString();
				} else {
					item = String.valueOf(user.getIdUnidadeOrganizacional());
				}
				if (Integer.parseInt(item) <= 0) {
					return new String[] { "", "(1 = 1)" };
				}

			} catch (Exception e) {
				return new String[] { "", "(1 = 1)" };
			}

			hie[0] = user.getUtil().replace(hie[0], "?", item);

			for (String element : sb) {
				if (element.indexOf("}") > -1) // evaluated
				{
					String el = element.substring(0, element.indexOf("}") + 1);
					if (!user.getUtil().EvaluateBoolean(el, user.getUtil().createContext(vo, user))) {
						continue;
					}
					element = element.substring(element.indexOf("}") + 1);
				}

				String strQuery = "(" + alias + "." + element + " :#opr#:  (" + hie[1] + recursive + ") )";
				if (direction.endsWith("ARRAY")) {
					List a = queryForList(null, hie[0] + " " + hie[1], vo, user);
					String strArray = user.getUtil()
							.arrayToDelimitedString(user.getUtil().extractArrayFromList(a, "ID", true), ",");
					strQuery = "(" + alias + "." + element + " :#opr#:  (" + strArray + ") )";
				}
				String perm = build_permission(user, strQuery, alias + "." + element, direction, vo);
				if (perm.trim().length() > 0) {
					if (cont > 0) {
						if (direction.endsWith("ARRAY")) {
							p += " OR ";
						} else {
							p += " AND ";
						}
					}
					p += perm;
				}
				cont++;
			}

			return new String[] { hie[0], p };
		}

	}

	public boolean isDefaultCRUD(final String serviceMethod) {
		return Constants.CRUD_METHODS.contains(serviceMethod);
	}

	private Map getSelect(final List cam, final String table, String where, final GenericVO vo, final boolean control,
			final boolean sortable, final boolean joinable, boolean aliasDefault, Object[] params, final Usuario user) {
		Map join = new LinkedHashMap<String, String>();
		List joins = new ArrayList();

		final ArrayList r = new ArrayList();
		for (final Iterator iterator = cam.iterator(); iterator.hasNext();) {
			final Map object = new HashMap(((Map) iterator.next()));

			if (!ParseXmlService.canAccess(object, user, true))
				continue;

			if (object.get("component") != null && (object.get("component").equals("fake")))
				continue;

			// if (object.get("listWhenExpression") != null
			// && user.getUtil().EvaluateBoolean(
			// object.get("listWhenExpression").toString(),
			// user.getUtil().createContext(vo, user)))
			// continue;
			if (aliasDefault && getDefaultDAOFields().contains(object.get(Constants.name))) {
				object.put(Constants.name,
						user.getUtil().replaceVarsMap((String) object.get(Constants.name), vo, user) + " "
								+ Dialeto.getInstance().getDialeto(user).getAliasCampo() + " "
								+ object.get(Constants.name) + "_" + getEntity().get("id"));
				r.add(object);
			} else if (object.get("CDATA") != null) {
				if (object.get("serviceMethod") != null
						&& (vo.getMatcher() != null && vo.getMatcher().trim().length() > 0)) {

					String cas = makeCase(object, user);

					if (cas != null)
						object.put(Constants.name, user.getUtil().replaceVarsMap(cas, vo, user) + " "
								+ Dialeto.getInstance().getDialeto(user).getAliasCampo() + " "

								+ object.get(Constants.name));
				} else
					object.put(Constants.name,
							user.getUtil().replaceVarsMap((String) object.get("CDATA"), vo, user) + " "
									+ Dialeto.getInstance().getDialeto(user).getAliasCampo() + " "
									+ object.get(Constants.name));
				r.add(object);
			} else if (joinable && object.get("service") != null) {

				if (object.get("serviceMethod") == null || isDefaultCRUD((String) object.get("serviceMethod"))) {
					// if (object.get("nature") != null
					// && object.get("nature").equals("volatile")) {
					// continue;
					// }
					Map object2 = new HashMap(object);
					Map column = user.getUtil().defineColumn(object, user, false, join.size());
					vo.getAliases().put(object2.get(Constants.name), column.get(Constants.COLUMN_NAME));

					if (column.get(Constants.join) != null && column.get(Constants.COLUMN_NAME) != null) {
						object2.put(Constants.name,
								column.get(Constants.name) + " "
										+ Dialeto.getInstance().getDialeto(user).getAliasCampo() + " "
										+ column.get(Constants.COLUMN_NAME));
						join.putAll((Map) column.get(Constants.join));
						joins.add(column);
						r.add(object2);
					}

				} else {
					if (object.get("serviceMethod") != null
							&& (vo.getMatcher() != null && vo.getMatcher().trim().length() > 0)) {

						String cas = makeCase(object, user);
						if (cas != null)
							object.put(Constants.name,
									user.getUtil().replaceVarsMap(cas, vo, user) + " "
											+ Dialeto.getInstance().getDialeto(user).getAliasCampo() + " "
											+ object.get(Constants.name) + "_" + object.get("labelValue") + " , "
											+ object.get("table") + "." + object.get(Constants.name));
					}

				}
				if (!object.containsKey("volatile")) {
					r.add(object);
				}
			} else if (!object.containsKey("volatile")) {
				r.add(object);
			}

		}
		if (control && vo.getMatcher() != null && vo.getMatcher().trim().length() > 0) {
			r.addAll(insertControlCollection);
		}
		ArrayList fjoins = new ArrayList();
		String[] sb = processWHERE(vo, user, fjoins, where);

		for (Iterator iterator = fjoins.iterator(); iterator.hasNext();) {
			Map c = (Map) iterator.next();
			join.putAll((Map) c.get(Constants.join));
			r.add(c);
		}

		String[] n = user.getUtil().extractArrayFromList(r, Constants.name, true);
		for (int i = 0; i < n.length; i++) {
			if (n[i].indexOf(".") == -1 && n[i].indexOf("(") == -1) {
				if (!n[i].equalsIgnoreCase("null")) {
					n[i] = table + "." + n[i];
				}
			}
		}

		if (vo.isPaginate()) {

			final int TOTAL_REG = Integer.parseInt(getTotalRecords(sb[0],
					getGroupFunction("count", "*", getEntity().get("table").toString(), where, vo, joins, user), params,
					user));
			try {
				vo.setTotal(String.valueOf(TOTAL_REG));
			} catch (final Exception e) {

			}
		}

		Map ret = new HashMap();
		String p = "  SELECT ";
		boolean distinct = false;

		if (vo.isForceDistinct()) {
			p = p + " distinct ";
			distinct = true;
		}

		ret.put("SELECT", p);

		ret.put("FIELDS", StringUtils.arrayToCommaDelimitedString(n));

		ret.put("FROM", createJoin(table, join, user).trim());

		ret.put("WHERE", sb);

		if (sortable) {
			Paginator paginator = vo;

			String sort = paginator.getSort();

			if (sort == null || sort.equals("")) {
				sort = getMainField(user);
			}

			if (sort.indexOf("*") > 1) {
				sort = sort.split("\\*")[0];
			}

			if (vo.getMatcher() != null && vo.getMatcher().trim().length() > 0 && vo.getMatcher().indexOf(",") == -1) {
				sort = vo.getMatcher();
			}
			ret.put("ORDER", getOrder(vo, sort, paginator.getDir(), user));
		}
		if (distinct) {
			String order = (String) ret.get("ORDER");
			Object fields = ret.get("FIELDS");
			if (order == null)
				order = getMainField(user);

			if (getEntity().get("type").equals("M")) {
				String fd = ret.get("FIELDS") + ", " + user.getUtil().replace(ret.get("ORDER").toString(),
						new String[] { " order by ", " ASC", " DESC" }, new String[] { " ", " ", " " });

				if (fd.trim().endsWith(","))
					fd = fd.trim().substring(0, fd.trim().length() - 1);
				ret.put("FIELDS", fd);
			}
			List f = Arrays.asList(n);
			String g = "";
			for (Iterator iterator = f.iterator(); iterator.hasNext();) {
				String object = (String) iterator.next();
				if (order.indexOf("," + object) > -1 || order.indexOf(object + ",") > -1)
					continue;
				else {
					String sr = object.split(" as ")[0];
					if (order.trim().indexOf(" " + sr + " ") == -1)
						g += sr + ",";
				}

			}
			if (order.trim().length() == 0)
				order = " order by ";
			else
				order = order + ",";
			ret.put("ORDER", order + g.substring(0, g.length() - 1));

		}

		return ret;
	}

	private String makeCase(Map object, Usuario user) {

		Map method = ((ServiceImpl) getService(object.get("service").toString(), user))
				.getMethod(object.get("serviceMethod").toString(), user);
		// TODO Auto-generated method stub
		if (method.get("type").equals("L")) {
			List bf = null;
			List f = (List) method.get(Constants.ALL_FIELDS);
			bf = user.getUtil().match(f, "component", "labelTransform");
			if (bf.size() > 0) {
				Map v = (Map) bf.get(0);
				v = user.getUtil().JSONtoMap(v.get("value").toString());
				return DAOUtil.makecase(object.get("table") + "." + object.get(Constants.name).toString(), v, user);
			}
			bf = user.getUtil().match(f, "component", "iconTransform");
			if (bf.size() > 0) {
				Map v = (Map) bf.get(0);
				v = user.getUtil().JSONtoMap(v.get("value").toString());
				if (object.get("CDATA") != null)
					return DAOUtil.makecase(object.get("CDATA").toString(), v, user);
				else
					return DAOUtil.makecase(object.get(Constants.name).toString(), v, user);
			}
		}
		return null;
	}

	private String[] processWHERE(GenericVO vo, Usuario user, List joins, String where) {

		String f2 = "";
		if (getEntity().get("filtro") != null) {
			f2 = getSqlWhere("",
					user.getUtil().EvaluateInString(user.getUtil().decode(getEntity().get("filtro").toString(), user),
							user.getUtil().createContext(vo, user)),
					user, vo, joins, false);
		}
		if (f2.length() > 0) {
			where = where + " AND " + f2;
		}

		String f3 = "";
		if (vo.getFiltro() != null) {
			f3 = getSqlWhere("", user.getUtil().decode(vo.getFiltro(), user), user, vo, joins, false);

		}
		if (f3.length() > 0) {
			where = where + " AND " + f3;
		}

		if (user.getFiltros().containsKey(entity.get("id"))) {

			List<String> itens = (List) user.getFiltros().get(entity.get("id"));
			for (final String map : itens) {
				where += " AND " + getSqlWhere("", map, user, vo, null, false);
			}
		}

		String[] sb = new String[] { "", "" };
		if (getEntity().get("segmentBy") != null && vo.get("SEGMENTBY") == null) {
			sb = getSegmentBy(user, "DOWN", getEntity().get("table").toString(), vo);
			where += " AND " + sb[1];
		} else {
			if (vo.get("SEGMENTBY") != null) {
				sb = getSegmentBy(user, (String) vo.get("SEGMENTBY"), getEntity().get("table").toString(), vo);
				if (!sb[1].isEmpty())
					where += " AND " + sb[1];
			}

		}
		return new String[] { sb[0], where };

	}

	private String createJoin(String table, Map join, Usuario user) {
		// TODO Auto-generated method stub
		String ret = "";
		if (join != null) {
			int xi = 0;
			Iterator i = join.keySet().iterator();

			while (i.hasNext()) {
				String object = (String) i.next();
				ret += " " + user.getUtil().replace(object, "<table_name>", (xi > 0 ? "" : table)) + " on "
						+ user.getUtil().replace((String) join.get(object), "<table_name>", table) + " ";
				xi++;
			}
		}

		return ret.equals("") ? table : ret;
	}

	public Object getService(final String name, final Usuario user) {

		try {
			return user.getFactory().getSpringBean(name, user);
		} catch (final Exception e) {
			return null;
		}
	}

	public GenericService getServiceById(final String id, final Usuario user) {
		final String name = user.getSysParser().getValueFromXQL(
				"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + id + "']", Constants.name, user,
				true, false);
		try {
			return (GenericService) user.getFactory().getSpringBean(name + "Service", user);
		} catch (final Exception e) {
			return null;
		}
	}

	@Override
	public String getSqlWhere(final String alias, final Object filtro, final Usuario user, final GenericVO vo,
			final List joins, boolean includeTrunc) {

		LogUtil.debug("FILTRO USADO " + getEntity().get("name") + " >>> " + filtro, user);

		if (filtro == null || ((String) filtro).trim().length() == 0 || ((String) filtro).trim().startsWith("/")
				|| !((String) filtro).trim().startsWith("{")) {
			return "";
		}

		try {

			String clause = parseFiltro(user.getUtil().JSONtoMap((String) filtro), alias, "AND", user, vo, false, joins,
					includeTrunc);

			clause = clause.trim();

			if (clause.endsWith("AND")) {
				clause = clause.substring(0, clause.lastIndexOf("AND"));
			}
			if (clause.endsWith("OR")) {
				clause = clause.substring(0, clause.lastIndexOf("OR"));
			}
			if (clause.trim().length() == 0) {
				return "";
			}

			return " (" + clause + ") ";
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			return "";

		}
	}

	public String getTotalRecords(String prefix, final String sql, final Object[] params, final Usuario user) {
		String count = "0";
		boolean fim = false;

		if (prefix == null)
			prefix = "";

		if (sql != null && !sql.equals("")) {

			try {
				final StringBuffer sqlf = new StringBuffer(prefix + " ");
				sqlf.append(sql);

				final DAOTemplate tem = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
						null, user.getUtil());
				try {
					if (params == null || sqlf.indexOf("?") == -1) {
						return tem.queryForObject(sqlf.toString(), String.class);
					} else {
						return tem.queryForObject(sqlf.toString(), params, String.class);
					}
				} catch (final org.springframework.dao.IncorrectResultSizeDataAccessException se) {
					return "0";
				}

			} catch (final Exception e) {
				LogUtil.exception(e, user);
				return "0";
			}
		}
		return count;
	}

	public int getType(final Map e, final Usuario user) {
		if (e.get("type").toString().equalsIgnoreCase("num")) {
			return Types.INTEGER;
		}
		if (e.get("type").toString().equalsIgnoreCase("array")) {

			if (e.get("columns") == null)
				return Types.INTEGER;
			else {
				Map c = user.getUtil().JSONtoMap(e.get("columns").toString());
				if (c.containsKey(e.get(Constants.name)) && ((Map) c.get(e.get(Constants.name))).containsKey("type"))
					return getType((Map) c.get(e.get(Constants.name)), user);
				if (getField((String) e.get("key"), user).get("type") != null) {
					return getType(getField((String) e.get("key"), user), user);
				}
				if ((getEntity().get(Constants.name) + Constants.SERVICE_BASE).equals(e.get("service"))) {
					return Types.VARCHAR;
				} else {
					return getType(((GenericService) getService((String) e.get("service"), user))
							.getProperty((String) e.get("key"), user), user);
				}
			}
		}
		if (e.get("type").toString().equalsIgnoreCase("date")) {
			return Types.TIMESTAMP;
		}
		if (e.get("type").toString().equalsIgnoreCase("file")) {
			return Types.BLOB;
		}
		if (e.get("type").toString().equalsIgnoreCase("money")) {
			return Types.DECIMAL;
		}
		if (e.get("type").toString().equalsIgnoreCase("time")) {
			return Types.TIME;
		}
		if (e.get("type").toString().equalsIgnoreCase("object")) {
			return Types.JAVA_OBJECT;
		}
		if (e.get("type").toString().equalsIgnoreCase("text")) {
			if (ConnectionManager.getInstance(user).getBancoConectado(user) == ConnectionManager.ORACLE)
				return Types.CLOB;
			else
				return Types.LONGVARCHAR;
		}
		return Types.VARCHAR;
	}

	@Override
	public String inclui(final GenericVO vo, final Usuario user) throws Exception {

		List cam = user.getUtil().filter(properties, insertFilters[0], insertFilters[1]);

		if (!vo.isImport())
			cam = user.getUtil().match(cam, "parent_id", getEntity().get("id").toString());
		else {
			List m = user.getUtil().filter(user.getUtil().match(properties, "parent_node", "method"),
					insertMethodFilters[0], insertMethodFilters[1]);
			for (Iterator iterator = m.iterator(); iterator.hasNext();) {
				Map object = (Map) iterator.next();
				if (user.getUtil().match(cam, Constants.name, object.get(Constants.name)).size() == 0)
					cam.add(object);
			}

		}

		if (!vo.getMethod().equals("") && !vo.getMethod().equals("inclui")) {
			cam = user.getUtil().match(properties, "parent_id", vo.getMethod());

			List ctx = user.getUtil().match(cam, "component", "context");

			for (Iterator iterator = ctx.iterator(); iterator.hasNext();) {
				Map object = (Map) iterator.next();

				cam.addAll((Collection) ((GenericService) getService(object.get("service").toString(), user))
						.getProperties().get(Constants.ALL_FIELDS));

			}

			cam = user.getUtil().filter(cam, insertMethodFilters[0], insertMethodFilters[1]);
		}

		final String pk = getPrimaryKey(user);
		final List data = user.getUtil().filter(cam, new String[] { "type" }, new String[] { "array" });
		final List datas = user.getUtil().filter(cam, new String[] { "type" }, new String[] { "array" });

		final String[] colums = user.getUtil().extractArrayFromList(data, Constants.name, true);

		for (Iterator iterator = datas.iterator(); iterator.hasNext();) {
			Map object = (Map) iterator.next();

			if (object.get("foneticSearch") != null
					&& object.get("foneticSearch").toString().equalsIgnoreCase("true")) {
				Map f = new HashMap(object);
				f.put("name", object.get("name") + "_FONETIC");
				data.add(f);
				vo.put((String) f.get("name"),
						user.getUtil().getSoundex(user).TextSound((String) vo.get(object.get("name"))));

			}
		}

		try {
			if (vo.get(pk) == null && data.size() > 0) {
				vo.put(pk, inclui(getEntity().get("table").toString(), data, user, vo));
			} else {
				if (data.size() > 0) {
					inclui(getEntity().get("table").toString(), data, user, vo,
							getPrimaryKeyMap(user).containsKey("calculated"), true);
				}
			}
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			throw new RuntimeException(e);
		}
		if (colums.length != cam.size()) {

			processArrayField(vo, user, pk);

		}
		if (cached && !vo.ignoreCache()) {
			afterPropertiesSet(user);
		}
		if (getEntity().get("refresh") != null)
			((DAOImpl) getDAOById(getEntity().get("refresh"), user)).afterPropertiesSet(user);
		if (vo.get(pk) == null) {
			return "";
		}
		return vo.get(pk).toString();
	}

	private Object inclui(final String table, final List<Map<String, String>> fields, final Usuario user,
			final GenericVO vo) throws Exception {

		return inclui(table, fields, user, vo, getPrimaryKeyMap(user).containsKey("calculated"), true);
	}

	private boolean isToIgnorePermission(GenericVO vo, Map<String, String> field, Usuario user) {
		if (vo.get(Constants.IGNORE_PERMISSION) != null
				&& Boolean.valueOf(String.valueOf(vo.get(Constants.IGNORE_PERMISSION)))
				&& ParseXmlService.isMyField(field, user)) {
			return true;
		}
		return false;
	}

	private Object inclui(final String table, final List<Map<String, String>> fields, final Usuario user,
			final GenericVO vo, final boolean forceId, final boolean autocontrol) throws Exception {

		List storages = new ArrayList();

		// Adiciona e declara os valores dos campos de cadastro
		if (autocontrol) {
			fields.addAll(insertControlCollection);

		}
		vo.put("ULTIMA_MODIFICACAO", new DataFormat().nowTimestamp());
		vo.put("DATA_CADASTRO", new DataFormat().nowTimestamp());
		vo.put("ID_USUARIO_CADASTRO", user.getIdUsuario());

		final InterfaceDialeto dialect = Dialeto.getInstance().getDialeto(user);
		final String[] colums = user.getUtil().extractArrayFromList((List) fields, Constants.name, true);

		// N"+Constants.ESCAPE_CHAR+"mero de campos, menos do o ID
		// (n"+Constants.ESCAPE_CHAR+"o precisa ou "+Constants.ESCAPE_CHAR+"
		// nextval)
		final int paramsLength = forceId ? colums.length : colums.length - 1;

		final List<Object> params = new ArrayList<Object>();
		final DAOTemplate tem = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo,
				user.getUtil());
		final SqlUpdate sqlUpdate = new SqlUpdate();
		sqlUpdate.setDataSource(tem.getDataSource());

		StringBuffer sql = new StringBuffer("insert into " + table + " (");
		final StringBuffer values = new StringBuffer(" values (");

		String fieldName;

		final ArrayList add = new ArrayList<String>();
		Map mapping = new HashMap();

		if (!vo.getMethod().equalsIgnoreCase("inclui") || getEntity().get("mapping") != null) // tratando
																								// mapping
																								// de
		// metodos
		{
			final GenericService s = getServiceById(getEntity().get("id").toString(), user);
			Map m = s.getMethod(vo.getMethod(), user);

			if (m == null) {
				m = getEntity();
			}

			if (m != null && m.get("mapping") != null) {
				mapping = user.getUtil().JSONtoMap((String) m.get("mapping"));

			}
		}

		String uploadDir = null;
		try {
			uploadDir = user.getSystemProperty("LOCALIZACAO_SERVIDOR");
		} catch (Exception e1) {
			LogUtil.exception(e1, user);
		}

		boolean repeat = false;
		for (final Map<String, String> field : fields) {
			if (!ParseXmlService.isMyField(field, user))
				continue;
			if (!ParseXmlService.canAccess(field, user, true)
					&& (field.get("type") != null && !field.get("type").equals("bit"))
					&& !isToIgnorePermission(vo, field, user)) {
				continue;
			}

			repeat = false;
			fieldName = field.get(Constants.name).toString();
			if (!forceId && fieldName.equalsIgnoreCase(getPrimaryKey(user))) {
				// oracle
				if (dialect.isGeneratedKeyBySequence()) {
					sql.append(fieldName);
					sql.append(", ");
					values.append(dialect.getSequenceName(table));
					values.append(".nextval, ");
				}
				continue;
			}
			if (add.contains(fieldName) || mapping.containsValue(fieldName)) {
				continue;
			}
			if (field.get("storage") != null && uploadDir != null) {

				storages.add(field);
				continue;
			}
			if (!mapping.containsKey(fieldName) && !mapping.containsKey("*" + fieldName)) {
				add.add(fieldName);
				sql.append(fieldName);
				sql.append(", ");
			} else {
				if (mapping.containsKey("*" + fieldName)) {
					repeat = true;
					add.add(fieldName);
					sql.append(fieldName);
					sql.append(", ");
					values.append("?, ");
					add.add(mapping.get("*" + fieldName));
					sql.append(mapping.get("*" + fieldName));
					sql.append(", ");
				} else {
					add.add(mapping.get(fieldName));
					sql.append(mapping.get(fieldName));
					sql.append(", ");
				}

			}

			values.append("?, ");

			if (field.get("type") != null && field.get("type").equalsIgnoreCase("bit")) {
				if (vo.get(field.get("name")) == null)
					vo.put(field.get("name"), "F");
			}

			int type = getType(field, user);
			if (type == Types.BLOB || type == Types.JAVA_OBJECT || type == Types.CLOB) {

				InputStream is = null;
				long length = 0;
				try {
					if (type == Types.JAVA_OBJECT) {
						type = Types.BLOB;
						String path = dumpToFile(vo.get(fieldName),
								user.getSessionId() + "-" + System.currentTimeMillis());
						is = new FileInputStream(path);
					} else if (type == Types.CLOB) {
						is = new ByteArrayInputStream(vo.get(fieldName).toString().getBytes());
					} else
						is = (InputStream) vo.get(fieldName);
					if (is != null) {
						length = is.available();
					}
				} catch (final Exception e) {
					LogUtil.exception(e, user);
				}
				try {
					/*
					 * if (ConnectionManager.getBancoConectado() ==
					 * ConnectionManager.ORACLE && false) { OracleLobHandler
					 * lobHandler = new OracleLobHandler(); parameterValues[1] =
					 * new SqlLobValue(is, length, lobHandler); } else {
					 */
					final DefaultLobHandler lobHandler = new DefaultLobHandler();
					params.add(new SqlLobValue(is, new Long(length).intValue(), lobHandler));
					if (repeat) {
						params.add(new SqlLobValue(is, new Long(length).intValue(), lobHandler));
					}

				} catch (final Exception e) {
					LogUtil.exception(e, user);
				}

			} else {
				Object v = vo.get(fieldName);
				if (v != null && v.toString().startsWith("${"))
					v = user.getUtil().Evaluate(v.toString(), user.getUtil().createContext(vo, user));

				params.add(v);
				if (repeat) {
					params.add(v);
				}
			}
			if (repeat) {
				sqlUpdate.declareParameter(new SqlParameter(fieldName, type));

			}
			sqlUpdate.declareParameter(new SqlParameter(fieldName, type));
		}

		// Troca o "+Constants.ESCAPE_CHAR+"ltimo ", " por ") "
		sql.replace(sql.length() - 2, sql.length() - 1, ") ");
		values.replace(values.length() - 2, values.length() - 1, ") ");

		sql.append(values);
		if (logSQL)
			LogUtil.debug("Query de Inclusao >> " + sql + " >>" + params, user);

		if (dialect.isGeneratedKeyBySequence()) {
			final String[] names = { getPrimaryKey(user) };
			sqlUpdate.setGeneratedKeysColumnNames(names);
		}
		final KeyHolder keys = new GeneratedKeyHolder();
		sqlUpdate.setReturnGeneratedKeys(true);

		sqlUpdate.setSql(sql.toString());
		sqlUpdate.update(params.toArray(), keys);
		Object r = null;
		if (!forceId) {
			// Retorna o id gerado
			final String columnName = dialect.generatedKeyColumnName(getPrimaryKey(user));
			r = Long.parseLong(keys.getKeys().get(columnName).toString());
		}
		if (r == null) {
			r = vo.get(getPrimaryKey(user));
		}
		vo.put(getPrimaryKey(user), r);

		String primaryKey = getPrimaryKey(user);
		List a = user.getUtil().match(fields, new String[] { "type", "unique" },
				new String[] { "bit", Constants.true_str });
		if (a.size() > 0) {

			for (final Iterator iterator = a.iterator(); iterator.hasNext();) {
				final Map object = (Map) iterator.next();

				if (object.get("filtro") != null) {
					final StandardEvaluationContext ctx = new StandardEvaluationContext();
					ctx.setVariable(Constants.VOKey, vo);
					vo.setFiltro(user.getUtil().EvaluateInString((String) object.get("filtro"), ctx));
				}

				final List aux = new ArrayList();
				aux.add(object);
				if (vo.get(object.get(Constants.name)) != null && vo.get(object.get(Constants.name)).equals("T")) {
					vo.put(object.get(Constants.name).toString(), "F");
					if (getEntity().get("master") != null && getEntity().get("type").equals("D")) {
						primaryKey += "!,"
								+ ((DAOImpl) getDAOById(getEntity().get("master"), user)).getPrimaryKey(user);
					}
					altera(getEntity().get("table").toString(), aux, primaryKey, user, vo, false);
				}
			}
		}

		if (table.equals(getEntity().get("table").toString())) {

			a = user.getUtil().match(properties, new String[] { "type", "nature" },
					new String[] { "scriptRule", "persistent" });
			if (a.size() > 0) {

				for (final Iterator iterator = a.iterator(); iterator.hasNext();) {
					final Map object = (Map) iterator.next();

					if (object.get("bind") != null) {

						final List aux = new ArrayList();
						aux.addAll(user.getUtil().match(properties, Constants.name, object.get("bind")));
						evalRhino(vo, user, object);
						altera(getEntity().get("table").toString(), aux, primaryKey, user, vo, true);

					}
				}
			}
		}
		if (uploadDir != null) {
			for (Iterator iterator = storages.iterator(); iterator.hasNext();) {
				Map field = (Map) iterator.next();
				String[] st = field.get("storage").toString().split("/");

				if (!uploadDir.endsWith(String.valueOf(java.io.File.separatorChar)))
					uploadDir += String.valueOf(java.io.File.separatorChar);

				uploadDir += getDefaultStorePath(user, vo.get(st[1]) == null ? null : vo.get(st[1]).toString(),
						st[0].toLowerCase());
				String rad = "";
				// FormFile file = (FormFile) vo.get("File1");
				if (user.getSystemProperty("LOCALIZACAO_USE_FILE_NAME") == null
						|| user.getSystemProperty("LOCALIZACAO_USE_FILE_NAME").equalsIgnoreCase("true"))
					rad = "-" + vo.get("FILE_NAME_" + field.get("name"));

				try {
					boolean criar = (new File(uploadDir)).mkdirs();
					user.getUtil().writeToDisk((InputStream) vo.get(field.get("name")),
							"pjw(" + vo.get(getPrimaryKey(user)).toString() + ")" + rad, uploadDir);
				} catch (FileNotFoundException e) {
					LogUtil.exception(e, user);
				} catch (IOException e) {
					LogUtil.exception(e, user);
				}
			}
		}
		return r;
	}

	public void evalRhino(GenericVO vo, Usuario user, Map field) throws ScriptException {
		CompiledScript eng = null;

		try {
			eng = ((Compilable) user.getUtil().getJSEngine()).compile((String) field.get("CDATA"));
		} catch (ScriptException e1) {
			LogUtil.exception(e1, user);
		}

		if (eng == null) {
			LogUtil.error("RhinoScript nao compilavel.");
		}

		final Bindings bindings = user.getUtil().getJSEngine().createBindings();
		bindings.put("user", user);
		bindings.put("dm", this);

		bindings.put(Constants.VOKey, vo);
		bindings.put("util", user.getUtil());
		bindings.put("df", new DataFormat());
		//bindings.put("cmp", new TagInputExt());
		bindings.put("result", new Object());
		Object r = null;

		r = eng.eval(bindings);

	}

	@Override
	public List<Map> Listagem(Object[] params, final GenericVO mvo, final Usuario user)
			throws PersistenceException, ConfigurationException {

		Paginator paginator = mvo;

		List a = new LinkedList();

		long time = System.currentTimeMillis();

		String sqlFinal = createSelect(params, mvo, user);

		if (getEntity().get("table") == null) {
			return new ArrayList<Map>();
		}

		a = queryForList(params, sqlFinal, mvo, user);
		if (logSQL)
			LogUtil.debug("Query de LISTA  TIME>> " + user.getUtil().millisecondsToMinutes(time) + " >>> [] ", user);
		time = System.currentTimeMillis() - time;
		mvo.setSTime(user.getUtil().millisecondsToMinutes(time));

		return a;
	}

	public String createSelect(Object[] params, final GenericVO mvo, final Usuario user) {
		return mountQuery(createSelectStruture(params, mvo, false, user), mvo, user);
	}

	public String mountQuery(Map m, GenericVO vo, Usuario user) {
		String s = m.get("SELECT") + " " + m.get("FIELDS") + " FROM " + m.get("FROM");

		String[] sb = (String[]) m.get("WHERE");

		s += " WHERE (1=1) " + sb[1] + " ";

		if (m.get("ORDER") != null)
			s += m.get("ORDER");

		// if (user != null && user.getIdUsuario() != null) {
		// s = s.replaceAll("@xusuario@", user.getIdUsuario().toString());
		// }

		if (vo != null && vo.paginate()) {

			s = Dialeto.getInstance().getDialeto(user).getPaginacao(vo, new String[] { sb[0], s }, getPaginateBy(user));

		} else {
			if (s.trim().toLowerCase().startsWith("select"))
				s = sb[0] + " " + s;
		}

		return s == null ? "" : s;
	}

	public List queryForList(Object[] params, String sqlFinal, GenericVO vo, Usuario user) {

		DAOTemplate tem = null;
		try {
			tem = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo, user.getUtil());
		} catch (final ConfigurationException e2) {

			LogUtil.exception(e2, user);
		} catch (final SQLException e2) {

			LogUtil.exception(e2, user);
		}
		List a;
		if (params != null) {
			if (logSQL)
				LogUtil.debug("Query de LISTA >> " + sqlFinal + " >>> " + Arrays.asList(params), user);
		} else {
			if (logSQL)
				LogUtil.debug("Query de LISTA >> " + sqlFinal + " >>> [] ", user);
		}

		a = tem.queryForList(sqlFinal, params);
		return a;
	}

	public Object calc(GenericVO vo, Usuario user) {
		String[] r = processWHERE(vo, user, null, "");
		if (vo.get("GROUP_FIELD") != null && vo.get("GROUP_FIELD").toString().contains(",")) {
			// retorna uma lista com items correspondentes
			return queryForList(null, getGroupFunction(vo.get("GROUP_FUNCTION").toString(),
					vo.get("GROUP_FIELD").toString(), getEntity().get("table"), r[1], vo, null, user), vo, user);
		} else {
			// retorna uma String com a contagem de registros...
			return getTotalRecords(r[0], getGroupFunction(vo.get("GROUP_FUNCTION").toString(),
					vo.get("GROUP_FIELD").toString(), getEntity().get("table"), r[1], vo, null, user), new Object[] {},
					user);
		}
	}

	private String getGroupFunction(String function, String field, String table, String where, GenericVO vo, List joins,
			Usuario user) {
		String res = "";
		String[] sb = processWHERE(vo, user, joins, where);

		String[] fieldsArray = null;
		if (field.contains(",")) {
			fieldsArray = field.split(",");
		}
		if (fieldsArray != null) {
			/*
			 * fieldsArray works:
			 * 
			 * SELECT function(fieldsArray[0]), fieldsArray[1], ... FROM ????
			 * GROUP BY ( fieldsArray[1] || vo.get('GROUP_BY') )
			 */
			String fields = ",";
			for (int i = 1; i < fieldsArray.length; i++) {
				fields += fieldsArray[i];
				if ((i + 1) != fieldsArray.length) {
					fields += ",";
				}
			}
			String groupBy = "";
			if (vo.get("GROUP_BY") != null && !vo.get("GROUP_BY").equals("null")) {
				groupBy = vo.get("GROUP_BY").toString();
			} else {
				groupBy = fieldsArray[1];
			}
			res = " ( SELECT " + function + "(" + fieldsArray[0] + ")" + fields + " FROM " + table + " WHERE 1=1  "
					+ sb[1] + " GROUP BY " + groupBy + " )";
		} else {
			res = " ( SELECT " + function + "(" + field + ")" + " FROM " + table + " WHERE 1=1  " + sb[1] + "  )";
		}
		return res;
	}

	@Override
	public Map obtem(final GenericVO vo, String filter, final Usuario user) throws Exception {
		String[] sb = new String[] { "", "" };
		if (cached && filter != null && filter.toString().startsWith(Constants.CACHE_SEPARATOR)) {
			final Cache cache = (Cache) getBean("dataCache", user);
			final net.sf.ehcache.Element element = cache.get(user.getSystem() + filter);
			if (element == null) {
				return null;
			}
			return (Map) element.getObjectValue();
		}
		final List all = whitExtensions(properties, user);

		if (all.isEmpty()) {
			return null;
		}

		List cam = user.getUtil().filter(all, "type", "array");
		List ctx = user.getUtil().match(
				user.getUtil().filter(user.getUtil().match(properties, "component", "context"), "key", null),
				"parent_id", getEntity().get("id").toString());

		List cam1 = user.getUtil().match(cam, "parent_id", getEntity().get("id").toString());
		List cam2 = user.getUtil().match(cam, "parent_node", "method");
		cam.addAll(cam1);
		cam.addAll(cam2);
		cam = user.getUtil().filter(cam, new String[] { "type", "volatile" },
				new String[] { "fake", Constants.true_str });
		cam.addAll(ctx);

		cam.addAll(insertControlCollection);

		final DAOTemplate jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo,
				user.getUtil());
		String pk = getPrimaryKey(user);

		final Object idv2 = null;
		Object[] args = null;
		String nkey = "";
		if (filter != null && !filter.startsWith("{") && filter.indexOf(",") > -1) {
			final String[] i = filter.split(",");
			args = new Object[i.length];
			for (int j = 0; j < i.length; j++) {
				nkey += " AND " + i[j] + " =  ? ";
				args[j] = vo.get(i[j]);
			}

		} else {
			if (filter != null && filter.startsWith("" + FIELD_REPLACE + "")) {
				final List e = user.getUtil().match(cam, Constants.name,
						user.getUtil().replace(filter, "" + FIELD_REPLACE + "", ""));
				final Map e1 = (Map) e.get(0);
				args = new Object[] { vo.get(filter) };
				pk = user.getUtil().replace(filter, "" + FIELD_REPLACE + "", "");
				filter = null;
			}

			else {
				if (filter != null && !filter.startsWith("{")) {
					pk = filter;

					try {
						if (((Map) user.getUtil().match(cam, Constants.name, filter).get(0)).get("type").toString()
								.equalsIgnoreCase("num")) {
							args = new Object[] { new Long(vo.get(filter).toString()) };
						} else {
							if (vo.isIgnoreCase()) {
								args = new Object[] { vo.get(filter).toString().toUpperCase() };

								if (user.getUtil().match(properties, Constants.name, filter).size() > 0) {
									pk = " UPPER(" + getEntity().get("table") + "." + filter + ") ";
								} else {
									pk = " UPPER(" + filter + ") ";
								}

							} else
								args = new Object[] { vo.get(filter).toString() };
						}

					} catch (final Exception e) {
						String[] ppk = user.getUtil().extractArrayFromList(
								user.getUtil().filter(cam, "mainField", null), Constants.name, true);
						if (ppk.length > 0)
							pk = ppk[0];
						args = new Object[] { vo.get(filter) };
					}
				} else {
					// if (vo.get(pk) != null) {
					args = new Object[] { vo.get(pk) };
					// }
				}
			}
			if (user.getUtil().match(properties, Constants.name, pk).size() > 0) {
				nkey = " AND " + getEntity().get("table") + "." + pk + " =  ? ";
			} else {
				nkey = " AND " + pk + " =  ? ";
			}
			// if(nkey.trim().length()>0 && vo.get(pk)==null)
			// nkey = "";

			if (args[0] == null && vo.getFiltro() != null && vo.getFiltro().startsWith("{")) {
				nkey = "";
				args = new Object[] {};
			}

			if (getEntity().get("segmentBy") != null && vo.get("SEGMENTBY") == null) {
				sb = getSegmentBy(user, "DOWN", getEntity().get("table").toString(), vo);
				nkey += " AND " + sb[1];
			} else {
				if (vo.get("SEGMENTBY") != null) {
					sb = getSegmentBy(user, (String) vo.get("SEGMENTBY"), getEntity().get("table").toString(), vo);
					nkey += " AND " + sb[1];
				}

			}

			if (getEntity().get("type") != null && getEntity().get("type").equals("D"))

			{
				final List r = user.getUtil().match(getProperties(), "relationKey", Constants.true_str);
				final ArrayList j = new ArrayList();
				j.addAll(Arrays.asList(args));
				if (r.size() > 0) {

					for (final Iterator iterator = r.iterator(); iterator.hasNext();) {
						final Map object = (Map) iterator.next();
						if (vo.get(object.get(Constants.name)) == null
								|| vo.get(object.get(Constants.name)).toString().trim().length() == 0) {
							continue;
						}
						if (user.getUtil().match(properties, Constants.name, object.get(Constants.name)).size() > 0) {
							// if(nkey.indexOf(" " +getEntity().get("table") +
							// "."
							// + object.get(Constants.name)+" ")==-1)
							nkey += " AND " + getEntity().get("table") + "." + object.get(Constants.name) + " =  ? ";
						} else {
							// if(nkey.indexOf(" "+object.get(Constants.name)+"
							// ")==-1)
							nkey += " AND " + object.get(Constants.name) + " =  ? ";
						}

						j.add(vo.get(object.get(Constants.name)));
					}

				}
				args = j.toArray();
			}
		}

		String f = "";

		try {
			if (filter != null) {
				f = getSqlWhere("", filter, user, vo, null, false);

			}

		} catch (final Exception e) {
			LogUtil.exception(e, user);
		}

		if (f.length() > 0) {
			f = " AND " + f;
		}

		GenericVO vo2 = null;
		boolean p = vo.isPaginate();
		vo.setPaginate(false);
		String sqlFinal = mountQuery(getSelect(cam, getEntity().get("table").toString(), nkey + f, vo, true, false,
				!vo.isLazyList(), false, args, user), vo, user);
		vo.setPaginate(p);
		try {

			if (args != null) {
				if (logSQL)
					LogUtil.debug("Query de OBTEM >> " + sqlFinal + " >>> " + Arrays.asList(args), user);
			} else {
				if (logSQL)
					LogUtil.debug("Query de OBTEM >> " + sqlFinal + " >>> [] ", user);
			}
			List storages = user.getUtil().filter(cam, "storage", null);
			String uploadDir = null;
			try {
				uploadDir = user.getSystemProperty("LOCALIZACAO_SERVIDOR");
			} catch (Exception e1) {
				LogUtil.exception(e1, user);
			}

			if (args != null && args.length == 1 && (args[0] == null || args[0].equals("null"))) {
				return vo2;
			}
			vo2 = (GenericVO) jt.queryForMap(sqlFinal, args);

			if (uploadDir != null) {
				if (!uploadDir.endsWith(String.valueOf(java.io.File.separatorChar)))
					uploadDir += String.valueOf(java.io.File.separatorChar);

				for (Iterator iterator = storages.iterator(); iterator.hasNext();) {
					Map field = (Map) iterator.next();
					String[] st = field.get("storage").toString().split("/");

					uploadDir += getDefaultStorePath(user, vo2.get(st[1]).toString(), st[0].toLowerCase());

					String[] dirList = new File(uploadDir).list();

					if (dirList != null) {
						for (int i = 0; i < dirList.length; i++) {
							String n = String.valueOf(dirList[i]);
							if (n.startsWith("pjw(" + vo.get(getPrimaryKey(user)).toString() + ")")) {

								File file = new File(uploadDir + n);
								FileInputStream fin = new FileInputStream(file);
								byte fileContent[] = new byte[(int) file.length()];
								fin.read(fileContent);
								vo2.put((String) field.get(Constants.name), fileContent);
							}
						}
					}
				}
			}

			vo2.put("SEGMENTBY", vo.get("SEGMENTBY")); // manter a ordem de
														// segmentacao de uma vo
														// para outra usado em
														// conjunto com cyclic.
			getArrayField(vo2, user);
		} catch (final EmptyResultDataAccessException e) {
			return null;
		} catch (final org.springframework.dao.IncorrectResultSizeDataAccessException e) {
			return jt.queryForList(sqlFinal, args).get(0);
		}

		return vo2;
	}

	private String otimizeJoin(String table, final String string, String object, List joins, Usuario user,
			String operator) {

		final String[] cm = string.toString().split("\\.");
		String cx = " ";

		if (!cm[0].equals(table)) {
			object = user.getUtil().replace(object, cm[1], FIELD_REPLACE);
			object = user.getUtil().replace(object, cx + table, cx + cm[0]);
			object = user.getUtil().replace(object, FIELD_REPLACE, cm[1]);
			table = cm[0];

		}

		final String[] s = object.toString().split(" where ");

		if (s[0].indexOf(string) > -1 && s.length == 2) { // se tamanho maior
			// add JOIN real
			if (operator.trim().startsWith("not")) {
				s[1] = user.getUtil().replace(s[1], new String[] { " = ", " like " },
						new String[] { " <> ", " not like " });
			}
			if (s[0].startsWith("(")) {
				return deleteLastCharAt(s[1]);
			} else {
				return s[1];
			}
		} else if (s.length == 3) {
			if (s[1].trim().startsWith(table) && s[1].trim().startsWith(s[0].substring(s[0].lastIndexOf(" ")).trim())) {
				return s[1] + " where " + deleteLastCharAt(s[2]);
			} else {
				return string + operator + object.toString();
			}
		} else if (s.length > 3) {
			final String[] c = object.toString().split(operator);

			return string + operator + object.toString();
		}
		if (string.indexOf("volatile(") == -1) {
			return string + operator + object.toString();
		} else {
			final String r = string.substring(string.indexOf("(") + 1, string.indexOf(")"));
			s[0] = s[0].substring(0, s[0].indexOf(".")) + "." + r.split("\\.")[1] + " "
					+ s[0].substring(s[0].indexOf("from"));
			return r + operator + s[0] + " where " + s[1];
		}
	}

	private String parseFiltro(final Map campos, String alias, final String operador, final Usuario user,
			final GenericVO vo, boolean translate, List joins, boolean includeTrunc) {
		String valor_um = null;
		String valor_codigo = null;
		String valor_dois = null;
		String nomecampo = null;
		String clause = "";
		boolean codigo;
		if (campos == null || campos.size() == 0) {
			return "";
		}

		final Iterator i = campos.keySet().iterator();

		while (i.hasNext()) {
			final String type = (String) i.next();

			if (type.toLowerCase().startsWith("g")) {
				boolean init = true;
				final List items = (List) campos.get(type);
				if (items.size() > 0) {
					clause += " (";
				}
				for (final Iterator iterator = items.iterator(); iterator.hasNext();) {

					final Map object = (Map) iterator.next();
					if (init) {
						init = false;
						final String p = parseFiltro(object, alias, type, user, vo, translate, joins, includeTrunc);
						if (p.trim().length() > 0 && !p.trim().startsWith("(")) {
							clause += "  (" + p + " " + getConcat(type, translate, user);
						} else if (p.trim().length() > 0) {
							clause += " " + p + " " + getConcat(type, translate, user);
						}
					} else {
						final String p = parseFiltro(object, alias, type, user, vo, translate, joins, includeTrunc);
						if (p.trim().length() > 0) {
							clause += " " + p + " " + getConcat(type, translate, user);
						}
					}
					if (!iterator.hasNext()) {
						clause = clause.trim();
						if (clause.endsWith(getConcat(type, translate, user).trim())) {
							clause = clause.substring(0, clause.lastIndexOf(getConcat(type, translate, user).trim()));
						}
						if (clause.trim().length() > 0 && !clause.trim().endsWith(")")) {
							clause = clause + ") " + getConcat(type, translate, user);
						} else {
							clause = clause + "  " + getConcat(type, translate, user);
						}
					}

				}
				clause = clause.trim();
				if (clause.endsWith(getConcat(type, translate, user).trim())) {
					clause = clause.substring(0, clause.lastIndexOf(getConcat(type, translate, user).trim()));
				}

				if (clause.trim().length() == 0) {
					return "";
				}
				if (items.size() > 0) {
					clause += ") ";
				}

				// clause = "(" + clause + ")" + getConcat(type);
			} else if (type.toLowerCase().startsWith("c"))// tratando condicao
			{
				codigo = false;
				final Map con = (Map) campos.get(type);

				if (con.get("j") != null) // join para sort
				{
					if (joins == null)
						joins = new ArrayList();
					String extraJoinCondition = "";
					String[] f = con.get("j").toString().split("\\|");

					if (f.length > 3) { // existe condicao para ID do
										// contexto
						String[] idAndValue = f[3].split(":");
						extraJoinCondition = " and " + f[1] + "." + idAndValue[0] + " = "
								+ ((idAndValue.length == 1 || idAndValue[1].isEmpty() || idAndValue[1] == null) ? "null"
										: idAndValue[1]);
					}
					Map d = new HashMap<String, String>();
					d.put(Constants.real_name, f[2]);
					d.put(Constants.name, f[1] + "." + f[0]);
					Map join = new LinkedHashMap<String, String>();
					join.put("<table_name> left outer join " + f[1],
							" <table_name>." + f[2] + " = " + f[1] + "." + f[2] + extraJoinCondition);

					d.put(Constants.join, join);
					if (!joins.contains(d))
						joins.add(d);

				}
				if (con.size() == 0) {
					return "";
				}

				valor_um = escapeSqlValue((String) con.get("v1"), user);

				if (!translate)
					try {
						valor_codigo = escapeSqlValue((String) con.get("vc"), user);
						if (valor_codigo.startsWith("'") && valor_codigo.endsWith("'")) {
							valor_codigo = (String) valor_codigo.subSequence(1, valor_codigo.length() - 1);
						}
					} catch (Exception e) {
						valor_codigo = null;
					}
				// && valor_um == null SE TEM VALOR CODIGO VAI SEMPRE PELO
				// CODIGO
				if (valor_codigo != null) {
					codigo = true;
				}
				if (valor_um != null && valor_um.equals(valor_codigo)) {
					codigo = false;
				}
				valor_dois = (String) con.get("v2");
				nomecampo = (String) con.get("f");

				if (nomecampo == null) {
					return "";
				}

				final String op = (String) con.get("o");
				// if (op.indexOf("%") == -1) {
				// try {
				// valor_codigo = new Long(valor_um).toString();
				// codigo = true;
				// } catch (final Exception e) {
				//
				// }
				// }

				if (translate || valor_codigo == null || valor_codigo.trim().equals("")
						|| !op.equalsIgnoreCase("=") && !op.equalsIgnoreCase("<>") && !op.equalsIgnoreCase("nl")
								&& !op.equalsIgnoreCase("in") && !op.equalsIgnoreCase("notin")) {
					codigo = false;

				}

				if (valor_codigo == null) {
					valor_codigo = valor_um;

				} else if (valor_dois == null) {
					valor_dois = valor_um;
				}

				String ctx = getEntity().get("id");

				if (con.get("ctx") != null) {
					ctx = (String) con.get("ctx");
				}

				if (nomecampo.equals("#C")) {
					return " ( " + user.getUtil().replace(user.getUtil().replace(valor_um, "^", "'"), "$TABLE",
							getEntity().get("table")) + ")  ";
				}

				if (nomecampo.startsWith("SHARING_")) {

					String t = getEntity().get("table");
					if (alias != null && alias.trim().length() > 0)
						t = alias;
					DAOImpl ss = (DAOImpl) getDAOById(nomecampo.replaceAll("SHARING_", ""), user);
					con.put("sharing", getEntity().get("id"));
					String w = ss.getSqlWhere(null,
							"{gA:[{c:" + con.toString().replaceAll(nomecampo, "ID_ITEM").replaceAll("\"", "'") + "}]}",
							user, vo, null, false);

					if (translate)
						return ss.translate("{gA:[{c:"
								+ con.toString().replaceAll(nomecampo, "ID_ITEM").replaceAll("\"", "'") + "}]}", user,
								vo);

					String[] ds = w.split(" in ");
					ds[0] = " (( (" + t + "." + getPrimaryKey(user);
					return StringUtils.arrayToDelimitedString(ds, " in ");
				}
				String ope = " in ";
				if (negaOperador(op))
					ope = " not in ";
				final String opc = getOperadorCampo(op, nomecampo, valor_codigo, valor_dois, codigo, ctx,
						con.get("sharing") != null, user, vo, joins, translate, alias, ope);

				if (opc.trim().replaceAll(" ", "").equals("()")) {
					if (includeTrunc)
						return "(1=1)";
					else
						return "";
				}

				final Object[] table = { nomecampo.toString(), opc, operador };
				if (translate) {
					return "(" + opc + ")";
				}
				String oClause = "";

				if (alias != null && alias.trim().length() > 0) {
					oClause = otimizeJoin(getEntity().get("table").toString(), alias + "." + getPrimaryKey(user),
							table[1].toString(), joins, user, ope);
				} else {
					oClause = otimizeJoin(getEntity().get("table").toString(),
							getEntity().get("table") + "." + getPrimaryKey(user), table[1].toString(), joins, user,
							ope);
				}

				if (oClause.trim().length() > 0) {
					return "(" + oClause + ")";
				}
			}
		}
		if (user.getUtil().replace(clause, " ", "").length() <= 3) {
			return "";
		}
		return clause;
	}

	private boolean negaOperador(String op) {

		ArrayList t = new ArrayList();
		t.add(1922);// "<>"
		t.add(1084);// "!="
		t.add(1060);// "not like"
		if (t.contains(op.hashCode()))
			return true;
		return false;
	}

	private String escapeSqlValue(String sqlStrValue, Usuario user) {

		String quoteEscape = Dialeto.getInstance().getDialeto(user).getQuoteEscape();
		/*
		 * TODO possivel correcao para escape em condicoes IN que
		 * contem o select no valor. if(sqlStrValue == null ||
		 * sqlStrValue.startsWith( "select ")){}
		 * 
		 */
		if (sqlStrValue == null) {
			return sqlStrValue;
		}
		if (sqlStrValue.trim().startsWith("'") && sqlStrValue.trim().endsWith("'")) {
			sqlStrValue = sqlStrValue.replaceAll("'", quoteEscape);
			sqlStrValue = "'" + sqlStrValue + "'";
		} else {
			sqlStrValue = sqlStrValue.replaceAll("'", quoteEscape);
		}
		return sqlStrValue;
	}

	public void cleanArrayField(final GenericVO vo, final Usuario user) {

		DAOTemplate tem = null;
		if (vo.get("relational_table") == null)
			return;
		try {
			tem = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo, user.getUtil());
		} catch (final ConfigurationException e2) {

			LogUtil.exception(e2, user);
		} catch (final SQLException e2) {

			LogUtil.exception(e2, user);
		}
		String ds = getDelete(vo.get("relational_table").toString(), " " + vo.get(Constants.name) + " =  ? ", user);
		tem.update(
				// ja teve table
				ds, new Object[] { forceLong(vo.get(vo.getKey())) });

	}

	private void processArrayField(final GenericVO vo, final Usuario user, final String pk) {
		DAOTemplate tem = null;

		List arrays = user.getUtil().match(properties, new String[] { "type", "volatile" },
				new String[] { "array", null });
		if (!vo.isImport() && !isDefaultCRUD(vo.getMethod())) {
			arrays = user.getUtil().match(arrays, "parent_id", vo.getMethod());
		}
		if (arrays.size() > 0) {
			try {
				tem = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, vo,
						user.getUtil());
			} catch (final ConfigurationException e2) {

				LogUtil.exception(e2, user);
			} catch (final SQLException e2) {

				LogUtil.exception(e2, user);
			}
			final GenericVO rvo = getServiceById(getEntity().get("id"), user).createVO(user);
			for (final Iterator iterator = arrays.iterator(); iterator.hasNext();) {
				final Map object = (Map) iterator.next();
				String[] items = null;
				if (ParseXmlService.canAccess(object, user, true)) {
					try {
						items = (String[]) vo.get(object.get(Constants.name));
					} catch (final Exception e) {
						items = new String[] { String.valueOf(vo.get(object.get(Constants.name))) };
					}
					if (items == null)
						items = new String[] {};

					if (object.get("cyclic") != null) {
						if (object.get("reverse") != null) {
							String w = "";
							if (object.get("filtro") != null) {

								w = " AND "
										+ getSqlWhere(object.get("relational_table").toString(),
												user.getUtil().EvaluateInString(object.get("filtro").toString(),
														user.getUtil().createContext(vo, user)),
												user, rvo, null, false);
							}
							if (vo.getMultipleId() != null && vo.getMultipleId().length > 0
									&& !vo.getMultipleId()[0].isEmpty()) {
								w += " AND " + object.get("key").toString() + " in  ("
										+ StringUtils.arrayToCommaDelimitedString(vo.getMultipleId()) + ") ";
								;
							}
							String ds = getDelete(object.get("relational_table").toString(),
									" " + object.get(Constants.name) + " =  ? " + w, user);
							tem.update(
									// ja teve table
									ds, new Object[] { forceLong(vo.get(object.get("reverse"))) });
							rvo.put((String) object.get(Constants.name), vo.get(object.get("reverse")));
						} else {
							tem.update(
									// ja teve table
									getDelete(object.get("relational_table").toString(),
											" " + object.get(Constants.name) + " =  ?", user),
									new Object[] { vo.get(pk) });
						}

					} else {
						tem.update(getDelete(object.get("relational_table").toString(), " " + pk + " =  ?", user),
								new Object[] { vo.get(pk) });
					}
				}
				// incluido novos

				if (vo.isImport() && items.length == 0)
					continue;

				if (items != null) {
					// if (object.get("unique") != null
					// && object.get("unique").equals(Constants.true_str)) {
					final List set = Arrays.asList(items);
					Collections.reverse(set);
					items = (String[]) set.toArray(new String[set.size()]);

					// }

					int cont = 0;
					for (final String item : items) {
						cont++;
						final List n = new ArrayList();
						n.addAll(user.getUtil().match(properties, "primaryKey", Constants.true_str));
						final Map fake = new HashMap(object);
						if (object.get("cyclic") == null) {
							fake.put(Constants.name, fake.get("key"));
						}
						n.add(fake);
						Object var = null;
						try {
							var = Long.parseLong(item.toString());
						} catch (final Exception e) {
							var = item;
							// try {
							// var = Long.parseLong(var.toString());
							// } catch (final Exception e1) {
							// continue;
							// }

						}
						if (var == null || var.toString().trim().length() == 0) {
							continue;
						}
						if (object.get("cyclic") != null && object.get("reverse") == null) {
							rvo.put((String) object.get(Constants.name), forceLong(vo.get(pk)));
							rvo.put(pk, var);
						} else if (object.get("reverse") == null) {
							rvo.put((String) fake.get(Constants.name), var);
							rvo.put(pk, forceLong(vo.get(pk)));
						} else {
							rvo.put(pk, forceLong(item));
						}

						try {
							if (rvo.get(object.get("key")) != null
									&& rvo.get(object.get("key")).toString().indexOf(",") > -1) {
								final String[] kproc = rvo.get(object.get("key")).toString().split(",");
								for (final String element : kproc) {
									try {
										rvo.put((String) object.get("key"), new Long(element));
									} catch (Exception e) {
										rvo.put((String) object.get("key"), element);
									}

									processColumnsArray(object, rvo, cont, n, user);
									inclui(object.get("relational_table").toString(), n, user, rvo, true, false);
									cont++;
								}

							} else {
								rvo.putAllAndGet(vo, false);
								processColumnsArray(object, rvo, cont, n, user);
								inclui(object.get("relational_table").toString(), n, user, rvo, true, false);
							}
						} catch (final Exception e) {
							LogUtil.exception(e, user);
							throw new RuntimeException(e);
						}
					}
				}

			}

		}
	}

	private Object forceLong(Object item) {
		// TODO Auto-generated method stub
		try {
			return new Long(item.toString());
		} catch (Exception e) {
			return item;
		}
	}

	private void processColumnsArray(final Map object, final GenericVO rvo, final int cont, final List n,
			Usuario user) {
		if (object.get("columns") != null) { // tratamento da
			// injecao de
			// dependencias
			// em tabelas mm
			// attr columns
			final Map c = user.getUtil().JSONtoMap((String) object.get("columns"));
			final Iterator ix = c.keySet().iterator();
			while (ix.hasNext()) {
				final Object object2 = ix.next();
				final Map cmp = (Map) c.get(object2);
				cmp.put(Constants.name, object2);
				n.add(cmp);
				final Object value = null;
				if (cmp.get("value") != null && cmp.get("value").equals("${SEQ}")) // apenas
				// tratamento
				// de
				// sequencia
				{
					rvo.put((String) object2, new Long(String.valueOf(cont)));
				} else if (cmp.get("value") != null) {
					rvo.put((String) object2, cmp.get("value"));
				}
			}
		}
	}

	public String queryLowerCase(final String q) {
		boolean b = false;
		String s = "";
		if (q.indexOf("'") > -1) {

			final String a[] = q.split("'");
			for (final String element : a) {
				if (b) {
					s += "'" + element + "'";
					b = false;
				} else {
					b = true;
					s += element.toLowerCase();
				}
			}
		} else {
			s = q;
		}

		return s;
	}

	@Override
	public int remove(String id, final String pk, GenericVO vo, final Usuario user) throws Exception {
		if ("null".equals(id)) {
			id = null;
		}

		List cam = null;
		if (vo.getMethod().equals("") || vo.getMethod().equalsIgnoreCase("remove")) {
			cam = user.getUtil().filter(properties, new String[] { "volatile" }, new String[] { Constants.true_str });

			cam = user.getUtil().match(cam, "parent_id", getEntity().get("id").toString());

		} else {

			cam = user.getUtil().match(properties, "parent_id", vo.getMethod());

			List ctx = user.getUtil().match(cam, "component", "context");

			for (Iterator iterator = ctx.iterator(); iterator.hasNext();) {
				Map object = (Map) iterator.next();

				cam.addAll((Collection) ((GenericService) getService(object.get("service").toString(), user))
						.getProperties().get(Constants.ALL_FIELDS));

			}

			cam = user.getUtil().filter(cam, updateMethodFilters[0], updateMethodFilters[1]);

		}

		if (cam.size() == 0) {
			return 0;
		}

		final DAOTemplate jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
				getServiceById(getEntity().get("id"), user).createVO(user), user.getUtil());

		final List td = user.getUtil().match(cam, "nature", "dependent");
		for (final Iterator iterator = td.iterator(); iterator.hasNext();) {

			final Map object = (Map) iterator.next();
			if (!((ParseXmlService) user.getSysParser()).canAccess(object, user, true)) {
				continue;
			}
			try {
				jt.update(getDelete(
						object.get("type").equals("array") ? object.get("relational_table").toString()
								: object.get("table").toString(),
						" " + (object.get("type").equals("array") ? pk : object.get(Constants.name)) + " =  ?", user),
						new Object[] { new Long(id) });

			} catch (Exception e) {
				if (object.get("type").equals("array")) {
					jt.update(getDelete(object.get("relational_table").toString(), " " + getPrimaryKey(user) + " =  ?",
							user), new Object[] { new Long(vo.get(getPrimaryKey(user)).toString()) });
				}
			}
		}

		if (getEntity().get("table") != null) {

			if (logSQL)
				LogUtil.debug(getDelete(getEntity().get("table").toString(), " " + pk + " =  ?", user) + ">>" + id,
						user);
			Object pkv = null;
			if (getType((Map) user.getUtil().match(properties, Constants.name, pk).get(0), user) == Types.INTEGER
					&& id != null && id.trim().length() > 0) {
				pkv = new Long(id);
			} else {
				pkv = id;
			}
			try {
				vo.setUpdatedRows(jt.update(getDelete(getEntity().get("table").toString(), " " + pk + " =  ?", user),
						new Object[] { pkv }));
			} catch (Exception e) {
				if (vo.isCommitSingle())
					LogUtil.exception(e, user);
				else
					throw e;
			}

			List storages = user.getUtil().filter(properties, "storage", null);
			String uploadDir = null;
			try {
				uploadDir = user.getSystemProperty("LOCALIZACAO_SERVIDOR");
			} catch (Exception e1) {
				LogUtil.exception(e1, user);
			}

			if (uploadDir != null) {
				if (!uploadDir.endsWith(String.valueOf(java.io.File.separatorChar)))
					uploadDir += String.valueOf(java.io.File.separatorChar);

				for (Iterator iterator = storages.iterator(); iterator.hasNext();) {
					Map field = (Map) iterator.next();
					String[] st = field.get("storage").toString().split("/");

					uploadDir += getDefaultStorePath(user, vo.get(st[1]).toString(), st[0].toLowerCase());

					String[] dirList = new File(uploadDir).list();

					if (dirList != null) {
						for (int i = 0; i < dirList.length; i++) {
							String n = String.valueOf(dirList[i]);
							if (n.startsWith("pjw(" + vo.get(getPrimaryKey(user)) + ")")) {
								File file = new File(uploadDir + n);
								file.setWritable(true);
								System.gc();
								if (!file.delete()) {
									throw new BusinessException(
											user.getLabel("lb.nao.foi.possivel.deletar.arquivo.do.file."
													+ "system.favor.verificar.permissoes.com.admin.da.rede"));
								}
							}
						}
					}

				}
			}

			return vo.getUpdatedRows();
		} else {
			return 0;
		}
	}

	/*
	 * private String[] getJoinQuery(Map values, Usuario user) { try {
	 * 
	 * if (values.get("service") != null) { DAOImpl d = (DAOImpl)
	 * getBean(values.get("service") .toString().replaceAll("Service",
	 * Constants.DAO_SUFIX)); return d.getJoinQuery((Map)
	 * user.getUtil().filter(d.properties, Constants.name,
	 * values.get("key").toString()).get(0), user);
	 * 
	 * }
	 * 
	 * String pk = user.getUtil().extractArrayFromList
	 * (user.getUtil().filter(properties, "primaryKey", null),
	 * Constants.name)[0];
	 * 
	 * String mf, tf = null; if (values.get("primaryKey") == null) { mf =
	 * values.get(Constants.name).toString(); tf =
	 * values.get("type").toString(); } else { mf =
	 * ((UtilServiceImpl)getService(
	 * "UtilService",user)).extractArrayFromList(((UtilServiceImpl
	 * )getService("UtilService",user)).filter(parent.properties, "mainField",
	 * null), Constants.name)[0]; tf =
	 * ((UtilServiceImpl)getService("UtilService"
	 * ,user)).extractArrayFromList(((UtilServiceImpl
	 * )getService("UtilService",user)).filter(parent.properties, "mainField",
	 * null), "type")[0]; }
	 * 
	 * // Map t = parent.getEntity(); // if (t.get("table") == null) Map<String,
	 * String> t = getEntity(); return new String[] { "select " + t.get("table")
	 * + "." + pk + " from " + t.get("table") + " where " + t.get("table") + "."
	 * + mf + " $ ", tf }; } catch (Exception e) { e.printStackTrace(); return
	 * new String[] {}; } }
	 */
	private String replaceForField(String value, final String old, final String nova, final String function,
			final String Campdo, String valor1, GenericVO vo, Usuario user) {
		if (valor1 == null) {
			valor1 = "";
		}

		if (valor1.indexOf("${") > -1) {
			Map ctx = new HashMap();
			ctx.put(Constants.VOKey, vo);
			valor1 = user.getUtil().EvaluateInString(valor1, user.getUtil().createContext(ctx, null));

		}
		if (valor1.equals("()")) {
			valor1 = "('null')";
		}

		while (value.indexOf(old) != -1) {
			final String sub = value.substring(value.indexOf(old) + old.length());
			if (function.length() > 0) {
				String tmp = value.substring(0, value.indexOf(old)).trim();
				String field = "";
				if (tmp.trim().endsWith(")")) {
					field = tmp.substring(tmp.indexOf("("), tmp.length());
					tmp = tmp.substring(0, tmp.indexOf("("));
				} else {
					field = tmp.substring(tmp.lastIndexOf(" "), tmp.length());
					tmp = tmp.substring(0, tmp.lastIndexOf(" "));
				}

				value = tmp + function + "(" + field + ") " + nova + " " + sub;
			} else {
				value = value.substring(0, value.indexOf(old)) + " " + nova + " " + valor1 + sub;
			}
		}
		return user.getUtil().replaceFuncionDialeto(value, user);
	}

	public void setCheck_code(final int check_code) {
		this.check_code = check_code;
	}

	public void setEntity(final Map<String, String> e) {
		entity = e;
	}

	public void setParent(final DAOImpl bean) {
		parent = bean;
	}

	public void setProperties(final List properties) {
		this.properties = properties;
	}

	@Override
	public String translate(String filtro, final Usuario user, final GenericVO vo) {

		String ret = parseFiltro(user.getUtil().JSONtoMap(user.getUtil().decode(filtro, user)), null, "AND", user, vo,
				true, null, false);
		if (ret.trim().length() == 0) {
			ret = "Nenhum filtro para este contexto";
		}

		return ret;

	}

	public List getChildFieldforJoin(String element, final Usuario user) {

		final ArrayList p = new ArrayList();

		List<Map<String, Object>> dataConfig = (List) user.getSysParser().getListValuesFromXQL("/systems/system[@name='"
				+ user.getSchema() + "']/module/bean[@master='" + getEntity().get("id") + "']", Constants.name, user,
				true, true);

		final String[] extensions = user.getUtil().extractArrayFromList(dataConfig, "id", true);
		for (final String extension : extensions) {
			DAOImpl d = ((DAOImpl) getDAOById(extension, user));
			final List rp = user.getUtil().match(user.getUtil().filter(d.properties, getFilters[0], getFilters[1]),
					Constants.name, element);

			if (rp.size() > 0) {
				Map object = new HashMap((Map) rp.get(0));
				if (object.get("service") == null)
					object.put("service", d.getEntity().get("name") + Constants.SERVICE_BASE);
				// else {
				object.put("component", "context");
				object.put("key", getPrimaryKey(user));
				object.put("volatile", "true");
				object.put("labelValue", object.get("name"));
				// object.put("name",
				// getPrimaryKey(user)+"_"+object.get("name"));
				// }
				p.add(object);
				return p;
			}

		}

		return p;

	}

	@Override
	public List whitExtensions(final List properties, final Usuario user) {

		final ArrayList p = new ArrayList();
		if (properties != null)
			p.addAll(properties);
		if (getEntity() == null) {
			return p;
		}
		if (getEntity().get("extends") != null) {
			final String[] extensions = getEntity().get("extends").split(",");
			for (final String extension : extensions) {
				final List rp = user.getUtil().filter(((DAOImpl) getDAOById(extension, user)).properties, getFilters[0],
						getFilters[1]);
				for (final Iterator iterator = rp.iterator(); iterator.hasNext();) {
					final Map object = (Map) iterator.next();
					if (user.getUtil().match(p, Constants.name, object.get(Constants.name).toString()).size() == 0) {
						p.add(object);
					}
				}
			}

		}
		p.addAll(insertControlCollection);
		return p;

	}

	@Override
	public void removeAll(final Usuario user) {
		// TODO Auto-generated method stub

		DAOTemplate jt;
		try {
			jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
					getServiceById(getEntity().get("id"), user).createVO(user), user.getUtil());
			jt.update(getDelete(getEntity().get("table").toString(), "", user), new Object[] {});
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			LogUtil.exception(e, user);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LogUtil.exception(e, user);
		}

	}

	@Override
	public Map joinable(Map<String, Object> jsoNtoMap, Usuario user) {
		Map r = new HashMap(jsoNtoMap);
		Iterator i = r.keySet().iterator();
		while (i.hasNext()) {
			String object = (String) i.next();
			Map f = getField(object, user);
			if (f.get("joinable") != null && f.get("joinable").equals("false"))
				jsoNtoMap.remove(object);

		}
		// TODO Auto-generated method stub
		return jsoNtoMap;
	}

	private Map getField(String object, Usuario user) {
		try {
			return (Map<String, String>) user.getUtil().match(properties, "name", object).get(0);
		} catch (Exception e) {
			return new HashMap();
		}

	}

	@Override
	public Map createSelectStruture(Object[] params, GenericVO mvo, boolean aliasDefault, Usuario user) {
		List cam = user.getUtil().filter(whitExtensions(properties, user), getFilters[0], getFilters[1]);

		if (!cached && !mvo.isExport()) {
			cam = user.getUtil().filter(cam, "type", "file");
			if (mvo.isLazyList()) {
				cam = user.getUtil().filter(cam, "listable", null);
				cam.add(getPrimaryKeyMap(user));
			}
			// if(Constants.JoinSuport)
			// {
			cam.addAll(user.getUtil().filter(user.getUtil().match(properties, "component", "context"), "key", null));
			// cam.addAll(user.getUtil().filter(user.getUtil().filter(user.getUtil().match(
			// user.getUtil().filter(properties,
			// "labelValue", "%,"),
			// "component", "context"), "key", null), "nature",
			// "volatile,"));
			// }
		}
		List methodFields = user.getUtil().match(cam, "parent_node", "method");

		cam = user.getUtil().match(cam, "parent_id", getEntity().get("id").toString());

		cam.addAll(methodFields);
		String where = "";
		if (mvo.getMatcher() != null && mvo.getMatcher().trim().length() > 0) {
			List mather = new ArrayList();
			String[] m = mvo.getMatcher().split(",");
			for (String element : m) {
				List d = user.getUtil().match(whitExtensions(properties, user), Constants.name, element);
				if (d.size() == 0) {
					d = getChildFieldforJoin(element, user);

				}
				mather.addAll(d);
			}
			if (mather.size() > 0) {
				cam = mather;
			}
		}

		if (mvo.getSearchBy() != null && params != null && params.length > 0) {
			if (user.getUtil().match(properties, Constants.name, mvo.getSearchBy()).size() > 0) {
				where += " AND " + getEntity().get("table") + "." + mvo.getSearchBy() + " = ? ";
			} else {
				where += " AND " + mvo.getSearchBy() + " = ? ";
			}

		} else if (getEntity().get("master") != null && getEntity().get("type").equals("D")) {

			String p = getPrimaryKey(user);
			final List rk = user.getUtil().filter(cam, "relationKey", null);
			// chave opcional de relacao o padrao "+Constants.ESCAPE_CHAR+" a
			// propria pk
			if (rk.size() > 0) {
				p = getEntity().get("table") + "." + user.getUtil().extractArrayFromList(rk, Constants.name, true)[0];
			}
			if (params != null && params.length > 0) {
				where += " AND " + p + " = ? ";
			}
		} else if (mvo.getSearchBy() != null && mvo.getFiltro() == null) // search
		// by
		// othe
		// field
		{

			String[] f = mvo.getSearchBy().split(",");
			ArrayList p = new ArrayList();
			for (String element : f) {
				if (mvo.get(element) !=

				null) {
					where += " AND " + getEntity().get("table") + "." + element + " = ? ";
					p.add(mvo.get(element));
				}

			}
			params = p.toArray();
		}

		// if (getEntity().get("sharing") != null) { // retirado pois deve ser
		// relationalKey
		// where += " AND CONTEXTO = '" + mvo.get("CONTEXTO") + "'";
		// }

		return getSelect(cam, getEntity().get("table").toString(), where, mvo, mvo.getControlCollection(), true, true,
				aliasDefault, params, user);
	}

	@Override
	public List getDefaultDAOFields() {
		// TODO Auto-generated method stub
		return defaultDAOFields;
	}
}
