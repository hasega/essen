package com.aseg.dao;

import java.util.List;
import java.util.Map;

public class Metadata {

	private List<String> sequences;
	private Map<String, Map<String, Map<String, Map<String, String>>>> tables;

	public List<String> getSequences() {
		return sequences;
	}

	public void setSequences(List<String> sequences) {
		this.sequences = sequences;
	}

	public Map<String, Map<String, Map<String, Map<String, String>>>> getTables() {
		return tables;
	}

	public void setTables(Map<String, Map<String, Map<String, Map<String, String>>>> tables) {
		this.tables = tables;
	}

}
