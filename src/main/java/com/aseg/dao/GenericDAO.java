package com.aseg.dao;

import java.util.List;
import java.util.Map;

import com.aseg.exceptions.PersistenceException;
import com.aseg.seguranca.Usuario;
import com.aseg.vo.GenericVO;

public interface GenericDAO {

	public Long altera(GenericVO vo, Usuario user) throws Exception;

	public String cont(String filtro_master, String filtro, String join, Usuario user, GenericVO vo) throws Exception;

	public void getArrayField(GenericVO vo2, Usuario user);

	public Map<String, String> getEntity();

	public List<Map<String, String>> getProperties();

	public String[] getSegmentBy(Usuario user, String Direction, String alias, GenericVO vo);

	public Object calc(GenericVO vo, Usuario user);

	public String inclui(GenericVO vo, Usuario user) throws Exception;

	public List Listagem(Object[] params, GenericVO mVO, Usuario user) throws PersistenceException;

	public Map obtem(GenericVO vo, String filter, Usuario user) throws Exception;

	public int remove(String id, String pk, GenericVO vo, Usuario user) throws Exception;

	public String translate(String filtro, Usuario user, GenericVO vo);

	public List whitExtensions(final List properties, final Usuario user);

	public String getSqlWhere(String alias, Object filtro, Usuario user, GenericVO vo, List joins, boolean acceptTrunc);

	public void removeAll(final Usuario user);

	public Map joinable(Map<String, Object> jsoNtoMap, Usuario user);

	public List queryForList(Object[] params, String sqlFinal, GenericVO vo, Usuario user);

	public void cleanArrayField(final GenericVO vo, final Usuario user);

	public String createSelect(Object[] params, final GenericVO mvo, final Usuario user);

	public Map createSelectStruture(Object[] objects, GenericVO v, boolean aliasDefault, Usuario user);

	public List getDefaultDAOFields();

}