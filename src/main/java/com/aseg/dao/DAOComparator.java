package com.aseg.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.JdbcTemplate;

import com.aseg.config.Constants;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.sql.InterfaceDialeto;
import com.aseg.util.sql.InterfaceDialeto.CONSTRAINT_TYPE;

/**
 * Title: Rain Description: Copyright: Copyright (c) 2010
 * 
 * @author: Haroldo Asega
 * 
 *  * @version 1.0
 */
public final class DAOComparator extends ServiceImpl {

	public static enum PROBLEM_TYPE {
		AUTOINCREMENT_NOT_EXISTS, COLUMN_LENGTH, COLUMN_NOT_EXISTS, COLUMN_NULL, COLUMN_NOT_NULL, COLUMN_PRECISION, COLUMN_TYPE, FOREIGN_KEY_NOT_EXISTS, PRIMARY_NOT_EXISTS, SEQUENCE_NOT_EXISTS, TABLE_NOT_EXISTS, TABLE_NOT_CONFIGURED, COLUMN_NOT_CONFIGURED, CHECK_CONSTRAINT_NOT_EXISTS, FOREIGNKEY_INDEX_NOT_EXISTS, INDEX_NOT_EXISTS
	}

	private static final Map<String, Map<String, String>> commom_fields = new HashMap<String, Map<String, String>>();
	static {
		Map<String, String> property = new HashMap<String, String>();
		property.put("type", "num");
		property.put("required", "true");
		property.put(Constants.name, "ID_USUARIO_CADASTRO");
		commom_fields.put("ID_USUARIO_CADASTRO", property);

		property = new HashMap<String, String>();
		property.put("type", "date");
		property.put("required", "true");
		property.put(Constants.name, "DATA_CADASTRO");
		commom_fields.put("DATA_CADASTRO", property);

		property = new HashMap<String, String>();
		property.put("type", "date");
		property.put(Constants.name, "ULTIMA_MODIFICACAO");
		commom_fields.put("ULTIMA_MODIFICACAO", property);
	}
	private final InterfaceDialeto dialect;
	private List<Map<String, Object>> incompatibilities;
	private final Metadata metadata;
	private final JdbcTemplate jdbcTemplate;

	private final ParseXmlService parse;

	private final Usuario user;

	public DAOComparator(final Metadata metadata, final InterfaceDialeto dialect, final JdbcTemplate jdbcTemplate,
			final ParseXmlService parse, final Usuario user) {

		this.metadata = metadata;
		this.dialect = dialect;
		this.jdbcTemplate = jdbcTemplate;
		this.parse = parse;
		this.user = user;
	}

	private void add(final String tableName, final PROBLEM_TYPE error) {
		add(tableName, null, error);
	}

	private void add(final String tableName, final String objectName, final PROBLEM_TYPE error) {
		add(tableName, objectName, error, null);
	}

	private void add(final String tableName, final String objectName, final PROBLEM_TYPE columnType,
			final Map<String, String> property, final Map<String, String> column) {

		final Map<String, Object> data = add(tableName, objectName, columnType, getConfigValue(property));

		if (column != null) {
			data.put("VALUE_FOUND", getValueFound(column));
		}
	}

	private Map<String, Object> add(final String tableName, final String objectName, final PROBLEM_TYPE error,
			final String property) {

		final Map<String, Object> data = new HashMap<String, Object>();

		data.put("TABLE", tableName);
		data.put("ERROR_CODE", error.name());

		if (objectName != null) {
			data.put("OBJECT", objectName);
		}

		if (property != null) {
			data.put("CONFIG_VALUE", property);
		}
		data.put("ID", data.hashCode());
		this.incompatibilities.add(data);
		return data;
	}

	private void compareAutoIncrement(final String tableName, final Map<String, String> property,
			final Map<String, String> column) {

		final String columnTypeName = column.get("TYPE_NAME");
		final boolean hasIdentifier = columnTypeName.contains(dialect.getAutoIncrementIdentifier());

		if (!hasIdentifier && property.get("type").equals("num")) {
			add(tableName, property.get(Constants.name), PROBLEM_TYPE.AUTOINCREMENT_NOT_EXISTS);
		}
	}

	private void compareColumn(final String tableName, final Map<String, String> property,
			final Map<String, Map<String, Map<String, String>>> table, final List<Map<String, Object>> constraints) {

		final String columnName = property.get(Constants.name);
		final Map<String, String> column = table.get("columns").get(columnName);

		if (column == null) {
			add(tableName, columnName, PROBLEM_TYPE.COLUMN_NOT_EXISTS);

		} else {
			if (!property.get("type").equals("array"))
				compareColumnType(tableName, property, column);

			if (DAOUtil.isPrimaryKey(property)) {
				comparePrimaryKey(tableName, columnName, table.get("primaryKeys"));

				if (dialect.isGeneratedKeyBySequence()) {

					if (property.get("type").equals("num") && property.get("relational") == null) {
						compareSequence(tableName, columnName);
					}
				} else {
					if (property.get("type").equals("num") && property.get("relational") == null) {
						compareAutoIncrement(tableName, property, column);
					}
				}

			} else {
				compareColumnConstraints(tableName, property, column, constraints);
			}
		}
	}

	private void compareColumnConstraints(final String tableName, final Map<String, String> property,
			final Map<String, String> column, final List<Map<String, Object>> constraints) {

		final String columnName = property.get(Constants.name);
		final String type = property.get("type");

		if (type.equals("bit")) {
			final String constraintName = dialect.getConstraintName(tableName, columnName, CONSTRAINT_TYPE.CHECK);

			if (!hasConstraint(constraintName, constraints)) {
				add(tableName, property.get(Constants.name), PROBLEM_TYPE.CHECK_CONSTRAINT_NOT_EXISTS, property,
						column);

			}
			if (DAOUtil.isNullable(property)) {
				compareNullable(tableName, property, column);

			} else if (isColumnNullable(column)) {
				add(tableName, property.get(Constants.name), PROBLEM_TYPE.COLUMN_NULL, property, column);
			}

		} else {
			compareNullable(tableName, property, column);
		}
	}

	private void compareColumnLength(final String tableName, final Map<String, String> property,
			final Map<String, String> column) {

		String propertylength = property.get("maxlength");

		if (propertylength == null) {
			propertylength = dialect.getDefaultLength(property.get("type"));
		}

		if (propertylength != null) {
			final String columnLength = column.get("COLUMN_SIZE");

			if (!propertylength.equals(columnLength)) {
				add(tableName, property.get(Constants.name), PROBLEM_TYPE.COLUMN_LENGTH, property, column);

			} else if (property.get("type").equals("money")) {
				compareColumnPrecision(tableName, property, column);
			}
		}
	}

	private void compareColumnPrecision(final String tableName, final Map<String, String> property,
			final Map<String, String> column) {

		String propertyPrecision = property.get("decimalPrecision");
		final String columnPrecision = column.get("DECIMAL_DIGITS");

		if (propertyPrecision == null) {
			propertyPrecision = "2";
		}

		if (!propertyPrecision.equals(columnPrecision)) {
			add(tableName, property.get(Constants.name), PROBLEM_TYPE.COLUMN_PRECISION, property, column);
		}
	}

	private void compareColumnType(final String tableName, final Map<String, String> property,
			final Map<String, String> column) {

		final String propertyType = property.get("type");
		final String columnTypeName = column.get("TYPE_NAME");
		final boolean limitedLength = property.get("maxlength") != null;

		if (!this.dialect.compatibleTypes(propertyType, limitedLength, columnTypeName)) {
			add(tableName, property.get(Constants.name), PROBLEM_TYPE.COLUMN_TYPE, property, column);

		} else {
			compareColumnLength(tableName, property, column);
		}
	}

	private void compareDetailTable(final Map<String, String> bean, final Collection<Map<String, String>> beans,
			final List<Map<String, Object>> indexes) {

		final String masterId = bean.get("master");
		final String tableName = bean.get("table");

		final Map<String, String> master = DAOUtil.findBeanById(masterId, beans);

		if (master != null) {
			final String masterTableName = master.get("table");
			final String masterPrimaryKey = DAOUtil.getPrimaryKeyName(masterTableName, parse, user);

			final Map<String, Map<String, Map<String, String>>> table = this.metadata.getTables().get(tableName);
			final Map<String, String> column = table.get("columns").get(masterPrimaryKey);

			if (isColumnNullable(column)) {
				add(tableName, masterPrimaryKey, PROBLEM_TYPE.COLUMN_NULL);

			} else {

				if (hasForeignKey(table, masterTableName, masterPrimaryKey, tableName, masterPrimaryKey)) {

					compareIndex(tableName, masterPrimaryKey, indexes, CONSTRAINT_TYPE.FOREIGNKEY_INDEX);

				} else {
					add(tableName, masterTableName, PROBLEM_TYPE.FOREIGN_KEY_NOT_EXISTS, masterPrimaryKey);
				}
			}
		}
	}

	private void compareFilters(final String tableName, final Collection<String> filters,
			final List<Map<String, Object>> indexes) {

		List<String> columnsName;
		final Set<String> indexColumns = new HashSet<String>();

		for (final String filter : filters) {
			columnsName = DAOUtil.getColumnsNameFromFilter(filter);

			Collections.sort(columnsName);

			final StringBuffer sb = new StringBuffer();

			for (int i = 0; i < columnsName.size(); i++) {

				if (i != 0) {
					sb.append(", ");
				}
				sb.append(columnsName.get(i));
			}
			indexColumns.add(sb.toString());
		}

		for (final String indexColumn : indexColumns) {
			if (!commom_fields.containsKey(indexColumn)) {
				Map property = DAOUtil.getProperty(user, parse, tableName, indexColumn.trim());
				if (property != null && property.get("name") != null)
					compareIndex(tableName, indexColumn, indexes, CONSTRAINT_TYPE.INDEX);
			}
		}
	}

	private void compareForeignKey(final String tableName, final Map<String, Map<String, Map<String, String>>> table,
			final Map<String, String> property, final List<Map<String, Object>> indexes) {

		final String serviceName = property.get("service");
		final String columnName = property.get("name");
		final String primaryKeyName = property.get("key");
		final String serviceTable = getServiceTable(serviceName);

		if (hasForeignKey(table, serviceTable, primaryKeyName, tableName, columnName)) {

			compareIndex(tableName, columnName, indexes, CONSTRAINT_TYPE.FOREIGNKEY_INDEX);

		} else {
			add(tableName, serviceTable, PROBLEM_TYPE.FOREIGN_KEY_NOT_EXISTS, columnName);
		}
	}

	private void compareIndex(final String tableName, final String columnName, final List<Map<String, Object>> indexes,
			final CONSTRAINT_TYPE type) {

		String[] columnList = columnName.split(",");

		for (int i = 0; i < columnList.length; i++) {
			Map property = DAOUtil.getProperty(user, parse, tableName, columnList[i].trim());
			if (property.get("type") != null && property.get("type").equals("array"))
				return;
		}

		final String columnJoin = columnName.replaceAll(",[ ]*", "_");

		final String indexName = dialect.getConstraintName(tableName, columnJoin, type);

		final PROBLEM_TYPE problemType = type == CONSTRAINT_TYPE.FOREIGNKEY_INDEX
				? PROBLEM_TYPE.FOREIGNKEY_INDEX_NOT_EXISTS : PROBLEM_TYPE.INDEX_NOT_EXISTS;

		if (!hasIndex(indexName, indexes)) {
			add(tableName, columnName, problemType);
		}
	}

	private void compareIndexesFromRelationalTable() {
		String tableName;
		final Map<String, Collection<String>> filters = DAOUtil.getFiltersFromRelationalTables(user, parse);

		List<Map<String, Object>> indexes;

		for (final Map.Entry<String, Collection<String>> entry : filters.entrySet()) {
			tableName = entry.getKey();
			indexes = getIndexes(tableName);
			compareFilters(tableName, entry.getValue(), indexes);
		}
	}

	private void compareNullable(final String tableName, final Map<String, String> property,
			final Map<String, String> column) {

		final boolean propertyNullable = DAOUtil.isNullable(property);
		final boolean columnNullable = isColumnNullable(column);

		if (propertyNullable && !columnNullable) {
			add(tableName, property.get(Constants.name), PROBLEM_TYPE.COLUMN_NOT_NULL);
		} else if (!propertyNullable && columnNullable) {
			add(tableName, property.get(Constants.name), PROBLEM_TYPE.COLUMN_NULL);
		}
	}

	private void comparePrimaryKey(final String tableName, final String columnName,
			final Map<String, Map<String, String>> primaryKeys) {

		if (primaryKeys.get(columnName) == null) {
			add(tableName, columnName, PROBLEM_TYPE.PRIMARY_NOT_EXISTS);
		}
	}

	private void comparePrimaryKey(final String tableName, final String columnName, final String secoundColumnName,
			final Map<String, Map<String, String>> primaryKeys) {

		if (primaryKeys.get(columnName) == null || primaryKeys.get(secoundColumnName) == null) {

			add(tableName, columnName + ", " + secoundColumnName, PROBLEM_TYPE.PRIMARY_NOT_EXISTS);
		}
	}

	private void compareRelationalColumns(final String relationalName, final Map<String, String> property,
			Map primarykey, List<Map<String, Object>> list) {

		final Map<String, Map<String, Map<String, String>>> relationalTable = this.metadata.getTables()
				.get(relationalName);

		final Map<String, Object> columns = user.getUtil().JSONtoMap(property.get("columns"));

		Map<String, String> column;

		for (final String columnName : columns.keySet()) {
			column = relationalTable.get("columns").get(columnName);

			if (column == null) {
				add(relationalName, columnName, PROBLEM_TYPE.COLUMN_NOT_EXISTS);

			}
		}

		final Map<String, Map<String, String>> rCol = relationalTable.get("columns");
		for (final String columnName : rCol.keySet()) {
			column = relationalTable.get("columns").get(columnName);

			if (property.get(Constants.name).equals(columnName)) {
				Map c = new HashMap(property);
				c.put("required", "true");
				c.put("relational", "true");
				if (columns.get(columnName) != null)
					c.putAll((Map<? extends String, ? extends String>) columns.get(columnName));
				compareColumn(relationalName, c, relationalTable, list);
			}

			if (primarykey.get(Constants.name).equals(columnName)) {
				Map c = new HashMap(primarykey);
				c.put("required", "true");
				c.put("relational", "true");

				if (primarykey.get(columnName) != null)
					c.putAll((Map<? extends String, ? extends String>) columns.get(columnName));

				primarykey.remove("primaryKey");
				compareColumn(relationalName, c, relationalTable, list);
			}

			if (columns.get(columnName) != null) {
				Map c = (Map<String, String>) columns.get(columnName);
				c.put(Constants.name, columnName);
				if (c.get("type") == null)
					c.put("type", "num");
				c.put("relational", "true");
				compareColumn(relationalName, c, relationalTable, list);
			}

		}
	}

	private void compareRelationalForeignKey(final String relationalName,
			final Map<String, Map<String, Map<String, String>>> relationalTable, final String relationalColumnName,
			final String tableName, final String columnName, final boolean checkIndex) {

		if (relationalTable.get("columns").get(columnName) == null) {
			add(relationalName, columnName, PROBLEM_TYPE.COLUMN_NOT_EXISTS);

		} else if (!hasForeignKey(relationalTable, tableName, relationalColumnName, relationalName, columnName)) {

			add(relationalName, tableName, PROBLEM_TYPE.FOREIGN_KEY_NOT_EXISTS, columnName);

		} else if (checkIndex) {
			final List<Map<String, Object>> indexes = getIndexes(relationalName);

			compareIndex(relationalName, columnName, indexes, CONSTRAINT_TYPE.FOREIGNKEY_INDEX);
		}

	}

	private void compareRelationalTable(final String tableName, final Map<String, String> property,
			final Map<String, String> primaryKey) {

		final String relationalName = property.get("relational_table");
		final Map<String, Map<String, Map<String, String>>> relationalTable = this.metadata.getTables()
				.get(relationalName);

		if (relationalTable == null) {
			add(relationalName, PROBLEM_TYPE.TABLE_NOT_EXISTS);

		} else {

			final String primaryKeyName = primaryKey.get("name");

			compareRelationalForeignKey(relationalName, relationalTable, primaryKeyName, tableName, primaryKeyName,
					true);

			final String serviceTable = getServiceTable(property.get("service"));

			final String propertyKey = property.get("key");

			if (serviceTable != null && property.get("serviceMethod") == null) {

				compareRelationalForeignKey(relationalName, relationalTable, propertyKey, serviceTable, propertyKey,
						false);

				if (DAOUtil.hasColumns(property)) {
					compareRelationalColumns(relationalName, property,
							DAOUtil.getProperty(user, parse, serviceTable, propertyKey),
							getConstraints(relationalName));
				}
			}
			comparePrimaryKey(relationalName, primaryKeyName, propertyKey, relationalTable.get("primaryKeys"));
		}
	}

	private void compareSequence(final String tableName, final String primaryKeyName) {

		final String sequenceName = dialect.getSequenceName(tableName);

		if (!metadata.getSequences().contains(sequenceName)) {
			add(tableName, sequenceName, PROBLEM_TYPE.SEQUENCE_NOT_EXISTS, primaryKeyName);
		}
	}

	private void compareTable(final String tableName, final Collection<Map<String, String>> properties,
			final List<Map<String, Object>> indexes) {

		final Map<String, Map<String, Map<String, String>>> table = this.metadata.getTables().get(tableName);

		final Map<String, String> primaryKey = DAOUtil.findPrimaryKey(properties);

		final List<Map<String, Object>> constraints = getConstraints(tableName);

		boolean hasInsertDate = false, hasLastModification = false, hasUserId = false;

		for (final Map<String, String> property : properties) {

			if (DAOUtil.isRelationalTable(property)) {
				compareRelationalTable(tableName, property, primaryKey);

			} else if (table != null && !DAOUtil.isIgnoredProperty(property)) {
				compareColumn(tableName, property, table, constraints);

				if (DAOUtil.isForeignKey(property)) {
					compareForeignKey(tableName, table, property, indexes);
				}

				if (property.get("foneticSearch") != null) {
					Map p = new HashMap(property);
					// Evitar que crie *_FONETIC_FONETIC
					if (!p.get("name").toString().endsWith("_FONETIC")) {
						p.put(Constants.name, user.getUtil().shortenIdentifier(property.get("name") + "_FONETIC"));
						compareColumn(tableName, p, table, null);
					}

				}

				if (property.get("name") != null && property.get("name").equals("DATA_CADASTRO")) {
					hasInsertDate = true;

				} else if (property.get("name") != null && property.get("name").equals("ULTIMA_MODIFICACAO")) {
					hasLastModification = true;

				} else if (property.get("name") != null && property.get("name").equals("ID_USUARIO_CADASTRO")) {
					hasUserId = true;
				}
			}
		}

		if (!hasInsertDate) {
			compareColumn(tableName, commom_fields.get("DATA_CADASTRO"), table, null);
		}

		if (!hasLastModification) {
			compareColumn(tableName, commom_fields.get("ULTIMA_MODIFICACAO"), table, null);
		}

		if (!hasUserId) {
			compareColumn(tableName, commom_fields.get("ID_USUARIO_CADASTRO"), table, null);

			if (hasForeignKey(table, "M_USUARIO", Constants.subject_key, tableName, "ID_USUARIO_CADASTRO")) {

				compareIndex(tableName, "ID_USUARIO_CADASTRO", indexes, CONSTRAINT_TYPE.FOREIGNKEY_INDEX);

			} else {

				add(tableName, "M_USUARIO", PROBLEM_TYPE.FOREIGN_KEY_NOT_EXISTS, "ID_USUARIO_CADASTRO");
			}
		}
	}

	private void getColumnsDiff(final List<Map<String, String>> diff, final Map<String, Map<String, String>> dbTable,
			final String tableName) {

		final Collection<Map<String, String>> properties = DAOUtil.getProperties(user, parse, tableName);
		Map<String, String> data;

		for (final String columnName : dbTable.keySet()) {

			if (!isCommonField(columnName) && !isConfiguredColumn(properties, columnName)) {

				data = new HashMap<String, String>();

				data.put("TABLE", tableName);
				data.put("ERROR_CODE", PROBLEM_TYPE.COLUMN_NOT_CONFIGURED.name());
				data.put("OBJECT", columnName);
				data.put("ID", "" + data.hashCode());
				diff.add(data);
			}
		}
	}

	private String getConfigValue(final Map<String, String> property) {

		String value = property.get("type").equals("array") ? "num" : property.get("type");

		String pMaxLength = property.get(property.get("maxlength"));
		if (pMaxLength == null) {
			pMaxLength = dialect.getDefaultLength(property.get("type"));
		}

		if (pMaxLength != null) {
			value += " (" + pMaxLength;

			if (property.get("decimalPrecision") != null) {
				value += "," + property.get("decimalPrecision");

			} else if (property.get("type").equals("money")) {
				value += ",2";
			}
			value += ")";
		}
		return value;
	}

	private List<Map<String, Object>> getConstraints(final String tableName) {
		final List<String> tableNames = new ArrayList<String>();
		tableNames.add(tableName);

		return jdbcTemplate.queryForList(dialect.getAllConstraints(tableNames));
	}

	public Map<String, Map<String, Map<String, Map<String, String>>>> getTables() {
		return metadata.getTables();

	}

	public List<Map<String, String>> getDiff() {

		final List<Map<String, String>> diff = new ArrayList<Map<String, String>>();

		final Collection<String> confTables = DAOUtil.getTables(user, parse);
		final Collection<String> confRelTables = getRelationalTables();

		final Map<String, Map<String, Map<String, Map<String, String>>>> dbTables = metadata.getTables();

		for (final String tableName : dbTables.keySet()) {

			if (!tableName.equals("M_LOG_BANCODADOS")) {
				Map<String, Map<String, String>> columns;

				if (confTables.contains(tableName)) {
					columns = dbTables.get(tableName).get("columns");
					getColumnsDiff(diff, columns, tableName);

				} else if (confRelTables.contains(tableName)) {
					columns = dbTables.get(tableName).get("columns");
					getRelationalColumnsDiff(diff, columns, tableName);

				} else {
					final Map<String, String> data = new HashMap<String, String>();

					data.put("TABLE", tableName);
					data.put("ERROR_CODE", PROBLEM_TYPE.TABLE_NOT_CONFIGURED.name());

					diff.add(data);
				}
			}
		}
		return diff;
	}

	public List<Map<String, Object>> getIncompatibilities() {

		this.incompatibilities = new ArrayList<Map<String, Object>>() {
			@Override
			// Evitar que duplique erros no setup
			public boolean add(Map<String, Object> o) {
				if (!super.contains(o)) {
					super.add(o);
					return true;
				}
				return false;
			}
		};

		final Collection<Map<String, String>> beans = DAOUtil.getBeans(user, parse);

		final List<String> tableLoaded = new ArrayList<String>();

		String tableName;

		for (final Map<String, String> bean : beans) {

			tableName = bean.get("table");

			if (!tableLoaded.contains(tableName)) {
				tableLoaded.add(tableName);
				getIncompatibilitiesFromTable(beans, tableName, bean);
			}
		}
		// compareIndexesFromRelationalTable();
		return this.incompatibilities;
	}

	private void getIncompatibilitiesFromTable(final Collection<Map<String, String>> beans, final String tableName,
			final Map<String, String> bean) {
		final Map<String, Map<String, Map<String, String>>> tableMetadata = this.metadata.getTables().get(tableName);

		if (tableMetadata == null) {
			add(tableName, PROBLEM_TYPE.TABLE_NOT_EXISTS);

		} else {
			final List<Map<String, Object>> indexes = getIndexes(tableName);

			if (bean.get("type").equals("D")) {
				compareDetailTable(bean, beans, indexes);
			}
			compareTable(tableName, DAOUtil.getProperties(user, parse, tableName), indexes);

			final Collection<String> filters = DAOUtil.getFilters(tableName, user, parse);
			compareFilters(tableName, filters, indexes);

			String fn = getMainFieldName(tableName);
			if (fn != null && !fn.startsWith("ID_")) {
				compareIndex(tableName, fn, indexes, CONSTRAINT_TYPE.INDEX);
			}

		}
	}

	private List<Map<String, Object>> getIndexes(final String tableName) {
		final List<String> tableNames = new ArrayList<String>();
		tableNames.add(tableName);

		return jdbcTemplate.queryForList(dialect.getAllIndexes(tableNames));
	}

	private String getPrimaryKeyName(final String tableName) {
		return parse.getValueFromXQL("/systems/system[@name='" + user.getSchema()
				+ "']/module/bean[not(@extends) and @table='" + tableName + "']/property[@primaryKey='true']", "name",
				user, true, false);
	}

	private String getMainFieldName(final String tableName) {
		return parse.getValueFromXQL("/systems/system[@name='" + user.getSchema()
				+ "']/module/bean[not(@extends) and @table='" + tableName + "']/property[@mainField='true']", "name",
				user, true, false);
	}

	private void getRelationalColumnsDiff(final List<Map<String, String>> diff,
			final Map<String, Map<String, String>> columns, final String tableName) {

		final List<Map<String, String>> properties = getRelationalProperties(tableName);

		Map<String, String> data;

		for (final String columnName : columns.keySet()) {

			if (!isRelationalConfiguredColumn(columnName, properties)) {

				data = new HashMap<String, String>();

				data.put("TABLE", tableName);
				data.put("ERROR_CODE", PROBLEM_TYPE.COLUMN_NOT_CONFIGURED.name());
				data.put("OBJECT", columnName);
				data.put("ID", "" + data.hashCode());
				diff.add(data);
			}
		}
	}

	private List<Map<String, String>> getRelationalProperties(final String tableName) {

		final String system = user.getSchema();

		final String xpath = "/systems/system[@name='" + system + "']/module/bean[not(@extends)]";

		final String xpathProperty = "/property[not(@volatile) and @component!='fake' and @relational_table='"
				+ tableName + "']";

		List l = (List<Map<String, String>>) parse.getListValuesFromXQL(
				new String[] { xpath + xpathProperty, xpath + "/method/" + xpathProperty }, "order", user, true, false);
		List fonetic = new ArrayList();
		for (Iterator iterator = l.iterator(); iterator.hasNext();) {
			Map object = (Map) iterator.next();
			if (object.get("foneticSearch") != null) {
				Map t = new HashMap(object);
				// Evitar que crie *_FONETIC_FONETIC
				if (t.get("name").toString().endsWith("_FONETIC")) {
					t.put("name", t.get("name") + "_FONETIC");
					fonetic.add(t);
				}
			}

		}

		l.addAll(fonetic);
		return l;

	}

	private Collection<String> getRelationalTables() {

		final String system = user.getSchema();

		final Collection<Map<String, String>> properties = parse
				.getListValuesFromXQL(
						new String[] {
								"/systems/system[@name='" + system + "']/module/bean/property[@relational_table]",
								"relational_table",
								"/systems/system[@name='" + system
										+ "']/module/bean/method/property[@relational_table]",
								"relational_table" },
						"order", user, true, false);

		final List<String> tables = new ArrayList<String>();

		for (final Map<String, String> property : properties) {

			if (!tables.contains(property.get("relational_table"))) {
				tables.add(property.get("relational_table"));
			}
		}

		return tables;
	}

	private String getServiceTable(final String serviceName) {
		return DAOUtil.getServiceTable(serviceName, user, parse);
	}

	private String getValueFound(final Map<String, String> column) {

		String value = column.get("TYPE_NAME") == null ? "" : column.get("TYPE_NAME");

		if (column.get("COLUMN_SIZE") != null) {
			value += " (" + column.get("COLUMN_SIZE");

			if (column.get("DECIMAL_DIGITS") != null && !column.get("DECIMAL_DIGITS").equals("0")) {
				value += "," + column.get("DECIMAL_DIGITS");
			}
			value += ")";
		}
		return value;
	}

	private boolean hasConstraint(final String constraintName, final List<Map<String, Object>> constraints) {

		String outherConstraintName, constraintType;

		for (final Map<String, Object> constraint : constraints) {
			constraintType = (String) constraint.get("CONSTRAINT_TYPE");

			if (dialect.isCheckConstraintType(constraintType)) {
				outherConstraintName = (String) constraint.get("CONSTRAINT_NAME");

				if (outherConstraintName.equals(constraintName)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean hasForeignKey(final Map<String, Map<String, Map<String, String>>> relationalTable,
			final String tableName, final String primaryKeyName, final String relationalName, final String columnName) {

		Map<String, String> foreignKey;
		String pkTableName, fkTableName, pkColumnName, fkColumnName;

		for (final Map.Entry<String, Map<String, String>> entry : relationalTable.get("foreignKeys").entrySet()) {
			foreignKey = entry.getValue();

			pkTableName = foreignKey.get("PKTABLE_NAME");
			fkTableName = foreignKey.get("FKTABLE_NAME");

			if (pkTableName.equals(tableName) && fkTableName.equals(relationalName)) {

				pkColumnName = foreignKey.get("PKCOLUMN_NAME");
				fkColumnName = foreignKey.get("FKCOLUMN_NAME");

				if (pkColumnName.equals(primaryKeyName) && fkColumnName.equals(columnName)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean hasIndex(final String indexName, final List<Map<String, Object>> indexes) {

		for (final Map<String, Object> index : indexes) {

			if (index.get("INDEX_NAME").equals(indexName)) {
				return true;
			}
		}
		return false;
	}

	private boolean isColumnNullable(final Map<String, String> column) {
		return column != null && column.get("NULLABLE") != null && column.get("NULLABLE").equals("1");
	}

	private boolean isCommonField(final String columnName) {
		return columnName.equals("ID_USUARIO_CADASTRO") || columnName.equals("DATA_CADASTRO")
				|| columnName.equals("ULTIMA_MODIFICACAO");
	}

	private boolean isConfiguredColumn(final Collection<Map<String, String>> properties, final String columnName) {

		for (final Map<String, String> property : properties) {

			if (property.get("name").equals(columnName)) {

				if (!DAOUtil.isIgnoredProperty(property)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isRelationalConfiguredColumn(final String columnName, final List<Map<String, String>> properties) {

		String primaryKeyName;
		Map<String, Object> columns;

		for (final Map<String, String> property : properties) {

			primaryKeyName = getPrimaryKeyName(property.get("table"));

			if (primaryKeyName.equals(columnName) || property.get("key").equals(columnName)
					|| property.get(Constants.name).equals(columnName)) {

				if (!DAOUtil.isIgnoredProperty(property)) {
					return true;
				}
			}

			if (DAOUtil.hasColumns(property)) {
				columns = user.getUtil().JSONtoMap(property.get("columns"));

				if (columns.get(columnName) != null) {
					return true;
				}
			}
		}
		return false;
	}
};
