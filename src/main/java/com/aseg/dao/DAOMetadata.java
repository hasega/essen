package com.aseg.dao;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.support.DatabaseMetaDataCallback;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.util.StringUtils;

public class DAOMetadata implements DatabaseMetaDataCallback {

	private Map<String, Map<String, String>> processColumns(final DatabaseMetaData databaseMetaData,
			final String tableName) throws SQLException {
		return processMedata(databaseMetaData.getColumns(databaseMetaData.getUserName(), "%", tableName, "%"));
	}

	private Map<String, Map<String, String>> processForeignKey(final DatabaseMetaData metaData, final String tableName)
			throws SQLException {
		return processMedata(metaData.getImportedKeys(metaData.getUserName(), null, tableName), "FK_NAME");
	}

	private Map<String, Map<String, String>> processMedata(final ResultSet resultSet) throws SQLException {
		return processMedata(resultSet, "COLUMN_NAME");
	}

	private Map<String, Map<String, String>> processMedata(final ResultSet resultSet, final String keyColumn)
			throws SQLException {

		final Map<String, Map<String, String>> metadatas = new HashMap<String, Map<String, String>>();
		Map<String, String> metadata;
		String columnName;
		String name;
		Object value;

		while (resultSet.next()) {
			name = "";
			metadata = new HashMap<String, String>();

			for (int i = 1; i < resultSet.getMetaData().getColumnCount(); i++) {
				columnName = resultSet.getMetaData().getColumnName(i).toUpperCase();

				if (columnName.equals(keyColumn)) {
					name = ((String) resultSet.getObject(i)).toUpperCase();
				}
				value = resultSet.getObject(i);

				if (value instanceof String) {
					metadata.put(columnName, ((String) value).toUpperCase());

				} else if (value != null) {
					metadata.put(columnName, value.toString());
				}
			}
			metadatas.put(name, metadata);
		}
		resultSet.close();
		return metadatas;
	}

	@Override
	public Metadata processMetaData(final DatabaseMetaData databaseMetaData)
			throws SQLException, MetaDataAccessException {

		final Metadata metadata = new Metadata();
		metadata.setSequences(processSequences(databaseMetaData));
		metadata.setTables(processTables(databaseMetaData));

		return metadata;
	}

	private Map<String, Map<String, String>> processPrimaryKey(final DatabaseMetaData metaData, final String tableName)
			throws SQLException {
		return processMedata(metaData.getPrimaryKeys(metaData.getUserName(), null, tableName));
	}

	private List<String> processSequences(final DatabaseMetaData databaseMetaData) throws SQLException {

		final List<String> sequences = new ArrayList<String>();

		final ResultSet resultSet = databaseMetaData.getTables(databaseMetaData.getUserName(), null, null,
				new String[] { "SEQUENCE" });

		while (resultSet.next()) {
			sequences.add(resultSet.getString(3));
		}
		resultSet.close();
		return sequences;
	}

	private Map<String, Map<String, Map<String, Map<String, String>>>> processTables(
			final DatabaseMetaData databaseMetaData) throws SQLException {

		final Map<String, Map<String, Map<String, Map<String, String>>>> tables = new HashMap<String, Map<String, Map<String, Map<String, String>>>>();
		Map<String, Map<String, Map<String, String>>> items = null;
		String tableName;

		final String userName = databaseMetaData.getUserName();
		final ResultSet resultSet = databaseMetaData.getTables(userName, null, null, new String[] { "TABLE" });

		final boolean isOracle = databaseMetaData.getDatabaseProductName().equalsIgnoreCase("oracle");

		boolean fromCatalog;
		String catalog;

		while (resultSet.next()) {
			tableName = resultSet.getString(3);

			fromCatalog = true;

			if (isOracle) {
				catalog = resultSet.getString(2);
				fromCatalog = catalog.equalsIgnoreCase(userName);
			}

			// ORACLE cria tabelas "recyclebin" que foram dropadas
			// Os nome dessas tabelas comecam com "BIN$"
			if (!tableName.contains("$") && fromCatalog) {
				items = new HashMap<String, Map<String, Map<String, String>>>();

				Map<String, Map<String, String>> c = processColumns(databaseMetaData, tableName);

				items.put("columns", c);
				items.put("primaryKeys", processPrimaryKey(databaseMetaData, tableName));
				items.put("foreignKeys", processForeignKey(databaseMetaData, tableName));
				String[] fr = c.keySet().toArray(new String[] {});
				String f = StringUtils.arrayToCommaDelimitedString(fr);
				Map st = new HashMap<String, String>();
				for (int i = 0; i < fr.length; i++) {
					fr[i] = "?";
				}
				st.put("select", "select " + f + " from " + tableName);
				st.put("insert", "insert into " + tableName + "(" + f + ")" + " values " + "("
						+ StringUtils.arrayToCommaDelimitedString(fr) + ")");
				st.put("fields", f);
				items.put("statements", st);
				tables.put(tableName.toUpperCase(), items);
			}
		}
		resultSet.close();
		return tables;
	}
}
