package com.aseg.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import com.aseg.config.Constants;
import com.aseg.exceptions.BusinessException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.util.ConnectionManager;
import com.aseg.util.service.ParseXml;
import com.aseg.util.sql.InterfaceDialeto;
import com.aseg.util.sql.InterfaceDialeto.CONSTRAINT_TYPE;

public class DBCreator extends DAOImpl {

	private final ParseXml parseXml;
	private final InterfaceDialeto dialect;
	private final JdbcTemplate jdbcTemplate;
	private final Usuario user;

	private Collection<Map<String, String>> beans;

	private final Map<String, Map<String, Map<String, String>>> tables;
	private final Map<String, List<String>> primaryKeys;
	private final Map<String, String> sequences;
	private final Map<String, Map<String, String>> foreignKeys;
	private final Map<String, List<String>> booleanConstraints;
	private final Map<String, Map<String, String>> tableIndexes;
	private final Map statements;

	private final boolean onlyLogDDL;

	private static final String[] CONSTRAINT_OPTION_BOOLEAN = { "T", "F" };

	public DBCreator(final ParseXml parseXml, final InterfaceDialeto dialeto, final JdbcTemplate jdbcTemplate,
			final boolean onlyLogDDL, final Usuario user) {

		this.parseXml = parseXml;
		this.dialect = dialeto;
		this.jdbcTemplate = jdbcTemplate;
		this.onlyLogDDL = onlyLogDDL;
		this.user = user;

		this.tables = new HashMap<String, Map<String, Map<String, String>>>();
		this.primaryKeys = new HashMap<String, List<String>>();
		this.sequences = new HashMap<String, String>();
		this.foreignKeys = new HashMap<String, Map<String, String>>();
		this.booleanConstraints = new HashMap<String, List<String>>();
		this.tableIndexes = new HashMap<String, Map<String, String>>();
		this.statements = new HashMap<String, Map<String, String>>();
	}

	private void addColumn(final String tableName, final String columnName, final Map<String, String> column) {

		final Map<String, Map<String, String>> table = getTable(tableName);
		final Map<String, String> oldColumn = table.get(columnName);

		if (oldColumn == null) {
			table.put(columnName, column);

		} else {

			// Se alguma vez essa campo foi definido como "nao nulo" e agora
			// nao e mais, entao nao deve ser "NOT NULL" no banco de
			// dados.
			if (!DAOUtil.isNullable(oldColumn) && DAOUtil.isNullable(column)) {
				oldColumn.put("required", Constants.false_str);
			}

		}
		if (column.get("foneticSearch") != null) {
			// Evitar que crie *_FONETIC_FONETIC
			if (!columnName.toString().endsWith("_FONETIC")) {
				table.put(columnName + "_FONETIC", column);
			}
		}
	}

	private void addCommonsFields(final String tableName) {
		final Map<String, Map<String, String>> table = getTable(tableName);

		Map<String, String> property = new HashMap<String, String>();
		property.put("type", "num");
		property.put("required", "true");
		table.put("ID_USUARIO_CADASTRO", property);

		property = new HashMap<String, String>();
		property.put("type", "date");
		property.put("required", "true");

		table.put("DATA_CADASTRO", property);

		property = new HashMap<String, String>();
		property.put("type", "date");
		table.put("ULTIMA_MODIFICACAO", property);

		addForeignKey(tableName, "ID_USUARIO_CADASTRO", "M_USUARIO");
	}

	private void addForeignKey(final String tableName, final String columnName, final String relationalTableName) {

		Map<String, String> foreignKey = foreignKeys.get(tableName);

		if (foreignKey == null) {
			foreignKey = new HashMap<String, String>();
			foreignKeys.put(tableName, foreignKey);
		}
		foreignKey.put(columnName, relationalTableName);
	}

	private void addPrimaryKeyColumn(final String tableName, final String foreignField, final String type) {

		final Map<String, String> field = new HashMap<String, String>();
		field.put("type", type);
		field.put("primaryKey", Constants.true_str);
		addColumn(tableName, foreignField, field);
	}

	private void addPrimaryKeyOnRelationalTable(final String relationalTable, final String foreignField,
			final String primaryKeyName) {
		List<String> fields = this.primaryKeys.get(relationalTable);

		if (fields == null) {
			fields = new ArrayList<String>();
			fields.add(foreignField);
			fields.add(primaryKeyName);
			this.primaryKeys.put(relationalTable, fields);

		} else {

			if (!fields.contains(foreignField)) {
				fields.add(foreignField);
			}

			if (!fields.contains(primaryKeyName)) {
				fields.add(primaryKeyName);
			}
		}
	}

	public void alterColumn(final String tableName, final String columnName) {
		loadSchema();

		final Map<String, String> field = getTable(tableName).get(columnName);

		try {
			executeDDL(dialect.alterColumn(tableName, columnName, field));
		} catch (final DataAccessException e) {
			final String temporaryColumn = "TMP_" + columnName;
			createColumn(tableName, temporaryColumn, field, true);

			try {
				executeDDL(dialect.copyColumn(tableName, columnName, temporaryColumn));
				renameColumn(tableName, columnName, "OLD_" + columnName);
				renameColumn(tableName, temporaryColumn, columnName);
				dropColumn(tableName, "OLD_" + columnName);

			} catch (final DataAccessException e2) {
				dropColumn(tableName, temporaryColumn);
				throw e2;
			}
		}
	}

	public void alterColumn(final String tableName, final String columnName, final boolean nullable) {

		loadSchema();

		final Map<String, Map<String, String>> table = this.tables.get(tableName);

		if (table == null) {
			throw new BusinessException("label.setup.bancodados.erro.tabelaNaoConfigurada");

		} else if (table.get(columnName) == null) {
			throw new BusinessException("label.setup.bancodados.erro.colunaNaoConfigurada");
		} else {
			executeDDL(dialect.alterColumn(tableName, columnName, table.get(columnName), nullable));
		}
	}

	private Map<String, String> columnFromProperty(final Map<String, String> property,
			final boolean isGeneratedKeyBySequence) {

		final Map<String, String> field = new HashMap<String, String>();
		final String type = property.get("type");
		if (type == null)
			return field;

		field.put("type", type);

		if (property.get("primaryKey") != null) {
			field.put("primaryKey", property.get("primaryKey"));

			if (!isGeneratedKeyBySequence) {
				field.put("autoIncrement", Constants.true_str);
			}
		}

		if (type.equals("bit")) {
			field.put("required", Constants.true_str);

		} else if (property.get("parent_node").equals("method")) {
			field.put("required", Constants.false_str);

		} else {
			field.put("required", property.get("required"));
		}

		if (property.get("foneticSearch") != null) {
			field.put("foneticSearch", property.get("foneticSearch"));
		}

		if (property.get("maxlength") != null) {
			field.put("length", property.get("maxlength"));
		}

		if (property.get("decimalPrecision") != null) {
			field.put("decimalPrecision", property.get("decimalPrecision"));
		}
		return field;
	}

	public void create() {
		LogUtil.debug("Criando banco de dados...", user);

		loadSchema();
		createTables();
		createPrimaryKeys();
		createForeignKeys();
		createConstraints();
		createIndexs();

		if (this.dialect.isGeneratedKeyBySequence()) {
			createSequences();
		}
	}

	public void createBooleanConstraint(final String tableName, final String fieldName) {
		createCheckConstraint(tableName, fieldName, CONSTRAINT_OPTION_BOOLEAN);
	}

	private void createCheckConstraint(final String tableName, final String fieldName, final String[] options) {
		executeDDL(this.dialect.createCheckConstraint(tableName, fieldName, options));
	}

	public void createColumn(final String tableName, final String columnName) {

		loadSchema();

		final Map<String, Map<String, String>> table = this.tables.get(tableName);

		if (table == null) {
			throw new BusinessException("label.setup.bancodados.erro.tabelaNaoConfigurada");

		} else if (table.get(columnName) == null) {
			throw new BusinessException("label.setup.bancodados.erro.colunaNaoConfigurada");
		} else {
			createColumn(tableName, columnName, table.get(columnName), true);
		}
	}

	private void createColumn(final String tableName, final String columnName, final Map<String, String> field,
			final boolean ignoreNotNull) {

		executeDDL(dialect.createColumn(tableName, columnName, field, ignoreNotNull));
	}

	private void createConstraints() {
		LogUtil.debug("Criando " + this.booleanConstraints.size() + " constraints...", user);

		String tableName;

		for (final Entry<String, List<String>> entry : this.booleanConstraints.entrySet()) {
			tableName = entry.getKey();

			for (final String fieldName : entry.getValue()) {
				createCheckConstraint(tableName, fieldName, CONSTRAINT_OPTION_BOOLEAN);
			}
		}
	}

	public void createForeignKey(final String relationalTable, final String tableName, final String fieldName) {

		executeDDL(this.dialect.createForeignKey(tableName, relationalTable, fieldName,
				DAOUtil.getPrimaryKeyName(tableName, parseXml, user)));
	}

	public void createForeignKeyIndex(final String tableName, final String fieldName) {
		final String constraintName = dialect.getConstraintName(tableName, fieldName, CONSTRAINT_TYPE.FOREIGNKEY_INDEX);
		executeDDL(this.dialect.createIndex(tableName, constraintName, fieldName));
	}

	private void createForeignKeys() {
		LogUtil.debug("Criando " + this.foreignKeys.size() + " chaves estrangeiras...", user);

		for (final Entry<String, Map<String, String>> entryForeignKeys : this.foreignKeys.entrySet()) {

			createForeignKeys(entryForeignKeys.getKey(), entryForeignKeys.getValue());
		}
	}

	private void createForeignKeys(final String relationalTable, final Map<String, String> foreignKeys) {

		String fieldName, tableName;

		for (final Entry<String, String> foreignKey : foreignKeys.entrySet()) {
			tableName = foreignKey.getValue();
			fieldName = foreignKey.getKey();
			createForeignKey(relationalTable, tableName, fieldName);
		}
	}

	public void createIndex(final String tableName, final String fieldName) {
		final String columnJoin = fieldName.replaceAll(",[ ]*", "_");
		final String constraintName = dialect.getConstraintName(tableName, columnJoin, CONSTRAINT_TYPE.INDEX);

		createIndex(tableName, fieldName, constraintName);
	}

	private void createIndex(final String tableName, final String fieldName, final String constraintName) {
		executeDDL(this.dialect.createIndex(tableName, constraintName, fieldName));
	}

	private void createIndexs() {
		LogUtil.debug("Criando " + this.tableIndexes.size() + " indices...", user);

		String tableName;

		for (final Entry<String, Map<String, String>> entry : this.tableIndexes.entrySet()) {
			tableName = entry.getKey();

			for (final Map.Entry<String, String> mapEntry : entry.getValue().entrySet()) {
				createIndex(tableName, mapEntry.getKey(), mapEntry.getValue());
			}
		}
	}

	public void createPrimaryKey(final String tableName) {

		loadSchema();

		createPrimaryKey(tableName, this.primaryKeys.get(tableName).toArray(new String[0]));
	}

	private void createPrimaryKey(final String tableName, final String[] fields) {
		executeDDL(this.dialect.createPrimaryKey(tableName, fields));
	}

	private void createPrimaryKeys() {
		LogUtil.debug("Criando " + this.primaryKeys.size() + " chaves primarias...", user);

		for (final Entry<String, List<String>> entry : this.primaryKeys.entrySet()) {
			createPrimaryKey(entry.getKey(), entry.getValue().toArray(new String[0]));
		}
	}

	public void createSequence(final String tableName, final String pkName, final long initial) {
		executeDDL(this.dialect.createSequence(tableName, pkName, initial));
		if (ConnectionManager.getInstance(this.user).getBancoConectado(this.user) == ConnectionManager.POSTGRESQL) {
			String c = " ALTER TABLE " + tableName + " ALTER COLUMN " + pkName + "   SET DEFAULT nextval('"
					+ getSequenceName(tableName) + "'::regclass);  ";
			executeSQL(c);
		}
	}

	public String getSequenceName(String tableName) {
		return this.dialect.getSequenceName(tableName);
	}

	public void createSequence(final String tableName, final String primaryKeyName) {

		loadSchema();

		if (this.tables.get(tableName) == null) {
			throw new BusinessException("label.setup.bancodados.erro.crieTabelaAntes");
		}
		final long max = jdbcTemplate.queryForObject("SELECT MAX(" + primaryKeyName + ") FROM " + tableName,
				Long.class);
		createSequence(tableName, primaryKeyName, max + 1);
	}

	private void createSequences() {
		LogUtil.debug("Criando " + this.sequences.size() + " sequencias...", user);

		for (final Entry<String, String> entry : this.sequences.entrySet()) {
			createSequence(entry.getKey(), null, 1);
		}
	}

	public void createTable(final String tableName) {

		loadSchema();

		if (this.tables.get(tableName) == null) {
			throw new BusinessException("label.setup.bancodados.erro.tabelaNaoConfigurada");
		} else {
			createTable(tableName, this.tables.get(tableName));
		}
	}

	private void createTable(final String tableName, final Map<String, Map<String, String>> fields) {

		executeDDL(dialect.createTable(tableName, fields));

	}

	private void createTables() {
		LogUtil.debug("Criando " + this.tables.size() + " tabelas...", user);

		for (final Entry<String, Map<String, Map<String, String>>> entry : this.tables.entrySet()) {
			try {
				createTable(entry.getKey(), entry.getValue());
			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

		}
	}

	public void dropColumn(final String tableName, final String columnName) {
		executeDDL(dialect.dropColumn(tableName, columnName));
	}

	public int dropConstraints(final List<String> tableNames) {

		final List<Map<String, Object>> constraints = jdbcTemplate.queryForList(dialect.getAllConstraints(tableNames));

		final int numConstraints = constraints.size();

		String constraintName, tableName, constraintType;
		final Map<String, String> primaryConstraints = new HashMap<String, String>();

		for (final Map<String, Object> constraint : constraints) {

			constraintName = (String) constraint.get("CONSTRAINT_NAME");
			tableName = (String) constraint.get("TABLE_NAME");
			constraintType = (String) constraint.get("CONSTRAINT_TYPE");

			if (dialect.isPrimaryKeyConstraintType(constraintType)) {
				primaryConstraints.put(constraintName, tableName);
			} else {
				executeDDL(dialect.dropConstraint(tableName, constraintName));
			}
		}

		// Restricoes de chave primaria deve ser removidas depois das
		// chaves
		// estrangeiras
		for (final String primaryConstraint : primaryConstraints.keySet()) {
			tableName = primaryConstraints.get(primaryConstraint);
			executeDDL(dialect.dropConstraint(tableName, primaryConstraint));
		}

		return numConstraints;
	}

	public int dropIndexes(final List<String> tableNames) {

		final List<Map<String, Object>> indexs = jdbcTemplate.queryForList(dialect.getAllIndexes(tableNames));

		final int numindexs = indexs.size();

		String indexName = null, tableName;
		final Map<String, String> primaryindexs = new HashMap<String, String>();

		for (final Map<String, Object> index : indexs) {

			indexName = (String) index.get("INDEX_NAME");
			tableName = (String) index.get("TABLE_NAME");
			// indexType = (String) index.get("INDEX_TYPE");
			try {
				executeDDL(dialect.dropIndex(tableName, indexName));
			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

		}

		// Restricoes de chave primaria deve ser removidas depois das
		// chaves
		// estrangeiras
		for (final String primaryindex : primaryindexs.keySet()) {
			tableName = primaryindexs.get(primaryindex);
			executeDDL(dialect.dropIndex(indexName, primaryindex));
		}

		return numindexs;

	}

	public void dropTable(final String tableName) {
		executeDDL(dialect.dropTable(tableName));
	}

	private void executeDDL(final String sql) {
		executeDDL(sql, false);
	}

	private void executeDDL(final String sql, final boolean byUser) {

		if (sql != null) {

			if (!this.onlyLogDDL) {

				jdbcTemplate.update(sql);

			}

			final Calendar cal = Calendar.getInstance();
			final Timestamp time = new Timestamp(cal.getTime().getTime());
			final String user = byUser ? "T" : "F";
			final String executado = this.onlyLogDDL ? "F" : "T";

			LogUtil.debug(sql, this.user);

			jdbcTemplate.update("INSERT INTO M_LOG_BANCODADOS (LOG, DATA, USUARIO, EXECUTADO) VALUES (?, ?, ?, ?)",
					new Object[] { sql, time, user, executado });

		}
	}

	public void executeSQL(final String sql) {
		executeDDL(sql, true);
	}

	private void extractConstraints(final String tableName, final Map<String, String> property) {

		if (property.get("type").equals("bit")) {
			List<String> booleanConstraint = this.booleanConstraints.get(tableName);
			final String propertyName = property.get(Constants.name);

			if (booleanConstraint == null) {
				booleanConstraint = new ArrayList<String>();
				booleanConstraint.add(propertyName);
				this.booleanConstraints.put(tableName, booleanConstraint);

			} else if (!booleanConstraint.contains(propertyName)) {
				booleanConstraint.add(propertyName);
			}
		}
	}

	private void extractForeignKey(final String tableName, final Map<String, String> property) {

		final String serviceName = property.get("service");
		final String columnName = property.get(Constants.name);
		final String serviceTable = getServiceTable(serviceName);
		addForeignKey(tableName, columnName, serviceTable);
	}

	private void extractIndex(final String tableName, final Map<String, String> property, final CONSTRAINT_TYPE type) {
		final Map<String, String> index = getIndexes(tableName);
		final String propertyName = property.get(Constants.name);
		final String constraintName = dialect.getConstraintName(tableName, propertyName, type);

		index.put(propertyName, constraintName);
	}

	private void extractIndexs(final String tableName, final Map<String, String> property) {

		if (DAOUtil.isForeignKey(property)) {
			extractIndex(tableName, property, CONSTRAINT_TYPE.FOREIGNKEY_INDEX);

		} else if (DAOUtil.isMainField(property)) {
			extractIndex(tableName, property, CONSTRAINT_TYPE.INDEX);
		}
	}

	private void extractInnerColumns(final String relationalTable, final Map<String, String> property) {

		final Map<String, Object> columns = user.getUtil().JSONtoMap(property.get("columns"));

		Map<String, String> column;
		final boolean isGeneratedKeyBySequence = this.dialect.isGeneratedKeyBySequence();

		for (final String columnName : columns.keySet()) {
			column = (Map<String, String>) columns.get(columnName);
			column.put("parent_node", property.get("parent_node"));
			addColumn(relationalTable, columnName, columnFromProperty(column, isGeneratedKeyBySequence));
		}
	}

	private void extractPrimaryKey(final String tableName, final Map<String, String> property) {

		List<String> fields = this.primaryKeys.get(tableName);
		final String fieldName = property.get(Constants.name);

		if (fields == null) {
			fields = new ArrayList<String>();
			fields.add(fieldName);
			this.primaryKeys.put(tableName, fields);

		} else if (!fields.contains(fieldName)) {
			fields.add(fieldName);
		}

		if (this.dialect.isGeneratedKeyBySequence()) {
			final String oldSequence = this.sequences.get(tableName);

			if (oldSequence != null && !oldSequence.equals(fieldName)) {
				LogUtil.debug("Inconsistencia: Tabela " + tableName + " com duas chaves primarias (" + oldSequence
					+ " e " + fieldName + ") solicitando criacao de sequencia.", user);
			}
			this.sequences.put(tableName, fieldName);
		}
	}

	private void extractRelationalTable(final String tableName, final Map<String, String> property,
			final Map<String, String> primaryKey) {

		// Adiciona a chave estrangeira

		final String relationalTable = property.get("relational_table");

		if (DAOUtil.hasColumns(property)) {
			extractInnerColumns(relationalTable, property);
		}

		final String foreignField = getForeignFieldName(property);
		final String primaryKeyName = primaryKey.get(Constants.name);
		final String serviceTable = getServiceTable(property.get("service"));
		final String serviceMethod = property.get("serviceMethod");
		if (property.get("sharingKey") == null) // sharing sem fk
			addForeignKey(relationalTable, primaryKeyName, tableName);

		if (serviceTable != null && serviceMethod == null) {
			addForeignKey(relationalTable, foreignField, serviceTable);
		}

		final String foreignFieldType = getForeignFieldType(property, serviceTable);

		// Adiciona os campos na tabela de relacao
		addPrimaryKeyColumn(relationalTable, primaryKeyName, primaryKey.get("type"));
		addPrimaryKeyColumn(relationalTable, foreignField, foreignFieldType);

		// Adiciona as chaves primarias na tabela de relacao
		addPrimaryKeyOnRelationalTable(relationalTable, foreignField, primaryKeyName);
	}

	private Collection<Map<String, String>> getBeans() {

		if (this.beans == null || this.beans.size() == 0) {
			this.beans = DAOUtil.getBeans(user, parseXml);
		}
		return this.beans;
	}

	private String getForeignFieldName(final Map<String, String> property) {
		final String foreignField;

		if (property.get("cyclic") != null && property.get("cyclic").equals("true")) {

			foreignField = property.get(Constants.name);

		} else {
			foreignField = property.get("key");
		}
		return foreignField;
	}

	private String getForeignFieldType(final Map<String, String> property, final String serviceTable) {
		final String foreignFieldType;

		final String serviceMethod = property.get("serviceMethod");

		if (serviceMethod == null) {
			foreignFieldType = serviceTable == null ? "alfa" : getPrimaryKeyType(serviceTable);
		} else {
			foreignFieldType = getServiceMethodType(property.get("service"), serviceMethod);
		}
		return foreignFieldType;
	}

	public Map<String, String> getIndexes(final String tableName) {

		Map<String, String> indexes = this.tableIndexes.get(tableName);

		if (indexes == null) {
			indexes = new HashMap<String, String>();
			this.tableIndexes.put(tableName, indexes);
		}
		return indexes;
	}

	private String getPrimaryKeyType(final String tableName) {
		return parseXml.getValueFromXQL("/systems/system[@name='" + user.getSchema()
				+ "']/module/bean[not(@extends) and @table='" + tableName + "']/property[@primaryKey='true']", "type",
				user, true, false);
	}

	public String getColumnType(final String tableName, final String campo) {
		return parseXml.getValueFromXQL("/systems/system[@name='" + user.getSchema()
				+ "']/module/bean[not(@extends) and @table='" + tableName + "']/property[@name='" + campo + "']",
				"type", user, true, false);
	}

	private String getServiceMethodType(final String service, final String serviceMethod) {

		final String beanName = service.replaceAll(Constants.SERVICE_BASE, "");

		final String type = parseXml.getValueFromXQL("/systems/system[@name='" + user.getSchema()
				+ "']/module/bean[@name='" + beanName + "']/method[@name='" + serviceMethod + "']/property", "type",
				user, true, false);

		return type.equals("") ? "num" : type;
	}

	private String getServiceTable(final String serviceName) {

		// if(serviceName.equalsIgnoreCase("SystemService"))
		final String tableName = DAOUtil.getServiceTable(serviceName, user, parseXml);
		return tableName == null || tableName.equals("") ? null : tableName;
	}

	private Map<String, Map<String, String>> getTable(final String tableName) {

		if (this.tables.get(tableName) == null) {
			// Usando TreeMap para que os campos sejam criados em ordem
			// alfabetica na tabela
			final Map<String, Map<String, String>> tableMap = new TreeMap<String, Map<String, String>>();
			this.tables.put(tableName, tableMap);
		}
		return this.tables.get(tableName);
	}

	private void loadColumns(final String tableName) {
		final Collection<Map<String, String>> properties = DAOUtil.getProperties(user, parseXml, tableName);

		final Map<String, String> primaryKey = DAOUtil.findPrimaryKey(properties);

		final boolean isGeneratedKeyBySequence = this.dialect.isGeneratedKeyBySequence();

		for (final Map<String, String> property : properties) {

			if (!DAOUtil.isIgnoredProperty(property)) {

				if (DAOUtil.isRelationalTable(property)) {
					extractRelationalTable(tableName, property, primaryKey);

				} else {
					addColumn(tableName, property.get(Constants.name),
							columnFromProperty(property, isGeneratedKeyBySequence));

					extractConstraints(tableName, property);
					extractIndexs(tableName, property);

					if (DAOUtil.isPrimaryKey(property)) {
						extractPrimaryKey(tableName, property);
					}

					if (DAOUtil.isForeignKey(property)) {
						extractForeignKey(tableName, property);
					}
				}
			}
		}
	}

	private void loadDetailTable(final Map<String, String> bean) {
		final String masterId = bean.get("master");
		final String relationalTable = bean.get("table");

		final Map<String, String> master = DAOUtil.findBeanById(masterId, getBeans());

		if (master != null) {
			final String tableName = master.get("table");
			final String primaryKey = DAOUtil.getPrimaryKeyName(tableName, parseXml, user);

			addForeignKey(relationalTable, primaryKey, tableName);

			// Chave estrangeira em "D" nao pode aceitar nulo
			final Map<String, String> field = this.tables.get(relationalTable).get(primaryKey);

			if (field != null) {
				field.put("required", Constants.true_str);
			}
		}
	}

	private void loadSchema() {

		if (this.tables.size() == 0) {
			loadTables();
		}
	}

	private void loadTable(final Map<String, String> bean) {

		final String tableName = bean.get("table");
		final String tableType = bean.get("type");

		// Adiciona campos default se nao for MM
		if (tableType != null) {
			addCommonsFields(tableName);
		}
		loadColumns(tableName);

		if (tableType.equals("D")) {
			loadDetailTable(bean);
		}
	}

	private void loadTables() {

		final Collection<Map<String, String>> beans = getBeans();
		final List<String> loadedTables = new ArrayList<String>();

		for (final Map<String, String> bean : beans) {

			if (!loadedTables.contains(bean.get("table"))) {
				loadTable(bean);

				loadedTables.add(bean.get("table"));
			}
		}
	}

	public void renameColumn(final String tableName, final String columnName, final String newColumnName) {

		executeDDL(dialect.remaneColumn(tableName, columnName, newColumnName));
	}

	public void renameTable(final String tableName, final String newTableName) {
		executeDDL(dialect.renameTable(tableName, newTableName));
	}

	public void updateColumn(String tableName, String string, Object object, boolean b) {
		if (b)
			executeDDL(" UPDATE " + tableName + " SET " + string + " = '" + object + "' WHERE " + string + " is null");
		else
			executeDDL(" UPDATE " + tableName + " SET " + string + " = " + replaceFuncions(object) + " WHERE " + string
					+ " is null");

	}

	public void updateFKColumn(String tableName, String string, Object object, String fkField, String fkTable,
			boolean b) {
		if (fkField == null || fkField.trim().length() == 0) {
			fkField = getPrimaryKeyName(fkTable);
		}
		if (b)
			executeDDL(" UPDATE " + tableName + " SET " + string + " = '" + object + "' WHERE " + string
					+ " not in (select " + fkField + " from " + fkTable + ") ");
		else
			executeDDL(" UPDATE " + tableName + " SET " + string + " = " + replaceFuncions(object) + " WHERE " + string
					+ " not in (select " + fkField + " from " + fkTable + ") ");

	}

	private String getPrimaryKeyName(String tableName) {
		// TODO Auto-generated method stub
		return parseXml.getValueFromXQL("/systems/system[@name='" + user.getSchema()
				+ "']/module/bean[not(@extends) and @table='" + tableName + "']/property[@primaryKey='true']",
				Constants.name, user, true, false);
	}

	private String replaceFuncions(Object object) {
		if (object.toString().equals("sysdate"))
			object = dialect.getSysdate();

		return object.toString();
	}

	public String getPropertyValue(String tableName, String campo, String string2) {
		return parseXml.getValueFromXQL("/systems/system[@name='" + user.getSchema()
				+ "']/module/bean[not(@extends) and @table='" + tableName + "']/property[@name='" + campo + "']",
				string2, user, true, false);
	}

	public void dropSequences(List<String> tableNames) {

		final List<Map<String, Object>> indexs = jdbcTemplate.queryForList(dialect.getAllSequences(tableNames));

		final int numindexs = indexs.size();

		String indexName = null, tableName;
		final Map<String, String> primaryindexs = new HashMap<String, String>();

		for (final Map<String, Object> index : indexs) {

			indexName = (String) index.get("SEQUENCE_NAME");
			tableName = indexName.split("_id_")[0];
			// indexType = (String) index.get("INDEX_TYPE");
			try {
				executeDDL(dialect.dropSequence(tableName, indexName, user));
			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

		}

		// TODO Auto-generated method stub

	}

	public void create(Map<String, Map<String, Map<String, Map<String, String>>>> tables2, DBCreator to) {
		// TODO Auto-generated method stub
		parseSchema(tables2, to);
		createTables();
		createPrimaryKeys();
		createForeignKeys();
		createConstraints();
		createIndexs();

		if (this.dialect.isGeneratedKeyBySequence()) {
			createSequences();
		}
	}

	public void parseSchema(Map<String, Map<String, Map<String, Map<String, String>>>> tables2, DBCreator to) {
		// TODO Auto-generated method stub
		Iterator i = tables2.keySet().iterator();
		while (i.hasNext()) {
			String object = (String) i.next();
			Map<String, Map<String, Map<String, String>>> t = tables2.get(object);
			getTable(object);
			Map<String, Map<String, String>> c = t.get("columns");
			Map<String, Map<String, String>> p = t.get("primaryKeys");
			Map<String, Map<String, String>> f = t.get("foreingKeys");
			Map<String, Map<String, String>> st = t.get("statements");
			addStatement(object, st);

			Iterator e = c.keySet().iterator();
			while (e.hasNext()) {
				Object object2 = (Object) e.next();
				addColumn(object, (String) object2, parseColumn(c.get(object2), p, f, to));
			}

		}
	}

	private Map<String, String> parseColumn(Map<String, String> map, Map<String, Map<String, String>> p,
			Map<String, Map<String, String>> f, DBCreator d) {

		Map c = new HashMap();
		String name = map.get("COLUMN_NAME");
		c.put("name", name);
		if (p.get(name) != null)
			c.put("primaryKey", "true");
		if (map.get("NULLABLE") != null && map.get("NULLABLE").toString().equals("0"))
			c.put("required", "true");

		c.put("type", d.dialect.getMetaType(map.get("TYPE_NAME"), map.get("DECIMAL_DIGITS")));
		// TODO Auto-generated method stub
		if (c.get("type") != null && c.get("type").equals("alfa")) {
			if (map.get("COLUMN_SIZE") != null)
				c.put("length", map.get("COLUMN_SIZE"));

		}
		return c;
	}

	private void addStatement(String object, Map<String, Map<String, String>> st) {
		// TODO Auto-generated method stub
		this.statements.put(object, st);
	}

	public void populateFromTemplate(final JdbcTemplate jdbcTemplate2, final List restrict) {
		// TODO Auto-generated method stub
		LogUtil.debug("Populando " + this.tables.size() + " tabelas...", user);
		SimpleJdbcInsert insert;

		int i = 0;

		for (final Entry<String, Map<String, Map<String, String>>> entry : this.tables.entrySet()) {
			try {

				i++;
				insert = new SimpleJdbcInsert(jdbcTemplate.getDataSource()).withTableName(entry.getKey());
				if (restrict != null && restrict.contains(entry.getKey())) {
					LogUtil.debug("Ignorando tabela " + entry.getKey(), user);
					continue;

				}
				Map st = ((Map) statements.get(entry.getKey()));
				List it = jdbcTemplate2.queryForList((String) st.get("select"));
				for (Iterator iterator = it.iterator(); iterator.hasNext();) {
					Map object = (Map) iterator.next();
					LogUtil.debug("Populando " + object, user);
					if (ConnectionManager.getInstance(user).getBancoConectado(user) == ConnectionManager.SQLSERVER) {
						Object[] cnames = st.get("fields").toString().split(",");
						String c = "";
						if (!entry.getKey().startsWith("MM_"))
							c = " SET IDENTITY_INSERT " + entry.getKey() + " ON  " + st.get("insert")
									+ "  SET IDENTITY_INSERT " + entry.getKey() + " OFF ";
						else
							c = (String) st.get("insert");
						Object[] values = new Object[cnames.length];
						for (int j = 0; j < cnames.length; j++) {
							values[j] = object.get(cnames[j]);
						}
						jdbcTemplate.update(c, values);
					} else
						insert.execute(object);

					// BoneCPDataSource datasource = (BoneCPDataSource)
					// jdbcTemplate.getDataSource();
					// jdbcTemplate.setDataSource(ConnectionManager.getInstance(user).refreshDataSource(datasource));

				}

				LogUtil.debug("Fim " + entry.getKey() + " " + i + " de " + this.tables.size(), user);

				LogUtil.debug("Populado " + this.tables.size() + " tabelas...", user);

			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

		}
		LogUtil.debug("Fim populate", user);

	}

	public String contSQL(String string) {
		// TODO Auto-generated method stub
		return "" + jdbcTemplate.queryForObject(string, Long.class);
	}
}
