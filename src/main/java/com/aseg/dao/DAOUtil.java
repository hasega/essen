package com.aseg.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import com.aseg.config.Constants;
import com.aseg.seguranca.Usuario;
import com.aseg.util.service.ParseXml;
import com.aseg.util.service.ParseXmlService;

public final class DAOUtil {

	public static Map<String, String> findBeanById(final String masterId, final Collection<Map<String, String>> beans) {

		for (final Map<String, String> master : beans) {

			if (master.get("id").equals(masterId)) {
				return master;
			}
		}
		return null;
	}

	public static Map<String, String> findPrimaryKey(final Collection<Map<String, String>> properties) {

		for (final Map<String, String> property : properties) {

			if (!DAOUtil.isIgnoredProperty(property) && DAOUtil.isPrimaryKey(property)) {
				return property;
			}
		}
		return null;
	}

	public static Collection<Map<String, String>> getBeans(final Usuario user, final ParseXml parseXml) {

		return parseXml.getListValuesFromXQL("/systems/system[@name='" + user.getSchema() + "']/module/bean[@table]",
				"table", user, true, true);
	}

	public static List<String> getColumnsNameFromFilter(final String filter) {
		final List<String> columnsName = new ArrayList<String>();

		final Pattern pattern = Pattern.compile("f[ ]*:[ ]*'([A-Z_]+)'");

		final Matcher matcher = pattern.matcher(filter);

		while (matcher.find()) {
			columnsName.add(matcher.group(1));
		}
		return columnsName;
	}

	public static Collection<String> getFilters(final String tableName, final Usuario user, final ParseXml parseXml) {

		final String xpathBean = "/systems/system[@name='" + user.getSchema() + "']/module/bean";

		final Collection<String> allFilter = parseXml.getListFromXQL(xpathBean + "[@table='" + tableName + "']/@filtro",
				user);

		final Collection<String> beansName = parseXml.getListFromXQL(xpathBean + "[@table='" + tableName + "']/@name",
				user);

		Collection<String> propertyFilter;

		for (final String beanName : beansName) {
			propertyFilter = parseXml.getListFromXQL(xpathBean
					+ "/property[not(@relational_table) and @filtro and @service='" + beanName + "Service']/@filtro",
					user);

			allFilter.addAll(propertyFilter);
		}
		return allFilter;
	}

	public static Map<String, Collection<String>> getFiltersFromRelationalTables(final Usuario user,
			final ParseXml parseXml) {

		final String xpathBean = "/systems/system[@name='" + user.getSchema() + "']/module/bean";

		final Collection<String> relationalTables = parseXml
				.getListFromXQL(xpathBean + "/property[@relational_table and @filtro]/@relational_table", user);

		Collection<String> propertyFilter;
		final Map<String, Collection<String>> relationalFilters = new HashMap<String, Collection<String>>();

		for (final String relationalTable : relationalTables) {

			propertyFilter = parseXml.getListFromXQL(
					xpathBean + "/property[@filtro and @relational_table='" + relationalTable + "']/@filtro", user);

			relationalFilters.put(relationalTable, propertyFilter);
		}
		return relationalFilters;
	}

	public static String getPrimaryKeyName(final String tableName, final ParseXml parse, Usuario user) {

		final String xpath = "/systems/system[@name='" + user.getSchema() + "']/module/bean[@table='" + tableName
				+ "']";

		return (String) parse.getValueFromXQL(xpath + "/property[@primaryKey='true']", "name", user, false, true);

		// return tableName.replaceAll("^[A-Z]+_", "ID_");
	}

	public static Collection<Map<String, String>> getProperties(final Usuario user, final ParseXml parse,
			final String tableName) {

		final String system = user.getSchema();

		final String xpath = "/systems/system[@name='" + system + "']/module/bean[not(@extends) and @table='"
				+ tableName + "']";

		final List<Map<String, String>> beanProperties = (List<Map<String, String>>) parse
				.getListValuesFromXQL(xpath + "/property[not(@volatile)]", "order", user, true, true);

		final Map<String, Map<String, String>> properties = new HashMap<String, Map<String, String>>();

		Map<String, String> oldProperty;
		String propertyName;

		for (final Map<String, String> property : beanProperties) {
			propertyName = property.get(Constants.name);

			oldProperty = properties.get(propertyName);

			if (oldProperty == null) {
				properties.put(propertyName, property);

				// Remove restricao (cyclic e nullable)
				if (DAOUtil.isNullable(property)) {
					property.remove("required");
				}

			} else if (!DAOUtil.isNullable(oldProperty) && DAOUtil.isNullable(property)) {

				properties.put(propertyName, property);
			}
		}

		final List<Map<String, String>> methodProperties = (List<Map<String, String>>) parse
				.getListValuesFromXQL(xpath + "/method/property[not(@volatile)]", "order", user, true, true);

		for (final Map<String, String> property : methodProperties) {
			propertyName = property.get(Constants.name);

			if (!properties.containsKey(propertyName)) {
				properties.put(propertyName, property);
				property.remove("required");
			}
		}
		Collection l = properties.values();
		List fonetic = new ArrayList();
		for (Iterator iterator = l.iterator(); iterator.hasNext();) {
			Map object = (Map) iterator.next();
			if (object.get("foneticSearch") != null) {
				Map t = new HashMap(object);
				t.put("name", t.get("name") + "_FONETIC");
				fonetic.add(t);
			}

		}

		fonetic.addAll(l);
		return fonetic;
	}

	public static String getServiceTable(final String serviceName, final Usuario user, final ParseXml parse) {

		final String beanName = serviceName.replaceAll("Service$", "");
		final String system = user.getSchema();
		final String tableName = parse.getValueFromXQL(
				"/systems/system[@name='" + system + "']/module/bean[@name='" + beanName + "']", "table", user, false,
				true);

		if (tableName == null) {
			return null;
		}
		return tableName.equals("") ? null : tableName;
	}

	public static Collection<String> getTables(final Usuario user, final ParseXml parse) {

		final String system = user.getSchema();

		final Collection<String> beans = parse
				.getListFromXQL("/systems/system[@name='" + system + "']/module/bean[@table]/@table", user);

		final List<String> tables = new ArrayList<String>();

		for (final String tableName : beans) {

			if (!tables.contains(tableName)) {
				tables.add(tableName);
			}
		}

		return tables;
	}

	public static boolean hasColumns(final Map<String, String> property) {
		return property.get("columns") != null;
	}

	public static boolean isForeignKey(final Map<String, String> property) {
		boolean foreignKey = property.get("service") != null;

		if (isRelationalTable(property) || property.get("serviceMethod") != null) {
			foreignKey = false;
		}
		return foreignKey;
	}

	public static boolean isIgnoredIndex(final Map<String, String> property) {

		if (property.get("type").equals("file")) {
			return true;
		}
		return false;
	}

	/** * Retorna true se a propriedade nao tem relacao com o mapeamento do banco
	 * de dados.
	 * 
	 * @param property
	 * @return
	 */
	public static boolean isIgnoredProperty(final Map<String, String> property) {

		if (property.get("volatile") != null && property.get("volatile").equals(Constants.true_str)) {
			return true;

		} else if (property.get("type").equals("sqlrule")) {
			return true;
		} else if (property.get("component") != null && property.get("component").equals("fake")) {
			return true;
		}
		return false;
	}

	public static boolean isMainField(final Map<String, String> property) {
		final String mainField = property.get("mainField");

		if (mainField != null && mainField.equals(Constants.true_str)) {
			return true;
		}
		return false;
	}

	public static boolean isNullable(final Map<String, String> property) {

		if (property.get("storage") != null)
			return true;

		if (isTrue(property, "required") || isTrue(property, "primaryKey") || isTrue(property, "relationKey")) {

			if ((isTrue(property, "cyclic") && !isTrue(property, "required")) || isTrue(property, "nullable")) {
				return true;
			}

			if (property.get("type") == null)
				return false;
			if (property.get("type").equals("bit"))
				return true;

			return false;
		}

		if (property.get("type") != null && property.get("type").equals("bit") && !isTrue(property, "nullable"))
			return false;
		return true;
	}

	public static boolean isPrimaryKey(final Map<String, String> property) {
		final String primaryKey = property.get("primaryKey");
		return primaryKey != null && primaryKey.equals(Constants.true_str) ? true : false;
	}

	public static boolean isRelationalTable(final Map<String, String> property) {
		return property.get("relational_table") != null ? true : false;
	}

	private static boolean isTrue(final Map<String, String> property, final String field) {

		return property.get(field) != null && property.get(field).toString().equals(Constants.true_str);
	}

	private DAOUtil() {
	}

	public static Map getProperty(Usuario user, ParseXmlService parse, String tableName, String string) {
		// TODO Auto-generated method stub
		final String system = user.getSchema();

		final String xpath = "/systems/system[@name='" + system + "']/module/bean[not(@extends) and @table='"
				+ tableName + "']";
		try {
			return ((List<Map<String, String>>) parse.getListValuesFromXQL(xpath + "/property[@name='" + string + "']",
					"order", user, true, true)).get(0);
		} catch (Exception e) {
			return new HashMap<String, String>();
		}

	}

	public static String makeJoin(List joins, String pk, String master, String joinType, Usuario user) {

		String[] fields = user.getUtil().extractArrayFromList(joins, "FIELDS", true);

		String f = StringUtils.arrayToCommaDelimitedString(fields);

		Object[] wheres = user.getUtil().extractObjectArrayFromList(joins, "WHERE", true);
		String w = "";
		String p = "";
		for (int i = 0; i < wheres.length; i++) {
			String[] ws = ((String[]) wheres[i]);

			String a = ws[1];
			if (p.length() == 0 && ws[0] != null && ws[0].trim().length() > 0)
				p = ws[0];
			if (a != null && !a.trim().equalsIgnoreCase("null") && a.trim().length() > 0 && a.indexOf("?") == -1)
				w += a;

		}

		String[] tables = user.getUtil().extractArrayFromList(joins, "FROM", true);

		String t = tables[0];

		for (int i = 1; i < tables.length; i++) {

			String tt = tables[i];

			if (((Map) user.getUtil().match(joins, "FROM", tt).get(0)).containsKey("volatile"))
				continue;

			if (tt.trim().indexOf(" ") > -1)
				tt = tt.trim().substring(0, tt.trim().indexOf(" "));
			t += joinType + " join " + tables[i] + " on " + master + "." + pk + " =  " + tt + "." + pk + " ";
		}

		Map m = (Map) joins.get(0);
		String o = (String) m.get("ORDER");
		if (o == null)
			o = "";
		if (w.toString().trim().length() > 0)
			w = " WHERE (1=1) " + w;
		// TODO Auto-generated method stub
		return p + " " + m.get("SELECT") + " " + f + " FROM " + t.trim() + w + o;
	}

	public static String makecase(String field, Map val, Usuario user) {
		if (val == null || val.size() == 0) {
			// TODO Auto-generated method stub
			return null;
		} else {
			String r = "";
			if (field.toUpperCase().indexOf(" WHEN ") == -1) {
				r = " ( CASE ";
				for (Object k : val.keySet()) {
					r += " WHEN " + field + " = '" + k + "' THEN  '" + user.getLabel(val.get(k).toString()) + "' ";
				}

				r += "	END) ";
			} else {
				r = field;
				for (Object k : val.keySet()) {
					r = user.getUtil().replace(r, "'" + k + "'", "'" + user.getLabel(val.get(k).toString()) + "'");
				}

			}

			return r;
		}
	}

}
