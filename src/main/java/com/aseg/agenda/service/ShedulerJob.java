package com.aseg.agenda.service;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;

public class ShedulerJob extends ServiceImpl {
	public void Log(final String id_robo, final long data, final String Mensagem, final String tipo_evento,
			Usuario user) {

		final long pk = 0;
		try {

			GenericService s = (GenericService) getService("LogAgendadorService", user);

			GenericVO vo = s.createVO(user);
			vo.put("ID_AGENDA_ROBO", new Long(id_robo));
			vo.put("DATA", user.getUtil().nowTimestamp());
			vo.put("MENSAGEM", Mensagem);
			vo.put("TIPO_DE_EVENTO", String.valueOf(tipo_evento));
			s.inclui(vo, user);

		} catch (final Exception e) {
			LogUtil.exception(e, user);
		}

		if (LogUtil.debugEnabled(user)) {
			try {
				GenericVO myvo = createVO(user);
				myvo.put("Assunto", "Erro no agendamento de tarefa");
				myvo.put("Corpo", Mensagem);
				myvo.put("idUsuario", user);
				// getEmailService(user).send(myvo, user);
			} catch (final Exception e) {
				LogUtil.exception(e, user);
			}
		}
	}
}
