package com.aseg.agenda.service;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobKey.jobKey;
import static org.quartz.impl.matchers.GroupMatcher.groupEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerUtils;
import org.quartz.UnableToInterruptJobException;
import org.quartz.impl.triggers.CronTriggerImpl;

import com.aseg.config.Constants;
import com.aseg.email.service.NotificationJob;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.util.DataFormat;
import com.aseg.vo.GenericVO;

public class AgendadorListenerTask extends ShedulerJob {

	private static volatile HashMap root = new HashMap();

	// static DAOTemplate tem = null;

	public void garanteExecucoesAnteriores(final Map tarefa, final Timestamp agora, CronTriggerImpl ct, Usuario user) {

		GenericService schedulerService = (GenericService) getService("SchedulerService", user);
		GenericService execSchedService = (GenericService) getService("ExecucaoAgendadorService", user);

		// LogUtil.debug("Estou agendando a tarefa "+tarefa);
		if (AgendaRoboManager.CONSOLELOG) {
			LogUtil.debug("garantindo execucoes " + tarefa, user);
		}
		long num_exec_retro = 0;

		if (tarefa.get("NUM_EXEC_RETROATIVAS") != null) {
			num_exec_retro = (Long) tarefa.get("NUM_EXEC_RETROATIVAS");
		}

		if (tarefa == null) {
			return;
		}

		if (num_exec_retro == 0) {
			if (AgendaRoboManager.CONSOLELOG) {
				System.out.println("nao garanti pois o numero de execucoes retroativas  0 " + tarefa);
			}

			// nao necessario garantir as execues anteriores
			return;
		}

		/*
		 * if ((Date) tarefa.get("DATA_ULTIMO_AGENDAMENTO") == null) {
		 * AgendaRoboManager.getInstance(user).Log(
		 * tarefa.get("ID_AGENDA_ROBO").toString(), agora.getTime(),
		 * "Data do ultimo agendamento nula, essa data foi setada para " +
		 * agora.toString() + "", AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
		 * try {
		 * 
		 * GenericVO vo = schedulerService.createVO(user);
		 * vo.put("ID_AGENDA_ROBO",
		 * (Integer.parseInt(tarefa.get("ID_AGENDA_ROBO").toString())));
		 * vo.put("DATA_ULTIMO_AGENDAMENTO", new
		 * Date(System.currentTimeMillis()));
		 * 
		 * schedulerService.altera(vo, user); //return; } catch (final Exception
		 * e) { e.printStackTrace(); return; } }
		 */

		// Garantindo execucoes corretas para programas que foram terminados
		// inesperadamente e a data de ultimo agendamento foi setada
		GenericVO vo = execSchedService.createVO(user);
		vo.setFiltro("{gA:[{c:{f:'EXECUTANDO',o:'=',v1:'T'}}, {c:{f:'DATA_FIM_EXECUCAO',o:'nl'}}]}");
		vo.put("ID_AGENDA_ROBO", (Integer.parseInt(tarefa.get("ID_AGENDA_ROBO").toString())));

		List items = new ArrayList();

		try {
			items = execSchedService.lista(vo, user);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			LogUtil.exception(e1, user);
		}

		final Iterator ito = items.iterator();
		int num_exec_antiga = 0;
		while (ito.hasNext() && num_exec_retro > num_exec_antiga) {
			final Map dados = (Map) ito.next();

			Class exec = null;
			if (tarefa.get("MULTI_THREAD") != null && tarefa.get("MULTI_THREAD").toString().equalsIgnoreCase("T")) {
				exec = AgendaRoboExecutor.class;
			} else {
				exec = StatefulAgendaRoboExecutor.class;
			}
			final JobDetail task_antiga = newJob(exec).withIdentity(
					tarefa.get("NOME").toString() + "-Atrasada" + num_exec_antiga + "-"
							+ tarefa.get("ID_AGENDA_ROBO").toString() + dados.get("DATA_EXECUCAO"),
					"group" + tarefa.get("ID_AGENDA_ROBO").toString()).storeDurably().requestRecovery().build();
			final JobDataMap param = task_antiga.getJobDataMap();

			final Iterator i = tarefa.keySet().iterator();
			while (i.hasNext()) {
				final Object key = i.next();
				param.put((String) key, tarefa.get(key));
				if (AgendaRoboManager.CONSOLELOG) {
					LogUtil.debug(key + " valor >>>" + tarefa.get(key), user);
				}

			}

			param.put("DATA_EXECUCAO", dados.get("DATA_EXECUCAO"));
			param.put("DATA_AGENDAMENTO", agora);
			param.put("ATRASADO", Constants.true_str);

			// Buscando o usurio que ir executar o JOB
			GenericService userService = (GenericService) getService("UsuarioService", user);
			// GenericService schedulerService = (GenericService)
			// getService("SchedulerService", user);

			Long idAgendaRobo = (Long) tarefa.get("ID_AGENDA_ROBO");
			GenericVO agendaVO = schedulerService.createVO(user);
			agendaVO.put("ID_AGENDA_ROBO", idAgendaRobo);
			try {
				agendaVO = schedulerService.obtem(agendaVO, user);
			} catch (Exception e1) {
				LogUtil.exception(e1, user);
				LogUtil.debug("No foi possvel recuperar a tarefa: " + task_antiga
						+ ". Agendamento de execues anteriores cancelado.", user);
			}

			Long idUser = (Long) agendaVO.get("ID_USUARIO_CADASTRO");
			GenericVO userVO = userService.createVO(user);
			userVO.put("ID_USUARIO", idUser);
			try {
				userVO = (GenericVO) userService.obtem(userVO, user);
			} catch (Exception e1) {
				LogUtil.exception(e1, user);
				LogUtil.debug("No foi possvel recuperar a tarefa: " + task_antiga + ". O usurio com identificador: "
						+ idUser + " no foi encontrado. Agendamento de execues anteriores cancelado.", user);
			}
			Usuario muser = new Usuario(user);
			muser = userService.bindUser(muser, userVO, false);

			// Adicionando o usurio que ir executar
			param.put(Constants.RAIN_USER, muser);

			task_antiga.getJobDataMap().putAll(param);

			final Trigger trigger = TriggerBuilder.newTrigger()
					.withIdentity(
							"Trigger de " + tarefa.get("NOME").toString() + "-Atrasada" + num_exec_antiga + "-"
									+ tarefa.get("ID_AGENDA_ROBO").toString() + dados.get("DATA_EXECUCAO"),
							"group" + tarefa.get("ID_AGENDA_ROBO").toString())
					.startNow().build();
			// trigger.setEndTime(new Date());

			// trigger.setMisfireInstruction(Trigger.INSTRUCTION_DELETE_TRIGGER);
			try {
				getScheduler(user).scheduleJob(task_antiga, trigger);
				LogUtil.debug("agendei" + task_antiga, user);

				GenericVO voExecucao = execSchedService.createVO(user);
				voExecucao.put("ID_EXECUCAO_AGENDA_ROBO", new Object[] { dados.get("ID_EXECUCAO_AGENDA_ROBO") });
				execSchedService.remove(voExecucao, user);
				num_exec_antiga++;
			} catch (final SchedulerException e) {
				LogUtil.exception(e, user);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				LogUtil.exception(e, user);
			}
		}

		// Listar as datas que a tarefa deveria ter sido executada
		List dates;
		if (tarefa.get("DATES") == null) {

			try {
				Date date = (Date) tarefa.get("DATA_INICIO_AGENDA");
				if (tarefa.get("DATA_ULTIMO_AGENDAMENTO") != null)
					date = (Date) tarefa.get("DATA_ULTIMO_AGENDAMENTO");
				dates = TriggerUtils.computeFireTimesBetween(ct, null, date, new Date(agora.getTime()));
			} catch (final java.lang.UnsupportedOperationException ex) {
				LogUtil.exception(ex, user);
				Log(tarefa.get("ID_AGENDA_ROBO").toString(), agora.getTime(),
						"Cron string invlida da tarefa " + tarefa.get("ID_AGENDA_ROBO").toString()
								+ " com data de execuo "
								+ user.getUtil().formatDate((Date) tarefa.get("DATA_EXECUCAO"), "dd/MM/yyyy HH:mm")
								+ ct,
						AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
				return;
			}
		} else {
			dates = (List) tarefa.get("DATES");
		}

		if (AgendaRoboManager.CONSOLELOG) {
			LogUtil.debug("dates: " + dates, user);
		}

		final Iterator it = dates.iterator();
		// Iterator it = teste.iterator();

		int cont = 0;
		int retro = -1;

		while (it.hasNext()) {
			final Date dataref = (Date) it.next();

			if (tarefa.get("NUM_EXEC_RETROATIVAS") != null) {
				retro = Integer.parseInt(tarefa.get("NUM_EXEC_RETROATIVAS").toString());
				if (dates.size() > retro + cont) {
					cont++;
					if (AgendaRoboManager.CONSOLELOG) {
						LogUtil.debug("pulei a execucao da data " + dataref + "com contador = " + cont
								+ " o nmero retro " + retro, user);
					}
					continue;
				}
			}

			if (!verificaExecucaoCorreta(tarefa.get("ID_AGENDA_ROBO").toString(), dataref, user)) {
				Class exec = null;
				if (tarefa.get("MULTI_THREAD") != null && tarefa.get("MULTI_THREAD").toString().equalsIgnoreCase("T")) {
					exec = AgendaRoboExecutor.class;
				} else {
					exec = StatefulAgendaRoboExecutor.class;

				}
				final JobDetail task_antiga = newJob(exec).withIdentity(tarefa.get("NOME").toString() + "-Atrasada-"
						+ tarefa.get("ID_AGENDA_ROBO").toString() + dataref,
						"group" + tarefa.get("ID_AGENDA_ROBO").toString()).build();
				final JobDataMap param = task_antiga.getJobDataMap();
				if (tarefa.get("MULTI_THREAD") != null && tarefa.get("MULTI_THREAD").toString().equalsIgnoreCase("F")) {
					if (dataref != dates.get(dates.size() - 1)) {
						tarefa.put("DATES", dates);
					} else {
						tarefa.remove("DATES");
					}
					param.put("TAREFA", tarefa);
					param.put("TRIGGER", ct.clone());
				}
				final Iterator i = tarefa.keySet().iterator();
				while (i.hasNext()) {
					final Object key = i.next();
					param.put((String) key, tarefa.get(key));
					if (AgendaRoboManager.CONSOLELOG) {
						System.out.println(key + " valor >>>" + tarefa.get(key));
					}

				}

				param.put("DATA_EXECUCAO", dataref);
				param.put("DATA_AGENDAMENTO", agora);
				param.put("ATRASADO", Constants.true_str);

				Trigger trigger = TriggerBuilder.newTrigger()
						.withIdentity(
								"Trigger de " + tarefa.get("NOME").toString() + "-Atrasada-"
										+ tarefa.get("ID_AGENDA_ROBO").toString() + dataref,
								"group" + tarefa.get("ID_AGENDA_ROBO").toString())
						.startNow().build();
				// trigger.setEndTime(new Date());
				// trigger.setMisfireInstruction(Trigger.INSTRUCTION_DELETE_TRIGGER);
				try {
					// LogUtil.debug("agendei"+task_antiga+ trigger);
					getScheduler(user).scheduleJob(task_antiga, trigger);
					if (tarefa.get("MULTI_THREAD") != null
							&& tarefa.get("MULTI_THREAD").toString().equalsIgnoreCase("F")) {
						return; // nesse ponto se multithread agenda uma e sai
						// da execucao do metodo para ser chamado
						// novamente no fim da execucao;
					}

				} catch (final SchedulerException e) {
					LogUtil.exception(e, user);
				}
			}
		}
	}

	private Scheduler getScheduler(Usuario user) {
		return (Scheduler) getService("TarefasSheduler", user);
	}

	public boolean verificaExecucaoCorreta(final String id_tarefa, final Date datareferencia, Usuario user) {
		if (AgendaRoboManager.CONSOLELOG) {
			LogUtil.debug("verificando execucao correta para" + datareferencia, user);
		}

		GenericService s = (GenericService) getService("ExecucaoAgendadorService", user);

		GenericVO vo = s.createVO(user);

		vo.put("ID_AGENDA_ROBO", new Long(Integer.parseInt(id_tarefa)));
		vo.put("DATA_EXECUCAO", datareferencia);

		try {
			List items = new ArrayList();

			s.lista(vo, user);

			final Iterator it = items.iterator();
			if (it.hasNext()) {
				final Map dados = (Map) it.next();
				final Date datafim = (Date) dados.get("DATA_FIM_EXECUCAO");
				if (datafim == null) {
					if (AgendaRoboManager.CONSOLELOG) {
						LogUtil.debug("nao finalizou apenas", user);
					}
					return false;
				} else {
					if (AgendaRoboManager.CONSOLELOG) {
						LogUtil.debug("executou correto", user);
					}
					return true;
				}
			} else {
				if (AgendaRoboManager.CONSOLELOG) {
					LogUtil.debug("nao iniciou a execucao", user);
				}

				return false;
			}

		}

		catch (final Exception e) {

			LogUtil.exception(e, user);
			return false;
		}

	}

	public void execute(Usuario user) throws Exception {

		LogUtil.debug("Iniciando listener do Agendador de Tarefas Rodando em : " + (new Date()), user);

		Scheduler sc = getScheduler(user);

		JobDetail listenerTask = newJob(NotificationJob.class)
				.withIdentity("Listener de notificacoes com antecedencia", Scheduler.DEFAULT_GROUP)

				.build();

		listenerTask.getJobDataMap().put(Constants.RAIN_USER, user);
		CronTriggerImpl ct = new CronTriggerImpl();

		ct.setJobKey(listenerTask.getKey());
		ct.setName("Listener Programado da Agenda");
		try {
			ct.setCronExpression(user.getSystemProperty("ANTECEDENCIA_PERIODICIDADE"));
		} catch (Exception e) {
			LogUtil.debug("Erro de configuracao para propriedade do sistema AGENDADOR_PERIODICIDADE Com valor >"
					+ user.getSystemProperty("ANTECEDENCIA_PERIODICIDADE"), user);
			ct.setCronExpression("0 0/5 * * * ?");
		}
		// ct.setMisfireInstruction(Trigger.INSTRUCTION_SET_TRIGGER_COMPLETE);
		sc.scheduleJob(listenerTask, ct);

		listAndSchedule(user);

		LogUtil.debug("Finalizado listener Agendador de Tarefas", user);
	}

	public void listAndSchedule(Usuario user) {
		try {
			GenericService sAgenda = (GenericService) getService("SchedulerService", user);
			GenericVO voAgenda = sAgenda.createVO(user);

			String hoje = new DataFormat().fromTimestamp(user.getUtil().nowTimestamp());

			voAgenda.setFiltro("{gO:[{c:{f:'DATA_INICIO_AGENDA',o:'=',v1:'" + hoje
					+ "'}},{c:{f:'DATA_FIM_AGENDA',o:'=',v1:'" + hoje + "'}}, "
					+ "{gA:[{c:{f:'DATA_INICIO_AGENDA',o:'<',v1:'" + hoje + "'}},{c:{f:'DATA_FIM_AGENDA',o:'>',v1:'"
					+ hoje + "'}}]}," + "{gA:[{c:{f:'DATA_INICIO_AGENDA',o:'<',v1:'" + hoje
					+ "'}},{c:{f:'DATA_FIM_AGENDA',o:'nl'}}]}]}");

			voAgenda.setPaginate(false);
			List progs = sAgenda.lista(voAgenda, user);

			if (AgendaRoboManager.CONSOLELOG) {
				LogUtil.debug("TEMOS  " + progs.size() + " JOBS PARA AGENDAR", user);
			}
			validateJobs(progs, user);
			final Iterator robos = progs.iterator();
			while (robos.hasNext()) {
				final HashMap robo = (HashMap) robos.next();

				GenericVO tarefa = sAgenda.obtem((GenericVO) robo, user);

				if (tarefa.get("STATUS") == null || !tarefa.get("STATUS").toString().equals("S")) {
					if (AgendaRoboManager.CONSOLELOG) {
						LogUtil.debug("AgendaRobo - agendando tarefa : " + robo, user);
					}
					schedule(tarefa, user);
				}

			}

		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
	}

	public void reschedule(GenericVO vo, Usuario user) {
		((AgendaRoboManager) getService("AgendaRoboManager", user)).reschedule(vo, user);
	}

	public String getJobStatusById(int id, Usuario user) {

		// retorna = R: Running, A: Ativo, S : Stoped, P : Paused

		List<JobExecutionContext> jobs = new ArrayList<JobExecutionContext>();

		try {
			jobs = getScheduler(user).getCurrentlyExecutingJobs();
			for (JobExecutionContext job : jobs) {
				if (Integer.parseInt("" + job.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO")) == id) {
					if (getScheduler(user).getTriggerState(job.getTrigger().getKey()) == TriggerState.PAUSED) {
						return "P";
					} else {
						return "R";
					}
				}
			}
		} catch (SchedulerException e) {
			LogUtil.exception(e, user);
		}

		if (getJobKeysById(id, user).isEmpty())
			return "S";
		else
			return "A";
	}

	public void stopJobById(int id, Usuario user) {
		try {
			for (JobKey jk : getJobKeysById(id, user)) {
				getScheduler(user).interrupt(jk);
			}
		} catch (UnableToInterruptJobException e) {
			LogUtil.exception(e, user);
		}
	}

	public void toggleRunningJob(int id, boolean toggleRunning, Usuario user) {

		// responsvel por pausar ou resumir os Jobs, com base na
		// varivel toggleRunning onde true == resumir e false == pausar.

		try {
			if (toggleRunning) { // Resume o Job
				for (JobKey jk : getJobKeysById(id, user)) {

					getScheduler(user).resumeJob(jk);
				}
			} else { // Pausa o Job
				for (JobKey jk : getJobKeysById(id, user)) {
					getScheduler(user).pauseJob(jk);

				}
			}
		} catch (SchedulerException e) {
			LogUtil.exception(e, user);
		}
	}

	public JobExecutionContext getJobExecutionContextById(int id, Usuario user) {
		List<JobExecutionContext> jobs = new ArrayList<JobExecutionContext>();
		try {
			jobs = getScheduler(user).getCurrentlyExecutingJobs();
			for (JobExecutionContext job : jobs) {
				if (Integer.parseInt("" + job.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO")) == id) {
					return job;
				}
			}
		} catch (SchedulerException e) {
			LogUtil.exception(e, user);
		}
		return null;
	}

	public List<JobKey> getJobKeysById(int id, Usuario user) {

		// recupera o nome do Job e do respectivoGroup,
		// a partir da ID que est cadastrada no banco.

		List result = new ArrayList();
		try {
			List<String> jobGroups = getScheduler(user).getJobGroupNames();
			for (String jg : jobGroups) {
				if (!jg.equals("DEFAULT")) {
					for (JobKey jobKey : getScheduler(user).getJobKeys(groupEquals(jg))) {
						if (Integer.parseInt("" + getScheduler(user).getJobDetail(jobKey).getJobDataMap()
								.get("ID_AGENDA_ROBO")) == id) {
							result.add(jobKey);
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		return result;
	}

	public void validateJobs(List jobs, Usuario user) {

		// Valida os jobs existentes no banco com os da memria,
		// excluindo da memria os que no esto mais no banco.

		try {
			List<String> jobGroups = getScheduler(user).getJobGroupNames();
			for (String jg : jobGroups) {
				if (!jg.equals("DEFAULT")) {
					for (JobKey jobKey : getScheduler(user).getJobKeys(groupEquals(jg))) {
						boolean existe = false;
						for (int i = 0; i < jobs.size(); i++) {
							HashMap<String, Object> jobItem = (HashMap<String, Object>) jobs.get(i);
							if (jobItem.get("NOME").toString().equals(jobKey.getName())) {
								existe = true;
							}
						}
						if (!existe) {
							getScheduler(user).deleteJob(jobKey);
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
	}

	private void schedule(final HashMap tarefa, Usuario user) throws JobExecutionException {

		Scheduler sc = getScheduler(user);
		String cronString = "";

		try {
			/*
			 * Data: 10/08/2011 Podemos reagendar uma tarefa! By: Vinicuis Ramos
			 */
			/*
			 * if (root.containsKey(tarefa.get("ID_AGENDA_ROBO"))) { if
			 * (AgendaRoboManager.CONSOLELOG) { LogUtil.debug("Tarefa ignorada "
			 * + tarefa); } return; } else {
			 * root.put(tarefa.get("ID_AGENDA_ROBO"), ""); }
			 */
			Class exec = null;
			if (tarefa.get("MULTI_THREAD") != null && tarefa.get("MULTI_THREAD").toString().equalsIgnoreCase("T")) {
				exec = AgendaRoboExecutor.class;
			} else {
				exec = StatefulAgendaRoboExecutor.class;
			}

			String taskName = tarefa.get("NOME").toString() + "-Atual-" + tarefa.get("ID_AGENDA_ROBO").toString();

			String taskGroup = "group" + tarefa.get("ID_AGENDA_ROBO").toString();

			final JobDetail task = newJob(exec).withIdentity(taskName, taskGroup).build();
			final JobDataMap params = task.getJobDataMap();

			final Iterator i = tarefa.keySet().iterator();
			while (i.hasNext()) {
				final Object key = i.next();
				params.put((String) key, tarefa.get(key));
			}

			if (AgendaRoboManager.CONSOLELOG) {
				LogUtil.debug("Tarefa sendo agendada " + tarefa, user);
			}

			try {
				cronString = tarefa.get("CRON_EXPRESSION").toString();
			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

			final CronTriggerImpl ct = new CronTriggerImpl();
			// ct.setMisfireInstruction(Trigger.INSTRUCTION_SET_TRIGGER_COMPLETE);
			final Timestamp agora = user.getUtil().nowTimestamp();

			ct.setJobKey(task.getKey());
			ct.setName(taskName);
			ct.setGroup(taskGroup);
			ct.setJobName(taskName);
			ct.setJobGroup(taskGroup);
			try {
				ct.setCronExpression(cronString);
			} catch (Exception e) {
				LogUtil.exception(e, user);
				/*
				 * TODO: Inserir mensagem de erro.
				 */
				if (AgendaRoboManager.CONSOLELOG) {
					LogUtil.debug("Expresso do Cron no  vlida para a tarefa: " + tarefa, user);
				}
				return;
			}

			params.put("DATA_AGENDAMENTO", agora);
			if ((Date) tarefa.get("DATA_INICIO_AGENDA") == null) {
				Log(tarefa.get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
						"Agenda Invlida - Data de incio da agenda no cadastrada para tarefa: "
								+ tarefa.get("ID_AGENDA_ROBO") + "; Programa: " + tarefa.get("NOME"),
						AgendaRoboManager.LOG_ERRO_EXECUCAO, user);

				if (AgendaRoboManager.CONSOLELOG) {
					LogUtil.debug("Tarefa ignorada por data de incio nula. Tarefa: " + tarefa, user);
				}
				return;
			}

			/*
			 * Data: 09/08/2011 Se a data de trmino do agendamento no estiver
			 * cadastrada, o agendamento deve ser contnuo. Por isso esta parte
			 * foi comentada.
			 * 
			 * if ((Date) tarefa.get("DATA_FIM_AGENDA") == null) {
			 * AgendaRoboManager.getInstance(user).Log(
			 * tarefa.get("ID_AGENDA_ROBO").toString(),
			 * System.currentTimeMillis(),
			 * "Agenda Invlida - Data do trmino do agendamento no cadastrada para tarefa: "
			 * + tarefa.get("ID_AGENDA_ROBO") + "; Programa: " +
			 * tarefa.get("NOME"), AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
			 * 
			 * if (AgendaRoboManager.CONSOLELOG) { System.out.println(
			 * "Tarefa ignorada. Data do trmino do agendamento est nula. Tarefa: "
			 * + tarefa); } return; }
			 */
			if (tarefa.get("DATA_FIM_AGENDA") != null
					&& ((Date) tarefa.get("DATA_FIM_AGENDA")).before(((Date) tarefa.get("DATA_INICIO_AGENDA")))) {
				Log(tarefa.get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
						"Agenda Invlida - Data de trmino do agendamento menor que a data de incio do agendamento. Tarefa: "
								+ tarefa.get("ID_AGENDA_ROBO") + "; Programa: " + tarefa.get("NOME"),
						AgendaRoboManager.LOG_ERRO_EXECUCAO, user);

				if (AgendaRoboManager.CONSOLELOG) {
					System.out.println("Tarefa ignorada data do ltimo agendamento anterior a data de incio" + tarefa);
				}
				return;
			}

			if (((Date) tarefa.get("DATA_INICIO_AGENDA")).after(agora)) {
				ct.setStartTime((Date) tarefa.get("DATA_INICIO_AGENDA"));
				// Garantir execues anteriores
			}

			// Eliminando tarefas que estao expiradas
			if (tarefa.get("DATA_FIM_AGENDA") != null && ((Date) tarefa.get("DATA_FIM_AGENDA")).before(agora)) {
				if (AgendaRoboManager.CONSOLELOG) {
					LogUtil.debug(
							"Sai da execucao pois estava expirado" + tarefa.get("DATA_FIM_AGENDA") + " agora" + agora,
							user);
					Log(tarefa.get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
							" Agenda " + tarefa.get("NOME").toString() + " fora da validade.",
							AgendaRoboManager.LOG_AGENDAMENTO, user);
				}
				JobDetail jd = sc.getJobDetail(jobKey(taskName, taskGroup));
				// Retirar a tarefa do agendamento
				if (jd != null)
					sc.deleteJob(jd.getKey());
				return;
			}

			if (tarefa.get("DATA_FIM_AGENDA") != null) {
				if (AgendaRoboManager.CONSOLELOG) {
					LogUtil.debug("setando meu final para" + tarefa.get("DATA_FIM_AGENDA"), user);
				}
				ct.setEndTime((Date) tarefa.get("DATA_FIM_AGENDA"));
			}

			try {
				// Buscando o usurio que ir executar o JOB
				GenericService userService = (GenericService) getService("UsuarioService", user);
				GenericService schedulerService = (GenericService) getService("SchedulerService", user);

				Long idAgendaRobo = (Long) tarefa.get("ID_AGENDA_ROBO");
				GenericVO agendaVO = schedulerService.createVO(user);
				agendaVO.put("ID_AGENDA_ROBO", idAgendaRobo);
				agendaVO = schedulerService.obtem(agendaVO, user);

				Long idUser = (Long) agendaVO.get("ID_USUARIO_CADASTRO");
				GenericVO userVO = userService.createVO(user);
				userVO.put("ID_USUARIO", idUser);
				userVO = (GenericVO) userService.obtem(userVO, user);
				Usuario muser = new Usuario(user);
				muser = userService.bindUser(muser, userVO, false);

				// Adicionando o usurio que ir executar
				params.put(Constants.RAIN_USER, muser);

				params.put("DATA_EXECUCAO", ct.getNextFireTime());
				String endtime = "";

				if (ct.getEndTime() == null) {
					endtime = " o sistema for parado";
				} else {
					endtime = user.getUtil().formatDate(ct.getEndTime(), "dd/MM/yyyy HH:mm");
				}

				// Atualizando os parmetros da tarefa
				task.getJobDataMap().putAll(params);

				// Agendando ou reagendando a tarefa
				JobDetail jd = sc.getJobDetail(jobKey(taskName, taskGroup));
				if (jd == null)
					sc.scheduleJob(task, ct);
				else {
					sc.deleteJob(jd.getKey());
					sc.scheduleJob(task, ct);
					// sc.rescheduleJob(ct.getName(), ct.getGroup(), ct);
				}

				Log(tarefa.get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
						"Tarefa agendada com sucesso para rodar "
								+ user.getUtil().formatDate(ct.getNextFireTime(), "dd/MM/yyyy HH:mm") + " at "
								+ endtime,
						AgendaRoboManager.LOG_AGENDAMENTO, user);

				if (AgendaRoboManager.CONSOLELOG) {
					LogUtil.debug("Tarefa agendada com sucesso" + tarefa, user);
				}

				// Executando tarefas anteriores
				if (((Date) tarefa.get("DATA_INICIO_AGENDA")).before(agora)) {
					// Se a data de trmino do agendamento for nula ou
					// for posterior a data atual e tivermos que garantir
					// execues anteriores
					// ento precisamos garantir as execues anteriores.
					ct.setStartTime(agora);
					if (((Date) tarefa.get("DATA_FIM_AGENDA") == null
							|| ((Date) tarefa.get("DATA_FIM_AGENDA")).after(agora))
							&& (tarefa.get("NUM_EXEC_RETROATIVAS") != null
									&& !tarefa.get("NUM_EXEC_RETROATIVAS").toString().equalsIgnoreCase("0"))) {
						garanteExecucoesAnteriores(tarefa, agora, ct, user);
						// Se a ltima vez que foi agendada a tarefa for
						// anterior ao fim da tarefa,
						// precisamos garantir as execues anteriores.
					} else if (((Date) tarefa.get("DATA_ULTIMO_AGENDAMENTO")) != null
							&& ((Date) tarefa.get("DATA_ULTIMO_AGENDAMENTO"))
									.before((Date) tarefa.get("DATA_FIM_AGENDA"))
							&& (tarefa.get("NUM_EXEC_RETROATIVAS") != null
									&& ((Long) tarefa.get("NUM_EXEC_RETROATIVAS")) > 0)) {
						garanteExecucoesAnteriores(tarefa, (Timestamp) tarefa.get("DATA_FIM_AGENDA"), ct, user);
					}
				}
			} catch (final java.lang.UnsupportedOperationException ex) {
				Log(tarefa.get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
						"configurao de trigger invlida da tarefa " + tarefa.get("ID_AGENDA_ROBO").toString()
								+ " com data de execuo "
								+ user.getUtil().formatDate((Date) tarefa.get("DATA_EXECUCAO"), "dd/MM/yyyy HH:mm")
								+ cronString,
						AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
				throw new JobExecutionException(ex);
			} catch (final Exception ex) {
				LogUtil.exception(ex, user);
				throw new JobExecutionException(ex);
			}
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			Log(tarefa.get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
					"Erro na execucao da tarefa " + tarefa.get("ID_AGENDA_ROBO").toString() + " com data de execuo "
							+ user.getUtil().formatDate((Date) tarefa.get("DATA_EXECUCAO"), "dd/MM/yyyy HH:mm") + " >> "
							+ e.getMessage(),
					AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
			throw new JobExecutionException(e);
		}
	}
}
