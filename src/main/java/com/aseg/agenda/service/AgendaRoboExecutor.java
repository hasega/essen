package com.aseg.agenda.service;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import javax.script.Bindings;
import javax.script.ScriptException;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.quartz.UnableToInterruptJobException;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.util.DataFormat;
import com.aseg.vo.GenericVO;

public class AgendaRoboExecutor extends ShedulerJob implements StatefulJob {

	private Thread thread;

	public void execute(final JobExecutionContext ctx) throws JobExecutionException {

		thread = Thread.currentThread();

		StartExecution(ctx);

		JobDataMap jobMap = ctx.getJobDetail().getJobDataMap();
		Usuario user = (Usuario) jobMap.get(Constants.RAIN_USER);

		try {
			String programa = user.getUtil().convertStreamToString((byte[]) jobMap.get("PROGRAMA"), user);

			final Bindings bindings = engine.createBindings();

			bindings.put("user", user);
			bindings.put("sm", this);
			bindings.put("util", user.getUtil());
			bindings.put("df", new DataFormat());
			//bindings.put("cmp", new TagInputExt());

			Object r = null;

			r = engine.eval(programa, bindings);
		} catch (final ScriptException e) {
			/*
			 * if (e.getMessage().indexOf("BusinessException") > -1) { throw new
			 * BusinessException(e.getCause().getCause() .getMessage().trim(),
			 * e.getCause().getCause()); }
			 */
			if (LogUtil.debugEnabled(user)) {
				LogUtil.exception(e, user);
				LogUtil.error("Ocorreu um erro na execuo do programa '" + jobMap.get("NOME").toString()
						+ "'. Reveja o seu script. ID_AGENDA: " + jobMap.get("ID_AGENDA_ROBO").toString());
			}
			Log(jobMap.get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
					"Erro na execucao da tarefa " + ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString()
							+ " com data de execuo "
							+ user.getUtil().formatDate((Date) jobMap.get("DATA_EXECUCAO"), "dd/MM/yyyy HH:mm") + " >> "
							+ e.getMessage(),
					AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
			throw new JobExecutionException(e);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			LogUtil.exception(e1, user);
		}

		FinalizeExecution(ctx);
	}

	public void FinalizeExecution(final JobExecutionContext ctx) throws JobExecutionException {

		Usuario user = (Usuario) ctx.getJobDetail().getJobDataMap().get(Constants.RAIN_USER);
		try {
			GenericService execSchedService = (GenericService) getService("ExecucaoAgendadorService", user);
			GenericVO vo = execSchedService.createVO(user);
			vo.put("ID_EXECUCAO",
					new Integer(Integer.parseInt(ctx.getJobDetail().getJobDataMap().get("ID_EXECUCAO").toString())));
			// Load database registry. Don't override the values.
			vo = execSchedService.obtem(vo, user);
			vo.put("EXECUTANDO", "F");
			vo.put("DATA_FIM_EXECUCAO", user.getUtil().nowTimestamp());

			try {
				vo.setAnulate(false);
				execSchedService.altera(vo, user);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				LogUtil.exception(e1, user);
				Log(ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
						"Tarefa finalizada. Erro na gravao da data de finalizao. "
								+ ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString()
								+ " com data de execuo "
								+ user.getUtil().formatDate(
										(Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO"),
										"dd/MM/yyyy HH:mm")
								+ " >>> " + e1.getMessage(),
						AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
				throw new JobExecutionException(e1);
			}

			Log(ctx.getJobDetail()
					.getJobDataMap().get(
							"ID_AGENDA_ROBO")
					.toString(),
					System.currentTimeMillis(),
					"Tarefa com execuo para "
							+ user.getUtil().formatDate((Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO"),
									"dd/MM/yyyy HH:mm")
							+ " foi finalizada ",
					AgendaRoboManager.LOG_FIM_EXECUCAO, user);
			final HashMap task = (HashMap) ctx.getJobDetail().getJobDataMap().get("TAREFA");
			if (task != null) {
				;
			}
			/*
			 * It is not required anymore! Date: 10/08/2011 By: Vinicius Ramos
			 * 
			 * AgendadorListenerTask agendadorListenerTask = new
			 * AgendadorListenerTask();
			 * 
			 * agendadorListenerTask.garanteExecucoesAnteriores( task, new
			 * Date(), (CronTriggerBean) ctx.getJobDetail().getJobDataMap()
			 * .get("TRIGGER"), user);
			 */
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			Log(ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
					"Erro na finalizao da tarefa " + ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString()
							+ " com data de execuo "
							+ user.getUtil().formatDate((Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO"),
									"dd/MM/yyyy HH:mm")
							+ " >>> " + e.getMessage(),
					AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
			throw new JobExecutionException(e);

		}
	}

	/**
	 * @param ctx
	 * @throws JobExecutionException
	 * @throws Exception
	 */
	public void StartExecution(final JobExecutionContext ctx) throws JobExecutionException {

		Usuario user = (Usuario) ctx.getJobDetail().getJobDataMap().get(Constants.RAIN_USER);

		try {

			GenericService s = (GenericService) getService("ExecucaoAgendadorService", user);

			ctx.getJobDetail().getJobDataMap().put("ID_EXECUCAO", null);
			Date agora = null;
			if (ctx.getJobDetail().getJobDataMap().get("ATRASADO") != null
					&& ctx.getJobDetail().getJobDataMap().get("ATRASADO").equals(Constants.true_str)) {
				agora = (Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO");
			} else {
				agora = ctx.getScheduledFireTime();
			}

			GenericVO vo = s.createVO(user);
			vo.put("ID_AGENDA_ROBO", new Long((ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString())));
			vo.put("EXECUTANDO", "T");
			vo.put("DATA_AGENDAMENTO", ctx.getJobDetail().getJobDataMap().get("DATA_AGENDAMENTO"));
			vo.put("DATA_INICIO_EXECUCAO", ctx.getJobDetail().getJobDataMap().get("DATA_INICIO_EXECUCAO"));
			vo.put("DATA_FIM_EXECUCAO", user.getUtil().nowTimestamp());

			ctx.getJobDetail().getJobDataMap().put("ID_EXECUCAO", new Long(s.inclui(vo, user)));

			GenericService serviceAgendador = (GenericService) getService("SchedulerService", user);

			GenericVO voAgendador = serviceAgendador.createVO(user);
			voAgendador.put("DATA_ULTIMO_AGENDAMENTO", user.getUtil().nowTimestamp());
			voAgendador.put("ID_AGENDA_ROBO",
					new Long((ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString())));
			voAgendador.setAnulate(false);
			serviceAgendador.altera(voAgendador, user);

			Log(ctx.getJobDetail()
					.getJobDataMap().get(
							"ID_AGENDA_ROBO")
					.toString(),
					System.currentTimeMillis(),
					"Tarefa com execuo para " + user.getUtil().formatDate(
							(Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO"), "dd/MM/yyyy HH:mm")
							+ " foi iniciada ",
					AgendaRoboManager.LOG_INICIO_EXECUCAO, user);
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			Log(ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
					"Erro na inicializao da tarefa "
							+ ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString()
							+ " com data de execuo "
							+ user.getUtil().formatDate((Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO"),
									"dd/MM/yyyy HH:mm")
							+ " >>> " + e.getMessage(),
					AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
			throw new JobExecutionException(e);
		}
	}

	public void interrupt() throws UnableToInterruptJobException {
		if (thread != null) {
			thread.interrupt();
		}
	}
}
