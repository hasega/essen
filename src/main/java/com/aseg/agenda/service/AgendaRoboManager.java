package com.aseg.agenda.service;

import java.util.Properties;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdScheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;

public class AgendaRoboManager extends ServiceImpl {

	public static boolean CONSOLELOG = true;
	private static AgendaRoboManager instance = null;
	public static String LOG_AGENDAMENTO = "AGENDAMENTO";
	public static String LOG_ERRO_EXECUCAO = "ERROEXECUCAO";
	public static String LOG_FIM_EXECUCAO = "TERMINOEXECUCAO";
	public static String LOG_INICIO_EXECUCAO = "INICIOEXECUCAO";

	public static AgendaRoboManager getInstance(final Usuario user) {
		if (instance == null) {
			instance = new AgendaRoboManager();
		}
		return instance;

	}

	public void getJobByName(String name) {

	}

	private AgendaRoboManager() {
	}

	public Scheduler getScheduler(Usuario user) throws SchedulerException {
		return (StdScheduler) getService("TarefasSheduler", user);
	}

	public AgendaRoboManager(final Usuario user) {
		try {

			final Properties prop = new Properties();

			prop.setProperty("org.quartz.scheduler.instanceName", "Tarefas" + user.getSystem());
			prop.setProperty("org.quartz.scheduler.instanceId", "shed1" + user.getSystem());

			prop.setProperty("org.quartz.scheduler.rmi.export", "false");
			prop.setProperty("org.quartz.scheduler.rmi.proxy", "false");
			prop.setProperty("org.quartz.scheduler.skipUpdateCheck", "true");

			prop.setProperty("org.quartz.threadPool.class", "org.quartz.simpl.ThreadPool");
			// prop.setProperty("org.quartz.threadPool.class",
			// "org.quartz.simpl.SimpleThreadPool");
			prop.setProperty("org.quartz.threadPool.threadCount", "10");
			prop.setProperty("org.quartz.threadPool.threadPriority", "1");
			prop.setProperty("org.quartz.jobStore.misfireThreshold", "86400000"); // um
																					// dia

			StdSchedulerFactory sf = new StdSchedulerFactory(prop);

			/*
			 * final Properties propa = new Properties();
			 * propa.setProperty("org.quartz.scheduler.instanceName", "Agenda" +
			 * user.getSystem());
			 * propa.setProperty("org.quartz.scheduler.instanceId", "shed2" +
			 * user.getSystem());
			 * propa.setProperty("org.quartz.threadPool.class",
			 * "org.quartz.simpl.SimpleThreadPool");
			 * propa.setProperty("org.quartz.threadPool.threadCount", "1");
			 * propa.setProperty("org.quartz.threadPool.threadPriority", "1");
			 * 
			 * propa.setProperty("org.quartz.scheduler.rmi.export", "false");
			 * propa.setProperty("org.quartz.scheduler.rmi.proxy", "false");
			 * propa.setProperty("org.quartz.scheduler.skipUpdateCheck",
			 * "true");
			 * 
			 * StdSchedulerFactory sfa = new StdSchedulerFactory(propa);
			 */

			AgendadorListenerTask t = new AgendadorListenerTask();

			Usuario muser = Usuario.getSystemUser();

			t.execute(muser);
			sf.initialize();

			// sfa.initialize();

			LogUtil.debug("Listener do Agendador de Tarefas Rodando com "
					+ this.getScheduler(muser).getMetaData().getThreadPoolSize() + " theads", user);

		} catch (final Exception e) {
			LogUtil.exception(e, user);
		}
	}

	public boolean reschedule(GenericVO vo, Usuario user) {
		try {
			// Guardar Super usurio para executar agendamento
			GenericService userService = (GenericService) getService("UsuarioService", user);
			GenericVO userVO = userService.createVO(user);
			userVO.put("SUPER_USER", "T");
			userVO = (GenericVO) userService.lista(userVO, user).get(0);
			Usuario muser = new Usuario(user);
			muser = userService.bindUser(muser, userVO, false);
			AgendadorListenerTask t = new AgendadorListenerTask();

			t.listAndSchedule(muser);

			return true;
		} catch (Exception e) {
			LogUtil.exception(e, user);
			return false;
		}
	}

}
