package com.aseg.agenda.service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;

import javax.script.Bindings;
import javax.script.ScriptException;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.util.DataFormat;
import com.aseg.vo.GenericVO;

public class StatefulAgendaRoboExecutor extends ShedulerJob implements StatefulJob {

	private Thread thread;

	@Override
	public void execute(final JobExecutionContext ctx) throws JobExecutionException {

		thread = Thread.currentThread();

		JobDataMap jobMap = ctx.getJobDetail().getJobDataMap();
		Usuario user = (Usuario) jobMap.get(Constants.RAIN_USER);
		StartExecution(ctx);

		String programa = "";
		try {
			programa = user.getUtil().convertStreamToString((byte[]) jobMap.get("PROGRAMA"), user);

			final Bindings bindings = engine.createBindings();

			bindings.put("user", user);
			bindings.put("sm", this);
			bindings.put("util", user.getUtil());
			bindings.put("df", new DataFormat());
		//	bindings.put("cmp", new TagInputExt());

			Object r = null;

			r = engine.eval(programa, bindings);
		} catch (final ScriptException e) {
			/*
			 * if (e.getMessage().indexOf("BusinessException") > -1) { throw new
			 * BusinessException(e.getCause().getCause() .getMessage().trim(),
			 * e.getCause().getCause()); }
			 */
			if (LogUtil.debugEnabled(user)) {
				LogUtil.exception(e, user);

				LogUtil.debug(programa, user);
				LogUtil.error("Ocorreu um erro na execuo do programa '" + jobMap.get("NOME").toString()
						+ "'. Reveja o seu script. ID_AGENDA: " + jobMap.get("ID_AGENDA_ROBO").toString());
			}
			Log(jobMap.get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
					"Erro na execucao da tarefa " + ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString()
							+ " com data de execuo "
							+ user.getUtil().formatDate((Date) jobMap.get("DATA_EXECUCAO"), "dd/MM/yyyy HH:mm") + " >> "
							+ e.getMessage(),
					AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
			throw new JobExecutionException(e);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			LogUtil.exception(e1, user);
		}

		FinalizeExecution(ctx, user);

	}

	public void FinalizeExecution(final JobExecutionContext ctx, Usuario user) throws JobExecutionException {
		try {
			GenericService execSchedService = (GenericService) getService("ExecucaoAgendadorService", user);
			GenericVO vo = execSchedService.createVO(user);
			vo.put("ID_EXECUCAO",
					new Integer(Integer.parseInt(ctx.getJobDetail().getJobDataMap().get("ID_EXECUCAO").toString())));
			// Load database registry. Don't override the values.
			vo = execSchedService.obtem(vo, user);
			vo.put("EXECUTANDO", 'F');
			vo.put("DATA_FIM_EXECUCAO", new Timestamp(System.currentTimeMillis()));

			try {
				execSchedService.altera(vo, user);
			} catch (Exception e1) {
				LogUtil.exception(e1, user);
				Log(ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
						"Tarefa finalizada. Erro na gravao da data de finalizao. "
								+ ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString()
								+ " com data de execuo "
								+ user.getUtil().formatDate(
										(Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO"),
										"dd/MM/yyyy HH:mm")
								+ " >>> " + e1.getMessage(),
						AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
				throw new JobExecutionException(e1);
			}

			Log(ctx.getJobDetail()
					.getJobDataMap().get(
							"ID_AGENDA_ROBO")
					.toString(),
					System.currentTimeMillis(),
					"Tarefa com execuo para "
							+ user.getUtil().formatDate((Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO"),
									"dd/MM/yyyy HH:mm")
							+ " foi finalizada ",
					AgendaRoboManager.LOG_FIM_EXECUCAO, user);
			final HashMap task = (HashMap) ctx.getJobDetail().getJobDataMap().get("TAREFA");
			if (task != null) {
				;
			}
			/*
			 * It is not required anymore! Date: 10/08/2011 By: Vinicius Ramos
			 * 
			 * AgendadorListenerTask agendadorListenerTask = new
			 * AgendadorListenerTask();
			 * 
			 * agendadorListenerTask.garanteExecucoesAnteriores( task, new
			 * Date(), (CronTriggerBean) ctx.getJobDetail().getJobDataMap()
			 * .get("TRIGGER"), user);
			 */
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			Log(ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
					"Erro na finalizao da tarefa " + ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString()
							+ " com data de execuo "
							+ user.getUtil().formatDate((Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO"),
									"dd/MM/yyyy HH:mm")
							+ " >>> " + e.getMessage(),
					AgendaRoboManager.LOG_ERRO_EXECUCAO, user);
			throw new JobExecutionException(e);

		}
	}

	public void interrupt() {
		if (thread != null) {
			thread.interrupt();
		}
	}

	/**
	 * @param ctx
	 * @throws JobExecutionException
	 * @throws Exception
	 */
	public void StartExecution(final JobExecutionContext ctx) throws JobExecutionException {

		Usuario user = (Usuario) ctx.getJobDetail().getJobDataMap().get(Constants.RAIN_USER);

		try {
			GenericService execSchedService = (GenericService) getService("ExecucaoAgendadorService", user);
			Date dataExec = null;
			if (ctx.getJobDetail().getJobDataMap().get("ATRASADO") != null
					&& ctx.getJobDetail().getJobDataMap().get("ATRASADO").equals(Constants.true_str)) {
				dataExec = (Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO");
			} else {
				dataExec = ctx.getScheduledFireTime();
			}

			GenericVO vo = execSchedService.createVO(user);
			vo.put("ID_AGENDA_ROBO", new Long((ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString())));
			vo.put("EXECUTANDO", "T");
			vo.put("INICIADO", "T");
			vo.put("DATA_EXECUCAO", user.getUtil().df.dateToTimestamp(dataExec));
			vo.put("DATA_AGENDAMENTO", user.getUtil().df
					.dateToTimestamp((Date) ctx.getJobDetail().getJobDataMap().get("DATA_AGENDAMENTO")));
			Timestamp data_inicio_exec = user.getUtil().nowTimestamp();
			if ((Date) ctx.getJobDetail().getJobDataMap().get("DATA_INICIO_EXECUCAO") != null)
				data_inicio_exec = user.getUtil().df
						.dateToTimestamp((Date) ctx.getJobDetail().getJobDataMap().get("DATA_INICIO_EXECUCAO"));
			vo.put("DATA_INICIO_EXECUCAO", data_inicio_exec);
			String idExec = execSchedService.inclui(vo, user);
			ctx.getJobDetail().getJobDataMap().put("ID_EXECUCAO", idExec);

			Log(ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
					"Tarefa com execuo para " + user.getUtil().formatDate(dataExec, "dd/MM/yyyy HH:mm")
							+ " foi iniciada ",
					AgendaRoboManager.LOG_INICIO_EXECUCAO,
					(Usuario) ctx.getJobDetail().getJobDataMap().get(Constants.RAIN_USER));

		} catch (final Exception e) {
			LogUtil.exception(e, user);
			Log(ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString(), System.currentTimeMillis(),
					"Erro na inicializao da tarefa "
							+ ctx.getJobDetail().getJobDataMap().get("ID_AGENDA_ROBO").toString()
							+ " com data de execuo "
							+ user.getUtil().formatDate((Date) ctx.getJobDetail().getJobDataMap().get("DATA_EXECUCAO"),
									"dd/MM/yyyy HH:mm")
							+ " >>> " + e.getMessage(),
					AgendaRoboManager.LOG_ERRO_EXECUCAO,
					(Usuario) ctx.getJobDetail().getJobDataMap().get(Constants.RAIN_USER));
			throw new JobExecutionException(e);
		}

	}
}
