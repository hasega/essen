package com.aseg.agenda.service;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.Trigger.CompletedExecutionInstruction;
import org.quartz.TriggerListener;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;

public class AgendadorTriggerListener extends ServiceImpl implements TriggerListener {
	String name;

	public AgendadorTriggerListener() {
	}

	public AgendadorTriggerListener(final String name) {
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @see org.quartz.TriggerListener#triggerComplete(org.quartz.Trigger,
	 *      org.quartz.JobExecutionContext, int)
	 */
	public void triggerComplete(final Trigger trigger, final JobExecutionContext context,
			final int triggerInstructionCode) {
		/*
		 * Map items = ((Map)context.getJobDetail().getJobDataMap());
		 * 
		 * if(items.get("ATRASADO")!=null) { System.err.println("Falha da
		 * trigger" + name + " says: \"Trigger '"+trigger.getFullName()+"'
		 * Fired!\""+trigger.getEndTime()); try {
		 * context.getScheduler().unscheduleJob
		 * (trigger.getName(),trigger.getGroup()); } catch (SchedulerException
		 * e) { e.printStackTrace(); } }
		 */
	}

	/**
	 * @see org.quartz.TriggerListener#triggerFired(org.quartz.Trigger,
	 *      org.quartz.JobExecutionContext)
	 */
	public void triggerFired(final Trigger trigger, final JobExecutionContext context) {

		Usuario user = (Usuario) context.getJobDetail().getJobDataMap().get(Constants.RAIN_USER);

		if (AgendaRoboManager.CONSOLELOG) {
			System.err.println("Listener " + name + " says: \"Trigger '" + trigger.getDescription() + "' Fired!\""
					+ trigger.getEndTime());
		}
		final Map items = context.getJobDetail().getJobDataMap();

		final Set i = items.keySet();
		final Iterator a = i.iterator();
		Date agora = new Date(System.currentTimeMillis());
		while (a.hasNext()) {
			final String b = a.next().toString();
			// LogUtil.debug.println(b+">>>>"+items.get(b));
			if (b.equals("DATA_EXECUCAO")) {
				agora = (Date) items.get(b);
			}
		}

		if (items.containsKey("ID_AGENDA_ROBO")) {
			final String id = items.get("ID_AGENDA_ROBO").toString();
			try {

				GenericService s = (GenericService) getService("AgendadorService", user);

				GenericVO vo = s.createVO(user);
				vo.put("ID_AGENDA_ROBO", new Long(Integer.parseInt(id)));
				vo.put("DATA_ULTIMO_AGENDAMENTO", user.getUtil().nowTimestamp());

				s.altera(vo, user);

				// Date agora = new Date(System.currentTimeMillis());
				// final DAOTemplate tem = new DAOTemplate(
				// ConnectionManager.getInstance(null).getDataSource(null),
				// null, null);
				// tem.update(
				// "UPDATE M_AGENDA_ROBO set DATA_ULTIMO_AGENDAMENTO = ? where
				// ID_AGENDA_ROBO =?",
				// new Object[] { agora, new Integer(Integer.parseInt(id)) });

				if (items.get("ATRASADO") == null) {
					items.put("DATA_EXECUCAO", agora);
				}

				if (AgendaRoboManager.CONSOLELOG) {
					LogUtil.debug("atualizei o ultimo agendamento de " + id, user);
				}
			} catch (final Exception ex1) {
				LogUtil.exception(ex1, user);
			}
		}
	}

	/**
	 * @see org.quartz.TriggerListener#triggerMisfired(org.quartz.Trigger)
	 */
	public void triggerMisfired(final Trigger trigger) {
		System.err.println("Falha da trigger " + name + " says: \"Trigger '" + trigger.getDescription() + "' Fired!\""
				+ trigger.getEndTime());
	}

	/**
	 * @see org.quartz.TriggerListener#vetoJobExecution(org.quartz.Trigger,
	 *      org.quartz.JobExecutionContext)
	 */
	public boolean vetoJobExecution(final Trigger trigger, final JobExecutionContext context) {
		return false;
	}

	@Override
	public void triggerComplete(Trigger arg0, JobExecutionContext arg1, CompletedExecutionInstruction arg2) {
		// TODO Auto-generated method stub

	}
}