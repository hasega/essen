package com.aseg.agenda;

import java.util.Calendar;
import java.util.Date;

/**
 * Title: Calendar
 * 
 * @author: Asega
 * @version 1.0
 * @extends Object
 */
public class Calendario extends Object {

	private Calendar calendar = null;
	private String navtip = "d";
	// private DateFormatSymbols dateFormatSymb = new DateFormatSymbols();
	private Calendar calAtual = null;

	/**
	 * Construtor sem parmetros setada a data e hora atual
	 */
	public Calendario() {
		calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 12);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		calAtual = Calendar.getInstance();
		// calAtual.set(Calendar.HOUR_OF_DAY, 12);
		// calAtual.set(Calendar.MINUTE, 0);
		// calAtual.set(Calendar.SECOND, 0);
		// calAtual.set(Calendar.MILLISECOND, 0);

	}

	/**
	 * Altera o valor da varivel
	 * 
	 * @param String
	 *            Valor para a varivel dia em Calendar
	 */
	public void setDia(String dia) {
		calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dia));
	}

	/**
	 * Altera o valor da varivel
	 * 
	 * @param String
	 *            Valor para a varivel mes em Calendar
	 */
	public void setMes(String mes) {
		calendar.set(Calendar.MONTH, Integer.parseInt(mes));
	}

	/**
	 * Altera o valor da varivel
	 * 
	 * @param String
	 *            Valor para a varivel ano em Calendar
	 */
	public void setAno(String ano) {
		calendar.set(Calendar.YEAR, Integer.parseInt(ano));
	}

	/**
	 * Altera o valor da varivel
	 * 
	 * @param String
	 *            Valor para a varivel semana em Calendar
	 */
	public void setSemana(String sem) {
		calendar.set(Calendar.WEEK_OF_MONTH, Integer.parseInt(sem));
	}

	/**
	 * Altera o valor da varivel
	 * 
	 * @param String
	 *            Valor para a varivel semana em Calendar
	 */
	public void setHora(String sem) {
		calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sem));
	}

	/**
	 * Altera o valor da varivel
	 * 
	 * @param String
	 *            Valor para a varivel semana em Calendar
	 */
	public void setMinuto(String sem) {
		calendar.set(Calendar.MINUTE, Integer.parseInt(sem));
	}

	/**
	 * Altera o valor da varivel
	 * 
	 * @param String
	 *            Valor para a varivel semana em Calendar
	 */
	public void setSegundo(String sem) {
		calendar.set(Calendar.MILLISECOND, Integer.parseInt(sem));
	}

	/**
	 * Altera o valor da varivel
	 * 
	 * @param String
	 *            Valor para a varivel time em Calendar
	 */
	public void setTime(String time) {
		long timeL = 0;
		try {
			timeL = Long.parseLong(time) * 1000L;
		} catch (Exception e) {

			return;
		}

		calendar.setTime(new Date(timeL));
	}

	/**
	 * Altera o valor da varivel
	 * 
	 * @param String
	 *            Valor para a varivel navtip
	 */
	public void setNavtip(String navtip) {
		this.navtip = navtip;
	}

	/**
	 * Retorna o valor da varivel time em Calendar
	 * 
	 * @return long
	 */
	public long getTime() {
		return calendar.getTime().getTime() / 1000L;
	}

	/**
	 * Retorna o valor da varivel dia em Calendar
	 * 
	 * @return int
	 */
	public int getDia() {
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Retorna o valor da varivel dia em Calendar
	 * 
	 * @return int
	 */
	public int getSemana() {
		return calendar.get(Calendar.WEEK_OF_MONTH);
	}

	public int getUltimodiadomes() {
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Retorna o valor da varivel mes em Calendar
	 * 
	 * @return int
	 */
	public int getMes() {
		return calendar.get(Calendar.MONTH);
	}

	/**
	 * Retorna o valor da varivel ano em Calendar
	 * 
	 * @return int
	 */
	public int getAno() {
		return calendar.get(Calendar.YEAR);
	}

	/**
	 * Retorna o valor da varivel timeAtual
	 * 
	 * @return long
	 */
	public long getTimeatual() {
		int hora = calAtual.get(Calendar.HOUR_OF_DAY);
		int minuto = calAtual.get(Calendar.MINUTE);
		int segundo = calAtual.get(Calendar.SECOND);
		int milli = calAtual.get(Calendar.MILLISECOND);
		calAtual.set(Calendar.HOUR_OF_DAY, 12);
		calAtual.set(Calendar.MINUTE, 0);
		calAtual.set(Calendar.SECOND, 0);
		calAtual.set(Calendar.MILLISECOND, 0);

		long timeAtual = calAtual.getTime().getTime() / 1000;

		calAtual.set(Calendar.HOUR_OF_DAY, hora);
		calAtual.set(Calendar.MINUTE, minuto);
		calAtual.set(Calendar.SECOND, segundo);
		calAtual.set(Calendar.MILLISECOND, milli);

		return timeAtual;
	}

	/**
	 * Retorna o valor da varivel horaAtual em Calendar
	 * 
	 * @return int
	 */
	public int getHoraatual() {
		return calAtual.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * Retorna o valor da varivel minutoAtual em Calendar
	 * 
	 * @return int
	 */
	public int getMinutoatual() {
		return calAtual.get(Calendar.MINUTE);
	}

	/**
	 * Retorna o valor da varivel diaAtual em Calendar
	 * 
	 * @return int
	 */
	public int getDiaatual() {
		return calAtual.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Retorna o valor da varivel mesAtual em Calendar
	 * 
	 * @return int
	 */
	public int getMesatual() {
		return calAtual.get(Calendar.MONTH);
	}

	/**
	 * Retorna o valor da varivel anoAtual em Calendar
	 * 
	 * @return int
	 */
	public int getAnoatual() {
		return calAtual.get(Calendar.YEAR);
	}

	/**
	 * Retorna o valor da varivel dateAtual em Calendar
	 * 
	 * @return Date
	 */
	public Date getDateatual() {
		return calAtual.getTime();
	}

	/**
	 * Retorna o valor da varivel date em Calendar
	 * 
	 * @return Date
	 */
	public Date getDate() {
		return calendar.getTime();
	}

	/**
	 * Retorna o valor da varivel proximo
	 * 
	 * @return long
	 */
	public long getProximo() {
		long retorno = 0;
		if (navtip.equals("d")) {
			add(Calendar.DAY_OF_MONTH, 1);
			retorno = getTime();
			add(Calendar.DAY_OF_MONTH, -1);
		} else if (navtip.equals("m")) {
			add(Calendar.MONTH, 1);
			retorno = getTime();
			add(Calendar.MONTH, -1);
		} else if (navtip.equals("a")) {
			add(Calendar.YEAR, 1);
			retorno = getTime();
			add(Calendar.YEAR, -1);
		} else if (navtip.equals("s")) {
			add(Calendar.WEEK_OF_MONTH, 1);
			retorno = getTime();
			add(Calendar.WEEK_OF_MONTH, -1);
		}
		return retorno;
	}

	/**
	 * Retorna o valor da varivel anterior
	 * 
	 * @return long
	 */
	public long getAnterior() {
		long retorno = 0;
		if (navtip.equals("d")) {
			add(Calendar.DAY_OF_MONTH, -1);
			retorno = getTime();
			add(Calendar.DAY_OF_MONTH, 1);
		} else if (navtip.equals("m")) {
			add(Calendar.MONTH, -1);
			retorno = getTime();
			add(Calendar.MONTH, 1);
		} else if (navtip.equals("a")) {
			add(Calendar.YEAR, -1);
			retorno = getTime();
			add(Calendar.YEAR, 1);
		} else if (navtip.equals("s")) {
			add(Calendar.WEEK_OF_MONTH, -1);
			retorno = getTime();
			add(Calendar.WEEK_OF_MONTH, 1);
		}
		return retorno;
	}

	/**
	 * Retorna o valor da varivel nextDay
	 * 
	 * @return long
	 */
	public long getProximodia() {
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		long nextDay = calendar.getTime().getTime();
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		return nextDay;
	}

	/**
	 * Retorna o valor da varivel nextMonth
	 * 
	 * @return long
	 */
	public long getProximomes() {
		calendar.add(Calendar.MONTH, 1);
		long nextMonth = calendar.getTime().getTime();
		calendar.add(Calendar.MONTH, -1);
		return nextMonth;
	}

	/**
	 * Retorna o valor da varivel nextYear
	 * 
	 * @return long
	 */
	public long getProximoano() {
		calendar.add(Calendar.YEAR, 1);
		long nextYear = calendar.getTime().getTime();
		calendar.add(Calendar.YEAR, -1);
		return nextYear;
	}

	/**
	 * Retorna o valor da varivel lastDay
	 * 
	 * @return long
	 */
	public long getAnteriordia() {
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		long lastDay = calendar.getTime().getTime();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		return lastDay;
	}

	/**
	 * Retorna o valor da varivel lastMonth
	 * 
	 * @return long
	 */
	public long getAnteriormes() {
		calendar.add(Calendar.MONTH, -1);
		long lastMonth = calendar.getTime().getTime();
		calendar.add(Calendar.MONTH, 1);
		return lastMonth;
	}

	/**
	 * Retorna o valor da varivel lastYear
	 * 
	 * @return long
	 */
	public long getAnteriorano() {
		calendar.add(Calendar.YEAR, -1);
		long lastYear = calendar.getTime().getTime();
		calendar.add(Calendar.YEAR, 1);
		return lastYear;
	}

	/**
	 * Altera o valor da varivel em Calendar
	 * 
	 * @param String
	 *            Valor para a varivel adddia
	 */
	public void setAdddia(String adddia) {
		add(Calendar.DAY_OF_MONTH, adddia);
	}

	/**
	 * Altera o valor da varivel em Calendar
	 * 
	 * @param String
	 *            Valor para a varivel addmes
	 */
	public void setAddmes(String addmes) {
		add(Calendar.MONTH, addmes);
	}

	/**
	 * Altera o valor da varivel em Calendar
	 * 
	 * @param String
	 *            Valor para a varivel addano
	 */
	public void setAddano(String addano) {
		add(Calendar.YEAR, addano);
	}

	public void setAddhora(String addhora) {
		add(Calendar.HOUR_OF_DAY, addhora);
	}

	public void setAddminuto(String addminuto) {
		add(Calendar.MINUTE, addminuto);
	}

	/**
	 * Adiciona o valor da varivel
	 * 
	 * @param int
	 *            Valor para a varivel field
	 * @param String
	 *            Valor para a varivel value
	 */
	private void add(int field, String value) {
		try {
			int valor = Integer.parseInt(value);
			add(field, valor);
		} catch (Exception e) {
		}
	}

	/**
	 * Altera o valor da varivel em Calendar
	 * 
	 * @param int
	 *            Valor para a varivel field
	 * @param String
	 *            Valor para a varivel value
	 */
	private void set(int field, int value) {
		calendar.set(field, value);
	}

	/**
	 * Adiciona o valor da varivel em Calendar
	 * 
	 * @param int
	 *            Valor para a varivel field
	 * @param String
	 *            Valor para a varivel value
	 */
	private void add(int field, int value) {
		calendar.add(field, value);
	}

	public long convertMillistoTime(long time) {

		return time / 1000;
	}

	public void setDate(Date date) {
		calendar.setTime(date);

	}

}