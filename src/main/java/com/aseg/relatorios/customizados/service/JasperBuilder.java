package com.aseg.relatorios.customizados.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.springframework.util.StringUtils;

import com.aseg.config.Constants;
import com.aseg.dao.DAOUtil;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.design.JRJdtCompiler;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class JasperBuilder extends ServiceImpl {

	private Map params = new HashMap();

	Map<String, ServiceImpl> services = new HashMap();
	private Usuario user = null;

	private String chave = "";
	private int x = 0;

	private int xsumheader = 0;

	private int y = 0;

	private String TextFields = "";

	private String subcontexto = null;

	private int old_size = 0;

	private Map tempsum = new HashMap();

	private List campos;

	private String query = "";

	private Map aliases = new HashMap();

	private Map sumitens = new HashMap();

	private List sf = new ArrayList();

	public JasperBuilder(final Map params, final Usuario user) {
		this.params = params;
		this.user = user;

	}

	private String getAlinhamento(final int tipo) {
		return "Left";
	}

	/*
	 * paramentros na tabela hash 1- nome = nome do relatorio
	 */

	private int getAltura(final int tipo) {

		return 13;
	}

	/*
	 * paramentros na tabela hash 1- campos = campos do relatorio 2 - table =
	 * tabela no banco de dados 3- filtro = filtro do relatorio
	 */

	private String getAtributoCampo(final String contexto, final String campo, final String att) {
		LogUtil.debug(campo, user);
		// final String[] var = campo.split("_");

		// LogUtil.debug(Arrays.asList(var));
		Map master = getField(contexto, campo, user);

		if (isTabular() && (att.equals("queryFieldName") || att.equals("name"))
				&& getService(contexto).getDAO().getDefaultDAOFields().contains(campo)) {
			return campo + "_" + contexto;
		}

		if (att.equals("queryFieldName")) {
			if (master.get("service") != null && master.get("labelValue") != null
					&& !"sqlrule".equals(master.get("type"))
					&& query.indexOf(campo + "_" + master.get("labelValue")) > -1) {
				if (!master.get("labelValue").toString().startsWith("ID_"))
					return campo + "_" + master.get("labelValue");
				else {
					final String auxCol = user.getUtil().defineColumn(master, user, true, 0).get(Constants.COLUMN_NAME);
					return auxCol;
				}
			} else {

				if (aliases.get(campo) != null)
					return aliases.get(campo).toString();
				else
					return campo;
			}

		}
		if (att.equals("mask")) {
			if ("money".equals(master.get("type")) || "money".equals(master.get("component")))
				return " #,##0.00";
			else
				return "";
		}
		String r = "";
		try {
			if (att.equalsIgnoreCase("column")) {
				r = user.getUtil().replaceFuncionDialeto((String) master.get(att), user);
			} else {
				r = (String) master.get(att);
			}
			if (r == null)
				return "";
			return r;

		} catch (final Exception e) {
			return "";

		}

	}

	private Map getField(String contexto, String campo, Usuario user2) {
		// TODO Auto-generated method stub
		GenericService s = getService(contexto);
		Map master = null;
		try {
			master = s.getProperty(campo, user);
		} catch (Exception e) {
			// TODO: handle exception
			List sc = s.getChilds(user);
			for (Iterator iterator = sc.iterator(); iterator.hasNext();) {
				GenericService object = (GenericService) iterator.next();
				try {
					master = object.getProperty(campo, user);
					master.put("ctx", object.getServiceId());
					break;
				} catch (Exception ex) {
					continue;
				}
			}
		}
		return master;
	}

	private ServiceImpl getService(String contexto) {
		// TODO Auto-generated method stub

		if (services.get(contexto) == null)
			services.put(contexto, (ServiceImpl) user.getUtil().getServiceById(contexto, user));
		return services.get(contexto);
	}

	private String getAtributoContexto(final String Contexto, final String att) {

		final Map master = (Map) getService(Contexto).getProperties();
		return (String) master.get(att);

	}

	private String getAtributoPagina(final String att) {

		return "";

	}

	public int getAtualPageWidth() {
		return x + 2;

	}

	public String getColumnFooter() {
		return "<columnFooter>" + "<band height=\"30\"  isSplitAllowed=\"true\" >" + "</band>" + "</columnFooter>";

	}

	public String getColumnReader() {

		String content = "	<columnHeader>" + "	<band height=\"" + (getAltura(0) + 1) + "\"  isSplitAllowed=\"true\" >";
		Object[] campos = null;
		if (isTabular())
			campos = getContextCampos();
		else
			campos = getCampos(getSubcontexto());
		for (int i = 0; i < campos.length; i++) {
			//
			// if (getSubcontexto() != null
			// && !getAtributoContexto(getSubcontexto(), "table")
			// .equalsIgnoreCase("*VIRTUAL*")) {
			// if (!campos[i].toString().startsWith(getSubcontexto())
			// && !isTabular()) {
			// continue;
			// }
			// }
			if (isTabular()) {
				Map p = (Map) campos[i];
				getTextField(p.get("CTX").toString(), p.get("id").toString());
				content += getStaticText(p.get("CTX").toString(), p.get("id").toString());

			} else {
				getTextField(getSubcontexto(), campos[i].toString());
				content += getStaticText(getSubcontexto(), campos[i].toString());
			}

		}
		content += "			</band>" + "</columnHeader>";
		return content;

	}

	private Object[] getCampos(String ctx) {

		Object[] s = null;
		if (this.campos == null || isTabular()) {
			this.campos = new ArrayList();
			// TODO Auto-generated method stub

			s = (Object[]) this.params.get("CMP_" + ctx).toString().split("\\$");

			for (int i = 0; i < s.length; i++) {
				Map m = user.getUtil().JSONtoMap(s[i].toString());
				if ((Boolean) m.get("checked"))
					this.campos.add(m);
			}
		}

		return user.getUtil().extractArrayFromList(this.campos, "id", true);

	}

	private Object[] getContextCampos() {

		Object[] s = null;
		ArrayList lis = new ArrayList();

		for (Object t : getContextos()) {

			String object = "CMP_" + t;
			// if (object.startsWith("CMP_")) {
			s = (Object[]) this.params.get(object).toString().split("\\$");
			String ctx = object.split("_")[1];
			for (int ix = 0; ix < s.length; ix++) {
				Map p = user.getUtil().JSONtoMap(s[ix].toString());
				p.put("CTX", ctx);
				if ((Boolean) p.get("checked"))
					lis.add(p);
			}
		}

		// TODO Auto-generated method stub

		return lis.toArray();

	}

	private String getContexto() {
		// TODO Auto-generated method stub
		return this.params.get("CONTEXTO").toString();
	}

	private String getSubcontexto() {
		// TODO Auto-generated method stub
		if (isSubreport())
			return subcontexto;
		else
			return getContexto();
	}

	private String getCor(String local, String campo) {
		try {
			return (String) ((Map) user.getUtil().match(Arrays.asList(getContextCampos()), "id", campo).get(0))
					.get(local).toString();

		} catch (Exception e) {
			if (local.toLowerCase().indexOf("fundo") > -1)
				return "#FFFFFF";
			else
				return "#000000";
		}

	}

	private boolean isSubreport() {
		// TODO Auto-generated method stub
		return subcontexto != null && subcontexto.trim().length() > 0;
	}

	public String getDetail() {
		final Object[] campos = getCampos(getSubcontexto());
		final String temp = getTextFields();
		final Object[] subcontextos = (Object[]) this.params.get("SUBCONTEXTO");
		String content = "<detail>" + "	<band height=\"";
		// if (containsSubReport)
		// content += "" + (xh + ) + "";
		// else
		content += "" + getAltura(0) + "";
		content += "\"  isSplitAllowed=\"true\" >";

		content += temp;

		if (false) // linha divisoria, desabilitado por enquanto
		{

			content += "<line direction=\"TopDown\">" + "	<reportElement" + "		mode=\"Opaque\"" + "		x=\"5\""
					+ "		y=\"" + (18) + "\"" + "		width=\"" + String.valueOf(x - 42) + "\""
					+ "		height=\"0\"" + "		forecolor=\"" + getCor("corDaFonte", "") + "\""
					+ "		backcolor=\"" + getCor("corDeFundo", "") + "\"" + "		key=\"line-1\""
					+ "		stretchType=\"NoStretch\"" + "		positionType=\"FixRelativeToTop\""
					+ "		isPrintRepeatedValues=\"true\"" + "		isRemoveLineWhenBlank=\"false\""
					+ "		isPrintInFirstWholeBand=\"false\"" + "		isPrintWhenDetailOverflows=\"false\"/>"
					+ "	<graphicElement stretchType=\"NoStretch\" pen=\"Thin\" fill=\"Solid\" />" + "</line>";

		}

		content += "</band>" + "</detail>";

		return content;

	}

	private String getFieldType(final String ctx, final String campo) {

		final Map master = getField(ctx, campo, user);

		if (master.get("service") != null && master.get("labelValue") != null)
			return "java.lang.String";
		String code = (String) master.get("type");

		if (code == null) {
			return "java.lang.String";
		} else if (code.equalsIgnoreCase("num") || code.equalsIgnoreCase("money")) {
			return "java.math.BigDecimal";
		} else if (code.equalsIgnoreCase("date")) {
			return "java.sql.Timestamp";
		} else {
			return "java.lang.String";
		}

	}

	private String getFonte(final String campo, final String att) {
		try {
			return (String) ((Map) user.getUtil().match(campos, "id", campo).get(0)).get(att).toString();
		} catch (Exception e) {
			// TODO: handle exception
			if (att.toLowerCase().indexOf("tamanho") > -1)
				return "10";

			if (att.toLowerCase().indexOf("fonte") > -1)
				return user.getDefaultSystemFont();

			return "false";
		}
	}

	public String getHeader() {
		final double pix = 0;

		// final int borders = Integer.valueOf(
		// user.getUtil().cm2px(
		// getMargem("X").toString() + getMargem("Y").toString()
		// + 2)).intValue();
		int aux = x + 55;
		String result_header = "";
		// if (true || isExcel()) {
		// aux = String.valueOf(x);
		// } else {
		// aux = user.getUtil().cm2px(getMargem("W").toString());
		// }

		String a = "0";
		String b = "0";
		String c = "0";
		String d = "0";
		if (!isSubreport()) {
			a = user.getUtil().cm2px(getMargem("H").toString());
			b = user.getUtil().cm2px(getMargem("W").toString());
			c = user.getUtil().cm2px(getMargem("X").toString());
			d = user.getUtil().cm2px(getMargem("Y").toString());

		}

		result_header = "<?xml version=\"1.0\" encoding=\"" + user.getEncode() + "\" ?>"
				+ "<jasperReport xmlns=\"http://jasperreports.sourceforge.net/jasperreports\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd\"  "
				+ "	 name=\"essenreport\" \n" + "		 columnCount=\"" + 1 + "\""
				+ "\n		 printOrder=\"Vertical\"\n" + "		 orientation=\"";
		if (getOrientacao().equals("R")) {
			result_header += "Portrait";
			result_header += "\"";
			result_header += "		 pageWidth=\"" + min(aux, 842) + "\"";
			result_header += "		 pageHeight=\"" + 595 // user.getUtil().cm2px(getTamanhoFolha("H").toString())
					+ "\"";
		} else {
			result_header += "Landscape";
			result_header += "\"";
			result_header += "		 pageWidth=\"" + 595 // user.getUtil().cm2px(getTamanhoFolha("H").toString())
					+ "\"";
			result_header += "		 pageHeight=\"" + min(aux, 842) + "\"";
		}
		result_header += "		 columnWidth=\"" + 0 + "\"" + "		 columnSpacing=\"0\"" + "		 leftMargin=\"" + a
				+ "\"" + "		 rightMargin=\"" + b + "\"" + "		 topMargin=\"" + c + "\""
				+ "		 bottomMargin=\"" + d + "\"" + "		 whenNoDataType=\"NoPages\""
				+ "		 isTitleNewPage=\"false\"";
		;
		if (!isSubreport()) {
			result_header += "  scriptletClass=\"DefaultScriptlet\"  ";
		}
		result_header += "		 isSummaryNewPage=\"false\">"
				+ "	<property name=\"ireport.scriptlethandling\" value=\"";
		if (!isSubreport()) {
			result_header += "2";
		} else {
			result_header += "2";
		}

		result_header += "\" />\n" + "  " + "<property name=\"ireport.encoding\" value=\"" + user.getEncode() + "\" />"
				+ "<import value=\"java.util.*\" />\n" + "<import value=\"net.sf.jasperreports.engine.*\" />\n"
				+ "<import value=\"net.sf.jasperreports.engine.data.*\" />\n";

		;
		// +user.getUtil().cm2px((int)pg.getTamanhoFolha().width)+
		return result_header;

	}

	private int min(int aux, int i) {
		// TODO Auto-generated method stub
		return aux < i ? i : aux;
	}

	private String getOrientacao() {
		return "R";
	}

	private Object getTamanhoFolha(String string) {
		// TODO Auto-generated method stub
		return y;
	}

	private Object getMargem(String string) {
		// TODO Auto-generated method stub
		return "1";
	}

	private boolean isExcel() {
		// TODO Auto-generated method stub
		return params.get("TIPO").toString().equals("CSV") || params.get("TIPO").toString().equals("XLS");
	}

	public File getJasperContent(final boolean sub, final String ctx) {

		File tmpFile = null;
		try {
			String userHome = System.getProperty("user.home");

			tmpFile = File.createTempFile("pjwreport", ".jrxml");
			tmpFile.deleteOnExit();
			final OutputStream outStream = new FileOutputStream(tmpFile);
			final OutputStreamWriter out = new OutputStreamWriter(outStream, "ISO-8859-1");
			final String result = getJasperContentFile();
			// LogUtil.debug(result);
			final File directory = new File(userHome + "/reports");
			if (directory.exists() && directory.canWrite()) {
				OutputStream bos;
				// LogUtil.debug(isSubreport());
				if (sub) {
					bos = new FileOutputStream(userHome + "/reports/sub_teste_" + ctx + ".jrxml");
				} else {
					bos = new FileOutputStream(userHome + "/reports/teste_" + ctx + ".jrxml");
				}

				final OutputStreamWriter out1 = new OutputStreamWriter(bos, "ISO-8859-1");
				out1.write(result);
				out1.close();
				bos.close();
				bos = null;

			}

			out.write(result);

			out.close();

		} catch (final Exception e) {
			LogUtil.exception(e, user);
		}
		// tmpFile.delete();
		return tmpFile;

	}

	public String getJasperContentFile() {
		String result = "";
		String columnreader = getColumnReader();
		if (isSubreport()) {
			result = getHeader();
			result += getReportQuery();
			result += columnreader;
			result += getDetail();
			result += getLastPageFooter();
			result += getSummary();
		} else {
			result = getHeader() + getReportQuery();
			if (!isExcel()) {
				result += getTitleAndBG();
			}
			result += columnreader;
			result += getDetail();
			result += getColumnFooter();
			result += getPageFooter();
			result += getLastPageFooter();
			result += getSummary();

		}

		return result;

	}

	private String getLargurapx(final String contexto, final String la) {
		try {
			return user.getUtil().cm2px(
					(String) ((Map) user.getUtil().match(campos, "id", la).get(0)).get("larguraColuna").toString());
		} catch (Exception e) {
			return "250";
		}
	}

	public String getLastPageFooter() {
		String conteudo = "";

		final Object[] campos = getCampos(getSubcontexto());

		final int inc = getAltura(0);
		int altura = inc + 19;
		int alturasum = 0;
		int alturacont = 0;
		int alturamed = 0;
		int alturamin = 0;
		int alturamax = 0;
		int alturacontd = 0;
		int alturavar = 0;
		int alturapd = 0;
		final int sumx = 0;

		String aux = "";

		if (containsSum()) { // containsSum) {
			altura += inc;
			alturasum = altura - 29;
			aux += "<staticText>" + "		<reportElement" + "			mode=\"Opaque\"" + "			x=\"" + 0 + "\""
					+ "			y=\"" + alturasum + "\"" + "			width=\""
					+ getLargurapx(getSubcontexto(), campos[0].toString())

					+ "\"" + "			height=\"" + inc + "\"" + "			forecolor=\""
					+ getCor("cCorDaFonte", campos[0].toString()) + "\"" + "			backcolor=\""
					+ getCor("cCorDeFundo", campos[0].toString()) + "\"" + "			key=\"staticTextPRAE-4\""
					+ "			stretchType=\"RelativeToBandHeight\"" + "			positionType=\"FixRelativeToTop\""
					+ "			isPrintRepeatedValues=\"true\"" + "			isRemoveLineWhenBlank=\"false\""
					+ "			isPrintInFirstWholeBand=\"false\"" + "			isPrintWhenDetailOverflows=\"false\"/>"
					// +
					// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
					// +
					// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#FFFFFF\"/> </box> "
					+ " <textElement textAlignment=\"Right\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
					+ "			<font fontName=\"" + getFonte("CABECALHO", "fonte")
					+ "\" pdfFontName=\"Helvetica\" size=\"" + getFonte("CABECALHO", "tamanhoDaFonte")

					+ "\" isBold=\"" + getFonte("CABECALHO", "negrito") + "\" isItalic=\""
					+ getFonte("CABECALHO", "italico") + "\" isUnderline=\"" + getFonte("CABECALHO", "sublinhado")
					+ "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode()
					+ "\" isStrikeThrough=\"false\" />" + "		</textElement>" + "	<text><![CDATA[Soma]]></text>"
					+ "	</staticText>";
		}
		if (containsCont()) {// containsCont) {
			altura += inc;
			alturacont = altura - 29;
			aux += "<staticText>" + "		<reportElement" + "			mode=\"Opaque\"" + "			x=\"" + 0 + "\""
					+ "			y=\"" + alturacont + "\"" + "			width=\""
					+ getLargurapx(getSubcontexto(), campos[0].toString()) + "\"" + "			height=\"" + inc + "\""
					+ "			forecolor=\""
					// + getCor(CORTEXTO, CABECALHO)
					+ "\"" + "			backcolor=\""
					// + getCor(CORFUNDO, CABECALHO)
					+ "\"" + "			key=\"staticTextPRAE-5\"" + "			stretchType=\"RelativeToBandHeight\""
					+ "			positionType=\"FixRelativeToTop\"" + "			isPrintRepeatedValues=\"true\""
					+ "			isRemoveLineWhenBlank=\"false\"" + "			isPrintInFirstWholeBand=\"false\""
					+ "			isPrintWhenDetailOverflows=\"false\"/>"
					// +
					// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
					// +
					// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#FFFFFF\"/> </box> "
					+ " <textElement textAlignment=\"Right\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
					+ "			<font fontName=\"" + getFonte("CABECALHO", "fonte")
					+ "\" pdfFontName=\"Helvetica\" size=\"" + getFonte("CABECALHO", "tamanhoDaFonte")

					+ "\" isBold=\"" + getFonte("CABECALHO", "negrito") + "\" isItalic=\""
					+ getFonte("CABECALHO", "italico") + "\" isUnderline=\"" + getFonte("CABECALHO", "sublinhado")
					+ "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode()
					+ "\" isStrikeThrough=\"false\" />" + "		</textElement>" + "	<text><![CDATA[Contagem]]></text>"
					+ "	</staticText>";
		}

		if (containsMed()) {// containsMed) {
			altura += inc;
			alturamed = altura - 29;
			aux += "<staticText>" + "		<reportElement" + "			mode=\"Opaque\"" + "			x=\"" + 0 + "\""
					+ "			y=\"" + alturamed + "\"" + "			width=\""
					+ getLargurapx(getSubcontexto(), campos[0].toString()) + "\"" + "			height=\"" + inc + "\""
					+ "			forecolor=\""
					// + getCor(CORTEXTO, CABECALHO)
					+ "\"" + "			backcolor=\""
					// + getCor(CORFUNDO, CABECALHO)
					+ "\"" + "			key=\"staticTextPRAE-6\"" + "			stretchType=\"RelativeToBandHeight\""
					+ "			positionType=\"FixRelativeToTop\"" + "			isPrintRepeatedValues=\"true\""
					+ "			isRemoveLineWhenBlank=\"false\"" + "			isPrintInFirstWholeBand=\"false\""
					+ "			isPrintWhenDetailOverflows=\"false\"/>"
					// +
					// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
					// +
					// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#FFFFFF\"/> </box> "
					+ " <textElement textAlignment=\"Right\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
					+ "			<font fontName=\"" + getFonte("CABECALHO", "fonte")
					+ "\" pdfFontName=\"Helvetica\" size=\"" + getFonte("CABECALHO", "tamanhoDaFonte")

					+ "\" isBold=\"" + getFonte("CABECALHO", "negrito") + "\" isItalic=\""
					+ getFonte("CABECALHO", "italico") + "\" isUnderline=\"" + getFonte("CABECALHO", "sublinhado")
					+ "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode()
					+ "\" isStrikeThrough=\"false\" />" + "		</textElement>" + "	<text><![CDATA[Mdia]]></text>"
					+ "	</staticText>";
		}
		if (containsMax()) {// containsMax) {
			altura += inc;
			alturamax = altura - 29;
			aux += "<staticText>" + "		<reportElement" + "			mode=\"Opaque\"" + "			x=\"" + 0 + "\""
					+ "			y=\"" + alturamax + "\"" + "			width=\""
					+ getLargurapx(getSubcontexto(), campos[0].toString()) + "\"" + "			height=\"" + inc + "\""
					+ "			forecolor=\""
					// + getCor(CORTEXTO, CABECALHO)
					+ "\"" + "			backcolor=\""
					// + getCor(CORFUNDO, CABECALHO)
					+ "\"" + "			key=\"staticTextPRAE-7\"" + "			stretchType=\"RelativeToBandHeight\""
					+ "			positionType=\"FixRelativeToTop\"" + "			isPrintRepeatedValues=\"true\""
					+ "			isRemoveLineWhenBlank=\"false\"" + "			isPrintInFirstWholeBand=\"false\""
					+ "			isPrintWhenDetailOverflows=\"false\"/>"
					// +
					// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
					// +
					// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#FFFFFF\"/> </box>"
					+ " <textElement textAlignment=\"Right\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
					+ "			<font fontName=\"" + getFonte("CABECALHO", "fonte")
					+ "\" pdfFontName=\"Helvetica\" size=\"" + getFonte("CABECALHO", "tamanhoDaFonte")

					+ "\" isBold=\"" + getFonte("CABECALHO", "negrito") + "\" isItalic=\""
					+ getFonte("CABECALHO", "italico") + "\" isUnderline=\"" + getFonte("CABECALHO", "sublinhado")
					+ "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode()
					+ "\" isStrikeThrough=\"false\" />" + "		</textElement>" + "	<text><![CDATA[Mximo]]></text>"
					+ "	</staticText>";
		}

		if (containsMin()) {// containsMin) {
			altura += inc;
			alturamin = altura - 29;
			aux += "<staticText>" + "		<reportElement" + "			mode=\"Opaque\"" + "			x=\"" + 0 + "\""
					+ "			y=\"" + alturamin + "\"" + "			width=\""
					+ getLargurapx(getSubcontexto(), campos[0].toString()) + "\"" + "			height=\"" + inc + "\""
					+ "			forecolor=\""
					// + getCor(CORTEXTO, CABECALHO)
					+ "\"" + "			backcolor=\""
					// + getCor(CORFUNDO, CABECALHO)
					+ "\"" + "			key=\"staticTextPRAE-8\"" + "			stretchType=\"RelativeToBandHeight\""
					+ "			positionType=\"FixRelativeToTop\"" + "			isPrintRepeatedValues=\"true\""
					+ "			isRemoveLineWhenBlank=\"false\"" + "			isPrintInFirstWholeBand=\"false\""
					+ "			isPrintWhenDetailOverflows=\"false\"/>"
					// +
					// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
					// +
					// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#FFFFFF\"/> </box>"
					+ " <textElement textAlignment=\"Right\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
					+ "			<font fontName=\"" + getFonte("CABECALHO", "fonte")
					+ "\" pdfFontName=\"Helvetica\" size=\"" + getFonte("CABECALHO", "tamanhoDaFonte")

					+ "\" isBold=\"" + getFonte("CABECALHO", "negrito") + "\" isItalic=\""
					+ getFonte("CABECALHO", "italico") + "\" isUnderline=\"" + getFonte("CABECALHO", "sublinhado")
					+ "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode()
					+ "\" isStrikeThrough=\"false\" />" + "		</textElement>" + "	<text><![CDATA[Mnimo]]></text>"
					+ "	</staticText>";
		}

		if (containsCont()) {// containsContd) {
			altura += inc;
			alturacontd = altura - 29;
			aux += "<staticText>" + "		<reportElement" + "			mode=\"Opaque\"" + "			x=\"" + 0 + "\""
					+ "			y=\"" + alturacontd + "\"" + "			width=\""
					+ getLargurapx(getSubcontexto(), campos[0].toString()) + "\"" + "			height=\"" + inc + "\""
					+ "			forecolor=\""
					// + getCor(CORTEXTO, CABECALHO)
					+ "\"" + "			backcolor=\""
					// + getCor(CORFUNDO, CABECALHO)
					+ "\"" + "			key=\"staticTextPRAE-9\"" + "			stretchType=\"RelativeToBandHeight\""
					+ "			positionType=\"FixRelativeToTop\"" + "			isPrintRepeatedValues=\"true\""
					+ "			isRemoveLineWhenBlank=\"false\"" + "			isPrintInFirstWholeBand=\"false\""
					+ "			isPrintWhenDetailOverflows=\"false\"/>"
					// +
					// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
					// +
					// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#FFFFFF\"/> </box>"
					+ ""
					+ " <textElement textAlignment=\"Right\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
					+ "			<font fontName=\"" + getFonte("CABECALHO", "fonte")
					+ "\" pdfFontName=\"Helvetica\" size=\"" + getFonte("CABECALHO", "tamanhoDaFonte")

					+ "\" isBold=\"" + getFonte("CABECALHO", "negrito") + "\" isItalic=\""
					+ getFonte("CABECALHO", "italico") + "\" isUnderline=\"" + getFonte("CABECALHO", "sublinhado")
					+ "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode()
					+ "\" isStrikeThrough=\"false\" />" + "		</textElement>"
					+ "	<text><![CDATA[Contagem Distinta]]></text>" + "	</staticText>";
		}

		if (containsVar()) {// containsVar) {
			altura += inc;
			alturavar = altura - 29;
			aux += "<staticText>" + "		<reportElement" + "			mode=\"Opaque\"" + "			x=\"" + 0 + "\""
					+ "			y=\"" + alturavar + "\"" + "			width=\""
					+ getLargurapx(getSubcontexto(), campos[0].toString()) + "\"" + "			height=\"" + inc + "\""
					+ "			forecolor=\""
					// + getCor(CORTEXTO, CABECALHO)
					+ "\"" + "			backcolor=\""
					// + getCor(CORFUNDO, CABECALHO)
					+ "\"" + "			key=\"staticTextPRAE-9\"" + "			stretchType=\"RelativeToBandHeight\""
					+ "			positionType=\"FixRelativeToTop\"" + "			isPrintRepeatedValues=\"true\""
					+ "			isRemoveLineWhenBlank=\"false\"" + "			isPrintInFirstWholeBand=\"false\""
					+ "			isPrintWhenDetailOverflows=\"false\"/>"
					// +
					// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
					// +
					// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#FFFFFF\"/> </box>"
					+ " <textElement textAlignment=\"Right\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
					+ "			<font fontName=\"" + getFonte("CABECALHO", "fonte")
					+ "\" pdfFontName=\"Helvetica\" size=\"" + getFonte("CABECALHO", "tamanhoDaFonte")

					+ "\" isBold=\"" + getFonte("CABECALHO", "negrito") + "\" isItalic=\""
					+ getFonte("CABECALHO", "italico") + "\" isUnderline=\"" + getFonte("CABECALHO", "sublinhado")
					+ "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode()
					+ "\" isStrikeThrough=\"false\" />" + "		</textElement>" + "	<text><![CDATA[Variao]]></text>"
					+ "	</staticText>";
		}

		if (containsDP()) {// containsDP) {
			altura += inc;
			alturapd = altura - 29;
			aux += "<staticText>" + "		<reportElement" + "			mode=\"Opaque\"" + "			x=\"" + 0 + "\""
					+ "			y=\"" + alturapd + "\"" + "			width=\""
					+ getLargurapx(getSubcontexto(), campos[0].toString()) + "\"" + "			height=\"" + inc + "\""
					+ "			forecolor=\""
					// + getCor("CORTEXTO", "CABECALHO")
					+ "\"" + "			backcolor=\""
					// + getCor(CORFUNDO, CABECALHO)
					+ "\"" + "			key=\"staticTextPRAE-9\"" + "			stretchType=\"RelativeToBandHeight\""
					+ "			positionType=\"FixRelativeToTop\"" + "			isPrintRepeatedValues=\"true\""
					+ "			isRemoveLineWhenBlank=\"false\"" + "			isPrintInFirstWholeBand=\"false\""
					+ "			isPrintWhenDetailOverflows=\"false\"/>"
					// +
					// " <box><topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
					// +
					// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#FFFFFF\"/> </box>"
					+ " <textElement textAlignment=\"Right\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
					+ "			<font fontName=\"" + getFonte("CABECALHO", "fonte")
					+ "\" pdfFontName=\"Helvetica\" size=\"" + getFonte("CABECALHO", "tamanhoDaFonte")

					+ "\" isBold=\"" + getFonte("CABECALHO", "negrito") + "\" isItalic=\""
					+ getFonte("CABECALHO", "italico") + "\" isUnderline=\"" + getFonte("CABECALHO", "sublinhado")
					+ "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode()
					+ "\" isStrikeThrough=\"false\" />" + "		</textElement>"
					+ "	<text><![CDATA[Desvio Padro]]></text>" + "	</staticText>";
		}
		int z = 1;
		String veri = "";

		/*
		 * Enumeration sumkeys = sumItens.keys(); while
		 * (sumkeys.hasMoreElements()) { String tmp = (String)
		 * sumkeys.nextElement(); String id =
		 * getAtributoCampo(campos[i].toString(), "id"); if (tmp
		 * .startsWith(id)) { HashMap h = new HashMap(); h.put("1",
		 * getAtributoCampo(campos[i].toString(), "label")); h.put("2",id);
		 * sumItens.put(tmp, h); } }
		 */

		// Enumeration keys = sumItens.keys();
		final Object[] lala = getSumItems().keySet().toArray();
		Arrays.sort(lala);

		for (final Object element : lala) {
			final String key = (String) element;
			if (!key.startsWith(veri) && campos.length - 1 > z) {
				z++;
			}
			veri = key.substring(0, key.length() - 2);
			String[] cmp = key.split("\\|");
			try {
				aux += getSumHeader(getAtributoCampo(cmp[0], cmp[1], "label"), this.params,
						Integer.valueOf(getLargurapx(cmp[0], cmp[1])).intValue(),
						Integer.valueOf(getFonte("", "tamanhoDaFonte")).intValue() + 3);

			} catch (final Exception ex) {
				LogUtil.exception(ex, user);
				aux += "";
			}

			final String tipo = cmp[2];
			int a = 0;
			if (tipo.equals("CO")) {
				a = alturacont;
			}
			if (tipo.equals("SO")) {
				a = alturasum;
			}
			if (tipo.equals("ME")) {
				a = alturamed;
			}
			if (tipo.equals("MA")) {
				a = alturamax;
			}
			if (tipo.equals("MI")) {
				a = alturamin;
			}
			if (tipo.equals("CU")) {
				a = alturacontd;
			}
			if (tipo.equals("VA")) {
				a = alturavar;
			}
			if (tipo.equals("DP")) {
				a = alturapd;
			}

			aux += getSumContent(cmp[0], cmp[1], tipo, a, this.params,
					Integer.valueOf(getLargurapx(cmp[0], cmp[1])).intValue(),
					Integer.valueOf(getFonte("", "tamanhoDaFonte")).intValue());

		}
		conteudo = "		<lastPageFooter>";
		if (aux.trim().length() == 0 && isSubreport()) {
			conteudo += "	<band height=\"0\"  isSplitAllowed=\"true\" >";

		} else {
			conteudo += "	<band height=\"" + (altura + 19) + "\"  isSplitAllowed=\"true\" >";
		}

		if (!isSubreport() && !isExcel()) {
			String t = user.getSystemProperty("TITLE");
			conteudo += "<line>		"
					+ "	<reportElement key=\"line-2\" x=\"0\" y=\"4\" width=\"802\" height=\"1\" forecolor=\"#000000\"/>	"
					+ "</line>		" + "<textField isBlankWhenNull=\"true\">		"
					+ "	<reportElement  key=\"textField-20\" x=\"0\" y=\"9\" width=\"165\" height=\"12\" forecolor=\"#000000\"/>	"

					+ "	<textFieldExpression><![CDATA[$P{user}.getLabel(\"M_RELATORIO.versao\")]]></textFieldExpression>	"
					+ "	</textField>		<textField isBlankWhenNull=\"true\">		"
					+ "	<reportElement key=\"textField-21\" x=\"0\" y=\"21\" width=\"165\" height=\"12\" forecolor=\"#000000\"/>	"

					+ "	<textFieldExpression><![CDATA[$P{user}.getLabel(\"M_RELATORIO.copyright\")]]></textFieldExpression>		</textField>	"
					+ "	<textField pattern=\"dd/MM/yyyy\" isBlankWhenNull=\"false\">		"
					+ "	<reportElement  key=\"textField-29\" x=\"624\" y=\"21\" width=\"178\" height=\"12\" forecolor=\"#000000\"/>	"
					+ "		<textElement textAlignment=\"Right\" verticalAlignment=\"Middle\">		"
					+ "		<font size=\"8\"/>			</textElement>		"
					+ "	<textFieldExpression  class =\"java.util.Date\" ><![CDATA[new Date()]]></textFieldExpression>		</textField>	"
					+ "	<textField isBlankWhenNull=\"false\">"
					+ "			<reportElement  key=\"textField-30\" x=\"624\" y=\"6\" width=\"178\" height=\"15\" forecolor=\"#000000\"/>	"
					+ "	<textElement textAlignment=\"Right\" verticalAlignment=\"Middle\">	"
					+ "			<font size=\"7\"/>			</textElement>	"
					+ "<textFieldExpression><![CDATA[$P{user}.getLabel(\"M_RELATORIO.PAGINA\")+\"   \"+ $V{PAGE_NUMBER}]]> "
					+ "   </textFieldExpression>	" + "	</textField>" + "	  ";
		}

		conteudo += aux;
		conteudo += "		</band>" + "	</lastPageFooter>";

		return conteudo;

	}

	private boolean containsMax() {
		// TODO Auto-generated method stub
		String[] a = user.getUtil().extractArrayFromList(user.getUtil().match(campos, "maximo", true), "id", true);
		addSumFields(a, "MA", getSubcontexto());
		return a.length > 0;

	}

	private void addSumFields(String[] a, String tipo, String contexto) {
		// TODO Auto-generated method stub
		for (int i = 0; i < a.length; i++) {
			if (sf.contains(a[i]))
				continue;
			else
				sf.add(contexto + "|" + a[i] + "|" + tipo);
		}
	}

	private boolean containsDP() {
		// TODO Auto-generated method stub
		String[] a = user.getUtil().extractArrayFromList(user.getUtil().match(campos, "desvio_padrao", true), "id",
				true);
		addSumFields(a, "DP", getSubcontexto());
		return a.length > 0;
	}

	private boolean containsVar() {
		// TODO Auto-generated method stub
		String[] a = user.getUtil().extractArrayFromList(user.getUtil().match(campos, "variacao", true), "id", true);
		addSumFields(a, "VA", getSubcontexto());
		return a.length > 0;
	}

	private boolean containsMin() {
		// TODO Auto-generated method stub
		String[] a = user.getUtil().extractArrayFromList(user.getUtil().match(campos, "minimo", true), "id", true);
		addSumFields(a, "MI", getSubcontexto());
		return a.length > 0;
	}

	private boolean containsMed() {
		// TODO Auto-generated method stub
		String[] a = user.getUtil().extractArrayFromList(user.getUtil().match(campos, "media", true), "id", true);
		addSumFields(a, "ME", getSubcontexto());
		return a.length > 0;
	}

	private boolean containsCont() {
		// TODO Auto-generated method stub
		String[] a = user.getUtil().extractArrayFromList(user.getUtil().match(campos, "contagem_distinta", true), "id",
				true);
		addSumFields(a, "CO", getSubcontexto());
		return a.length > 0;
	}

	private boolean containsSum() {
		// TODO Auto-generated method stub
		String[] a = user.getUtil().extractArrayFromList(user.getUtil().match(campos, "somatorio", true), "id", true);
		addSumFields(a, "SO", getSubcontexto());
		return a.length > 0;
	}

	private Map getSumItems() {
		// TODO Auto-generated method stub
		return sumitens;
	}

	public String getPageFooter() {
		String conteudo = "";
		conteudo = "	<pageFooter>" + "	<band height=\"34\" splitType=\"Stretch\"> ";
		if (!isSubreport() && isPdf() && !isExcel()) {
			conteudo += "<line>		"
					+ "	<reportElement  key=\"line-2\" x=\"0\" y=\"4\" width=\"802\" height=\"1\" forecolor=\"#000000\"/>	"
					+ "	</line>		" + "<textField isBlankWhenNull=\"true\">	"
					+ "		<reportElement  key=\"textField-20\" x=\"0\" y=\"9\" width=\"165\" height=\"12\" forecolor=\"#000000\"/>"
					+ "	<textFieldExpression>"
					+ "	<![CDATA[$P{user}.getLabel(\"M_RELATORIO.versao\")]]></textFieldExpression>"
					+ "		</textField>	" + "	<textField isBlankWhenNull=\"true\">	"
					+ "		<reportElement  key=\"textField-21\" x=\"0\" y=\"21\" width=\"165\" height=\"12\" forecolor=\"#000000\"/>	"
					+ "		<textElement textAlignment=\"Left\" verticalAlignment=\"Middle\">"
					+ "				<font size=\"8\"/>		" + "	</textElement>		" + "	<textFieldExpression>"
					+ "	<![CDATA[$P{user}.getLabel(\"M_RELATORIO.copyright\")]]></textFieldExpression>	"
					+ "	</textField>	" + "	<textField pattern=\"dd/MM/yyyy\" isBlankWhenNull=\"false\">		"
					+ "	<reportElement  key=\"textField-29\" x=\"624\" y=\"21\" width=\"178\" height=\"12\" forecolor=\"#000000\"/>	"

					+ "<textFieldExpression class =\"java.util.Date\"><![CDATA[new Date()]]> "
					+ "</textFieldExpression>	" + "	</textField>	"
					+ "	<textField isBlankWhenNull=\"false\">	"
					+ "		<reportElement  key=\"textField-30\" x=\"624\" y=\"6\" width=\"178\" height=\"15\" forecolor=\"#000000\"/>"

					+ "	<textFieldExpression><![CDATA[$P{user}.getLabel(\"M_RELATORIO.PAGINA\")+\"   \"+ $V{PAGE_NUMBER}]]>   "
					+ "</textFieldExpression>		</textField>	";
		}

		conteudo += "	</band>" + "</pageFooter>";

		return conteudo;
	}

	private boolean isPdf() {
		// TODO Auto-generated method stub
		return params.get("TIPO").toString().equals("PDF");
		// || params.get("TIPO").toString().equals("XLS");
	}

	public String getReportQuery() {

		String result = "";

		Map join = new HashMap();

		if (isSubreport()) {
			result += "<parameter name=\"" + chave + "\" isForPrompting=\"true\" class=\"java.math.BigDecimal\"/>\n";
		} else {
			result += " <parameter name=\"user\" isForPrompting=\"false\" class=\"Usuario\" /> \n "
					+ " <parameter name=\"image_check\" isForPrompting=\"false\" class=\"java.io.File\" />  ";
		}

		if (containsSubReport()) {
			final Object[] subcontextos = (Object[]) getSubcontextos();
			for (final Object subcontexto2 : subcontextos) {

				result += "<parameter name=\"SUB_" + subcontexto2
						+ "\" isForPrompting=\"false\" class=\"net.sf.jasperreports.engine.JasperReport\"/>\n";
			}
		}

		result += "<parameter name=\"sessionID\" isForPrompting=\"false\" class=\"java.lang.String\"/>" + "\n";

		result += "<queryString><![CDATA[   ";

		String jasperFields = "";

		Object[] campos = null;
		if (isTabular())
			campos = getContextCampos();
		else
			campos = getCampos(getSubcontexto());
		String Sumario = "";
		final String[] Sum = getSumFields();
		if (Sumario == null) {
			Sumario = "";
		}

		/*
		 * Object[] soma = (Object[]) this.params.get("campos_soma"); for (int i
		 * = 0; i < soma.length; i++) { Sumario += soma[i] + "_1"; } soma =
		 * (Object[]) this.params.get("campos_media"); for (int i = 0; i <
		 * soma.length; i++) { Sumario += soma[i] + "_2"; } soma = (Object[])
		 * this.params.get("campos_maximo"); for (int i = 0; i < soma.length;
		 * i++) { Sumario += soma[i] + "_3"; } soma = (Object[])
		 * this.params.get("campos_minimo"); for (int i = 0; i < soma.length;
		 * i++) { Sumario += soma[i] + "_4"; }
		 */
		String jaspervars = "";

		for (final String element : Sum) {
			final StringTokenizer st1 = new StringTokenizer(element, "|");
			final String contexto = st1.nextToken();
			// if (!isTabular()) {
			// if (!contexto.equalsIgnoreCase(subcontexto)) {
			// continue;
			// }
			// }

			final String campo = st1.nextToken();
			final String tipo = st1.nextToken();

			getSumItems().put(contexto + "|" + campo + "|" + tipo, new HashMap());
			jaspervars += "<variable name=\"" + contexto + "_" + campo + "_" + tipo
					+ "\" class=\"java.math.BigDecimal\" resetType=\"Report\" calculation=\"" + getTipoSumario(tipo)
					+ "\">" + "<variableExpression><![CDATA[$F{" + campo + "}]]></variableExpression>"
					+ "<initialValueExpression><![CDATA[new java.math.BigDecimal(0) " + "]]></initialValueExpression>"
					+ "</variable>\n";

		}

		for (int i = 0; i < campos.length; i++) {
			if (isTabular()) {
				Map c = (Map) campos[i];
				jasperFields += "<field name=\""
						+ getAtributoCampo(c.get("CTX").toString(), c.get("id").toString(), "queryFieldName")
						+ "\" class=\"" + getFieldType(c.get("CTX").toString(), c.get("id").toString()) + "\"/>\n";
			} else
				jasperFields += "<field name=\""
						+ getAtributoCampo(getSubcontexto(), campos[i].toString(), "queryFieldName") + "\" class=\""
						+ getFieldType(getSubcontexto(), campos[i].toString()) + "\"/>\n";

		}
		if (result.trim().endsWith(",")) {
			result = result.substring(0, result.lastIndexOf(","));
		}

		if (containsSubReport()) {

			// result += " , " + getAtributoContexto(getContexto(), "id") + "."
			// + getAtributoContexto(getContexto(), "key");
			// result += Dialeto.getInstance().getDialeto(user).getAliasCampo()
			// + " chave_primaria";
			jasperFields += "<field name=\"" + chave + "\" class=\"java.math.BigDecimal\"/>\n";
		}

		// }
		// queryCount = "select count(*) as ct " + tk;

		final Object[] registros = getSort();
		String ordem = " ";
		for (int i = 0; i < registros.length; i++) {
			if (getSubcontexto() != null
					&& !getAtributoContexto(getSubcontexto(), "table").equalsIgnoreCase("*VIRTUAL*")) {
				if (!registros[i].toString().startsWith(getSubcontexto()) && (containsSubReport() || isSubreport())) {
					continue;
				}
			}

			try {
				if (ordem.trim().length() > 0) {
					ordem += " , ";
				}
				final String t = user.getUtil().replace(registros[i].toString(), new String[] { "-", "*C*", "*D*" },
						new String[] { " ", " asc ", " desc " });
				ordem += t;
			} catch (final Exception e) {

				LogUtil.exception(e, user);
			}

		}

		result += query + " ]]></queryString>";
		String subreports = "";
		if (containsSubReport()) {

			for (final Object subcontexto2 : getSubcontextos()) {
				subreports += "<group  name=\"sr" + subcontexto2 + " \" >" + "<groupExpression><![CDATA[$F{" + chave
						+ "}]]></groupExpression>" + "<groupHeader>" + "<band height=\"0\"  isSplitAllowed=\"true\" >"
						+ "</band>" + "</groupHeader>" + "<groupFooter>" + "<band height=\"" + getAltura(0)
						+ "\"  isSplitAllowed=\"true\" >" + "		<staticText>" + "	<reportElement"
						+ "		mode=\"Opaque\"" + "		x=\"" + 0 + "\"" + "		y=\"" + 0 + "\""
						+ "		width=\""
						+ getLargurapx(getSubcontexto(), getAtributoContexto(subcontexto2.toString(), "label")) + "\""
						+ "		height=\"" + (getAltura(0) - 5) + "\"" + "		forecolor=\""
						+ getCor("tCorDaFonte", subcontexto2.toString()).toString() + "\"" + "		backcolor=\""
						+ getCor("tCorDeFundo", subcontexto2.toString()) + "\"" + "		key=\"staticText" + subcontexto2
						+ "\"" + "		stretchType=\"RelativeToBandHeight\""
						+ "		positionType=\"FixRelativeToTop\"" + "		isPrintRepeatedValues=\"true\""
						+ "		isRemoveLineWhenBlank=\"true\"" + "		isPrintInFirstWholeBand=\"true\""
						+ "		isPrintWhenDetailOverflows=\"true\"" + "/>\n"
						// +
						// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
						// lineColor=\"#000000\"/>"
						// +
						// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
						// lineColor=\"#000000\"/>"
						// +
						// " <bottomPen lineWidth=\"0.0\"
						// lineColor=\"#000000\"/>"
						// +
						// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
						// lineColor=\"#FFFFFF\"/> </box>\n"
						+ "	<textElement " + "textAlignment=\"" + "Center"
						+ "\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
						+ "		<font fontName=\"" + getFonte("", "fonte") + "\" pdfFontName=\"Helvetica\" size=\""
						+ getFonte("", "tamanhoDaFonte") + "\" isBold=\"" + getFonte("", "negrito") + "\" isItalic=\""
						+ getFonte("", "italico") + "\" isUnderline=\"" + getFonte("", "sublinhado")
						+ "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode()
						+ "\" isStrikeThrough=\"false\" />\n" + "	</textElement>" + "<text><![CDATA["
						+ getAtributoContexto(subcontexto2.toString(), "label") + "]]></text>" + "</staticText>" +

						" <subreport  isUsingCache=\"true\" >" + "			<reportElement"
						+ "					mode=\"Opaque\"" + "				x=\""
						+ getLargurapx(getSubcontexto(), getAtributoContexto(subcontexto2.toString(), "label")) + "\""
						+ "				y=\"" + 0 + "\"" + "				width=\""
						+ (x == 0 ? 842
								: String.valueOf(
										x - Integer
												.valueOf(getLargurapx(getSubcontexto(),
														getAtributoContexto(subcontexto2.toString(), "label")))
												.intValue()))
						+ "\"" + "				height=\"" + (getAltura(0) - 5) + "\""
						+ "				forecolor=\"#000000\"" + "				backcolor=\"#FFFFFF\""
						+ "				key=\"subreport-1\"" + "				stretchType=\"RelativeToBandHeight\""
						+ "				positionType=\"Float\"" + "				isPrintRepeatedValues=\"true\""
						+ "					isRemoveLineWhenBlank=\"true\""
						+ "					isPrintInFirstWholeBand=\"true\""
						+ "				isPrintWhenDetailOverflows=\"true\"/>"
						+ "				<subreportParameter  name=\"" + chave + "\">"
						+ "					<subreportParameterExpression><![CDATA[$F{" + chave
						+ "}]]></subreportParameterExpression>" + "				</subreportParameter>"
						+ "				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>"
						+ "				<subreportExpression class=\"net.sf.jasperreports.engine.JasperReport\">"
						+ "   <![CDATA[$P{SUB_" + subcontexto2 + "}]]>" + "  </subreportExpression>"
						+ "		</subreport>"

						+ "</band>" + "</groupFooter>" + "</group>";

				// xh+=10;
			}
		}

		result = result + jasperFields + jaspervars + subreports;

		return result;
	}

	private Object[] getSort() {
		// TODO Auto-generated method stub
		return new String[0];
	}

	private Map getFiltros(Object object) {
		// TODO Auto-generated method stub
		Map s = new HashMap();

		Map f = (Map) params.get("FILTROS");
		if (object == null)
			return f;
		else {
			if (f.containsKey(object))
				s.put(object, f.get(object));
		}

		return s;
	}

	private String[] getSumFields() {
		containsCont();
		containsDP();
		containsMax();
		containsMed();
		containsMin();
		containsSum();
		containsVar();
		// TODO Auto-generated method stub
		return (String[]) sf.toArray(new String[] {});
	}

	private boolean containsSubReport() {
		// TODO Auto-generated method stub
		return getSubcontextos().length > 0;
	}

	public String getStaticText(String ctx, final String o) {

		final String conteudo = "		<staticText>" + "	<reportElement" + "		mode=\"Opaque\"" + "		x=\""
				+ x + "\"" + "		y=\"" + y + "\"" + "		width=\"" + getLargurapx(ctx, o) + "\""
				+ "		height=\"" + getAltura(0) + "\"" + "		forecolor=\"" + getCor("cCorDaFonte", o).toString()
				+ "\"" + "		backcolor=\"" + getCor("cCorDeFundo", o) + "\"" + "		key=\"staticText" + o + "\""
				+ "		stretchType=\"RelativeToBandHeight\"" + "		positionType=\"FixRelativeToTop\""
				+ "		isPrintRepeatedValues=\"true\"" + "		isRemoveLineWhenBlank=\"true\""
				+ "		isPrintInFirstWholeBand=\"true\"" + "		isPrintWhenDetailOverflows=\"true\"" + "/>\n"
				// +
				// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#000000\"/>"
				// +
				// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#000000\"/>"
				// + " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
				// +
				// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#FFFFFF\"/> </box>"
				+ "	<textElement " + "textAlignment=\"" + getAlinhamento(0)
				+ "\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">" + "		<font fontName=\""
				+ getFonte(o, "fonte") + "\" pdfFontName=\"Helvetica\" size=\"" + getFonte(o, "cTamanhoDaFonte")
				+ "\" isBold=\"" + getFonte(o, "cNegrito") + "\" isItalic=\"" + getFonte(o, "cItalico")
				+ "\" isUnderline=\"" + getFonte(o, "cSublinhado") + "\" isPdfEmbedded =\"false\" pdfEncoding =\""
				+ user.getEncode() + "\" isStrikeThrough=\"false\" />\n" + "	</textElement>" + "<text><![CDATA["
				+ user.getLabel(getAtributoCampo(ctx, o, "label")) + "]]></text>" + "</staticText>";

		x += Integer.parseInt(getLargurapx(ctx, o));

		if (getAtributoPagina("ishtml").equalsIgnoreCase("html")) {
			x += 20;
		}

		return conteudo;
	}

	public Object[] getSubcontextos() {
		ArrayList s = new ArrayList();
		if (isSubreport())
			return s.toArray();
		Object[] o = ((Object[]) params.get("CONTEXTOS"));
		for (int i = 0; i < o.length; i++) {
			if (o[i].toString().equalsIgnoreCase(params.get("CONTEXTO").toString()))
				continue;
			s.add(o[i]);
		}
		return s.toArray();
	}

	public Object[] getContextos() {

		return ((Object[]) params.get("CONTEXTOS"));

	}

	public String getSumContent(final String ctx, final String Field, String tipo, final int ya, final Map or,
			final int i, final int inc) {
		return "<textField isStretchWithOverflow=\"true\" pattern=\"" + getAtributoCampo(ctx, Field, "mask")
				+ "\" isBlankWhenNull=\"false\" evaluationTime=\"Now\" hyperlinkType=\"None\"  hyperlinkTarget=\"Self\" >"
				+ "	<reportElement" + "		mode=\"Opaque\"" + "		x=\"" + xsumheader + "\"" + "		y=\"" + ya
				+ "\"" + "			width=\"" + i + "\"" + "			height=\"" + inc + "\""
				+ "			forecolor=\"" + getCor("corDaFonte", "") + "\"" + "			backcolor=\""
				+ getCor("corDeFundo", "") + "\"" + "			key=\"staticTextPRAE-" + xsumheader + "\""
				+ "			stretchType=\"RelativeToBandHeight\"" + "			positionType=\"FixRelativeToTop\""
				+ "			isPrintRepeatedValues=\"true\"" + "			isRemoveLineWhenBlank=\"true\""
				+ "			isPrintInFirstWholeBand=\"true\"" + "			isPrintWhenDetailOverflows=\"true\"/>"
				// +
				// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#000000\"/>"
				// +
				// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#000000\"/>"
				// + " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
				// +
				// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#FFFFFF\"/> </box>"
				+ "		<textElement textAlignment=\"" + getAlinhamento(0)
				+ "\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
				+ "			<font fontName=\"" + getFonte(Field, "fonte") + "\" pdfFontName=\"Helvetica\" size=\""
				+ getFonte(Field, "tamanhoDaFonte") + "\" isBold=\"" + getFonte(Field, "negrito") + "\" isItalic=\""
				+ getFonte(Field, "italico") + "\" isUnderline=\"" + getFonte(Field, "sublinhado")
				+ "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode() + "\" isStrikeThrough=\"false\" />"
				+ "		</textElement>" +

				"<textFieldExpression   class=\"java.math.BigDecimal\"><![CDATA[$V{" + ctx + "_" + Field + "_" + tipo
				+ "}]]></textFieldExpression>" + "</textField>";

	}

	public String getSumHeader(final String Titulo, final Map or, final int size, final int inc) {
		final Object[] campos = getCampos(getSubcontexto());
		if (tempsum.containsKey(Titulo)) {
			return "";
		} else {
			tempsum.put(Titulo, "");
			if (xsumheader == 0) {
				xsumheader = Integer.valueOf(getLargurapx(getSubcontexto(), campos[0].toString())).intValue();
				old_size = size;
			} else {
				xsumheader += old_size;
				old_size = size;
			}
			return "	<staticText>" + "	<reportElement" + "		mode=\"Opaque\"" + "		x=\"" + xsumheader
					+ "\"" + "		y=\"0\"" + "		width=\"" + size + "\"" + " 	height=\"" + inc + "\""
					+ " 	forecolor=\"" + getCor("tCorDaFonte", "") + "\"" + "		backcolor=\""
					+ getCor("tCorDeFundo", "") + "\"" + "		key=\"staticTextPRAE-4\""
					+ "			stretchType=\"RelativeToBandHeight\"" + "			positionType=\"FixRelativeToTop\""
					+ "			isPrintRepeatedValues=\"true\"" + "			isRemoveLineWhenBlank=\"true\""
					+ "			isPrintInFirstWholeBand=\"true\"" + "			isPrintWhenDetailOverflows=\"true\"/>"
					// +
					// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#000000\"/>"
					// +
					// " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
					// +
					// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
					// lineColor=\"#FFFFFF\"/> </box>"
					+ "		<textElement textAlignment=\"" + getAlinhamento(0)
					+ "\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">"
					+ "			<font fontName=\"" + getFonte(Titulo, "fonte") + "\" pdfFontName=\"Helvetica\" size=\""
					+ getFonte(Titulo, "tamanhoDaFonte") + "\" isBold=\"" + getFonte(Titulo, "negrito")
					+ "\" isItalic=\"" + getFonte(Titulo, "italico") + "\" isUnderline=\""
					+ getFonte(Titulo, "sublinhado") + "\" isPdfEmbedded =\"false\" pdfEncoding =\"" + user.getEncode()
					+ "\" isStrikeThrough=\"false\" />" + "		</textElement>" + "<text><![CDATA["
					+ user.getLabel(Titulo) + "]]></text>" + "</staticText>";

		}
	}

	public String getSummary() {
		String aux = "	<summary>";
		if (isSubreport()) {
			aux += "	<band height=\"0\"  isSplitAllowed=\"true\" >";
		} else {
			aux += "	<band height=\"23\"  isSplitAllowed=\"true\" >";
		}

		aux += "	</band>" + "	</summary>" + "</jasperReport>";

		return aux;

	}

	public void getTextField(String ctx, final String o) {

		String result = "		<textField isStretchWithOverflow=\"true\" pattern=\"" + getAtributoCampo(ctx, o, "mask")
				+ "\" isBlankWhenNull=\"true\" evaluationTime=\"Now\" hyperlinkType=\"None\" >" + "<reportElement"
				+ "	mode=\"Opaque\"" + "	x=\"" + x + "\"" + "	y=\"" + y + "\"" + "		width=\""
				+ getLargurapx(ctx, o) + "\"" + "		height=\"" + getAltura(0) + "\"" + "		forecolor=\""
				+ getCor("corDaFonte", o) + "\"" + "		backcolor=\"" + getCor("corDeFundo", o) + "\""
				+ "		key=\"TextField" + o + "\"" + "	stretchType=\"RelativeToBandHeight\""
				+ "		isPrintRepeatedValues=\"true\"" + "		isRemoveLineWhenBlank=\"true\""
				+ "		isPrintInFirstWholeBand=\"true\"" + "		isPrintWhenDetailOverflows=\"true\"" + "/>\n"
				// +
				// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#000000\"/>"
				// +
				// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#000000\"/>"
				// + " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
				// +
				// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#FFFFFF\"/> </box>"
				+ " <textElement textAlignment=\"" + getAlinhamento(0)
				+ "\" verticalAlignment=\"Top\" rotation=\"None\" lineSpacing=\"Single\">" + "		<font fontName=\""
				+ getFonte(o, "fonte") + "\" pdfFontName=\"Helvetica\" size=\"" + getFonte(o, "tamanhoDaFonte")
				+ "\" isBold=\"" + getFonte(o, "negrito") + "\" isItalic=\"" + getFonte(o, "italico")
				+ "\" isUnderline=\"" + getFonte(o, "sublinhado") + "\" isPdfEmbedded =\"false\" pdfEncoding =\""
				+ user.getEncode() + "\" isStrikeThrough=\"false\" />\n" + "	</textElement>"
				+ "	<textFieldExpression   class=\"" + getFieldType(ctx, o) // neste
				// ponto
				// para
				// campos
				// count
				+ "\"><![CDATA[";
		if (getAtributoCampo(ctx, o, "type").equalsIgnoreCase("concat")) {
			result += "$P{REPORT_SCRIPTLET}.concatResult(";
		}
		if (getAtributoCampo(ctx, o, "type").equalsIgnoreCase("bit")
				|| "checkbox".equals(getAtributoCampo(ctx, o, "component"))) {

			result += "$F{" + getAtributoCampo(ctx, o, "queryFieldName") + "}.equals(\"T\")?\"Sim\":\"No\" ";

		} else {
			result += "$F{" + getAtributoCampo(ctx, o, "queryFieldName") + "}";
		}
		if (getAtributoCampo(ctx, o, "type").equalsIgnoreCase("concat")) {
			result += ", \"" + getAtributoCampo(ctx, o, "column") + " \")";
		}
		result += "" + "]]></textFieldExpression>" + "	</textField>";

		TextFields += result;
	}

	public String getTextFields() {
		return TextFields;
	}

	private String getTipoSumario(final String tipo) {
		if (tipo.equalsIgnoreCase("CO")) // contagem
		{
			// containsCont = true;
			return "Count";
		} else if (tipo.equals("SO")) // soma
		{
			// containsSum = true;
			return "Sum";
		} else if (tipo.equals("ME")) // media
		{
			// containsMed = true;
			return "Average";
		} else if (tipo.equals("MA")) // maximo
		{
			// containsMax = true;
			return "Highest";
		} else if (tipo.equals("CU")) // contagem distinta
		{
			// containsContd = true;
			return "Highest";
		} else if (tipo.equals("VA")) // variacao
		{
			// containsVar = true;
			return "Variance";
		} else if (tipo.equals("DP")) // desvio padrao
		{
			// containsDP = true;
			return "StandardDeviation";
		} else // minimo
		{
			// containsMin = true;
			return "Lowest";
		}

	}

	public String getTitleAndBG() {

		Map fil = getFiltros(null);
		Iterator i = fil.keySet().iterator();
		String f = user.getLabel("M_RELATORIO.selecao") + ":";
		while (i.hasNext()) {
			String object = (String) i.next();
			GenericService s = getService(object);
			f += user.getLabel((String) s.getProperties().get("label")) + " > "
					+ s.translate(fil.get(object).toString(), user, null) + " \n";
		}

		final String tit = ""

				+ "		<title>" + "<band height=\"0\"  isSplitAllowed=\"true\" >" + "</band>" + "</title>"
				+ "<pageHeader>"

				+ "		<band height=\"" + (60 + (15 * fil.size())) + "\"  isSplitAllowed=\"true\" >"
				+ "		<staticText >" + "<reportElement key=\"textField-31\" x=\"0\" y=\"48\" width=\"802\" height=\""
				+ 15 * fil.size() + "\" forecolor=\"#000000\"/><" + "textElement/><text>" + f + "</text>"
				+ "</staticText>" + "			<staticText>" + "				<reportElement"
				+ "					mode=\"Opaque\"" + "					x=\"150\"" + "					y=\"1\""
				+ "					width=\"" + (getLargurapx("", "")) + "\"" + "					height=\"47\""
				+ "					forecolor=\"" + iif(this.params, "cor_fonte_titulo", "#000000") + "\""
				+ "					backcolor=\"" + iif(this.params, "cor_backgroud_titulo", "#FFFFFF") + "\""
				+ "					key=\"staticText-2\"" + "					stretchType=\"RelativeToBandHeight\""
				+ "					positionType=\"FixRelativeToTop\""
				+ "					isPrintRepeatedValues=\"true\"" + "					isRemoveLineWhenBlank=\"true\""
				+ "					isPrintInFirstWholeBand=\"true\""
				+ "					isPrintWhenDetailOverflows=\"true\"/>\n"
				// +
				// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#000000\"/>"
				// +
				// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#000000\"/>"
				// + " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
				// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#FFFFFF\"/> </box>"
				+ "				<textElement textAlignment=\"" + getAlinhamento(0)
				+ "\" verticalAlignment=\"Middle\" rotation=\"None\" lineSpacing=\"Single\">"
				+ "					<font fontName=\"" + iif(this.params, "fonte_titulo", user.getDefaultSystemFont())
				+ "\" pdfFontName=\"Helvetica\" size=\"" + iif(this.params, "tamanho_fonte_titulo", "14")
				+ "\" isBold=\"" + getFonte("Titulo", "negrito") + "\" isItalic=\"" + getFonte("Titulo", "italico")
				+ "\" isUnderline=\"" + getFonte("Titulo", "sublinhado") + "\" isPdfEmbedded =\"true\" pdfEncoding =\""
				+ user.getEncode() + "\" isStrikeThrough=\"false\" />\n" + "				</textElement>"
				+ "			<text><![CDATA[" + iif(this.params, "NOME", "") + " ]]></text>" + "			</staticText>"
				+ "  			<image  scaleImage=\"RetainShape\" vAlign=\"Middle\" hAlign=\"Left\" isUsingCache=\"true\" evaluationTime=\"Now\" hyperlinkType=\"None\">"
				+ "<reportElement" + "						mode=\"Transparent\"" + "						x=\"0\""
				+ "						y=\"1\"" + "						width=\"150\""
				+ "						height=\"47\"" + "						forecolor=\"#000000\""
				+ "						backcolor=\"#FFFFFF\"" + "						key=\"image-logo\""
				+ "						stretchType=\"RelativeToBandHeight\""
				+ "						positionType=\"FixRelativeToTop\""
				+ "						isPrintRepeatedValues=\"true\""
				+ "						isRemoveLineWhenBlank=\"true\""
				+ "						isPrintInFirstWholeBand=\"true\""
				+ "						isPrintWhenDetailOverflows=\"true\"/>\n" +
				// +
				// " <box> <topPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#000000\"/>"
				// +
				// " <leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#000000\"/>"
				// + " <bottomPen lineWidth=\"0.0\" lineColor=\"#000000\"/>"
				// +
				// " <rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"
				// lineColor=\"#FFFFFF\"/> </box>"
				// +
				" <graphicElement stretchType=\"RelativeToTallestObject\" pen=\"None\" fill=\"Solid\" />\n"
				+ "					<imageExpression class=\"java.io.File\"><![CDATA[(File)$P{user}.getSystemPropertyFile( \"LOGO_CLIENTE\" )]]></imageExpression>				</image>"
				+ "		</band>" + "	</pageHeader>";
		return tit;

	}

	public String iif(final Map H, final String key, final String retorno) {
		if (H.containsKey(key)) {
			if (H.get(key) != null && H.get(key).toString().trim() != "") {
				return H.get(key).toString();

			} else {
				return retorno;
			}

		} else {
			return retorno;
		}

	}

	public boolean isTabular() {
		return this.params.get("ESTILO").toString().equalsIgnoreCase("T");
	}

	/**
	 * @author asega
	 * @param p
	 * @return Cria uma hashmap contendo os arquivos xml do relatorio contendo o
	 *         arquivo principal e os subrelatorios
	 */

	public HashMap makeReport()

	{
		final HashMap retorno = new HashMap();

		if (params.get("ESTILO").toString().equalsIgnoreCase("S")) {

			// preparando o sub-report
			for (Object subcontexto2 : getSubcontextos()) {

				// ProccessMonitor.root.put(
				// this.id,
				// new ProccessInfo(1, new Long("10").longValue(),
				// new Long(String.valueOf(i)).longValue(),
				// new Long("0").longValue(),
				// "Gerando Sub-Relatrio de "
				// + getAtributoContexto(
				// subcontextos[i].toString(),
				// "Label")));

				final JasperBuilder jb = new JasperBuilder(this.params, this.user);
				final String ctx = subcontexto2.toString();
				jb.setSubcontexto(ctx, false);

				final JRJdtCompiler comp = new JRJdtCompiler();
				try {
					params.put("SUB_" + ctx, comp.compileReport(JRXmlLoader.load(jb.getJasperContent(true, ctx))));
				} catch (final JRException e) {

					LogUtil.exception(e, user);
				}

			}

			final JasperBuilder jb = new JasperBuilder(params, this.user);
			// principal

			final String ctx = this.params.get("CONTEXTO").toString();

			jb.setSubcontexto(ctx, true);

			// ProccessMonitor.root.put(
			// this.id,
			// new ProccessInfo(1, new Long("10").longValue(), new Long(
			// String.valueOf("5")).longValue(), new Long("0")
			// .longValue(), "Gerando Relatrio de "
			// + getAtributoContexto(ctx, "Label")));
			params.put("PRINCIPAL", jb.getJasperContent(false, ctx));
			// ProccessMonitor.root.put(
			// this.id,
			// new ProccessInfo(1, new Long("10").longValue(), new Long(
			// String.valueOf("10")).longValue(), new Long("0")
			// .longValue(), "Gerando Relatrio de "
			// + getAtributoContexto(ctx, "Label")));

		} else // estilo tabular
		{

			final JasperBuilder jb = new JasperBuilder(params, this.user);

			jb.setSubcontexto(this.params.get("CONTEXTO").toString(), true);
			// ProccessMonitor.root.put(
			// this.id,
			// new ProccessInfo(1, new Long("10").longValue(), new Long(
			// String.valueOf("5")).longValue(), new Long("0")
			// .longValue(), "Gerando Relatrio de Tabular "));

			params.put("PRINCIPAL", jb.getJasperContent(false, "TAB"));

			// ProccessMonitor.root.put(
			// this.id,
			// new ProccessInfo(1, new Long("10").longValue(), new Long(
			// String.valueOf("10")).longValue(), new Long("0")
			// .longValue(), "Gerando Relatrio de Tabular "));

		}

		return retorno;
	}

	private void setSubcontexto(String ctx, boolean master) {
		if (!master)
			subcontexto = ctx;
		else
			subcontexto = null;
		LogUtil.debug(ctx, user);
		ServiceImpl service = getService(ctx);

		if (master || service.getParent(user) == null || service.getParent(user).getPrimaryKey() == null)
			chave = (String) service.getPrimaryKey().get("name");
		else
			chave = (String) service.getParent(user).getPrimaryKey().get("name");

		if (this.params.get("ESTILO").toString().equalsIgnoreCase("S") || !containsSubReport()) {

			GenericVO v = service.createVO(user);
			Object[] cmp = getCampos(getSubcontexto());
			String c = StringUtils.arrayToCommaDelimitedString(cmp);

			v.setMatcher(c + "," + service.getPrimaryKey().get("name"));
			v.setPaginate(false);
			v.setSort(service.getMainField("obtem", user).get("name").toString());
			if (isSubreport())
				v.searchBy(chave);
			if (getFiltros(null).get(ctx) != null)
				v.setFiltro(getFiltros(null).get(ctx).toString());

			query = user.getUtil().replace(service.getDAO().createSelect(new Object[1], v, user), "?",
					" $P{" + chave + "} ");
			aliases.putAll(v.getAliases());

		} else { // tabular

			Object[] ct = getContextos();
			List joins = new ArrayList();
			for (int i = 0; i < ct.length; i++) {

				ServiceImpl s = (ServiceImpl) service.getServiceById((String) ct[i], user);

				GenericVO v = s.createVO(user);
				Object[] cmp = getCampos((String) ct[i]);
				if (cmp.length == 0)
					continue;
				boolean sum = false;
				for (int j = 0; j < cmp.length; j++) {
					if ("context".equals(getField(ct[i].toString(), cmp[j].toString(), user).get("volatile")))
						sum = true;
					else
						sum = false;
				}

				String c = StringUtils.arrayToCommaDelimitedString(cmp);
				// v.searchBy(chave);
				if (!sum)
					v.setMatcher(c + "," + s.getPrimaryKey().get("name"));
				else
					v.setMatcher(c);

				v.setPaginate(false);
				v.setSort(service.getMainField("obtem", user).get("name").toString());

				if (getFiltros(null).get(ct[i]) != null)
					v.setFiltro(getFiltros(null).get(ct[i]).toString());
				if (sum)
					v.setControlCollection(false);
				Map jo = s.getDAO().createSelectStruture(new Object[1], v, true, user);
				if (sum)
					jo.put("volatile", sum);
				joins.add(jo);
				aliases.putAll(v.getAliases());
			}

			query = DAOUtil.makeJoin(joins, chave, service.getDAO().getEntity().get("table").toString(), " left ",
					user);

		}
	}
}
