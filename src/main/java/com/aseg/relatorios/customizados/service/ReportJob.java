package com.aseg.relatorios.customizados.service;

import static org.quartz.JobKey.jobKey;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.impl.StdScheduler;

import com.aseg.config.Constants;
import com.aseg.dao.GenericDAO;
import com.aseg.exceptions.StreamingException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.relatorios.DefaultScriptlet;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericJOB;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.util.ConnectionManager;
import com.aseg.util.DAOTemplate;
import com.aseg.vo.GenericVO;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvExporterParameter;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXhtmlExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.fill.AsynchronousFillHandle;
import net.sf.jasperreports.engine.fill.AsynchronousFilllListener;
import net.sf.jasperreports.engine.fill.JRAbstractLRUVirtualizer;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.query.JRJdbcQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.j2ee.servlets.BaseHttpServlet;

public class ReportJob extends GenericJOB implements AsynchronousFilllListener, JRExportProgressMonitor {

	@Override
	public void execute(final JobExecutionContext ctx) throws JobExecutionException {
		final Map params = (Map) ctx.getJobDetail().getJobDataMap();

		if ("F".equals(params.get("FIXO"))) {
			try {
				params.put("CONTEXTO", params.get("MODEL"));

				final JasperBuilder jb = new JasperBuilder(params,
						(Usuario) ctx.getJobDetail().getJobDataMap().get("user"));
				final HashMap relatoriosxml = jb.makeReport();
				ctx.getJobDetail().getJobDataMap().put("reports", relatoriosxml);
			} catch (Exception e) {
				e.printStackTrace();
				LogUtil.exception(e, user);
				return;
			}
		}
		this.std = (StdScheduler) params.get("SHEDULER");
		this.JobName = (String) params.get("JOB_NAME");
		this.GroupName = (String) params.get("GROUP_NAME");

		try {

			this.processReport(ctx.getJobDetail().getJobDataMap());
		} catch (final Exception e) {
			e.printStackTrace();
			throw new JobExecutionException(e);
		}

	}

	public void removeReport(final String key) {

		this.reportCancelled();

	}

	private final String defaultFileName = "pjweb_relatorio";

	JasperPrint jasperPrint = null;
	StdScheduler std = null;

	String JobName = null;
	String GroupName = null;

	String TID = "";

	Connection con = null;
	String dir = null;

	public String getTID() {
		return TID;
	}

	public void setTID(String tID) {
		TID = tID;
	}

	/**
	 * Descrio do mtodo
	 * 
	 * @param request
	 *            Descrio do parmetro
	 * @param response
	 *            Descrio do parmetro
	 * @exception IOException
	 *                Descrio da exceo
	 * @exception ServletException
	 *                Descrio da exceo
	 * @since 26 de Junho de 2002
	 */
	public void processReport(final Map params) throws Exception {

		this.user = (Usuario) params.get(Constants.RAIN_USER);
		this.vo = params;
		this.contexto = (String) ((Map) vo).get("CONTEXTO");

		this.urlbase = user.getUtil().getContext_root();

		this.setFileIndex(1);
		this.setTotalSize(new Long("0").longValue());
		this.setBytesRead(new Long("0").longValue());
		this.setStatus(this.user.getLabel("M_RELATORIO.preparando"));

		this.setTID(user.getSessionId());

		this.run();
		int cont = 0;
		while (wait && !cancel) {
			cont++;
			Thread.sleep(10000);
			// if (cont > 360) {
			// System.out.println("TIME OUT REPORT");
			// throw new JobExecutionException();
			// }
		}

		if (cancel) {
			terminate();
		}

	}

	public String getIdRelatorio() {
		try {
			return this.vo.get("ID_RELATORIO").toString();
		} catch (Exception e) {
			return null;
		}
	}

	private void setSystemProperties() {

		String pathJR = user.getSession().getServletContext().getRealPath("/WEB-INF/lib");
		System.setProperty("jasper.reports.compile.class.path", pathJR);
		// System.setProperty(
		// "jasper.reports.compile.class.path",
		// util.appcontext
		// .getRealPath("/WEB-INF/lib/jasperreports-4.0.0.jar")
		// + System.getProperty("path.separator")
		// + util.appcontext.getRealPath("/WEB-INF/classes/"));
		/*
		 * final Properties prop = System.getProperties(); if (false) {
		 * prop.put("awt.toolkit", "com.eteks.awt.PJAToolkit");
		 * System.setProperties(prop); System.setProperty("java.awt.headless",
		 * Constants.true_str); System.setProperty("java.awt.graphicsenv",
		 * "com.eteks.java2d.PJAGraphicsEnvironment");
		 * 
		 * }
		 */

	}

	String contexto = "";

	String queryPrefix = ""; // Hierarqui

	int count = 0;

	String ct = "";

	private JRExporter exporter = null;

	String ext = "";

	private AsynchronousFillHandle ffil = null;

	Map imagesMap;

	int numreg = 0;

	// private byte[] output1 = null;

	int paginas = 0;

	DAOTemplate tem = null;

	String tipo = "";

	String urlbase = "";

	public Usuario user = null;

	JRAbstractLRUVirtualizer virtualizer = null;

	// ObjReport objReport = null;
	Boolean zipFile = null;

	Map vo = null;

	private boolean wait;

	private boolean cancel = false;

	private boolean success = false;

	private boolean fail = false;

	@Override
	public void afterPageExport() {
		count++;
	}

	public boolean canceled() {
		return cancel;
	}

	public void Cancel() throws JobExecutionException {
		terminate();
		cancel = true;

	}

	public void enviar(GenericVO vo, Usuario user) {
		HttpServletResponse response = (HttpServletResponse) vo.get("RESPONSE");
		HttpServletRequest request = (HttpServletRequest) vo.get("REQUEST");
		if (this.exporter != null) {

			// response.setContentLength(this.output1.length);

			try {

				if (this.vo.get("ZIP") != null && Boolean.valueOf(this.vo.get("ZIP").toString()).booleanValue()) {
					// this.zipar(
					// response,
					// this.output1,
					// this.getTitulo().trim().length() == 0 ? "SemTitulo"
					// : this.getTitulo(), ext);

				} else {
					String titulo = this.getTitulo().trim().length() == 0 ? "SemTitulo" : this.getTitulo();
					titulo = user.getUtil().encode(titulo, user);
					titulo += "." + ext;

					ByteArrayOutputStream outStream = new ByteArrayOutputStream();

					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outStream);

					exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING,
							request.getCharacterEncoding() == null ? user.getEncode() : request.getCharacterEncoding());

					exporter.exportReport();

					response.setContentType(ct);
					if (!ext.equalsIgnoreCase("html")) {
						response.setHeader("Pragma", "");
					}
					response.setHeader("Cache-Control", "");
					response.setHeader("Content-Disposition",
							"attachment; filename=\"" + user.getUtil().decode(titulo, user) + "\"");
					// response.setContentLength(this.output1.length);
					request.getSession().setAttribute(BaseHttpServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE,
							this.jasperPrint);
					request.getSession().setAttribute("IMAGES_MAP", imagesMap);

					response.getOutputStream().write(outStream.toByteArray());

					response.getOutputStream().flush();
					terminate();
				}
				// JasperRender.removeReport(user.getSessionId());
			} catch (JRException re) {
				terminate();
				cancel = true;
				if (ext.equals("xls")) {
					throw new StreamingException(
							user.getLabel(
									"M_RELATORIO.LIMITE_DE_LINHAS_EXCEDIDO_INSIRA_UM_FILTRO_OU_UTILIZE_FORMATO_XSLX"),
							user.getLabel(
									"M_RELATORIO.LIMITE_DE_LINHAS_EXCEDIDO_INSIRA_UM_FILTRO_OU_UTILIZE_FORMATO_XSLX"));
				}
			} catch (final Exception e) {
				LogUtil.exception(e, user);
			}

		} else {
			this.removeReport(this.tipo + request.getSession().getId());

		}
		wait = false;
	}

	public void exportar(final String tipo) {
		// this.status = 6;
		this.paginas = jasperPrint.getPages().size();

		switch (tipo.hashCode()) {
		case 79058: // "PDF".hashCode():
			ct = "application/pdf";
			ext = "pdf";
			exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, this);

			// this.output1 = exportReportToBytes(jasperPrint, exporter);
			break;

		case 67046: // "CSV".hashCode():
			ct = "text/csv";
			ext = "csv";
			exporter = new JRCsvExporter();
			try {
				String separador = this.user.getSystemUserProperty("SEPARADOR_CSV");
				if (separador == null) {
					separador = ";";
				}
				exporter.setParameter(JRCsvExporterParameter.FIELD_DELIMITER, separador);
			} catch (final Exception e) {
				LogUtil.exception(e, user);
			}

			exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, this);
			jasperPrint.setProperty("net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.1", "columnHeader");

			break;

		case 87007: // "XLS".hashCode():
			ct = "application/vnd.ms-excel";
			ext = "xls";
			exporter = new JRXlsExporter();
			exporter.setParameter(JExcelApiExporterParameter.MAXIMUM_ROWS_PER_SHEET, 65536);
			exporter.setParameter(JExcelApiExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JExcelApiExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
			exporter.setParameter(JExcelApiExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			jasperPrint.setProperty("net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.1", "columnHeader");
			exporter.setParameter(JExcelApiExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, this);
			exporter.setParameter(JRXlsAbstractExporterParameter.SHEET_NAMES, new String[] { "Relatrio ProJuris Web" });

			break;

		case 10: // "ODS".hashCode():
			ct = "application/vnd.oasis.opendocument.calc";
			ext = "ods";
			exporter = new JROdsExporter();
			exporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, this);
			jasperPrint.setProperty("net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.1", "columnHeader");
			exporter.setParameter(JRXlsAbstractExporterParameter.SHEET_NAMES,
					new String[] { user.getLabel("M_RELATORIO.sheet_names") });

			break;

		case 2228139: // "HTML".hashCode():
			ct = "text/html";
			ext = "html";
			exporter = new JRXhtmlExporter();
			// final String pat = urlbase + "/";
			// final File direct = new
			// File(util.appcontext.getRealPath("/"));
			exporter.setParameter(JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRHtmlExporterParameter.HTML_HEADER,
					"<head>  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/> </head>");

			exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, this);

			/* printWriter here is a reference for response.getWriter() */
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			jasperPrint.setProperty("net.sf.jasperreports.export.xls.exclude.origin.keep.first.band.1", "columnHeader");
			// exporter.setParameter(JRExporterParameter.OUTPUT_WRITER,response.getWriter());
			// gonna set url pattern given for Image servlet with a reponse
			// parameter <url-pattern>/imageServlet</url-pattern>
			// exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI,
			// "imageServlet?image=");
			// imagesMap = new HashMap();
			// exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP,
			// imagesMap);

			// this.output1 = exportReportToBytes(jasperPrint, exporter);
			break;
		case 81476: // "RTF".hashCode():
			ct = "application/rtf";
			exporter = new JRRtfExporter();
			exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, this);
			ext = "rtf";
			// this.output1 = exportReportToBytes(jasperPrint, exporter);

			break;

		case 78111: // "ODT".hashCode():
			ct = "application/vnd.oasis.opendocument.text";
			exporter = new JROdtExporter();
			exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, this);
			ext = "odt";
			// this.output1 = exportReportToBytes(jasperPrint, exporter);

			break;

		case 2697305: // "RTF".hashCode():
			ct = "application/xslx";
			exporter = new JRXlsxExporter();
			exporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporter.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
			exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, this);
			exporter.setParameter(JRXlsAbstractExporterParameter.SHEET_NAMES,
					new String[] { user.getLabel("M_RELATORIO.sheet_names") });
			exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, this);
			ext = "xlsx";
			// this.output1 = exportReportToBytes(jasperPrint, exporter);

			break;

		case 2103872: // "DOCX".hashCode():
			ct = "application/docx";
			exporter = new JRDocxExporter();
			exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, this);
			ext = "docx";
			// this.output1 = exportReportToBytes(jasperPrint, exporter);

			break;

		default:
			break;
		}

		wait = true;

	}

	public int getPaginasExportadas() {
		return this.count;
	}

	public String getSessionId() {
		return this.user.getSessionId();
	}

	public String getTitulo() {
		try {
			return (String) ((Map) vo).get("TITULO") == null ? (String) ((Map) vo).get("NOME")
					: (String) ((Map) vo).get("TITULO");
		} catch (final Exception ex) {
			return "";
		}
	}

	public int getTotalPaginas() {
		return this.paginas;
	}

	public int getTotalRegistros() {
		return this.numreg;
	}

	private JasperReport load(final String string, String prefix) {

		final GenericService s = (GenericService) user.getFactory().getSpringBean("RelatorioFixoService",
				user);
		GenericVO vo = s.createVO(user);
		vo.putKey("ID_RELATORIO", string);
		try {
			vo = s.obtem(vo, user);
			final net.sf.jasperreports.engine.design.JRJdtCompiler comp = new net.sf.jasperreports.engine.design.JRJdtCompiler();
			return comp.compileReport(streamToJasper(new ByteArrayInputStream((byte[]) vo.get("ARQUIVO_XML")), prefix));

		} catch (final Exception e) {

			LogUtil.exception(e, user);
		}

		return null;
	}

	public void parar() {
		try {
			this.wait();
			this.virtualizer.wait();
			this.ffil.wait();
		} catch (final Exception ex) {
			LogUtil.exception(ex, user);
		}
	}

	public void terminate() {
		this.exporter = null;
		try {
			if (this.con != null && !this.con.isClosed()) {
				this.con.close();
			}
			if (this.ffil != null) {
				this.ffil.cancellFill();
			}
			if (this.virtualizer != null) {
				this.virtualizer.cleanup();
			}
			if (dir != null && !new File(dir).mkdir()) {
				user.getUtil().deleteDir(new File(dir));
			}

			this.std.interrupt(jobKey(JobName, GroupName));
			try {
				this.finalize();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				LogUtil.exception(e, user);
			}

		}

		catch (final java.lang.IllegalStateException e) {
			// LogUtil.exception(e, user);
		} catch (final Exception e) {
			LogUtil.exception(e, user);
		}
	}

	@Override
	public void reportCancelled() {

		terminate();
	}

	@Override
	public void reportFillError(final Throwable e) {
		LogUtil.exception(e, user);

		this.setFileIndex(1);
		this.setTotalSize(new Long("-1").longValue());
		this.setBytesRead(new Long("-1").longValue());
		this.setStatus(e.getMessage());

		terminate();
	}

	@Override
	public void reportFinished(final JasperPrint pr) {
		// this.status = 5;

		this.jasperPrint = pr;
		this.exportar((String) this.vo.get("TIPO"));
		if (this.getBytesRead() != -1) {
			this.setStatus(user.getLabel("M_RELATORIO.concluido"));
			this.setInProgress(false);
			this.success = true;
			this.setTotalSize(new Long("1").longValue());
			this.setBytesRead(new Long("1").longValue());
		}
		try {
			if (this.con != null && !this.con.isClosed()) {
				this.con.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LogUtil.exception(e, user);
		}

	}

	public void restart() {

		this.ffil.startFill();
	}

	public void run() {
		dir = System.getProperty("java.io.tmpdir") + File.separator + this.user.getSessionId();
		if (!new File(dir).mkdir()) {
			user.getUtil().deleteDir(new File(dir));
			new File(dir).mkdir();
		}

		if (cancel) {
			return;
		}

		JRSwapFile swapFile = new JRSwapFile(dir, 1024, 1024);

		this.virtualizer = new JRSwapFileVirtualizer(1, swapFile, true);
		this.virtualizer.setReadOnly(true);

		this.setFileIndex(1);
		this.setTotalSize(new Long("1").longValue());
		this.setBytesRead(new Long("0").longValue());
		this.setStatus(user.getLabel("M_RELATORIO.iniciando"));

		// this.status = 1;
		JasperDesign jasperDesign = null;
		final HashMap params = new HashMap();
		if (!isFixo()) {
			this.setStatus(this.user.getLabel("M_RELATORIO.lendoArquivos"));
			try {
				jasperDesign = JRXmlLoader.load((File) vo.get("PRINCIPAL"));
			} catch (final JRException e) {
				LogUtil.exception(e, user);
			}

		}

		else {

			jasperDesign = streamToJasper(new ByteArrayInputStream((byte[]) ((Map) vo).get("ARQUIVO_XML")), "");

		}
		if (cancel) {
			return;
		}
		this.setStatus(this.user.getLabel("M_RELATORIO.contandoRegistros"));
		String q = "";
		try {
			q = jasperDesign.getQuery().getText();
			final ServiceImpl c = (ServiceImpl) ((ServiceImpl) user.getUtil().getServiceById(contexto, user));
			if (!isFixo() && q.lastIndexOf("FROM " + c.getDAO().getEntity().get("table")) != -1)
				q = q.substring(q.lastIndexOf("FROM " + c.getDAO().getEntity().get("table")));
			else
				q = q.substring(q.lastIndexOf("FROM"));

		} catch (final Exception e) {
			LogUtil.debug(q, user);
			LogUtil.exception(e, user);
			throw new RuntimeException("A clausula from da query do relatorio deve estar toda em letras maiusculas");
		}
		String Filtro = "";

		// if (getSubRelatorios() != null) {
		//
		// final Iterator i1 = getSubRelatorios().keySet().iterator();
		// int cont = 0;
		//
		// while (i1.hasNext()) {
		// final String element = (String) i1.next();
		//
		// final ServiceImpl f = (ServiceImpl) ((ServiceImpl) user
		// .getUtil().getFactory()
		// .getSpringBean("RelatorioFixoService", user))
		// .getServiceById(element, user);
		//
		// final GenericVO vo = f.createVO(user);
		// final GenericDAO filter = f.getDAO();
		//
		// Filtro = (String) getSubRelatorios().get(element);
		// if (Filtro.length() > 3 && !Filtro.equalsIgnoreCase("null")) {
		// String ft = "";
		// ft = filter.getSqlWhere(Filtro.substring(0, 2), Filtro,
		// this.user, vo, null, false);
		//
		// vo.put("isReport", true);
		//
		// params.put("filtro_string_" + Filtro.substring(0, 2),
		// filter.translate(Filtro, this.user, vo));
		//
		// if (cont == 0) {
		//
		// String sb[] = ((GenericService) getServiceById(
		// "pegar o contexto do FIltro", this.user))
		// .getDAO().getSegmentBy(this.user, "DOWN",
		// contexto, vo);
		// queryPrefix += " " + sb[0];
		//
		// params.put("filtro", " and " + sb[1] + " and " + ft);
		// } else {
		// params.put("filtro" + cont, " and " + ft);
		// }
		//
		// } else {
		// if (cont == 0) {
		//
		// String sb[] = ((GenericService) getServiceById(element,
		// this.user)).getDAO().getSegmentBy(this.user,
		// "DOWN", contexto, vo);
		// queryPrefix = sb[0];
		//
		// params.put("filtro", " and " + sb[1]);
		// }
		//
		// }
		// cont++;
		// }
		// }

		if (getFiltros() != null && getFiltros().size() > 0) // Fixos
		{
			// int cont =0;
			final Iterator i1 = getFiltros().keySet().iterator();
			while (i1.hasNext()) {
				final String element = i1.next().toString();
				Filtro = getFiltros().get(element).toString();
				final ServiceImpl f = (ServiceImpl) ((ServiceImpl) user.getUtil()).getServiceById(element, user);
				final GenericDAO filter = f.getDAO();
				final GenericVO vo = f.createVO(user);
				if (Filtro.trim().length() > 0) {

					vo.put("isReport", true);

					params.put("filtro_string_" + element, filter.translate(Filtro, this.user, vo));

					String ft = "";
					ft = filter.getSqlWhere(element, user.getUtil().decode(Filtro, user), user, vo, null, false);

					if (ft.trim().length() < 2) {
						ft = "(1=1)";
					}

					if (ft.trim().length() > 0) {
						String sb[] = ((GenericService) getServiceById(element, this.user)).getDAO()
								.getSegmentBy(this.user, "DOWN", element, vo);
						queryPrefix += " " + sb[0];

						params.put("filtro_" + element, " and " + sb[1] + " and " + ft);
					}

					if (q.toUpperCase().indexOf("$P!{" + "FILTRO_" + element + "}") > -1) {

						if (ft.trim().length() > 0) {
							ft = "" + params.get("filtro_" + element);
						}
					}

					q = user.getUtil().replace(q.toUpperCase(), "$P!{" + "FILTRO_" + element + "}", ft);

				} else {

					String sb[] = ((GenericService) getServiceById(element, this.user)).getDAO().getSegmentBy(this.user,
							"DOWN", contexto, vo);

					queryPrefix += " " + sb[0];

					params.put("filtro_" + element, " and " + sb[1]);
					if (q.toUpperCase().indexOf("$P!{FILTRO_" + element + "}") > -1) {

						q = user.getUtil().replace(q.toUpperCase(), "$P!{FILTRO_" + element + "}", " and " + sb[1]);

					}

				}
			}
		} else {
			String sb[] = ((GenericService) getServiceById(contexto, this.user)).getDAO().getSegmentBy(this.user,
					"DOWN", contexto, ((GenericService) getServiceById(contexto, this.user)).createVO(user));
			queryPrefix += " " + sb[0];
			params.put("filtro_" + contexto, " and " + sb[1]);
			q = user.getUtil().replace(q.toUpperCase(), "$P!{FILTRO_" + contexto + "}", " and " + sb[1]);
		}

		if (q.toUpperCase().indexOf("$P!{FILTRO}") > -1) {
			String[] sb = ((GenericService) getServiceById(contexto, this.user)).getDAO().getSegmentBy(this.user,
					"DOWN", contexto, ((GenericService) getServiceById(contexto, this.user)).createVO(user));
			queryPrefix += " " + sb[0];
			q = user.getUtil().replace(q.toUpperCase(), "$P!{FILTRO}", " and " + sb[1]);
		}

		// this.status = 3;

		boolean hasGroup = false;
		int posGroup = q.toUpperCase().indexOf("GROUP");

		if (posGroup > -1) {
			hasGroup = true;
		}

		/*
		 * if (posGroup > -1 && q.toUpperCase().indexOf("BY", posGroup) > -1 &&
		 * posGroup - q.toUpperCase().indexOf("BY", posGroup) < 10) { q =
		 * q.substring(0, q.toUpperCase().indexOf("GROUP"));
		 * 
		 * }
		 */

		int posOrder = q.toUpperCase().indexOf("ORDER");

		if (posOrder > -1 && q.toUpperCase().indexOf("BY", posOrder) > -1
				&& posOrder - q.toUpperCase().indexOf("BY", posOrder) < 10) {
			q = q.substring(0, q.toUpperCase().indexOf("ORDER"));
		}

		if (queryPrefix.trim().length() > 0) {
			q = queryPrefix + " SELECT count(*) as ct " + q;
			if (isFixo()) {
				JRDesignQuery query = new JRDesignQuery();
				query.setText(queryPrefix + " " + jasperDesign.getQuery().getText());
				jasperDesign.setQuery(query);
			}

		} else {
			q = " SELECT count(*) as ct " + q;
		}

		if (hasGroup && (ConnectionManager.getInstance(user).getBancoConectado(user) == (ConnectionManager.ORACLE))) {

			if (queryPrefix.trim().length() > 0) {
				q = queryPrefix + "   SELECT count(ct) FROM (" + q + ")";

			}
			q = " SELECT count(ct) FROM (" + q + ")";
		}
		if (cancel) {
			return;
		}

		if (isFixo()) {

			String[] filhos = null;
			try {
				final GenericService lim = (GenericService) user.getFactory()
						.getSpringBean("RelatorioFixoService", user);
				filhos = (String[]) ((Map) vo).get("ID_RELATORIO_DETALHE");// lim.getFilhos(idRelatorio,
				// this.user);
			} catch (final Exception e) {
				LogUtil.exception(e, user);
			}

			int a = 0;

			for (final String filho : filhos) {

				final JasperReport ab = load(filho, queryPrefix);

				params.put("sr_" + a, ab);
				a++;
			}

		}
		try {
			if (tem == null) {
				tem = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
						((GenericService) getServiceById(contexto, this.user)).createVO(user), user.getUtil());
			}

			if (q.indexOf("count(") > -1 && !hasGroup)
				this.numreg = tem.queryForObject(q, Integer.class);
			else
				this.numreg = tem.queryForList(q).size();

			if (numreg == 0) {
				// this.status = 8;
				// root.remove(this.getSessionId());
				this.setFileIndex(1);
				this.setTotalSize(new Long("-1").longValue());
				this.setBytesRead(new Long("-1").longValue());
				this.setStatus(user.getLabel("M_RELATORIO.vazio"));

			} else {
				this.setFileIndex(1);
				this.setTotalSize(new Long(numreg).longValue());
				this.setBytesRead(new Long("0").longValue());
				this.setStatus(user.getLabel("M_RELATORIO.contados") + " " + numreg + " "
						+ user.getLabel("M_RELATORIO.aguardandoInicio") + " "
				// + user.getUtil().df.fromTimestampcomHora(new Timestamp(
				// System.currentTimeMillis()))

				);
			}

			// setSystemProperties();
			params.put("user", user);
			params.put("REPORT_LOCALE", user.getUtil().getLocale());
			params.put("REPORT_SCRIPTLET", new DefaultScriptlet());
			params.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			params.put("sessionID", this.getSessionId());
			params.put("title", getTitulo());
			params.put("ID_RELATORIO", getIdRelatorio());
			if (this.vo.get("TIPO").equals("HTML") || this.vo.get("TIPO").equals("CSV")
					|| this.vo.get("TIPO").equals("XLS") || this.vo.get("TIPO").equals("XLSX")
					|| this.vo.get("TIPO").equals("ODS")) {

				params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			}
			if (cancel) {
				return;
			}
			// LogUtil.debug(params.get(JRParameter.IS_IGNORE_PAGINATION));
			con = ConnectionManager.getInstance(user).getConexao(user);
			// con.setHoldability(ResultSet.CLOSE_CURSORS_AT_COMMIT);
			// con.set
			// con.set
			params.put(Constants.RAIN_USER, user);
			params.putAll(vo);
			final net.sf.jasperreports.engine.design.JRJdtCompiler comp = new net.sf.jasperreports.engine.design.JRJdtCompiler();
			JasperReport rep = comp.compileReport(jasperDesign);
			rep.setProperty("net.sf.jasperreports.subreport.runner.factory",
					"net.sf.jasperreports.engine.fill.JRContinuationSubreportRunnerFactory");
			rep.setProperty("net.sf.jasperreports.governor.max.pages.enabled", "false");
			rep.setProperty("net.sf.jasperreports.governor.timeout.enabled", "false");
			rep.setProperty(JRJdbcQueryExecuterFactory.PROPERTY_JDBC_CONCURRENCY, "readOnly");
			rep.setProperty(JRJdbcQueryExecuterFactory.PROPERTY_JDBC_FETCH_SIZE, "1000");

			rep.setProperty(JRJdbcQueryExecuterFactory.PROPERTY_JDBC_RESULT_SET_TYPE, "forwardOnly");

			ffil = AsynchronousFillHandle.createHandle(rep, params, con);

			ffil.addListener(this);

			// ffil.setThreadName(this.getName());
			ffil.startFill();
			wait = true;
			// this.status = 4;
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			this.setFileIndex(1);
			this.setTotalSize(new Long("0").longValue());
			this.setBytesRead(new Long("0").longValue());
			this.setStatus(e.getMessage());
			throw new RuntimeException(e);
		}
	}

	private boolean isFixo() {
		// TODO Auto-generated method stub
		return "T".equals(vo.get("FIXO"));
	}

	private Map getFiltros() {
		// TODO Auto-generated method stub

		if (vo.get("FILTROS") instanceof String)
			return user.getUtil().JSONtoMap(vo.get("FILTROS").toString());
		return (Map) vo.get("FILTROS");
	}

	private Map getSubRelatorios() {

		Map s = new HashMap();
		Iterator e = vo.keySet().iterator();
		while (e.hasNext()) {
			String type = (String) e.next();
			if (type.toUpperCase().startsWith("SUB_")) {
				s.put(user.getUtil().replace(type, "SUB_", ""), vo.get(type));
			}
		}
		return s;

	}

	private JasperDesign streamToJasper(final InputStream stream, String prefix) {

		try {
			JasperDesign a = JRXmlLoader.load(stream);
			if (prefix != null && prefix.trim().length() > 0) {
				JRDesignQuery query = new JRDesignQuery();
				query.setText(prefix + " " + a.getQuery().getText());
				a.setQuery(query);
			}
			return a;
		} catch (final JRException e) {

			LogUtil.exception(e, user);
			return null;
		}
	}

	public void zipar(final HttpServletResponse response, final byte[] a, String titulo, final String ext)
			throws IOException {
		final String temp;
		final ZipOutputStream zout = new ZipOutputStream(response.getOutputStream());
		if (titulo.trim().length() == 0) {
			titulo = defaultFileName;
		}
		// ServletContext servletContext = getServletContext();
		try {
			response.setContentType("application/x-zip-compressed");
			response.setHeader("Content-Disposition",
					"attachment; filename=\"" + titulo.replace('.', '_') + ".zip" + "\"");
			zout.putNextEntry(new ZipEntry(titulo + "." + ext));
			// byte [] a = out.toString().getBytes();
			zout.write(a, 0, a.length);
			zout.closeEntry();
			zout.finish();
			final String zip = zout.toString();
			response.setContentLength(a.length);
			response.getOutputStream().println(zip);
			response.getOutputStream().flush();
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			e.printStackTrace(new PrintWriter(response.getOutputStream()));
		}

	}

	public boolean isSuccess() {
		// TODO Auto-generated method stub
		return success;
	}

	public boolean isFail() {
		// TODO Auto-generated method stub
		return fail;
	}
}
