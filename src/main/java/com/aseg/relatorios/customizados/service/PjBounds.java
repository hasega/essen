package com.aseg.relatorios.customizados.service;

/**
 * Define o tamanho e/ou a posio de um componente em centmetros.
 * 
 * @author Haroldo Asega
 * @created 19 de Junho de 2004
 * @version
 * @since 19 de Junho de 2004
 */
public class PjBounds extends Object implements java.io.Serializable {

	/**
	 * Altura do componente. Usado tambm para definir a margem inferior da
	 * pgina.
	 * 
	 * @since 19 de Junho de 2004
	 */
	public double width;

	/**
	 * Largura do componente. Usado tambm para definir a margem direita da
	 * pgina.
	 * 
	 * @since 19 de Junho de 2004
	 */
	public double height;

	/**
	 * Posio do componente no eixo X da pgina. Usado tambm para definir a margem
	 * superior da pgina.
	 * 
	 * @since 19 de Junho de 2004
	 */
	public double x;
	/**
	 * Posio do componente no eixo Y da pgina. Usado tambem para definir a
	 * margem esquerda da pgina.
	 * 
	 * @since 19 de Junho de 2004
	 */
	public double y;

	/**
	 * Construtor que define um objeto Bounds. Todos os atributos so iguais a
	 * zero.
	 * 
	 * @since 19 de Junho de 2004
	 */
	public PjBounds() {
		this(0, 0, 0, 0);
	}

	/**
	 * Construtor que define uma largura e uma altura.
	 * 
	 * @param largura
	 *            Descrio do parmetro
	 * @param altura
	 *            Descrio do parmetro
	 * @since 19 de Junho de 2004
	 */
	public PjBounds(double largura, double altura) {
		this(largura, altura, 0, 0);
	}

	/**
	 * Construtor para o objeto Bounds
	 * 
	 * @param largura
	 *            Largura
	 * @param altura
	 *            Altura
	 * @param x
	 *            Descrio do parmetro
	 * @param y
	 *            Descrio do parmetro
	 * @since 19 de Junho de 2004
	 */
	public PjBounds(double largura, double altura, double x, double y) {
		width = largura;
		height = altura;
		this.x = x;
		this.y = y;
	}

	/**
	 * Construtor para o objeto Bounds
	 * 
	 * @param b
	 *            Descrio do parmetro
	 * @since 19 de Junho de 2004
	 */
	public PjBounds(PjBounds b) {
		this(b.width, b.height, b.x, b.y);
	}

	/**
	 * Retorna uma nova instancia deste objeto.
	 * 
	 * @return O valor bounds
	 * @since 19 de Junho de 2004
	 */
	public PjBounds getBounds() {
		return new PjBounds(this);
	}

	/**
	 * Compara este objeto com o objeto passado no mtodo.
	 * 
	 * @param object
	 *            Objeto <code>PjBounds</code> a ser comparado
	 * @return <b>true</b> Se este objeto igual ao objeto <i>object</i>
	 * @since 19 de Junho de 2004
	 */
	public boolean equals(Object object) {
		if (object instanceof PjBounds) {
			PjBounds b = (PjBounds) object;
			if (b.width == width && b.height == height && b.x == x && b.y == y) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Retorna a representao String deste Objeto.
	 * 
	 * @return String
	 * @since 19 de Junho de 2004
	 */
	public String toString() {
		String tostring = new String("Largura: " + Double.toString(width) + "\n" + "Altura : " + Double.toString(height)
				+ "\n" + "Pos X  : " + Double.toString(x) + "\n" + "Pos Y  : " + Double.toString(y));
		return tostring;
	}
}
