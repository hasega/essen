package com.aseg.relatorios;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.relatorios.customizados.service.ReportJob;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.util.ConnectionManager;
import com.aseg.util.DAOTemplate;
import com.aseg.util.correcaomonetaria.CorrecaoMonetaria;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

/**
 * @author Haroldo Asega
 * @version $Id: DefaultScriptlet.java,v 1.4.10.1 2008/04/18 19:46:37 hasega Exp
 *          $
 */
public class DefaultScriptlet extends JRDefaultScriptlet {

	public UtilServiceImpl util = null;

	/**
	 *
	 */
	@Override
	public void beforeReportInit() throws JRScriptletException {
		// LogUtil.debug("call beforeReportInit");
	}

	/**
	 *
	 */
	@Override
	public void afterReportInit() throws JRScriptletException {
		util = (UtilServiceImpl) ((Map) this.getParameterValue("REPORT_PARAMETERS_MAP")).get("UtilService");
	}

	/**
	 *
	 */
	@Override
	public void beforePageInit() throws JRScriptletException {
		// LogUtil.debug("call beforePageInit : PAGE_NUMBER = " +
		// this.getVariableValue("PAGE_NUMBER"));
	}

	/**
	 *
	 */
	@Override
	public void afterPageInit() throws JRScriptletException {
		// LogUtil.debug("call afterPageInit : PAGE_NUMBER = " +
		// this.getVariableValue("PAGE_NUMBER"));
	}

	/**
	 *
	 */
	@Override
	public void beforeColumnInit() throws JRScriptletException {
		// LogUtil.debug("call beforeColumnInit");
	}

	/**
	 *
	 */
	@Override
	public void afterColumnInit() throws JRScriptletException {
		// LogUtil.debug("call afterColumnInit");
	}

	/**
	 *
	 */
	@Override
	public void beforeGroupInit(String groupName) throws JRScriptletException {
		// if (groupName.equals("CityGroup"))
		// {
		// LogUtil.debug("call beforeGroupInit : City = " +
		// this.getFieldValue("City"));
		// }
	}

	/**
	 *
	 */
	@Override
	public void afterGroupInit(String groupName) throws JRScriptletException {
		// if (groupName.equals("CityGroup"))
		// {
		// LogUtil.debug("call afterGroupInit : City = " +
		// this.getFieldValue("City"));

		// String allCities = (String)this.getVariableValue("AllCities");
		// String city = (String)this.getFieldValue("City");
		// StringBuffer sbuffer = new StringBuffer();

		// if (allCities != null)
		// {
		// sbuffer.append(allCities);
		// sbuffer.append(", ");
		// }

		// sbuffer.append(city);
		// this.setVariableValue("AllCities", sbuffer.toString());
		// }
	}

	/**
	 *
	 */
	@Override
	public void beforeDetailEval() throws JRScriptletException {

		try {
			Usuario user = (Usuario) ((Map) this.getParameterValue("REPORT_PARAMETERS_MAP"))
					.get(Constants.RAIN_USER);
			if (user == null)
				return;

			ReportJob report = ReportUtil.getInstance(user).getReportJob(user.getSessionId(), user);

			if (report != null) {
				report.setBytesRead(new Long((Integer) this.getVariableValue("REPORT_COUNT")).longValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Object getService(final String name, final Usuario user) {

		return user.getFactory().getSpringBean(name, user);
	}

	/**
	 *
	 */
	@Override
	public void afterDetailEval() throws JRScriptletException {
	}

	public String concatResult(Object a, String sql, Usuario user) {
		try {
			final DAOTemplate tem = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, null,
					util);
			List result = tem.queryForList(sql, new Object[] { a }, String.class);
			return StringUtils.arrayToCommaDelimitedString(result.toArray());
		} catch (Exception e) {
			LogUtil.exception(e, user);
			return "!error!";
		}
	}

	/**
	 * Retorna o resultado do SQL como uma lista, mas contendo na lista apenas o
	 * campo selecionado
	 *
	 * @param sql
	 * @param a
	 * @param field
	 * @param user
	 * @return
	 */
	public List<String> resultToList(String sql, Object a, String field, Usuario user) {
		try {
			ConnectionManager connectionManager = ConnectionManager.getInstance(user);
			final DAOTemplate tem = new DAOTemplate(connectionManager.getDataSource(user), user, null, util);
			List resultSet = tem.queryForList(sql, new Object[] { a }, String.class);
			if (resultSet == null) {
				resultSet = new ArrayList<String>();
			}
			return resultSet;
		} catch (Exception e) {
			LogUtil.exception(e, user);
			return new ArrayList<String>();
		}
	}

	public BigDecimal atualizaProcessoOLD(Long idProcesso, String campo, String item, Usuario user) {
		GenericService x = (GenericService) getService("ProcessoService", user);
		GenericVO vo = x.createVO(user);

		vo.putKey("ID_PROCESSO", idProcesso);
		try {
			List v = (List) x.invoke("updateValues", vo, user);
			return (BigDecimal) ((Map) util.match(v, "BRANCO", campo).get(0)).get(item);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		return null;
	}

	public BigDecimal atualizaProcesso(Long idProcesso, String campo, String item, Usuario user) {
		GenericService x = (GenericService) getService("ProcessoService", user);
		GenericVO vo = x.createVO(user);

		vo.putKey("ID_PROCESSO", idProcesso);
		try {
			List v = (List) x.invoke("updateValues", vo, user);
			// TODO mudar a converso do campo para String (Tirar o cast para
			// int)
			// BigDecimal result = (BigDecimal) ((Map)v.get(campo)).get(item);
			BigDecimal result = (BigDecimal) ((Map) v.get(new Integer(campo).intValue())).get(item);
			return result;
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		return null;
	}

	public List atualizaProcesso(Long idProcesso, Usuario user) {
		GenericService x = (GenericService) getService("ProcessoService", user);
		GenericVO vo = x.createVO(user);

		vo.putKey("ID_PROCESSO", idProcesso);
		try {
			List result = (List) x.invoke("updateValues", vo, user);
			return result;
		} catch (Exception e) {
			return new ArrayList();
		}

	}

	public Object AtualizaValor(BigDecimal valor, Date database, Date datajuros, BigDecimal id_metodo, Usuario user,
			BigDecimal multa, String Campo) throws JRScriptletException, Exception {
		try {

			CorrecaoMonetaria monete = new CorrecaoMonetaria();

			// <property name="VALOR_ATUAL" type="money" /> // valores para o
			// campo que vc quer pegar
			// <property name="VALOR_MULTA" type="money" />
			// <property name="VALOR_JUROS" type="money" />

			GenericVO vo = new GenericVO();
			vo.put("DATA_INICIAL", new Timestamp(database.getTime()));
			vo.put("DATA_JUROS", new Timestamp(datajuros.getTime()));
			vo.put("ID_METODO_ATUALIZACAO", id_metodo);
			vo.put("MULTA", multa);
			vo.put("VALOR", valor);
			vo.put("DATA_FINAL", user.getDataFinal());

			vo = monete.calcular(vo, user);
			Double rt = 0D;
			if (Campo.equalsIgnoreCase("CM") || Campo.equalsIgnoreCase("JUROS") || Campo.equalsIgnoreCase("MULTA")
					|| Campo.equalsIgnoreCase("TOTAL")) {
				if (Campo.equalsIgnoreCase("CM")) {
					rt = valor.doubleValue() + ((BigDecimal) vo.get("VALOR_CM")).doubleValue();
				}
				if (Campo.equalsIgnoreCase("JUROS")) {
					rt = valor.doubleValue() + ((BigDecimal) vo.get("VALOR_JUROS")).doubleValue();
				}
				if (Campo.equalsIgnoreCase("MULTA")) {
					rt = valor.doubleValue() + ((BigDecimal) vo.get("VALOR_MULTA")).doubleValue();
				}
				if (Campo.equalsIgnoreCase("TOTAL")) {
					rt = valor.doubleValue() + ((BigDecimal) vo.get("VALOR_CM")).doubleValue()
							+ ((BigDecimal) vo.get("VALOR_JUROS")).doubleValue()
							+ ((BigDecimal) vo.get("VALOR_MULTA")).doubleValue();
				}
			} else {
				throw new IllegalArgumentException(Campo);
			}
			return rt;
		} catch (Exception e) {
			LogUtil.exception(e, user);
			return null;
		}
	}

	public String getConfigSistema(String a, String b) {
		return "";
	}

	public String sumTimes(List<String> times) {
		return UtilServiceImpl.sumTimes(times);
	}

}
