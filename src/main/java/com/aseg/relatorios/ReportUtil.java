package com.aseg.relatorios;

import static org.quartz.JobBuilder.newJob;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdScheduler;

import com.aseg.config.Constants;
import com.aseg.exceptions.ConfigurationException;
import com.aseg.relatorios.customizados.service.ReportJob;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;

public class ReportUtil extends ServiceImpl {

	static ReportUtil instance = null;

	public static ReportUtil getInstance(final Usuario user) {
		if (instance == null) {
			instance = new ReportUtil();
		}
		return instance;

	}

	private final Map reports = new HashMap<String, String>();

	private ReportUtil() {

	}

	public boolean pageSetup(final GenericVO mVO, final Usuario user) throws Exception {

		mVO.preservOld();

		return true;
	}

	public boolean create(final GenericVO mVO, final Usuario user)

	{

		this.reports.put(user.getSessionId(), mVO.clone());

		return true;
	}

	public boolean prepareSubContexto(final GenericVO mVO, final Usuario user) throws Exception {

		Map report = (Map) this.reports.get(user.getSessionId());
		transferCampos(mVO, report);
		transferSubContextFields(report, mVO);

		if (report.get("SUBCONTEXTO") == null || (((String[]) report.get("SUBCONTEXTO")).length == 1
				&& ((String[]) report.get("SUBCONTEXTO"))[0].equals(""))) {
			try {
				mVO.put("SUBCONTEXTO", (String[]) report.get("CONTEXTO"));
			} catch (Exception e) {
				mVO.put("SUBCONTEXTO", report.get("CONTEXTO").toString().split(","));
			}

		} else {
			mVO.put("SUBCONTEXTO", report.get("SUBCONTEXTO"));
		}
		mVO.put("ID_RELATORIO", report.get("ID_RELATORIO"));
		return true;
	}

	private void transferSubContextFields(Map from, Map to) {
		if (from.get("ESTILO") != null)
			to.put("ESTILO", from.get("ESTILO"));

		if (from.get("NOME") != null)
			to.put("NOME", from.get("NOME"));

		if (from.get("DESCRICAO") != null)
			to.put("DESCRICAO", from.get("DESCRICAO"));

		if (from.get("CONTEXTO") != null)
			to.put("CONTEXTO", from.get("CONTEXTO"));

	}

	@SuppressWarnings("unchecked")
	public boolean prepareCampos(final GenericVO mVO, final Usuario user) throws Exception {

		Map report = (Map) this.reports.get(user.getSessionId());

		transferCampos(report, mVO);

		if (mVO.get("NOME") == null) {
			transferFiltros(mVO, report, user);
		} else {
			transferSubContextFields(mVO, report);
		}
		keepContexts(mVO, report);

		mVO.put("ID_RELATORIO", report.get("ID_RELATORIO"));
		if (mVO.get("SUBCONTEXTO") != null)
			report.put("SUBCONTEXTO", mVO.get("SUBCONTEXTO").toString().split(","));

		return true;
	}

	private void transferCampos(Map from, Map to) {
		Iterator i = from.keySet().iterator();
		while (i.hasNext()) {
			String object = (String) i.next();
			if (object.startsWith("CMP_"))
				to.put(object, from.get(object));
		}
	}

	private void keepContexts(GenericVO mVO, Map report) {
		if (mVO.get("SUBCONTEXTO") != null) {
			List contextos = new ArrayList();
			contextos.add(report.get("CONTEXTO"));
			String[] sub = mVO.get("SUBCONTEXTO").toString().split(",");
			for (int i = 0; i < sub.length; i++) {
				if (sub[i] != null && sub[i].trim().length() > 0)
					contextos.add(sub[i]);
			}
			report.put("CONTEXTOS", contextos.toArray());
		}
		mVO.put("CONTEXTOS", report.get("CONTEXTOS"));
	}

	public boolean prepareFiltros(final GenericVO mVO, final Usuario user) {
		Map report = (Map) this.reports.get(user.getSessionId());
		mVO.put("FILTROS", report.get("CONTEXTOS"));
		transferCampos(mVO, report);
		if (report.get("FILTROS") != null) {

			for (Entry<String, String> it : ((Map<String, String>) report.get("FILTROS")).entrySet()) {
				if (it.getValue() != null) {
					String filtro = URLDecoder.decode(String.valueOf(it.getValue()));
					mVO.put("FILTRO_" + it.getKey(), filtro);
				}
			}
		}

		return false;
	}

	public boolean processFiltros(final GenericVO mVO, final Usuario user) throws Exception {
		Map report = (Map) this.reports.get(user.getSessionId());
		if (mVO.get("load_filters_to_report") == null || !mVO.get("load_filters_to_report").equals("false")) {
			report.put("FILTROS", extractFiltros(mVO, user));
		}

		if (report.get("TIPO") != null && mVO.get("TIPO") == null) {
			mVO.put("TIPO", report.get("TIPO"));
		}

		return true;
	}

	private void transferFiltros(Map from, Map to, Usuario user) {
		to.put("FILTROS", extractFiltros((GenericVO) from, user));
	}

	private Object extractFiltros(GenericVO mVO, Usuario user) {
		Map fil = new HashMap<String, String>();
		Iterator e = mVO.keySet().iterator();
		while (e.hasNext()) {
			String type = (String) e.next();
			if (type.toUpperCase().startsWith("FILTRO_")) {
				fil.put(user.getUtil().replace(type, "FILTRO_", ""), mVO.get(type));
			}
		}
		return fil;
	}

	public boolean sheduleReport(final GenericVO mVO, final Usuario user) throws Exception {
		Map report = (Map) this.reports.get(user.getSessionId());

		if (getReportJobByUser(user.getIdUsuario(), user) == null) {
			Trigger trigger = TriggerBuilder.newTrigger().startNow().build();
			// trigger.setEndTime(new Date());
			// trigger.("Trigger de " + "-Relatorios-" + user.getIdUsuario() +
			// new Date());

			// trigger.setGroup("group" + user.getIdUsuario());
			// trigger.setMisfireInstruction(Trigger.INSTRUCTION_DELETE_TRIGGER);

			String jn = " Relatorio " + "-*-" + user.getIdUsuario() + new Date();
			String gn = " GroupRelatorio " + "-*-" + user.getIdUsuario() + new Date();

			final JobDetail task_antiga = newJob(ReportJob.class).withIdentity(jn, gn).build();
			final Map param = task_antiga.getJobDataMap();

			final StdScheduler std = (StdScheduler) getService("ReportsSheduler", user);
			report.put("PROCESS", true);
			((GenericVO) report).putDiff(mVO);

			param.putAll(report);

			param.put("user", user);
			param.put("JOB_NAME", jn);
			param.put("GROUP_NAME", gn);
			param.put("SHEDULER", std);

			if (std.getCurrentlyExecutingJobs().size() == std.getMetaData().getThreadPoolSize()) {
				param.put("SHEDULED", true);
				std.scheduleJob(task_antiga, trigger);
				user.be("M_RELATORIO.aguardandoProcessamentoResgatePosterior", true);
			}
			std.scheduleJob(task_antiga, trigger);
		}

		return true;
	}

	public boolean download(final GenericVO mVO, final Usuario user) throws Exception {

		ReportJob pf = getReportJob(user.getSessionId(), user);
		if (pf != null) {
			reports.remove(user.getSessionId());
			pf.enviar(mVO, user);
		}

		return false;
	}

	public ReportJob getReportJob(String string, Usuario user) {
		final StdScheduler she = (StdScheduler) getService("ReportsSheduler", user);

		ReportJob pf = null;
		for (Iterator iterator = she.getCurrentlyExecutingJobs().iterator(); iterator.hasNext();) {
			JobExecutionContext jec = (JobExecutionContext) iterator.next();
			pf = (ReportJob) jec.getJobInstance();
			if (pf.getTID().equals(string) && pf.canceled() == false) {
				return pf;
			}

		}
		return null;
	}

	public ReportJob getReportJobBySession(String string, Usuario user) {
		final StdScheduler she = (StdScheduler) getService("ReportsSheduler", user);
		ReportJob pf = null;
		for (Iterator iterator = she.getCurrentlyExecutingJobs().iterator(); iterator.hasNext();) {
			JobExecutionContext jec = (JobExecutionContext) iterator.next();
			pf = (ReportJob) jec.getJobInstance();
			if (pf.getTID().startsWith(string) && pf.canceled() == false) {
				return pf;
			}

		}
		return null;
	}



	public ReportJob getReportJobByUser(Long id, Usuario user) {
		final StdScheduler she = (StdScheduler) getService("ReportsSheduler", user);
		ReportJob pf = null;
        if (she == null)
            return null;
		for (Iterator iterator = she.getCurrentlyExecutingJobs().iterator(); iterator.hasNext();) {
			JobExecutionContext jec = (JobExecutionContext) iterator.next();
			pf = (ReportJob) jec.getJobInstance();
			if (pf != null && pf.user != null && pf.user.getIdUsuario().equals(id) && pf.canceled() == false) {
				return pf;
			}

		}
		return null;
	}

	public boolean status(final GenericVO mVO, final Usuario user) throws Exception {

		// System.out.println( user.getLogin()+" >>> " + user.getSessionId() +
		// "-" + mVO.get("ID_RELATORIO"));
		ReportJob pf = getReportJob(user.getSessionId(), user);
		if (pf == null) {
			return false;
		}

		mVO.put("TEXT", "<font color='red'>" + pf.getStatus() + "</font>");
		try {
			mVO.put("CONT", new BigDecimal(pf.getBytesRead())
					.divide(new BigDecimal(pf.getTotalSize()), 1000, BigDecimal.ROUND_FLOOR).doubleValue());

		} catch (final Exception e) {

			mVO.put("CONT", 0);
		}

		if (pf.isSuccess())
			mVO.put("TEXT", "<font color='red'>" + user.getLabel("M_RELATORIO.concluido") + "</font>");

		mVO.put("INPROGRESS", pf.isInProgress());
		mVO.put("SUCCESS", pf.isSuccess());
		mVO.put("FAIL", pf.isFail());

		mVO.setEncapsulate(false);
		return false;
	}

	public boolean cancelByUser(Long id, final Usuario user) throws Exception {

		// ReportJob pf = getReportJob(
		// user.getSessionId() + "-" + mVO.get("ID_RELATORIO"), user);
		ReportJob pf = getReportJobByUser(id, user);
		if (pf != null) {
			pf.Cancel();
		}
		return false;
	}

	public List<Map<String, String>> getFontes(final GenericVO vo, final Usuario user) {
		final List<Map<String, String>> labels = new ArrayList<Map<String, String>>();

		final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		final Font[] fonts = ge.getAllFonts();
		Map<String, String> label;

		for (final Font f : fonts) {
			label = new HashMap<String, String>();
			label.put("ID", f.getFontName());
			label.put("NOME", f.getFontName());
			labels.add(label);
		}
		if (labels.size() == 0) {
			label = new HashMap<String, String>();
			label.put("ID", user.getDefaultSystemFont());
			label.put("NOME", user.getDefaultSystemFont());
			labels.add(label);
		}
		return labels;
	}

	public List<Map<String, String>> getFormatosArquivos(final GenericVO vo, final Usuario user) {

		final List<Map<String, String>> listFormatos = new ArrayList<Map<String, String>>();

		Map<String, String> formato = new HashMap<String, String>();
		formato.put("ID_FORMATOARQUIVO", "HTML");
		formato.put("FORMATOARQUIVO", "HTML");
		listFormatos.add(formato);

		formato = new HashMap<String, String>();
		formato.put("ID_FORMATOARQUIVO", "CSV");
		formato.put("FORMATOARQUIVO", "CSV");
		listFormatos.add(formato);

		formato = new HashMap<String, String>();
		formato.put("ID_FORMATOARQUIVO", "XLS");
		formato.put("FORMATOARQUIVO", "XLS");
		listFormatos.add(formato);

		formato = new HashMap<String, String>();
		formato.put("ID_FORMATOARQUIVO", "PDF");
		formato.put("FORMATOARQUIVO", "PDF");
		listFormatos.add(formato);

		formato = new HashMap<String, String>();
		formato.put("ID_FORMATOARQUIVO", "RTF");
		formato.put("FORMATOARQUIVO", "RTF");
		listFormatos.add(formato);

		return listFormatos;
	}

	public List<Map<String, String>> getFormatosFolhas(final GenericVO vo, final Usuario user) {

		final List<Map<String, String>> list = new ArrayList<Map<String, String>>();

		Map<String, String> formato = new HashMap<String, String>();
		formato.put("ID_FORMATOFOLHA", "Retrato");
		formato.put("FORMATOFOLHA", "Retrato");
		list.add(formato);

		formato = new HashMap<String, String>();
		formato.put("ID_FORMATOFOLHA", "Paisagem");
		formato.put("FORMATOFOLHA", "Paisagem");
		list.add(formato);

		return list;
	}

	public Map getReportByUser(Usuario user) {
		// TODO Auto-generated method stub

		return (Map) this.reports.get(user.getSessionId());
	}

	public boolean getCampos(GenericVO vo, Usuario user) {
		// TODO Auto-generated method stub

		List a = new ArrayList();

		String contexto = (String) vo.get("CONTEXTO");
		Map t = new HashMap();

		a = getCamposContexto(contexto, t, user);
		Map report = (Map) this.reports.get(user.getSessionId());
		if (report.get("CMP_" + contexto) != null) {
			Object[] s = (Object[]) report.get("CMP_" + contexto).toString().split("\\$");
			for (int i = 0; i < s.length; i++) {
				s[i] = user.getUtil().JSONtoMap(s[i].toString()).get("id");
			}
			a = user.getUtil().SortCollection(a, "ID", s);
		} else
			user.getUtil().SortCollection(a, "NAME");
		vo.put(Constants.LIST_DATA, a);
		return false;
	}

	private List getCamposContexto(String contexto, Map t, Usuario user) {
		ServiceImpl ser = (ServiceImpl) getServiceById(contexto, user);
		List a = new ArrayList();
		List f = ser.getAllFields(user);

		t.put(ser.getPrimaryKey().get("name"), "ID");
		for (Iterator iterator = f.iterator(); iterator.hasNext();) {
			Map object = (Map) iterator.next();
			/*
			 * if ("context".equals(object.get("component")) &&
			 * "dependent".equals(object.get("nature"))) { ServiceImpl to =
			 * (ServiceImpl) getService( (String) object.get("service"), user);
			 * if (!to.getParent(user).getProperties().get("type").equals("M"))
			 * a.addAll(getCamposContexto((String) to.getServiceId(), t, user));
			 * }
			 */

			if ((object.get("volatile") != null && !"context".equals(object.get("component")))
					|| (object.get("xtype") != null && object.get("xtype").equals("container"))
					|| t.containsKey(object.get("name")) || "array".equals(object.get("type")) // permitir
																								// depois
					|| "scriptRule".equals(object.get("type")) || "fake".equals(object.get("component"))
					|| "hidden".equals(object.get("component")) || (object.get("service") != null
							&& (object.get("key") == null || object.get("labelValue") == null)))
				continue;

			HashMap tamanho = new HashMap<String, Integer>();
			tamanho.put("ID", object.get("name"));
			t.put(object.get("name"), "ID");
			tamanho.put("NAME",
					user.getLabel((String) (object.get("label") == null ? object.get("name") : object.get("label"))));
			tamanho.put("TYPE", object.get("type"));
			if (user.getUtil().match(a, "NAME", tamanho.get("NAME")).size() == 0)
				a.add(tamanho.clone());
		}
		return a;
	}

	public boolean getSubContextos(final GenericVO vo, final Usuario user) {

		final List<Map<String, Integer>> listTamanhos = new ArrayList<Map<String, Integer>>();
		//
		Map report = (Map) this.reports.get(user.getSessionId());

		String contexto = (String) report.get("CONTEXTO");
		ServiceImpl ser = (ServiceImpl) getService("LtModuloService", user);
		GenericVO mvo = ser.createVO(user);
		mvo.setFiltro("{gA:[{c:{f:'MASTER',o:'=', vc:'" + contexto + "'}}, {c:{f:'TIPO',o:'=', v1:'D'}}]})");
		List a = new ArrayList();
		try {
			a = ser.lista(mvo, user);
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Iterator iterator = a.iterator(); iterator.hasNext();) {
			Map object = (Map) iterator.next();
			mvo.setFiltro("{gA:[{c:{f:'MASTER',o:'=', vc:'" + object.get("CONTEXTO")
					+ "'}}, {c:{f:'TIPO',o:'=', v1:'D'}}]})");
			try {
				listTamanhos.addAll(ser.lista(mvo, user));
			} catch (ConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		listTamanhos.addAll(a);

		vo.put(Constants.LIST_DATA, listTamanhos);
		return false;

	}

	public List<Map<String, String>> getTamanhosFolhas(final GenericVO vo, final Usuario user) {

		final List<Map<String, String>> list = new ArrayList<Map<String, String>>();

		Map<String, String> tamalhos = new HashMap<String, String>();
		tamalhos.put("ID_TAMANHOFOLHA", "A");
		tamalhos.put("TAMANHOFOLHA", "A4 (21,0 x 29,7 cm)");
		list.add(tamalhos);

		tamalhos = new HashMap<String, String>();
		tamalhos.put("ID_TAMANHOFOLHA", "C");
		tamalhos.put("TAMANHOFOLHA", "Carta (21,6 x 27,9 cm)");
		list.add(tamalhos);

		tamalhos = new HashMap<String, String>();
		tamalhos.put("ID_TAMANHOFOLHA", "O");
		tamalhos.put("TAMANHOFOLHA", "Ofcio (21,6 x 35,5 cm)");
		list.add(tamalhos);

		tamalhos = new HashMap<String, String>();
		tamalhos.put("ID_TAMANHOFOLHA", "E");
		tamalhos.put("TAMANHOFOLHA", "Executivo  (18,4 x 26,7 cm)");
		list.add(tamalhos);

		tamalhos = new HashMap<String, String>();
		tamalhos.put("ID_TAMANHOFOLHA", "P");
		tamalhos.put("TAMANHOFOLHA", "Personalizado:");
		list.add(tamalhos);

		return list;
	}

	public List<Map<String, String>> getEstilos(final GenericVO vo, final Usuario user) {

		final List<Map<String, String>> listEstilos = new ArrayList<Map<String, String>>();

		Map<String, String> estilo = new HashMap<String, String>();

		estilo.put("ID_ESTILO", "N");
		estilo.put("ESTILO", "N");
		listEstilos.add(estilo);

		estilo = new HashMap<String, String>();
		estilo.put("ID_ESTILO", "I");
		estilo.put("ESTILO", "I");
		listEstilos.add(estilo);

		estilo = new HashMap<String, String>();
		estilo.put("ID_ESTILO", "S");
		estilo.put("ESTILO", "S");
		listEstilos.add(estilo);

		return listEstilos;
	}

	public boolean cancel(final GenericVO mVO, final Usuario user) throws Exception {

		reports.remove(user.getSessionId());
		ReportJob pf = getReportJobBySession(user.getSessionId(), user);
		if (pf != null) {
			pf.Cancel();
		}
		pf = getReportJobByUser(user.getIdUsuario(), user);

		if (pf != null) {
			pf.Cancel();
		}
		return false;
	}

}