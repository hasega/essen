package com.aseg.util.factory;

import com.aseg.agenda.service.AgendaRoboManager;
import com.aseg.config.Constants;
import com.aseg.dao.DAOImpl;
import com.aseg.dao.GenericDAO;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.util.ConnectionManager;
import com.aseg.util.apresentacao.BaseController;
import com.aseg.util.apresentacao.DefaultController;
import com.aseg.util.apresentacao.SetupController;
import com.aseg.util.service.ParseXml;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.service.SetupService;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheFactoryBean;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.servlet.handler.AbstractDetectingUrlHandlerMapping;
import org.springframework.web.servlet.handler.AbstractUrlHandlerMapping;
import org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping;
import org.springframework.web.servlet.mvc.multiaction.ParameterMethodNameResolver;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

@Configuration
@EnableCaching
public class SpringFactory extends AbstractUrlHandlerMapping implements BeanFactoryAware

{

    @Bean
    public EhCacheFactoryBean xmlCache() {
        EhCacheFactoryBean c = new EhCacheFactoryBean();
        c.setCacheManager(getEhCacheFactory().getObject());
        c.setCacheName("SIS_CACHE");
        return c;
    }

    @Bean
    public EhCacheFactoryBean dataCache() {
        EhCacheFactoryBean c = new EhCacheFactoryBean();
        c.setCacheManager(getEhCacheFactory().getObject());
        c.setCacheName("DATA_CACHE");
        return c;
    }

    @Bean
    public EhCacheManagerFactoryBean getEhCacheFactory() {
        EhCacheManagerFactoryBean factoryBean = new EhCacheManagerFactoryBean();
        factoryBean.setConfigLocation(new ClassPathResource("eh-cache.xml"));
        factoryBean.setShared(true);
        return factoryBean;
    }

    @Bean
    public ParameterMethodNameResolver methodNameResolver() {
        ParameterMethodNameResolver p = new ParameterMethodNameResolver();
        p.setParamName(Constants.ActionParam);
        p.setDefaultMethodName("init");
        return p;
    }

    @Bean(name = "ParseSys")
    @Primary
    public ParseXmlService ParseSys() {
        ParseXmlService x = new ParseXmlService();
        try {
            x.setConfigLocation(new File(System.getProperty("rain.config.file")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        x.setName("ParseSys");
        x.setAdapter("system");

        return x;

    }


    @Bean(name = "ParseFilter")
    public ParseXmlService ParseFilter() {
        ParseXmlService x = new ParseXmlService();
        try {
            x.setConfigLocation(new File(System.getProperty("rain.config.file")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        x.setName("ParseFilter");
        x.setAdapter("filter");

        return x;

    }

    @Bean
    public BeanNameUrlHandlerMapping handlerMapping() {
        return new BeanNameUrlHandlerMapping(){};
    }

    @Bean(name = "Setup")
    public SetupService Setup() {
        SetupService x = new SetupService();
        Map p = new HashMap<String, Object>();

        p.put("name", "Setup");
        p.put("label", "Setup");
        p.put("initial", "lista");


        List ALL_FIELDS = new ArrayList();
        ALL_FIELDS.add(new HashMap<String, Object>() {
            {
                put("name", "ID");
                put("type", "alfa");
                put("primaryKey", "true");
                put("label", "label.id");

            }
        });
        ALL_FIELDS.add(new HashMap<String, Object>() {
            {
                put("name", "NAME");
                put("type", "alfa");
                put("listable", "1;0");

                put("label", "label.name");
                put("mainfield", "true");

            }
        });
        p.put("ALL_FIELDS", ALL_FIELDS);

        List ALL_METHODS = new ArrayList();
        ALL_METHODS.add(new HashMap<String, Object>() {
            {
                put("name", "setupConnection");
                put("type", "U");
                put("layers", "VS");
                put("label", "label.setup.conexao");
            }
        });

        ALL_METHODS.add(new HashMap<String, Object>() {
            {
                put("name", "dataBaseDiff");
                put("type", "L");
                put("layers", "VS");

                put("label", "label.setup.bancodados.diferenca");
                List ALL_FIELDSM1 = new ArrayList();
                ALL_FIELDSM1.add(new HashMap<String, Object>() {
                    {
                        put("name", "TABLE");
                        put("type", "alfa");
                        put("listable", "1;80");
                        put("defaultSearch", "true");
                        put("label", "llabel.setup.bancodados.tabela");

                    }
                });
                ALL_FIELDSM1.add(new HashMap<String, Object>() {
                    {
                        put("name", "ERROR_TYPE");
                        put("type", "alfa");
                        put("listable", "2;150");
                        put("label", "label.setup.bancodados.erro");

                    }
                });
                ALL_FIELDSM1.add(new HashMap<String, Object>() {
                    {
                        put("name", "OBJECT");
                        put("type", "alfa");
                        put("listable", "3;80");
                        put("label", "label.setup.bancodados.objeto");
                    }
                });

                put("ALL_FIELDS", ALL_FIELDSM1);

            }
        });

        ALL_METHODS.add(new HashMap<String, Object>() {
            {
                put("name", "setupConnection");
                put("type", "U");
                put("layers", "VS");
                put("label", "label.setup.conexao");
            }
        });

        ALL_METHODS.add(new HashMap<String, Object>() {
            {
                put("name", "dataBaseAnalys");
                put("type", "L");
                put("layers", "VS");

                put("label", "label.setup.bancodados.analise");
                List ALL_FIELDSM2 = new ArrayList();
                ALL_FIELDSM2.add(new HashMap<String, Object>() {
                    {
                        put("name", "TABLE");
                        put("type", "alfa");
                        put("listable", "1;80");
                        put("defaultSearch", "true");
                        put("label", "label.setup.bancodados.tabela");

                    }
                });
                ALL_FIELDSM2.add(new HashMap<String, Object>() {
                    {
                        put("name", "ERROR_TYPE");
                        put("type", "alfa");
                        put("listable", "2;150");
                        put("label", "label.setup.bancodados.erro");

                    }
                });
                ALL_FIELDSM2.add(new HashMap<String, Object>() {
                    {
                        put("name", "OBJECT");
                        put("type", "alfa");
                        put("listable", "3;80");
                        put("label", "label.setup.bancodados.objeto");
                    }
                });
                ALL_FIELDSM2.add(new HashMap<String, Object>() {
                    {
                        put("name", "CONFIG_VALUE");
                        put("type", "alfa");
                        put("listable", "4;50");
                        put("label", "label.setup.bancodados.configurado");
                    }
                });

                ALL_FIELDSM2.add(new HashMap<String, Object>() {
                    {
                        put("name", "VALUE_FOUND");
                        put("type", "alfa");
                        put("listable", "4;50");
                        put("label", "label.setup.bancodados.encontrado");
                    }
                });

                put("ALL_FIELDS", ALL_FIELDSM2);

            }
        });

        ALL_METHODS.add(new HashMap<String, Object>() {
            {
                put("name", "semanticValidation");
                put("type", "L");
                put("layers", "VS");
                put("label", "label.setup.validation");

                List ALL_FIELDSM3 = new ArrayList();
                ALL_FIELDSM3.add(new HashMap<String, Object>() {
                    {

                        put("name", "BEAN");
                        put("type", "alfa");
                        put("listable", "1;50");
                        put("label", "label.setup.validacao.bean");

                    }
                });
                ALL_FIELDSM3.add(new HashMap<String, Object>() {
                    {
                        put("name", "ERROR_TYPE");
                        put("type", "alfa");
                        put("listable", "2;80");
                        put("label", "label.setup.validacao.erro");

                    }

                });
                ALL_FIELDSM3.add(new HashMap<String, Object>() {
                    {
                        put("name", "PROPERTY");
                        put("type", "alfa");
                        put("listable", "3;80");
                        put("label", "label.setup.validacao.propriedade");
                    }

                });

                put("ALL_FIELDS", ALL_FIELDSM3);

            }
        });

        p.put("ALL_METHODS", ALL_METHODS);

        x.setProperties(p);
        return x;

    }

    @Bean(name = "/SetupAction.do")
    public SetupController SetupController() {
        SetupController x = new SetupController();
        x.setService((GenericService) beanFactory.getBean("Setup"));
        return x;

    }

    // private WebApplicationContext ctx = null;
    private static ConfigurableListableBeanFactory beanFactory = null;
    private static ApplicationContext ctx = null;
    public boolean state = false;
    public final UtilServiceImpl util = new UtilServiceImpl();

    private String serverkey = null;

    private final Map<String, List<Map<String, Object>>> systemProperties = new HashMap<String, List<Map<String, Object>>>() {
        public List<Map<String, Object>> put(String arg0, Object arg1) {
            return super.put(arg0, (List<Map<String, Object>>) arg1);
        }

        ;
    };
    private final Map<String, String> systems = new HashMap<String, String>() {
        public String put(String arg0, String arg1) {
            return super.put(arg0, arg1);
        }

        ;
    };
    private static boolean initialized = false;

    // public void setContext(WebApplicationContext fis,
    // Usuario user) {
    //
    // try {
    // ctx = fis;
    //
    // } catch (Exception ex) {
    // ex.printStackTrace();
    // }
    //
    // }

    public static boolean isInitialized() {
        return initialized;
    }

    public static void setInitialized(boolean initialized) {
        SpringFactory.initialized = initialized;
    }



    public Object getSpringBean(final String beanId, final Usuario user) {
        systems.put(user.getSchema(),user.getSystem());

        Object a = null;
        if (beanFactory == null) {
            if (ctx == null) {
                return null;
            }
            return ctx.getBean(beanId);
        } else {
            if (beanFactory.containsBean(user.getSystem() + "/" + beanId)) {
                a = beanFactory.getBean(user.getSystem() + "/" + beanId);

            } else {
                // resgatando generic beans
                if (beanFactory.containsBean(beanId)) {
                    try {
                        return beanFactory.getBean(beanId);
                    } catch (final Exception e) {
                        return null;
                    }
                } else {
                    if(beanFactory.containsSingleton(beanId))
                        return beanFactory.getSingleton(beanId);
                    if (!beanFactory.containsSingleton(user.getSystem() + "/" + beanId)
                            && loadBean(beanId, false, user)) {
                        return getSpringBean(beanId, user);
                    } else {
                        if (beanId.indexOf(Constants.SERVICE_BASE) > -1) {
                            try {
                                return beanFactory.getBean(beanId.replaceAll(Constants.SERVICE_BASE, ""));
                            } catch (Exception e) {
                                try {
                                    if (beanFactory.containsBean(user.getSystem() + "/" + beanId)) {
                                        return beanFactory.getBean(user.getSystem() + "/" + beanId);
                                    }
                                } catch (Exception e2) {
                                    LogUtil.exception(e2, user);
                                }
                            }

                        }
                    }
                }
            }

        }

        return a;

    }

    public List getSystemProperty(final String system) {
        return systemProperties.get(system);
    }

    public Map<String, String> getSystems() {
        return systems;
    }

    public Map getSystemProperties() {
        return systemProperties;
    }

    @Bean
    public SpringFactory beanFactory() {
        Usuario.setFactory(this);
        final Usuario user = new Usuario();
        final ParseXmlService s = (ParseXmlService) getSpringBean("ParseSys", user);
        if (s == null) {
            return null;
        } else {
            this.initialized = true;

            org.springframework.scheduling.quartz.SchedulerFactoryBean t = new org.springframework.scheduling.quartz.SchedulerFactoryBean();

            // beanFactory.registerSingleton("TarefasSheduler", t);
            // org.springframework.scheduling.quartz.SchedulerFactoryBean t1 =
            // new org.springframework.scheduling.quartz.SchedulerFactoryBean();
            //
            // beanFactory.registerSingleton("EmailSheduler", t1);
            // org.springframework.scheduling.quartz.SchedulerFactoryBean t2 =
            // new org.springframework.scheduling.quartz.SchedulerFactoryBean();
            //
            // beanFactory.registerSingleton("ReportsSheduler", t2);
            // org.springframework.scheduling.quartz.SchedulerFactoryBean t3 =
            // new org.springframework.scheduling.quartz.SchedulerFactoryBean();
            //
            // beanFactory.registerSingleton("WorkFlowSheduler", t3);
            //
            // beanFactory.freezeConfiguration();

        }
        final List sis = (List) s.getListValuesFromXQL("/systems/system", null, user, false, false);
        for (final Iterator iterator = sis.iterator(); iterator.hasNext(); ) {
            final Map object = (Map) iterator.next();
            user.setSystem((String) object.get(Constants.name));
            user.setConnectionRoute((String) object.get(Constants.name));

            final Map clas = (s).getMapValuesFromXQL(
                    "/systems/system[@name='" + user.getSchema()
                            + "']/module/bean", // [@main // [@main or @initial or @cached or @loadOnStartup]
                    // or
                    // @initial
                    // or
                    // @cached
                    // or
                    // @loadOnStartup]
                    Constants.name, user, true, false);
            final Iterator i = clas.keySet().iterator();
            // boolean iss = user.isSetup();
            // user.setSetup(true);

            while (i.hasNext()) {
                final String object2 = (String) i.next();
                Map b = (Map<String, String>) clas.get(object2);
                loadBean(b, false, user);

            }
            // user.setSetup(iss);

            populateModules(user);

            try {
                final List a = ConnectionManager.getInstance(user).loadSASConfiguration(user);
                for (final Iterator ix = a.iterator(); ix.hasNext(); ) {
                    final Map o = (Map) ix.next();

                    systems.put(o.get("VALOR_CONFIG_SISTEMA").toString(), user.getSchema());
                    systemProperties.put(o.get("VALOR_CONFIG_SISTEMA").toString(),
                            (List<Map<String, Object>>) o.get("CONFIG_ITEMS"));

                    if (user.getSystem().equals(o.get("VALOR_CONFIG_SISTEMA").toString())) {

                        try {

                            GenericService userService = (GenericService) getSpringBean("UsuarioService", user);
                            GenericVO userVO = userService.createVO(user);
                            userVO.putKey("SUPER_USER", "T");
                            userVO = (GenericVO) userService.obtem(userVO, user);
                            Usuario usuario = userService.bindUser(user, userVO, false);

							/*
                             * TODO: Permitir o agendamento de vrias instncias,
							 * por enquanto s vale para a instncia onde o
							 * Sistema igual ao Schema Data: 09/08/2011 By:
							 * Vinicius Ramos
							 */
                            if (user.getSystem().equalsIgnoreCase(user.getSchema())) {
                                user.setConnectionRoute(user.getSystem());
                                AgendaRoboManager agendaRobo = new AgendaRoboManager(usuario);
                            }

                        } catch (final Exception ex) {
                            LogUtil.notify("Sistema Agendador Robo [ERRO] ");
                            LogUtil.exception(ex, user);

                        }

                        continue;
                    }
                    user.setSystem(o.get("VALOR_CONFIG_SISTEMA").toString());
                    user.setConnectionRoute(user.getSystem());
                    populateModules(user);
                    loadBean(o.get("VALOR_CONFIG_SISTEMA").toString(), false, user);

                    final Map claxs = s.getMapValuesFromXQL(
                            "/systems/system[@name='" + user.getSchema() + "']/module/bean", // [@main
                            // or
                            // @initial
                            // or
                            // @cached
                            // or
                            // @loadOnStartup]
                            Constants.name, user, true, false);
                    final Iterator ix2 = claxs.keySet().iterator();
                    // iss = user.isSetup();
                    // user.setSetup(true);

                    while (ix2.hasNext()) {
                        final String object2 = (String) ix2.next();

                        loadBean((Map<String, String>) clas.get(object2), false, user);

                    }
                    // user.setSetup(iss);
                    user.setSystem(user.getSchema());

                }
            } catch (final Exception e) {
                LogUtil.exception(e, user);
            }

        }
        final AbstractDetectingUrlHandlerMapping as = (AbstractDetectingUrlHandlerMapping) getSpringBean(
                "handlerMapping", user);
        as.initApplicationContext();
        return this;
    }



    private synchronized boolean loadBean(final Map<String, String> type, boolean refresh, final Usuario user) {
        state = true;
        boolean r = true;

        DAOImpl DAOtarget;
        final String sys = user.getSystem();
        boolean drefresh = refresh;

        if (beanFactory.containsSingleton(sys + "/" + type.get(Constants.name) + Constants.DAO_SUFIX)) {

            DAOtarget = (DAOImpl) beanFactory.getSingleton(sys + "/" + type.get(Constants.name) + Constants.DAO_SUFIX);
            if (refresh) {
                r = false;
                if (DAOtarget.getCheck_code() != Integer.valueOf(type.get("hashCode").toString())) {
                    LogUtil.debug("Refresh no DAO para " + type.get(Constants.name), user);
                    DAOtarget.setEntity(type);
                    DAOtarget.setCheck_code(Integer.valueOf(type.get("hashCode").toString()));
                    DAOtarget.setProperties(null);
                }
            }
        } else {

            LogUtil.debug("Criando DAO generico para " + sys + " > " + type.get(Constants.name), user);
            DAOtarget = new DAOImpl();
            DAOtarget.setEntity(type);
            DAOtarget.setCheck_code(Integer.valueOf(type.get("hashCode").toString()));
            DAOtarget.setProperties(null);
            beanFactory.registerSingleton(sys + "/" + type.get(Constants.name) + Constants.DAO_SUFIX, DAOtarget);
            drefresh = true;
        }

        if (type.get("master") != null && type.get("table") != null) {
            final String ParentName = sys + "/"
                    + (user.getSysParser()).getValueFromXQL(
                    "/systems/system[@name='" + sys + "']/module/bean[@id='" + type.get("master") + "']",
                    Constants.name, user, true, false)
                    + Constants.DAO_SUFIX;

            if (beanFactory.containsBean(ParentName)) {
                DAOtarget.setParent((DAOImpl) beanFactory.getBean(ParentName));
            }

        }

        try {
            if (drefresh)
                DAOtarget.afterPropertiesSet(user);
        } catch (final Exception e2) {
            LogUtil.exception(e2, user);
        }

        // if (!type.get(Constants.name).toString().equalsIgnoreCase("System"))
        // {
        // DAOtarget.processMetadata(user);
        // }

        ServiceImpl ServiceTarget;
        if (beanFactory.containsSingleton(sys + "/" + type.get(Constants.name) + "Service")) {
            ServiceTarget = (ServiceImpl) beanFactory.getSingleton(sys + "/" + type.get(Constants.name) + "Service");
            if (refresh) {
                r = false;
                if (ServiceTarget.getCheck_code() != Integer.valueOf(type.get("hashCode").toString())) {
                    LogUtil.debug("Refresh no Service para " + type.get(Constants.name), user);
                    ServiceTarget.setProperties(type, user);
                    ServiceTarget.setCheck_code(Integer.valueOf(type.get("hashCode").toString()));
                }
            }
        } else {
            LogUtil.debug("Criando Service  generico para " + type.get(Constants.name), user);
            ServiceTarget = new ServiceImpl();
            ServiceTarget.setProperties(type, user);
            ServiceTarget.setCheck_code(Integer.valueOf(type.get("hashCode").toString()));
            if (type.get("forceWorkflowListener") == null && user.getLogin() != null && user.isProcessListeners()) {
                LogUtil.debug(ServiceTarget.getName(), user);
                ServiceTarget.registerListeners(user);
            }
            beanFactory.registerSingleton(sys + "/" + type.get(Constants.name) + "Service", ServiceTarget);

        }

        // LogUtil.debug(Arrays.asList(ReflectionUtils
        // .getAllDeclaredMethods(ServiceTarget.getClass())));
        final Method m = ReflectionUtils.findMethod(ServiceTarget.getClass(), "setDAO",
                new Class[]{GenericDAO.class});
        try {
            m.invoke(ServiceTarget, new Object[]{DAOtarget});
        } catch (final IllegalArgumentException e1) {
            LogUtil.exception(e1, user);
        } catch (final IllegalAccessException e1) {
            LogUtil.exception(e1, user);
        } catch (final InvocationTargetException e1) {
            LogUtil.exception(e1, user);
        }

        BaseController target;

        final String ac = sys + "/" + type.get(Constants.name) + Constants.ACTION_BASE + Constants.ACTION_SUFIX;
        if (beanFactory.containsSingleton("/" + ac)) {
            target = (BaseController) beanFactory.getSingleton("/" + ac);
            r = false;
        } else {
            LogUtil.debug("Criando Action  generico para " + type.get(Constants.name), user);
            target = new DefaultController();
        }
        final Method ms = ReflectionUtils.findMethod(target.getClass(), "setService",
                new Class[]{GenericService.class});
        try {
            ms.invoke(target, new Object[]{ServiceTarget});
        } catch (final IllegalArgumentException e1) {
            LogUtil.exception(e1, user);
        } catch (final IllegalAccessException e1) {
            LogUtil.exception(e1, user);
        } catch (final InvocationTargetException e1) {
            LogUtil.exception(e1, user);
        }

        // target.setActionId(ac);
        if (!beanFactory.containsSingleton("/" + ac)) {
            beanFactory.registerSingleton("/" + ac, target);
           // ((BeanNameUrlHandlerMapping) beanFactory.getBean("handlerMapping")).getHandlerMap().put("/" + ac, target);
            if (type.get("filterListener") != null) {
                String[] filters = type.get("filterListener").split(",");
                for (int i = 0; i < filters.length; i++) {
                    beanFactory.registerSingleton("/" + util.replace(ac, Constants.ACTION_SUFIX, "." + filters[i]),
                            target);
                 //   ((BeanNameUrlHandlerMapping) beanFactory.getBean("handlerMapping")).getHandlerMap().put(("/" + util.replace(ac, Constants.ACTION_SUFIX, "." + filters[i])),
                 //           target);
                }
            }

        }
        state = false;
        return r;
    }

	/*
	 * 
	 * try { List clientes = ConnectionManager.getInstance(
	 * user).afterPropertiesSet( user);
	 * 
	 * if (clientes != null) { for (Iterator iterator = clientes.iterator();
	 * iterator .hasNext();) { Map object = (Map) iterator.next();
	 * BaseController target = new DefaultController();
	 * target.setActionId(object.get("VALOR_CONFIG_SISTEMA") .toString() + "." +
	 * Constants.ASP_SUFIX); if (!beanFactory.containsSingleton("/" +
	 * object.get("VALOR_CONFIG_SISTEMA").toString() + Constants.ASP_SUFIX)) {
	 * beanFactory.registerSingleton( "/" + object.get("VALOR_CONFIG_SISTEMA")
	 * .toString() + Constants.ASP_SUFIX, target); } target.setDinamic(true);
	 * 
	 * } if (clientes.size() > 0) { BaseController target = new
	 * DefaultController();
	 * target.setActionId(ConnectionManager.DEFAULT_DATASOURCE + "." +
	 * Constants.ASP_SUFIX); if (!beanFactory.containsSingleton("/" +
	 * ConnectionManager.DEFAULT_DATASOURCE + Constants.ACTION_SUFIX)) {
	 * beanFactory.registerSingleton("/" + ConnectionManager.DEFAULT_DATASOURCE
	 * + Constants.ACTION_SUFIX, target); } target.setDinamic(true);
	 * 
	 * } } } catch (Exception e) {
	 * 
	 * // e.printStackTrace(); }
	 */

    public boolean loadBean(String BeanName, boolean refresh, final Usuario user) {
        if (user == null)
            return false;
        BeanName = util.replace(BeanName,
                new String[]{Constants.ACTION_SUFIX, "Action", "Service", Constants.DAO_SUFIX, user.getSystem(), "/"},
                new String[]{"", "", "", "", "", ""});
        final Map clas = user.getSysParser().getMapValuesFromXQL("/systems/system[@name='" + user.getSchema()
                + "']/module/bean[@type!='L' and @name='" + BeanName + "']", Constants.name, user, true, false);
        final Map<String, String> cls = (Map<String, String>) clas.get(BeanName);
        if (cls != null) {
            boolean r = loadBean(cls, refresh, user);
            return r;
        } else {
            return false;
        }
    }

    private void populateModules(final Usuario user) {
        ParseXml ps = user.getSysParser();
        final List<Map<String, String>> configuredBeans = (List<Map<String, String>>) ps.getListValuesFromXQL(
                "/systems/system[@name='" + user.getSchema() + "']/module/bean[@type!='F']", Constants.name, user, true,
                true);

        final GenericService beanService = (GenericService) getSpringBean("LtModuloService", user);
        final GenericVO beanVO = beanService.createVO(user);
        beanVO.setProcess(false);
        beanVO.setPaginate(false);
        try {
            List<GenericVO> persistedBeans = beanService.lista(beanVO, user);
            // deletando beans persistidos que no esto mais configurados no
            // config.
            for (final GenericVO pBean : persistedBeans) {
                List configured = util.match(configuredBeans, "id", pBean.get("CONTEXTO"));
                if (configured.size() == 0) {
                    beanService.remove(pBean, user);
                }
            }

            for (final Map<String, String> bean : configuredBeans) {

                beanVO.clear();
                beanVO.putKey("CONTEXTO", bean.get("id"));
                String ma = "";

                if (bean.get("master") != null && !bean.get("type").equals("M")) {

                    Map master = ps.getMapValuesFromXQL("/systems/system[@name='" + user.getSchema()
                            + "']/module/bean[@id='" + bean.get("master") + "']", "id", user, false, true)
                            .get(bean.get("master"));

                    if (bean.get("type").equals("A")) {
                        ma = " (Tab. Aux.) ";
                    }
                    if (master != null) {
                        ma = ma + " (" + user.getLabel((String) master.get("label")) + ")";
                    }
                    beanVO.put("MASTER", bean.get("master"));

                }

                beanVO.put("MODULO", user.getLabel(bean.get("label")) + ma);
                beanVO.put("TIPO", bean.get("type"));
                beanService.merge(beanVO, user);

            }
        } catch (Exception e) {
            LogUtil.exception(e, user);
        }
    }

    public void reloadBeanFactory(final Usuario user) throws Exception {

        final String[] b = beanFactory.getSingletonNames();
        for (final String element : b) {
            loadBean(element, true, user);
        }
        // if(type.equals(clas.get(clas.size()-1)))
        // DAOtarget.freeMetadata(user);
    }

    public void reloadFactory() {

        Usuario user = new Usuario();
        final ParseXmlService s = (ParseXmlService) user.getSysParser();
        // ConnectionManager.getInstance(user).loadConnection(user);
        try {
            s.afterPropertiesSet();
        } catch (final Exception e1) {
            LogUtil.exception(e1, user);
        }
        final Iterator i = systems.keySet().iterator();
        while (i.hasNext()) {
            final String object = (String) i.next();
            if(object==null)
                continue;

            try {
                user = new Usuario();
                user.setSystem(object);

                reloadBeanFactory(user);

                systems.put(object, object);
            } catch (final Exception e) {
                LogUtil.exception(e, user);
            }

        }

    }

    public void setBeanFactory(final ConfigurableListableBeanFactory fis, final Usuario user) {

        try {
            beanFactory = fis;

        } catch (final Exception ex) {
            LogUtil.exception(ex, user);
        }

    }

    public static void setContext(final ApplicationContext requiredWebApplicationContext) {

        ctx = requiredWebApplicationContext;

    }

    public boolean systemIsLoaded(final Usuario user) {
        return systems.containsKey(user.getSystem());
    }

    public String[] getBeansNamesByClass(String cls) {
        try {
            // ctx.getBeanNamesForType(Class.forName(cls)),
            return (String[]) ArrayUtils.addAll(new String[]{}, beanFactory.getBeanNamesForType(Class.forName(cls)));
        } catch (ClassNotFoundException e) {
            LogUtil.exception(e, null);
        }
        return new String[]{};
    }

    public Map getBeansByClass(String cls) {
        try {
            // ctx.getBeanNamesForType(Class.forName(cls)),
            return beanFactory.getBeansOfType(Class.forName(cls));
        } catch (ClassNotFoundException e) {
            LogUtil.exception(e, null);
        }
        return new HashMap();
    }

    public String getServerKey(Usuario user) {
        // TODO Auto-generated method stub
        if (serverkey == null) {
            final ParseXmlService s = (ParseXmlService) user.getSysParser();
            try {
                serverkey = InetAddress.getLocalHost().getHostName() + ":"
                        + s.getValueFromXQL("/systems", "serverPort", user, false, false);
            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                LogUtil.exception(e, user);
            }
        }
        return serverkey;
    }

    public void registerSingleton(String string, Object sc) {
        // TODO Auto-generated method stub

        beanFactory.registerSingleton(string, sc);
    }

    public void refreshSystem(Usuario user, String System) throws Exception {
        synchronized (getSystemProperty(System)) {
            LogUtil.debug("REFRESH SYSTEM " + System, user);
            final ServiceImpl s = (ServiceImpl) getSpringBean("ASPConfigService", user);
            final GenericVO vo = s.createVO(user);
            vo.putKey("VALOR_CONFIG_SISTEMA", System);
            Map cliente = null;

            cliente = s.obtem(vo, user);
            final ServiceImpl si = (ServiceImpl) user.getFactory().getSpringBean("ASPConfigItemService", user);
            final GenericVO vi = si.createVO(user);
            vi.setPaginate(false);
            vi.put("ID_PAI", cliente.get("ID_CONFIG_SISTEMA"));
            vi.put("ID_CONFIG_SISTEMA", cliente.get("ID_CONFIG_SISTEMA"));
            getSystemProperty(System).clear();
            getSystemProperty(System).addAll(si.lista(vi, user));
            LogUtil.debug("REFRESH SYSTEM NEW ITEMS " + getSystemProperty(System), user);

        }
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        // TODO Auto-generated method stub
        this.beanFactory = (ConfigurableListableBeanFactory) beanFactory;
        Usuario.setFactory(this);


    }

    public BeanFactory getBeanFactory() {
        // TODO Auto-generated method stub
        return this.beanFactory;
    }
}
