package com.aseg.util.wsdl;

import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.aseg.config.Constants;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.service.util.XMLUtil;
import com.aseg.util.service.UtilServiceImpl;
import com.jamesmurty.utils.XMLBuilder;

public class Bean2WSDL extends ServiceImpl {

	private static final String TARGET_NAMESPACE = "http://webservices.essen.aseg.com.br";
	private static final String XSD_TYPE_HEXBINARY = "xsd:hexBinary";
	private static final String XSD_TYPE_BOOLEAN = "xsd:boolean";
	private static final String XSD_TYPE_TIME = "xsd:time";
	private static final String XSD_TYPE_DATE = "xsd:date";
	private static final String XSD_TYPE_DECIMAL = "xsd:decimal";
	private static final String XSD_TYPE_INTEGER = "xsd:integer";
	private static final String XSD_TYPE_STRING = "xsd:string";

	private static final String XSD_INDICATOR_SEQUENCE = "xsd:sequence";
	private static final String XSD_INDICATOR_ALL = "xsd:all";

	private static final String WSDL_ELEMENT_START_SESSION = "IniciarSessao";
	private static final String WSDL_ELEMENT_END_SESSION = "EncerrarSessao";
	private static final String WSDL_ELEMENT_INCLUDE = "Incluir";
	private static final String WSDL_ELEMENT_UPDATE = "Alterar";
	private static final String WSDL_ELEMENT_GET = "Obtem";
	private static final String WSDL_ELEMENT_REMOVE = "Remover";
	private static final String WSDL_ELEMENT_LIST = "Lista";

	private static final String WSDL_OUTPUT = "output";
	private static final String WSDL_INPUT = "input";
	private static final String WSDL_REFERENCE = "impl:";
	private static final String WSDL_SESSION = "session";
	private static final String WSDL_RESPONSE = "Response";
	private static final String WSDL_REQUEST = "Request";
	private static final String WSDL_PARAMETER = "Parameter";
	private static final String WSDL_RESULT = "result";
	private static final String WSDL_ID = "id";

	public void create(Writer writer, String beanName, GenericService service, String location, String encoding,
			Usuario user) throws ParserConfigurationException, FactoryConfigurationError, TransformerException {

		Map<String, Object> properties = service.getProperties();
		List<Map<String, Object>> methods = service.getMethods(user);

		XMLBuilder xmlBuilder = XMLBuilder.create("wsdl:definitions").a("targetNamespace", TARGET_NAMESPACE)
				.a("xmlns:wsdl", "http://schemas.xmlsoap.org/wsdl/").a("xmlns:xsd", "http://www.w3.org/2001/XMLSchema")
				.a("xmlns:soap", "http://schemas.xmlsoap.org/wsdl/soap/").a("xmlns:impl", TARGET_NAMESPACE);

		beanName = XMLUtil.toTagName(beanName);

		createElements(xmlBuilder, beanName, properties, methods);
		createMessages(xmlBuilder, beanName, methods);
		createPortyType(xmlBuilder, beanName, methods);
		createBinding(xmlBuilder, beanName, methods);
		createService(xmlBuilder, beanName, location);

		Properties outputProperties = new Properties();
		outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
		outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");
		outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");
		outputProperties.put(javax.xml.transform.OutputKeys.ENCODING, encoding);

		xmlBuilder.toWriter(writer, outputProperties);
	}

	private static String getType(String propertyType) {

		if (propertyType.equals("alfa")) {
			return XSD_TYPE_STRING;
		} else if (propertyType.equals("num")) {
			return XSD_TYPE_INTEGER;
		} else if (propertyType.equals("array")) {
			return XSD_TYPE_INTEGER;
		} else if (propertyType.equals("money")) {
			return XSD_TYPE_DECIMAL;
		} else if (propertyType.equals("date")) {
			return XSD_TYPE_DATE;
		} else if (propertyType.equals("time")) {
			return XSD_TYPE_TIME;
		} else if (propertyType.equals("bit")) {
			return XSD_TYPE_BOOLEAN;
		} else if (propertyType.equals("file")) {
			return XSD_TYPE_HEXBINARY;
		}
		return null;
	}

	private static boolean isRequired(Map<String, Object> property) {

		if (property.get("required") != null && property.get("required").equals(Constants.true_str)) {
			return true;
		}
		return false;
	}

	private void createElements(XMLBuilder xmlBuilder, String beanName, Map<String, Object> properties,
			List<Map<String, Object>> methods) {

		XMLBuilder schema = xmlBuilder.e("wsdl:types").e("xsd:schema").a("targetNamespace", TARGET_NAMESPACE);

		List<Map<String, Object>> fields = (List<Map<String, Object>>) properties.get(Constants.ALL_FIELDS);

		// Tipos genricos
		createBean(schema, beanName, fields);
		createMethodElements(schema, beanName, methods);
		createElement(schema, WSDL_SESSION, XSD_TYPE_STRING);

		// Sesso
		XMLBuilder complexType = createElementTypeRequest(schema, WSDL_ELEMENT_START_SESSION);
		createElement(complexType, "usuario", XSD_TYPE_STRING);
		createElement(complexType, "senha", XSD_TYPE_STRING);
		createElement(createElementTypeRequest(schema, WSDL_ELEMENT_END_SESSION), WSDL_SESSION, XSD_TYPE_STRING);
		createElement(createElementTypeResponse(schema, WSDL_ELEMENT_END_SESSION), "return", XSD_TYPE_BOOLEAN);

		// CRUD
		createBeanElement(createElementTypeRequest(schema, WSDL_ELEMENT_INCLUDE), beanName);
		createElement(createElementTypeResponse(schema, WSDL_ELEMENT_INCLUDE), WSDL_ID, XSD_TYPE_INTEGER);

		createBeanElement(createElementTypeRequest(schema, WSDL_ELEMENT_UPDATE), beanName);
		createElement(createElementTypeResponse(schema, WSDL_ELEMENT_UPDATE), WSDL_ID, XSD_TYPE_INTEGER);

		createBeanElement(createElementTypeRequest(schema, WSDL_ELEMENT_GET), beanName);
		createBeanElement(createElementTypeResponse(schema, WSDL_ELEMENT_GET), beanName);

		createBeanElement(createElementTypeRequest(schema, WSDL_ELEMENT_REMOVE), beanName);
		createBeanElement(createElementTypeResponse(schema, WSDL_ELEMENT_REMOVE), beanName);

		createBeanElement(createElementTypeRequest(schema, WSDL_ELEMENT_LIST), beanName);
		createBeanElementArray(createElementTypeResponse(schema, WSDL_ELEMENT_LIST), beanName);

	}

	private void createBean(XMLBuilder schema, String beanName, List<Map<String, Object>> properties) {

		XMLBuilder complexType = createComplexType(schema, beanName, XSD_INDICATOR_ALL);

		createBean(complexType, properties);
	}

	private void createBean(XMLBuilder complexType, List<Map<String, Object>> properties) {
		UtilServiceImpl util = new UtilServiceImpl();
		properties = util.match(properties, "xtype", "property");

		for (Map<String, Object> property : properties) {

			if (!property.get("type").equals("scriptRule")) {
				createElement(complexType, XMLUtil.toTagName(property.get(Constants.name).toString()),
						getType(property.get("type").toString()), !isRequired(property));
			}
		}
		// Elementos padro de cadastro
		createElement(complexType, "id-usuario-cadastro", XSD_TYPE_INTEGER, true);
		createElement(complexType, "data-cadastro", XSD_TYPE_DATE, true);
		createElement(complexType, "ultima-modificacao", XSD_TYPE_DATE, true);

		// Cria tags de busca
		createElement(complexType, "by", XSD_TYPE_STRING, true);
		createElement(complexType, "key", XSD_TYPE_STRING, true);

		// Cria taga de paginao
		createElement(complexType, "filter", XSD_TYPE_STRING, true);
		createElement(complexType, "total", XSD_TYPE_STRING, true);
		createElement(complexType, "where", XSD_TYPE_STRING, true);
		createElement(complexType, "alias", XSD_TYPE_STRING, true);
		createElement(complexType, "paginate", XSD_TYPE_BOOLEAN, true);
		createElement(complexType, "start", XSD_TYPE_STRING, true);
		createElement(complexType, "dir", XSD_TYPE_STRING, true);
		createElement(complexType, "limit", XSD_TYPE_STRING, true);
		createElement(complexType, "sort", XSD_TYPE_STRING, true);
		complexType = createElement(complexType, "multipleid", true);
		complexType = createComplexType(complexType);
		createElementArray(complexType, WSDL_ID, XSD_TYPE_INTEGER, 1);
	}

	private void createMethodElements(XMLBuilder schema, String beanName, List<Map<String, Object>> methods) {

		XMLBuilder element;
		List<Map<String, Object>> fields;
		String methodName;

		for (Map<String, Object> method : methods) {
			fields = (List<Map<String, Object>>) method.get(Constants.ALL_FIELDS);

			methodName = method.get(Constants.name).toString();

			createBean(schema, methodName, fields);

			createBeanElement(createElementTypeRequest(schema, methodName), beanName);

			element = createElementTypeResponse(schema, methodName);

			if (method.get("type").equals("L")) {
				createBeanElementArray(element, methodName);

			} else {
				createBeanElement(element, methodName);
			}
		}
	}

	private static XMLBuilder createElementTypeRequest(XMLBuilder xmlBuilder, String name) {
		return createElementType(xmlBuilder, name + WSDL_REQUEST);
	}

	private static XMLBuilder createElementTypeResponse(XMLBuilder xmlBuilder, String name) {
		return createElementType(xmlBuilder, name + WSDL_RESPONSE);
	}

	private static XMLBuilder createElementType(XMLBuilder xmlBuilder, String name) {
		return createComplexType(xmlBuilder.e("xsd:element").a(Constants.name, name));
	}

	private static XMLBuilder createComplexType(XMLBuilder xmlBuilder) {
		return createComplexType(xmlBuilder, XSD_INDICATOR_SEQUENCE);
	}

	private static XMLBuilder createComplexType(XMLBuilder xmlBuilder, String orderIndicator) {
		return xmlBuilder.e("xsd:complexType").e(orderIndicator);
	}

	private static XMLBuilder createComplexType(XMLBuilder xmlBuilder, String name, String orderIndicator) {
		return xmlBuilder.e("xsd:complexType").a(Constants.name, name).e(orderIndicator);
	}

	private static XMLBuilder createBeanElement(XMLBuilder complexType, String beanName) {
		return createElement(complexType, beanName, WSDL_REFERENCE + beanName);
	}

	private static XMLBuilder createBeanElementArray(XMLBuilder complexType, String beanName) {

		return createBeanElement(complexType, beanName).a("maxOccurs", "unbounded").a("minOccurs", "0");
	}

	private static XMLBuilder createElement(XMLBuilder complexType, String elementName, boolean optional) {

		XMLBuilder element = complexType.e("xsd:element").a(Constants.name, elementName);

		if (optional) {
			element.a("minOccurs", "0");
		}
		return element;
	}

	private static XMLBuilder createElement(XMLBuilder complexType, String elementName, String elementType) {

		return createElement(complexType, elementName, false).a("type", elementType);
	}

	private static void createElement(XMLBuilder complexType, String elementName, String elementType,
			boolean optional) {

		createElement(complexType, elementName, optional).a("type", elementType);
	}

	private static void createElementArray(XMLBuilder complexType, String elementName, String elementType,
			int minOccurs) {

		createElement(complexType, elementName, elementType).a("minOccurs", Integer.toString(minOccurs)).a("maxOccurs",
				"unbounded");
	}

	private static void createMessages(XMLBuilder root, String beanName, List<Map<String, Object>> methods) {

		// Cria mensagens de sessao
		createMessagePartElement(createMessage(root, WSDL_ELEMENT_START_SESSION + WSDL_REQUEST),
				WSDL_ELEMENT_START_SESSION + WSDL_REQUEST, WSDL_PARAMETER);
		createMessagePartElement(createMessage(root, WSDL_ELEMENT_START_SESSION + WSDL_RESPONSE), WSDL_SESSION,
				WSDL_RESULT);

		createMessagePartElement(createMessage(root, WSDL_ELEMENT_END_SESSION + WSDL_REQUEST),
				WSDL_ELEMENT_END_SESSION + WSDL_REQUEST, WSDL_PARAMETER);
		createMessagePartElement(createMessage(root, WSDL_ELEMENT_END_SESSION + WSDL_RESPONSE),
				WSDL_ELEMENT_END_SESSION + WSDL_RESPONSE, WSDL_RESULT);

		// Cria mensagen para CRUD
		createMessagePartElements(root, WSDL_ELEMENT_INCLUDE);
		createMessagePartElements(root, WSDL_ELEMENT_UPDATE);
		createMessagePartElements(root, WSDL_ELEMENT_GET);
		createMessagePartElements(root, WSDL_ELEMENT_REMOVE);
		createMessagePartElements(root, WSDL_ELEMENT_LIST);

		createMethodMessages(root, methods);
	}

	private static void createMethodMessages(XMLBuilder root, List<Map<String, Object>> methods) {

		String methodName;
		XMLBuilder message;

		for (Map<String, Object> method : methods) {
			methodName = method.get(Constants.name).toString();

			message = createMessage(root, methodName + WSDL_REQUEST);

			createMessagePartElement(message, WSDL_SESSION, WSDL_SESSION);

			createMessagePartElement(message, methodName + WSDL_REQUEST, WSDL_PARAMETER);

			createMessagePartElement(createMessage(root, methodName + WSDL_RESPONSE), methodName + WSDL_RESPONSE,
					WSDL_RESULT);
		}
	}

	private static void createMessagePartElements(XMLBuilder root, String element) {
		XMLBuilder message = createMessage(root, element + WSDL_REQUEST);

		createMessagePartElement(message, WSDL_SESSION, WSDL_SESSION);
		createMessagePartElement(message, element + WSDL_REQUEST, WSDL_PARAMETER);

		createMessagePartElement(createMessage(root, element + WSDL_RESPONSE), element + WSDL_RESPONSE, WSDL_RESULT);
	}

	private static void createMessagePartElement(XMLBuilder message, String element, String name) {
		createMessagePart(message, "element", WSDL_REFERENCE + element, name);
	}

	private static void createMessagePart(XMLBuilder message, String part, String element, String elementName) {
		message.e("wsdl:part").a(part, element).a(Constants.name, elementName);
	}

	private static XMLBuilder createMessage(XMLBuilder xmlBuilder, String name) {
		return xmlBuilder.e("wsdl:message").a(Constants.name, name);
	}

	private static void createPortyType(XMLBuilder xmlBuilder, String beanName, List<Map<String, Object>> methods) {

		XMLBuilder portType = xmlBuilder.e("wsdl:portType").a(Constants.name, beanName + "WS");

		createPortyTypeOperations(portType, WSDL_ELEMENT_START_SESSION);
		createPortyTypeOperations(portType, WSDL_ELEMENT_END_SESSION);
		createPortyTypeOperations(portType, WSDL_ELEMENT_INCLUDE);
		createPortyTypeOperations(portType, WSDL_ELEMENT_UPDATE);
		createPortyTypeOperations(portType, WSDL_ELEMENT_GET);
		createPortyTypeOperations(portType, WSDL_ELEMENT_REMOVE);
		createPortyTypeOperations(portType, WSDL_ELEMENT_LIST);

		String methodName;

		for (Map<String, Object> method : methods) {
			methodName = method.get(Constants.name).toString();
			createPortyTypeOperations(portType, methodName);
		}
	}

	private static void createPortyTypeOperations(XMLBuilder portType, String element) {

		XMLBuilder operation = portType.e("wsdl:operation").a(Constants.name, element);
		createPortyTypeOperation(operation, WSDL_INPUT, element + WSDL_REQUEST);
		createPortyTypeOperation(operation, WSDL_OUTPUT, element + WSDL_RESPONSE);
	}

	private static void createPortyTypeOperation(XMLBuilder operation, String type, String name) {
		operation.e("wsdl:" + type).a("message", WSDL_REFERENCE + name).a(Constants.name, name);
	}

	private static void createBinding(XMLBuilder xmlBuilder, String beanName, List<Map<String, Object>> methods) {

		XMLBuilder binding = xmlBuilder.root().e("wsdl:binding").a(Constants.name, beanName + "WSSoapBinding").a("type",
				WSDL_REFERENCE + beanName + "WS");

		binding.e("soap:binding").a("style", "document").a("transport", "http://schemas.xmlsoap.org/soap/http");

		createBindingOperation(binding, WSDL_ELEMENT_START_SESSION);
		createBindingOperation(binding, WSDL_ELEMENT_END_SESSION);
		createBindingOperation(binding, WSDL_ELEMENT_INCLUDE);
		createBindingOperation(binding, WSDL_ELEMENT_UPDATE);
		createBindingOperation(binding, WSDL_ELEMENT_GET);
		createBindingOperation(binding, WSDL_ELEMENT_REMOVE);
		createBindingOperation(binding, WSDL_ELEMENT_LIST);

		String methodName;

		for (Map<String, Object> method : methods) {
			methodName = method.get(Constants.name).toString();
			createBindingOperation(binding, methodName);
		}
	}

	private static void createBindingOperation(XMLBuilder binding, String element) {

		XMLBuilder operation = binding.e("wsdl:operation").a(Constants.name, element);
		operation.e("soap:operation").a("soapAction", "");

		createBindingOperationType(operation, WSDL_INPUT, element + WSDL_REQUEST);
		createBindingOperationType(operation, WSDL_OUTPUT, element + WSDL_RESPONSE);
	}

	private static void createBindingOperationType(XMLBuilder operation, String type, String name) {
		operation.e("wsdl:" + type).a(Constants.name, name).a(Constants.name, name).e("soap:body").a("use", "literal");
	}

	private static void createService(XMLBuilder xmlBuilder, String beanName, String url) {

		xmlBuilder.e("wsdl:service").a(Constants.name, beanName + "WSService").e("wsdl:port")
				.a("binding", WSDL_REFERENCE + beanName + "WSSoapBinding").a(Constants.name, beanName + "WS")
				.e("soap:address").a("location", url + "/" + XMLUtil.toVoName(beanName) + "WS");
	}
}
