package com.aseg.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.util.StringUtils;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;

public class DAOTemplate extends JdbcTemplate implements RowMapper<Map<String, Object>> {
	private GenericVO type = null;
	public UtilServiceImpl util = null;

	public DAOTemplate(final DataSource arg, final Usuario user, GenericVO vo, UtilServiceImpl util) {
		super.setDataSource(arg);
		this.util = util;
		if (vo == null) {
			vo = new GenericVO();
		}
		this.type = (GenericVO) vo.clone();
		this.type.clear();

	}

	/**
	 * Create a Map instance to be used as column map.
	 * <p>
	 * By default, a linked case-insensitive Map will be created.
	 * 
	 * @param columnCount
	 *            the column count, to be used as initial capacity for the Map
	 * @return the new Map instance
	 * @see org.springframework.util.LinkedCaseInsensitiveMap
	 */
	@SuppressWarnings("unchecked")
	protected GenericVO createColumnMap(final int columnCount) {
		return (GenericVO) this.type.clone();
	}

	/**
	 * Determine the key to use for the given column in the column Map.
	 * 
	 * @param columnName
	 *            the column name as returned by the ResultSet
	 * @return the column key to use
	 * @see java.sql.ResultSetMetaData#getColumnName
	 */
	protected String getColumnKey(final String columnName) {
		return columnName;
	}

	@Override
	protected RowMapper<Map<String, Object>> getColumnMapRowMapper() {

		return this;
	}

	/**
	 * Retrieve a JDBC object value for the specified column.
	 * <p>
	 * The default implementation uses the <code>getObject</code> method.
	 * Additionally, this implementation includes a "hack" to get around Oracle
	 * returning a non standard object for their TIMESTAMP datatype.
	 * 
	 * @param rs
	 *            is the ResultSet holding the data
	 * @param index
	 *            is the column index
	 * @return the Object returned
	 * @see org.springframework.jdbc.support.JdbcUtils#getResultSetValue
	 */
	protected Object getColumnValue(final ResultSet rs, final int index) throws SQLException {
		return JdbcUtils.getResultSetValue(rs, index);
	}

	@Override
	public GenericVO mapRow(final ResultSet rs, final int rowNum) throws SQLException {
		final ResultSetMetaData rsmd = rs.getMetaData();
		final int columnCount = rsmd.getColumnCount();
		final GenericVO mapOfColValues = createColumnMap(columnCount);
		for (int i = 1; i <= columnCount; i++) {
			final String key = getColumnKey(JdbcUtils.lookupColumnName(rsmd, i));
			final Object obj = getColumnValue(rs, i);
			mapOfColValues.put(key, obj);
		}
		return mapOfColValues.validate(null);
	}

	public String queryForCharSeparatedString(final String sql, final Object[] args, final String c, final String field)
			throws DataAccessException {

		final List a = this.queryForList(sql, args);
		final ArrayList b = new ArrayList();
		for (final Iterator iterator = a.iterator(); iterator.hasNext();) {
			final Map object = (Map) iterator.next();
			b.add(object.get(field));
		}
		return StringUtils.arrayToDelimitedString(b.toArray(), c);
	}

	@Override
	public List queryForList(String sql) throws DataAccessException {
		sql = util.replace(sql, "1=1    AND", "");

		PagedListHolder f = new PagedListHolder();

		// final List upperCaseLinha = new LinkedList();
		try {
			return query(sql, getColumnMapRowMapper());
		} catch (final Exception e) {
			LogUtil.exception(e, null);
			return null;

		}

		// for (int i = 0, n = linhas.size(); i < n; i++) {
		// final GenericVO upperCaseTupla = (GenericVO) type.clone();
		// upperCaseTupla.clear();
		// final Object objTupla = linhas.get(i);
		// final Map tupla = (Map) objTupla;
		// final Iterator itTupla = tupla.keySet().iterator();
		// while (itTupla.hasNext()) {
		// String key = itTupla.next().toString();
		// final Object valor = tupla.get(key);
		// key = key.toUpperCase();
		// upperCaseTupla.put(key, valor);
		// }
		// upperCaseLinha.add(upperCaseTupla);
		// }
		//
		// return upperCaseLinha;
	}

	@Override
	public List<Map<String, Object>> queryForList(String sql, final Object[] params) throws DataAccessException {

		sql = util.replace(sql, "1=1    AND", "");
		if (params == null || params.length == 0) {
			return queryForList(sql);
		}

		// final List upperCaseLinha = new LinkedList();
		try {
			return query(sql, params, getColumnMapRowMapper());
		} catch (final Exception e) {
			LogUtil.exception(e, null);
			return new ArrayList();

		}
	}

	@Override
	public Map<String, Object> queryForMap(final String sql, final Object... args) throws DataAccessException {

		return super.queryForMap(sql, args);
	}

	@Override
	public int update(final String sql, final Object[] args) {

		return super.update(sql, args);

	}

}