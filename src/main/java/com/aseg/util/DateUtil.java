package com.aseg.util;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import com.aseg.logauditoria.service.LogUtil;

public class DateUtil {


    private static final SimpleDateFormat DATE;
    private static final SimpleDateFormat DATE_PT_BR;
    private static final SimpleDateFormat DATE_TIME;
    private static final SimpleDateFormat DATE_TIME_PT_BR;
    private static final SimpleDateFormat DB_DATE;
    private static final SimpleDateFormat DB_DATE_TIME;
    private static final SimpleDateFormat TIME;
    private static final SimpleDateFormat TIME_SEC;
    static long llastLogonAdjust = 11644473600000L; // adjust factor for
    private static final Locale Local = new Locale("pt", "BR");
    transient final static SimpleDateFormat dateFormat = new SimpleDateFormat();

    static {
        DATE = new SimpleDateFormat("dd/MM/yyyy");
        DATE_PT_BR = new SimpleDateFormat("dd/mm/yyyy", Local);
        // TIME = new SimpleDateFormat("HH:mm");
        DB_DATE = new SimpleDateFormat("yyyy-MM-dd");
        DB_DATE_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DATE_TIME = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        DATE_TIME_PT_BR = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Local);

        TIME = new SimpleDateFormat("HH:mm");
        TIME_SEC = new SimpleDateFormat("HH:mm:ss");


    }

    public static boolean horaIsOk(final String hora) {
        return checkFormat(hora, "HH:mm");
    }


    /**
     * M�todo dataIsOk
     *
     * @param String Valor para a vari�vel data
     * @return boolean
     */
    public boolean dataIsOk(final String data) {
        return checkFormat(data, "dd/MM/yyyy");
    }

    public String dateTimeToString(final Date data) {
        if (data != null) {
            dateFormat.applyPattern("dd/MM/yyyy HH:mm:ss");
            return dateFormat.format(data);
        } else {
            return "";
        }
    }

    /**
     * M�todo getDataAtual()
     *
     * @return Date
     */
    public static Date getDataAtual() {
        final SimpleTimeZone zone = new SimpleTimeZone(TimeZone.getTimeZone(
                "America/Sao_Paulo").getRawOffset(), "America/Sao_Paulo");
        return new java.util.Date(Calendar.getInstance(zone).getTimeInMillis());
    }

    public Object getHoraAtual() {
        final SimpleTimeZone zone = new SimpleTimeZone(TimeZone.getTimeZone(
                "America/Sao_Paulo").getRawOffset(), "America/Sao_Paulo");
        dateFormat.applyPattern("HH:mm");
        return dateFormat.format(new java.util.Date(Calendar.getInstance(zone)
                .getTimeInMillis()));

    }


    public static String now() {
        return DateUtil.fromTimestamp(new Timestamp(java.util.Calendar.getInstance()
                .getTime().getTime()));
    }

    public static Timestamp nowTimestamp() {
        return new Timestamp(java.util.Calendar.getInstance().getTime()
                .getTime());
    }

    /**
     * M�todo formatDate
     *
     * @param Date   Valor para a vari�vel data
     * @param String Valor para a vari�vel patters
     * @return String
     */
    public static String formatDate(final Date data, final String pattern) {
        dateFormat.applyPattern(pattern);
        try {
            return dateFormat.format(data);
        } catch (final Exception e) {
            return null;
        }
    }

    /**
     * @param time
     * @param pattern
     * @return
     */
    public String formatDate(final long time, final String pattern) {
        Date data;
        data = new Date(time);
        dateFormat.applyPattern(pattern);
        try {
            return dateFormat.format(data);
        } catch (final Exception e) {
            return null;
        }
    }

    /**
     * M�todo dateTimeToString
     *
     * @param String Valor para a vari�vel value
     * @return String
     */
    public String dateTimeToString(final String data) {
        if (data.length() > 10) {
            return data.substring(8, 10) + "/" + data.substring(5, 7) + "/"
                    + data.substring(0, 4);
        } else {
            return data;
        }
    }

    /**
     * M�todo dateToString
     *
     * @param Date Valor para a vari�vel data
     * @return String
     */
    public String dateToString(final Date data) {
        if (data != null) {
            dateFormat.applyPattern("dd/MM/yyyy");
            return dateFormat.format(data);
        } else {
            return "";
        }
    }

    public static boolean isValidDate(String data) {

		/*
         * Recebe uma String, e verifica se e uma data valida. Os padroes
		 * reconhecidos sao definidos no String[] formats...
		 */

        String[] formats = {"yyyy-MM-dd", "dd-MM-yyyy", "yyyy-MM-dd HH:mm:ss",
                "dd-MM-yyyy HH:mm:ss", "yyyy/MM/dd", "dd/MM/yyyy",
                "yyyy/MM/dd HH:mm:ss", "dd/MM/yyyy HH:mm:ss"};

        boolean result = true;
        for (int i = 0; i < formats.length; i++) {
            result = true;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(formats[i]);
                sdf.setLenient(false);
                sdf.parse(data);
            } catch (ParseException e) {
                result = false;
            } catch (IllegalArgumentException e) {
                result = false;
            } catch (Exception e) {
                result = false;
            }
            if (result == true) {
                break;
            }
        }

        return result;

    }

    /**
     * M�todo stringToDate
     *
     * @param String Valor para a vari�vel data
     * @return Date
     */
    public static Date stringToDate(final String data) {
        dateFormat.applyPattern("dd/MM/yyyy");
        try {
            return dateFormat.parse(data);
        } catch (final Exception e) {
            return null;
        }
    }


    /**
     * M�todo stringToDateConcatAtualTime
     *
     * @param String Valor para a vari�vel data
     * @return Date
     */
    public Date stringToDateConcatAtualTime(final String data) {
        final String dia = data.substring(0, 2);
        final String mes = data.substring(3, 5);
        final String ano = data.substring(6, 10);

        final Calendar cal = Calendar.getInstance();

        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dia));
        cal.set(Calendar.MONTH, Integer.parseInt(mes) - 1);
        cal.set(Calendar.YEAR, Integer.parseInt(ano));
        cal.set(Calendar.HOUR_OF_DAY,
                Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, Calendar.getInstance().get(Calendar.MINUTE));

        cal.set(Calendar.SECOND, Calendar.getInstance().get(Calendar.SECOND));
        // LogUtil.debug( new Date(cal.getTime().getTime()).toString());
        return new Date(cal.getTime().getTime());

    }

    /**
     * M�todo stringToTime
     *
     * @param String Valor para a vari�vel time
     * @return Date
     */
    public Date stringToTime(final String time) {
        dateFormat.applyPattern("HH:mm");
        try {
            return dateFormat.parse(time);
        } catch (final Exception e) {
            return null;
        }
    }


    /**
     * M�todo stringToDateTime
     *
     * @param String Valor para a vari�vel data
     * @return Date
     */
    public Date stringToDateTime(final String data) {
        dateFormat.applyPattern("dd/MM/yyyy HH:mm:ss");
        try {
            // LogUtil.debug(dateFormat.parse(data));
            return dateFormat.parse(data);
        } catch (final Exception e) {
            LogUtil.exception(e, null);
            return null;
        }
    }

    public Date parseDate(final String data, final String pattern) {
        Date result = null;
        if (pattern == null) {
            SimpleDateFormat formatter = null;
            if (data.contains("UTC-")) {
                formatter = new SimpleDateFormat(
                        "EEE MMM dd HH:mm:ss 'UTC'Z yyyy", Locale.ENGLISH);
            } else if (data.contains("GMT-")) {
                formatter = new SimpleDateFormat(
                        "EEE MMM dd yyyy HH:mm:ss 'GMT'Z", Locale.ENGLISH);
            } else {
                formatter = new SimpleDateFormat("yyyy-MM-dd");
            }
            try {
                return formatter.parse(data);
            } catch (ParseException e) {
                return null;
            }
        } else {
            dateFormat.applyPattern(pattern);
            try {
                return dateFormat.parse(data);
            } catch (final Exception e) {
                return null;
            }
        }
    }

    /**
     * M�todo checkFormat
     *
     * @param String Valor para a vari�vel value
     * @param String Valor para a vari�vel format
     * @return boolean
     */
    private static boolean checkFormat(final String value, final String format) {
        dateFormat.applyPattern(format);
        try {
            return dateFormat.parse(value) != null;
        } catch (final ParseException pe) {
            return false;
        }
    }

    public static Timestamp dateToTimestamp(Date date) {

        return new Timestamp(date.getTime());

    }

    public static Timestamp add(Timestamp t, int field, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(t.getTime());
        c.add(field, amount);
        return new Timestamp(c.getTimeInMillis());
    }


    public static Time fromStringHora(final String entrada) {
        try {
            return new Time(TIME.parse(entrada).getTime());
        } catch (ParseException e1) {
            // TODO Auto-generated catch block

            try {

                return new Time(TIME_SEC.parse(
                        entrada.toString().substring(11, 13) + ":"
                                + entrada.toString().substring(14, 16) + ":"
                                + entrada.toString().substring(17, 19))
                        .getTime());
            } catch (final Exception e) {

                try {
                    return new Time(TIME_SEC.parse(entrada + ":00").getTime());

                } catch (final Exception ex) {
                    return new Time(new Long(entrada));
                }
            }
        }

    }


    public static Object formatoMonetario(final Object a, final int c) {
        String o = "";
        if (a != null && a.toString().length() > 0) {

            if (a instanceof BigDecimal) {

                o = formatoMonetario((BigDecimal) a, c);

            } else if (a instanceof Double) {

                o = formatoMonetario(new BigDecimal((Double) a), c);

            } else {
                final String var = (String) a;
                if (var.indexOf("%") > -1) {
                    o = var;
                } else if (var.indexOf("URH") > -1) {
                    o = formatoMonetario(currencyToBigDecimal(var), c);
                    o += " URH";
                } else {
                    o = formatoMonetario(currencyToBigDecimal(var), c);
                }

            }
        }
        return o;
    }

    public static BigDecimal currencyToBigDecimal(String var) {

        final DecimalFormatSymbols dfs = new DecimalFormatSymbols(Local);
        String ds = String.valueOf(dfs.getDecimalSeparator());

        String gs = String.valueOf(dfs.getGroupingSeparator());
        if(gs.equalsIgnoreCase("."))
        	gs ="\\.";
        if(ds.equalsIgnoreCase("."))
        	ds ="\\.";
        var = var.replaceAll(gs, "");
        var = var.replaceAll(ds, ".");

        // TODO Auto-generated method stub
        return new BigDecimal(var);
    }

    //
    // private String fromTimestampToDatabaseDateTime(final Timestamp entrada) {
    // String out = "";
    // try {
    // out = DB_DATE_TIME.format(entrada);
    // return out;
    // } catch (final Exception ex) {
    // ex.printStackTrace();
    // return null;
    // }
    //
    // }
    //
    // private double formatarMonetario(final String valor) {
    // double retorno = 0.00;
    // if (valor != null || valor.trim().length() != 0) {
    // final DecimalFormatSymbols dfs = new DecimalFormatSymbols(Local);
    // // dfs.setDecimalSeparator(',');
    // // dfs.setGroupingSeparator('.');
    // final DecimalFormat format = new DecimalFormat(
    // "#,###,###,###,###.0#", dfs);
    // try {
    // retorno = format.parse(valor).doubleValue();
    // } catch (final ParseException e) {
    // return retorno;
    // }
    // }
    // return retorno;
    // }
    //
    // // Formata o valor com 4 casas decimais
    // private double formatarQuantidade(final String valor) {
    // double retorno = 0.0000;
    // if (valor != null || valor.trim().length() != 0) {
    // final DecimalFormatSymbols dfs = new DecimalFormatSymbols(Local);
    // // dfs.setDecimalSeparator(',');
    // // dfs.setGroupingSeparator('.');
    // final DecimalFormat format = new DecimalFormat(
    // "#,###,###,###,###.0###", dfs);
    // try {
    // retorno = format.parse(valor).doubleValue();
    // } catch (final ParseException e) {
    // e.printStackTrace();
    // }
    // }
    // return retorno;
    // }
    //
    // private String formatoMonetario(final BigDecimal valor) {
    // return formatoMonetario(valor, 2);
    // }

    // private String formatoMonetarioUsuario(final BigDecimal valor,
    // final User user) {
    // try {
    // String v = formatoMonetario(valor, 2);
    // v = SysUtil.replace(v, ".", "");
    // v = SysUtil.replace(v, ",",
    // user.getSystemProperty("SEPARADOR_DECIMAL"));
    //
    // return v;
    //
    // } catch (final NumberFormatException e) {
    // e.printStackTrace();
    // } catch (final Exception e) {
    // e.printStackTrace();
    // }
    // return null;
    // }
    //
    // private String fromlong(final long entrada) {
    // return fromTimestamp(new Timestamp(entrada));
    // }
    //
    // private Timestamp fromString(final SimpleDateFormat format,
    // final String date) throws ParseException {
    //
    // return new Timestamp(format.parse(date).getTime());
    // }
    //
    // private String fromString(final String entrada) {
    //
    // try {
    // return entrada.toString().substring(11, 13);
    // } catch (final Exception e) {
    // // LogUtil.debug("[DataFormat -- fromTimestamp] Erro de parse
    // // da data "+entrada);
    // e.printStackTrace();
    // return null;
    // }
    // }

    // private Time fromStringHora(final String entrada) {
    // DateFormat dtFmt = new SimpleDateFormat("HH:mm:ss");
    // try {
    // final Time temp = new Time(dtFmt.parse(
    // entrada.substring(entrada.indexOf(':') - 2,
    // entrada.length())).getTime());
    // return temp;
    // } catch (final Exception e) {
    // dtFmt = new SimpleDateFormat("HH:mm");
    // try {
    // final Time temp = new Time(dtFmt.parse(
    // entrada.substring(entrada.indexOf(':') - 2,
    // entrada.length())).getTime());
    // return temp;
    // } catch (final ParseException e1) {
    //
    // e1.printStackTrace();
    // }
    // }
    // return null;
    // }

    //
    //
    // private String fromTimestampcomHora(final Timestamp entrada) {
    //
    // try {
    // return DATE_TIME.format(entrada);
    // } catch (final Exception e) {
    // // LogUtil.debug("[DataFormat -- fromTimestamp] Erro de parse
    // // da data "+entrada);
    // e.printStackTrace();
    // return null;
    // }
    // }
    public static String fromTimestamp(final Timestamp entrada) {

        if (entrada == null) {
            return null;
        }
        try {
            return DATE.format(entrada);
        } catch (final Exception e) {
            LogUtil.exception(e, null);
            return null;
        }
    }

    public static String fromTimestampcomHoraptBR(final Timestamp entrada) {
        String out = "";

        try {
            out = DATE_TIME_PT_BR.format(entrada);
            return out;
        } catch (final IllegalArgumentException pexp) {
            try {
                out = DB_DATE_TIME.format(entrada);
                return out;
            } catch (final Exception ex) {
                LogUtil.exception(ex, null);
                return null;
            }
        } catch (final Exception e) {
            // LogUtil.debug("[DataFormat -- fromTimestamp] Erro de parse
            // da data "+entrada);
            LogUtil.exception(e, null);
            return null;
        }
    }

    public static Object stringToTimestamp(final Object value) {
        Date a = null;
        if (value == null || value.toString().trim().length() == 0) {
            return null;
        }
        if (value.equals("now")) {
            return new Timestamp(System.currentTimeMillis());
        }
        try {
            a = DATE.parse(value.toString());
        } catch (final Exception e) {
            try {
                a = DATE_PT_BR.parse(value.toString());
            } catch (final Exception e1) {
                try {
                    a = DATE_TIME.parse(value.toString());
                } catch (final ParseException e2) {

                    try {
                        a = DATE_TIME_PT_BR.parse(value.toString());
                    } catch (final Exception e3) {
                        try {
                            a = DB_DATE.parse(value.toString());
                        } catch (final Exception e4) {

                            try {
                                a = DB_DATE_TIME.parse(value.toString());
                            } catch (final Exception e5) {
                                // convertendo datas LDAP
                                try {
                                    if (value == null || value.equals("0"))
                                        return null;

                                    // date Epoch
                                    a = new Date(Long.parseLong(value
                                            .toString())
                                            / 10000
                                            - llastLogonAdjust); //

                                } catch (final Exception e6) {
                                    // convertendo datas LDAP
                                    LogUtil.exception(e6, null);
                                }
                            }
                        }

                    }

                }

            }

        }

        if (a == null) {
            return null;
        } else {
            return new Timestamp(a.getTime());
        }
    }

    public static String millisecondsToMinutes(long millis) {
        return String.format(
                "%d m, %d s",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis)
                        - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                        .toMinutes(millis)));
    }


    /**
     * M�todo timeToString
     *
     * @param Date Valor para a vari�vel time
     * @return String
     */
    public String timeToString(final Date time) {
        dateFormat.applyPattern("HH:mm");
        return dateFormat.format(time);
    }

    /**
     * M�todo transformaHoraMilisegundo
     *
     * @return Retorna Milisegundo em string.Exemplo : number
     */

    // public HashMap JSONtoMap(String json)
    // {
    // JSONDeserializer<HashMap> d = new JSONDeserializer<HashMap>();
    // return d.use(null, HashMap.class).deserialize(json);
    // }
    public int transformaHoraMilisegundo(final String Hora) throws Exception {

        int Hrs = 0, Minut = 0;
        final int seg = 0, mseg = 0;
        int horamilisegundos = 0;

        final String total = Hora
                .substring(0, Hora.toString().lastIndexOf(":"));

        try {

            Hrs = Integer.parseInt(total.substring(0, 2));

            Minut = Integer.parseInt(total.substring(3, 5));

        } catch (final Exception e) {

            Hrs = Integer.parseInt(total.substring(0, 3));

            Minut = Integer.parseInt(total.substring(4, 5));

        }

        horamilisegundos = mseg + seg * 1000 + Minut * 60000

                + Hrs * 3600000;

        // LogUtil.debug("horamilisegundos ::: >> " + horamilisegundos);

        return horamilisegundos;

    }

    public String transformaMilisegundoHora(final String milis)

            throws Exception {

        int Hrs, Minut, seg, milisegundo;

        String Hora = null, Minuto = null, Segundo = null;

        milisegundo = Integer.parseInt(milis);

        Hrs = milisegundo / 3600000;

        milisegundo = milisegundo % 3600000;

        Minut = milisegundo / 60000;

        milisegundo = milisegundo % 60000;

        seg = milisegundo / 1000;

        if (String.valueOf(Hrs).length() < 2) {
            Hora = "0" + String.valueOf(Hrs);
        } else {
            Hora = String.valueOf(Hrs);
        }

        if (String.valueOf(Minut).length() < 2) {
            Minuto = "0" + String.valueOf(Minut);
        } else {
            Minuto = String.valueOf(Minut);
        }

        if (String.valueOf(seg).length() < 2) {
            Segundo = "0" + String.valueOf(seg);
        } else {
            Segundo = String.valueOf(seg);
        }

        // LogUtil.debug("HORAS ::: >> " + Hora + ":"+ Minuto+":"+Segundo);

        return Hora + ":" + Minuto + ":" + Segundo;

    }

    /**
     * Formatadores de nmeros!!!
     */
    private static String formatoMonetario(final BigDecimal valor,
                                           final int qtdCentavos) {
        final NumberFormat format = NumberFormat.getNumberInstance(Local);
        // format = NumberFormat.getCurrencyInstance(util.getLocale());
        format.setMaximumFractionDigits(qtdCentavos);
        format.setMinimumFractionDigits(qtdCentavos);
        String stringRetorno = "0,00";
        try {
            // Assume form is a DecimalFormat
            stringRetorno = format.format(valor);
            if (stringRetorno.equals("-0,00")) {
                stringRetorno = "0,00";
            }
        } catch (final IllegalArgumentException e) {
            // LogUtil.debug("Exceo em
            // MostrarResumoAction.formatoMonetario - valor: " + valor + " -
            // Erro: " + e.getMessage());
        }
        return stringRetorno;
    }

    public static Object fromTime(Time substring) {
        // TODO Auto-generated method stub
        return TIME.format(substring);
    }

    public static Object fromTimeSec(Time substring) {
        // TODO Auto-generated method stub
        return TIME_SEC.format(substring);
    }


    public static String subtractDay(String valor1) {
        // TODO Auto-generated method stub
        return null;
    }
}