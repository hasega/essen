package com.aseg.util.workflow;

import com.aseg.seguranca.Usuario;
import com.opensymphony.workflow.config.SpringConfiguration;

/**
 * @author Lucas Neves Martins
 * @email lmartins@aseg.com.br
 **/
public class WorkflowConfiguration extends SpringConfiguration {

	Usuario user;

	public WorkflowConfiguration() {

	}

	public Usuario getUser() {
		return user;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

}
