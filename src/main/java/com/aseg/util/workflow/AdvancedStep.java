package com.aseg.util.workflow;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;
import com.opensymphony.workflow.spi.Step;

/**
 * @author Lucas Neves Martins
 * @email lmartins@aseg.com.br
 **/
public class AdvancedStep extends ServiceImpl implements Step {

	private static final long serialVersionUID = -3140981403231508003L;

	public List getCurrentSteps(Long instanceId, Usuario user) {

		GenericService s = (GenericService) user.getFactory().getSpringBean("WorkFlowCurrentStepService",
				user);
		List steps = new ArrayList();

		try {
			GenericVO vo = s.createVO(user);
			vo.setFiltro("{gA:[{c:{f:'ID_ACTION',o:'nl'}}]}");
			vo.putKey("ID_WORKFLOW_INSTANCE", instanceId);
			List stepsMaps = s.lista(vo, user);

			for (Iterator iterator = stepsMaps.iterator(); iterator.hasNext();) {
				GenericVO stepMap = (GenericVO) iterator.next();
				AdvancedStep step = new AdvancedStep();

				step.id = (Long) stepMap.get("ID_WORKFLOW_INSTANCE_CURSTEP");

				step.entryId = (Long) stepMap.get("ID_WORKFLOW_INSTANCE");
				step.stepId = ((Long) stepMap.get("ID_STEP")).intValue();

				if (stepMap.get("ID_ACTION") == null) {
					step.actionId = 0;
				} else {
					step.actionId = ((Long) stepMap.get("ID_ACTION")).intValue();
				}

				step.owner = (String) stepMap.get("OWNER");
				step.startDate = (Date) stepMap.get("START_DATE");
				step.finishDate = (Date) stepMap.get("FINISH_DATE");
				step.dueDate = (Date) stepMap.get("DUE_DATE");
				step.status = (String) stepMap.get("STATUS");
				step.caller = (String) stepMap.get("CALLER");

				steps.add(step);

			}

			return steps;

		} catch (Exception e) {
			LogUtil.exception(e, user);
			return null;
		}
	}

	private Date dueDate;
	private Date finishDate;
	private Date startDate;
	private String caller;
	private String owner;
	private String status;
	private long[] previousStepIds;
	private int actionId;
	private int stepId;
	private long entryId;

	private long id;

	public AdvancedStep() {

	}

	public AdvancedStep(Long currentStepId, Usuario user) {

		GenericService s = (GenericService) user.getFactory().getSpringBean("WorkFlowCurrentStepService",
				user);

		try {

			Map m = s.obtem(s.createVO(user).putKey("ID_WORKFLOW_INSTANCE_CURSTEP", currentStepId), user);

			LogUtil.debug("Carregando Step...", user);

			this.id = currentStepId;
			this.entryId = (Long) m.get("ID_WORKFLOW_INSTANCE");
			this.stepId = ((Long) m.get("ID_STEP")).intValue();

			if (m.get("ID_ACTION") == null) {
				this.actionId = 1;
			} else {
				this.actionId = ((Long) m.get("ID_ACTION")).intValue();
			}

			this.owner = (String) m.get("OWNER");
			this.startDate = (Date) m.get("START_DATE");
			this.finishDate = (Date) m.get("FINISH_DATE");
			this.dueDate = (Date) m.get("DUE_DATE");
			this.status = (String) m.get("STATUS");
			this.caller = (String) m.get("CALLER");

		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
	}

	@Override
	public int getActionId() {
		return actionId;
	}

	@Override
	public String getCaller() {
		return caller;
	}

	@Override
	public Date getDueDate() {
		return dueDate;
	}

	@Override
	public long getEntryId() {
		return entryId;
	}

	@Override
	public Date getFinishDate() {
		return finishDate;
	}

	public long getInstanceId() {
		return entryId;
	}

	@Override
	public String getOwner() {
		return owner;
	}

	@Override
	public long[] getPreviousStepIds() {
		return previousStepIds;
	}

	@Override
	public Date getStartDate() {
		return startDate;
	}

	@Override
	public String getStatus() {
		return status;
	}

	@Override
	public int getStepId() {
		return stepId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public void setEntryId(long entryId) {
		this.entryId = entryId;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setPreviousStepIds(long[] previousStepIds) {
		this.previousStepIds = previousStepIds;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setStepId(int stepId) {
		this.stepId = stepId;
	}

	@Override
	public long getId() {
		return this.id;
	}

}
