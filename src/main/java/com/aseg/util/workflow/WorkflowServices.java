package com.aseg.util.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;
import com.opensymphony.workflow.InvalidActionException;
import com.opensymphony.workflow.InvalidEntryStateException;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.InvalidRoleException;
import com.opensymphony.workflow.WorkflowException;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;

/**
 * @author Lucas Neves Martins
 * @email lmartins@aseg.com.br
 **/
public class WorkflowServices extends ServiceImpl {

	static WorkflowServices instance;

	public static WorkflowServices getInstance(Usuario user) {
		if (instance == null) {
			instance = new WorkflowServices();
		}
		return instance;

	}

	private WorkflowServices() {
		// TODO Auto-generated constructor stub
	}

	public boolean delete(GenericVO vo, Usuario user) throws InvalidActionException, InvalidRoleException,
			InvalidInputException, InvalidEntryStateException, WorkflowException {

		LogUtil.fatal("Call WorkflowServices.delete nao foi implementada");

		return true;
	}

	public boolean getImage(GenericVO vo, Usuario user) {

		AdvancedWorkflow wf = new AdvancedWorkflow(new Long(vo.get("ID_WORKFLOW").toString()), user);

		WorkflowDescriptor wd = wf.getWorkflowDescriptor();

		LogUtil.debug("refresh IMAGE", user);
		return false;
	}

	public boolean listActions(GenericVO vo, Usuario user) {

		// Long workflowId;
		Long workflowInstanceId;

		if (vo.get("ID_WORKFLOW_INSTANCE") != null) {

			try {

				return false;

			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

		} else {

			LogUtil.fatal("listActions recebeu uma GenericVO sem os valores esperados.");
			return false;
		}

		return false;

	}

	public boolean listSteps(GenericVO vo, Usuario user) {

		Long workflowInstanceId;

		if (vo.get("ID_WORKFLOW") != null) {

			try {

				workflowInstanceId = Long.parseLong(vo.get("ID_WORKFLOW").toString());

				List<StepDescriptor> steps = new AdvancedWorkflow(workflowInstanceId, user).getWorkflowDescriptor()
						.getSteps();

				ArrayList data = new ArrayList();

				Iterator<StepDescriptor> iterator = steps.iterator();
				while (iterator.hasNext()) {
					StepDescriptor s = iterator.next();
					Map c = new HashMap();

					c.put(Constants.name, s.getName());
					c.put("ID_STEP", s.getId());

					// if (s.getId() == wfi.getState()) {
					// c.put("CURRENT", Constants.true_str);
					// } else {
					// c.put("CURRENT", Constants.false_str);
					// }

					data.add(c);
				}

				vo.put(Constants.LIST_DATA, data);

				return false;

			} catch (Exception e) {
				LogUtil.exception(e, user);
				LogUtil.fatal("listSteps no conseguiu carregar a instncia do Workflow.");
			}

		} else {

			LogUtil.fatal("listSteps recebeu uma GenericVO sem os valores esperados.");
			return false;
		}

		return false;

	}

	public boolean refresh(GenericVO vo, Usuario user) {
		LogUtil.debug("refresh WFConfig", user);
		return true;
	}

	public boolean start(GenericVO vo, Usuario user) throws InvalidActionException, InvalidRoleException,
			InvalidInputException, InvalidEntryStateException, WorkflowException {

		Long instanceId = getWfc().createInstance(vo, vo, null, user);

		vo.put("ID_WORKFLOW_INSTANCE", instanceId);

		/*
		 * AdvancedWorkflow wf = new AdvancedWorkflow( (Long)
		 * vo.get("ID_WORKFLOW"), user);
		 * 
		 * 
		 * Long id = null; try { id = wf.initialize(1, null); } catch (Exception
		 * e) {
		 * 
		 * e.printStackTrace(); } vo.put("ID_WORKFLOW_INSTANCE", new Long(id));
		 * 
		 * return false;
		 */

		return false;
	}

}
