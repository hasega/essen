package com.aseg.util.workflow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.FactoryException;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.InvalidActionException;
import com.opensymphony.workflow.InvalidEntryStateException;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.InvalidRoleException;
import com.opensymphony.workflow.JoinNodes;
import com.opensymphony.workflow.Register;
import com.opensymphony.workflow.StoreException;
import com.opensymphony.workflow.TypeResolver;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.Workflow;
import com.opensymphony.workflow.WorkflowContext;
import com.opensymphony.workflow.WorkflowException;
import com.opensymphony.workflow.basic.BasicWorkflowContext;
import com.opensymphony.workflow.config.Configuration;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import com.opensymphony.workflow.loader.ConditionalResultDescriptor;
import com.opensymphony.workflow.loader.ConditionsDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.opensymphony.workflow.loader.JoinDescriptor;
import com.opensymphony.workflow.loader.RegisterDescriptor;
import com.opensymphony.workflow.loader.RestrictionDescriptor;
import com.opensymphony.workflow.loader.ResultDescriptor;
import com.opensymphony.workflow.loader.SplitDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.opensymphony.workflow.query.WorkflowExpressionQuery;
import com.opensymphony.workflow.query.WorkflowQuery;
import com.opensymphony.workflow.spi.Step;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.opensymphony.workflow.spi.WorkflowStore;
import com.opensymphony.workflow.util.VariableResolver;

/**
 * @author Lucas Neves Martins
 * @email lmartins@aseg.com.br
 **/
public class AdvancedWorkflow extends ServiceImpl implements Workflow {

	Long id;
	String nome;
	String descricao;
	String ctx;
	WorkflowDescriptor workflowDescriptor;
	private Usuario user;
	private WorkflowConfiguration configuration;
	private final ThreadLocal stateCache = new ThreadLocal();
	private TypeResolver typeResolver;
	private List currentSteps;
	protected WorkflowContext context;

	public AdvancedWorkflow(final Long idWorkflow, Usuario user) {
		context = new BasicWorkflowContext(user.getLogin());
		stateCache.set(new HashMap());

		GenericService s = (GenericService) user.getFactory().getSpringBean("WorkFlowService", user);

		try {

			Map m = s.obtem(s.createVO(user).putKey("ID_WORKFLOW", idWorkflow), user);

			this.user = user;
			this.nome = (String) m.get("NOME");
			this.descricao = (String) m.get("DESCRICAO");
			this.id = idWorkflow;

			workflowDescriptor = this.getWorkflowDescriptor();

		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

	}

	public AdvancedWorkflow(final String workflowName, Usuario user) {
		stateCache.set(new HashMap());

		GenericService s = (GenericService) user.getFactory().getSpringBean("WorkFlowService", user);

		try {

			Map m = s.obtem(s.createVO(user).putKey("NOME", workflowName), user);

			this.user = user;
			this.nome = workflowName;
			this.descricao = (String) m.get("DESCRICAO");
			this.id = (Long) m.get("ID_WORKFLOW");
			this.ctx = (String) m.get("CONTEXTO");
			this.setConfiguration(getConfiguration());
			workflowDescriptor = this.getWorkflowDescriptor();

		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

	}

	public String getCtx() {
		return ctx;
	}

	public void setCtx(String ctx) {
		this.ctx = ctx;
	}

	@Override
	public boolean canInitialize(String workflowName, int initialAction) {
		return canInitialize(workflowName, initialAction, null);
	}

	@Override
	public boolean canInitialize(String workflowName, int initialAction, Map inputs) {
		final String mockWorkflowName = workflowName;
		WorkflowEntry mockEntry = new WorkflowEntry() {

			@Override
			public long getId() {
				return 0;
			}

			@Override
			public int getState() {
				return WorkflowEntry.CREATED;
			}

			@Override
			public String getWorkflowName() {
				return mockWorkflowName;
			}

			@Override
			public boolean isInitialized() {
				return false;
			}
		};

		// since no state change happens here, a memory instance is just fine
		PropertySet ps = PropertySetManager.getInstance("memory", null);
		Map transientVars = new HashMap();

		if (inputs != null) {
			transientVars.putAll(inputs);
		}

		try {
			populateTransientMap(mockEntry, transientVars, Collections.EMPTY_LIST, new Integer(initialAction),
					Collections.EMPTY_LIST, ps);

			return canInitialize(workflowName, initialAction, transientVars, ps);
		} catch (InvalidActionException e) {
			LogUtil.fatal(e.getMessage());

			return false;
		} catch (WorkflowException e) {
			LogUtil.fatal("Error checking canInitialize", e);

			return false;
		}
	}

	private boolean canInitialize(String workflowName, int initialAction, Map transientVars, PropertySet ps)
			throws WorkflowException {

		transientVars.put("user", user);

		WorkflowDescriptor wf = getWorkflowDescriptor();

		ActionDescriptor actionDescriptor = wf.getInitialAction(initialAction);

		if (actionDescriptor == null) {
			throw new InvalidActionException("Invalid Initial Action #" + initialAction);
		}

		RestrictionDescriptor restriction = actionDescriptor.getRestriction();
		ConditionsDescriptor conditions = null;

		if (restriction != null) {
			conditions = restriction.getConditionsDescriptor();
		}

		return passesConditions(conditions, new HashMap(transientVars), ps, 0);
	}

	/**
	 * @ejb.interface-method
	 */

	@Override
	public boolean canModifyEntryState(long id, int newState) {
		try {
			WorkflowStore store = getPersistence();
			WorkflowEntry entry = store.findEntry(id);
			int currentState = entry.getState();
			boolean result = false;

			switch (newState) {
			case WorkflowEntry.COMPLETED:

				if (currentState == WorkflowEntry.ACTIVATED) {
					result = true;
				}

				break;

			case WorkflowEntry.CREATED:
				result = false;

			case WorkflowEntry.ACTIVATED:

				if (currentState == WorkflowEntry.CREATED || currentState == WorkflowEntry.SUSPENDED) {
					result = true;
				}

				break;

			case WorkflowEntry.SUSPENDED:

				if (currentState == WorkflowEntry.ACTIVATED) {
					result = true;
				}

				break;

			case WorkflowEntry.KILLED:

				if (currentState == WorkflowEntry.CREATED || currentState == WorkflowEntry.ACTIVATED
						|| currentState == WorkflowEntry.SUSPENDED) {
					result = true;
				}

				break;

			default:
				result = false;

				break;
			}

			return result;
		} catch (StoreException e) {
			LogUtil.fatal("Error checking state modifiable for instance #" + id, e);
		}

		return false;
	}

	@Override
	public void changeEntryState(long id, int newState) throws WorkflowException {
		WorkflowStore store = getPersistence();
		WorkflowEntry entry = store.findEntry(id);

		if (entry.getState() == newState) {
			return;
		}

		if (canModifyEntryState(id, newState)) {
			if (newState == WorkflowEntry.KILLED || newState == WorkflowEntry.COMPLETED) {
				Collection currentSteps = getCurrentSteps(id);

				if (currentSteps.size() > 0) {
					completeEntry(null, id, currentSteps, newState);
				}
			}

			store.setEntryState(id, newState);
		} else {
			throw new InvalidEntryStateException("Can't transition workflow instance #" + id + ". Current state is "
					+ entry.getState() + ", requested state is " + newState);
		}

		LogUtil.debug(entry.getId() + " : State is now : " + entry.getState(), user);

	}

	protected void checkImplicitFinish(ActionDescriptor action, long id) throws WorkflowException {
		AdvancedWorkflowStore store = (AdvancedWorkflowStore) getPersistence();
		WorkflowInstance entry = (WorkflowInstance) store.findEntry(id);

		AdvancedWorkflow workflow = entry.getWorkflow(user);

		WorkflowDescriptor wf = workflow.getWorkflowDescriptor();

		Collection currentSteps = store.findCurrentSteps(id);

		boolean isCompleted = true;

		for (Iterator iterator = currentSteps.iterator(); iterator.hasNext();) {
			Step step = (Step) iterator.next();
			StepDescriptor stepDes = wf.getStep(step.getStepId());

			// if at least on current step have an available action
			if (stepDes.getActions().size() > 0) {
				isCompleted = false;
			}
		}

		if (isCompleted) {
			completeEntry(action, id, currentSteps, WorkflowEntry.COMPLETED);
		}
	}

	/*
	 * public Configuration getConfiguration() { return this.getConfiguration();
	 * }
	 */

	protected void completeEntry(ActionDescriptor action, long id, Collection currentSteps, int state)
			throws StoreException {
		getPersistence().setEntryState(id, state);

		Iterator i = new ArrayList(currentSteps).iterator();

		while (i.hasNext()) {
			Step step = (Step) i.next();
			String oldStatus = action != null ? action.getUnconditionalResult().getOldStatus() : "Finished";
			getPersistence().markFinished(step, action != null ? action.getId() : -1, new Date(), oldStatus,
					context.getCaller());
			getPersistence().moveToHistory(step);
		}
	}

	private Step createNewCurrentStep(ResultDescriptor theResult, WorkflowEntry entry, WorkflowStore store,
			int actionId, Step currentStep, long[] previousIds, Map transientVars, PropertySet ps)
			throws WorkflowException {
		try {
			int nextStep = theResult.getStep();

			if (nextStep == -1) {
				if (currentStep != null) {
					nextStep = currentStep.getStepId();
				} else {
					throw new StoreException(
							"Illegal argument: requested new current step same as current step, but current step not specified");
				}
			}

			LogUtil.debug("Outcome: stepId=" + nextStep + ", status=" + theResult.getStatus() + ", owner="
					+ theResult.getOwner() + ", actionId=" + actionId + ", currentStep="
					+ (currentStep != null ? currentStep.getStepId() : 0), user);

			if (previousIds == null) {
				previousIds = new long[0];
			}

			String owner = theResult.getOwner();

			VariableResolver variableResolver = getConfiguration().getVariableResolver();

			if (owner.equals("${caller}")) {
				owner = "Sistema";
			}

			if (owner != null && !owner.equals("Sistema")) {
				Object o = variableResolver.translateVariables(owner, transientVars, ps);
				owner = o != null ? o.toString() : null;
			}

			String oldStatus = theResult.getOldStatus();
			oldStatus = variableResolver.translateVariables(oldStatus, transientVars, ps).toString();

			String status = theResult.getStatus();
			status = variableResolver.translateVariables(status, transientVars, ps).toString();

			if (currentStep != null) {
				store.markFinished(currentStep, actionId, new Date(), oldStatus, context.getCaller());
				store.moveToHistory(currentStep);

				// store.moveToHistory(actionId, new Date(), currentStep,
				// oldStatus, context.getCaller());
			}

			// construct the start date and optional due date
			Date startDate = new Date();
			Date dueDate = null;

			if (theResult.getDueDate() != null && theResult.getDueDate().length() > 0) {
				Object dueDateObject = variableResolver.translateVariables(theResult.getDueDate(), transientVars, ps);

				if (dueDateObject instanceof Date) {
					dueDate = (Date) dueDateObject;
				} else if (dueDateObject instanceof String) {
					long offset = 0;

					try {
						offset = Long.parseLong((String) dueDateObject);
					} catch (NumberFormatException e) {
					}

					if (offset > 0) {
						dueDate = new Date(startDate.getTime() + offset);
					}
				} else if (dueDateObject instanceof Number) {
					Number num = (Number) dueDateObject;
					long offset = num.longValue();

					if (offset > 0) {
						dueDate = new Date(startDate.getTime() + offset);
					}
				}
			}
			String decisao = "";
			if (transientVars.get(Constants.VOKey) != null
					&& ((Map) transientVars.get(Constants.VOKey)).get("DECISAO") != null) {
				decisao = ((Map) transientVars.get(Constants.VOKey)).get("DECISAO").toString();
			}

			AdvancedStep newStep = (AdvancedStep) ((AdvancedWorkflowStore) store).createCurrentStep(entry.getId(),
					nextStep, owner, startDate, dueDate, status, previousIds, decisao);

			transientVars.put("createdStep", newStep);

			if (previousIds != null && previousIds.length == 0 && currentStep == null) {
				// At this point, it must be a brand new workflow, so we'll
				// overwrite the empty currentSteps
				// with an array of just this current step
				List currentSteps = new ArrayList();
				currentSteps.add(newStep);
				transientVars.put("currentSteps", new ArrayList(currentSteps));
			}

			WorkflowDescriptor descriptor = ((AdvancedWorkflow) transientVars.get("descriptor"))
					.getWorkflowDescriptor();
			StepDescriptor step = descriptor.getStep(nextStep);

			if (step == null) {
				throw new WorkflowException("step #" + nextStep + " does not exist");
			}

			List preFunctions = step.getPreFunctions();

			for (Iterator iterator = preFunctions.iterator(); iterator.hasNext();) {
				FunctionDescriptor function = (FunctionDescriptor) iterator.next();
				executeFunction(function, transientVars, ps);
			}

			return newStep;
		} catch (WorkflowException e) {
			context.setRollbackOnly();
			throw e;
		}
	}

	/**
	 * "long id" o Id da Instncia.
	 */

	@Override
	public void doAction(long id, int actionId, Map inputs) throws WorkflowException {
		WorkflowInstance entry = new WorkflowInstance(id, user);
		AdvancedWorkflowStore store = new AdvancedWorkflowStore(entry.getWorkflow(user), user);

		AdvancedPropertySet aps = new AdvancedPropertySet(entry.getId(), user);
		/*
		 * if (aps.getString("" + actionId) != null && aps.getString("" +
		 * actionId).equals("Done")) { LogUtil.info(
		 * "Action j foi executada antes."); return; }
		 */
		if (entry.getState() != WorkflowEntry.ACTIVATED) {
			LogUtil.info("Esta instancia de workflow nao esta ativa.", user);
			return;
		}
		LogUtil.debug("A instancia esta ativa.", user);
		List currentSteps = store.findCurrentSteps(id);
		ActionDescriptor action = null;

		PropertySet ps = store.getPropertySet(id);
		Map transientVars = new HashMap();

		if (inputs != null) {
			transientVars.put(Constants.VOKey, inputs);
		}

		populateTransientMap(entry, transientVars, workflowDescriptor.getRegisters(), new Integer(actionId),
				currentSteps, ps);

		transientVars.put("user", user);

		boolean validAction = false;

		// check global actions
		for (Iterator gIter = workflowDescriptor.getGlobalActions().iterator(); !validAction && gIter.hasNext();) {
			ActionDescriptor actionDesc = (ActionDescriptor) gIter.next();

			if (actionDesc.getId() == actionId) {
				action = actionDesc;

				if (isActionAvailable(action, transientVars, ps, 0)) {
					validAction = true;
				}
			}
		}

		for (Iterator iter = currentSteps.iterator(); !validAction && iter.hasNext();) {
			Step step = (Step) iter.next();
			StepDescriptor s = workflowDescriptor.getStep(step.getStepId());

			for (Iterator iterator = s.getActions().iterator(); !validAction && iterator.hasNext();) {
				ActionDescriptor actionDesc = (ActionDescriptor) iterator.next();

				if (actionDesc.getId() == actionId) {
					action = actionDesc;

					if (isActionAvailable(action, transientVars, ps, s.getId())) {
						validAction = true;
					}
				}
			}
		}

		if (!validAction) {
			throw new InvalidActionException("Action " + actionId + " is invalid");
		}

		try {
			// transition the workflow, if it wasn't explicitly finished, check
			// for an implicit finish
			if (!transitionWorkflow(entry, currentSteps, store, workflowDescriptor, action, transientVars, inputs,
					ps)) {
				checkImplicitFinish(action, id);
			}

		} catch (WorkflowException e) {
			context.setRollbackOnly();
			throw e;
		}
	}

	protected void executeFunction(FunctionDescriptor function, Map transientVars, PropertySet ps)
			throws WorkflowException {
		if (function != null) {
			String type = function.getType();

			Map args = new HashMap(function.getArgs());

			for (Iterator iterator = args.entrySet().iterator(); iterator.hasNext();) {
				Map.Entry mapEntry = (Map.Entry) iterator.next();
				mapEntry.setValue(getConfiguration().getVariableResolver()
						.translateVariables((String) mapEntry.getValue(), transientVars, ps));
			}

			FunctionProvider provider = getResolver().getFunction(type, args);

			if (provider == null) {
				String message = "Could not load FunctionProvider class";
				context.setRollbackOnly();
				throw new WorkflowException(message);
			}

			try {
				provider.execute(transientVars, args, ps);
			} catch (WorkflowException e) {
				context.setRollbackOnly();
				throw e;
			}
		}
	}

	@Override
	public void executeTriggerFunction(long id, int triggerId) throws WorkflowException {
		WorkflowStore store = getPersistence();
		WorkflowInstance entry = new WorkflowInstance(id, user);

		if (entry == null) {
			LogUtil.warn("Cannot execute trigger #" + triggerId + " on non-existent workflow id#" + id, user);

			return;
		}

		WorkflowDescriptor wf = getConfiguration().getWorkflow(entry.getWorkflowName());

		PropertySet ps = store.getPropertySet(id);
		Map transientVars = new HashMap();
		populateTransientMap(entry, transientVars, wf.getRegisters(), null, store.findCurrentSteps(id), ps);
		executeFunction(wf.getTriggerFunction(triggerId), transientVars, ps);
	}

	public List findCurrentSteps(long entryId) throws StoreException {
		return new AdvancedStep().getCurrentSteps(id, user);
	}

	@Override
	public int[] getAvailableActions(long id) {
		return getAvailableActions(id, new HashMap());
	}

	/**
	 * Get the available actions for the specified workflow instance.
	 * 
	 * @ejb.interface-method
	 * @param id
	 *            The workflow instance id.
	 * @param inputs
	 *            The inputs map to pass on to conditions
	 * @return An array of action id's that can be performed on the specified
	 *         entry.
	 * @throws IllegalArgumentException
	 *             if the specified id does not exist, or if its workflow
	 *             descriptor is no longer available or has become invalid.
	 */

	@Override
	public int[] getAvailableActions(long id, Map inputs) {
		try {

			WorkflowInstance entry = new WorkflowInstance(id, user);
			AdvancedWorkflowStore store = new AdvancedWorkflowStore(entry.getWorkflow(user), user);
			// LogUtil.debug("yoho!");

			if (entry == null) {
				throw new IllegalArgumentException("No such workflow instance/entry with id " + id);
			}

			if (entry.getState() != WorkflowEntry.ACTIVATED) {
				return new int[0];
			}

			WorkflowDescriptor wf = this.getWorkflowDescriptor();

			if (wf == null) {
				throw new IllegalArgumentException("Workflow " + entry.getWorkflowName() + " no existe.");
			}

			List l = new ArrayList();
			PropertySet ps = store.getPropertySet(id);
			Map transientVars = new HashMap();
			Collection currentSteps = store.findCurrentSteps(id);

			populateTransientMap(entry, transientVars, wf.getRegisters(), new Integer(0), currentSteps, ps);

			transientVars.put(Constants.VOKey, inputs);
			transientVars.put("user", user);

			// get global actions
			List globalActions = wf.getGlobalActions();

			for (Iterator iterator = globalActions.iterator(); iterator.hasNext();) {

				ActionDescriptor action = (ActionDescriptor) iterator.next();
				RestrictionDescriptor restriction = action.getRestriction();
				ConditionsDescriptor conditions = null;

				transientVars.put("actionId", new Integer(action.getId()));

				if (restriction != null) {
					conditions = restriction.getConditionsDescriptor();
				}

				// todo verify that 0 is the right currentStepId
				if (passesConditions(wf.getGlobalConditions(), transientVars, ps, 0)
						&& passesConditions(conditions, transientVars, ps, 0)) {
					l.add(new Integer(action.getId()));
				}
			}

			// get normal actions
			for (Iterator iterator = currentSteps.iterator(); iterator.hasNext();) {
				Step step = (Step) iterator.next();
				l.addAll(getAvailableActionsForStep(wf, step, transientVars, ps));
			}

			int[] actions = new int[l.size()];

			for (int i = 0; i < actions.length; i++) {
				actions[i] = ((Integer) l.get(i)).intValue();
			}

			return actions;
		} catch (Exception e) {
			LogUtil.exception(e, user);
			return new int[0];
		}
	}

	protected List getAvailableActionsForStep(WorkflowDescriptor wf, Step step, Map transientVars, PropertySet ps)
			throws WorkflowException {

		List l = new ArrayList();
		StepDescriptor s = wf.getStep(step.getStepId());

		if (s == null) {
			LogUtil.warn("getAvailableActionsForStep called for non-existent step Id #" + step.getStepId(), user);

			return l;
		}
		LogUtil.debug("rresgatando acoes para o step > " + step.getStepId(), user);
		List actions = s.getActions();

		if (actions == null || actions.size() == 0) {
			return l;
		}

		for (Iterator iterator2 = actions.iterator(); iterator2.hasNext();) {
			ActionDescriptor action = (ActionDescriptor) iterator2.next();
			RestrictionDescriptor restriction = action.getRestriction();
			ConditionsDescriptor conditions = null;

			transientVars.put("actionId", new Integer(action.getId()));

			if (restriction != null) {
				conditions = restriction.getConditionsDescriptor();
			}

			if (passesConditions(wf.getGlobalConditions(), // provavelmente
															// feito para
					// inicair ou nao o workflow
					// vamos ver como fica depois
					// com o que ja esta feito
					new GenericVO().putAllAndGet(transientVars, true), ps, s.getId())
					&& passesConditions(conditions, new GenericVO().putAllAndGet(transientVars, true), ps,
							s.getId())) {
				l.add(new Integer(action.getId()));
			}
		}
		LogUtil.debug(l.size() + "  acoes resgatadaspara o step > " + step.getStepId(), user);

		return l;
	}

	protected int[] getAvailableAutoActions(long id, Map inputs) {
		try {
			WorkflowStore store = getPersistence();
			WorkflowEntry entry = store.findEntry(id);
			if (entry == null) {
				throw new IllegalArgumentException("No such workflow id " + id);
			}
			if (entry.getState() != WorkflowEntry.ACTIVATED) {
				LogUtil.debug("--> state is " + entry.getState(), user);
				return new int[0];
			}

			WorkflowDescriptor wf = ((WorkflowInstance) entry).getWorkflow(user).getWorkflowDescriptor();

			if (wf == null) {
				throw new IllegalArgumentException("No such workflow " + entry.getWorkflowName());
			}

			List l = new ArrayList();
			PropertySet ps = store.getPropertySet(id);
			Map transientVars = new HashMap();
			transientVars.put(Constants.VOKey, inputs);
			Collection currentSteps = store.findCurrentSteps(id);

			populateTransientMap(entry, transientVars, wf.getRegisters(), new Integer(0), currentSteps, ps);

			// get global actions
			List globalActions = wf.getGlobalActions();

			for (Iterator iterator = globalActions.iterator(); iterator.hasNext();) {
				ActionDescriptor action = (ActionDescriptor) iterator.next();

				transientVars.put("actionId", new Integer(action.getId()));

				if (action.getAutoExecute()) {
					if (isActionAvailable(action, transientVars, ps, 0)) {
						l.add(new Integer(action.getId()));
					}
				}
			}

			// get normal actions
			for (Iterator iterator = currentSteps.iterator(); iterator.hasNext();) {
				Step step = (Step) iterator.next();
				l.addAll(getAvailableActionsForStep(wf, step, transientVars, ps));

			}

			int[] actions = new int[l.size()];

			for (int i = 0; i < actions.length; i++) {
				actions[i] = ((Integer) l.get(i)).intValue();
			}

			return actions;
		} catch (Exception e) {
			LogUtil.exception(e, user);
			LogUtil.fatal("Error checking available actions");

			return new int[0];
		}
	}

	/**
	 * Get the configuration for this workflow. This method also checks if the
	 * configuration has been initialized, and if not, initializes it.
	 * 
	 * @return The configuration that was set. If no configuration was set, then
	 *         the default (static) configuration is returned.
	 * 
	 */
	public WorkflowConfiguration getConfiguration() {

		if (configuration == null) {

			WorkflowConfiguration wc = new WorkflowConfiguration();

			AdvancedWorkflowStore ws = new AdvancedWorkflowStore(user);

			try {
				ws.init(user);

			} catch (StoreException e) {
				LogUtil.exception(e, user);
			}

			JDBCWorkflowFactory fac = new JDBCWorkflowFactory();
			fac.init(user);
			wc.setFactory(fac);
			wc.setStore(ws);
			wc.setUser(user);

			setConfiguration(wc);

			// Configs.put(user.getConnectionRoute(), wc);

		}

		return this.configuration;

	}

	@Deprecated
	public Configuration getConfiguration(Usuario user) {
		return getConfiguration();
	}

	private Step getCurrentStep(WorkflowDescriptor wfDesc, int actionId, List currentSteps, Map transientVars,
			PropertySet ps) throws WorkflowException {
		if (currentSteps.size() == 1) {
			return (Step) currentSteps.get(0);
		}

		for (Iterator iterator = currentSteps.iterator(); iterator.hasNext();) {
			Step step = (Step) iterator.next();
			ActionDescriptor action = wfDesc.getStep(step.getStepId()).getAction(actionId);

			// $AR init
			if (isActionAvailable(action, transientVars, ps, step.getStepId())) {
				return step;
			}

			// $AR end
		}

		return null;
	}

	@Override
	public List getCurrentSteps(long id) {

		if (this.currentSteps == null) {
			try {
				this.currentSteps = findCurrentSteps(id);
			} catch (StoreException e) {
				LogUtil.exception(e, user);
			}
		}

		return currentSteps;
	}

	@Override
	public int getEntryState(long id) {

		GenericService s = (GenericService) user.getFactory().getSpringBean("WorkFlowInstanceService", user);

		GenericVO vo = s.createVO(user);

		try {

			vo.put("ID_WORKFLOW_INSTANCE", id);
			vo = s.obtem(vo, user);

		} catch (StoreException e) {
			LogUtil.exception(e, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		return (Integer) vo.get("STATE");
	}

	@Override
	public List getHistorySteps(long id) {

		return null;
	}

	/*
	 * public Configuration getConfiguration() { return getConfiguration(); }
	 */

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	protected WorkflowStore getPersistence() throws StoreException {
		if (this.workflowDescriptor == null) {
			this.workflowDescriptor = getWorkflowDescriptor();
		}
		return new AdvancedWorkflowStore(this, this.user);
	}

	@Override
	public PropertySet getPropertySet(long id) {
		LogUtil.fatal("AdvancedWorkflow.getPropertySet no foi implementado!");
		return null;
	}

	public TypeResolver getResolver() {
		if (typeResolver == null) {
			typeResolver = TypeResolver.getResolver();
		}

		return typeResolver;
	}

	@Override
	public List getSecurityPermissions(long id) {
		LogUtil.fatal("AdvancedWorkflow.getSecurityPermissions no foi implementado!");
		return null;
	}

	@Override
	public List getSecurityPermissions(long id, Map inputs) {
		LogUtil.fatal("AdvancedWorkflow.getSecurityPermissions no foi implementado!");
		return null;
	}

	public Usuario getUser() {
		return user;
	}

	public WorkflowDescriptor getWorkflowDescriptor() {
		WorkflowDescriptor wfd = getWorkflowDescriptor(getNome());
		if (wfd != null) {
			return wfd;
		} else {
			LogUtil.fatal("Nao foi possivel carregar o Workflow Descriptor.");
			return null;
		}
	}

	@Override
	public WorkflowDescriptor getWorkflowDescriptor(String workflowName) {
		try {

			return getConfiguration().getWorkflow(getNome());

		} catch (FactoryException e) {
			LogUtil.exception(e, user);
			LogUtil.fatal("Error loading workflow descriptor with name: " + workflowName);
			// new
			// BusinessException("Descritor do Workflow invalido, verifique o
			// mesmo.");
		}

		return null;
	}

	private WorkflowDescriptor getWorkflowDescriptorForAction(ActionDescriptor action) {
		AbstractDescriptor objWfd = action;

		while (!(objWfd instanceof WorkflowDescriptor)) {
			objWfd = objWfd.getParent();
		}

		WorkflowDescriptor wf = (WorkflowDescriptor) objWfd;

		return wf;
	}

	@Override
	public String getWorkflowName(long id) {
		LogUtil.fatal("AdvancedWorkflow.getWorkflowName no foi implementado! Tentou carregar o nome usando ID=" + id);
		return null;
	}

	@Override
	public String[] getWorkflowNames() {
		LogUtil.fatal("AdvancedWorkflow.getWorkflowNames no foi implementado!");
		return null;
	}

	public long initialize(int initialAction, Map inputs)
			throws InvalidRoleException, InvalidInputException, WorkflowException {
		return initialize(getNome(), initialAction, inputs);
	}

	@Override
	public long initialize(String workflowName, int initialAction, Map inputs)
			throws InvalidRoleException, InvalidInputException, WorkflowException {
		WorkflowDescriptor wf = getWorkflowDescriptor();

		WorkflowStore store = getPersistence();
		WorkflowEntry entry = store.createEntry(workflowName);

		/*
		 * INICIO
		 * 
		 * Essa parte insere o Objeto na tabela de instancias, esse cdigo tem
		 * que sair daqui e o relacionamento Instancia -> Objeto deve ser feito
		 * com as PropertySets
		 */

		GenericService s = (GenericService) user.getFactory().getSpringBean("WorkFlowInstanceService", user);

		GenericVO vo = s.createVO(user);

		vo.put("ID_WORKFLOW_INSTANCE", entry.getId());
		vo.put("ID_WORKFLOW", this.getId());

		try {
			vo = s.obtem(vo, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		vo.put("OBJETO", inputs.get("OBJETO"));

		String result = "";
		vo.setMethod("altera");
		vo.setProcess(false);
		vo.setAnulate(false);
		try {
			result = s.altera(vo, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		/* FIM */

		// start with a memory property set, but clone it after we have an ID
		PropertySet ps = store.getPropertySet(entry.getId());
		Map transientVars = new HashMap();

		if (inputs != null) {
			transientVars.putAll(inputs);
		}

		populateTransientMap(entry, transientVars, wf.getRegisters(), new Integer(initialAction),
				Collections.EMPTY_LIST, ps);

		transientVars.put("user", user);

		if (!canInitialize(workflowName, initialAction, transientVars, ps)) {
			context.setRollbackOnly();
			throw new InvalidRoleException("You are restricted from initializing this workflow");
		}

		ActionDescriptor action = wf.getInitialAction(initialAction);

		try {
			transitionWorkflow(entry, Collections.EMPTY_LIST, store, wf, action, transientVars, inputs, ps);
		} catch (WorkflowException e) {
			context.setRollbackOnly();
			throw e;
		}

		long entryId = entry.getId();

		// now clone the memory PS to the real PS
		// PropertySetManager.clone(ps, store.getPropertySet(entryId));
		return entryId;
	}

	private boolean isActionAvailable(ActionDescriptor action, Map transientVars, PropertySet ps, int stepId)
			throws WorkflowException {
		if (action == null) {
			return false;
		}

		WorkflowDescriptor wf = getWorkflowDescriptorForAction(action);

		Map cache = (Map) stateCache.get();

		Boolean result = null;

		if (cache != null) {
			result = (Boolean) cache.get(action);
		} else {
			cache = new HashMap();
			stateCache.set(cache);
		}

		if (result == null) {
			RestrictionDescriptor restriction = action.getRestriction();
			ConditionsDescriptor conditions = null;

			if (restriction != null) {
				conditions = restriction.getConditionsDescriptor();
			}

			result = new Boolean(passesConditions(wf.getGlobalConditions(), new HashMap(transientVars), ps, stepId)
					&& passesConditions(conditions, new HashMap(transientVars), ps, stepId));
			cache.put(action, result);
		}

		return result.booleanValue();
	}

	protected boolean passesCondition(ConditionDescriptor conditionDesc, Map transientVars, PropertySet ps,
			int currentStepId) throws WorkflowException {

		// transientVars.put("user", user);

		String type = conditionDesc.getType();

		Map args = new HashMap(conditionDesc.getArgs());

		for (Iterator iterator = args.entrySet().iterator(); iterator.hasNext();) {
			Map.Entry mapEntry = (Map.Entry) iterator.next();
			mapEntry.setValue(getConfiguration().getVariableResolver().translateVariables((String) mapEntry.getValue(),
					transientVars, ps));
		}

		if (currentStepId != -1) {
			Object stepId = args.get("stepId");

			if (stepId != null && stepId.equals("-1")) {
				args.put("stepId", String.valueOf(currentStepId));
			}
		}

		Condition condition = getResolver().getCondition(type, args);

		if (condition == null) {
			context.setRollbackOnly();
			throw new WorkflowException("Could not load condition");
		}

		if (this.getWorkflowDescriptor() != null && this.getWorkflowDescriptor().getMetaAttributes() != null
				&& this.getWorkflowDescriptor().getMetaAttributes().get("SRC") != null) {

			args.put("src", this.getWorkflowDescriptor().getMetaAttributes().get("SRC"));

		}

		try {

			boolean passed = condition.passesCondition(transientVars, args, ps);

			if (conditionDesc.isNegate()) {
				passed = !passed;
			}

			return passed;
		} catch (Exception e) {
			context.setRollbackOnly();

			if (e instanceof WorkflowException) {
				throw (WorkflowException) e;
			}

			throw new WorkflowException("Unknown exception encountered when checking condition " + condition, e);
		}
	}

	protected boolean passesConditions(ConditionsDescriptor descriptor, Map transientVars, PropertySet ps,
			int currentStepId) throws WorkflowException {
		if (descriptor == null) {
			return true;
		}

		transientVars.put("user", user);

		return passesConditions(descriptor.getType(), descriptor.getConditions(), transientVars, ps, currentStepId);
	}

	protected boolean passesConditions(String conditionType, List conditions, Map transientVars, PropertySet ps,
			int currentStepId) throws WorkflowException {

		// transientVars.put("user", user);

		if (conditions == null || conditions.size() == 0) {
			return true;
		}

		boolean and = "AND".equals(conditionType);
		boolean or = !and;

		for (Iterator iterator = conditions.iterator(); iterator.hasNext();) {
			AbstractDescriptor descriptor = (AbstractDescriptor) iterator.next();
			boolean result;

			if (descriptor instanceof ConditionsDescriptor) {
				ConditionsDescriptor conditionsDescriptor = (ConditionsDescriptor) descriptor;
				result = passesConditions(conditionsDescriptor.getType(), conditionsDescriptor.getConditions(),
						transientVars, ps, currentStepId);
			} else {
				result = passesCondition((ConditionDescriptor) descriptor, transientVars, ps, currentStepId);
			}

			if (and && !result) {
				return false;
			} else if (or && result) {
				return true;
			}
		}

		if (and) {
			return true;
		} else if (or) {
			return false;
		} else {
			return false;
		}
	}

	protected void populateTransientMap(WorkflowEntry entry, Map transientVars, List registers, Integer actionId,
			Collection currentSteps, PropertySet ps) throws WorkflowException {

		WorkflowInstance wfi = (WorkflowInstance) entry;

		transientVars.put("user", user);
		transientVars.put("context", context);
		transientVars.put("entry", entry);
		transientVars.put("store", getPersistence());
		transientVars.put("configuration", getConfiguration());
		transientVars.put("descriptor", wfi.getWorkflow(user));

		if (actionId != null) {
			transientVars.put("actionId", actionId);
		}

		if (transientVars.get(Constants.VOKey) != null
				&& ((Map) transientVars.get(Constants.VOKey)).get("ID_WORKFLOW_INSTANCE") != null) {
			// carregando as decises da instncia do workflow do histrico de
			// passos ...
			GenericService hsServ = (GenericService) getService("WorkFlowHistoryStepService", user);
			GenericVO hsVO = hsServ.createVO(user);
			hsVO.putKey("ID_WORKFLOW_INSTANCE", ((Map) transientVars.get(Constants.VOKey)).get("ID_WORKFLOW_INSTANCE"));
			int count = 0;

			try {
				List<GenericVO> histSteps = hsServ.lista(hsVO, user);
				for (GenericVO gVO : histSteps) {
					if (gVO.get("DECISAO") != null) {
						((Map) transientVars.get("VO")).put("DECISAO_" + count,
								(Map) user.getUtil().JSONtoMap(gVO.get("DECISAO").toString()));
						count++;
					}
				}
				// carregando a decisao do passo atual.
				hsServ = (GenericService) getService("WorkFlowCurrentStepService", user);
				hsVO = hsServ.createVO(user);
				hsVO.putKey("ID_WORKFLOW_INSTANCE",
						((Map) transientVars.get(Constants.VOKey)).get("ID_WORKFLOW_INSTANCE"));

				hsVO = hsServ.obtem(hsVO, user);
				if (hsVO.get("DECISAO") != null && !hsVO.get("DECISAO").toString().isEmpty()) {
					((Map) transientVars.get("VO")).put("DECISAO_ATUAL",
							(Map) user.getUtil().JSONtoMap(hsServ.obtem(hsVO, user).get("DECISAO").toString()));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		transientVars.put("currentSteps", new ArrayList(currentSteps));
		// now talk to the registers for any extra objects needed in scope
		for (Iterator iterator = registers.iterator(); iterator.hasNext();) {
			RegisterDescriptor register = (RegisterDescriptor) iterator.next();
			Map args = register.getArgs();

			String type = register.getType();
			Register r = getResolver().getRegister(type, args);

			if (r == null) {
				String message = "Could not load register class";
				context.setRollbackOnly();
				throw new WorkflowException(message);
			}

			try {
				transientVars.put(register.getVariableName(), r.registerVariable(context, entry, args, ps));
			} catch (Exception e) {
				context.setRollbackOnly();

				if (e instanceof WorkflowException) {
					throw (WorkflowException) e;
				}

				throw new WorkflowException(
						"An unknown exception occured while registering variable using register " + r, e);
			}
		}
	}

	@Override
	public List query(WorkflowExpressionQuery query) throws WorkflowException {
		return getPersistence().query(query);
	}

	@Override
	public List query(WorkflowQuery query) throws StoreException {
		return getPersistence().query(query);
	}

	@Override
	public boolean removeWorkflowDescriptor(String workflowName) throws FactoryException {
		return getConfiguration().removeWorkflow(workflowName);
	}

	@Override
	public boolean saveWorkflowDescriptor(String workflowName, WorkflowDescriptor descriptor, boolean replace)
			throws FactoryException {
		boolean success = getConfiguration().saveWorkflow(workflowName, descriptor, replace);

		return success;
	}

	@Override
	public void setConfiguration(Configuration configuration) {
		this.configuration = (WorkflowConfiguration) configuration;
	}

	public void setConfiguration(WorkflowConfiguration configuration) {
		this.configuration = configuration;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

	protected boolean transitionWorkflow(WorkflowEntry entry, List currentSteps, WorkflowStore store,
			WorkflowDescriptor wf, ActionDescriptor action, Map transientVars, Map inputs, PropertySet ps)
			throws WorkflowException {

		transientVars.put("user", user);

		Map cache = (Map) stateCache.get();

		if (cache != null) {
			cache.clear();
		} else {
			stateCache.set(new HashMap());
		}

		Step step = getCurrentStep(wf, action.getId(), currentSteps, transientVars, ps);

		if (action.getValidators().size() > 0) {
			verifyInputs(entry, action.getValidators(), Collections.unmodifiableMap(transientVars), ps);
		}

		// we're leaving the current step, so let's execute its post-functions
		// check if we actually have a current step
		if (step != null) {
			List stepPostFunctions = wf.getStep(step.getStepId()).getPostFunctions();

			for (Iterator iterator = stepPostFunctions.iterator(); iterator.hasNext();) {
				FunctionDescriptor function = (FunctionDescriptor) iterator.next();
				executeFunction(function, transientVars, ps);
			}
		}

		// preFunctions
		List preFunctions = action.getPreFunctions();

		for (Iterator iterator = preFunctions.iterator(); iterator.hasNext();) {
			FunctionDescriptor function = (FunctionDescriptor) iterator.next();
			executeFunction(function, transientVars, ps);
		}

		// check each conditional result
		List conditionalResults = action.getConditionalResults();
		List extraPreFunctions = null;
		List extraPostFunctions = null;
		ResultDescriptor[] theResults = new ResultDescriptor[1];

		for (Iterator iterator = conditionalResults.iterator(); iterator.hasNext();) {
			ConditionalResultDescriptor conditionalResult = (ConditionalResultDescriptor) iterator.next();

			if (passesConditions(null, conditionalResult.getConditions(), Collections.unmodifiableMap(transientVars),
					ps, step != null ? step.getStepId() : -1)) {
				// if (evaluateExpression(conditionalResult.getCondition(),
				// entry, wf.getRegisters(), null, transientVars)) {
				theResults[0] = conditionalResult;

				if (conditionalResult.getValidators().size() > 0) {
					verifyInputs(entry, conditionalResult.getValidators(), Collections.unmodifiableMap(transientVars),
							ps);
				}

				extraPreFunctions = conditionalResult.getPreFunctions();
				extraPostFunctions = conditionalResult.getPostFunctions();

				break;
			}
		}

		// use unconditional-result if a condition hasn't been met
		if (theResults[0] == null) {
			theResults[0] = action.getUnconditionalResult();
			verifyInputs(entry, theResults[0].getValidators(), Collections.unmodifiableMap(transientVars), ps);
			extraPreFunctions = theResults[0].getPreFunctions();
			extraPostFunctions = theResults[0].getPostFunctions();
		}

		LogUtil.debug("theResult=" + theResults[0].getStep() + ' ' + theResults[0].getStatus(), user);

		if (extraPreFunctions != null && extraPreFunctions.size() > 0) {
			// run any extra pre-functions that haven't been run already
			for (Iterator iterator = extraPreFunctions.iterator(); iterator.hasNext();) {
				FunctionDescriptor function = (FunctionDescriptor) iterator.next();
				executeFunction(function, transientVars, ps);
			}
		}

		// go to next step
		if (theResults[0].getSplit() != 0) {
			// the result is a split request, handle it correctly
			SplitDescriptor splitDesc = wf.getSplit(theResults[0].getSplit());
			Collection results = splitDesc.getResults();
			List splitPreFunctions = new ArrayList();
			List splitPostFunctions = new ArrayList();

			// check all results in the split and verify the input against any
			// validators specified
			// also build up all the pre and post functions that should be
			// called.
			for (Iterator iterator = results.iterator(); iterator.hasNext();) {
				ResultDescriptor resultDescriptor = (ResultDescriptor) iterator.next();

				if (resultDescriptor.getValidators().size() > 0) {
					verifyInputs(entry, resultDescriptor.getValidators(), Collections.unmodifiableMap(transientVars),
							ps);
				}

				splitPreFunctions.addAll(resultDescriptor.getPreFunctions());
				splitPostFunctions.addAll(resultDescriptor.getPostFunctions());
			}

			// now execute the pre-functions
			for (Iterator iterator = splitPreFunctions.iterator(); iterator.hasNext();) {
				FunctionDescriptor function = (FunctionDescriptor) iterator.next();
				executeFunction(function, transientVars, ps);
			}

			if (!action.isFinish()) {
				// now make these steps...
				boolean moveFirst = true;

				theResults = new ResultDescriptor[results.size()];
				results.toArray(theResults);

				for (Iterator iterator = results.iterator(); iterator.hasNext();) {
					ResultDescriptor resultDescriptor = (ResultDescriptor) iterator.next();
					Step moveToHistoryStep = null;

					if (moveFirst) {
						moveToHistoryStep = step;
					}

					long[] previousIds = null;

					if (step != null) {
						previousIds = new long[] { step.getId() };
					}

					createNewCurrentStep(resultDescriptor, entry, store, action.getId(), moveToHistoryStep, previousIds,
							transientVars, ps);
					moveFirst = false;
				}
			}

			// now execute the post-functions
			for (Iterator iterator = splitPostFunctions.iterator(); iterator.hasNext();) {
				FunctionDescriptor function = (FunctionDescriptor) iterator.next();
				executeFunction(function, transientVars, ps);
			}
		} else if (theResults[0].getJoin() != 0) {
			// this is a join, finish this step...
			JoinDescriptor joinDesc = wf.getJoin(theResults[0].getJoin());
			step = store.markFinished(step, action.getId(), new Date(), theResults[0].getOldStatus(),
					context.getCaller());
			store.moveToHistory(step);

			// ... now check to see if the expression evaluates
			// (get only current steps that have a result to this join)
			Collection joinSteps = new ArrayList();
			joinSteps.add(step);

			// currentSteps = store.findCurrentSteps(id); // shouldn't need to
			// refresh the list
			for (Iterator iterator = currentSteps.iterator(); iterator.hasNext();) {
				Step currentStep = (Step) iterator.next();

				if (currentStep.getId() != step.getId()) {
					StepDescriptor stepDesc = wf.getStep(currentStep.getStepId());

					if (stepDesc.resultsInJoin(theResults[0].getJoin())) {
						joinSteps.add(currentStep);
					}
				}
			}

			// we also need to check history steps that were finished before
			// this one
			// that might be part of the join
			List historySteps = store.findHistorySteps(entry.getId());

			for (Iterator i = historySteps.iterator(); i.hasNext();) {
				Step historyStep = (Step) i.next();

				if (historyStep.getId() != step.getId()) {
					StepDescriptor stepDesc = wf.getStep(historyStep.getStepId());

					if (stepDesc.resultsInJoin(theResults[0].getJoin())) {
						joinSteps.add(historyStep);
					}
				}
			}

			JoinNodes jn = new JoinNodes(joinSteps);
			transientVars.put("jn", jn);

			// todo verify that 0 is the right value for currentstep here
			if (passesConditions(null, joinDesc.getConditions(), Collections.unmodifiableMap(transientVars), ps, 0)) {
				// move the rest without creating a new step ...
				ResultDescriptor joinresult = joinDesc.getResult();

				if (joinresult.getValidators().size() > 0) {
					verifyInputs(entry, joinresult.getValidators(), Collections.unmodifiableMap(transientVars), ps);
				}

				// now execute the pre-functions
				for (Iterator iterator = joinresult.getPreFunctions().iterator(); iterator.hasNext();) {
					FunctionDescriptor function = (FunctionDescriptor) iterator.next();
					executeFunction(function, transientVars, ps);
				}

				long[] previousIds = new long[joinSteps.size()];
				int i = 1;

				for (Iterator iterator = joinSteps.iterator(); iterator.hasNext();) {
					Step currentStep = (Step) iterator.next();

					if (currentStep.getId() != step.getId()) {
						// if this is already a history step (eg, for all join
						// steps completed prior to this one),
						// we don't move it, since it's already history.
						if (!historySteps.contains(currentStep)) {
							store.moveToHistory(currentStep);
						}

						previousIds[i] = currentStep.getId();
						i++;
					}
				}

				if (!action.isFinish()) {
					// ... now finish this step normally
					previousIds[0] = step.getId();
					theResults[0] = joinDesc.getResult();

					// we pass in null for the current step since we've already
					// moved it to history above
					createNewCurrentStep(joinDesc.getResult(), entry, store, action.getId(), null, previousIds,
							transientVars, ps);
				}

				// now execute the post-functions
				for (Iterator iterator = joinresult.getPostFunctions().iterator(); iterator.hasNext();) {
					FunctionDescriptor function = (FunctionDescriptor) iterator.next();
					executeFunction(function, transientVars, ps);
				}
			}
		} else {
			// normal finish, no splits or joins
			long[] previousIds = null;

			if (step != null) {
				previousIds = new long[] { step.getId() };
			}

			if (!action.isFinish()) {
				createNewCurrentStep(theResults[0], entry, store, action.getId(), step, previousIds, transientVars, ps);
			}
		}

		// postFunctions (BOTH)
		if (extraPostFunctions != null) {
			for (Iterator iterator = extraPostFunctions.iterator(); iterator.hasNext();) {
				FunctionDescriptor function = (FunctionDescriptor) iterator.next();
				executeFunction(function, transientVars, ps);
			}
		}

		List postFunctions = action.getPostFunctions();

		for (Iterator iterator = postFunctions.iterator(); iterator.hasNext();) {
			FunctionDescriptor function = (FunctionDescriptor) iterator.next();
			executeFunction(function, transientVars, ps);
		}

		// if executed action was an initial action then workflow is activated
		if (wf.getInitialAction(action.getId()) != null && entry.getState() != WorkflowEntry.ACTIVATED) {
			changeEntryState(entry.getId(), WorkflowEntry.ACTIVATED);
		}

		// if it's a finish action, then we halt
		if (action.isFinish()) {
			completeEntry(action, entry.getId(), getCurrentSteps(entry.getId()), WorkflowEntry.COMPLETED);

			return true;
		}

		// get available autoexec actions
		int[] availableAutoActions = getAvailableAutoActions(entry.getId(), inputs);

		// we perform the first autoaction that applies, not all of them.
		/*
		 * IGNORANDO ESSA PARTE TEMPORARIAMENTE if (availableAutoActions.length
		 * > 0) { doAction(entry.getId(), availableAutoActions[0], inputs); }
		 */
		return false;
	}

	/**
	 * Validates input against a list of ValidatorDescriptor objects.
	 * 
	 * @param entry
	 *            the workflow instance
	 * @param validators
	 *            the list of ValidatorDescriptors
	 * @param transientVars
	 *            the transientVars
	 * @param ps
	 *            the persistence variables
	 * @throws InvalidInputException
	 *             if the input is deemed invalid by any validator
	 */
	protected void verifyInputs(WorkflowEntry entry, List validators, Map transientVars, PropertySet ps)
			throws WorkflowException {
		for (Iterator iterator = validators.iterator(); iterator.hasNext();) {
			ValidatorDescriptor input = (ValidatorDescriptor) iterator.next();

			if (input != null) {
				String type = input.getType();
				HashMap args = new HashMap(input.getArgs());

				for (Iterator iterator2 = args.entrySet().iterator(); iterator2.hasNext();) {
					Map.Entry mapEntry = (Map.Entry) iterator2.next();
					mapEntry.setValue(getConfiguration().getVariableResolver()
							.translateVariables((String) mapEntry.getValue(), transientVars, ps));
				}

				Validator validator = getResolver().getValidator(type, args);

				if (validator == null) {
					String message = "Could not load validator class";
					context.setRollbackOnly();
					throw new WorkflowException(message);
				}

				try {
					validator.validate(transientVars, args, ps);
				} catch (InvalidInputException e) {
					throw e;
				} catch (Exception e) {
					context.setRollbackOnly();

					if (e instanceof WorkflowException) {
						throw (WorkflowException) e;
					}

					String message = "An unknown exception occured executing Validator " + validator;
					throw new WorkflowException(message, e);
				}
			}
		}
	}

}
