package com.aseg.util.workflow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;
import com.opensymphony.module.propertyset.PropertyException;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetSchema;

/**
 * @author Lucas Neves Martins
 * @email lmartins@aseg.com.br
 **/
// TODO Implementar esta classe!!!

public class AdvancedPropertySet extends ServiceImpl implements PropertySet {

	public static enum DataType {
		UNKNOWN, BOOLEAN, INTEGER, LONG, DOUBLE, STRING, TEXT, DATE, OBJECT, XML, DATA, PROPERTIES
	}

	/** Lista de tipos de dados suportados pela implementacao do PropertySet */
	private final ArrayList<Integer> typeSupport;

	/** valor do item. data = dados, informacao. data != dia no calendario */
	private final Map<String, Object> data;
	private final Map<String, DataType> type;
	private final Map<String, Boolean> isSettableItemMap;
	private final String globalKey;

	public AdvancedPropertySet(Long idWorkflowInstance, Usuario user) {

		data = new HashMap<String, Object>();
		type = new HashMap<String, DataType>();
		isSettableItemMap = new HashMap<String, Boolean>();

		typeSupport = new ArrayList<Integer>();
		typeSupport.add(DataType.BOOLEAN.ordinal());
		typeSupport.add(DataType.INTEGER.ordinal());
		typeSupport.add(DataType.LONG.ordinal());
		typeSupport.add(DataType.DOUBLE.ordinal());
		typeSupport.add(DataType.STRING.ordinal());
		typeSupport.add(DataType.TEXT.ordinal());
		typeSupport.add(DataType.DATE.ordinal());

		globalKey = "default";

		ServiceImpl ser = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowPropertySetService", user);

		GenericVO queryVo = ser.createVO(user);

		queryVo.setFiltro("{gA:[{c:{f:'GLOBAL_KEY',o:'=',v1:'" + globalKey + "'}}]}");

		try {
			// propertySet = ser.obtem(queryVo, user);
			List c = ser.lista(queryVo, user);

			if (c.size() > 0) {

				for (Iterator iterator = c.iterator(); iterator.hasNext();) {

					Map property = (Map) iterator.next();

					String itemKey = (String) property.get("ITEM_KEY");

					String itemTypeLabel = (String) property.get("ITEM_TYPE_LABEL");

					if (itemTypeLabel == null || itemTypeLabel.equals(null)) {
						itemTypeLabel = "String";
					}

					DataType dataType = stringToDataType(itemTypeLabel);

					type.put(itemKey, dataType);

					switch (dataType) {
					case BOOLEAN:
						data.put(itemKey, property.get("STRING_VALUE"));
						break;
					case INTEGER:
						data.put(itemKey, property.get("FLOAT_VALUE"));
						break;
					case LONG:
						data.put(itemKey, property.get("NUMBER_VALUE"));
						break;
					case DOUBLE:
						data.put(itemKey, property.get("NUMBER_VALUE"));
						break;
					case STRING:
						data.put(itemKey, property.get("STRING_VALUE"));
						break;
					case TEXT:
						data.put(itemKey, property.get("STRING_VALUE"));
						break;
					case DATE:
						data.put(itemKey, property.get("DATE_VALUE"));
						break;
					case OBJECT:
						data.put(itemKey, property.get("DATA_VALUE"));
						break;
					case XML:
						data.put(itemKey, property.get("DATA_VALUE"));
						break;
					case DATA:
						data.put(itemKey, property.get("DATA_VALUE"));
						break;
					case PROPERTIES:
						data.put(itemKey, property.get("DATA_VALUE"));
						break;
					default:
						break;
					}

				}

			}

		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

	}

	@Override
	public boolean exists(String arg0) throws PropertyException {
		if (data.containsKey(arg0)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object getAsActualType(String arg0) throws PropertyException {
		return getItemValue(arg0);
	}

	@Override
	public boolean getBoolean(String arg0) throws PropertyException {
		if (supportsType(DataType.BOOLEAN.ordinal())) {
			return (Boolean) getItemValue(arg0);
		} else {
			LogUtil.error("AdvancedPropertySet nao suporta 'Boolean'");
			return false;
		}
	}

	@Override
	public byte[] getData(String arg0) throws PropertyException {
		// Nao tem suporte!
		return null;
	}

	@Override
	public Date getDate(String arg0) throws PropertyException {
		if (supportsType(DataType.DATE.ordinal())) {
			return (Date) getItemValue(arg0);
		} else {
			LogUtil.error("AdvancedPropertySet nao suporta 'Date'");
			return null;
		}
	}

	@Override
	public double getDouble(String arg0) throws PropertyException {
		if (supportsType(DataType.DOUBLE.ordinal())) {
			return (Double) getItemValue(arg0);
		} else {
			LogUtil.error("AdvancedPropertySet nao suporta 'Double'");
			return 0;
		}
	}

	@Override
	public int getInt(String arg0) throws PropertyException {
		if (supportsType(DataType.INTEGER.ordinal())) {
			return (Integer) getItemValue(arg0);
		} else {
			LogUtil.error("AdvancedPropertySet nao suporta 'Integer'");
			return 0;
		}
	}

	public Object getItemValue(String itemKey) {
		// DataType itemType = type.get(itemKey);
		return data.get(itemKey);
	}

	@Override
	public Collection getKeys() throws PropertyException {
		return data.keySet();
	}

	@Override
	public Collection getKeys(int arg0) throws PropertyException {
		return getKeys();
	}

	@Override
	public Collection getKeys(String arg0) throws PropertyException {
		return getKeys();
	}

	@Override
	public Collection getKeys(String arg0, int arg1) throws PropertyException {
		return getKeys();
	}

	@Override
	public long getLong(String arg0) throws PropertyException {
		if (supportsType(DataType.LONG.ordinal())) {
			return (Long) getItemValue(arg0);
		} else {
			LogUtil.error("AdvancedPropertySet nao suporta 'Long'");
			return 0;
		}
	}

	@Override
	public Object getObject(String arg0) throws PropertyException {
		if (supportsType(DataType.OBJECT.ordinal())) {
			return getItemValue(arg0);
		} else {
			LogUtil.error("AdvancedPropertySet nao suporta 'Object'");
			return null;
		}
	}

	@Override
	public Properties getProperties(String arg0) throws PropertyException {
		if (supportsType(DataType.PROPERTIES.ordinal())) {
			return (Properties) getItemValue(arg0);
		} else {
			LogUtil.error("AdvancedPropertySet nao suporta 'Properties'");
			return null;
		}
	}

	@Override
	public PropertySetSchema getSchema() throws PropertyException {
		return null;
	}

	@Override
	public String getString(String arg0) throws PropertyException {
		if (supportsType(DataType.STRING.ordinal())) {
			return (String) getItemValue(arg0);
		} else {
			LogUtil.error("AdvancedPropertySet nao suporta 'String'");
			return null;
		}
	}

	@Override
	public String getText(String arg0) throws PropertyException {
		if (supportsType(DataType.TEXT.ordinal())) {
			return (String) getItemValue(arg0);
		} else {
			LogUtil.error("AdvancedPropertySet nao suporta 'Text'");
			return null;
		}
	}

	@Override
	public int getType(String arg0) throws PropertyException {
		return type.get(arg0).ordinal();
	}

	@Override
	public Document getXML(String arg0) throws PropertyException {
		if (supportsType(DataType.XML.ordinal())) {
			return (Document) data;
		} else {
			LogUtil.error("AdvancedPropertySet nao suporta 'XML'");
			return null;
		}
	}

	@Override
	public void init(Map arg0, Map arg1) {
	}

	@Override
	public boolean isSettable(String arg0) {
		Boolean isSettable = false;
		if (isSettableItemMap.get(arg0) != null) {
			isSettable = isSettableItemMap.get(arg0);
		}
		return isSettable;
	}

	@Override
	public void remove() throws PropertyException {
	}

	@Override
	public void remove(String arg0) throws PropertyException {
	}

	@Override
	public void setAsActualType(String arg0, Object arg1) throws PropertyException {
	}

	@Override
	public void setBoolean(String arg0, boolean arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.BOOLEAN.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	@Override
	public void setData(String arg0, byte[] arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.DATA.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	@Override
	public void setDate(String arg0, Date arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.DATE.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	@Override
	public void setDouble(String arg0, double arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.DOUBLE.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	@Override
	public void setInt(String arg0, int arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.INTEGER.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	@Override
	public void setLong(String arg0, long arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.LONG.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	@Override
	public void setObject(String arg0, Object arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.OBJECT.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	@Override
	public void setProperties(String arg0, Properties arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.PROPERTIES.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	@Override
	public void setSchema(PropertySetSchema arg0) throws PropertyException {
	}

	@Override
	public void setString(String arg0, String arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.STRING.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	@Override
	public void setText(String arg0, String arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.TEXT.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	@Override
	public void setXML(String arg0, Document arg1) throws PropertyException {
		if (this.isSettable(arg0) && supportsType(DataType.XML.ordinal())) {
			data.put(arg0, arg1);
		}
	}

	private DataType stringToDataType(String s) {
		return DataType.valueOf(s.toUpperCase());
	}

	@Override
	public boolean supportsType(int arg0) {
		if (typeSupport.contains(arg0)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean supportsTypes() {
		return true;
	}

}
