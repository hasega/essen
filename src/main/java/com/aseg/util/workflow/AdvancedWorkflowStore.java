package com.aseg.util.workflow;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.StoreException;
import com.opensymphony.workflow.query.WorkflowExpressionQuery;
import com.opensymphony.workflow.query.WorkflowQuery;
import com.opensymphony.workflow.spi.Step;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.opensymphony.workflow.spi.WorkflowStore;

/**
 * @author Lucas Neves Martins
 * @email lmartins@aseg.com.br
 **/
public class AdvancedWorkflowStore extends ServiceImpl implements WorkflowStore {

	protected String currentPrevTable;
	protected String currentTable;
	protected String entryId;
	protected String entryName;
	// protected String entrySequence;
	protected String entryState;
	protected String entryTable;
	protected String historyPrevTable;
	protected String historyTable;
	protected String stepActionId;
	protected String stepCaller;
	protected String stepDueDate;
	protected String stepEntryId;
	protected String stepFinishDate;
	protected String stepId;
	protected String stepOwner;
	protected String stepPreviousId;
	// protected String stepSequence;
	protected String stepStartDate;
	protected String stepStatus;
	protected String stepStepId;

	String init = null;
	Usuario user;
	AdvancedWorkflow workflow;

	public AdvancedWorkflowStore(AdvancedWorkflow workflow, Usuario user) {
		this.workflow = workflow;

		this.user = user;
		try {
			init(this.user);
		} catch (StoreException e) {
			// TODO jogar uma runtime exception?
			LogUtil.exception(e, user);
		}
	}

	public AdvancedWorkflowStore(Usuario user) {
		this.user = user;
	}

	@Override
	public Step createCurrentStep(long entryId, int wfStepId, String owner, Date startDate, Date dueDate, String status,
			long[] previousIds) throws StoreException {
		return createCurrentStep(entryId, wfStepId, owner, startDate, dueDate, status, previousIds, null);

	}

	public Step createCurrentStep(long entryId, int wfStepId, String owner, Date startDate, Date dueDate, String status,
			long[] previousIds, String decisao) throws StoreException {
		try {

			ServiceImpl ser = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowCurrentStepService",
					user);

			GenericVO v = ser.createVO(user);

			v.putKey("ID_WORKFLOW_INSTANCE_CURSTEP", null);
			v.putKey("ID_WORKFLOW_INSTANCE", entryId);
			v.putKey("ID_STEP", wfStepId);
			v.putKey("ID_ACTION", null);
			v.putKey("OWNER", owner);
			v.putKey("START_DATE", null);
			v.putKey("DUE_DATE", null);
			v.putKey("STATUS", status);
			v.putKey("CALLER", user.getLogin());
			if (decisao != null) {
				v.putKey("DECISAO", decisao);
			}
			// WorkflowEntry.CREATED

			Long id = null;

			try {
				// @TODO arrumar esse 100 aqui, a funcao tem que pegar a
				// initialAction pelo workflow descriptor!
				// id = wf.initialize(workflowName, 100, null);
				v.setLazyList(true);
				id = new Long(ser.inclui(v, user));

			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

			return new AdvancedStep(id, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
			throw new StoreException("Erro ao criar uma nova instancia de workflow", e);
		}
	}

	@Override
	public WorkflowEntry createEntry(String workflowName) throws StoreException {

		try {

			AdvancedWorkflow wf = new AdvancedWorkflow(workflowName, user);

			ServiceImpl ser = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowInstanceService", user);

			GenericVO v = ser.createVO(user);
			v.setAnulate(false);
			v.putKey("ID_WORKFLOW", wf.getId());
			v.putKey("STATE", WorkflowEntry.CREATED);
			v.putKey("OBJETO", wf.getCtx());

			Long id = null;

			try {
				v.setLazyList(true);
				id = new Long(ser.inclui(v, user));
			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

			return new WorkflowInstance(id, user);
		} catch (Exception e) {
			throw new StoreException("Erro ao criar uma nova instancia de workflow", e);
		}
	}

	private boolean exists(String name) {

		GenericService s = (GenericService) user.getFactory().getSpringBean("WorkFlowService", user);
		try {

			boolean exists = false;

			GenericVO vo = s.createVO(user);
			vo.putKey("NOME", name);
			Map ws = null;

			try {
				ws = s.obtem(vo, user);
				if (ws != null && !ws.isEmpty() && ws.get("NOME") != null) {
					exists = true;
				}
			} catch (Exception e) {
				LogUtil.exception(e, user);
				LogUtil.debug("Could not check if [" + name + "] exists", user);
			}

			return exists;

		} catch (Exception e) {
			LogUtil.exception(e, user);
			return false;
		}
	}

	public boolean exists(final String name, final Connection conn) {
		return exists(name);
	}

	@Override
	public List findCurrentSteps(long entryId) throws StoreException {

		GenericService s = (GenericService) user.getFactory().getSpringBean("WorkFlowCurrentStepService",
				user);

		List currentStepsVO = new ArrayList<Map>();
		List currentSteps = new ArrayList<AdvancedStep>();

		try {

			GenericVO vo = s.createVO(user);
			vo.putKey("ID_WORKFLOW_INSTANCE", entryId);

			try {
				currentStepsVO = s.lista(vo, user);

			} catch (Exception e) {
				LogUtil.exception(e, user);
				LogUtil.error("Nao foi possivel encontrar CurrentSteps da instancia[" + entryId + "]");
			}

			try {
				for (Iterator iterator = currentStepsVO.iterator(); iterator.hasNext();) {
					Map resultVO = (Map) iterator.next();

					Long stepId = (Long) resultVO.get("ID_WORKFLOW_INSTANCE_CURSTEP");

					AdvancedStep step = new AdvancedStep(stepId, user);

					currentSteps.add(step);
				}
			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

		} catch (Exception e) {
			LogUtil.exception(e, user);
			return null;
		}

		return currentSteps;
	}

	@Override
	public WorkflowEntry findEntry(long theEntryId) throws StoreException {
		WorkflowInstance entry = new WorkflowInstance(theEntryId, this.user);
		return entry;
	}

	@Override
	public List findHistorySteps(long entryId) throws StoreException {
		LogUtil.fatal("AdvancedWorkflowStore.findHistorySteps no foi implementado!");
		return null;
	}

	public String getInit() {
		return init;
	}

	private String getInitProperty(Map props, String strName, String strDefault) {
		Object o = props.get(strName);

		if (o == null) {
			return strDefault;
		}

		return (String) o;
	}

	@Override
	public PropertySet getPropertySet(final long entryId) {
		PropertySet ps = new AdvancedPropertySet(entryId, user);
		return ps;
	}

	private String getShoes() {
		return null;
	}

	public Usuario getUser() {
		return user;
	}

	@Override
	public void init(final Map arg0) throws StoreException {
		entryTable = getInitProperty(arg0, "entry.table", "OS_WFENTRY");
		entryId = getInitProperty(arg0, "entry.id", "ID");
		entryName = getInitProperty(arg0, "entry.name", Constants.name);
		entryState = getInitProperty(arg0, "entry.state", "STATE");
		historyTable = getInitProperty(arg0, "history.table", "OS_HISTORYSTEP");
		currentTable = getInitProperty(arg0, "current.table", "OS_CURRENTSTEP");
		currentPrevTable = getInitProperty(arg0, "currentPrev.table", "OS_CURRENTSTEP_PREV");
		historyPrevTable = getInitProperty(arg0, "historyPrev.table", "OS_HISTORYSTEP_PREV");
		stepId = getInitProperty(arg0, "step.id", "ID");
		stepEntryId = getInitProperty(arg0, "step.entryId", "ENTRY_ID");
		stepStepId = getInitProperty(arg0, "step.stepId", "STEP_ID");
		stepActionId = getInitProperty(arg0, "step.actionId", "ACTION_ID");
		stepOwner = getInitProperty(arg0, "step.owner", "OWNER");
		stepCaller = getInitProperty(arg0, "step.caller", "CALLER");
		stepStartDate = getInitProperty(arg0, "step.startDate", "START_DATE");
		stepFinishDate = getInitProperty(arg0, "step.finishDate", "FINISH_DATE");
		stepDueDate = getInitProperty(arg0, "step.dueDate", "DUE_DATE");
		stepStatus = getInitProperty(arg0, "step.status", "STATUS");
		stepPreviousId = getInitProperty(arg0, "step.previousId", "PREVIOUS_ID");
	}

	public void init(final Usuario user) throws StoreException {

		init(new HashMap());
		this.user = user;

		// START - define atributos de mapeamento da store
		this.entryId = "ID_WORKFLOW_INSTANCE";
		this.entryState = "STATE";
		this.entryTable = "M_WORKFLOW_INSTANCE";
		this.currentTable = "M_WORKFLOW_INSTANCE_CURSTEP";
		this.stepActionId = "ID_ACTION";
		this.stepId = "ID_WORKFLOW_INSTANCE_CURSTEP";
		this.stepStepId = "ID_STEP";
		this.stepEntryId = "ID_WORKFLOW_INSTANCE";
		// END

	}

	@Override
	public Step markFinished(Step step, int actionId, Date finishDate, String status, String caller)
			throws StoreException {

		try {

			ServiceImpl ser = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowCurrentStepService",
					user);
			GenericVO v = ser.createVO(user);
			v.putKey("ID_WORKFLOW_INSTANCE_CURSTEP", step.getId());
			v = ser.obtem(v, user);
			v.put("CALLER", caller);
			v.put("STATUS", status);
			v.put("FINISH_DATE", user.getUtil().df.dateToTimestamp(finishDate));
			v.put("ID_ACTION", actionId);
			v.setMethod("altera");
			v.setLazyList(true);
			String result = null;

			try {
				v.setProcess(false);
				result = ser.altera(v, user);
			} catch (Exception e) {
				LogUtil.error("Nao foi possivel alterar o CurrentStep [" + step.getId() + "");
				LogUtil.exception(e, user);
			}

			/*
			 * 
			 * SimpleStep theStep = (SimpleStep) step;
			 * theStep.setActionId(actionId); theStep.setFinishDate(finishDate);
			 * theStep.setStatus(status); theStep.setCaller(caller);
			 * 
			 * return theStep;
			 */

			return new AdvancedStep(step.getId(), user);
		} catch (Exception e) {
			throw new StoreException("Erro ao criar uma nova instancia de workflow", e);
		}

	}

	@Override
	public void moveToHistory(Step step) throws StoreException {
		// LogUtil.fatal("AdvancedWorkflowStore.moveToHistory no foi
		// implementado!");

		GenericService s = (GenericService) user.getFactory().getSpringBean("WorkFlowCurrentStepService",
				user);
		GenericService h = (GenericService) user.getFactory().getSpringBean("WorkFlowHistoryStepService",
				user);
		GenericVO vo = s.createVO(user);
		vo.putKey("ID_WORKFLOW_INSTANCE_CURSTEP", step.getId());

		try {
			h.inclui(h.createVO(user).putAllAndGet(s.obtem(vo, user), true), user);
		} catch (Exception e1) {
			LogUtil.exception(e1, user);
		}

		String result = "";

		try {
			result = s.remove(vo, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		LogUtil.info(result, user);

	}

	@Override
	public List query(WorkflowExpressionQuery query) throws StoreException {
		LogUtil.fatal("AdvancedWorkflowStore.query no foi implementado!");
		return null;
	}

	@Override
	public List query(WorkflowQuery query) throws StoreException {
		LogUtil.fatal("AdvancedWorkflowStore.query no foi implementado!");
		return null;
	}

	@Override
	public void setEntryState(long id, int state) throws StoreException {

		GenericService s = (GenericService) user.getFactory().getSpringBean("WorkFlowInstanceService", user);
		try {

			GenericVO vo = s.createVO(user);
			vo.put("ID_WORKFLOW_INSTANCE", id);
			vo = s.obtem(vo, user);

			vo.put("STATE", state);
			vo.setMethod("altera");
			vo.setLazyList(true);
			String retorno = s.altera(vo, user);

		} catch (StoreException e) {
			LogUtil.exception(e, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

	}

	public void setInit(final String init) {
		this.init = init;
	}

	public void setUser(Usuario user) {
		this.user = user;
	}

}
