package com.aseg.util.workflow.conditions;

import java.util.HashMap;
import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleScriptContext;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.service.UtilServiceImpl;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Condition;

public class EvaluateRhino implements Condition {
	public final UtilServiceImpl util = new UtilServiceImpl();

	private ScriptEngine engine = new ScriptEngineManager().getEngineByName("js");
	private ScriptContext scriptCtx = new SimpleScriptContext();

	@Override
	public boolean passesCondition(Map transientVars, Map args, PropertySet ps) {

		Object rhinoScript = args.get("script");
		Usuario user = (Usuario) transientVars.get("user");
		LogUtil.debug("################## RhinoScript - Workflow ########################", user);
		LogUtil.debug(rhinoScript, user);
		LogUtil.debug("################## RhinoScript - Workflow ########################", user);

		ParseXmlService s = (ParseXmlService) user.getSysParser();

		GenericService propertyService = (GenericService) user.getFactory().getSpringBean("WorkFlowPropertySetService",
				user);
		Bindings bds = scriptCtx.getBindings(ScriptContext.ENGINE_SCOPE);
		bds.put("user", user);
		bds.put("sm", s);
		bds.put("propertyService", propertyService);
		bds.put("VO", transientVars.get("VO"));
		bds.put("ID_WORKFLOW_INSTANCE", ((HashMap) transientVars.get("VO")).get("ID_WORKFLOW_INSTANCE"));
		bds.put("util", util);
		bds.put("df", util.df);
	//	bds.put("cmp", new TagInputExt());
		bds.put("result", new Object());

		boolean result = false;

		if (args.get("src") != null) {
			rhinoScript = args.get("src").toString() + "\n" + rhinoScript;
		}

		try {
			result = (Boolean) engine.eval((String) rhinoScript, scriptCtx);
		} catch (ScriptException e) {
			e.printStackTrace();
		}

		LogUtil.debug("RHINO" + util.clearForJSON((String) rhinoScript) + ">>>" + result, user);

		return result;

	}

}
