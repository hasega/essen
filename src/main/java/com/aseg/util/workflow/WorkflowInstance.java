package com.aseg.util.workflow;

import java.util.Map;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;
import com.opensymphony.workflow.spi.WorkflowEntry;

/**
 * @author Lucas Neves Martins
 * @email lmartins@aseg.com.br
 **/
public class WorkflowInstance extends ServiceImpl implements WorkflowEntry {

	private static final long serialVersionUID = 4944353186250221431L;

	protected AdvancedWorkflow workflow;

	protected int state;

	protected long id;

	protected long workflowId;

	protected String objeto;

	/**
	 * Ligacao com o Objeto relacionado a instancia. Ex: CO-146 => Contrato com
	 * ID = 146.
	 */
	public WorkflowInstance(final Long idInstance, Usuario user) {
		GenericService instanceService = (GenericService) user.getFactory()
				.getSpringBean("WorkFlowInstanceService", user);
		try {
			Map instanceMap = instanceService
					.obtem(instanceService.createVO(user).putKey("ID_WORKFLOW_INSTANCE", idInstance), user);

			id = idInstance.intValue();
			state = ((Long) instanceMap.get("STATE")).intValue();
			workflowId = (Long) instanceMap.get("ID_WORKFLOW");
			objeto = (String) instanceMap.get("OBJETO");

		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

	}

	@Override
	public long getId() {
		return id;
	}

	public String getObjeto() {

		String result = null;

		if (objeto != null) {
			result = objeto;
		} else {
			LogUtil.error("objeto da instancia  nulo");
		}

		return result;
	}

	@Override
	public int getState() {
		return state;
	}

	public AdvancedWorkflow getWorkflow(Usuario user) {
		if (workflow != null) {
			return workflow;
		} else {
			AdvancedWorkflow wf = new AdvancedWorkflow(workflowId, user);
			this.workflow = wf;
			return this.workflow;
		}
	}

	public AdvancedWorkflow getWorkflowForceRefresh() {
		return null;
	}

	public long getWorkflowId() {
		return workflowId;
	}

	@Override
	public String getWorkflowName() {
		return this.workflow.getNome();
	}

	@Override
	public boolean isInitialized() {
		return true;
	}

	public void setObject(String object) {
		if (object == null) {
			LogUtil.error("Object was null");
		} else {
			this.objeto = object;
		}
	}

	public String getAtctualStepStatus(Usuario user) {
		GenericService instanceService = (GenericService) user.getFactory()
				.getSpringBean("WorkFlowCurrentStepService", user);
		GenericVO vo =

				instanceService.createVO(user).putKey("ID_WORKFLOW_INSTANCE", new Long(getId()));
		vo.setFiltro("{gA:[{c:{f:'ID_WORKFLOW_INSTANCE_CURSTEP',o:'max'}}]}");
		try {
			Map instanceMap = instanceService.obtem(vo, user);

			return instanceMap.get("STATUS").toString();

		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		return "";

	}

}
