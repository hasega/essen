package com.aseg.util.workflow;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.xml.sax.SAXException;

import com.aseg.config.Constants;
import com.aseg.exceptions.BusinessException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FactoryException;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.opensymphony.workflow.loader.WorkflowLoader;
import com.opensymphony.workflow.loader.XMLWorkflowFactory;

/**
 * @author Lucas Neves Martins
 * @email lmartins@aseg.com.br
 **/
public class JDBCWorkflowFactory extends XMLWorkflowFactory implements FunctionProvider {
	public final UtilServiceImpl util = new UtilServiceImpl();

	class WfConfig {
		String wfName;
		WorkflowDescriptor descriptor;
		long lastModified;

		public WfConfig(String name) {
			wfName = name;
		}
	}

	/**
	 * 
	 */

	ServiceImpl WFService = null;
	Usuario user = null;

	private static final long serialVersionUID = 1L;

	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) {
		String name = (String) args.get(Constants.name);
		WorkflowDescriptor wfds = (WorkflowDescriptor) transientVars.get("descriptor");

		try {
			saveWorkflow(name, wfds, false);
		} catch (Exception e) {
		}
	}

	private boolean exists(String name, Connection conn) {
		boolean exists = false;

		GenericVO vo = WFService.createVO(user);
		vo.putKey("NOME", name);
		Map ws = null;
		try {
			ws = WFService.obtem(vo, user);
			if (ws != null && !ws.isEmpty()) {
				exists = true;
			}
		} catch (Exception e) {
			LogUtil.exception(e, user);
			LogUtil.debug("Could not check if [" + name + "] exists", user);
		}

		return exists;
	}

	@Override
	public WorkflowDescriptor getWorkflow(String name, boolean validate) throws FactoryException {

		if (name == null) {
			throw new RuntimeException("Workflow name is null");
		}

		WfConfig c = (WfConfig) workflows.get(name);

		if (c == null) {
			throw new RuntimeException("Unknown workflow name \"" + name + "\"");
		}

		LogUtil.debug("getWorkflow " + name + " descriptor=" + c.descriptor, user);

		if (c.descriptor != null) {
			if (reload) {
				// @todo check timestamp
				try {
					c.descriptor = load(c.wfName, validate);
				} catch (FactoryException e) {
					throw e;
				} catch (Exception e) {
					throw new FactoryException("Error reloading workflow", e);
				}
			}
		} else {
			try {
				c.descriptor = load(c.wfName, validate);
			} catch (FactoryException e) {
				throw e;
			} catch (Exception e) {
				throw new FactoryException("Error loading workflow", e);
			}
		}

		return c.descriptor;
	}

	@Override
	public String[] getWorkflowNames() {
		int i = 0;
		String[] res = new String[workflows.keySet().size()];
		Iterator it = workflows.keySet().iterator();

		while (it.hasNext()) {
			res[i++] = (String) it.next();
		}

		return res;
	}

	private void init() {
		workflows = new HashMap();
	}

	public void init(Usuario user) {
		this.user = user;
		this.WFService = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowService", user);
		init();
		try {
			initDone();
		} catch (FactoryException e) {
			LogUtil.exception(e, user);
		}

	}

	@Override
	public void initDone() throws FactoryException {

		try {
			init();
			reload = true;
			List ws = WFService.lista(WFService.createVO(user), user);
			for (Iterator iterator = ws.iterator(); iterator.hasNext();) {
				Map object = (Map) iterator.next();
				WfConfig config = new WfConfig((String) object.get("NOME"));
				workflows.put(object.get("NOME"), config);
			}

		} catch (Exception e) {
			throw new FactoryException("Could not read workflow names from datasource", e);
		}
	}

	protected WorkflowDescriptor load(final String wfName, boolean validate)
			throws IOException, SAXException, FactoryException {
		byte[] wf;

		try {
			wf = read(wfName);
		} catch (SQLException e) {
			throw new FactoryException("Error loading workflow:" + e, e);
		}

		ByteArrayInputStream is = new ByteArrayInputStream(wf);

		return WorkflowLoader.load(is, validate);
	}

	public byte[] read(String workflowname) throws SQLException {
		byte[] wf = new byte[0];
		GenericVO vo = WFService.createVO(user);
		vo.putKey("NOME", workflowname);
		Map ws = null;
		try {
			ws = WFService.obtem(vo, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		wf = (byte[]) ws.get("ARQUIVO_XML");
		return wf;
	}

	public GenericVO refresh(GenericVO vo, Usuario user) throws Exception, BusinessException {
		this.user = user;
		initDone();
		return vo;
	}

	@Override
	public boolean removeWorkflow(String name) throws FactoryException {
		boolean removed = false;
		try {
			workflows.remove(name); // deletar instancias
		} catch (Exception e) {
			LogUtil.exception(e, user);
			throw new FactoryException("Unable to remove workflow: " + e.toString(), e);
		}

		return true;
	}

	@Override
	public boolean saveWorkflow(String name, WorkflowDescriptor descriptor, boolean replace) throws FactoryException {
		WfConfig c = (WfConfig) workflows.get(name);
		if (c != null && !replace) {
			return false;
		}
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		Writer out = new OutputStreamWriter(bout);
		PrintWriter writer = new PrintWriter(out);
		writer.println(WorkflowDescriptor.XML_HEADER);
		writer.println(WorkflowDescriptor.DOCTYPE_DECL);
		descriptor.writeXML(writer, 0);
		writer.flush();
		writer.close();

		// @todo is a backup necessary?
		try {
			return write(name, bout.toByteArray());
		} catch (SQLException e) {
			throw new FactoryException("Unable to save workflow: " + e.toString(), e);
		} finally {
			WfConfig config = new WfConfig(name);
			workflows.put(name, config);
		}
	}

	// ~ Inner Classes
	// //////////////////////////////////////////////////////////

	public boolean write(String name, byte[] wf) throws SQLException {
		boolean written = false;
		Connection conn = null;

		try {
			GenericVO vo = WFService.createVO(user);
			vo.putKey("NOME", name);
			vo.put("ARQUIVO_XML", wf);
			if (exists(name, conn)) {
				WFService.altera(vo, user);
			} else {
				WFService.inclui(vo, user);
			}
			written = true;
		} catch (Exception e) {
			LogUtil.exception(e, user);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
				}
			}
		}

		return written;
	}

}
