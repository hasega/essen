package com.aseg.util.workflow;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptException;

import org.springframework.expression.spel.support.StandardEvaluationContext;

import com.aseg.config.Constants;
import com.aseg.email.service.EmailJob;
import com.aseg.email.service.EmailService;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.util.DataFormat;
import com.aseg.util.service.ParseXmlService;
import com.aseg.vo.GenericVO;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.WorkflowException;

/**
 * @author Lucas Neves Martins
 * @email lmartins@aseg.com.br
 * 
 * 
 * 
 **/
public class WorkflowCommunication implements FunctionProvider {

	private static WorkflowCommunication instance;

	public static WorkflowCommunication getInstance(Usuario user) {
		if (instance == null) {
			instance = new WorkflowCommunication();
		}
		return instance;
	}

	/**
	 * INFO : para avisos que no precisam de ateno especial do usurio.<br/>
	 * <br/>
	 * ALERT: para mensagens que precisam de ateno especial do usurio. <br/>
	 * <br/>
	 * ERROR: para avisos de problemas na execuo do workflow.<br/>
	 * <br/>
	 * BLOCKING: para aviso de uma situao que precisa de feedback do usurio. Ex:
	 * uma mensagem que avisa que um contrato precisa de aprovao do usurio.
	 */
	enum MessagePriorityLevel {
		INFO, ALERT, ERROR, BLOCKING
	}

	/**
	 * GUI/UI: Mensagem que deve ser mostrada pela interface do sistema. Ex:
	 * Painel de Controle<br/>
	 * EMAIL: Mensagem que deve ser enviada por email para o usurio<br/>
	 * 
	 */
	enum MessageType {
		GUI, EMAIL
	}

	public Object evalRhino(GenericVO vo, Object rhinoScript, Usuario user, Map transientVars, boolean requiredBool) {
		CompiledScript eng = null;
		String src = "";
		if (transientVars.get("ENTRY") != null) {
			WorkflowInstance entry = (WorkflowInstance) transientVars.get("ENTRY");
			AdvancedWorkflow ad = entry.getWorkflow(user);
			src = (String) ad.getWorkflowDescriptor().getMetaAttributes().get("SRC");
			transientVars.put("ID_WORKFLOW", ad.getId());
		} else if (transientVars.get("SRC") != null) {
			src = (String) transientVars.get("SRC");
		} else if (transientVars.get("descriptor") != null) {
			AdvancedWorkflow ad = (AdvancedWorkflow) transientVars.get("descriptor");
			transientVars.put("ID_WORKFLOW", ad.getId());
			src = (String) ad.getWorkflowDescriptor().getMetaAttributes().get("SRC");
		}

		try {
			eng = ((Compilable) user.getUtil().engine).compile(src + (String) rhinoScript);
		} catch (ScriptException e1) {
			LogUtil.exception(e1, user);
		}

		if (eng == null) {
			LogUtil.error("RhinoScript nao compilavel.");
			return null;
		}

		ParseXmlService s = (ParseXmlService) user.getSysParser();

		GenericService propertyService = (GenericService) user.getFactory()
				.getSpringBean("WorkFlowPropertySetService", user);

		final Bindings bindings = user.getUtil().engine.createBindings();
		bindings.put("user", user);
		bindings.put("sm", s);
		bindings.put("tvars", transientVars);
		bindings.put("propertyService", propertyService);
		bindings.put(Constants.VOKey, vo);
		bindings.put("util", user.getUtil());
		bindings.put("ID_WORKFLOW", vo.get("ID_WORKFLOW"));
		bindings.put("ID_WORKFLOW_INSTANCE", ((WorkflowInstance) transientVars.get("entry")).getId());
		bindings.put("df", new DataFormat());
		//bindings.put("cmp", new TagInputExt());
		Object result = new Object();
		bindings.put("result", result);
		Object r = null;

		try {
			r = eng.eval(bindings);
		} catch (final ScriptException e) {
			LogUtil.exception(e, user);
		}

		/*
		 * try { if (bindings.get("result") != null) { return
		 * bindings.get("result"); } } catch (Exception e) {
		 * LogUtil.exception(e,user); }
		 */
		if (requiredBool) {
			return false;
		}
		return r;
	}

	/**
	 * Adiciona uma instncia para ser executada por uma Thread Pool.
	 */
	protected void processActions(WorkflowInstance instance, GenericVO vo, Usuario user) {

		vo.put("WF_ATUAL_STEP", instance.getAtctualStepStatus(user));

		ServiceImpl ser = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowService", user);

		if (instance == null || !instance.isInitialized()) {
			return;
		}

		try {

			AdvancedWorkflow workflow = instance.getWorkflow(user);
			vo.put("ID_WORKFLOW_INSTANCE", instance.getId());
			LogUtil.warn("Carregando available actions para o workflow: " + workflow.getNome(), user);
			int availableActions[] = workflow.getAvailableActions(instance.getId(), vo);
			if (availableActions.length > 0) {
				vo.setProcess(false);
				for (int availableAction : availableActions) {
					executeAction(instance, workflow, availableAction, vo, user);
				}
			} else {
				LogUtil.warn("Nao ha acoes disponiveis para execucao.", user);
			}
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

	}

	/**
	 * Cria uma instancia do workflow recebido por parametro (workflowId) e liga
	 * essa instancia ao objeto recebido por parametro (ownerObject)
	 */

	public Long createInstance(final Map workflow, GenericVO VO, final Object ownerObject, final Usuario user) {

		Long result = new Long(0);

		if (workflow != null && user != null) {

			String objectBeanId = "";
			Long objectNumberId = new Long(0);
			boolean ownerObjectIsValid = false;

			if (ownerObject != null) {
				try {
					if (((String) ownerObject).split("-").length == 2) {
						objectBeanId = ((String) ownerObject).split("-")[0];
						objectNumberId = new Long(((String) ownerObject).split("-")[1]);

						if (objectBeanId != null && !objectBeanId.equals("") && objectNumberId != null
								&& objectNumberId > 0) {
							ownerObjectIsValid = true;
						}

					}

				} catch (Exception e) {
					LogUtil.error("ownerObject invalido ");
					LogUtil.exception(e, user);
				}
			} else {
				LogUtil.warn("Owner Object nulo, continuando sem ele...", user);
			}

			Map vo = new HashMap();

			if (ownerObjectIsValid) {
				vo.put("OBJETO", ownerObject);
			} else {
				vo.put("OBJETO", null);
			}

			try {
				AdvancedWorkflow aworkflow = new AdvancedWorkflow(new Long(workflow.get("ID_WORKFLOW").toString()),
						user);
				Map vars = new HashMap();
				if (aworkflow.getWorkflowDescriptor().getMetaAttributes().get("SRC") != null) {
					vars.put("SRC", aworkflow.getWorkflowDescriptor().getMetaAttributes().get("SRC"));
					vars.put("ID_WORKFLOW", workflow.get("ID_WORKFLOW"));
				}
				if (workflow.get("SCRIPT") != null) {
					Boolean val = (Boolean) evalRhino(VO, workflow.get("SCRIPT"), user, vars, true);
					if (!val) {
						return result;
					}
				}
				vo.put(Constants.VOKey, VO);
				result = aworkflow.initialize(1, vo);
			} catch (Exception e) {
				LogUtil.exception(e, user);
			}

		}

		return result;
	}

	// public boolean createInstanceIsSucessful(final Long workflowId,
	// final Object ownerObject, final Usuario user) {
	// Long result = createInstance(workflowId, ownerObject, user);
	// if (result != null && result > 0) {
	// return true;
	// } else {
	// return false;
	// }
	// }

	/**
	 * Cria instancias dos workflow recebido por parametro (workflowIds[]) e
	 * liga essas instancias ao objeto recebido por parametro (ownerObject)
	 */
	public List<Long> createInstancesForBeanId(final String beanId, final String ownerObject, final Usuario user,
			GenericVO vo) {

		LogUtil.info("Criando instancias de workflow para o Bean: " + beanId, user);

		List<Long> results = new ArrayList<Long>();

		List<Map> workflowIds = getWorkflowsByBeanId(beanId, user);

		for (int i = 0; i < workflowIds.size(); i++) {
			results.add(createInstance(workflowIds.get(i), vo, ownerObject, user));
		}

		return results;
	}

	public void executarInstancia(GenericVO vo, Usuario user) throws Exception {

		ServiceImpl ser = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowInstanceService", user);

		GenericVO v = ser.createVO(user);
		v.put("ID_WORKFLOW_INSTANCE", vo.get("ID_WORKFLOW_INSTANCE"));

		String ownerObject = (String) ser.obtem(v, user).get("OBJETO");
		WorkflowInstance instance = new WorkflowInstance((Long) vo.get("ID_WORKFLOW_INSTANCE"), user);

		try {
			if (ownerObject != null && ownerObject.split("-").length == 2) {
				String objectBeanId = ownerObject.split("-")[0];
				Long objectNumberId = new Long(ownerObject.split("-")[1]);
				ServiceImpl bs = (ServiceImpl) ser.getServiceById(objectBeanId, user);
				GenericVO vr = bs.createVO(user);
				vr.putKey(bs.getPrimaryKey().get(Constants.name), objectNumberId);
				vo.putAll(bs.obtem(vr, user));

			}

		} catch (Exception e) {
			LogUtil.error("ownerObject invalido ");
			LogUtil.exception(e, user);
		}

		executarInstancia(instance, vo, user);
	}

	/**
	 * Carrega uma lista com as Actions disponiveis para execucao, se houver uma
	 * ou mais actions, executa todas.
	 * 
	 * @param instance
	 * @param user
	 */
	public void executarInstancia(WorkflowInstance instance, GenericVO vo, Usuario user) {

		processActions(instance, vo, user);

	}

	/**
	 * Executa todas as acoes disponiveis para as Instancias ligadas ao Bean :
	 * beanId.
	 * 
	 * @param beanId
	 * @param user
	 */
	// public void executarInstanciasParaContextos(String beanId,
	// Usuario user) {
	//
	// LogUtil.info("executar acoes de instancias ligadas ao master bean: "
	// + beanId);
	//
	// try {
	// List<WorkflowInstance> instances = WorkflowInstance
	// .getInstancesForBeanId(beanId, user);
	//
	// if (instances != null && instances.size() > 0) {
	//
	// for (Object element : instances) {
	// GenericVO workflowInstanceMap = (GenericVO) element;
	// LogUtil.info("Instancia "
	// + workflowInstanceMap.get("ID_WORKFLOW_INSTANCE")
	// + " foi carregada");
	// // TODO Implementar uma Thread pra processar cada workflow
	//
	// if (workflowInstanceMap.containsKey("STATE")
	// && workflowInstanceMap.get("STATE").equals(
	// new Long(0))) {
	//
	// LogUtil.error("Instancia nova, para executar as acoes e necessario que
	// esta instancia seja inicializada.");
	//
	// } else {
	//
	// executarInstancia(workflowInstanceMap, false, user);
	// }
	//
	// }
	//
	// }
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }

	/**
	 * Executa todas as acoes disponiveis para as Instancias ligadas ao Bean :
	 * beanId.
	 * 
	 * @param beanId
	 * @param user
	 */
	public void executarInstanciasParaObjeto(Object[] ownerObject, GenericVO vo, Usuario user) {

		List<WorkflowInstance> instances = new ArrayList<WorkflowInstance>();

		for (Object element : ownerObject) {
			instances.addAll(getInstancesForObject(element, user));
		}

		for (Object element : instances) {
			WorkflowInstance workflowInstance = (WorkflowInstance) element;
			try {
				processActions(workflowInstance, vo, user);
			} catch (Exception e) {
				LogUtil.error("Nao foi possivel adicionar a execucao de workflow ao ThreadPool.");
				LogUtil.exception(e, user);
			}
		}

	}

	public List<WorkflowInstance> getInstancesForBeanId(String beanId, Usuario user) throws Exception {

		Long workflowId = new Long(0);
		List instancesMap = new ArrayList();
		List instances = new ArrayList();

		/**
		 * Carregar o Id do Workflow com CONTEXTO = beanId
		 */

		GenericService instanceService = (GenericService) user.getFactory().getSpringBean("WorkFlowService",
				user);
		try {
			GenericVO instanceMap = instanceService.obtem(instanceService.createVO(user).putKey("CONTEXTO", beanId),
					user);

			workflowId = (Long) instanceMap.get("ID_WORKFLOW");

		} catch (Exception e) {
			LogUtil.exception(e, user);
			return null;
		}

		/**
		 * Carregar a Lista de Instancias com ID_WORKFLOW = ?
		 */

		if (workflowId != null && workflowId > 0) {

			instanceService = (GenericService) user.getFactory().getSpringBean("WorkFlowInstanceService",
					user);

			instancesMap = instanceService.lista(instanceService.createVO(user).putKey("ID_WORKFLOW", workflowId),
					user);

		} else {
			return null;
		}

		return instancesMap;

	}

	public List<WorkflowInstance> getInstancesForObject(Object ownerObject, Usuario user) {

		List<Map<String, Object>> instancesMap = new ArrayList<Map<String, Object>>();
		List<WorkflowInstance> instances = new ArrayList<WorkflowInstance>();

		/**
		 * Carregar o Id do Workflow com CONTEXTO = beanId
		 */

		GenericService instanceService = (GenericService) user.getFactory()
				.getSpringBean("WorkFlowInstanceService", user);

		GenericVO queryVo = instanceService.createVO(user);
		queryVo.setFiltro("{gA:[{c:{f:'OBJETO',o:'=',v1:'" + ownerObject + "'}}]}");

		try {
			instancesMap = instanceService.lista(queryVo, user);

			for (Object element : instancesMap) {
				Map<String, Object> instanceMap = (Map<String, Object>) element;
				Long instanceId = (Long) instanceMap.get("ID_WORKFLOW_INSTANCE");
				WorkflowInstance instance = new WorkflowInstance(instanceId, user);
				instances.add(instance);
			}

		} catch (Exception e) {
			LogUtil.exception(e, user);
			return null;
		}

		return instances;
	}

	public String getObjectBeanIdFromObject(String ownerObject) {

		String result = null;

		if (ownerObject != null) {

			try {
				result = ownerObject.split("-")[0];
			} catch (Exception e) {
				LogUtil.error("No foi possvel ler o BeanId do objeto");
			}

		}

		return result;
	}

	public String getObjectNumericIdFromObject(String ownerObject) {

		String result = null;

		if (ownerObject != null) {

			try {
				result = ownerObject.split("-")[1];
			} catch (Exception e) {
				LogUtil.error("No foi possvel ler o BeanId do objeto");
			}

		}

		return result;

	}

	/**
	 * Executa uma action (actionId) na instancia (workflowInstanceId). <br>
	 * <br>
	 * argumentosDeEntrada pode ser nulo (esta map a map de variaveis que serao
	 * usadas no xml para escrever a logica), na documentacao do OsWorkflow,
	 * essas sao as TransientVars. <br>
	 * <br>
	 * actionId o mesmo id que est no XML.
	 * 
	 * @throws WorkflowException
	 */
	public boolean executeAction(final Long workflowInstanceId, final int actionId, Map argumentosDeEntrada,
			final Usuario user) {

		WorkflowInstance workflowInstance = new WorkflowInstance(workflowInstanceId, user);

		AdvancedWorkflow workflow = workflowInstance.getWorkflow(user);

		return executeAction(workflowInstance, workflow, actionId, argumentosDeEntrada, user);

	}

	public boolean executeAction(WorkflowInstance workflowInstance, AdvancedWorkflow workflow, final int actionId,
			Map argumentosDeEntrada, final Usuario user) {

		try {
			workflow.doAction(workflowInstance.getId(), actionId, argumentosDeEntrada);
			return true;
		} catch (WorkflowException e) {
			LogUtil.error("Nao foi possivel executar a acao do Workflow");
			LogUtil.exception(e, user);
			return false;
		}

	}

	/**
	 * Retorna um Array de Inteiros, com os ID's das actions disponiveis para
	 * execucao no momento.
	 */
	public int[] getAvailableActions(WorkflowInstance workflowInstance, Usuario user) {
		return workflowInstance.getWorkflow(user).getAvailableActions(workflowInstance.getId());
	}

	public WorkflowConfiguration getConfiguration(AdvancedWorkflow workflow, Usuario user) {
		return workflow.getConfiguration();
	}

	/**
	 * Retorna uma lista com os IDs numricos dos workflows para o Bean recebido
	 * por parmetro.
	 */
	public List<Map> getWorkflowsByBeanId(String beanId, Usuario user) {

		ServiceImpl ser = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowService", user);

		GenericVO vo = ser.createVO(user);

		vo.setFiltro("{gA:[{c:{f:'CONTEXTO',o:'=',vc:'" + beanId + "'}}, {c:{f:'AUTO', o:'=', vc:'T'}}]}");

		try {
			vo.setPaginate(false); // sem isso se tivermos mais de 20 nao viria
			return ser.lista(vo, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		return new ArrayList<Map>();

	}

	/**
	 * Retorna uma List com os IDs das instancias ligadas ao objeto recebido por
	 * parametro.
	 */
	public List<Long> getWorkflowInstances(final Long ownerObjectId, final Usuario user) {

		final List<Long> instancias = new ArrayList<Long>();

		return instancias;
	}

	/**
	 * Retorna uma List com as instancias ligadas ao objeto recebido por
	 * parametro.
	 */
	public List<WorkflowInstance> getWorkflowInstances(final Object ownerObject, final Usuario user) {

		final List<WorkflowInstance> instancias = new ArrayList<WorkflowInstance>();

		return instancias;
	}

	@Override
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {

		String targetMethod = (String) args.get("targetMethod");

		if (targetMethod.equals("sendMessage")) {
			Usuario user = (Usuario) transientVars.get("user");
			String message = (String) args.get("message");
			String title = (String) args.get("title");
			String emailSRC = (String) args.get("email");
			String messageType = (String) args.get("type");
			String messagePriority = (String) args.get("priority");
			File f = null;
			MessagePriorityLevel priority = MessagePriorityLevel.valueOf(messagePriority);
			MessageType type = MessageType.valueOf(messageType);
			if (args.get("att") != null) {
				f = (File) evalRhino((GenericVO) transientVars.get(Constants.VOKey), args.get("att"), user,
						transientVars, false);
			}

			try {
				sendMessage(message, title, emailSRC, transientVars, type, priority, f, user);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				LogUtil.exception(e, user);
			}

		}

		if (targetMethod.equals("sendTask")) {
			Usuario user = (Usuario) transientVars.get("user");
			// String options = (String) args.get("options");
			String params = (String) args.get("params");
			String items = (String) args.get("items");
			String message = (String) args.get("message");
			String messageType = (String) args.get("type");

			Object idp = evalRhino((GenericVO) transientVars.get(Constants.VOKey), args.get("ID_PERFIL"), user,
					transientVars, false);

			LogUtil.debug("Perfil :" + idp, user);

			Long id_perfil = null;

			if (idp != null) {
				id_perfil = new Long(idp.toString());
			}
			Object idu = evalRhino((GenericVO) transientVars.get(Constants.VOKey), args.get("ID_USUARIO"), user,
					transientVars, false);

			LogUtil.debug("Id do perfil responsvel pelo passo atual: " + idp, user);

			Long id_usuario = null;
			if (idu != null) {
				id_usuario = new Long(idu.toString());
			}

			LogUtil.debug("Id do usurio responsvel pelo passo atual: " + idu, user);

			String messagePriority = (String) args.get("priority");

			MessagePriorityLevel priority = MessagePriorityLevel.valueOf(messagePriority);
			MessageType type = MessageType.valueOf(messageType);

			sendTask(((WorkflowInstance) transientVars.get("entry")).getId(), message, type, priority, params, items,
					user, id_usuario, id_perfil, transientVars);

		}

	}

	private void sendMessage(String message, String title, String emailSRC, Map transientVars, MessageType messageType,
			MessagePriorityLevel priority, File att, Usuario user) throws Exception {

		StandardEvaluationContext ctx = user.getUtil().createContext((Map) transientVars.get(Constants.VOKey), user);
		message = user.getUtil().EvaluateInString(user.getUtil().replace(message, "#{", "${"), ctx);
		LogUtil.debug(message, user);
		title = user.getUtil().EvaluateInString(user.getUtil().replace(title, "#{", "${"), ctx);

		String email = evalRhino((GenericVO) transientVars.get(Constants.VOKey), emailSRC, user, transientVars, false)
				.toString();

		LogUtil.info("************* sendMessage recebeu:" + priority.name(), user);
		LogUtil.info(" Email: " + email, user);
		LogUtil.info(" Message: " + message, user);
		LogUtil.info(" Type: " + messageType.name(), user);
		LogUtil.info(" Priority: " + priority.name(), user);
		LogUtil.info("*************", user);

		if (email.trim().length() == 0) {
			LogUtil.info(" Sem email saindo", user);
			return;
		}

		if (messageType == MessageType.EMAIL) {
			ServiceImpl ser = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowService", user);

			EmailService emailService = ser.getEmailService(user);
			GenericVO vo = emailService.createVO(user);
			vo.put(EmailJob.EMAIL_CORPO, message);
			vo.put(EmailJob.EMAIL_ASSUNTO, title);
			vo.put(EmailJob.EMAIL_PARA, email);
			vo.put(EmailJob.FILE_ATTACHMENT, att);
			emailService.send(vo, user);

		}

	}

	private void sendTask(Long instanceId, String message, MessageType messageType, MessagePriorityLevel priority,
			String params, String items, Usuario user, Long idu, Long idp, Map transientVars) {

		StandardEvaluationContext ctx = user.getUtil().createContext((Map) transientVars.get(Constants.VOKey), user);

		LogUtil.info("************* sendTask recebeu:" + priority.name(), user);
		LogUtil.info(" Message: " + message, user);
		LogUtil.info(" Type: " + messageType.name(), user);
		LogUtil.info(" Priority: " + priority.name(), user);
		LogUtil.info(" Params: " + params, user);
		LogUtil.info(" Items: " + items, user);
		LogUtil.info(" Perfil: " + idp, user);
		LogUtil.info(" Usuario: " + idu, user);
		LogUtil.info("*************", user);

		ServiceImpl ser = (ServiceImpl) user.getFactory().getSpringBean("WorkFlowAtividadeService", user);

		if (ser == null) {
			LogUtil.error("No foi possvel carregar o service de WorkFlowAtividade");
			return;
		}

		items = user.getUtil().replace(items, "#{", "${");
		params = user.getUtil().replace(params, "#{", "${");
		items = user.getUtil().EvaluateInString(user.getUtil().replace(items, "#{", "${"), ctx);
		items = user.getUtil().replace(items, "@{", "${");
		message = user.getUtil().EvaluateInString(user.getUtil().replace(message, "#{", "${"), ctx);
		GenericVO vo = ser.createVO(user);
		vo.put("ID_WORKFLOW_INSTANCE", instanceId);
		vo.put("TAREFA", message);
		vo.put("ITEMS", items);
		vo.put("PARAMS", params);
		// vo.put("OPTIONS", options);
		vo.put("ID_USUARIO", idu);
		vo.put("ID_PERFIL", idp);

		String result = null;

		try {
			vo.setProcess(false); // as intancias de workflow serao ignoradas
			result = ser.inclui(vo, user);
			vo.setProcess(true);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		LogUtil.debug("result=" + result, user);

	}
}
