package com.aseg.util.service;

import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;

public class Soundex {
	/*******************************************************
	 * ProJuris 7
	 * 
	 * $Author: hasega $ $Date: 2012/05/21 14:51:27 $Header:
	 * /home/gabriel/essen8/pjwin/Sounds.pas,v 1.1 2008/01/15 20:48:25 gcherem
	 * Exp $ $Id: Soundex.java,v 1.1.2.4 2012/02/17 14:50:27 hasega Exp $
	 * $Locker: $ $Name: REFACTORING $ $RCSfile: Soundex.java,v $ $Revision:
	 * 1.1.2.7 $ $Source: /home/cvs/pjweb4.5/EssenWEB/src/br/com/aseg/essen/util
	 * /service/Attic/Soundex.java,v $ $State: Exp $ $Log: Sounds.pas,v $
	 * Revision 1.1 2008/01/15 20:48:25 gcherem empty log message ***
	 * 
	 * Revision 1.1 2007/08/24 23:30:04 gcherem empty log message ***
	 * 
	 * Revision 1.3 2007/04/03 16:48:03 gabriel empty log message ***
	 * 
	 * Revision 1.2 2006/01/20 22:12:15 gabriel empty log message ***
	 * 
	 * Revision 1.1 2003/12/08 16:57:38 gabriel no message
	 * 
	 * 
	 ******************************************************* 
	 * 
	 * Este c—digo fonte Ž de propriedade da Sigma Computa‹o Ltda.
	 * 
	 * Copyright © 1995-2003 Sigma Computa‹o Ltda. Todos os direitos reservados.
	 *******************************************************/

	/********************************************************************
	 * } {* The following are selected letters of the alphabet which are divided
	 * *} {* into their corresponding code (1-6). You might need to modify these
	 * for *} {* different languages depending upon whether the language
	 * requires *} {* alphabets other than the ones specified below *} {
	 ********************************************************************/

	String regexDelimiter = "[\\s+,\\.\\;'\"/\\-:]"; // common delimiters

	// in freeform text

	public String RemoveAcento(String string) {

		string = Normalizer.normalize(string, Normalizer.Form.NFD);
		string = string.replaceAll("[^\\p{ASCII}]", "");

		return string;
	}

	public String TextSound(String InpStr) {
		int i = 1;
		String W = "";
		// Ê T = #9 tab #10 lf #13 cr #32 espaco #44 #46 #59 ' " ; , / - : ;
		// //common delimiters in freeform text

		String Result = "";
		int I = 0;
		while (true) {
			W = ParseWord(InpStr, regexDelimiter, I);
			I++;
			if (W != null && W.trim().length() > 0)
				Result = Result + Sound(W);
			else if (W != null && W.trim().length() == 0)
				continue;
			else
				break;
		}

		return Result;

	}

	public String[] ParseWord(String a) {
		return a.split(regexDelimiter);
	}

	private String ParseWord(String inpStr, String t, int i) {
		// TODO Auto-generated method stub
		try {
			return inpStr.split(t)[i];
		} catch (Exception e) {
			return null;
		}
	}

	private String ReplaceStr(String InpStr, String SubStr, String WithStr, boolean pStart, boolean pMid,
			boolean pEnd) {
		int i = 0;

		if (pStart && InpStr.startsWith(SubStr)) {
			InpStr = InpStr.replaceAll("^" + SubStr, WithStr);
		}
		if (pMid) {
			String res = "";
			String fin = "";
			if (InpStr.startsWith(SubStr)) {
				res += SubStr;
				InpStr = InpStr.substring(SubStr.length());
			}
			if (InpStr.endsWith(SubStr) && !pEnd) {
				fin += SubStr;
				InpStr = InpStr.substring(0, (InpStr.length() - SubStr.length()));
			}
			InpStr = res + InpStr.replaceAll("(" + SubStr + ")", WithStr) + fin;
		}
		if (pEnd && InpStr.endsWith(SubStr)) {
			InpStr = InpStr.replaceAll(SubStr + "$", WithStr);
		}
		return InpStr;
	}

	public String Sound(String InpStr) {
		/********************************************************************
		 * } {* Uppercase & remove invalid characters from given string *}
		 * {*****
		 * ***************************************************************} {*
		 * Please have a long & hard look at this code if you have modified any
		 * of *} {* the constants Chars1,Chars2 ... Chars6 by increasing the
		 * overall range *} {* of alphabets *} {
		 ********************************************************************/

		final List Chars1 = (List) Arrays.asList(new Character[] { 'B', 'P', 'F', 'V' });
		final List Chars2 = (List) Arrays.asList(new Character[] { 'C', 'S', 'K', 'G', 'J', 'Q', 'X', 'Z' });
		final List Chars3 = (List) Arrays.asList(new Character[] { 'D', 'T' });
		final List Chars4 = (List) Arrays.asList(new Character[] { 'L' });
		final List Chars5 = (List) Arrays.asList(new Character[] { 'M', 'N' });
		final List Chars6 = (List) Arrays.asList(new Character[] { 'R' });

		if (InpStr == null) {
			InpStr = "";
		}
		InpStr = InpStr.toUpperCase();
		String vStr = "";
		String Result = null;

		// for i = 1 to Length(InpStr)do
		// case InpStr[i] of
		// 'a'..'z': vStr = vStr + UpCase(InpStr[i]);
		// 'A'..'Z': vStr = vStr + InpStr[i];
		// } {case}
		vStr = RemoveAcento(InpStr.toUpperCase());

		if (!vStr.equals("")) {
			/**************************************************************************
			 * } {* Language Tweaking Section *}
			 * {*******************************
			 * *************************************} {* Tweak for language
			 * peculiarities e.g. "CAt"="KAt", "KNIfe"="NIfe" *} {*
			 * "PHone"="Fone", "PSYchology"="SIchology", "EXcel"="Xcel" etc...
			 * *} {* You will need to modify these for different languages.
			 * Optionally, you *} {* may choose not to have this section at all,
			 * in which case, the output *} {* of Sound() will correspond to
			 * that of SoundEx(). Please note however *} {* the importance of
			 * what you replace & the order in which you replace. *}
			 * {***********
			 * *********************************************************} {*
			 * Also, please note that the following replacements are targeted
			 * for the *} {* English language & that too is subject to
			 * improvements *} {
			 ********************************************************************/
			vStr = ReplaceStr(vStr, "XCE", "ÇE", false, true, true);
			vStr = ReplaceStr(vStr, "XCI", "ÇI", false, true, true);
			vStr = ReplaceStr(vStr, "XCY", "ÇI", false, true, true);
			vStr = ReplaceStr(vStr, "SS", "Ç", false, true, false);
			vStr = ReplaceStr(vStr, "EXA", "EZA", true, false, false);
			vStr = ReplaceStr(vStr, "EXE", "EZE", true, false, false);
			vStr = ReplaceStr(vStr, "EXI", "EZI", true, false, false);
			vStr = ReplaceStr(vStr, "EXY", "EZI", true, false, false);
			vStr = ReplaceStr(vStr, "EXO", "EZO", true, false, false);
			vStr = ReplaceStr(vStr, "EXU", "EZU", true, false, false);
			vStr = ReplaceStr(vStr, "QUI", "KI", true, true, true);
			vStr = ReplaceStr(vStr, "QUY", "KI", true, true, true);
			vStr = ReplaceStr(vStr, "QUE", "KE", true, true, true);
			vStr = ReplaceStr(vStr, "AU", "AL", false, false, true);
			vStr = ReplaceStr(vStr, "EU", "EL", false, false, true);
			vStr = ReplaceStr(vStr, "CA", "KA", true, true, true);
			vStr = ReplaceStr(vStr, "CL", "KL", true, true, true);
			vStr = ReplaceStr(vStr, "CK", "K", true, true, true);
			vStr = ReplaceStr(vStr, "X", "KS", false, false, true);
			vStr = ReplaceStr(vStr, "SCH", "X", true, true, false);
			vStr = ReplaceStr(vStr, "SH", "X", true, true, false);
			vStr = ReplaceStr(vStr, "CH", "X", true, true, false);
			vStr = ReplaceStr(vStr, "PH", "F", true, true, true);
			vStr = ReplaceStr(vStr, "LH", "L", true, true, false);
			vStr = ReplaceStr(vStr, "SA", "ZA", false, true, true);
			vStr = ReplaceStr(vStr, "SE", "ZE", false, true, true);
			vStr = ReplaceStr(vStr, "SI", "ZI", false, true, true);
			vStr = ReplaceStr(vStr, "SY", "ZI", false, true, true);
			vStr = ReplaceStr(vStr, "SO", "ZO", false, true, true);
			vStr = ReplaceStr(vStr, "SU", "ZU", false, true, true);
			vStr = ReplaceStr(vStr, "CA", "KA", true, true, true);
			vStr = ReplaceStr(vStr, "CE", "SE", true, true, true);
			vStr = ReplaceStr(vStr, "CI", "SI", true, true, true);
			vStr = ReplaceStr(vStr, "CY", "SI", true, true, true);
			vStr = ReplaceStr(vStr, "CO", "KO", true, true, true);
			vStr = ReplaceStr(vStr, "CU", "KU", true, true, true);
			vStr = ReplaceStr(vStr, "JE", "GE", true, true, true);
			vStr = ReplaceStr(vStr, "JI", "GI", true, true, true);
			vStr = ReplaceStr(vStr, "JY", "GI", true, true, true);
			vStr = ReplaceStr(vStr, "EZ", "ES", false, false, true);
			vStr = ReplaceStr(vStr, "Ç", "S", true, true, true);
			vStr = ReplaceStr(vStr, "Y", "I", true, true, true);
			vStr = ReplaceStr(vStr, "W", "V", true, true, true);

			/********************************************************************
			 * } {* String Assembly Section *} {
			 ********************************************************************/
			char PrevCh = '\0';
			Result = String.valueOf(vStr.charAt(0));

			for (int i = 1; i < vStr.length(); i++) {
				if (Result.length() == 4)
					break;

				char CurrCh = vStr.charAt(i);
				if (CurrCh != PrevCh) {
					if (Chars1.contains(CurrCh))
						Result = Result + "1";
					else if (Chars2.contains(CurrCh))
						Result = Result + "2";
					else if (Chars3.contains(CurrCh))
						Result = Result + "3";
					else if (Chars4.contains(CurrCh))
						Result = Result + "4";
					else if (Chars5.contains(CurrCh))
						Result = Result + "5";
					else if (Chars6.contains(CurrCh))
						Result = Result + "6";

					PrevCh = CurrCh;
				}
			}
		} else
			Result = "";

		while (Result.length() < 4) {
			Result = Result + "0";
		}

		return Result;

	}
}
