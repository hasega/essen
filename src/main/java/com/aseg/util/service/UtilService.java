package com.aseg.util.service;

import java.util.List;
import java.util.Map;

import com.aseg.seguranca.Usuario;
import com.aseg.vo.GenericVO;

public interface UtilService {
	List<Map<String, String>> getTrueOrFalse(GenericVO vo, Usuario user);

}