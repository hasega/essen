package com.aseg.util.service;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.jdbc.support.MetaDataAccessException;

import com.aseg.config.Constants;
import com.aseg.dao.DAOComparator;
import com.aseg.dao.DAOComparator.PROBLEM_TYPE;
import com.aseg.dao.DAOMetadata;
import com.aseg.dao.DBCreator;
import com.aseg.dao.Metadata;
import com.aseg.exceptions.BusinessException;
import com.aseg.exceptions.ConfigurationException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.service.importation.XMLImporter;
import com.aseg.service.setup.SemanticValidation;
import com.aseg.util.ConnectionManager;
import com.aseg.util.DAOTemplate;
import com.aseg.util.sql.Dialeto;
import com.aseg.util.sql.InterfaceDialeto;
import com.aseg.vo.GenericVO;

public class SetupService extends ServiceImpl implements FactoryBean, InitializingBean {

	public enum DataBaseOptions {
		NOT_CONFIGURED, LIST_PROBLEMS, REMOVE_CONSTRAINTS, CREATE_DATA_BASE, EXECUTE_SQL, REMOVE_INDEXES, REMOVE_SEQUENCES, CLONE_DATA_BASE, FONETIC_INDEX, CREATE_CONNECTION, PROCESS_LABELS, DISABLE_SETUP, EXECUTAR_SCRIPT_RULE;
	};

	private List<Map<String, Object>> dataBaseAnalys;
	private Metadata metadata;
	private List<Map<String, String>> validationErrors;
	private List<Map<String, String>> dataBaseDiff;
	private Map mainField;

	@Override
	public void afterPropertiesSet() throws Exception {

		List p = (List) getProperties().get(Constants.ALL_FIELDS);
		for (final Iterator iterator = p.iterator(); iterator.hasNext();) {
			final Map object = (Map) iterator.next();
			ParseXmlService.trataCampos(object);

			if (object.get("mainfield") != null && object.get("mainfield").equals(Constants.true_str)) {
				this.mainField = object;
			}
		}

		p = getMethods(null);
		if (p != null) {
			for (final Iterator iterator = p.iterator(); iterator.hasNext();) {
				final Map object = (Map) iterator.next();
				final List c = (List) object.get(Constants.ALL_FIELDS);

				if (c != null) {
					for (final Iterator i = c.iterator(); i.hasNext();) {
						final Map cd = (Map) i.next();
						ParseXmlService.trataCampos(cd);
					}
				}
			}
		}
	}

	private void amendDatabase(final DBCreator dbCreator, final Map<String, Object> problems) {
		boolean sucess = false;

		final PROBLEM_TYPE problemType = DAOComparator.PROBLEM_TYPE.valueOf((String) problems.get("ERROR_CODE"));

		final String tableName = problems.get("TABLE").toString();
		try {
			switch (problemType) {
			case TABLE_NOT_EXISTS:
				dbCreator.createTable(tableName);
				sucess = true;
				break;
			case COLUMN_NOT_EXISTS:
				dbCreator.createColumn(tableName, problems.get("OBJECT").toString());
				sucess = true;
				break;
			case SEQUENCE_NOT_EXISTS:
				dbCreator.createSequence(tableName, problems.get("CONFIG_VALUE").toString());
				sucess = true;
				break;

			case AUTOINCREMENT_NOT_EXISTS:
				String t = dbCreator.contSQL("select max(" + problems.get("OBJECT") + ")+1 from " + tableName);
				if (t.trim().equalsIgnoreCase("0"))
					t = "1";
				dbCreator.createSequence(tableName, problems.get("OBJECT").toString(), new Long(t).longValue());

				sucess = true;
				break;

			case COLUMN_NOT_NULL:
				if (problems.get("DEFAULT_VALUE") != null && problems.get("DEFAULT_VALUE").toString().length() > 0) {
					dbCreator.updateColumn(tableName, problems.get("OBJECT").toString(), problems.get("DEFAULT_VALUE"),
							problems.get("ESCAPED") != null);
				}
				dbCreator.alterColumn(tableName, problems.get("OBJECT").toString(), true);
				sucess = true;

				break;
			case COLUMN_NULL:
				if (problems.get("DEFAULT_VALUE") != null && problems.get("DEFAULT_VALUE").toString().length() > 0) {
					dbCreator.updateColumn(tableName, problems.get("OBJECT").toString(), problems.get("DEFAULT_VALUE"),
							problems.get("ESCAPED") != null);
				}

				dbCreator.alterColumn(tableName, problems.get("OBJECT").toString(), false);
				sucess = true;
				break;
			case COLUMN_TYPE:
				dbCreator.alterColumn(tableName, problems.get("OBJECT").toString());
				sucess = true;
				break;
			case COLUMN_LENGTH:
				dbCreator.alterColumn(tableName, problems.get("OBJECT").toString());
				sucess = true;
				break;
			case COLUMN_PRECISION:
				dbCreator.alterColumn(tableName, problems.get("OBJECT").toString());
				sucess = true;
				break;
			case PRIMARY_NOT_EXISTS:
				dbCreator.createPrimaryKey(tableName);
				sucess = true;
				break;
			case FOREIGN_KEY_NOT_EXISTS:
				if (problems.get("DEFAULT_VALUE") != null
						&& problems.get("DEFAULT_VALUE").toString().trim().length() > 0) {
					dbCreator.updateFKColumn(tableName, problems.get("CONFIG_VALUE").toString(),
							problems.get("DEFAULT_VALUE"),
							dbCreator.getPropertyValue(tableName, problems.get("CONFIG_VALUE").toString(), "key"),
							problems.get("OBJECT").toString(), problems.get("ESCAPED") != null);
				}

				dbCreator.createForeignKey(tableName, problems.get("OBJECT").toString(),
						problems.get("CONFIG_VALUE").toString());
				sucess = true;
				break;
			case CHECK_CONSTRAINT_NOT_EXISTS:
				dbCreator.createBooleanConstraint(tableName, (String) problems.get("OBJECT"));
				sucess = true;
				break;
			case FOREIGNKEY_INDEX_NOT_EXISTS:
				dbCreator.createForeignKeyIndex(tableName, (String) problems.get("OBJECT"));
				sucess = true;
				break;
			case INDEX_NOT_EXISTS:
				dbCreator.createIndex(tableName, (String) problems.get("OBJECT"));
				sucess = true;
				break;
			}

		} catch (Exception e) {
			if (problems.get("ERROR_TYPE") != null
					&& problems.get("ERROR_TYPE").toString().indexOf(e.getMessage()) == -1)
				problems.put("ERROR_TYPE", problems.get("ERROR_TYPE") + " <br> " + e.getMessage());
		}
		if (sucess)
			dataBaseAnalys.remove(problems);

	}

	public List<Map<String, String>> getTrueOrFalse(final GenericVO vo, final Usuario user) {
		final List<Map<String, String>> labels = new ArrayList<Map<String, String>>();
		Map<String, String> label = new HashMap<String, String>();

		label.put(Constants.name, user.getLabel("label.sim"));
		label.put("ID", "T");
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.nao"));
		label.put("ID", "F");
		labels.add(label);
		return labels;

	}

	public List getLanguages(GenericVO vo, Usuario user) {

		final List<Map<String, String>> linguagens = new ArrayList<Map<String, String>>();

		try {
			GenericService lang = (GenericService) getService("LinguagemService", user);
			GenericVO langVO = lang.createVO(user);
			List<GenericVO> vos = (ArrayList<GenericVO>) lang.lista(langVO, user);

			for (Iterator iterator = vos.iterator(); iterator.hasNext();) {
				GenericVO langVos = (GenericVO) iterator.next();
				Map<String, String> linguagem = new HashMap<String, String>();
				linguagem.put("NAME", (langVos.get("VALOR_CONFIG_SISTEMA")).toString());
				linguagem.put("ID", (langVos.get("ID_CONFIG_SISTEMA")).toString());
				linguagens.add(linguagem);
			}

		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		return linguagens;
	}

	public List<Map<String, String>> getConnections(final GenericVO vo, final Usuario user) {
		final List<Map<String, String>> labels = new ArrayList<Map<String, String>>();

		Map sys = ConnectionManager.getInstance(user).getPool();
		Iterator i = sys.keySet().iterator();
		while (i.hasNext()) {
			String object = (String) i.next();

			Map<String, String> label = new HashMap<String, String>();

			label.put("NAME", object);
			label.put("ID", object);
			labels.add(label);

		}

		return labels;

	}

	public List<Map<String, String>> getDrivers(final GenericVO vo, final Usuario user) {
		final List<Map<String, String>> labels = new ArrayList<Map<String, String>>();

		Map<String, String> label1 = new HashMap<String, String>();
		label1.put(Constants.name, "Oracle");
		label1.put("ID", "oracle.jdbc.driver.OracleDriver");
		label1.put("tem", "jdbc:oracle:thin:@localhost:1521:orcl");
		labels.add(label1);
		Map<String, String> label2 = new HashMap<String, String>();
		label2.put(Constants.name, "SqlServer");
		label2.put("tem", "jdbc:jtds:sqlserver://localhost:1433/essen");
		label2.put("ID", "net.sourceforge.jtds.jdbc.Driver");
		labels.add(label2);
		Map<String, String> label3 = new HashMap<String, String>();
		label3.put(Constants.name, "Postgres");
		label3.put("ID", "org.postgresql.Driver");
		label3.put("tem", "jdbc:postgresql://ferrugem/ctc");
		labels.add(label3);

		return labels;

	}

	public void amendDatabase(final List<Map<String, Object>> problems, final boolean onlyLogDDL, GenericVO vo,
			final Usuario user) throws SQLException {

		final DBCreator dbCreator = getDBCreator(onlyLogDDL, user);

		for (final Map<String, Object> problem : problems) {
			try {

				final PROBLEM_TYPE errorGuide = DAOComparator.PROBLEM_TYPE.valueOf((String) problem.get("ERROR_CODE"));
				problem.put("DEFAULT_VALUE", vo.get("DEFAULT_VALUE"));
				problem.put("ESCAPED", vo.get("ESCAPED"));

				amendDatabase(dbCreator, problem);

				if (vo.get("ALL_NAMES") != null && vo.get("ALL_NAMES").equals("T")) {
					List<Map<String, Object>> l = new ArrayList<Map<String, Object>>(dataBaseAnalys);
					for (final Map<String, Object> problem2 : l) {

						final PROBLEM_TYPE error = DAOComparator.PROBLEM_TYPE
								.valueOf((String) problem2.get("ERROR_CODE"));

						if (problem.get("OBJECT").equals(problem2.get("OBJECT")) && errorGuide.equals(error)) {
							try {
								problem2.put("DEFAULT_VALUE", vo.get("DEFAULT_VALUE"));
								problem2.put("ESCAPED", vo.get("ESCAPED"));
								amendDatabase(dbCreator, problem2);
							} catch (Exception e) {
								LogUtil.exception(e, user);
							}
						}
					}

				}
				if (vo.get("ALL_TYPES") != null && vo.get("ALL_TYPES").equals("T")) {
					// dataBaseAnalys.remove(problem);

					List<Map<String, Object>> l = new ArrayList<Map<String, Object>>(dataBaseAnalys);
					for (final Map<String, Object> problem2 : l) {

						final PROBLEM_TYPE error = DAOComparator.PROBLEM_TYPE
								.valueOf((String) problem2.get("ERROR_CODE"));

						if (errorGuide.equals(error) && dbCreator
								.getColumnType(problem.get("TABLE").toString(), problem.get("OBJECT").toString())
								.equals(dbCreator.getColumnType(problem2.get("TABLE").toString(),
										problem2.get("OBJECT").toString()))) {
							try {
								problem2.put("DEFAULT_VALUE", vo.get("DEFAULT_VALUE"));
								problem2.put("ESCAPED", vo.get("ESCAPED"));
								amendDatabase(dbCreator, problem2);
							} catch (Exception e) {
								LogUtil.exception(e, user);
							}
						}
					}

				}
			} catch (Exception e) {
				LogUtil.exception(e, user);
			}
		}
	}

	public void amendDatabase(final List<Map<String, Object>> problems, final boolean onlyLogDDL, final Usuario user)
			throws SQLException {

		final DBCreator dbCreator = getDBCreator(onlyLogDDL, user);

		for (final Map<String, Object> problem : problems) {
			try {
				amendDatabase(dbCreator, problem);
			} catch (Exception e) {
				LogUtil.exception(e, user);
			}
		}
	}

	public void amendDatabase(final String problemType, final boolean onlyLogDDL, final Usuario user)
			throws SQLException {

		final PROBLEM_TYPE errorGuide = DAOComparator.PROBLEM_TYPE.valueOf(problemType);
		final DBCreator dbCreator = getDBCreator(onlyLogDDL, user);

		PROBLEM_TYPE error;
		List<Map<String, Object>> l = new ArrayList<Map<String, Object>>(dataBaseAnalys);
		for (final Map<String, Object> problem : l) {

			error = DAOComparator.PROBLEM_TYPE.valueOf((String) problem.get("ERROR_CODE"));

			if (errorGuide.equals(error)) {
				try {
					amendDatabase(dbCreator, problem);
				} catch (Exception e) {
					LogUtil.exception(e, user);
				}
			}
		}
	}

	private synchronized void cleanDataBaseAnalyse() {
		// Zera a lista de anlises do banco de dados
		dataBaseAnalys = null;
		dataBaseDiff = null;
		metadata = null;
	}

	public void createDataBase(final boolean onlyLogDDL, final Usuario user) throws SQLException {
		final DBCreator dbCreator = getDBCreator(onlyLogDDL, user);
		dbCreator.create();
	}

	public void createDataBaseLogTable(final Usuario user) throws SQLException {
		final JdbcTemplate jdbcTemplate = getJDBCTemplate(user);
		final InterfaceDialeto dialect = getDialect(user);

		final Map<String, Map<String, String>> fields = new HashMap<String, Map<String, String>>();

		Map<String, String> field = new HashMap<String, String>();
		field.put("type", "alfa");
		field.put("length", "4000");
		fields.put("LOG", field);

		field = new HashMap<String, String>();
		field.put("type", "date");
		fields.put("DATA", field);

		field = new HashMap<String, String>();
		field.put("type", "bit");
		fields.put("USUARIO", field);

		field = new HashMap<String, String>();
		field.put("type", "bit");
		fields.put("EXECUTADO", field);

		try {
			jdbcTemplate.execute(dialect.createTable("M_LOG_BANCODADOS", fields));
		} catch (final Exception e) {
			// Pode j existir
		}
	}

	public synchronized List<Map<String, Object>> dataBaseAnalys(final GenericVO vo, final Usuario us)
			throws Exception {

		if (dataBaseAnalys == null) {

			final Metadata metadata = getDBMetadata(us);

			final InterfaceDialeto dialect = getDialect(us);

			final JdbcTemplate jdbcTemplate = getJDBCTemplate(us);

			final DAOComparator comparator = new DAOComparator(metadata, dialect, jdbcTemplate, getParseXmlService(us),
					us);

			List<Map<String, Object>> diff = null;

			try {
				diff = comparator.getIncompatibilities();
			} catch (final Exception e) {
				LogUtil.exception(e, us);
			}

			String label;

			for (final Map<String, Object> analyse : diff) {
				label = "label.setup.bancodados." + analyse.get("ERROR_CODE").toString().toLowerCase();
				analyse.put("ERROR_TYPE", us.getLabel(label));
				analyse.put("ID", analyse.hashCode());
				LogUtil.debug("Populando anlise do DB> " + analyse, us);
			}

			dataBaseAnalys = diff;
		}

		final String s = vo.getSort() == null ? "TABLE" : vo.getSort();
		us.getUtil().SortCollection(dataBaseAnalys, s);

		if (vo.paginate()) {
			vo.put("TOTAL", Integer.toString(dataBaseAnalys.size()));

			final int start = Integer.parseInt(vo.get("START").toString());
			final int limit = Integer.parseInt(vo.get("LIMIT").toString());
			int end = start + limit;
			final int size = dataBaseAnalys.size();

			if (size >= limit) {
				end = size < end ? size : end;
				return slice(dataBaseAnalys,start, end);
			}
		}

		return new ArrayList<>(dataBaseAnalys);
	}
    public static <T> List<T> slice(List<T> list, int index, int count) {
        List<T> result = new ArrayList<T>();
        if (index >= 0 && index < list.size()) {
            int end = index + count < list.size() ? index + count : list.size();
            for (int i = index; i < end; i++) {
                result.add(list.get(i));
            }
        }
        return result;
    }
	public boolean dataBaseCreated(final Usuario user) throws Exception {
		final Metadata metadata = getDBMetadata(user);
		return metadata != null && metadata.getTables().get("M_CONFIG_SISTEMA") != null;
	}

	public synchronized List<Map<String, String>> dataBaseDiff(final GenericVO vo, final Usuario us) throws Exception {

		if (dataBaseDiff == null) {
			dataBaseDiff = new ArrayList<Map<String, String>>();

			final Metadata metadata = getDBMetadata(us);

			final InterfaceDialeto dialect = getDialect(us);

			final JdbcTemplate jdbcTemplate = getJDBCTemplate(us);

			final DAOComparator comparator = new DAOComparator(metadata, dialect, jdbcTemplate, getParseXmlService(us),
					us);

			final List<Map<String, String>> diff = comparator.getDiff();

			String label;
			int i = 0;

			for (final Map<String, String> analyse : diff) {
				label = "label.setup.bancodados." + analyse.get("ERROR_CODE").toLowerCase();
				analyse.put("ERROR_TYPE", us.getLabel(label));
				analyse.put("ID", String.valueOf(analyse.hashCode()));
				dataBaseDiff.add(analyse);
			}

		}

		final String s = vo.getSort() == null ? "TABLE" : vo.getSort();
		us.getUtil().SortCollection(dataBaseDiff, s);

		if (vo.paginate()) {
			vo.put("TOTAL", Integer.toString(dataBaseDiff.size()));

			final int start = Integer.parseInt(vo.get("START").toString());
			final int limit = Integer.parseInt(vo.get("LIMIT").toString());
			int end = start + limit;
			final int size = dataBaseDiff.size();

			if (size >= limit) {
				end = size < end ? size : end;
				return slice(dataBaseDiff,start, end);
			}
		}

		return dataBaseDiff;
	}

	public void dropAllConstraints(final boolean onlyLogDDL, final Usuario user) throws SQLException, Exception {

		final Metadata metadata = getDBMetadata(user);
		final List<String> tableNames = new ArrayList<String>();

		for (final String tableName : metadata.getTables().keySet()) {
			tableNames.add(tableName);
		}
		getDBCreator(onlyLogDDL, user).dropConstraints(tableNames);
	}

	public void dropColumn(final String tableName, final String columnName, final boolean onlyLogDDL,
			final Usuario user) throws SQLException {

		getDBCreator(onlyLogDDL, user).dropColumn(tableName, columnName);
		dataBaseDiff.remove(user.getUtil()
				.match(dataBaseDiff, new String[] { "TABLE", "OBJECT" }, new String[] { tableName, columnName })
				.get(0));
	}

	public void dropTable(final String tableName, final boolean onlyLogDDL, final Usuario user) throws SQLException {

		getDBCreator(onlyLogDDL, user).dropTable(tableName);
		dataBaseDiff.remove(user.getUtil().match(dataBaseDiff, "TABLE", tableName).get(0));
	}

	public void executeSQL(final String sql, final boolean onlyLogDDL, final Usuario user) throws SQLException {
		getDBCreator(onlyLogDDL, user).executeSQL(sql);
	}

	public synchronized Map<String, Object> getDataBaseDiff(final int index, Usuario user) {

		if (dataBaseDiff == null) {
			return null;
		}
		return (Map<String, Object>) user.getUtil().match(dataBaseDiff, "ID", "" + index).get(0);
	}

	public List<Map<String, String>> getDataBaseOptions(final GenericVO vo, final Usuario user) {

		final List<Map<String, String>> labels = new ArrayList<Map<String, String>>();
		Map<String, String> label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.listarInconsistencias"));
		label.put("ID", DataBaseOptions.LIST_PROBLEMS.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.listarTabelasNaoConfiguradas"));
		label.put("ID", DataBaseOptions.NOT_CONFIGURED.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.removerRestricoes"));
		label.put("ID", DataBaseOptions.REMOVE_CONSTRAINTS.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.removerIndexes"));
		label.put("ID", DataBaseOptions.REMOVE_INDEXES.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.removerSequences"));
		label.put("ID", DataBaseOptions.REMOVE_SEQUENCES.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.executarSQL"));
		label.put("ID", DataBaseOptions.EXECUTE_SQL.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.criar"));
		label.put("ID", DataBaseOptions.CREATE_DATA_BASE.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.manager"));
		label.put("ID", DataBaseOptions.CLONE_DATA_BASE.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.processLabels"));
		label.put("ID", DataBaseOptions.PROCESS_LABELS.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.fonetic"));
		label.put("ID", DataBaseOptions.FONETIC_INDEX.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.bancodados.createConnection"));
		label.put("ID", DataBaseOptions.CREATE_CONNECTION.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.disableSetup"));
		label.put("ID", DataBaseOptions.DISABLE_SETUP.name());
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.setup.executarScript"));
		label.put("ID", DataBaseOptions.EXECUTAR_SCRIPT_RULE.name());
		labels.add(label);

		return labels;
	}

	public synchronized Map<String, Object> getDataBaseProblem(final int index, Usuario user) {

		if (dataBaseAnalys == null) {
			return null;
		}
		return (Map<String, Object>) user.getUtil().match(dataBaseAnalys, "ID", "" + index).get(0);
	}

	private DBCreator getDBCreator(final boolean onlyLogDDL, final Usuario user) throws SQLException {

		if (user.vars.get("DB_CREATOR_SETUP") != null)
			return (DBCreator) user.vars.get("DB_CREATOR_SETUP");

		final InterfaceDialeto dialeto = getDialect(user);

		final JdbcTemplate jdbcTemplate = getJDBCTemplate(user);

		final DBCreator dbCreator = new DBCreator(getParseXmlService(user), dialeto, jdbcTemplate, onlyLogDDL, user);
		user.vars.put("DB_CREATOR_SETUP", dbCreator);

		return dbCreator;
	}

	private Metadata getDBMetadata(final Usuario user) throws MetaDataAccessException, SQLException {

		if (metadata == null) {
			metadata = (Metadata) JdbcUtils.extractDatabaseMetaData(
					ConnectionManager.getInstance(user).getDataSource(user), new DAOMetadata());
		}
		return metadata;
	}

	private InterfaceDialeto getDialect(final Usuario us) {
		final InterfaceDialeto dialect = Dialeto.getInstance().getDialeto(us);
		return dialect;
	}

	private JdbcTemplate getJDBCTemplate(final Usuario user) throws SQLException {
		final JdbcTemplate jdbcTemplate = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
				createVO(user), user.getUtil());
		return jdbcTemplate;
	}

	public Map getMainField() {
		return this.mainField;
	}

	@Override
	public Object getObject() throws Exception {
		return this;
	}

	@Override
	public Class getObjectType() {
		return this == null ? this.getClass() : SetupService.class;
	}

	private ParseXmlService getParseXmlService(final Usuario user) {
		return (ParseXmlService) user.getSysParser();
	}

	public void importFile(final InputStream file, final Usuario user) throws Exception {

		new XMLImporter(this, user).process(file, null);
	}

	public void initSetup(final Usuario user) {
		cleanDataBaseAnalyse();
		validationErrors = null;
		user.vars.remove("DB_CREATOR_SETUP");
	}

	public boolean isServiceEmpty(final String serviceName, final Usuario user) throws Exception {

		final ServiceImpl service = (ServiceImpl) getService(serviceName, user);

		final GenericVO vo = service.createVO(user);
		vo.setLimit("1");
		return service.lista(vo, user).size() == 0;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}

	@Override
	public List lista(final GenericVO vo, final Usuario user) throws ConfigurationException, Exception {

		return getParseXmlService(user).lista(vo, user);
	}

	@Override
	public GenericVO obtem(final GenericVO vo, final Usuario user) throws Exception {
		return super.obtem(vo, user);
	}

	public void renameColumn(final String tableName, final String columnName, final String newColumnName,
			final boolean onlyLogDDL, final Usuario user) throws SQLException {

		getDBCreator(onlyLogDDL, user).renameColumn(tableName, columnName, newColumnName);
		dataBaseDiff.remove(user.getUtil()
				.match(dataBaseDiff, new String[] { "TABLE", "OBJECT" }, new String[] { tableName, columnName })
				.get(0));
	}

	public void renameTable(final String tableName, final String newTableName, final boolean onlyLogDDL,
			final Usuario user) throws SQLException {

		getDBCreator(onlyLogDDL, user).renameTable(tableName, newTableName);
		dataBaseDiff.remove(user.getUtil().match(dataBaseDiff, "TABLE", tableName).get(0));

	}

	public void saveConnectionParams(final GenericVO vo, final Usuario user) {
		final ParseXmlService parse = getParseXmlService(user);

		final Map<String, String> attrs = new HashMap<String, String>();
		attrs.put(ConnectionManager.URL, vo.get("DATAURL").toString());
		attrs.put("serverPort", vo.get("SERVERPORT").toString());
		attrs.put(ConnectionManager.DRIVER, vo.get("DRIVER").toString());
		attrs.put(ConnectionManager.USER, vo.get("USERNAME").toString());
		attrs.put(ConnectionManager.PASSWORD, vo.get("PASSWORD").toString());

		try {
			parse.saveAttributes(vo.get("SYSTEM").toString(), attrs);
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			throw new BusinessException("label.setup.connection.error.save");
		}
		metadata = null;
	}

	public List<Map<String, String>> semanticValidation(final GenericVO vo, final Usuario us) throws Exception {

		if (validationErrors == null) {

			validationErrors = new SemanticValidation().validate(getParseXmlService(us), us);

			String label;
			int i = 0;

			for (final Map<String, String> analyse : validationErrors) {
				label = "label.setup.validacao." + analyse.get("ERROR").toLowerCase();
				analyse.put("ERROR_TYPE", us.getLabel(label));
				analyse.put("ID", Integer.toString(i++));
			}
		}

		if (vo.paginate()) {
			vo.put("TOTAL", Integer.toString(validationErrors.size()));

			final int start = Integer.parseInt(vo.get("START").toString());
			final int limit = Integer.parseInt(vo.get("LIMIT").toString());
			int end = start + limit;
			final int size = validationErrors.size();

			if (size >= limit) {
				end = size < end ? size : end;
				return slice(validationErrors,start, end);
			}
		}

		return validationErrors;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Map setupConnection(final GenericVO vo, final Usuario user) {

		return getParseXmlService(user).getMapValuesFromXQL("/systems/system[@name='" + user.getSchema() + "']",
				Constants.name, user, true, false).get(user.getSchema());
	}

	public void dropAllIndexes(boolean logOnlyDLL, Usuario user) throws MetaDataAccessException, SQLException {
		final Metadata metadata = getDBMetadata(user);
		final List<String> tableNames = new ArrayList<String>();

		for (final String tableName : metadata.getTables().keySet()) {
			tableNames.add(tableName);
		}
		getDBCreator(logOnlyDLL, user).dropIndexes(tableNames);
	}

	public void dropAllSequences(boolean logOnlyDLL, Usuario user) throws MetaDataAccessException, SQLException {
		final Metadata metadata = getDBMetadata(user);
		final List<String> tableNames = new ArrayList<String>();

		for (final String tableName : metadata.getTables().keySet()) {
			tableNames.add(tableName);
		}
		getDBCreator(logOnlyDLL, user).dropSequences(tableNames);
	}

	public void foneticIndex(Usuario user) {
		List fonetics = (List) getParseXmlService(user).getListValuesFromXQL(

				"/systems/system[@name='" + user.getSchema() + "']/module/bean/property[@foneticSearch='true']",
				"order", user, true, true);
		String src = " sm.altera(sm.obtem(VO,user),user); ";

		for (Iterator iterator = fonetics.iterator(); iterator.hasNext();) {
			Map object = (Map) iterator.next();
			GenericService g = (GenericService) getParseXmlService(user)
					.getServiceById((String) object.get("parent_id"), user);
			GenericVO pv = g.createVO(user);
			pv.setLazyList(true);
			pv.setMatcher((String) g.getPrimaryKey().get(Constants.name));
			pv.setLimit("500");
			g.loopLista(pv, src, user);
			user.getUtil().getSoundex(user);

		}
	}

	public void createConnection(String string, String string2, String string3, String string4, String string5,
			Usuario user) {

		ConnectionManager.getInstance(user).createPool(string, string2, string3, string4, string5, user);

	}

	public void cloneDatabase(Usuario from, Usuario to, boolean create, boolean populate, List restrict)
			throws MetaDataAccessException, SQLException {
		if (from.getConnectionRoute().equalsIgnoreCase(to.getConnectionRoute()))
			throw new BusinessException("label.bancos.iguais ");
		final Metadata metadata = getDBMetadata(from);

		final InterfaceDialeto dialect = getDialect(from);

		final JdbcTemplate jdbcTemplate = getJDBCTemplate(from);

		final DAOComparator comparator = new DAOComparator(metadata, dialect, jdbcTemplate, getParseXmlService(from),
				from);
		final DBCreator dbCreator = getDBCreator(false, to);
		if (create)
			dbCreator.create(comparator.getTables(), getDBCreator(false, from));
		if (populate) {
			dbCreator.parseSchema(comparator.getTables(), getDBCreator(false, from));

			dbCreator.populateFromTemplate(jdbcTemplate, restrict);
		}

	}

	public Map<String, String> processLabels(Usuario user) {

		Map<String, String> labels = new HashMap<String, String>();

		try {
			List<Map<String, String>> pm = (List) getParseXmlService(user).getListValuesFromXQL(
					new String[] { "/systems/system[@name='" + user.getSchema() + "']//bean[@table or @label]",
							"/systems/system[@name='" + user.getSchema()
									+ "']//bean//property[not(@primaryKey='true') and not(@relationKey='true') and not(@component='hidden')]",
							"/systems/system[@name='" + user.getSchema() + "']//bean//container",
							"/systems/system[@name='" + user.getSchema() + "']//bean//method[not (@type='L')]" },
					null, user, true, true);

			for (Map<String, String> map : pm) {

				if (map.get("component") != null && map.get("value") != null
						&& (map.get("component").equals("labelTransform")
								|| map.get("component").equals("iconTransform"))) {
					List<String> l = splitLabel(map.get("value"), user);

					for (String string : l) {
						if (absentLabel(string, user)) {
							labels.put(string, "");
						}
					}
				} else if (map.get("component") == null || (!map.get("component").equals("labelTransform")
						&& !map.get("component").equals("iconTransform"))) {
					if (absentLabel(map.get("label"), user)) {
						labels.put(map.get("label"), "");
					}

					if (map.get("type").equals("scriptRule") && map.get("CDATA") != null) {
						if ((map.get("CDATA").indexOf("user.addActionMessage")) != -1
								|| map.get("CDATA").indexOf("user.be") != -1
								|| map.get("CDATA").indexOf("user.getLabel") != -1) {

							List<String> labelsSearch = searchLabel(map.get("CDATA"),
									new String[] { "user.be", "user.addActionMessage", "user.getLabel" });

							for (String search : labelsSearch) {
								if (absentLabel(search, user)) {
									labels.put(search, "");
								}
							}
						}
					}
				}
			}

			return labels;

		} catch (Exception e) {
			LogUtil.exception(e, user);
			// e.printStackTrace();
		}

		return labels;
	}

	/**
	 * Procurando labels dos metodos "user.addActionMessage", "user.be" e
	 * "user.getLabel"
	 */

	public static List<String> searchLabel(String script, String[] search) {

		List<String> labels = new ArrayList<String>();

		for (int i = 0; i < search.length; i++) {

			int count = 0;
			while (script.indexOf(search[i], count) != -1) {
				count = script.indexOf(search[i], count) + search[i].length();
				String valor = "";
				if (script.substring(count + 1, count + 2).equals("'")
						|| script.substring(count + 1, count + 2).equals("\"")) {
					valor = script.substring(count + 2,
							script.indexOf(script.substring(count + 1, count + 2), count + 2));

					// Tem que conter pelo menos um digito[a-zA-Z0-9] no pode
					// conter qualquer
					// tipo de espao e no pode acabar com ponto[^\\.]
					if (valor.matches("\\w+[^\\s]*[^\\.]$")) {
						labels.add(valor);
					}
				}
			}
		}
		return labels;
	}

	/**
	 * Transformado labelTransform ou iconTransform em map
	 */

	public List<String> splitLabel(String label, Usuario user) {
		List<String> labelsList = new ArrayList<String>();
		Map<String, Object> lb = user.getUtil().JSONtoMap(label);

		for (java.util.Map.Entry<String, Object> it : lb.entrySet()) {
			labelsList.add(it.getValue().toString());
		}
		return labelsList;
	}

	/**
	 * Comparando para ver labels inexistentes na base
	 */

	public Boolean absentLabel(String label, Usuario user) {

		try {
			if (user.getLabel(label).equals(label)) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			LogUtil.exception(e, user);
			// e.printStackTrace();
		}
		return false;

	}

	public void disableSetup(Usuario user) {
		final Map<String, String> attrs = new HashMap<String, String>();
		attrs.put("setup", Constants.false_str);

		try {
			getParseXmlService(user).saveAttributesInSystems(attrs);
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			throw new BusinessException("label.setup.disable.setup.error");
		}
	}
}
