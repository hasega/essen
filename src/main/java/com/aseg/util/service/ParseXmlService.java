package com.aseg.util.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import com.aseg.config.Constants;
import com.aseg.exceptions.ConfigurationException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.vo.GenericVO;

import net.sf.ehcache.Cache;

public class ParseXmlService extends ServiceImpl implements FactoryBean, InitializingBean, ParseXml {
    public ParseXmlService(){
        System.out.println("new parser");
    }

	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private static String attValue(final Node n, String att) {
		try {
			if (n.getNodeName().equalsIgnoreCase("method") && att.equalsIgnoreCase("id")) {
				att = Constants.name;
			}
			return n.getAttributes().getNamedItem(att).getNodeValue();
		} catch (final Exception e) {
			return null;
		}

	}

	private static Node findParentByType(final Node n, final String type) {
		if (n.getNodeName().equalsIgnoreCase(type)) {
			return n;
		} else if (n.getParentNode() != null) {
			return findParentByType(n.getParentNode(), type);
		} else {
			return null;
		}
	}

	// static void printElementAttributes(final Document doc) {
	// final NodeList nl = doc.getElementsByTagName("*");
	// Element e;
	// Node n;
	// NamedNodeMap nnm;
	//
	// String attrname;
	// String attrval;
	// int i, len;
	//
	// len = nl.getLength();
	//
	// for (int j = 0; j < len; j++) {
	// e = (Element) nl.item(j);
	// LogUtil.debug(e.getTagName() + ":", user);
	// nnm = e.getAttributes();
	//
	// if (nnm != null) {
	// for (i = 0; i < nnm.getLength(); i++) {
	// n = nnm.item(i);
	// attrname = n.getNodeName();
	// attrval = n.getNodeValue();
	// LogUtil.debug(" " + attrname + " = " + attrval, user);
	// }
	// }
	// LogUtil.debug("", user);
	// }
	// }
	//
	// static void printElements(final Document doc) {
	// final NodeList nl = doc.getElementsByTagName("*");
	// Node n;
	//
	// for (int i = 0; i < nl.getLength(); i++) {
	// n = nl.item(i);
	// LogUtil.debug(n.getNodeName() + " ", user);
	// }
	//
	// }

	@Override
	public Map<String, String> processFilterAdapter(final Map<String, String> hashtable, final Usuario user) {
		if (hashtable.get("primaryKey") != null)
			return null;

		if (hashtable.get("ignoreAdapter") != null
				&& hashtable.get("ignoreAdapter").toString().toLowerCase().indexOf("f") > -1)
			return null;

		if (
		// hashtable.get("volatile") != null
		// && !hashtable.get("type").toString()
		// .equalsIgnoreCase("context")
		// ||
		hashtable.get("primaryKey") != null
				// || hashtable.get("type").toString().equalsIgnoreCase("fake")
				|| hashtable.get(Constants.component) != null
						&& (hashtable.get(Constants.component).toString().equalsIgnoreCase("scriptRule")
								|| hashtable.get(Constants.component).toString().equalsIgnoreCase("grid")
								|| hashtable.get(Constants.component).toString().equalsIgnoreCase("hidden"))) {
			return null;
		}

		if (hashtable.get("xtype").toString().equalsIgnoreCase("property")) {

			trataCampos(hashtable);

			if (hashtable.get("service") != null) {
				if (hashtable.get("service").toString().indexOf(".") > 0) {
					hashtable.put("serviceMethod", hashtable.get("service").toString().split("\\.")[1]);
					hashtable.put("service", hashtable.get("service").toString().split("\\.")[0]);
				} else {
					if (hashtable.get("serviceMethod") == null) {
						hashtable.put("serviceMethod", "lista");
					}
				}
				if (hashtable.get("component") != null
						&& hashtable.get("component").toString().equalsIgnoreCase("checkbox"))
					hashtable.put("type", "bit");
				else
					hashtable.put("type", "alfa");
				// **MATCHSER

				user.getUtil().matchService(hashtable, hashtable.get(Constants.name), null, false, user);

			}
			if (hashtable.get("component") != null
					&& hashtable.get("component").toString().equalsIgnoreCase("checkbox"))
				hashtable.put("type", "bit");

			if (hashtable.get("foneticSearch") != null
					&& hashtable.get("foneticSearch").toString().equalsIgnoreCase("true"))
				hashtable.put("type", "charF");

			if (user != null) {
				hashtable.put("id", hashtable.get(Constants.name));
			}

		}

		return hashtable;
	}

	public static Map<String, String> trataCampos(final Map<String, String> e) {

		if (e == null) {
			return null;
		}
		if (e.get("mainField") != null && e.get("calculated") == null) {
			e.put("required", Constants.true_str);
		}

		if (e.get("type") == null) {
			e.put("type", "alfa");
		}

		if (e.get(Constants.component) == null) {
			e.put(Constants.component, e.get("type"));
		}
		if (e.get("span") == null) {
			e.put("span", "1");
		}
		if (e.get("primaryKey") != null && !e.get(Constants.component).equalsIgnoreCase("context")) {
			e.put(Constants.component, "hidden");
		}
		if (e.get("type").equals("money")) {
			if (e.get("precision") == null) {
				e.put("precision", "2");
			}
		}

		if (e.get(Constants.component).equals("memo") && !e.get("type").equalsIgnoreCase("text")) {
			if (e.get("maxlength") == null) {
				e.put("maxlength", "4000");
			}
		}

		if (e.get("listable") != null && e.get("listable").indexOf(';') > -1 && !e.get("listable").startsWith("${")) {
			e.put("width", e.get("listable").split(";")[1]);

		}

		if (e.get("type").equalsIgnoreCase("null")) {
			e.put("mask", "9");
		}
		if (e.get(Constants.component).equalsIgnoreCase("tab")) {
			e.put("action", e.get("service"));
		}

		return MapUtils.unmodifiableMap(e);
	}

	private String adapter;

	private static DocumentBuilder builder;

	// private Cache cache;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aseg. ((UtilServiceImpl) getService("UtilService",
	 * user)).ParseXml#getHashValuesFromXQL(java.lang .String, java.lang.String)
	 */

	private File configLocation;

	private Document document;

	// boolean usecache = false;

	XPath xpath = XPathFactory.newInstance().newXPath();

	private Map<String, String> adapterTransform(final Map<String, String> hashtable, final Usuario user) {

		if (getAdapter() != null && getAdapter().equalsIgnoreCase("filter")) {
			return processFilterAdapter(hashtable, user);
		}

		if (getAdapter() != null && getAdapter().equalsIgnoreCase("report")) {
			return processReportAdapter(hashtable, user);
		}

		return hashtable;

	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// Assert.notNull(cache, "Um cache requerido para este bean.");
		// cache.removeAll();
		Assert.notNull(configLocation, "Um arquivo de configuracao  requerido para este bean.");
		builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		document = builder.parse(new FileInputStream(configLocation));
		// if (!user.s.production_env) {

		final FileChangeListener fl = new FileChangeListener() {

			@Override
			public void fileChanged(final String fileName) {
                try {
                    builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    document = builder.parse(new FileInputStream(configLocation));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Usuario.getFactory().reloadFactory();
				Usuario.getPerfis().clear();
			}
		};
		FileMonitor.getInstance().addFileChangeListener(fl, configLocation.getAbsolutePath(), 1000);
		// }
		// printElementAttributes(document);
		final XMLReader xr = XMLReaderFactory.createXMLReader();
		// SAXHandler handler = new SAXHandler();
		// xr.setContentHandler(handler);
		// xr.setErrorHandler(handler);
		// List p = (List) getProperties().get(Constants.ALL_FIELDS);
		// for (final Iterator iterator = p.iterator(); iterator.hasNext();) {
		// final Map object = (Map) iterator.next();
		// trataCampos(object);
		// }
		//
		// p = getMethods(null);
		// if (p != null) {
		// for (final Iterator iterator = p.iterator(); iterator.hasNext();) {
		// final Map object = (Map) iterator.next();
		// final List c = (List) object.get(Constants.ALL_FIELDS);
		// for (final Iterator i = c.iterator(); i.hasNext();) {
		// final Map cd = (Map) i.next();
		// trataCampos(cd);
		// }
		// }
		// }

		// xr.parse(new InputSource(configLocation.getInputStream()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.aseg. ((UtilServiceImpl) getService("UtilService",
	 * user)).ParseXml#getListValuesFromXQL(java.lang .String)
	 */
	public static boolean isMyField(final Map a, final Usuario user) {
		if (a.get("client") != null) {
			int res = clientCanAccess(a, user);
			if (res != 0) {
				return res == 1 ? true : false;
			}
		}
		return true;
	}

	public static boolean canAccess(final Map a, final Usuario user, boolean restrict) {

		if (!restrict) {
			return true;
		}
		List r = new ArrayList();
		if (user == null || a == null) {
			return false;
		}

		if (a.get("client") != null) {
			int res = clientCanAccess(a, user);
			if (res != 0) {
				return res == 1 ? true : false;
			}
		}

		if (user.isSuperUser()) {
			return true;
		}

		String v = user.getSystemProperty("RESTRICT");
		if (v != null) {
			v = v.replaceAll(" ", "");
			r = java.util.Arrays.asList(v.split(","));
		}
		if (a.get("xtype") != null && a.get("xtype").equals("system")) {
			return true;
		}

		if (a.get("xtype") != null && a.get("xtype").equals("bean") && a.get("table") != null) {// modulo
			// inteiro
			if (user.verificaPermisao(a.get("label").toString().hashCode())) {
				return !r.contains(a.get("table").toString());
			}

		} else {
			if (a.get("label") != null) {// propriedades e metodos
				if (user.verificaPermisao(a.get("label").toString().hashCode())
						&& !r.contains(a.get("label").toString())) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		}
		return false;
	}

	public static int clientCanAccess(Map a, Usuario user) {
		// Retorna 0 caso no tenha tido efeito nenhum. 1 se tem acesso e -1 se
		// no tem acesso.

		int aux = 0;
		String s = user.getSystemProperty("SIGLA_CLIENTE");
		if ( s == null)
			s = user.getSystem();
		List sd = Arrays.asList(a.get("client").toString().split(","));

		if (s != null && sd.contains("!" + s))
			aux = -1;

		boolean ret = (s != null && sd.contains(s));
		if (!ret) {
			if (a.get("client").toString().indexOf("!" + s) == -1 && a.get("client").toString().indexOf("!") > -1) {
				aux = 1;
			} else {
				aux = -1;
			}

		}
		return aux;
	}

	// systems
	@Override
	public int countXQL(final String s, final Usuario user, boolean restriction) {
		return getNumberFromXQL("count(" + s + ")", user, restriction).intValue();
	}

	public String getAdapter() {
		return adapter;
	}

	public long getCheckSum(Usuario user) {
		return user.getUtil().getChecksumValue(configLocation.getAbsolutePath());
	}

	@Override
	public Collection<String> getListFromXQL(final String xql, final Usuario user) {

		try {
			final NodeList nodelist = (NodeList) xpath.evaluate(xql, document, XPathConstants.NODESET);

			final Collection<String> list = new HashSet<String>(nodelist.getLength());

			for (int i = 0; i < nodelist.getLength(); i++) {
				list.add(nodelist.item(i).getNodeValue());
			}
			return list;

		} catch (final XPathExpressionException e) {
			LogUtil.exception(e, user);
		}
		return null;
	}

    @Override
    public Map getMapFromXQL(String xql, Usuario user) {
        try {
            final NodeList nodelist = (NodeList) xpath.evaluate(xql, document, XPathConstants.NODESET);

            final HashMap list = new HashMap<String,Object>();

            for (int i = 0; i < nodelist.getLength(); i++) {
                final Element elem = (Element) nodelist.item(i);
                final NamedNodeMap n5 = elem.getAttributes();

                list.put("xtype", elem.getNodeName());

                if (elem.getTextContent() != null && !elem.getTextContent().trim().equals("")
                        && (list.get("xtype").toString().equalsIgnoreCase("property")
                        || list.get("xtype").toString().equalsIgnoreCase("container"))
                        || list.get("xtype").toString().equalsIgnoreCase("data")
                        || list.get("xtype").toString().equalsIgnoreCase("dbProcedure")
                        || list.get("xtype").toString().equalsIgnoreCase("clientScript")) {
                    list.put("CDATA", elem.getTextContent());
                } else if (list.get("xtype").toString().equalsIgnoreCase("bean")) {
                    list.put("hashCode", String.valueOf(getStringFromNode(elem).hashCode()));
                }
                for (int b = 0; b < n5.getLength(); b++) {
                    final Node attribute = n5.item(b);
                    list.put(attribute.getNodeName(), attribute.getNodeValue());
                }

            }
            return list;

        } catch (final XPathExpressionException e) {
            LogUtil.exception(e, user);
        }
        return null;
    }

    public Collection<Map<String, String>> getListChildsFromXQL(final String s, final String sort, final Usuario user) {
		final ArrayList<Map<String, String>> hashmap = new ArrayList<Map<String, String>>();

		LogUtil.debug(" chamado " + s, user);

		try {

			HashMap<String, String> hashMap;
			final NodeList nodelist = (NodeList) xpath.evaluate(s, document, XPathConstants.NODESET);

			if (nodelist == null) {
				return hashmap;
			}
			// Process the elements in the nodelist
			for (int i = 0; i < nodelist.getLength(); i++) {

				hashMap = new HashMap<String, String>(nodelist.getLength());
				// Get element

				final Element elem = (Element) nodelist.item(i);
				final NodeList n5 = elem.getChildNodes();

				for (int a = 0; a < n5.getLength(); a++) {
					Object o = n5.item(a);

					final Node el = (Node) o;

					hashMap.put(el.getNodeName(), el.getTextContent());

				}

				Map<String, String> aux = hashMap;
				hashmap.add(aux);
			}

		} catch (final Exception exception) {
			LogUtil.exception(exception, user);
		}
		if (sort != null) {
			user.getUtil().SortCollection(hashmap, sort);
		}

		return hashmap;

	}

	@Override
	public Collection<Map<String, String>> getListValuesFromXQL(final String s, String sort, final Usuario user,
			final boolean processAdapter, boolean restriction) {
		final ArrayList<Map<String, String>> hashmap = new ArrayList<Map<String, String>>();

		Long tini = System.nanoTime();

		net.sf.ehcache.Element element = null;
		String cacheKey = null;
		// if (cache != null) {
		// cacheKey = s + "*L*" + String.valueOf(sort);
		//
		// element = cache.get(cacheKey);
		// }
		//
		// if (element == null || !usecache) {
		try {
			HashMap<String, String> hashMap;
			final NodeList nodelist = (NodeList) xpath.evaluate(s, document, XPathConstants.NODESET);

			if (nodelist == null) {
				return hashmap;
			}
			// Process the elements in the nodelist
			for (int i = 0; i < nodelist.getLength(); i++) {
				hashMap = new HashMap<String, String>(nodelist.getLength());
				// Get element
				final Element elem = (Element) nodelist.item(i);
				final NamedNodeMap n5 = elem.getAttributes();
				hashMap.put("xtype", elem.getNodeName());

				if (elem.getTextContent() != null && !elem.getTextContent().trim().equals("")
						&& (hashMap.get("xtype").toString().equalsIgnoreCase("property")
								|| hashMap.get("xtype").toString().equalsIgnoreCase("container"))
						|| hashMap.get("xtype").toString().equalsIgnoreCase("data")
						|| hashMap.get("xtype").toString().equalsIgnoreCase("dbProcedure")
						|| hashMap.get("xtype").toString().equalsIgnoreCase("clientScript")) {
					hashMap.put("CDATA", elem.getTextContent());
				} else if (hashMap.get("xtype").toString().equalsIgnoreCase("bean")) {
					hashMap.put("hashCode", String.valueOf(getStringFromNode(elem).hashCode()));
				}
				for (int b = 0; b < n5.getLength(); b++) {
					final Node attribute = n5.item(b);
					hashMap.put(attribute.getNodeName(), attribute.getNodeValue());
				}
				if (hashMap.get("master") == null) {

					hashMap.put("parent_node", elem.getParentNode().getNodeName());
					hashMap.put("parent_id", attValue(elem.getParentNode(), "id"));
					hashMap.put("master", attValue(findParentByType(elem, "bean"), "master"));
					hashMap.put("table", attValue(findParentByType(elem, "bean"), "table"));

				}

				Map<String, String> aux = hashMap;
				String id = "";
				String lmaster = "";
				if (aux.get("xtype").equalsIgnoreCase("property") || aux.get("xtype").equalsIgnoreCase("container")
						|| aux.get("xtype").equalsIgnoreCase("method")) {
					if (aux.get("table") != null) {

						if (countXQL("/systems/system[@name='" + user.getSchema() + "']//bean[@table='"
								+ aux.get("table") + "' and @type='M'  and  not(@extends)]", user, restriction) > 1) {
							id = "." + aux.get("parent_id");
						}

						aux.put("label", aux.get("table") + id + "." + aux.get(Constants.name));
					} else {
						if (aux.get("parent_id") != null) {
							lmaster = aux.get("parent_id");
						}

						if (lmaster == null || lmaster.trim().equals("")) {
							lmaster = "";
						} else {
							lmaster = lmaster + ".";
						}
						aux.put("label", lmaster + aux.get(Constants.name));
					}
					trataCampos(aux);
				} else if (aux.get("xtype").equalsIgnoreCase("bean") && aux.get("label") == null) {

					if (countXQL("/systems/system[@name='" + user.getSchema() + "']//bean[@table='" + aux.get("table")
							+ "' and @type='M' and  not(@extends)]", user, restriction) > 1) {
						id = "." + aux.get("id");
					}

					aux.put("label", aux.get("table") + id);
				}

				if (processAdapter) {
					aux = adapterTransform(aux, user);
				}

				if (canAccess(hashMap, user, restriction)) {
					String sx = "";
					if (sort != null && sort.startsWith("${")) {
						sx = (String) user.getUtil().Evaluate(sort, user.getUtil().createContext(aux, user));
						aux.put("sort_calc", sx);
						//
					}

					hashmap.add(aux);
				}
			}

		} catch (final Exception exception) {
			// LogUtil.exception(exception,user);
		}
		if (sort != null) {
			if (sort.startsWith("${"))
				sort = "sort_calc";
			user.getUtil().SortCollection(hashmap, sort);
		}

		// System.out.println(" chamado time "+ (System.nanoTime()-tini) + s);

		// if (!usecache) {
		return hashmap;
		// }
		// element = new net.sf.ehcache.Element(cacheKey, hashmap);
		// cache.put(element);
		// }
		// return (Collection) element.getObjectValue();
		// return hashmap;
	}

	// private boolean verificaUnicoModulo(Object a) {
	// try {
	// if (a == null || a.toString().length() == 0) {
	// return true;
	// }
	//
	// if (a.toString().startsWith("!")) {
	// boolean verifica = Boolean.valueOf(
	// rb.getString(a.toString().substring(1))).booleanValue();
	// return !verifica;
	// } else {
	// boolean verifica = Boolean.valueOf(rb.getString(a.toString()))
	// .booleanValue();
	//
	// return verifica;
	// }
	//
	// } catch (Exception e) {
	// throw new EssenException(e);
	// }
	//
	// }

	@Override
	public Collection<Map<String, String>> getListValuesFromXQL(final String[] s, final String sort, final Usuario user,
			final boolean processAdapter, boolean restriction) {

		final Collection<Map<String, String>> c = new ArrayList<Map<String, String>>();

		for (final String element : s) {
			c.addAll(getListValuesFromXQL(element, sort, user, processAdapter, restriction));
		}
		return c;
	}

	@Override
	public Map<String, Map<String, String>> getMapValuesFromXQL(final String s, final String s1, final Usuario user,
			final boolean processAdapter, boolean restriction) {

		// LogUtil.debug(" chamado hash >> " + s);
		final HashMap<String, Map<String, String>> hashmap = new HashMap<String, Map<String, String>>();
		net.sf.ehcache.Element element = null;
		String cacheKey = null;
		// if (cache != null) {
		// cacheKey = s + "*H*" + s1;
		//
		// element = cache.get(cacheKey);
		// }
		// if (element == null || !usecache) {
		try {

			Map<String, String> hashtable;

			final NodeList nodelist = (NodeList) xpath.evaluate(s, document, XPathConstants.NODESET);
			// Process the elements in the nodelist
			for (int i = 0; i < nodelist.getLength(); i++) {

				hashtable = new HashMap<String, String>(nodelist.getLength());

				// Get element
				final Element elem = (Element) nodelist.item(i);
				final NamedNodeMap n5 = elem.getAttributes();
				hashtable.put("xtype", elem.getNodeName());

				if (elem.getTextContent() != null && elem.getTextContent().toString().trim().length() > 0
						&& (hashtable.get("xtype").toString().equalsIgnoreCase("property")
								|| hashtable.get("xtype").toString().equalsIgnoreCase("container"))) {

					hashtable.put("CDATA", elem.getTextContent());
				} else if (hashtable.get("xtype").toString().equalsIgnoreCase("bean")) {
					hashtable.put("hashCode", String.valueOf(getStringFromNode(elem).hashCode()));
				}
				for (int b = 0; b < n5.getLength(); b++) {
					final Node attribute = n5.item(b);
					hashtable.put(attribute.getNodeName(), attribute.getNodeValue());

				}

				if (hashtable.get("master") == null) {

					hashtable.put("parent_node", elem.getParentNode().getNodeName());
					hashtable.put("parent_id", attValue(elem.getParentNode(), "id"));
					hashtable.put("master", attValue(findParentByType(elem, "bean"), "id"));
					hashtable.put("table", attValue(findParentByType(elem, "bean"), "table"));

				}

				String id = "";

				Map<String, String> aux = hashtable;
				if (aux.get("xtype").equalsIgnoreCase("property")) {
					String lmaster = "";
					if (countXQL("/systems/system[@name='" + user.getSchema() + "']//bean[@table='" + aux.get("table")
							+ "' and @type='M' and  not(@extends)]", user, restriction) > 1) {
						id = "." + aux.get("parent_id");
					}

					if (aux.get("table") != null) {
						aux.put("label", aux.get("table") + id + "." + aux.get(Constants.name));
					} else {
						lmaster = aux.get("parent_id");

						if (lmaster == null || lmaster.trim().equals("")) {
							lmaster = "";
						} else {
							lmaster = lmaster + ".";
						}
						aux.put("label", lmaster + aux.get(Constants.name));
					}
					trataCampos(aux);
				} else if (aux.get("xtype").equalsIgnoreCase("bean") && aux.get("label") == null) {
					if (countXQL("/systems/system[@name='" + user.getSchema() + "']//bean[@table='" + aux.get("table")
							+ "' and @type='M' and  not(@extends)]", user, restriction) > 1) {
						id = "." + aux.get("id");
					}

					aux.put("label", aux.get("table") + id);
				}
				if (canAccess(aux, user, restriction)) {
					if (processAdapter) {
						aux = adapterTransform(aux, user);
					}
					if (aux != null) {
						hashmap.put(aux.get(s1), aux);
					}
				}
			}

		} catch (final Exception exception) {
			exception.printStackTrace();
		}
		// if (!usecache) {
		return hashmap;
		// }
		// element = new net.sf.ehcache.Element(cacheKey, hashmap);
		// cache.put(element);
		// }
		// return hashmap;

		// final Map<String, Map<String, String>> out = (Map<String, Map<String,
		// String>>) element
		// .getValue();
		// return out;

	}

	@Override
	public Map<String, Map<String, String>> getMapValuesFromXQL(final String[] s, final String s1, final Usuario user,
			final boolean processAdapter, boolean restriction) {

		final Map<String, Map<String, String>> c = new HashMap<String, Map<String, String>>();

		for (final String element : s) {
			c.putAll(getMapValuesFromXQL(element, s1, user, processAdapter, restriction));
		}
		return c;
	}

	@Override
	public Double getNumberFromXQL(final String query, final Usuario user, boolean restriction) {

		try {
			return (Double) xpath.evaluate(query, document, XPathConstants.NUMBER);
		} catch (final XPathExpressionException e) {
			// LogUtil.exception(e,user);
		}
		return new Double(-1);
	}

	@Override
	public Object getObject() throws Exception {
		return this;
	}

	@Override
	public Class<? extends ParseXmlService> getObjectType() {
		return this == null ? this.getClass() : ParseXmlService.class;
	}

	@Override
	public InputStream getStreamConfig() throws IOException {
		return new FileInputStream(configLocation);
	}

	public String getStringFromNode(final Node node) {
		try {
			final DOMSource domSource = new DOMSource(node);
			final StringWriter writer = new StringWriter();
			final StreamResult result = new StreamResult(writer);
			final TransformerFactory tf = TransformerFactory.newInstance();
			final Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			writer.flush();
			return writer.toString();
		} catch (final TransformerException ex) {
			LogUtil.exception(ex, null);
			return null;
		}
	}

	@Override
	public String getValueFromXQL(final String s, final String value, final Usuario user, final boolean processAdapter,
			boolean restriction) {
		final Map<String, Map<String, String>> a = getMapValuesFromXQL(s, value, user, true, restriction);
		final Iterator<String> i = a.keySet().iterator();
		while (i.hasNext()) {
			final String type = i.next();
			return a.get(type).get(value);
		}
		return "";

	}

	@Override
	public String getXmlValue(final String name, final String value, final Usuario user, boolean restriction) {
		return "<" + name + ">" + value + "</" + name + ">";
	}

	@Override
	public boolean isSingleton() {
		return false;
	}

	@Override
	public List describe(final GenericVO vo, final Usuario user) {
		final ServletRequest req = (ServletRequest) vo.get(Constants.RequestProcessed);

		// vo.setEncapsulate(false);
		String item = (String) vo.get("NODE");
		final String Type = (String) vo.get("LABELPROPERTY");
		final String navigate = (String) vo.get("MODEL");
		boolean for_checked = false;

		if (vo.get("FOR_CHECKED") != null && vo.get("FOR_CHECKED").toString().equalsIgnoreCase(Constants.true_str)) {
			for_checked = true;
		}

		final ParseXml ParseFilter = (ParseXml) user.getFactory().getSpringBean("ParseFilter", user);

		final Map items = ParseFilter
				.getMapValuesFromXQL(
						new String[] { "/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + navigate
								+ "' or (@master='" + navigate + "' and @type='D')]/property", },
						"label", user, true, true);

		items.putAll(
				user.getUtil().modifiedMap(
						ParseFilter
								.getMapValuesFromXQL(
										new String[] {
												"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='"
														+ navigate + "']/method[@type!='L']/property", },
										"label", user, true, true),
						"parent_id", navigate));

		final Map cmp1 = new HashMap();
		cmp1.put(Constants.name, "ID_USUARIO_CADASTRO");
		cmp1.put("colspan", "1");
		cmp1.put("xtype", "property");
		cmp1.put("label", "ID_USUARIO_CADASTRO");
		cmp1.put(Constants.component, "select");
		cmp1.put("key", "ID_USUARIO");
		cmp1.put("labelValue", "NOME");
		cmp1.put("service", "UsuarioService");
		cmp1.put("serviceParams", "{SEGMENTBY:'false'}");
		cmp1.put("type", "num");
		items.put("ID_USUARIO_CADASTRO", ParseFilter.processFilterAdapter(cmp1, user));

		final Map cmp2 = new HashMap();
		cmp2.put(Constants.name, "DATA_CADASTRO");
		cmp2.put("label", "DATA_CADASTRO");
		cmp2.put("xtype", "property");
		cmp2.put("type", "date");
		cmp2.put(Constants.component, "date");
		items.put("DATA_CADASTRO", ParseFilter.processFilterAdapter(cmp2, user));

		Map cmp3 = new HashMap();
		cmp3.put(Constants.name, "ULTIMA_MODIFICACAO");
		cmp3.put("label", "ULTIMA_MODIFICACAO");
		cmp3.put("xtype", "property");
		cmp3.put("type", "date");
		cmp3.put(Constants.component, "date");
		items.put("ULTIMA_MODIFICACAO", ParseFilter.processFilterAdapter(cmp3, user));
		GenericService s = (GenericService) getServiceById(navigate, user);
		Map ac = user.getUtil().JSONtoMap((String) s.getProperties().get("acceptSharing"));

		if (ac.size() > 0) {

			Iterator i = ac.keySet().iterator();
			while (i.hasNext()) {
				String object = (String) i.next();
				if (canAccess(((GenericService) getServiceById(object, user)).getProperties(), user, true)) {
					GenericService ss = (GenericService) getServiceById(object, user);

					if (ss == null) // sharing inexistente
						continue;
					Map z = user.getUtil().JSONtoMap((String) ss.getProperties().get("sharing"));
					if (z.size() > 0) {
						if ("item".equalsIgnoreCase((String) z.get("type"))) {

							if (ss.getMainField("inclui", user) == null)
								continue;
							cmp3 = new HashMap();
							cmp3.put(Constants.name, "SHARING_" + ss.getProperties().get("id"));
							cmp3.put("label", ss.getProperties().get("label"));
							cmp3.put("xtype", "property");
							cmp3.put("filtro", "{gA:[{c:{f:'CONTEXTO',o:'=',vc:'" + s.getServiceId() + "'}}]}");
							cmp3.put("type", "array");
							cmp3.put(Constants.component, "date");
							cmp3.put("service", ss.getName());
							cmp3.put("key", ss.getPrimaryKey().get(Constants.name));
							cmp3.put("labelValue", ss.getMainField("inclui", user).get(Constants.name));
							items.put(ss.getProperties().get("label"), ParseFilter.processFilterAdapter(cmp3, user));

						}

					}

				}
			}
		}

		String campo1 = null;
		final Object[] array_obj = items.keySet().toArray();
		final int tam1 = items.size();
		int aux1 = -1;
		String master = null;
		String res = "[";
		for (int u = 0; u < array_obj.length; u++) {
			aux1++;
			campo1 = array_obj[u].toString();
			final HashMap values = (HashMap) items.get(campo1);
			if ((values.get("volatile") != null) && values.get("component") != null
					&& !values.get("component").equals("context")) {
				continue;
			}
			if (values.get("filtro") != null

					&& values.get("filtro").equals("false")) {
				continue;
			}
			if (values.get("type") != null

					&& values.get("type").equals("file")) {
				continue;
			}
			if (values.get("primaryKey") != null

					&& values.get("primaryKey").equals("true")) {
				continue;
			}
			if (values.get("service") != null && navigate.equals(values.get("parent_id"))) {
				values.put("serviceID",
						((GenericService) getService((String) values.get("service"), user)).getProperties().get("id"));
			}
			// else
			// {
			// values.put("serviceID",navigate);
			// }
			if (!navigate.equals(values.get("master"))) {
				master = user.getLabel(ParseFilter.getValueFromXQL("/systems/system[@name='" + user.getSchema()
						+ "']/module/bean[@id='" + values.get("master") + "']", "table", user, true, true));
			} else {
				master = "";
			}

			if (!navigate.equalsIgnoreCase(campo1)) {
				String l = user.getLabel(campo1);

				if (master != null && !master.equals("")) {
					l = l + " (" + master + ")";
				}

				if (values.get("parent_id") == null) {
					values.put("parent_id", vo.get("model"));
				}

				res += " ['" + values.get("type") + "'," + u + ",'" + values.get("id") + "','" + l + "','" + master
						+ "','" + values.get("service") + "','" + values.get("labelValue") + "','" + values.get("key")
						+ "','" + values.get("serviceMethod") + "','" + values.get("parent_id") + "','"
						+ values.get("maxLength") + "','" + l + "','" + values.get("serviceID") + "','"
						+ user.getUtil().encode((String) values.get("filtro"), user) + "']";

				res += ",";

			}
		}
		res = res.trim().substring(0, res.lastIndexOf(","));
		res += " ]";

		List a = new ArrayList();
		Map c = new HashMap();
		c.put("TITLE",
				user.getLabel((String) ((GenericService) getServiceById(navigate, user)).getProperties().get("label")));
		c.put("ITEMS", res);
		a.add(c);
		return a;

	}

	public Object getServiceById(final String name, final Usuario user) {
		final String Name = getValueFromXQL(
				"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + name + "']", Constants.name,
				user, true, false);
		try {
			if (Name.length() == 0) {
				return null;
			}
			return user.getFactory().getSpringBean(Name + "Service", user);
		} catch (final Exception e) {
			return null;
		}
	}

	public List<Map<String, String>> lista(final GenericVO vo, final Usuario user) throws Exception {
		//
		// if (vo.getFiltro() != null && !vo.getFiltro().trim().startsWith("/"))
		// {
		// return super.lista(vo, user);
		// }

		final List<Map<String, String>> list = (List<Map<String, String>>) getListValuesFromXQL(vo.getFiltro(),
				vo.getSort(), user, true, true);

		if (vo.getMapping() == null) {
			return user.getUtil().ManageCollection(list, "rename", "*", "upper");
		} else {
			return user.getUtil().mappingCollection(list, vo.getMapping(), "U", true, user);
		}
	}

	public GenericVO obtem(final String id, final String Filter, final Usuario user) throws Exception {
		return new GenericVO().putAllAndGet(
				getMapValuesFromXQL("/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + id + "']",
						"id", user, true, true).get(id),
				true);
	}

	private Map<String, String> processReportAdapter(final Map<String, String> hashtable, final Usuario user) {
		if (hashtable.get("primaryKey") != null)
			return null;

		if (hashtable.get("ignoreAdapter") != null
				&& hashtable.get("ignoreAdapter").toString().toLowerCase().indexOf("r") > -1)
			return null;

		if (
		// hashtable.get("volatile") != null
		// && !hashtable.get("type").toString()
		// .equalsIgnoreCase("context")
		// ||
		hashtable.get("primaryKey") != null
				// || hashtable.get("type").toString().equalsIgnoreCase("fake")
				|| hashtable.get(Constants.component) != null
						&& (hashtable.get(Constants.component).toString().equalsIgnoreCase("scriptRule")
								|| hashtable.get(Constants.component).toString().equalsIgnoreCase("grid")
								|| hashtable.get(Constants.component).toString().equalsIgnoreCase("hidden"))) {
			return null;
		}
		return hashtable;
	}

	public String remove(final String[] ids, final Usuario user) throws Exception {
		return String.valueOf(ids.length);
	}

	public void saveAttributes(final String systemName, final Map<String, String> attributes)
			throws ConfigurationException, FileNotFoundException, IOException, TransformerException {

		String xpathExpression = "/systems/system[@name='" + systemName + "']";

		Node element = getNodeFromXpathExpression(xpathExpression);

		if (element == null) {
			throw new ConfigurationException("Sistema com nome " + systemName + " no foi encontrado.",
					ConfigurationException.TYPE.SYSTEM_NOT_FOUND);
		}

		updateElementAttributes(attributes, element);
		updateConfigFile();

	}

	public void saveAttributesInSystems(final Map<String, String> attributes) throws TransformerConfigurationException,
			FileNotFoundException, TransformerFactoryConfigurationError, IOException, TransformerException {

		String xpathExpression = "/systems";

		Node element = getNodeFromXpathExpression(xpathExpression);

		if (element == null) {
			throw new ConfigurationException("No foi possvel localizar o elemento systems",
					ConfigurationException.TYPE.SYSTEM_NOT_FOUND);
		}

		updateElementAttributes(attributes, element);
		updateConfigFile();

	}

	private Node getNodeFromXpathExpression(String xpathExpression) {
		Node element = null;
		try {
			element = (Node) xpath.evaluate(xpathExpression, document, XPathConstants.NODE);
		} catch (final XPathExpressionException e1) {
			LogUtil.exception(e1, null);
		}
		return element;
	}

	private void updateElementAttributes(final Map<String, String> attributes, Node element) {
		final NamedNodeMap attrs = element.getAttributes();
		Node nodeAttr;

		for (final Map.Entry<String, String> entry : attributes.entrySet()) {
			nodeAttr = attrs.getNamedItem(entry.getKey());

			if (nodeAttr == null) {
				nodeAttr = document.createAttribute(entry.getKey());
				element.appendChild(nodeAttr);
			}
			nodeAttr.setTextContent(entry.getValue());
		}
	}

	private void updateConfigFile() throws TransformerFactoryConfigurationError, TransformerConfigurationException,
			FileNotFoundException, IOException, TransformerException {

		final Transformer transformer = TransformerFactory.newInstance().newTransformer();

		final DOMSource source = new DOMSource(document);

		final FileOutputStream stream = new FileOutputStream(configLocation);
		final StreamResult result = new StreamResult(stream);

		transformer.transform(source, result);
	}

	public void setAdapter(final String nodePath) {
		this.adapter = nodePath;
	}

	/**
	 * sets cache name to be used
	 */
	public void setCache(final Cache cache) {
		// this.cache = cache;
		// this.cache.removeAll();

	}

	public void setConfigLocation(final File configLocation) {
		this.configLocation = configLocation;
	}

	public Object getService(final String name, final Usuario user) {
		return user.getFactory().getSpringBean(name, user);
	}

	public void setXMLStream(InputStream f) {

		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			LogUtil.exception(e1, null);
		}
		try {

			document = builder.parse(f);

		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static byte[] convertStreamToByte(InputStream is) throws IOException {

		if (is != null) {
			/*
			 * Writer writer = new StringWriter(); char[] buffer = new
			 * char[1024];
			 * 
			 * try {
			 * 
			 * Reader reader = new BufferedReader(new InputStreamReader(is,
			 * "ISO-8859-1")); int n;
			 * 
			 * while ((n = reader.read(buffer)) != -1) { writer.write(buffer, 0,
			 * n); }
			 * 
			 * } catch (IOException e) { e.printStackTrace(); } return
			 * writer.toString().getBytes();
			 */
			return IOUtils.toByteArray(is);
		}
		return null;
	}

	public static String convertStreamToString(InputStream is)

			throws IOException {

		/*
		 * 
		 * To convert the InputStream to String we use the 34.
		 * Reader.read(char[] buffer) method. We iterate until the
		 * 
		 * Reader return -1 which means there's no more data to
		 * 
		 * read. We use the StringWriter class to produce the string.
		 */

		if (is != null) {

			Writer writer = new StringWriter();
			char[] buffer = new char[1024];

			try {

				Reader reader = new BufferedReader(

						new InputStreamReader(is, "ISO-8859-1"));

				int n;

				while ((n = reader.read(buffer)) != -1) {

					writer.write(buffer, 0, n);

				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			return writer.toString();

		} else {

			return "";

		}
	}

	public String getStrinNodeFromXQL(String q, Usuario user) {

		NodeList nodelist = null;
		try {
			nodelist = (NodeList) xpath.evaluate(q, document, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			LogUtil.exception(e, user);
		}
		String nd = StringEscapeUtils.unescapeHtml(nodelist.item(0).getFirstChild().getTextContent());
		LogUtil.debug("GET STRING FROM NODE XQL >> " + nd, user);
		return nd;
	}

}

class SAXHandler extends DefaultHandler {

	@Override
	public void endDocument() {
		// LogUtil.debug("End document", user);
	}

	@Override
	public void endElement(final String uri, final String name, final String qName) {
		if ("".equals(uri)) {
			// LogUtil.debug("End element: " + qName, user);
		} else {
			// LogUtil.debug("End element: {" + uri + "}" + name, user);
		}
	}

	@Override
	public void startDocument() {
		// LogUtil.debug("Start document", user);
	}

	@Override
	public void startElement(final String uri, final String name, final String qName, final Attributes atts) {
		if ("".equals(uri)) {
			// LogUtil.debug("Start element: " + qName, user);
		} else {
			// LogUtil.debug("Start element: {" + uri + "}" + name, user);
		}
	}

}
