package com.aseg.util.service;

import flexjson.transformer.AbstractTransformer;

class StringTransformer extends AbstractTransformer {

	@Override
	public void transform(Object object) {
		if (object.toString().startsWith("%=")) {
			getContext().write(object.toString().substring(2));
		} else {
			getContext().writeQuoted((String) object);
		}
	}

}