package com.aseg.util.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.aseg.seguranca.Usuario;
import com.aseg.vo.GenericVO;

public interface ParseXml {

	public void afterPropertiesSet() throws Exception;

	public int countXQL(String string, Usuario user, boolean restriction);

	public Collection<String> getListFromXQL(String string, Usuario user);
    public Map getMapFromXQL(String string, Usuario user);


    public Collection<Map<String, String>> getListValuesFromXQL(String query, String sort, Usuario user,
			boolean processAdapter, boolean restriction);

	/**
	 * Mtodo getHashValuesFromXQL
	 * 
	 * @param processAdapter
	 * @param String
	 * @param String
	 * 
	 * @return HashMap
	 */

	public Collection<Map<String, String>> getListValuesFromXQL(String[] queries, String sort, Usuario user,
			boolean processAdapter, boolean restriction);

	public Map<String, Map<String, String>> getMapValuesFromXQL(String query, String key, Usuario user,
			boolean processAdapter, boolean restriction);

	public Map<String, Map<String, String>> getMapValuesFromXQL(String[] queries, String key, Usuario user,
			boolean processAdapter, boolean restriction);

	public Double getNumberFromXQL(String query, Usuario user, boolean restriction);

	public InputStream getStreamConfig() throws IOException;

	public String getValueFromXQL(String query, String value, Usuario user, boolean processAdapter,
			boolean restriction);

	public String getXmlValue(String name, String value, Usuario user, boolean restriction);

	public Map<String, String> processFilterAdapter(final Map<String, String> hashtable, final Usuario user);

	public List describe(GenericVO v, Usuario user);

}
