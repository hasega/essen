package com.aseg.util.service;

public interface FileChangeListener {
	public void fileChanged(String fileName);
}