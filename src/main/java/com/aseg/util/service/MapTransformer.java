package com.aseg.util.service;

import java.util.Map;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;

import flexjson.JSONContext;
import flexjson.Path;
import flexjson.TypeContext;
import flexjson.transformer.AbstractTransformer;
import flexjson.transformer.TransformerWrapper;

class MapTransformer extends AbstractTransformer {

	@Override
	public void transform(Object object) {
		JSONContext context = getContext();
		Path path = context.getPath();
		Map value = (Map) object;

		TypeContext typeContext = getContext().writeOpenObject();
		for (Object key : value.keySet()) {
			if (key.toString().indexOf(Constants.SERVICE_KEY) > -1
					|| (value.get(key) != null && value.get(key).getClass().isArray())) {
				continue;
			}
			path.enqueue(key != null ? key.toString() : null);

			if (context.isIncluded(key != null ? key.toString() : null, value.get(key))) {

				TransformerWrapper transformer = (TransformerWrapper) context.getTransformer(value.get(key));

				if (!transformer.isInline()) {
					if (!typeContext.isFirst())
						getContext().writeComma();
					typeContext.increment();
					// ((Object) typeContext).setFirst(false);
					if (key != null) {
						getContext().writeName(key.toString());
					} else {
						getContext().writeName(null);
					}
				}

				if (key != null) {
					typeContext.setPropertyName(key.toString());
				} else {
					typeContext.setPropertyName(null);
				}
				try {
					transformer.transform(value.get(key));
				} catch (Exception e) {
					LogUtil.exception(e, null);
				}

			}

			path.pop();

		}
		getContext().writeCloseObject();
	}
}
