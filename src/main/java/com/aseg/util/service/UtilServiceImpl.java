package com.aseg.util.service;

import static org.quartz.impl.matchers.GroupMatcher.groupEquals;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.imgscalr.Scalr;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Trigger;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.BeanResolver;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.SpelCompilerMode;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.aseg.config.Constants;
import com.aseg.exceptions.ConfigurationException;
import com.aseg.exceptions.GenericRuntimeException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.relatorios.customizados.service.ReportJob;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.tests.ConfigSisAux;
import com.aseg.util.ConnectionManager;
import com.aseg.util.DAOTemplate;
import com.aseg.util.DataFormat;
import com.aseg.util.DateIterator;
import com.aseg.util.sql.Dialeto;
import com.aseg.util.sql.InterfaceDialeto;
import com.aseg.vo.GenericVO;
import com.thoughtworks.xstream.core.util.Base64Encoder;

import flexjson.JSONSerializer;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

public class UtilServiceImpl extends ServiceImpl implements UtilService, Serializable {

	static String privateKey = "S1ST3M@S#GUR0D3CRYPT";
	private transient static MessageDigest crypt;
	private static final Locale Local = new Locale("pt", "BR");

	// public ServletContext appcontext = null;

	private transient final String context_root = null;
	private transient ScriptEngineManager manager = new ScriptEngineManager();
	GenericVO internalVO = new GenericVO();

	public final ScriptEngine getJSEngine() {
		ScriptEngine e = manager.getEngineByName("nashorn");
		// try {
		// e.eval("load('nashorn:mozilla_compat.js');");
		// } catch (ScriptException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }
		return e;
	}

	private transient JSONSerializer jsonSerializer = new JSONSerializer()
			.transform(new StringTransformer(), String.class).transform(new MapTransformer(), GenericVO.class)
			.transform(new AnulateTransformer(), byte[].class);

	private transient final SimpleDateFormat dateFormat = new SimpleDateFormat();
	public transient final long MAX_SEQUENCE_PASTA = 1000000000;
	SpelParserConfiguration config = new SpelParserConfiguration(SpelCompilerMode.IMMEDIATE,
			this.getClass().getClassLoader());

	public transient final ExpressionParser PARSER = new SpelExpressionParser(config);
	public transient final Soundex soundex = new Soundex();
	private transient XPath xpath = XPathFactory.newInstance().newXPath();

	public transient DataFormat df = new DataFormat();
	private static BeanResolver beanResolver;

	public UtilServiceImpl() {
		// final String cl = Thread.currentThread().getStackTrace()[2]
		// .getClassName();
		// System.out.println("NOVO UTILSERVICE >>" + cl);
	}

	public String testRepository(Usuario user, String Nome) {
		String uploadDir = null;
		String fp = null;
		try {
			uploadDir = user.getSystemProperty("LOCALIZACAO_SERVIDOR");
		} catch (Exception e1) {
			LogUtil.exception(e1, user);
		}
		String T = "";
		try {
			if ((new File(uploadDir)).exists())
				T = "OK";
			else
				T = "ERROR";

			boolean criar = (new File(uploadDir)).mkdirs();
			File g = user.getUtil().writeToDisk(
					new ByteArrayInputStream(("TESTE DE ARQUIVO " + getDataAtual().toGMTString()).getBytes()), Nome,
					uploadDir);
			fp = g.getAbsolutePath();

		} catch (FileNotFoundException e) {
			LogUtil.exception(e, user);
		} catch (IOException e) {
			LogUtil.exception(e, user);
		}

		fp = " File " + Nome + " OK, DIR >> " + uploadDir + " >> " + T + " FULL PATH + " + fp;
		fp = replace(fp, "\\", "/");
		return fp;
	}

	public boolean isNumeric(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isDigit(c))
				return false;
		}
		return true;
	}

	public String convertStreamToString(InputStream is, Usuario user)

			throws IOException {

		/*
		 * 
		 * To convert the InputStream to String we use the 34.
		 * Reader.read(char[] buffer) method. We iterate until the
		 * 
		 * Reader return -1 which means there's no more data to
		 * 
		 * read. We use the StringWriter class to produce the string.
		 */

		if (is != null) {

			Writer writer = new StringWriter();
			char[] buffer = new char[1024];

			try {

				Reader reader = new BufferedReader(

						new InputStreamReader(is, user.getEncode()));

				int n;

				while ((n = reader.read(buffer)) != -1) {

					writer.write(buffer, 0, n);

				}

			} finally {

				// is.close();

			}

			return writer.toString();

		} else {

			return "";

		}
	}

	public String convertStreamToString(byte[] is, Usuario user)

			throws IOException {

		/*
		 * 
		 * To convert the InputStream to String we use the 34.
		 * Reader.read(char[] buffer) method. We iterate until the
		 * 
		 * Reader return -1 which means there's no more data to
		 * 
		 * read. We use the StringWriter class to produce the string.
		 */

		if (is != null) {

			Writer writer = new StringWriter();
			char[] buffer = new char[1024];

			try {

				Reader reader = new BufferedReader(

						new InputStreamReader(new ByteArrayInputStream(is), user.getEncode()));

				int n;

				while ((n = reader.read(buffer)) != -1) {

					writer.write(buffer, 0, n);

				}

			} finally {

				// is.close();

			}

			return writer.toString();

		} else {

			return "";

		}
	}

	public String unContext(final String string, final GenericService s) {

		if (s != null && s.getProperties().get(Constants.ALL_FIELDS) != null) {
			final List f = (List) s.getProperties().get(Constants.ALL_FIELDS);
			final List m = match(f, Constants.name, string);
			if (m.size() == 0) {
				for (final Iterator iterator = f.iterator(); iterator.hasNext();) {
					final Map object = (Map) iterator.next();
					if (string.endsWith(object.get(Constants.name).toString())) {
						return object.get(Constants.name).toString();
					}

				}
			}
		}
		return string;
	}

	public String _subDifData(final long dataini, final long datafim) {
		final StringBuffer result = new StringBuffer();
		long diffMillis;
		final NumberFormat nf = NumberFormat.getInstance();
		final long _dias = 24 * 60 * 60 * 1000;
		final long _horas = _dias / 24;
		final long _minutos = _horas / 60;
		double tmp;

		result.delete(0, result.length());
		diffMillis = datafim - dataini;
		nf.setMaximumFractionDigits(0);
		if (diffMillis > _dias) {
			tmp = Math.floor(diffMillis / _dias);
			result.append(nf.format(tmp) + " d ");
			diffMillis -= tmp * _dias;
		}

		if (diffMillis > _horas) {
			tmp = Math.floor(diffMillis / _horas);
			// Qtas horas?
			result.append(nf.format(tmp) + " hrs ");
			diffMillis -= tmp * _horas;
		}

		if (diffMillis > _minutos) {
			tmp = Math.floor(diffMillis / _minutos);
			// qtos minutos?
			result.append(nf.format(tmp) + " min ");

			diffMillis -= tmp * _minutos;
		}

		tmp = diffMillis / 1000;
		nf.setMaximumFractionDigits(2);
		result.append(nf.format(tmp) + " seg");

		return result.toString();
	}

	public List<String> arrayToList(final String[] object) {
		final List<String> a = new ArrayList<String>();
		for (final String element : object) {
			a.add(element);
		}
		return a;
	}

	public void exec(String cmd) throws IOException, InterruptedException {
		exec(cmd, null);
	}

	public void exec(String cmd, final OutputStream out) throws IOException, InterruptedException {
		final Process p = Runtime.getRuntime().exec(cmd);

		new Thread(new Runnable() {
			public void run() {
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line = null;

				try {
					while ((line = input.readLine()) != null) {
						System.out.println(line);
						if (out != null)
							out.write(line.getBytes());
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();

		p.waitFor();
	}

	public String escapeHTML(String a) {
		a = org.apache.commons.lang.StringEscapeUtils.escapeHtml(a);

		// a = a.replace("/", "%2F");
		// a = a.replace("\\?", "%3F");
		// a = a.replace("=", "%3D");
		// a = a.replace("&", "%26");
		// a = a.replace("@", "%40");

		return a;
	}

	public BufferedImage blurImage(final BufferedImage image) {
		final float ninth = 1.0f / 9.0f;
		final float[] blurKernel = { ninth, ninth, ninth, ninth, ninth, ninth, ninth, ninth, ninth };

		final Map<RenderingHints.Key, Object> map = new HashMap<RenderingHints.Key, Object>();
		map.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		map.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		map.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		final RenderingHints hints = new RenderingHints(map);
		final BufferedImageOp op = new ConvolveOp(new Kernel(3, 3, blurKernel), ConvolveOp.EDGE_NO_OP, hints);
		return op.filter(image, null);
	}

	/**
	 * Metodo checkEmail
	 * 
	 * @param String
	 *            Valor para a varievel mail
	 * @return boolean
	 */
	public boolean checkEmail(final String mail) {

		final char arroba = "@".charAt(0);
		final char underscore = "_".charAt(0);
		final char ponto = ".".charAt(0);
		final char vazio = '\u0000';
		final char hifen = "-".charAt(0);

		final int idxArb = mail.indexOf(arroba);
		if (idxArb <= 0) {
			return false;
		} else if (idxArb == mail.length() - 1) {
			return false;
		}

		final int idxVz = mail.indexOf(vazio);
		if (idxVz != -1) {
			return false;
		}

		final boolean firstNum = Character.isDigit(mail.charAt(0));
		if (firstNum) {
			return false;
		}

		for (int i = 0; i < mail.length(); i++) {
			final char c = mail.charAt(i);

			if (c != arroba && c != underscore && c != ponto && c != vazio && c != hifen
					&& !Character.isUnicodeIdentifierStart(c) && c == "$".charAt(0)) {
				return false;
			}
		}
		return true;
	}

	public int getMonth(Date dateStart, Date dateEnd) {

		int count = 0;

		if (dateStart != null && dateEnd != null && dateStart.before(dateEnd)) {

			Calendar clStart = Calendar.getInstance();
			clStart.setTime(dateStart);

			Calendar clEnd = Calendar.getInstance();
			clEnd.setTime(dateEnd);

			while (clStart.get(Calendar.MONTH) != clEnd.get(Calendar.MONTH)
					|| clStart.get(Calendar.YEAR) != clEnd.get(Calendar.YEAR)) {
				clStart.add(Calendar.MONTH, 1);
				count++;
			}
		}

		return count;
	}

	/**
	 * Metodo checkFormat
	 * 
	 * @param String
	 *            Valor para a varievel value
	 * @param String
	 *            Valor para a varievel format
	 * @return boolean
	 */
	private boolean checkFormat(final String value, final String format) {
		dateFormat.applyPattern(format);
		try {
			return dateFormat.parse(value) != null;
		} catch (final ParseException pe) {
			return false;
		}
	}

	public String clearForJSON(String evaluate) {
		if (evaluate == null) {
			return null;
		}
		evaluate.replaceAll("\r\n", " ");
		evaluate = evaluate.replaceAll("\n", " ");
		evaluate = evaluate.replaceAll("\r", " ");
		evaluate = evaluate.replaceAll("\f", " ");
		evaluate = evaluate.replaceAll("'", "\'");

		evaluate = evaluate.replaceAll("\\\"", "	&quot;");
		evaluate = evaluate.replaceAll("\"", "	&quot;");
		evaluate = evaluate.replaceAll("'", "\'");

		return evaluate;
	}

	public String cm2px(final double cm) {
		String a = String.valueOf(Math.round(cm * 283 / 10));
		if (a.indexOf(".") > -1) {
			a = a.substring(0, a.indexOf("."));
		}
		return a;
	}

	public String cm2px(final int cm) {

		return String.valueOf(cm * 283 / 10);
	}

	public String cm2px(String string) {
		return String.valueOf(Math.round((Integer.valueOf(string) / 2.54) * 72));
	}

	public void CollectionAddAll(final Collection<Object> list, final Object[] items) {

		for (final Object item : items) {
			list.add(item);
		}
	}

	public Object[] concat(final String[] extractArrayFromList, final String word, final String dir) {
		if (dir.equals("A")) {

			for (String element : extractArrayFromList) {
				element = element + word;
			}
		} else {
			for (String element : extractArrayFromList) {
				element = word + element;
			}
		}
		return extractArrayFromList;
	}

	/**
	 * Metodo convert2DDMMAAAA
	 * 
	 * @param String
	 *            Valor para a varievel data
	 * @return String
	 */
	public String convert2DDMMAAAA(final String data) {
		try {
			String d = null;
			d = data.substring(8, data.length()) + "/" + data.substring(5, 7) + "/" + data.substring(0, 4);
			return d;
		} catch (final Exception e) {
			LogUtil.exception(e, null);
			return null;
		}
	}

	/**
	 * Metodo converterData - D/M/YYYY
	 * 
	 * @param Date
	 * @return String
	 * 
	 */

	public String converterData(final Date data) {

		try {

			Calendar dt = Calendar.getInstance();
			dt.setTime(data);
			int dias = dt.get(Calendar.DAY_OF_MONTH);
			int mes = dt.get(Calendar.MONTH);
			int ano = dt.get(Calendar.YEAR);

			String dataCover = dias + "/" + (mes + 1) + "/" + ano;
			return dataCover;

		} catch (final Exception e) {
			LogUtil.exception(e, null);
			return null;
		}
	}

	public int countLines(final String str) {
		final String[] lines = str.split("\r\n|\r|\n");
		return lines.length + 1;
	}

	public String createCriteria(final Map par, final String operador, final String group) {

		final ArrayList r = new ArrayList();
		final Iterator i = par.keySet().iterator();
		while (i.hasNext()) {
			String a = (String) i.next();
			if (a.startsWith("!") || a.equalsIgnoreCase("IGNORE_CLAUSE"))
				continue;

			r.add("{c:{f:'" + a + "', o:'" + operador + "', v1:'" + par.get(a) + "'}}");

		}

		return "{g" + group + ":" + r + "}";
	}

	/**
	 * Metodo dataIsOk
	 * 
	 * @param String
	 *            Valor para a varievel data
	 * @return boolean
	 */
	public boolean dataIsOk(final String data) {
		return checkFormat(data, "dd/MM/yyyy");
	}

	public String dateTimeToString(final Date data) {
		if (data != null) {
			dateFormat.applyPattern("dd/MM/yyyy HH:mm:ss");
			return dateFormat.format(data);
		} else {
			return "";
		}
	}

	/**
	 * Metodo dateTimeToString
	 * 
	 * @param String
	 *            Valor para a varievel value
	 * @return String
	 */
	public String dateTimeToString(final String data) {
		if (data.length() > 10) {
			return data.substring(8, 10) + "/" + data.substring(5, 7) + "/" + data.substring(0, 4);
		} else {
			return data;
		}
	}

	/**
	 * Metodo dateToString
	 * 
	 * @param Date
	 *            Valor para a varievel data
	 * @return String
	 */
	public String dateToString(final Date data) {
		if (data != null) {
			dateFormat.applyPattern("dd/MM/yyyy");
			return dateFormat.format(data);
		} else {
			return "";
		}
	}

	public String decode(final String a, Usuario user) {
		try {
			// LogUtil.debug(URLDecoder.decode(a, user.getEncode()));
			return URLDecoder.decode(a, user.getEncode());
		} catch (final Exception ex) {
			// LogUtil.exception(ex,user);
			return a;
		}

	}

	public String shortenIdentifier(String identifier) {
		// loop atÃ© que o identificador fique menor q 30
		int cont = 0;
		while (identifier.length() > 30 && cont < 30) {
			cont++;
			// Verifica se alguma palavra pode ser abreviada
			if (identifier.matches(".+_([A-Z0-9]{3})[A-Z0-9]+.+")) {
				// Abrevia a Ãºltima palavra para os seus 3 primeiros digitos
				identifier = identifier.replaceFirst("(_[A-Z0-9]{3})[A-Z0-9]+((_[A-Z0-9]{1,3})+)?$", "$1$2");
				continue;
			}
			// Se todas foram abreviadas simplesmente corta a string
			identifier = identifier.substring(0, 30);
		}
		return identifier;
	}

	public boolean isIPAddress(String str) {
		Pattern ipPattern = Pattern.compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");
		return ipPattern.matcher(str).matches();
	}

	public Soundex getSoundex(Usuario user) {
		return soundex;
	}

	public Map<String, String> defineColumn(final Map data, final Usuario user, boolean noJoin, int numControl) {
		Map d = new HashMap<String, String>();
		d.put(Constants.real_name, data.get(Constants.name));
		Map join = new LinkedHashMap<String, String>();
		if (data == null || data.get(Constants.name) == null
				|| (!noJoin && data.get("joinable") != null && data.get("joinable").equals("false"))) {
			return d;
		}
		String n = data.get(Constants.name).toString();
		if (data.get("service") != null) {
			final GenericService ser = (GenericService) user.getService(data.get("service").toString().split("\\.")[0]);
			String f = "";
			if (data.get("labelValue") != null) {
				f = data.get("labelValue").toString();

			} else {
				if (data.get("key") != null) {
					f = data.get("key").toString();
				}

			}
			String talias = "";
			String cn = "";
			if (!noJoin && f.trim().length() > 0) {
				if (ser == null)
					return d;

				if (numControl > 0) {
					cn = "_" + numControl;
				}
				try {
					talias = shortenIdentifier(ser.getProperties().get("table") + "_" + data.get(Constants.name) + cn);
				} catch (Exception e) {
					LogUtil.exception(e, user);
				}
				d.put(Constants.name, talias + "." + f);
				String criteria = "";
				String joinType = " left outer ";
				if (data.get("contextParams") != null) { // afeta o context
					// ADVERSO ex
					// pq CLIENTE ='F
					final Map par = ser.getDAO().joinable(
							JSONtoMap((String) removeInvalidItemsFromContextParams(data.get("contextParams"))), user);
					if (ser == null)
						return d;
					if (par.size() > 0) {

						criteria = ser.getDAO().getSqlWhere(talias, createCriteria(par, "=", "A"), user,
								ser.createVO(user), null, false);
						// joinType = " inner ";
					}
					if (criteria.trim().length() > 0) {
						criteria = " AND " + criteria;
					}
				}
				if (data.get("volatile") == null) {
					if (ser == null)
						return d;
					join.put(
							"<table_name> " + joinType + " join " + ser.getProperties().get("table")
									+ Dialeto.getInstance().getDialeto(user).getAliasTabela(false) + talias,
							" <table_name>." + data.get(Constants.name) + " = " + talias + "." + data.get("key") + " "
									+ criteria);
				} else if (data.get("component") != null && data.get("component").equals("context")) {
					Object k = ser.getParent(user).getPrimaryKey();
					if (k != null)
						k = ((Map) k).get(Constants.name);
					else
						return d;

					join.put(
							"<table_name> " + joinType + " join " + ser.getProperties().get("table")
									+ Dialeto.getInstance().getDialeto(user).getAliasTabela(false) + talias,
							" <table_name>." + k + " = " + talias + "." + k + " " + criteria);
				}
				d.put(Constants.join, join);
			}

			if (f.trim().length() == 0) {
				f = (String) data.get(Constants.name);
			}
			if (ser == null)
				return null;
			List aux = match((List) ser.getProperties().get(Constants.ALL_FIELDS), Constants.name, f);
			if (aux.size() == 0) {
				for (Object element : ser.getMethods(user)) {
					Map object = (Map) element;
					aux = match((List) object.get(Constants.ALL_FIELDS), Constants.name, f);
					if (aux.size() > 0) {
						break;
					}

				}
			}
			if (aux.size() > 0) {
				Map c = defineColumn((Map) aux.get(0), user, noJoin, numControl + 1);
				if (c.containsKey(Constants.name)) {
					d.put(Constants.name, c.get(Constants.name));
				}

				if (c.containsKey(Constants.join)) {
					Map djoin = (Map) c.get(Constants.join);
					Iterator i = djoin.keySet().iterator();
					while (i.hasNext()) {
						String object = (String) i.next();
						join.put(replace(object, "<table_name>", ""),
								replace((String) djoin.get(object), "<table_name>", talias));
					}

				}
				if (((Map) aux.get(0)).get("volatile") == null) {
					n += "_" + c.get("COLUMN_NAME");
				} else {
					n = null;

				}

			}
		}
		if (n != null) {
			d.put("COLUMN_NAME", shortenIdentifier(n));
		}
		return d;
	}

	public boolean deleteDir(final File dir) {
		if (dir.isDirectory()) {
			final String[] children = dir.list();
			for (final String element : children) {
				final boolean success = deleteDir(new File(dir, element));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();

	}

	public String getTorF(boolean v) {
		if (v)
			return "T";
		else
			return "F";
	}

	public String encode(final String a, Usuario user) {
		try {

			String evaluate = URLEncoder.encode(a, user.getEncode());

			return evaluate;
		} catch (final Exception ex) {
			return "null";
		}

	}

	public String encode(String url, final String id, final String text, final boolean urlRewrite, Usuario user) {

		while (url.indexOf("encode(") > -1) {
			String aux = url.substring(url.indexOf("encode(") + 7);
			aux = aux.substring(0, aux.indexOf(")"));
			url = replace(url, "encode(" + aux + ")", encode(aux, user));
		}
		if (!urlRewrite) {
			return url;
		}
		if (url.startsWith("javascript") || url.indexOf("jsessionid") > -1) {
			return url;
		}
		if (url.indexOf("?") > -1) {
			if (text == null || text.length() == 0) {
				return replace(replace(url, "?", ";jsessionid=" + id + "$$"), "$$", "?");
			} else {
				return replace(replace(url, "?", ";jsessionid=" + id + "$$"), "$$", "?texto=" + text + "&");
			}

		}
		if (text == null || text.length() == 0) {
			return url + ";jsessionid=" + id;
		} else {
			return url + ";jsessionid=" + id + "?texto=" + text;
		}

	}

	public Map encodeMap(final Map<String, Object> par, Usuario user) {

		final Map n = new HashMap();
		final Iterator i = par.keySet().iterator();
		while (i.hasNext()) {
			final String a = (String) i.next();
			n.put(a, encode(par.get(a).toString(), user));

		}

		return n;
	}

	public String encodeText(final String valueOf) {

		return replace(valueOf, new String[] { "\n", "\t", "\r", "'", "\"" },
				new String[] { "\\n", "\\t", "\\r", "\\'", "\\\"" });
	}

	private void printRequest(HttpServletRequest httpRequest) {
		System.out.println("receive " + httpRequest.getMethod() + " notification for " + httpRequest.getRequestURI());

		System.out.println(" \n\n Headers");

		Enumeration headerNames = httpRequest.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = (String) headerNames.nextElement();
			System.out.println(headerName + " = " + httpRequest.getHeader(headerName));
		}

		System.out.println(" \n\n Attributes");

		Enumeration attrNames = httpRequest.getAttributeNames();
		while (attrNames.hasMoreElements()) {
			String attr = (String) attrNames.nextElement();
			System.out.println(attr + " = " + httpRequest.getAttribute(attr));
		}

		System.out.println("\n\nParameters");

		Enumeration params = httpRequest.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			System.out.println(paramName + " = " + httpRequest.getParameter(paramName));
		}

		System.out.println("\n\n Row data");
		System.out.println(extractPostRequestBody(httpRequest));
	}

	static String extractPostRequestBody(HttpServletRequest request) {
		if ("POST".equalsIgnoreCase(request.getMethod())) {
			Scanner s = null;
			try {
				s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return s.hasNext() ? s.next() : "";
		}
		return "";
	}

	public Map getCmp(final String string, GenericService targetService, final Usuario user) {

		if (targetService == null) {
			return null;
		}
		Map m = null;
		List f = match((List) targetService.getProperties().get(Constants.ALL_FIELDS), Constants.name,
				string.toUpperCase());
		if (f.size() > 0) {
			m = (Map) f.get(0);
		} else {
			f = match((List) targetService.getProperties().get(Constants.ALL_FIELDS), Constants.component, "context");
			for (final Iterator iterator = f.iterator(); iterator.hasNext();) {
				final Map object = (Map) iterator.next();

				if (string.startsWith(object.get(Constants.name).toString())) {
					f = match(
							(List) ((GenericService) user.getService(object.get("service").toString())).getProperties()
									.get(Constants.ALL_FIELDS),
							Constants.name, string.replaceAll(object.get(Constants.name) + "_", "").toUpperCase());
					if (f.size() > 0) {
						m = (Map) f.get(0);
					}
				}
			}

		}
		return m;
	}

	public Object evalService(final Map d, final String name, final Usuario user) {

		GenericVO vo = null;
		Object r = null;

		final GenericService service = evalVOService(d, user);
		if (d == null || (d.get(name) == null && d.get("key").equals(name)) || service == null) {
			return null;
		}
		vo = service.createVO(user);
		vo.putKey(vo.getKey(), d.get(name));

		try {
			vo.setSort(null);
		} catch (final Exception e) {

		}
		// if (d.get(Constants.FILTER_KEY) != null
		// && d.get(Constants.FILTER_KEY).toString().length() > 0) {
		// vo.setFiltro((String) d.get(Constants.FILTER_KEY));
		// }

		// List items = getItems(vo,user);
		String k = (String) d.get("key");
		if (d.get("labelValue") != null) {
			k = d.get("labelValue").toString();
		}
		Object i = null;
		String m = (String) d.get("serviceMethod");
		if (m == null || m.equals("lista") && d.get(d.get("key")) != null) {

			m = "obtem";
		} else if (m == null || m.equals("lista")
				&& (d.get("CONTEXTPARAMS") == null || d.get("CONTEXTPARAMS").toString().trim().length() == 0)) {
			return null;
		}
		try {// depreciado com suporte a join
				// if (d.get("CONTEXTPARAMS") != null) { // afeta o context
				// ADVERSO ex
				// // pq CLIENTE ='F
				// final Map par = JSONtoMap((String) d.get("CONTEXTPARAMS"));
				// if (par.size() > 0) {
				// vo.setFiltro(mergeFiltro(vo.getFiltro(),
				// createCriteria(par, "=", "A")));
				// }
				// }
			vo.putAllAndGet(d, false);
			vo.setPaginate(false);
			// vo.setLazyList(true);
			i = service.invoke(m, vo, user);
		} catch (final Exception e1) {
			LogUtil.exception(e1, user);
		}
		if (i != null && !i.getClass().isAssignableFrom(ArrayList.class)) {
			final String[] labels = k.split(",");
			for (String label2 : labels) {
				Object val = null;
				if (label2.indexOf("#") > -1) {
					label2 = label2.split("#")[0];
					final GenericVO dx = (GenericVO) vo.clone();
					dx.putAll((Map) match((List) service.getProperties().get(Constants.ALL_FIELDS), Constants.name,
							label2).get(0));
					val = ((GenericVO) i).getFormated(label2, user);
					dx.putKey(label2, val);
					// **MATCHSER
					// matchService(dx, name, val, user);
					// **EVSER
					// val = evalService(dx, name, user);

				} else {

					if (d.get(Constants.CONTEXT_KEY) != null && d.get(Constants.CONTEXT_KEY).equals(true)) { // contexto
						// veja
						// classForCompoent
						// e
						// ID_SITUACAO_SITUCAO
						// Ã© o
						// comeco
						// dos
						// joins
						val = ((GenericVO) i).getFormated(name, user);
					}

					if (val == null) {
						val = ((GenericVO) i).getFormated(label2, user);
					}
				}

				if (val != null) {
					r = (r == null ? "" : r) + val.toString() + ", ";
				}
			}
			if (r != null) {
				r = r.toString().substring(0, r.toString().lastIndexOf(","));
			}
		} else {
			if (d.get(d.get("key")) == null) {
				if (d.get("CONTEXTPARAMS") != null && !d.get("CONTEXTPARAMS").toString().isEmpty()
						&& !d.get("CONTEXTPARAMS").toString().equals("{}")) {

					final Map par = JSONtoMap((String) removeInvalidItemsFromContextParams(d.get("CONTEXTPARAMS")));
					try {
						r = ((Map) match((List) i, par.keySet().toArray(), par.values().toArray()).get(0))
								.get(d.get("labelValue"));
					} catch (final Exception e) {
						r = "";
					}
				} else {
					r = extractValueFromList((List) i, d.get("key").toString(), d.get(d.get(Constants.name)),
							d.get("labelValue").toString());
				}
			} else {
				r = extractValueFromList((List) i, d.get("key").toString(), d.get(d.get("key")),
						d.get("labelValue").toString());
			}

		}
		if (r == null) {
			r = "";
		}
		return r;

	}

	private Object evalSPEL(final String Action, final StandardEvaluationContext ctx) {

		Object r = null;
		String Ael = "";
		Object c = null;
		if (Action.indexOf(Constants.EXPRESSION_PREFIX) == -1) {
			c = toNumber(Action);
			if (c != null)
				return c;

		}
		c = toNumber(replace(Action, new String[] { "${", "}" }, new String[] { "", "" }));
		if (c != null)
			return c;

		if ((StringUtils.countOccurrencesOf(Action, " ") < 3))
			Ael = replace(Action, new String[] { "${", "}" }, new String[] { "#", "" });
		else
			Ael = replace(Action, new String[] { "${", "}" }, new String[] { "#{", "}" });

		try {
			r = PARSER.parseExpression(Ael).getValue(ctx);
		} catch (Exception e) {
			e.printStackTrace();
			try {
                String expr  = replace(Action, new String[] { "${", "}" },
                        new String[] { "#", "" });
				r = PARSER.parseExpression(expr)
						.getValue(ctx);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			if (r != null)
				return r;

			try {
				r = PARSER.parseExpression(Action).getValue(ctx);
			} catch (Exception ex) {
				// LogUtil.exception(ex,null);
			}

			if (r != null)
				return r;

		}

		if (r != null && r.equals("null")) {
			return null;
		}
		return r;
	}

    public static void populate(Object target,final Map data) {
        final Method[] f = target.getClass().getMethods();

        try {
            BeanUtils.populate(target, data);
        } catch (final Exception e1) {
                    e1.printStackTrace();
            }

        for (final Method field : f) {
            final String name = field.getName().toLowerCase();
            if (name.startsWith("set")) {
                if (data.containsKey(name.subSequence(3, name.length()))) {
                    try {
                        field.invoke(target, data.get(name.subSequence(3, name.length())));
                    } catch (final IllegalArgumentException e) {
                        // LogUtil.exception(e, user);
                    } catch (final IllegalAccessException e) {
                        // LogUtil.exception(e, user);
                    } catch (final InvocationTargetException e) {
                        // LogUtil.exception(e, user);
                    }
                }
            }
        }
    }

	public Object Evaluate(final String varEl, StandardEvaluationContext ctx) {

		if (varEl == null) {
			return null;
		}

		if (varEl.equalsIgnoreCase("false")) {
			return false;
		}
		if (varEl.equalsIgnoreCase("true")) {
			return true;
		}

		if (varEl.equalsIgnoreCase("now")) {
			return new Timestamp(Calendar.getInstance().getTime().getTime());
		}

		Object var = null;

		if (varEl.startsWith(Constants.EXPRESSION_PREFIX) || varEl.startsWith("#") ) {
			var = evalSPEL(varEl, ctx);
		} else {

			var = evalSPEL(Constants.EXPRESSION_PREFIX + varEl + Constants.EXPRESSION_SUFIX, ctx);

			if (var != null && var.toString().startsWith(Constants.EXPRESSION_PREFIX)) {
				var = evalSPEL((String) var, ctx);
			}

		}
		return var;

	}

	private Object toNumber(String varEl) {
		// TODO Auto-generated method stub
		try {
			return new Long(varEl);
		} catch (Exception e) {

			try {
				return new Long(varEl);
			} catch (Exception ex) {
				return null;
			}

		}
	}

	public boolean EvaluateBoolean(String string, StandardEvaluationContext ctx) {
		string = replace(string, "^", "'");
		if (string.trim().length() == 0)
			return false;
		final Object s = Evaluate(string, ctx);
		if (s == null) {
			return false;
		} else {
			return Boolean.valueOf((Boolean) s);
		}
	}

	// req.getAttribute(Constants.VOKey)

	// public String EvaluateInString(String Action, final ServletRequest req) {
	// if (Action == null) {
	// return null;
	// }
	// if (Action.indexOf('\'') > -1)
	// Action = replace(Action, "^", "\\\"");
	// else
	// Action = replace(Action, "^", "'");
	//
	// while (Action.indexOf("${") > -1) {
	//
	// final String expr = Action.subSequence(Action.indexOf("${"),
	// Action.indexOf("}", Action.indexOf("${")) + 1)
	// .toString();
	// Object v = eval(expr, createContext(req));
	// if (v == null) {
	// v = "";
	// } else {
	// // LogUtil.debug(v.getClass().getName(), user);
	// if (v instanceof List || v instanceof Map) {
	// v = JSONserialize(v);
	// }
	// }
	// Action = replace(Action, expr, v.toString());
	//
	// }
	//
	// return replace(Action, "#{", "${");
	// }

	public Timestamp getDataExpiracaoSenha(Usuario user) {
		Object daysToAdd = user.getSystemProperty("DIAS_EXPIRACAO_SENHA");
		if (daysToAdd == null) {
			daysToAdd = 90;
		}
		GregorianCalendar gc = new GregorianCalendar();
		gc.add(Calendar.DAY_OF_MONTH, Integer.parseInt(daysToAdd.toString()));

		return new Timestamp(gc.getTime().getTime());
	}

	public String hash(final String seed, final int tam) {
		try {
			if (crypt == null) {
				crypt = MessageDigest.getInstance("MD5");
			}
			crypt.update(seed.getBytes("UTF-8"));
		} catch (final Exception E) {
			LogUtil.exception(E, null);
			return null;
		}
		final byte raw[] = crypt.digest();
		String hash = new Base64Encoder().encode(raw);
		if (tam > 0) {
			hash = hash.substring(0, tam);
		}
		return hash;
	}

	public List iteratorToList(Iterator i) {
		List a = new ArrayList();
		while (i.hasNext()) {
			a.add(i.next());

		}
		return a;
	}

	public Iterator dateIterator(Timestamp b, Timestamp e, int f) {
		Calendar a = Calendar.getInstance();
		a.setTimeInMillis(b.getTime());
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(e.getTime());
		return new DateIterator(a, c, f);
	}

	public List getBeansByClassName(String cls, Usuario user) {
		List r = new ArrayList();
		String[] a = user.getFactory().getBeansNamesByClass(cls);
		for (String element : a) {
			Map d = new HashMap();
			d.put("ID", element);
			d.put("NOME", element);
			// d.put("BEAN", util.getFactory().getSpringBean(element, user));
			r.add(d);

		}
		return r;
	}

	public List describeJobs(Usuario user) {

		List r = new ArrayList();
		try {
			Map a = user.getFactory().getBeansByClass("org.springframework.scheduling.quartz.SchedulerFactoryBean");
			Set xa = a.keySet();
			for (Object element : xa) {

				final SchedulerFactoryBean she = (SchedulerFactoryBean) a.get(element);
				ReportJob pf = null;
				List<String> groups = she.getScheduler().getJobGroupNames();

				for (String jg : groups) {
					if (!jg.equals("DEFAULT")) {
						for (JobKey jobKey : she.getScheduler().getJobKeys(groupEquals(jg))) {

							JobDetail currentJob = she.getScheduler().getJobDetail(jobKey);

							Map d = new HashMap();
							d.put("ID", currentJob.getKey());
							d.put("NOME", currentJob.getKey().getName());
							d.put("SHEDULER", element);

							// d.put("BEAN",
							// util.getFactory().getSpringBean(element, user));

							if (!she.getScheduler().getTriggersOfJob(jobKey).isEmpty()) {
								Trigger t = she.getScheduler().getTriggersOfJob(jobKey).get(0);
								try {
									d.put("LAST_FIRE", user.getUtil().df.fromTimestampcomHoraptBR(
											new Timestamp(t.getPreviousFireTime().getTime())));
								} catch (Exception e) {
									d.put("LAST_FIRE", " N/A ");
								}

								d.put("NEXT_FIRE", user.getUtil().df
										.fromTimestampcomHoraptBR(new Timestamp(t.getNextFireTime().getTime())));
								try {
									d.put("FINAL_FIRE", user.getUtil().df
											.fromTimestampcomHoraptBR(new Timestamp(t.getFinalFireTime().getTime())));
								} catch (Exception e) {
									d.put("FINAL_FIRE", " N/A ");
								}

							}

							r.add(d);

						}
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return r;
	}

	public String getStackTrace(Exception t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		return sw.toString();
	}

	public String EvaluateInString(String Action, final StandardEvaluationContext ctx) {
		if (Action == null) {
			return null;
		}
		Action = replace(Action, "^", "\\\"");
		while (Action.indexOf("${") > -1) {
			final String expr = Action.subSequence(Action.indexOf("${"), Action.indexOf("}", Action.indexOf("${")) + 1)
					.toString();
			Object v = evalSPEL(expr, ctx);
			if (v == null) {
				v = "";
			} else {
				if (v instanceof List || v instanceof Map) {
					v = JSONserialize(v);
				}
			}
			Action = replace(Action, expr, v.toString());

		}

		return replace(Action, "#{", "${");
	}

	private Object EvaluateSPEL(final String Action, final ServletRequest req) {

		if (Action == null) {
			return null;
		}
		try {
			final StandardEvaluationContext ctx = new StandardEvaluationContext(req);
			ctx.setVariable("sessionScope", ((HttpServletRequest) req).getSession());
			ctx.setVariable(Constants.RAIN_USER, ((HttpServletRequest) req).getAttribute(Constants.RAIN_USER));

			Enumeration e = req.getAttributeNames();
			while (e.hasMoreElements()) {
				final Object object = e.nextElement();
				ctx.setVariable((String) object, req.getAttribute((String) object));
			}
			e = req.getParameterNames();
			while (e.hasMoreElements()) {
				final Object object = e.nextElement();
				ctx.setVariable((String) object, req.getParameter((String) object));
			}
			final Object r = evalSPEL(Action, ctx);
			return r == null ? null : r;
		} catch (final Exception e) {

			return null;
		}

	}

	public String arrayToDelimitedString(Object[] arr, String delim) {
		return StringUtils.arrayToDelimitedString(arr, delim);
	}

	public GenericService evalVOService(final Map vo, final Usuario user) {
		GenericService service = null;

		final String s = (String) vo.get("service");

		try {
			service = (GenericService) user.getService(s);
		} catch (final Exception e) {
			return service;
		}
		return service;
	}

	public String[] extractArrayFromList(final Collection<Map<String, Object>> collection, final String field,
			final boolean unique) {

		final List<String> c = new ArrayList<String>();
		if (collection == null) {
			return new String[] {};
		}
		for (final Map<String, Object> element : collection) {
			if (element.get(field) != null) {
				if (unique) {
					if (!c.contains(element.get(field).toString())) {
						c.add(element.get(field).toString());
					}
				} else {
					c.add(element.get(field).toString());
				}

			}
		}
		return c.toArray(new String[c.size()]);
	}

	public Object[] extractObjectArrayFromList(final Collection<Map<String, Object>> collection, final String field,
			final boolean unique) {

		final List<Object> c = new ArrayList<Object>();
		if (collection == null) {
			return new Object[] {};
		}
		for (final Map<String, Object> element : collection) {
			if (element.get(field) != null) {
				if (unique) {
					if (!c.contains(element.get(field))) {
						c.add(element.get(field));
					}
				} else {
					c.add(element.get(field));
				}

			}
		}
		return c.toArray(new Object[c.size()]);
	}

	public String extractJsonFromList(Collection<Map<String, Object>> items, String key, String cols) {
		if (items != null && cols != null && items.size() > 0 && !cols.isEmpty()) {
			String json = "[";
			cols += "," + key;
			String[] columns = cols.split(",");

			for (Map<String, Object> mp : items) {
				json += "{";
				for (String c : columns) {
					json += c + ":'" + mp.get(c) + "',";
				}
				json = json.substring(0, json.lastIndexOf(",")) + "},";
			}

			json = json.substring(0, json.lastIndexOf(",")) + "]";
			return json;
		} else {
			return "";
		}
	}

	public Integer[] extractIntArrayFromList(final List<Map<String, Object>> a, final String field,
			final boolean unique) {

		final List<Integer> c = new ArrayList<Integer>();
		for (final Map<String, Object> element : a) {
			if (element.get(field) != null) {
				if (unique) {
					if (!c.contains(Integer.valueOf(element.get(field).toString()))) {
						c.add(Integer.valueOf(element.get(field).toString()));
					}
				} else {
					c.add(Integer.valueOf(element.get(field).toString()));
				}

			}
		}
		return c.toArray(new Integer[c.size()]);
	}

	public Object extractValueFromList(final List<Map> a, final String field, final Object value, final String data) {
		if (a == null) {
			return null;
		}
		for (final Map element : a) {

			if (element == null) {
				return null;
			}
			if (value == null) {
				if (element.get(field) == null) {
					return element.get(data);
				}
			} else if (element.get(field) != null
					&& element.get(field).toString().trim().equals(value.toString().trim())) {
				return element.get(data);
			}
		}
		if (field.toString().equalsIgnoreCase(data) && a.size() == 1) {
			return a.get(0).get(field);
		}
		return null;

	}

	public List filter(final List a, final Object field, final Object value) {
		final ArrayList b = new ArrayList();
		final int i = 0;
		if (a == null) {
			return b;
		}
		for (final Iterator iterator = a.iterator(); iterator.hasNext();) {
			final Map object = (Map) iterator.next();
			if (value != null && value.toString().startsWith("%")) {
				if (object.get(field) != null
						&& object.get(field).toString().indexOf(replace((String) value, "%", "")) > -1) {
					continue;
				}
				b.add(object);

			} else {
				if (object.get(field) != null && value != null && object.get(field).toString().equals(value)) {
					if (value == null) {
						b.add(object);
					} else {
						continue;
					}
				} else {
					if (object.get(field) == value) {
						continue;
					}
					b.add(object);
				}
			}

		}
		return b;
	}

	public List filterAndClone(final List a, final Object field, final Object value) {
		ArrayList b = new ArrayList();
		final int i = 0;
		if (a == null) {
			return b;
		}
		for (final Iterator iterator = a.iterator(); iterator.hasNext();) {
			final Map object = (Map) iterator.next();
			if (value != null && value.toString().startsWith("%")) {
				if (object.get(field) != null
						&& object.get(field).toString().indexOf(replace((String) value, "%", "")) > -1) {
					continue;
				}
				b.add(new HashMap(object));
			} else {
				if (object.get(field) != null && value != null && object.get(field).toString().equals(value)) {
					if (value == null) {
						b.add(new HashMap(object));
					} else {
						continue;
					}
				} else {
					if (object.get(field) == value) {
						continue;
					}
					b.add(new HashMap(object));
				}
			}
		}
		return b;
	}

	public List filter(final List a, final Object[] field, final Object[] value) {
		List e = a;
		for (int i = 0; i < value.length; i++) {
			e = filter(e, field[i], value[i]);
		}
		return e;
	}

	public Map filterMap(final Map object, final Object[] objects) {
		final List s = Arrays.asList(objects);
		final Iterator i = object.keySet().iterator();
		final Map ne = new HashMap();
		while (i.hasNext()) {
			final Object object2 = i.next();
			if (s.contains(object2)) {
				ne.put(object2, object.get(object2));
			}
		}
		return ne;
	}

	public List filterMapsInList(final List attribute, final String[] strings) {
		final List n = new LinkedList();

		for (final Iterator iterator = attribute.iterator(); iterator.hasNext();) {
			final Map object = (Map) iterator.next();

			n.add(filterMap(object, strings));

		}
		return n;
	}

	public String FormatByteSize(final Long bytes) {

		final long B = 1; // byte
		final long KB = 1024 * B; // kilobyte
		final long MB = 1024 * KB; // megabyte
		final long GB = 1024 * MB; // gigabyte
		if (bytes > GB) {
			return (new BigDecimal(bytes).divide(new BigDecimal(GB))).setScale(2, BigDecimal.ROUND_UP) + " Gb";
		} else {
			if (bytes > MB) {
				return (new BigDecimal(bytes).divide(new BigDecimal(MB))).setScale(2, BigDecimal.ROUND_UP) + " Mb";
			} else {
				if (bytes > KB) {
					return (new BigDecimal(bytes).divide(new BigDecimal(KB))).setScale(2, BigDecimal.ROUND_UP) + " Kb";
				} else {
					return (new BigDecimal(bytes).divide(new BigDecimal(B))).setScale(2, BigDecimal.ROUND_UP) + " b";
				}
			}
		}
	}

	/**
	 * Metodo formatDate
	 * 
	 * @param Date
	 *            Valor para a varievel data
	 * @param String
	 *            Valor para a varievel patters
	 * @return String
	 */
	public String formatDate(final Date data, final String pattern) {
		dateFormat.applyPattern(pattern);
		try {
			return dateFormat.format(data);
		} catch (final Exception e) {
			return null;
		}
	}

	/**
	 * @param time
	 * @param pattern
	 * @return
	 */
	public String formatDate(final long time, final String pattern) {
		Date data;
		data = new Date(time);
		dateFormat.applyPattern(pattern);
		try {
			return dateFormat.format(data);
		} catch (final Exception e) {
			return null;
		}
	}

	public String gerarSenha() {
		final Random random = new Random();
		final char[] buffer = new char[12];
		for (int i = 0; i < buffer.length; i++) {
			int code = 0;
			do {
				code = random.nextInt(150);
			} while (!(code >= 65 && code <= 90) && // A...Z
					!(code >= 97 && code <= 122)); // a...z
			buffer[i] = (char) code;
		}
		return new String(buffer);
	}

	public Object[] getActionStringAndParams(final Map m, final boolean local_ref, final boolean json,
			final ServletRequest request) {

		String r = "";
		final String n = m.get(Constants.name).toString();
		String method = (String) m.get(Constants.ActionParam);
		if (local_ref) {
			return new String[] { n + "Action" + Constants.ACTION_SUFIX + "?" + Constants.ActionParam + "=show&node="
					+ m.get("id") + "&model=" + m.get("model"), null };
		}
		r += n + "Action" + Constants.ACTION_SUFIX;
		if (method == null) {
			method = "lista";
		}
		if (json) {
			return new Object[] { r, getParams(method, request, false, true) };
		}
		r += "?" + Constants.ActionParam + "=" + method;
		return new String[] { r, null };

	}

	public long getChecksumValue(final String fname) {
		final Checksum checksum = new CRC32();
		try {
			final BufferedInputStream is = new BufferedInputStream(new FileInputStream(fname));
			final byte[] bytes = new byte[1024];
			int len = 0;

			while ((len = is.read(bytes)) >= 0) {
				checksum.update(bytes, 0, len);
			}
			is.close();
		} catch (final IOException e) {
			LogUtil.exception(e, null);
		}
		return checksum.getValue();
	}

	public String getContext_root() {
		return context_root;
	}

	/**
	 * Metodo getDataAtual()
	 * 
	 * @return Date
	 */
	public Date getDataAtual() {
		final SimpleTimeZone zone = new SimpleTimeZone(TimeZone.getTimeZone("America/Sao_Paulo").getRawOffset(),
				"America/Sao_Paulo");
		return new java.util.Date(Calendar.getInstance(zone).getTimeInMillis());
	}

	public Object getHoraAtual() {
		final SimpleTimeZone zone = new SimpleTimeZone(TimeZone.getTimeZone("America/Sao_Paulo").getRawOffset(),
				"America/Sao_Paulo");
		dateFormat.applyPattern("HH:mm");
		return dateFormat.format(new java.util.Date(Calendar.getInstance(zone).getTimeInMillis()));

	}

	/**
	 * 
	 * @return Data atual em milesegundos <b>DD/MM/YYYY</b>. Nos milesegundos
	 *         neo esteo contidos os milesegundos da data atual, nem segundos,
	 *         nem minutos e nem horas.
	 */

	public Long getDataAtualDDMMYYYY() {

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR, 0);

		return cal.getTimeInMillis();
	}

	public List<Map> getItems(final GenericVO VO, final Usuario user) {

		final List<Map> items = new LinkedList<Map>();
		final GenericService service = evalVOService(VO, user);
		if (service != null) {
			try {

				final GenericVO sVO = service.createVO(user).cloneConfig(VO);

				sVO.setPaginate(false);

				String method = (String) VO.get("serviceMethod");
				if (method == null || method.toString().length() == 0) {
					items.addAll(service.lista(sVO, user));
				} else {

					if (method.startsWith("obtem")) {
						method = method.replaceAll("obtem", "lista");
					}
					try {
						if (service == null) {
							return new ArrayList<Map>();
						}
						List a = (List<Map>) service.invoke(method, sVO, user);
						if (a == null && sVO.get(Constants.LIST_DATA) != null)
							a = (List<Map>) sVO.get(Constants.LIST_DATA);
						items.addAll(a);
					} catch (final Exception e) {
						e.printStackTrace();
						LogUtil.exception(e, user);
						throw new ConfigurationException(
								" O method " + method + " neo existe no Service " + service.getName(),
								ConfigurationException.TYPE.METHOD_NOT_FOUND);
					}

				}
			} catch (final Exception e1) {
				LogUtil.exception(e1, user);
				// throw new RainException(e1);
			}
		} else {
			// throw new ConfigurationException("Refactiorin Error",
			// ConfigurationException.TYPE.METHOD_NOT_FOUND);
			try {
				final String m = (String) VO.get("serviceMethod");
				// items = (List<Map>) Evaluate(m, request,
				// getPageContext(request));
			} catch (final Exception e2) {
				LogUtil.exception(e2, user);
			}
		}

		return items == null ? new LinkedList<Map>() : items;
	}

	/**
	 * Metodo getLocale
	 * 
	 * @return Locale
	 */
	public Locale getLocale() {
		return new Locale("pt", "BR");
	}

	public synchronized Long getNextValue(String tabela, final String Campo, String where, final boolean sequence,
			final Usuario user) throws Exception {
		boolean role = false;
		if (where != null) {
			where = " WHERE " + where;
		} else {
			where = "";
		}
		if (sequence) {
			try {
				final DAOTemplate jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
						null, this);
				InterfaceDialeto dialeto = Dialeto.getInstance().getDialeto(user);

				final List listaValor = jt.queryForList(
						"SELECT " + dialeto.generatedKeyColumnName(Campo) + " as VALOR  " + dialeto.fromDual());

				if (listaValor == null || listaValor.isEmpty()) {
					return new Long(1);
				} else {
					try {
						return Long.parseLong(((Map) listaValor.get(0)).get("VALOR").toString());
					} catch (Exception e) {
						return new Long(1);
					}
				}

			} catch (final Exception sql) {
				LogUtil.exception(sql, user);
				// ************ Registrar erro no arquivo de log de excecoes
				// ***************
			}

		}
		if (role) {
			boolean jaExiste = true; // Verifica se a sequence j exite
			long temp;

			tabela = tabela.toUpperCase();
			try {
				final DAOTemplate jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
						null, this);

				List listaValor = jt.queryForList(
						" SELECT " + Campo + " as VALOR  FROM " + tabela.toLowerCase() + where + " order by " + Campo);

				for (final Iterator iterator = listaValor.iterator(); iterator.hasNext();) {
					final Map rs = (Map) iterator.next();
					temp = Long.parseLong(rs.get("VALOR").toString()) + 1;
					while (jaExiste) {
						if (jt.queryForList(
								" SELECT 1 FROM " + tabela.toLowerCase() + " WHERE " + Campo + " = " + temp + " ")
								.size() == 0) {
							jaExiste = false;
							break;
						} else if (temp < MAX_SEQUENCE_PASTA) {
							temp = temp + 1;
						} else {
							throw new Exception(
									"Sequencia invalida, numero maximo atingido na tabela " + tabela + " de 999999999");
						}
					}

					return temp;
				}
				if (listaValor.isEmpty()) {
					temp = 1;
					while (jaExiste) {
						listaValor = jt.queryForList(
								" SELECT 1 FROM " + tabela.toLowerCase() + " WHERE " + Campo + " = " + temp);
						if (listaValor.isEmpty()) {
							jaExiste = false;
							break;
						} else if (temp < MAX_SEQUENCE_PASTA) {
							temp = temp + 1;
						} else {
							throw new Exception("Sequencia invalida, numero maximo atingido na tabela "
									+ tabela.toLowerCase() + " de 999999999");
						}
					}

					return temp;
				}
			} catch (final Exception e) {
				LogUtil.debug("erro pegando tabela" + tabela, user);
				LogUtil.exception(e, user);
			}
			throw new Exception("Sequencia invalida");
		} else {
			try {
				final DAOTemplate jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
						null, this);

				final List listaValor = jt.queryForList("SELECT MAX(" + Dialeto.getInstance().getDialeto(user)
						.getToNumber(Campo + Dialeto.getInstance().getDialeto(user).getConcatOperator() + " '' ") +
				// + Campo +
						") as VALOR FROM " + tabela.toLowerCase() + " " + where);

				if (listaValor == null || listaValor.isEmpty()) {
					return new Long(1);
				} else {
					try {
						return Long.parseLong(((Map) listaValor.get(0)).get("VALOR").toString()) + 1;
					} catch (Exception e) {
						return new Long(1);
					}
				}

			} catch (final Exception sql) {
				LogUtil.exception(sql, user);
				// ************ Registrar erro no arquivo de log de excecoes
				// ***************
			}
			throw new Exception("Sequencia invalida");

		}
	}

	public Map getParams(final String method, final ServletRequest request, boolean removeKeys,
			boolean forceExistence) {
		final Map a = new HashMap();

		List<String> re = (List) request.getAttribute(Constants.REQUEST_KEYS);
		Usuario user = (Usuario) request.getAttribute(Constants.RAIN_USER);
		StandardEvaluationContext ctx = user.getRequestEvaluationContext(request);
		if (re != null) {
			re = new ArrayList(re);
			for (final String o : re) {
				if (removeKeys && (o.equals(Constants.FILTER_KEY) || o.equals("MULTIPLEID") || o.startsWith("_"))) {
					continue;
				}
				String label = "";
				Object value = "";

				if (o.indexOf(":") > -1) {
					label = o.split(":")[0];
					value = Evaluate(o.split(":")[1], ctx);
				} else {
					label = o;
					value = Evaluate(label, ctx);
				}
				if (value == null || value.equals("")) {
					value = Evaluate("#VO.get('"+label+"')", ctx);
				}
				if (value != null) {
					if (value.getClass().isArray()) {
						value = org.springframework.util.StringUtils.arrayToCommaDelimitedString((Object[]) value);
						if (value != null && !value.equals("null")) {
							a.put(label, value);
						}
					} else {
						a.put(label, encode(value.toString(), user));
					}
				}
			}
		}

		if (method != null) {
			a.put(Constants.ActionParam, method);
		}
		if (forceExistence && a.size() == 0) {
			throw new ConfigurationException("Requerido Params", ConfigurationException.TYPE.PARAM_NOT_FOUND);
		}

		return a;
	}

	public Object getSessionTimeOutl(final ServletRequest request) {

		dateFormat.applyPattern("HH:mm");
		return dateFormat.format(new Date(((HttpServletRequest) request).getSession().getMaxInactiveInterval()));

	}

	public Timestamp getTimestamp() {
		final SimpleTimeZone zone = new SimpleTimeZone(TimeZone.getTimeZone("America/Sao_Paulo").getRawOffset(),
				"America/Sao_Paulo");
		// LogUtil.debug(new Date());
		// LogUtil.debug(Calendar.getInstance(zone,getLocale()));
		return new java.sql.Timestamp(Calendar.getInstance(zone).getTimeInMillis());
	}

	public List<Map<String, String>> groupCollection(final List<Map<String, String>> c, final Object group) {
		final List<Map<String, String>> r = new ArrayList<Map<String, String>>();
		final List m = new ArrayList();
		for (final Map data : c) {

			if (data.get(group) != null && !m.contains(data.get(group))) {
				m.add(data.get(group));
				r.add(data);
			}

		}
		return r;
	}

	public boolean horaIsOk(final String hora) {
		return checkFormat(hora, "HH:mm");
	}

	/**
	 * Metodo isMonetario
	 * 
	 * @param String
	 *            Valor para a varievel value
	 */
	public boolean isMonetario(final String value) {
		if (value == null || "".equals(value)) {
			return false;
		}

		final StringTokenizer token = new StringTokenizer(value, ",");

		if (token.countTokens() > 2) {
			return false;
		} else if (token.countTokens() == 2) {
			final String esquerda = token.nextToken();
			final String direita = token.nextToken();
			if (direita.length() > 2) {
				return false;
			}
			if (!verificaNumero(direita) || !verificaNumero(esquerda)) {
				return false;
			}

		} else {
			final String esquerda = token.nextToken();
			if (!verificaNumero(esquerda)) {
				return false;
			}
		}
		return true;
	}

	public String JSONserialize(final Object a) {
		final String b = jsonSerializer.deepSerialize(a);
		// LogUtil.debug(b);
		if (b == null || b.equalsIgnoreCase("null")) {
			return "{}";
		}
		return b;

	}

	public Map<String, Object> JSONtoMap(String Json) {
		final Map<String, Object> map = new HashMap<String, Object>();
		try {

			if (Json == null || Json.toString().length() == 0) {
				return map;
			}
			// Json = util.replace(Json, "\\", "");
			final JsonConfig cfg = new JsonConfig();
			cfg.setRootClass(LinkedHashMap.class);
			cfg.setArrayMode(JsonConfig.MODE_OBJECT_ARRAY);
			cfg.setHandleJettisonSingleElementArray(false);

			Json.replaceAll("\r\n", " ");
			Json = Json.replaceAll("\n", " ");
			Json = Json.replaceAll("\r", " ");
			Json = Json.replaceAll("\f", " ");
			Json = Json.replaceAll("'", "\'");

			Json = Json.replaceAll("'", "\'");

			return JSONObject.fromObject(Json, cfg);
		} catch (final Exception e) {

			return map;
		}
	}

	/**
	 * Metodo jurosComposto
	 * 
	 * @param double
	 *            Valor para a varievel montante
	 * @param double
	 *            Valor para a varievel juros
	 * @param double
	 *            Valor para a varievel periodo
	 * @return double
	 */
	public double jurosComposto(final double montante, final double juros, final double periodo) {
		return montante * Math.pow(1 + juros, periodo) - montante;
	}

	/**
	 * Metodo jurosSimples
	 * 
	 * @param double
	 *            Valor para a varievel montante
	 * @param double
	 *            Valor para a varievel juros
	 * @param double
	 *            Valor para a varievel periodo
	 * @return double
	 */
	public double jurosSimples(final double montante, final double juros, final double periodo) {
		return montante * juros * periodo;
	}

	public String loadin(final List possiveis, final boolean validate) {

		String exp = null;
		BufferedReader a;

		a = new BufferedReader(new InputStreamReader(System.in));

		try {
			exp = a.readLine();
		}

		catch (final IOException ioe) {
			LogUtil.exception(ioe, null);
		}
		if (validate) {
			if (possiveis.size() == 0 && (exp == null || exp.toString().trim().length() == 0)) {
				LogUtil.debug("Esta opeeo e obrigatoria :" + possiveis, null);
				return loadin(possiveis, validate);
			} else if (possiveis.size() == 1 && possiveis.get(0).equals("dt")) {
				try {
					final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					sdf.setLenient(false);
					final Date aDate = sdf.parse(exp);
				} catch (final Exception e) {
					System.out.println("Data Invalida digite uma data velida no padreo (dd/mm/aaaa):");
					return loadin(possiveis, validate);
				}

			} else if (!possiveis.contains(exp) && possiveis.size() > 0) {
				LogUtil.debug("Opeeo Invalida digite uma das seguintes opeees :" + possiveis, null);
				return loadin(possiveis, validate);
			}

			return exp;

		}
		return exp;

	}

	public List ManageCollection(List f, final String operation, final Object attribute, final Object value) {
		f = (List) ((ArrayList) f).clone();
		if (operation.equalsIgnoreCase("remove")) {
			for (final Iterator iterator = f.iterator(); iterator.hasNext();) {
				Map m = null;
				try {
					m = (Map) iterator.next();
				} catch (final Exception e) {
					continue;
				}
				m.remove(attribute);

			}

		}
		if (operation.equalsIgnoreCase("add")) {
			for (final Iterator iterator = f.iterator(); iterator.hasNext();) {
				Map m = null;
				try {
					m = (Map) iterator.next();
				} catch (final Exception e) {
					continue;
				}
				m.put(attribute, value);

			}
		}
		if (operation.equalsIgnoreCase("match")) {
			final List attrs = Arrays.asList(((String[]) attribute));
			for (final Iterator iterator = f.iterator(); iterator.hasNext();) {
				Map m = null;
				try {
					m = (Map) iterator.next();
				} catch (final Exception e) {
					continue;
				}
				final Object[] a = m.keySet().toArray();
				for (int i = 0; i < a.length; i++) {
					if (!attrs.contains(a[i])) {
						m.remove(a[i]);
					}
				}

			}
		}

		if (operation.equalsIgnoreCase("rename")) {

			if (attribute.equals("*")) {

				if (value.equals("upper")) {
					Object[] a;
					Object v;
					Map m;

					for (final Iterator iterator = f.iterator(); iterator.hasNext();) {

						try {
							m = (Map) iterator.next();
						} catch (final Exception e) {
							continue;
						}
						a = m.keySet().toArray();

						for (final Object element : a) {
							v = m.get(element);
							m.remove(element);
							m.put(element.toString().toUpperCase(), v);
						}
					}
				}

			} else {
				final List attrs = Arrays.asList(((String[]) attribute));
				final List values = Arrays.asList(((String[]) value));
				for (final Iterator iterator = f.iterator(); iterator.hasNext();) {
					Map m = null;
					try {
						m = (Map) iterator.next();
					} catch (final Exception e) {
						continue;
					}
					final Object[] a = m.keySet().toArray();
					for (final Object element : a) {
						if (attrs.contains(element)) {
							final Object v = m.get(element);
							m.remove(element);
							m.put(values.get(attrs.indexOf(element)), v);
						}

					}

				}
			}
		}

		return f;

	}

	public List<Map<String, String>> mappingCollection(final List<Map<String, String>> list, final String mapping,
			final String KeyCase, final boolean excludeOthers, Usuario user) {

		final List ne = new ArrayList<Map<String, String>>();

		try {

			final Map map = JSONtoMap(decode(mapping, user));

			for (final Map object : list) {
				final HashMap<String, Object> data = new HashMap<String, Object>();
				final Iterator i = map.keySet().iterator();

				while (i.hasNext()) {
					String key = (String) i.next();
					String f = map.get(key).toString();
					if (KeyCase != null && f != null) {
						if (KeyCase.equalsIgnoreCase("U")) {
							f = f.toUpperCase();
						}
						if (KeyCase.equalsIgnoreCase("L")) {
							f = f.toLowerCase();
						}
					}
					Object value = key;
					if (key != null && key.indexOf("$") > -1) {
						key = EvaluateInString(key, createContext(object, user));
					}

					value = object.get(key) == null ? key : object.get(key);
					data.put(f, value);
				}
				if (excludeOthers) {
					ne.add(data.clone());
				} else {
					object.putAll(data);
					ne.add(object);
				}
			}

		} catch (Exception e) {
			LogUtil.exception(e, user);
		}

		return ne;
	}

	public String[] tokensBetwen(String content, String sep)

	{
		ArrayList a = new ArrayList();
		String[] s = content.split(sep);
		for (int i = 0; i < s.length; i++) {
			if (i % 2 == 1)
				a.add(s[i]);
		}

		return (String[]) a.toArray(new String[] {});

	}

	public StandardEvaluationContext createContext(Map items, Usuario user) {
		final StandardEvaluationContext ctx = new StandardEvaluationContext();
		ctx.setRootObject(items);
		ctx.setVariables(items);
		ctx.setVariable("user", user);
		ctx.setVariable("util", this);
		// ctx.setVariable("ALL_VARS", vt);
		return ctx;
	}

	public String getMailUserByPerfil(Long idPerfil, GenericService sm, Usuario user) {

		ServiceImpl ser = (ServiceImpl) user.getService(Constants.subject_service);

		GenericVO vo = ser.createVO(user);

		vo.setFiltro("{gA:[{c:{f:'ID_PERFIL',o:'',vc:'" + idPerfil.toString() + "'}}]}");

		vo.setPaginate(false); // sem isso se tivermos mais de 20 nao viria

		try {
			return org.springframework.util.StringUtils.arrayToDelimitedString(
					extractArrayFromList(ser.lista(vo, user), Constants.subject_key, true), "^,^");
		} catch (ConfigurationException e) {
			LogUtil.exception(e, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		return "";
	}

	public String getMailUser(List ids, GenericService sm, Usuario user) {

		if (ids == null || ids.size() == 0) {
			return "";
		}

		ServiceImpl ser = (ServiceImpl) user.getService(Constants.subject_service);

		GenericVO vo = ser.createVO(user);

		vo.setFiltro("{gA:[{c:{f:'ID_USUARIO',o:'in',vc:'" + org.springframework.util.StringUtils
				.arrayToDelimitedString(extractArrayFromList(ids, Constants.subject_key, true), ",") + "'}}]}");
		vo.put("SEGMENTBY", "FALSE");

		vo.setPaginate(false); // sem isso se tivermos mais de 20 nao viria

		List users = null;
		try {
			users = ser.lista(vo, user);
		} catch (ConfigurationException e1) {
			LogUtil.exception(e1, user);
		} catch (Exception e1) {
			LogUtil.exception(e1, user);
		}
		String str = "";
		for (Iterator iterator = users.iterator(); iterator.hasNext();) {
			Map object = (Map) iterator.next();
			str += object.get("NOME") + " <" + object.get("EMAIL") + ">,";
		}

		if (str.trim().endsWith(",")) {
			str = str.trim().substring(0, str.trim().length() - 1);
		}

		try {
			return str;
		} catch (ConfigurationException e) {
			LogUtil.exception(e, user);
		} catch (Exception e) {
			LogUtil.exception(e, user);
		}
		return "";
	}



	public List match(final List a, final Object field, final Object value) {
		final List b = new ArrayList();
		final int i = 0;
		if (a == null) {
			return b;
		}
		for (final Iterator iterator = a.iterator(); iterator.hasNext();) {
			final Map object = (Map) iterator.next();
			if (value == null && object.get(field) == null) {
				b.add(object);
				continue;
			}
			if (object.get(field) != null && value != null
					&& (object.get(field).equals(value) || object.get(field).toString().equals(value))) {
				if (value == null) {
					continue;
				} else {
					b.add(object);
				}
			} else {
				if (value != null && value.toString().endsWith("ne null") && object.get(field) != null) {
					b.add(object);
				}
				if (object.get(field) == value) {
					b.add(object);
				}
			}

		}

		return b;

	}

	public List match(final List a, final Object[] field, final Object[] value) {
		List e = a;
		for (int i = 0; i < value.length; i++) {
			e = match(e, field[i], value[i]);
		}
		return e;
	}

	public void matchService(final Map key, final String name, final Object value, final boolean onlyNames,
			final Usuario user) {

		if (key.get("service") == null) {
			return;
		}
		final String[] s = key.get("service").toString().split("\\.");
		key.put("service", s[0]);

		if (s.length > 1) {
			key.put("serviceMethod", s[1]);
		} else {
			if (key.get("method") != null && key.get("method").toString().trim().length() > 0
					&& key.get("serviceMethod") == null) {
				key.put("serviceMethod", key.get("method").toString());
			} else if (key.get("serviceMethod") == null) {
				key.put("serviceMethod", "obtem");
			}
		}
		if (key != null && key.get("service") != null) {

			final GenericService aux = (GenericService) user.getService((String) key.get("service"));

			if (aux == null) {
				return;
				// throw new ConfigurationException(
				// "Service nao declarado no componente" + key,
				// ConfigurationException.TYPE.PARAM_NOT_FOUND);
			}

			Map k = null;
			if (key.get("labelValue") == null) {
				k = aux.getPrimaryKey();
			} else {
				final List a = match((List) aux.getAllFields(user), Constants.name, key.get("labelValue").toString());
				if (a.size() > 0) {
					k = (Map) a.get(0);
				}
			}
			if (k != null && k.get("service") != null) {
				final GenericVO svo = aux.createVO(user);
				svo.match(key);
				if (onlyNames) {
					//
					// **EVSER

					LogUtil.debug("// **EVSER", user);
					// key.put(k.get("key"),
					// evalService(svo.putAllAndGet(key, false), name,
					// user));
				}
				key.put("service", k.get("service"));
				key.put("key", k.get("key"));
				key.put("labelValue", k.get("labelValue"));
				if (k.get("foneticSearch") != null)
					key.put("foneticSearch", k.get("foneticSearch"));
				// System.out.println("MATHSERVICE");
				matchService(key, name, svo.get(k.get("key")), user);

			} else {
				if (k != null && k.get("foneticSearch") != null)
					key.put("foneticSearch", k.get("foneticSearch"));
				key.remove("FILTRO"); // limpando o filtro da VO
				key.remove("CONTEXTPARAMS"); // limpando o filtro da VO
				if (aux.getName() != null && key.get("service") == null) {
					key.put("service", aux.getName());
				}
				if (aux.getProperties().get(Constants.NOME_CHAVE) != null && key.get("key") == null) {
					key.put("key", aux.getProperties().get(Constants.NOME_CHAVE).toString());
				}
				if (aux.getProperties().get(Constants.MAIN_FIELD) != null && key.get("labelValue") == null) {
					Map mf = aux.getMainField((String) key.get("serviceMethod"), user);

					key.put("labelValue", mf.get(Constants.name));
					if (mf.get("foneticSearch") != null)
						key.put("foneticSearch", mf.get("foneticSearch"));
				}

			}

		}
	}

	public void matchService(final Map key, final String name, final Object value, final Usuario user) {
		matchService(key, name, value, true, user);
	}

	public String mergeFiltro(String o, String n) {
		// tenta colocar no valor de o o grupo and se existir

		if (o != null && o.trim().length() > 0) {
			if (o.startsWith("{gO:") && n.startsWith("{gA:")) {
				final String tmp = n;
				n = o;
				o = tmp;
			}
			if (!((o.toLowerCase().indexOf("c:") > -1 || o.toLowerCase().indexOf("\"c\":") > -1)
					&& (o.toLowerCase().indexOf("f:") > -1 || o.toLowerCase().indexOf("\"f\":") > -1))) {
				return n;
			}
			final Map old = JSONtoMap(o);
			if (old.size() == 0) {
				return n;
			}
			((List) old.get(old.keySet().toArray()[0])).add(JSONtoMap(n));
			return JSONserialize(old);

		} else {
			return n;
		}
	}

	public String mergeFiltro(String o, String n, boolean ignoreEquals) {
		// tenta colocar no valor de o o grupo and se existir

		if (o != null && o.trim().length() > 0) {
			if (o.startsWith("{gO:") && n.startsWith("{gA:")) {
				final String tmp = n;
				n = o;
				o = tmp;
			}
			if (!(o.toLowerCase().indexOf("c:") > -1 && o.toLowerCase().indexOf("f:") > -1)) {
				return n;
			}
			final Map old = JSONtoMap(o);
			if (old.size() == 0) {
				return n;
			}
			((List) old.get(old.keySet().toArray()[0])).add(JSONtoMap(n));
			return JSONserialize(old);

		} else {
			return n;
		}
	}

	/**
	 * Metodo monthsBetweenDates
	 * 
	 * @param Date
	 *            Valor para a varievel date1
	 * @param Date
	 *            Valor para a varievel date2
	 * @return String
	 */
	public int monthsBetweenDates(final Date date1, final Date date2) {
		int month1, month2, year1, year2;

		final Calendar cal = Calendar.getInstance();
		cal.setTime(date1);
		month1 = cal.get(Calendar.MONTH);
		year1 = cal.get(Calendar.YEAR);

		cal.setTime(date2);
		month2 = cal.get(Calendar.MONTH);
		year2 = cal.get(Calendar.YEAR);

		return 12 * (year2 - year1) + month2 - month1;
	}

	/**
	 * Metodo numberFormat
	 * 
	 * @param double
	 *            Valor para a varievel valor
	 * @return String
	 */
	public String numberFormat(final double valor) {
		final DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
		simbolos.setDecimalSeparator(',');
		simbolos.setGroupingSeparator('.');
		final DecimalFormat form = new DecimalFormat("#,##0.00", simbolos);
		return form.format(valor);
	}

	/**
	 * Metodo parseDate
	 * 
	 * @param String
	 *            Valor para a varievel data
	 * @param String
	 *            Valor para a varievel pattern
	 * @return Date
	 */
	public Date parseDate(final String data, final String pattern) {
		Date result = null;
		if (pattern == null) {
			SimpleDateFormat formatter = null;
			if (data.contains("UTC-")) {
				formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss 'UTC'Z yyyy", Locale.ENGLISH);
			} else if (data.contains("GMT-")) {
				formatter = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss 'GMT'Z", Locale.ENGLISH);
			} else {
				formatter = new SimpleDateFormat("yyyy-MM-dd");
			}
			try {
				return formatter.parse(data);
			} catch (ParseException e) {
				return null;
			}
		} else {
			dateFormat.applyPattern(pattern);
			try {
				return dateFormat.parse(data);
			} catch (final Exception e) {
				return null;
			}
		}
	}

	public String px2cm(final double cm) {
		String a = String.valueOf(Math.round(cm / 283 * 10));
		if (a.indexOf(".") > -1) {
			a = a.substring(0, a.indexOf("."));
		}
		return a;
	}

	public String px2cm(final int cm) {

		return String.valueOf(cm / 283 * 10);
	}

	/**
	 * Metodo removePontos
	 * 
	 * @param String
	 *            Valor para a varievel x
	 * @return String
	 */
	public final String removePontos(final String x) {
		String out = "";
		for (int i = 0; i < x.length(); i++) {
			if (x.charAt(i) != '.' && Character.isDigit(x.charAt(i))) {
				out += x.charAt(i);
			}
		}
		return out;
	}

	public Map renameKeys(final Map jsoNtoMap, final String s) {

		final Map a = new HashMap();
		final Iterator i = jsoNtoMap.keySet().iterator();
		while (i.hasNext()) {
			final Object object = i.next();
			a.put(s + "_" + object, jsoNtoMap.get(object));

		}
		return a;
	}

	/**
	 * Metodo replace
	 * 
	 * @param String
	 *            Valor para a varievel aInput
	 * @param String
	 *            Valor para a varievel aOldPattern
	 * @param String
	 *            Valor para a varievel aNewPattern
	 * @return String
	 */
	public String replace(final String aInput, final String aOldPattern, final String aNewPattern) {
		try {
			if (aOldPattern.equals(aNewPattern)) {
				return aInput;
			}

			if (aInput.indexOf(aOldPattern) == -1) {
				return aInput;
			}
			final StringBuffer result = new StringBuffer();
			// startIdx and idxOld delimit various chunks of aInput; these
			// chunks always end where aOldPattern begins
			int startIdx = 0;
			int idxOld = 0;
			while ((idxOld = aInput.indexOf(aOldPattern, startIdx)) >= 0) {
				// grab a part of aInput which does not include aOldPattern
				result.append(aInput.substring(startIdx, idxOld));
				// add aNewPattern to take place of aOldPattern
				result.append(aNewPattern);
				// LogUtil.debug("apendou " +aNewPattern+ " no lugar de "+
				// aOldPattern );
				// reset the startIdx to just after the current match, to see
				// if there are any further matches
				startIdx = idxOld + aOldPattern.length();
			}
			// the final chunk will go to the end of aInput
			result.append(aInput.substring(startIdx));
			return result.toString();

		} catch (final Exception ex) {
			// LogUtil.exception(ex,null);
			return aInput;
		}

	}

	public String replace(final String aInput, final String[] aOldPattern, final String[] aNewPattern) {

		if (aOldPattern.length != aNewPattern.length) {
			throw new RuntimeException("passagem de parametros para replace incorreta");
		} else {
			String result = aInput;
			for (int i = 0; i < aNewPattern.length; i++) {
				result = replace(result, aOldPattern[i], aNewPattern[i]);

			}
			return result;
		}

	}

	public String replaceFuncionDialeto(String q, final Usuario user) {

		if (user == null || Dialeto.getInstance().getDialeto(user) == null) {
			return q;
		}
		// TODO replace || por getConcat
		q = replaceFunction(q, "DIFF_DATE", " " + Dialeto.getInstance().getDialeto(user)
				.getDiffDate("$replace$,$replace1$").replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");

		q = replaceFunction(q, "diff_date", " " + Dialeto.getInstance().getDialeto(user)
				.getDiffDate("$replace$,$replace1$").replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");

		q = replaceFunction(q, "TRUNC_DATE", " " + Dialeto.getInstance().getDialeto(user).getTruncDate("$replace$")
				.replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");
		q = replaceFunction(q, "trunc_date", " " + Dialeto.getInstance().getDialeto(user).getTruncDate("$replace$")
				.replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");
		q = replaceFunction(q, "LIMIT", " " + Dialeto.getInstance().getDialeto(user).getTrunc("$replace$")
				.replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");
		q = replaceFunction(q, "limit", " " + Dialeto.getInstance().getDialeto(user).getTrunc("$replace$")
				.replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");

		// q = replaceFunction(
		// q,
		// "SUBSTR",
		// " "
		// + Dialeto.getInstance().getDialeto(user)
		// .getSubStr("$replace$", "$replace1$", "$replace2")
		// .replaceAll("\\(", "[").replaceAll("\\)", "]")
		// + " ");
		//
		// q = replaceFunction(
		// q,
		// "substr",
		// " "
		// + Dialeto.getInstance().getDialeto(user)
		// .getSubStr("$replace$", "$replace1$", "$replace2")
		// .replaceAll("\\(", "[").replaceAll("\\)", "]")
		// + " ");

		q = replace(q, "CONCAT_OPERATOR", " " + Dialeto.getInstance().getDialeto(user).getConcatOperator()

				+ " ");
		q = replace(q, "concat_operator", " " + Dialeto.getInstance().getDialeto(user).getConcatOperator()

				+ " ");

		q = replaceFunction(q, "CAST_NUM", " " + Dialeto.getInstance().getDialeto(user).getToNumber("$replace$")
				.replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");
		q = replaceFunction(q, "cast_num", " " + Dialeto.getInstance().getDialeto(user).getToNumber("$replace$")
				.replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");

		q = replaceFunction(q, "TO_CHAR", " " + Dialeto.getInstance().getDialeto(user)
				.getToChar("$replace$", java.sql.Types.VARCHAR).replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");
		q = replaceFunction(q, "to_char", " " + Dialeto.getInstance().getDialeto(user)
				.getToChar("$replace$", java.sql.Types.VARCHAR).replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");
		q = replaceFunction(q, "DATE_CHAR", " " + Dialeto.getInstance().getDialeto(user)
				.getToChar("$replace$", java.sql.Types.DATE).replaceAll("\\(", "[").replaceAll("\\)", "]")

				+ " ");

		q = replaceFunction(q, "TIME_CHAR", " " + Dialeto.getInstance().getDialeto(user)
				.getToChar("$replace$", java.sql.Types.TIME).replaceAll("\\(", "[").replaceAll("\\)", "]") + " ");

		q = replaceFunction(q, "UPPERCLOB", " " + " UPPER(" + Dialeto.getInstance().getDialeto(user)
				.getToChar("$replace$", java.sql.Types.VARCHAR).replaceAll("\\(", "[").replaceAll("\\)", "]") + " ) ");

		return q.replaceAll("\\]", ")").replaceAll("\\[", "(")
				.replaceAll("SYSDATE", Dialeto.getInstance().getDialeto(user).getSysdate())
				.replaceAll("sysdate", Dialeto.getInstance().getDialeto(user).getSysdate())
				.replaceAll("SB_S", Dialeto.getInstance().getDialeto(user).getSubStrName())
				.replaceAll("sb_s", Dialeto.getInstance().getDialeto(user).getSubStrName());
	}

	public String replaceFunction(String q, final String old, final String _new) {
		while (q.indexOf(old) != -1) {
			String sub = q.substring(q.indexOf(old) + old.length()); // sufix

			final String tmp = q.substring(0, q.indexOf(old)) + " "; // prefix
			String field = " " + sub.substring(sub.indexOf("(") + 1, sub.indexOf(")"));
			field = " " + field;
			sub = sub.substring(sub.indexOf(")") + 1, sub.length());
			if (field.indexOf(";") > 0) {
				final String[] a = field.split(";");
				try {
					q = tmp + " "
							+ replace(_new, new String[] { "$replace$", "$replace1$" }, new String[] { a[0], a[1] })
							+ " " + sub;
				} catch (final Exception e) {
					LogUtil.exception(e, null);
				}
			}
			if (field.indexOf(",") > 0) {
				final String[] a = field.split(",");
				try {
					q = tmp + " "
							+ replace(_new, new String[] { "$replace$", "$replace1$" }, new String[] { a[0], a[1] })
							+ " " + sub;
				} catch (final Exception e) {
					LogUtil.exception(e, null);
				}
			}

			else {
				q = tmp + " " + replace(_new, "$replace$", " " + field) + " " + sub;
			}
		}
		return q;

	}

	public String replaceVarsMap(String var, final Map<String, Object> values, final Usuario user) {
		final Iterator<String> i = values.keySet().iterator();
		String key;

		while (i.hasNext()) {
			key = i.next();
			if (values.get(key) == null) {
				var = replace(var, "#" + key + "#", "null");
			} else {
				var = replace(var, "#" + key + "#", values.get(key).toString());
			}
		}
		return EvaluateInString(replaceFuncionDialeto(var, user), createContext(values, user));
	}

	public BufferedImage resize(final BufferedImage image, final int width, final int height) throws IOException {
		// final int type = image.getType() == 0 ? BufferedImage.TYPE_INT_ARGB :
		// image.getType();
		// final BufferedImage resizedImage = new BufferedImage(width, height,
		// type);
		// final Graphics2D g = resizedImage.createGraphics();
		// g.setComposite(AlphaComposite.Src);
		// g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
		// RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		// g.setRenderingHint(RenderingHints.KEY_RENDERING,
		// RenderingHints.VALUE_RENDER_QUALITY);
		// g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		// RenderingHints.VALUE_ANTIALIAS_ON);
		// g.drawImage(image, 0, 0, width, height, null);
		// g.dispose();
		// return resizedImage;

		BufferedImage thumbnail = Scalr.resize(image, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, width, height,
				Scalr.OP_ANTIALIAS);
		return thumbnail;

	}

	public Map reverseMap(final Map object) {
		final HashMap map = new HashMap();
		final Iterator i = object.keySet().iterator();
		while (i.hasNext()) {
			final Object object2 = i.next();
			map.put(object.get(object2), object2);

		}

		return map;
	}

	public double round(final double d, final int decimalPlace) {
		// see the Javadoc about why we use a String in the constructor
		// http://java.sun.com/j2se/1.5.0/docs/api/java/math/BigDecimal.html#
		// BigDecimal(double)
		BigDecimal bd = new BigDecimal(Double.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	public String semMascara(final String value) {
		final StringBuffer result = new StringBuffer();
		if (value == null) {
			return value;
		}
		for (int i = 0; i < value.length(); i++) {
			if (Character.isDigit(value.charAt(i))) {
				result.append(value.charAt(i));
			}
		}

		return result.toString();
	}

	public void SortCollection(final List hashmap, final String sort) {
		java.util.Collections.sort(hashmap, new Comparator() {
			@Override
			public int compare(final Object o1, final Object o2) {
				try {
					if (((HashMap) o2).get(sort) != null) {
						Object a = ((HashMap) o2).get(sort).toString();
						Object b = ((HashMap) o1).get(sort).toString();
						try {
							a = new BigDecimal(a.toString());
							b = new BigDecimal(b.toString());
							return ((BigDecimal) b).compareTo((BigDecimal) a);
						} catch (final Exception e) {

						}
						if (sort.equalsIgnoreCase("label")) {
							throw new GenericRuntimeException(
									"substituir por um metodo SORT passando o evaluate do label");
						}
						return ((String) b).compareTo((String) a);
					} else {
						return -1;
					}
				} catch (final Exception e) {
					return 1;
				}
			}
		});
	}

	public List SortCollection(List hashmap, final String sort, Object[] s) {
		List n = new ArrayList();
		for (int i = 0; i < s.length; i++) {
			List m = match(hashmap, sort, s[i]);
			n.addAll(m);
			hashmap.removeAll(m);
		}
		n.addAll(hashmap);
		return n;
	}

	/**
	 * Metodo stringToDate
	 * 
	 * @param String
	 *            Valor para a varievel data
	 * @return Date
	 */
	public Date stringToDate(final String data) {
		dateFormat.applyPattern("dd/MM/yyyy");
		try {
			return dateFormat.parse(data);
		} catch (final Exception e) {
			return null;
		}
	}

	/**
	 * Metodo stringToDateConcatAtualTime
	 * 
	 * @param String
	 *            Valor para a varievel data
	 * @return Date
	 */
	public Date stringToDateConcatAtualTime(final String data) {
		final String dia = data.substring(0, 2);
		final String mes = data.substring(3, 5);
		final String ano = data.substring(6, 10);

		final Calendar cal = Calendar.getInstance();

		cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dia));
		cal.set(Calendar.MONTH, Integer.parseInt(mes) - 1);
		cal.set(Calendar.YEAR, Integer.parseInt(ano));
		cal.set(Calendar.HOUR_OF_DAY, Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, Calendar.getInstance().get(Calendar.MINUTE));

		cal.set(Calendar.SECOND, Calendar.getInstance().get(Calendar.SECOND));
		// LogUtil.debug( new Date(cal.getTime().getTime()).toString());
		return new Date(cal.getTime().getTime());

	}

	/**
	 * Metodo stringToDateTime
	 * 
	 * @param String
	 *            Valor para a varievel data
	 * @return Date
	 */
	public Date stringToDateTime(final String data) {
		dateFormat.applyPattern("dd/MM/yyyy HH:mm:ss");
		try {
			// LogUtil.debug(dateFormat.parse(data));
			return dateFormat.parse(data);
		} catch (final Exception e) {
			LogUtil.exception(e, null);
			return null;
		}
	}

	/**
	 * getStringToHash<br>
	 * Converte a um "hash" que foi convertido para string de volta para um
	 * HashMap.<br>
	 * <br>
	 * Exemplo de uso:<br>
	 * &nbsp;&nbsp;&nbsp;&nbsp;String dadosStr = "{ID_PROCESSO=2,
	 * CAMPO1=1,2,3,4, NOTA=Teste1=teste2+1}";<br>
	 * &nbsp;&nbsp;&nbsp;&nbsp;HashMap dadosHash = getStringToHash(dadosStr);
	 * <br>
	 * <br>
	 * Obs: o nome da chave deve seguir o padro [A-Z][_0-9A-Z]*, mesmo padro
	 * utilizado nas colunas do banco de dados.<br>
	 * 
	 * @param name
	 *            String no formato "hash".
	 * @return HashMap Retorna o HashMap da string que foi passada.
	 * @since 4.5
	 */
	public Map<String, String> StringToMap(String dados) {
		final Map<String, String> map = new HashMap<String, String>();
		Pattern pat = Pattern.compile("[{}]*"); // Remove as chaves da string
		// hash quando existir.
		Matcher mat = pat.matcher(dados);
		Matcher matAux = pat.matcher(dados);
		dados = mat.replaceAll("");

		if (dados.length() > 0) { // Se depois da remoo a string no for
			// vazia procura pelo padro das KEYs
			dados = ", " + dados; // Adiciona ", " no inicio da string de hash
			// para que a primeira KEY fique no padro.
			pat = Pattern.compile(", [A-Z][_0-9A-Z]*=");
			mat = pat.matcher(dados);
			matAux = pat.matcher(dados);
			dados = mat.replaceAll("#@") + "&@"; // Identifica as KEYs e
			// substitui por ##
			final String[] values = dados.split("#@"); // Realiza o split e
			// obtem o
			// VALUE das KEYs
			values[values.length - 1] = values[values.length - 1].substring(0, values[values.length - 1].length() - 2);
			final String[] keys = new String[values.length - 1];
			int x = 0;

			while (matAux.find()) { // Encontra e obtem todas as KEYs
				keys[x] = matAux.group().replaceAll("=", "").replaceAll(", ", "");
				x++;
			}

			for (int i = 0; i < keys.length; i++) { // Monta o HashMap baseado
				// nos dados obtidos da
				// string do hash.
				map.put(keys[i], values[i + 1]);
			}
		}
		return map;
	}

	/**
	 * Metodo stringToTime
	 * 
	 * @param String
	 *            Valor para a varievel time
	 * @return Date
	 */
	public Date stringToTime(final String time) {
		dateFormat.applyPattern("HH:mm");
		try {
			return dateFormat.parse(time);
		} catch (final Exception e) {
			return null;
		}
	}

	public List subCollection(final List a, final String group) {

		final Map eg = new HashMap();
		Map e = new HashMap();
		final Map items = new LinkedHashMap();
		for (final Iterator iterator = a.iterator(); iterator.hasNext();) {
			final Map object = (Map) iterator.next();
			if (group != null) {
				if (items.get(object.get(group)) == null) {
					e = new HashMap();
					items.put(object.get(group), e);
				} else {
					e = (Map) items.get(object.get(group));
				}
			}

			for (final Iterator i = object.keySet().iterator(); i.hasNext();) {
				final Object data = i.next();
				if (!e.containsKey(data)) {

					e.put(data, new BigDecimal(0));
				}

				try {
					final double um = ((BigDecimal) e.get(data)).doubleValue();

					final double dois = new BigDecimal(object.get(data).toString()).doubleValue();

					e.put(data, new BigDecimal(Math.max(um, dois) - Math.min(um, dois)));
				} catch (final Exception ex) {
					e.put(data, object.get(data));
				}

			}

		}
		if (group == null) {
			final List r = new ArrayList();
			r.add(e);
			return r;
		} else {
			return new ArrayList(items.values());
		}

	}

	public List sumCollection(final List a, final String group) {

		final Map eg = new HashMap();
		Map e = new HashMap();
		final Map items = new LinkedHashMap();
		for (final Iterator iterator = a.iterator(); iterator.hasNext();) {
			final Map object = (Map) iterator.next();
			if (group != null) {
				if (items.get(object.get(group)) == null) {
					e = new HashMap();
					items.put(object.get(group), e);
				} else {
					e = (Map) items.get(object.get(group));
				}
			}

			for (final Iterator i = object.keySet().iterator(); i.hasNext();) {
				final Object data = i.next();
				if (!e.containsKey(data)) {

					e.put(data, new BigDecimal(0));
				}

				try {
					e.put(data, new BigDecimal(((BigDecimal) e.get(data)).doubleValue()
							+ new BigDecimal(object.get(data).toString()).doubleValue()));
				} catch (final Exception ex) {
					e.put(data, object.get(data));
				}

			}

		}
		if (group == null) {
			final List r = new ArrayList();
			r.add(e);
			return r;
		} else {
			return new ArrayList(items.values());
		}

	}

	/**
	 * Metodo timeToString
	 * 
	 * @param Date
	 *            Valor para a varievel time
	 * @return String
	 */
	public String timeToString(final Date time) {
		dateFormat.applyPattern("HH:mm");
		return dateFormat.format(time);
	}

	/**
	 * 
	 * Metodo transformaHoraMilisegundo
	 * 
	 * 
	 * 
	 * @return Retorna Milisegundo em string.Exemplo : number
	 * 
	 * 
	 */

	// public HashMap JSONtoMap(String json)
	// {
	// JSONDeserializer<HashMap> d = new JSONDeserializer<HashMap>();
	// return d.use(null, HashMap.class).deserialize(json);
	// }

	public int transformaHoraMilisegundo(final String Hora) throws Exception {

		int Hrs = 0, Minut = 0;
		final int seg = 0, mseg = 0;
		int horamilisegundos = 0;

		final String total = Hora.substring(0, Hora.toString().lastIndexOf(":"));

		try {

			Hrs = Integer.parseInt(total.substring(0, 2));

			Minut = Integer.parseInt(total.substring(3, 5));

		} catch (final Exception e) {

			Hrs = Integer.parseInt(total.substring(0, 3));

			Minut = Integer.parseInt(total.substring(4, 5));

		}

		horamilisegundos = mseg + seg * 1000 + Minut * 60000

				+ Hrs * 3600000;

		// LogUtil.debug("horamilisegundos ::: >> " + horamilisegundos);

		return horamilisegundos;

	}

	public String transformaMilisegundoHora(final String milis)

			throws Exception {

		int Hrs, Minut, seg, milisegundo;

		String Hora = null, Minuto = null, Segundo = null;

		milisegundo = Integer.parseInt(milis);

		Hrs = milisegundo / 3600000;

		milisegundo = milisegundo % 3600000;

		Minut = milisegundo / 60000;

		milisegundo = milisegundo % 60000;

		seg = milisegundo / 1000;

		if (String.valueOf(Hrs).length() < 2) {
			Hora = "0" + String.valueOf(Hrs);
		} else {
			Hora = String.valueOf(Hrs);
		}

		if (String.valueOf(Minut).length() < 2) {
			Minuto = "0" + String.valueOf(Minut);
		} else {
			Minuto = String.valueOf(Minut);
		}

		if (String.valueOf(seg).length() < 2) {
			Segundo = "0" + String.valueOf(seg);
		} else {
			Segundo = String.valueOf(seg);
		}

		// LogUtil.debug("HORAS ::: >> " + Hora + ":"+ Minuto+":"+Segundo);

		return Hora + ":" + Minuto + ":" + Segundo;

	}

	/**
	 * Metodo verificaNumero
	 * 
	 * @param String
	 *            Valor para a varievel valor
	 * @return boolean
	 */
	public boolean verificaNumero(final String valor) {
		if (valor == null || valor.equals("")) {
			return false;
		}
		try {
			return true;
		} catch (final Exception e) {
			return false;
		}
	}

	public File writeToTmp(final InputStream file, final String fileName) {
		try {
			return writeToDisk(file, fileName, System.getProperty("java.io.tmpdir"));
		} catch (FileNotFoundException e) {
			LogUtil.exception(e, null);
		} catch (IOException e) {
			LogUtil.exception(e, null);
		}
		return null;
	}

	public File writeToTmp(byte[] br, String fileName) {
		final ByteArrayInputStream fileInputStream = new ByteArrayInputStream(br);
		try {
			return writeToDisk(fileInputStream, fileName, System.getProperty("java.io.tmpdir"));
		} catch (FileNotFoundException e) {
			LogUtil.exception(e, null);
		} catch (IOException e) {
			LogUtil.exception(e, null);
		}
		return null;
	}

	public File writeToDisk(final InputStream file, final String fileName, final String destinationPath)
			throws FileNotFoundException, IOException {
		final File g = new File(destinationPath + "/" + fileName);

		final OutputStream out = new FileOutputStream(g);
		final byte buf[] = new byte[1024];
		int len;
		while ((len = file.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		out.close();
		file.close();

		return g;
	}

	public File writeToDisk(final MultipartFile file, final String fileName, final String destinationPath)
			throws FileNotFoundException, IOException {

		InputStream stream = null;
		OutputStream bos = null;
		int bytesRead = 0;
		long fileSize = 0;
		byte[] buffer = null;

		stream = file.getInputStream();
		bos = new FileOutputStream(destinationPath + fileName);

		fileSize = file.getSize();

		bytesRead = 0;
		buffer = new byte[(int) fileSize];
		while ((bytesRead = stream.read(buffer, 0, (int) fileSize)) != -1) {
			bos.write(buffer, 0, bytesRead);
		}

		bos.close();
		stream.close();

		return new File(destinationPath + fileName);
	}

	public List<Map<String, String>> getEstadoCivil(final GenericVO vo, final Usuario user) {
		final List<Map<String, String>> labels = new ArrayList<Map<String, String>>();

		Map<String, String> label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.estado_civil.solteiro"));
		label.put("ID", "S");
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.estado_civil.casado"));
		label.put("ID", "C");
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.estado_civil.separado"));
		label.put("ID", "E");
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.estado_civil.divorciado"));
		label.put("ID", "D");
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.estado_civil.viuvo"));
		label.put("ID", "V");
		labels.add(label);

		label = new HashMap<String, String>();
		label.put(Constants.name, user.getLabel("label.estado_civil.nd"));
		label.put("ID", "N");
		labels.add(label);

		return labels;
	}

	public List<Map<String, Integer>> getHoras(final GenericVO vo, final Usuario user) {
		return getRange(24);
	}

	public List<Map<String, Integer>> getMinutos(final GenericVO vo, final Usuario user) {
		return getRange(59);
	}

	private List<Map<String, Integer>> getRange(final int range) {
		final ArrayList<Map<String, Integer>> labels = new ArrayList<Map<String, Integer>>();
		Map<String, Integer> label = new HashMap<String, Integer>();

		for (int i = 0; i < range; i++) {
			label = new HashMap<String, Integer>();
			label.put(Constants.name, i);
			label.put("ID", i);
			labels.add(label);
		}
		return labels;
	}

	@Override
	public List<Map<String, String>> getTrueOrFalse(final GenericVO vo, final Usuario user) {
		final List<Map<String, String>> labels = new ArrayList<Map<String, String>>();
		Map<String, String> label = new HashMap<String, String>();

		label.put("NAME", user.getLabel("label.sim"));
		label.put("ID", "T");
		labels.add(label);

		label = new HashMap<String, String>();
		label.put("NAME", user.getLabel("label.nao"));
		label.put("ID", "F");
		labels.add(label);
		return labels;

	}

	public GenericVO obtem(final String id, final String Filter, final Usuario user) throws Exception {

		return null;
	}

	public String remove(final String[] ids, final Usuario user) throws Exception {
		return String.valueOf(ids.length);
	}

	public String getActionUrl(GenericService service, Usuario user) {
		String a = user.getSystemPath();
		if (!a.equalsIgnoreCase("/" + service.getName().replaceAll(Constants.SERVICE_BASE, Constants.ACTION_BASE)
				+ Constants.ACTION_SUFIX))
			a+="/" + service.getName().replaceAll(Constants.SERVICE_BASE, Constants.ACTION_BASE)
					+ Constants.ACTION_SUFIX;
		return a;
	}

	public String subtractDay(String valor1) {
		try {
			long time = ((Timestamp) new DataFormat().stringToTimestamp(valor1)).getTime();
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(time);
			cal.add(Calendar.DAY_OF_MONTH, -1);

			return new DataFormat().fromTimestamp(new Timestamp(cal.getTimeInMillis()));
		} catch (Exception e) {
			return new DataFormat().fromTimestamp(new Timestamp(System.currentTimeMillis()));
		}
	}

	/**
	 * Calcula a diferenea de duas datas em dias <br>
	 * 
	 * @author efraimfarias
	 * @param dataInicial
	 * @param dataFinal
	 * @return quantidade de dias existentes entre a dataInicial e dataFinal.
	 */
	public int diferencaEmDias(Date dataInicial, Date dataFinal) {
		int result = 0;
		long diferenca = dataFinal.getTime() - dataInicial.getTime();
		double diferencaEmDias = (diferenca / 1000) / 60 / 60 / 24; // resultado
																	// e
																	// diferenea
																	// entre as
																	// datas em
																	// dias

		result = (int) diferencaEmDias;

		return result;
	}

	/**
	 * Calcula a diferenea de duas datas <br>
	 * 
	 * @author efraimfarias
	 * @param dataInicial
	 * @param dataFinal
	 * @return Lista das Datas existentes entre a dataInicial e dataFinal
	 *         inclusive.
	 */
	public ArrayList<Date> diferencaEntreDatas(Date dataInicial, Date dataFinal) {
		ArrayList<Date> result = new ArrayList<Date>();

		Calendar data = Calendar.getInstance();
		data.setTime(dataInicial);

		int dias = diferencaEmDias(dataInicial, dataFinal);

		result.add(data.getTime());

		for (int x = 1; x <= dias; x++) {
			data.add(Calendar.DAY_OF_MONTH, 1);
			result.add(data.getTime());
		}

		return result;
	}

	public Object decode(String[] s, Usuario user) {
		for (int i = 0; i < s.length; i++) {
			s[i] = decode(s[i], user);
		}
		return s;
	}

	public String millisecondsToMinutes(long millis) {
		return String.format("%d m, %d s", TimeUnit.MILLISECONDS.toMinutes(millis),
				TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
	}

	public Map modifiedMap(Map<String, Map<String, String>> mapValuesFromXQL, String Key, Object Value) {
		Map retu = new HashMap();
		Iterator i = mapValuesFromXQL.keySet().iterator();
		while (i.hasNext()) {
			String object = (String) i.next();
			Map v = new HashMap(mapValuesFromXQL.get(object));
			v.put(Key, Value);
			retu.put(object, v);
		}
		return retu;
	}

	public ParseXmlService createXMLParser(InputStream f) {
		ParseXmlService p = new ParseXmlService();
		try {
			p.setXMLStream(f);
		} catch (Exception e) {
			return null;
		}
		return p;
	}

	public boolean valid(String driver) {
		return driver != null && driver.toString().trim().length() > 0;
	}

	public boolean nullOrTrue(String driver) {
		return driver == null || Boolean.valueOf(driver);
	}

	public boolean validAndTrue(String driver) {
		return driver != null && driver.toString().trim().length() > 0 && Boolean.valueOf(driver);
	}

	public boolean validAndFalse(String driver) {
		return driver != null && driver.toString().trim().length() > 0 && !Boolean.valueOf(driver);
	}

	public Map<String, String> getInterpretaHashAuditoria(GenericVO voCtx, final Usuario user) {

		Map<String, String> result = new HashMap<String, String>();
		String infoAdic = null;

		if (voCtx.get("INFORMACAO_ADICIONAL") != null) {
			infoAdic = voCtx.get("INFORMACAO_ADICIONAL").toString();
		}

		result.put("INFORMACAO_ANTIGA",
				getLabelsOfContentOfAttribute(
						(voCtx.get("INFORMACAO_ANTIGA") == null ? "" : voCtx.get("INFORMACAO_ANTIGA").toString()), user,
						infoAdic));
		result.put("INFORMACAO_NOVA", getLabelsOfContentOfAttribute(
				(voCtx.get("INFORMACAO_NOVA") == null ? "" : voCtx.get("INFORMACAO_NOVA").toString()), user, infoAdic));
		result.put("CHAVE", getLabelsOfContentOfAttribute(
				(voCtx.get("CHAVE") == null ? "" : voCtx.get("CHAVE").toString()), user, infoAdic));

		result.put("INFORMACAO_ADICIONAL", getLabelsOfContentOfAttribute((infoAdic == null ? "" : infoAdic), user,
				(infoAdic == null ? "" : infoAdic)));

		return result;

	}

	public boolean isValidDate(String data) {

		/*
		 * Recebe uma String, e verifica se e uma data valida. Os padroes
		 * reconhecidos sao definidos no String[] formats...
		 */

		String[] formats = { "yyyy-MM-dd", "dd-MM-yyyy", "yyyy-MM-dd HH:mm:ss", "dd-MM-yyyy HH:mm:ss", "yyyy/MM/dd",
				"dd/MM/yyyy", "yyyy/MM/dd HH:mm:ss", "dd/MM/yyyy HH:mm:ss" };

		boolean result = true;
		for (int i = 0; i < formats.length; i++) {
			result = true;
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(formats[i]);
				sdf.setLenient(false);
				sdf.parse(data);
			} catch (ParseException e) {
				result = false;
			} catch (IllegalArgumentException e) {
				result = false;
			} catch (Exception e) {
				result = false;
			}
			if (result == true) {
				break;
			}
		}

		return result;

	}

	public Object getContentTypeFromString(String str) {

		/*
		 * Este metodo recebe uma String como parametro, e verifica o seu
		 * conteudo, retornando um Object do tipo, para ser vefificado com o
		 * instanceOf
		 */

		String resString = "";
		boolean resBoolean = false;
		Number resNumber = 0;
		Date resDate = new Date();

		boolean isString = false;
		boolean isDate = false;
		boolean isNumber = true;
		boolean isBoolean = false;

		if (!str.equals("") && !str.equals(" ")) {
			char[] valChar = str.toCharArray(); // Verifica se e numero...
			for (int i = 0; i < valChar.length; i++) {
				if (!Character.isDigit(valChar[i])) {
					isNumber = false;
				}
			}

			str = str.trim();
			if (!isNumber) {
				if (str.length() == 4 || str.length() == 5) { // Verifica se e
																// boolean..
					if (str.contains("true") || str.contains("false")) {
						isBoolean = true;
					}
				}
			}

			if (!isNumber && !isBoolean) { // Verifica se e uma Data
				isDate = this.isValidDate(str);
			}

			if (!isBoolean && !isNumber && !isDate) { // Comportamento Default -
														// STRING
				isString = true;
			}

			if (isNumber) {
				return resNumber;
			} else if (isBoolean) {
				return resBoolean;
			} else if (isString) {
				return resString;
			} else if (isDate) {
				return resDate;
			} else {
				return str;
			}
		} else {
			return str;
		}
	}

	// Retorna uma String com o conteedo de uma coluna(atributo), formatando
	// com
	// um label cadastrado no sistema.
	public String getLabelsOfContentOfAttribute(String str, Usuario user, String infoAdd) {
		StringBuilder result = new StringBuilder("<br>");
		Set<Entry<String, String>> items = user.getUtil().StringToMap(str).entrySet();

		for (Entry<String, String> i : items) {
			if (!isPropOfVO(i.getKey())) {
				String lb = user.getLabel(infoAdd + "." + i.getKey());
				if (lb.startsWith(infoAdd) && lb.endsWith(i.getKey())) {
					result.append(i.getKey());
				} else {
					result.append(lb);
				}
				result.append(" = ");

				if (i.getValue() == null || i.getValue().isEmpty() || i.getValue().trim().equals("null")) {
					result.append(user.getLabel("lb.auditoria.reg.em.branco"));
				} else {
					result.append(i.getValue());
				}
				result.append(", <br>");
			}
		}
		if (!(result.length() == 4)) {
			result.deleteCharAt(result.lastIndexOf(","));
		}
		return result.toString();
	}

	public boolean isPropOfVO(String v) {
		List<String> props = internalVO.getStaticprops();
		for (String it : props) {
			if (it.equals(v)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Formatadores de nmeros!!!
	 */
	public static String formatoMonetario(final BigDecimal valor, final int qtdCentavos) {
		final NumberFormat format = NumberFormat.getNumberInstance(Local);
		// format = NumberFormat.getCurrencyInstance(util.getLocale());
		format.setMaximumFractionDigits(qtdCentavos);
		format.setMinimumFractionDigits(qtdCentavos);
		String stringRetorno = "0,00";
		try {
			// Assume form is a DecimalFormat
			stringRetorno = format.format(valor);
			if (stringRetorno.equals("-0,00")) {
				stringRetorno = "0,00";
			}
		} catch (final IllegalArgumentException e) {
			// LogUtil.debug("Exceo em
			// MostrarResumoAction.formatoMonetario - valor: " + valor + " -
			// Erro: " + e.getMessage());
		}
		return stringRetorno;
	}

	public NodeList xqlInString(String doc, String xql) {
		NodeList result = null;
		try {
			result = (NodeList) xpath.evaluate(xql, stringToDocument(doc), XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return result;
	}

	public String valid(String systemProperty, String string) {
		return valid(systemProperty) ? systemProperty : string;
	}

	public Object[] removeNull(String[] evaluate) {
		ArrayList a = new ArrayList();
		for (int i = 0; i < evaluate.length; i++) {
			if (evaluate[i] != null && !evaluate[i].toString().equals("null"))
				a.add(evaluate[i]);
		}
		return (Object[]) a.toArray();
	}

	public Document stringToDocument(String xmlSource) throws SAXException, ParserConfigurationException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(new InputSource(new StringReader(xmlSource)));
	}

	public String criptografa(String plainTextPassword) {
		byte[] cleartext = null;

		try {
			cleartext = plainTextPassword.getBytes("UTF8");
			DESKeySpec keySpec = new DESKeySpec(privateKey.getBytes("UTF8"));
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey key = keyFactory.generateSecret(keySpec);
			Cipher cipher = null;

			cipher = Cipher.getInstance("DES");

			cipher.init(Cipher.ENCRYPT_MODE, key);
			String encrypedPwd = null;

			encrypedPwd = new String(java.util.Base64.getEncoder().encode(cipher.doFinal(cleartext)));

			return encrypedPwd;
		} catch (Exception e) {
			LogUtil.exception(e, null);
		}
		return null;

	}

	public String streamToString(InputStream inputStream) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length = 0;
		while ((length = inputStream.read(buffer)) != -1) {
			baos.write(buffer, 0, length);
		}
		return new String(baos.toByteArray());
	}

	public InputStream stringToStream(String str, Usuario user) throws UnsupportedEncodingException {

		return new ByteArrayInputStream(str.getBytes(user.getEncode()));

	}

	// DECODE encryptedPwd String
	public String descriptografa(String encryptedPwd) {
		byte[] encrypedPwdBytes = null;
		try {
			DESKeySpec keySpec = new DESKeySpec(privateKey.getBytes("UTF8"));
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey key = keyFactory.generateSecret(keySpec);
			encrypedPwdBytes = Base64.decodeBase64(encryptedPwd);
			Cipher cipher = null;
			cipher = Cipher.getInstance("DES");

			cipher.init(Cipher.DECRYPT_MODE, key);

			byte[] plainTextPwdBytes = null;

			plainTextPwdBytes = (cipher.doFinal(encrypedPwdBytes));

			return new String(plainTextPwdBytes);
		} catch (Exception e) {
			LogUtil.exception(e, null);
		}
		return null;
	}

	public boolean cronExpressionIsValid(String cron) {
		return org.quartz.CronExpression.isValidExpression(cron);
	}

	public String trim(String detalhes) {
		if (detalhes == null)
			return null;
		detalhes.replaceAll("\r\n", " ");
		detalhes = detalhes.replaceAll("\n", " ");
		detalhes = detalhes.replaceAll("\r", " ");
		detalhes = detalhes.replaceAll("\f", " ");
		detalhes = detalhes.trim();
		return detalhes;
	}

	public String getStringDatePart(String part, String field, Usuario user) {
		return Dialeto.getInstance().getDialeto(user).getStringDatePart(part, field);
	}

	public boolean isNumber(String valor) {
		try {
			new BigDecimal(valor);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public String getHashCode(String e) {
		return String.valueOf(e.hashCode());
	}

	public XPath getXpathInstance() {
		return xpath;
	}

	public String getClientScripts(final Usuario user, final ServletRequest request) {
		String result = "";
		List<Map<String, String>> scripts = (List<Map<String, String>>) user.getSysParser().getListValuesFromXQL(
				"/systems/system[@name='" + user.getSchema() + "']/module/bean//property[@type='clientScript']",
				"order", user, true, true);
		for (Map<String, String> s : scripts) {
			if (s.get("CDATA") != null) {
				result += EvaluateInString(s.get("CDATA"), user.getRequestEvaluationContext(request));
			}
		}
		return result;

	}

	public List<Map<String, String>> getFontes(final GenericVO vo, final Usuario user) {

		final List<Map<String, String>> labels = new ArrayList<Map<String, String>>();
		final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		final Font[] fonts = ge.getAllFonts();

		Map<String, String> label;

		for (final Font f : fonts) {
			label = new HashMap<String, String>();
			label.put("ID", f.getFontName());
			label.put("NOME", f.getFontName());
			labels.add(label);
		}
		if (labels.size() == 0) {
			label = new HashMap<String, String>();
			label.put("ID", user.getDefaultSystemFont());
			label.put("NOME", user.getDefaultSystemFont());
			labels.add(label);
		}
		return labels;
	}

	public String getExtDataWithFonts() {
		String result = "[";
		List<Map<String, String>> fontes = getFontes(null, null);
		for (int i = 0; i < fontes.size(); i++) {
			result += "['" + fontes.get(i).get("ID") + "', '" + fontes.get(i).get("NOME") + "']";
			if ((i + 1) != fontes.size()) {
				result += ",";
			}
		}
		result += "]";
		return result;
	}

	public String getBeanLabel(Usuario user, String beanId) {
		return user.getLabel(ConfigSisAux.getInstance(user).getBeanById(beanId).get("label"));
	}

	public void executeScript(final GenericVO vo, final Usuario user) throws ScriptException {
		getJSEngine().eval((String) vo.get("SCRIPT"), getBindings(user, vo));
	}

	public String powStr(int x, String str, boolean cutLast) {
		String ret = "";
		for (int i = 0; i < x; i++) {
			ret += str;
		}
		if (cutLast)
			return ret.substring(0, ret.length() - 1);
		else
			return ret;
	}

	public static String sumTimes(List<String> times) {

		int h = 0;
		int m = 0;
		int s = 0;
		int[] transfer = new int[2];

		for (String tm : times) {
			if (tm != null && (tm.matches("^(20|21|22|23|[01]\\d|\\d)(([:.][0-5]\\d){1,2})$") || tm.matches("[0-9]"))) {
				String[] hms = tm.split(":");
				if (hms.length == 1) {
					h += Integer.parseInt(hms[0]);
				} else if (hms.length == 2) {
					h += Integer.parseInt(hms[0]);
					m += Integer.parseInt(hms[1]);
				} else if (hms.length == 3) {
					h += Integer.parseInt(hms[0]);
					m += Integer.parseInt(hms[1]);
					s += Integer.parseInt(hms[2]);
				}
			}
		}

		transfer = transferTime(m, h);
		m = transfer[0];
		h = transfer[1];

		transfer = transferTime(s, m);
		s = transfer[0];
		m = transfer[1];

		transfer = transferTime(m, h);
		m = transfer[0];
		h = transfer[1];

		return (h == 0 ? "00" : h) + ":" + (m == 0 ? "00" : m) + ":" + (s == 0 ? "00" : s);
	}

	public static int[] transferTime(int from, int to) {
		while (from >= 60) {
			from -= 60;
			to++;
		}
		return new int[] { from, to };
	}

	public boolean isJsonArray(Object validate) {
		String text = String.valueOf(validate);
		try {
			org.json.JSONArray jsonArray = new org.json.JSONArray(text);
			return true;
		} catch (org.json.JSONException jsonArrayException) {
			return false;
		}
	}

	public boolean isJsonObject(Object validate) {
		String text = validate.toString();
		try {
			org.json.JSONObject jsonObject = new org.json.JSONObject(text);
			return true;
		} catch (org.json.JSONException jsonObjectException) {
			return false;
		}
	}

	public String removeInvalidItemsFromContextParams(Object ctxPrms) {
		if (ctxPrms == null) {
			return "";
		}

		Map contextParams = JSONtoMap(ctxPrms.toString());
		if (contextParams.get("@OVERRIDE_ATTR") != null) {
			contextParams.remove("@OVERRIDE_ATTR");
		}

		return contextParams.toString();
	}

	public boolean isJson(Object validate) {
		if (validate == null) {
			return false;
		} else {
			return isJsonArray(validate) || isJsonObject(validate);
		}
	}

	public String replaceLastString(String text, String strToReplace, String replacement) {
		return text.replaceFirst("(?s)" + strToReplace + "(?!.*?" + strToReplace + ")", replacement);
	}

	public String getDefaultFont(Usuario user) {
		// TODO Auto-generated method stub
		final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		final Font[] fonts = ge.getAllFonts();
		String a = null;
		for (int i = 0; i < fonts.length; i++) {
			if (fonts[i].getFontName().toLowerCase().indexOf("arial") > -1)
				a = fonts[i].getFontName();
		}
		if (a == null)
			for (int i = 0; i < fonts.length; i++) {
				if (fonts[i].getFontName().toLowerCase().indexOf("sans") > -1)
					a = fonts[i].getFontName();
			}

		if (a == null)
			for (int i = 0; i < fonts.length; i++) {
				if (fonts[i].getFontName().toLowerCase().indexOf("mono") > -1)
					a = fonts[i].getFontName();
			}
		if (a == null)
			return fonts[0].getFontName();
		return a;
	}

	public boolean validateMyInstancia(GenericVO vo, Usuario user) {

		if (user.getSession().getAttribute("ID_INSTANCIA") != null && vo.get("ID_PAI") != null) {
			if (user.getSession().getAttribute("ID_INSTANCIA").toString().equals(vo.get("ID_PAI").toString())) {
				return true;
			}
		}
		return false;
	}

	public Timestamp nowTimestamp() {
		// TODO Auto-generated method stub
		return new Timestamp(System.currentTimeMillis());
	}

	public String now() {
		// TODO Auto-generated method stub
		return df.fromTimestamp(getTimestamp());
	}

}
