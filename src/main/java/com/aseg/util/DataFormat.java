package com.aseg.util;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.util.service.UtilServiceImpl;

public class DataFormat {

	private static final SimpleDateFormat DATE;
	private static final SimpleDateFormat DATE_PT_BR;
	private static final SimpleDateFormat DATE_TIME;
	private static final SimpleDateFormat DATE_TIME_PT_BR;
	private static final SimpleDateFormat DB_DATE;
	private static final SimpleDateFormat DB_DATE_TIME;
	private static final SimpleDateFormat TIME;
	private static final SimpleDateFormat TIME_SEC;
	static long llastLogonAdjust = 11644473600000L; // adjust factor for
	private static final Locale Local = new Locale("pt", "BR");

	static {
		DATE = new SimpleDateFormat("dd/MM/yyyy");
		DATE_PT_BR = new SimpleDateFormat("dd/mm/yyyy", Local);
		// TIME = new SimpleDateFormat("HH:mm");
		DB_DATE = new SimpleDateFormat("yyyy-MM-dd");
		DB_DATE_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DATE_TIME = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		DATE_TIME_PT_BR = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Local);

		TIME = new SimpleDateFormat("HH:mm");
		TIME_SEC = new SimpleDateFormat("HH:mm:ss");

	}

	public Timestamp dateToTimestamp(Date date) {

		return new Timestamp(date.getTime());

	}

	public BigDecimal currencyToBigDecimal(String var) {

		final DecimalFormatSymbols dfs = new DecimalFormatSymbols(Local);
		String ds = String.valueOf(dfs.getDecimalSeparator());

		String gs = String.valueOf(dfs.getGroupingSeparator());

		var = var.replaceAll(gs, "");
		var = var.replaceAll(ds, ".");

		// TODO Auto-generated method stub
		return new BigDecimal(var);
	}

	public Time fromStringHora(final String entrada) {
        if(entrada == null || entrada.trim().length()==0)
            return null;
		try {
			return new Time(TIME.parse(entrada).getTime());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block

			try {

				return new Time(TIME_SEC.parse(entrada.toString().substring(11, 13) + ":"
						+ entrada.toString().substring(14, 16) + ":" + entrada.toString().substring(17, 19)).getTime());
			} catch (final Exception e) {

				try {
					return new Time(TIME_SEC.parse(entrada + ":00").getTime());

				} catch (final Exception ex) {
                    try {
                        return new Time(new Long(entrada));
                    }
                    catch (Exception ex1)
                    {
                        return null;
                    }
				}
			}
		}

	}

	public Object formatoMonetario(final Object a, final int c) {
		String o = "";
		if (a != null && a.toString().length() > 0) {

			if (a instanceof BigDecimal) {

				o = formatoMonetario((BigDecimal) a, c);

			} else if (a instanceof Double) {

				o = formatoMonetario(new BigDecimal((Double) a), c);

			} else {
				final String var = (String) a;
				if (var.indexOf("%") > -1) {
					o = var;
				} else if (var.indexOf("URH") > -1) {
					o = formatoMonetario(currencyToBigDecimal(var), c);
					o += " URH";
				}

				else {
					o = formatoMonetario(currencyToBigDecimal(var), c);
				}

			}
		}
		return o;
	}

	//
	// private String fromTimestampToDatabaseDateTime(final Timestamp entrada) {
	// String out = "";
	// try {
	// out = DB_DATE_TIME.format(entrada);
	// return out;
	// } catch (final Exception ex) {
	// ex.printStackTrace();
	// return null;
	// }
	//
	// }
	//
	// private double formatarMonetario(final String valor) {
	// double retorno = 0.00;
	// if (valor != null || valor.trim().length() != 0) {
	// final DecimalFormatSymbols dfs = new DecimalFormatSymbols(Local);
	// // dfs.setDecimalSeparator(',');
	// // dfs.setGroupingSeparator('.');
	// final DecimalFormat format = new DecimalFormat(
	// "#,###,###,###,###.0#", dfs);
	// try {
	// retorno = format.parse(valor).doubleValue();
	// } catch (final ParseException e) {
	// return retorno;
	// }
	// }
	// return retorno;
	// }
	//
	// // Formata o valor com 4 casas decimais
	// private double formatarQuantidade(final String valor) {
	// double retorno = 0.0000;
	// if (valor != null || valor.trim().length() != 0) {
	// final DecimalFormatSymbols dfs = new DecimalFormatSymbols(Local);
	// // dfs.setDecimalSeparator(',');
	// // dfs.setGroupingSeparator('.');
	// final DecimalFormat format = new DecimalFormat(
	// "#,###,###,###,###.0###", dfs);
	// try {
	// retorno = format.parse(valor).doubleValue();
	// } catch (final ParseException e) {
	// e.printStackTrace();
	// }
	// }
	// return retorno;
	// }
	//
	// private String formatoMonetario(final BigDecimal valor) {
	// return formatoMonetario(valor, 2);
	// }

	// private String formatoMonetarioUsuario(final BigDecimal valor,
	// final Usuario user) {
	// try {
	// String v = formatoMonetario(valor, 2);
	// v = user.getUtil().replace(v, ".", "");
	// v = user.getUtil().replace(v, ",",
	// user.getSystemProperty("SEPARADOR_DECIMAL"));
	//
	// return v;
	//
	// } catch (final NumberFormatException e) {
	// e.printStackTrace();
	// } catch (final Exception e) {
	// e.printStackTrace();
	// }
	// return null;
	// }
	//
	// private String fromlong(final long entrada) {
	// return fromTimestamp(new Timestamp(entrada));
	// }
	//
	// private Timestamp fromString(final SimpleDateFormat format,
	// final String date) throws ParseException {
	//
	// return new Timestamp(format.parse(date).getTime());
	// }
	//
	// private String fromString(final String entrada) {
	//
	// try {
	// return entrada.toString().substring(11, 13);
	// } catch (final Exception e) {
	// // LogUtil.debug("[DataFormat -- fromTimestamp] Erro de parse
	// // da data "+entrada);
	// e.printStackTrace();
	// return null;
	// }
	// }

	// private Time fromStringHora(final String entrada) {
	// DateFormat dtFmt = new SimpleDateFormat("HH:mm:ss");
	// try {
	// final Time temp = new Time(dtFmt.parse(
	// entrada.substring(entrada.indexOf(':') - 2,
	// entrada.length())).getTime());
	// return temp;
	// } catch (final Exception e) {
	// dtFmt = new SimpleDateFormat("HH:mm");
	// try {
	// final Time temp = new Time(dtFmt.parse(
	// entrada.substring(entrada.indexOf(':') - 2,
	// entrada.length())).getTime());
	// return temp;
	// } catch (final ParseException e1) {
	//
	// e1.printStackTrace();
	// }
	// }
	// return null;
	// }

	//
	//
	// private String fromTimestampcomHora(final Timestamp entrada) {
	//
	// try {
	// return DATE_TIME.format(entrada);
	// } catch (final Exception e) {
	// // LogUtil.debug("[DataFormat -- fromTimestamp] Erro de parse
	// // da data "+entrada);
	// e.printStackTrace();
	// return null;
	// }
	// }
	public String fromTimestamp(final Timestamp entrada) {

		if (entrada == null) {
			return null;
		}
		try {
			return DATE.format(entrada);
		} catch (final Exception e) {
			LogUtil.exception(e, null);
			return null;
		}
	}

	public String fromTimestampcomHoraptBR(final Timestamp entrada) {
		String out = "";

		try {
			out = DATE_TIME_PT_BR.format(entrada);
			return out;
		} catch (final IllegalArgumentException pexp) {
			try {
				out = DB_DATE_TIME.format(entrada);
				return out;
			} catch (final Exception ex) {
				LogUtil.exception(ex, null);
				return null;
			}
		} catch (final Exception e) {
			// LogUtil.debug("[DataFormat -- fromTimestamp] Erro de parse
			// da data "+entrada);
			LogUtil.exception(e, null);
			return null;
		}
	}

	public Object stringToTimestamp(final Object value) {
		Date a = null;
		if (value == null || value.toString().trim().length() == 0) {
			return null;
		}
		if (value.equals("now")) {
			return new Timestamp(System.currentTimeMillis());
		}
		try {
			a = DATE.parse(value.toString());
		} catch (final Exception e) {
			try {
				a = DATE_PT_BR.parse(value.toString());
			} catch (final Exception e1) {
				try {
					a = DATE_TIME.parse(value.toString());
				} catch (final ParseException e2) {

					try {
						a = DATE_TIME_PT_BR.parse(value.toString());
					} catch (final Exception e3) {
						try {
							a = DB_DATE.parse(value.toString());
						} catch (final Exception e4) {

							try {
								a = DB_DATE_TIME.parse(value.toString());
							} catch (final Exception e5) {
								// convertendo datas LDAP
								try {
									if (value == null || value.equals("0"))
										return null;

									// date Epoch
									a = new Date(Long.parseLong(value.toString()) / 10000 - llastLogonAdjust); //

								} catch (final Exception e6) {
									// convertendo datas LDAP
									LogUtil.exception(e6, null);
								}
							}
						}

					}

				}

			}

		}

		if (a == null) {
			return null;
		} else {
			return new Timestamp(a.getTime());
		}
	}

	/**
	 * 
	 * Formatadores de nmeros!!!
	 * 
	 */
	private String formatoMonetario(final BigDecimal valor, final int qtdCentavos) {
		final NumberFormat format = NumberFormat.getNumberInstance(Local);
		// format = NumberFormat.getCurrencyInstance(util.getLocale());
		format.setMaximumFractionDigits(qtdCentavos);
		format.setMinimumFractionDigits(qtdCentavos);
		String stringRetorno = "0,00";
		try {
			// Assume form is a DecimalFormat
			stringRetorno = format.format(valor);
			if (stringRetorno.equals("-0,00")) {
				stringRetorno = "0,00";
			}
		} catch (final IllegalArgumentException e) {
			// LogUtil.debug("Exceo em
			// MostrarResumoAction.formatoMonetario - valor: " + valor + " -
			// Erro: " + e.getMessage());
		}
		return stringRetorno;
	}

	public Object fromTime(Time substring) {
		// TODO Auto-generated method stub
		return TIME.format(substring);
	}

	public Object fromTimeSec(Time substring) {
		// TODO Auto-generated method stub
		return TIME_SEC.format(substring);
	}

	public Timestamp nowTimestamp() {
		// TODO Auto-generated method stub
		return new Timestamp(System.currentTimeMillis());
	}
}