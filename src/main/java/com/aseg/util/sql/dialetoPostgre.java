package com.aseg.util.sql;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.util.StringUtils;

import com.aseg.config.Constants;
import com.aseg.exceptions.ConfigurationException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.Paginator;
import com.aseg.util.ConnectionManager;
import com.aseg.util.DAOTemplate;
import com.aseg.util.service.UtilServiceImpl;

public class dialetoPostgre implements InterfaceDialeto {

	public final UtilServiceImpl util = new UtilServiceImpl();

	public dialetoPostgre() {
		LogUtil.debug("Dialeto de banco de dados Postgresql utilizado            [OK]", null);
	}

	@Override
	public String alterColumn(final String tableName, final String columnName, final Map<String, String> field) {

		String sql = sqlAlterColumn(tableName, columnName);
		sql += " TYPE " + getDataType(field) + " " + getColumnDefinition(field);
		return sql;
	}

	@Override
	public String alterColumn(final String tableName, final String columnName, final Map<String, String> field,
			final boolean nullable) {

		String sql = sqlAlterColumn(tableName, columnName);
		sql += nullable ? " DROP NOT NULL" : " SET NOT NULL";
		return sql;
	}

	@Override
	public boolean compatibleTypes(final String propertyType, final boolean limitedLength, final String columnType) {

		final String dataType = getDataType(propertyType, limitedLength);

		if (columnType.indexOf("SERIAL") > -1 && propertyType.equals("num")) {
			return true;
		}

		return columnType.equals(dataType);
	}

	@Override
	public String copyColumn(final String tableName, final String columnOrigin, final String columnDestiny) {

		return "UPDATE " + tableName + " SET " + columnDestiny + " = " + columnOrigin;
	}

	@Override
	public String createCheckConstraint(final String tableName, final String fieldName, final String[] options) {

		return "ALTER TABLE " + tableName + " ADD CONSTRAINT "
				+ getConstraintName(tableName, fieldName, CONSTRAINT_TYPE.CHECK) + " CHECK (" + fieldName
				+ " = ANY (ARRAY['" + StringUtils.arrayToDelimitedString(options, "','") + "']))";
	}

	@Override
	public String createColumn(final String tableName, final String columnName, final Map<String, String> field,
			final boolean ignoreNotNull) {

		String a = "ALTER TABLE " + tableName + " ADD COLUMN " + columnName + " " + getDataType(field);
		if (ignoreNotNull)
			return a;

		return a + " " + getColumnDefinition(field);
	}

	@Override
	public String createForeignKey(final String tableName, final String relationalTable, final String fieldName,
			String pkName) {
		return "ALTER TABLE " + relationalTable + " ADD  CONSTRAINT " + "FK_" + relationalTable + "_" + fieldName
				+ " FOREIGN KEY (" + fieldName + ") REFERENCES " + tableName;
	}

	@Override
	public String createIndex(final String tableName, final String constraintName, final String fieldName) {

		return "CREATE INDEX " + constraintName + " ON " + tableName + "(" + fieldName + ")";
	}

	@Override
	public String createPrimaryKey(final String tableName, final String fieldName) {
		return "ALTER TABLE " + tableName + " ADD CONSTRAINT " + "PK_" + tableName + " PRIMARY KEY (" + fieldName + ")";
	}

	@Override
	public String createPrimaryKey(final String tableName, final String[] fieldNames) {
		return createPrimaryKey(tableName, StringUtils.arrayToCommaDelimitedString(fieldNames));
	}

	@Override
	public String createSequence(final String tableName, final String pkName, final long initialValue) {
		return "CREATE SEQUENCE " + getSequenceName(tableName) + " INCREMENT 1 START " + initialValue + " CACHE 1 ";

	}

	@Override
	public String createTable(final String tableName, final Map<String, Map<String, String>> fields) {

		Map<String, String> field;
		final StringBuffer sql = new StringBuffer("CREATE TABLE " + tableName + " ( ");

		for (final Entry<String, Map<String, String>> entry : fields.entrySet()) {
			field = entry.getValue();

			sql.append(entry.getKey());
			sql.append(" ");
			sql.append(getDataType(field));
			sql.append(" ");
			sql.append(getColumnDefinition(field));
			sql.append(',');
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.append(" )");
		return sql.toString();
	}

	@Override
	public String createUser(final String user, final String pass) {

		return null;
	}

	@Override
	public String dropColumn(final String tableName, final String columnName) {
		return "ALTER TABLE " + tableName + " DROP COLUMN " + columnName;
	}

	@Override
	public String dropConstraint(final String tableName, final String constraintName) {

		return "ALTER TABLE " + tableName + " DROP CONSTRAINT " + constraintName;
	}

	@Override
	public String dropIndex(final String tableName, final String indexName) {
		return "DROP INDEX " + indexName;
	}

	@Override
	public String dropTable(final String tableName) {
		return "DROP TABLE " + tableName;
	}

	@Override
	public String fromDual() {
		return "";
	}

	@Override
	public String generatedKeyColumnName(final String primaryKey) {
		return primaryKey.toLowerCase();
	}

	@Override
	public String getAliasCampo() {
		return " as ";
	}

	@Override
	public String getAliasTabela(final boolean named) {
		if (named) {
			return " as usealias ";
		} else {
			return " as ";
		}
	}

	@Override
	public String getAllConstraints(final List<String> tableNames) {
		final StringBuffer sb = new StringBuffer();

		for (int i = 0; i < tableNames.size(); i++) {
			if (i != 0) {
				sb.append(',');
			}
			sb.append("'");
			sb.append(tableNames.get(i));
			sb.append("'");
		}

		return "SELECT UPPER(CONSTRAINT_NAME) AS CONSTRAINT_NAME, UPPER(TABLE_NAME) AS TABLE_NAME, UPPER(CONSTRAINT_TYPE) AS CONSTRAINT_TYPE "
				+ "FROM information_schema.table_constraints WHERE UPPER(TABLE_NAME) IN (" + sb.toString() + ")";
	}

	@Override
	public String getAllIndexes(final List<String> tableNames) {
		final StringBuffer sb = new StringBuffer();

		for (int i = 0; i < tableNames.size(); i++) {
			if (i != 0) {
				sb.append(',');
			}
			sb.append("'");
			sb.append(tableNames.get(i));
			sb.append("'");
		}

		return "SELECT UPPER(indexname) AS INDEX_NAME, UPPER(tablename) AS TABLE_NAME, null AS INDEX_TYPE FROM "
				+ "pg_catalog.pg_indexes WHERE UPPER(tablename) IN (" + sb.toString().toUpperCase() + ")";
	}

	@Override
	public String getAutoIncrementIdentifier() {
		return "SERIAL";
	}

	private String getColumnDefinition(final Map<String, String> field) {
		String definition = "";
		if (field.get("type") != null)
			if (field.get("type").equals("date") || field.get("type").equals("time")) {
				definition += " WITHOUT TIME ZONE";
			}

		if (isValueTrue(field, "required")) {
			definition += " NOT NULL";
		}
		return definition;
	}

	@Override
	public String getCompareInsensitive(final String campo, final boolean cote) {
		if (cote) {
			return "'" + campo + "'";
		}
		return campo;
	}

	@Override
	public String getConcat(final String str1, final String str2) {
		return " " + str1 + "||" + str2 + " ";
	}

	@Override
	public String getConstraintName(final String tableName, final String fieldName, final CONSTRAINT_TYPE type) {

		String pre;

		switch (type) {
		case CHECK:
			pre = "CK";
			break;
		case FOREIGNKEY_INDEX:
			pre = "XFK";
			break;
		default:
			pre = "X";
		}

		return pre + "_" + tableName + "_" + fieldName;
	}

	@Override
	public String getDataFromDate(final String data) {
		return null;
	}

	@Override
	public String getDataHoraFromDate(final String data) {
		return null;
	}

	private String getDataType(final Map<String, String> field) {

		if (isValueTrue(field, "primaryKey") && field.get("type").equals("num")) {
			return getAutoIncrementIdentifier();
		}

		return getDataType(field.get("type"), field.get("length") != null) + getLength(field);
	}

	@Override
	public String getDataType(final String propertyType, final boolean limitedLength) {

		if (propertyType == null || propertyType.equals("num")) {

			if (limitedLength) {
				return "NUMERIC";
			}
			return "INT8";
		} else if (propertyType.equals("alfa")) {
			return "VARCHAR";
		} else if (propertyType.equals("money")) {
			return "NUMERIC";
		} else if (propertyType.equals("date")) {
			return "TIMESTAMP";
		} else if (propertyType.equals("time")) {
			return "TIME";
		} else if (propertyType.equals("text")) {
			return "TEXT";
		} else if (propertyType.equals("bit")) {
			return "BPCHAR";
		} else if (propertyType.equals("file") || propertyType.equals("object")) {

			return "BYTEA";
		}
		return null;
	}

	public String getMetaType(final String propertyType, String decimal) {

		if (propertyType == null || (propertyType.equals("INT8") && decimal.equals("0"))) {
			return "num";
		} else if ((propertyType.equals("NUMERIC") && !decimal.equals("0"))) {
			return "money";
		} else if (propertyType.equals("VARCHAR")) {
			return "alfa";
		} else if (propertyType.equals("TIMESTAMP")) {
			return "date";
		} else if (propertyType.equals("BPCHAR")) {
			return "bit";
		} else if (propertyType.equals("BYTEA")) {

			return "file";
		}
		return null;
	}

	@Override
	public String getDefaultLength(final String propertyType) {

		if (propertyType.equals("alfa")) {
			return "100";
		} else if (propertyType.equals("money")) {
			return "16";
		}
		return null;
	}

	@Override
	public String getDiffDate(final String columns) {
		final String[] c = columns.split(",");
		return "EXTRACT (DAY FROM (" + c[0] + " - " + c[1] + "))";
	}

	@Override
	public String getHoraFromDate(final String data) {
		return "TO_CHAR(" + data + ",'HH24:MI')";
	}

	private String getLength(final Map<String, String> field) {
		String length = field.get("length");
		final String type = field.get("type");

		if (length == null) {

			if (type == null || type.equals("num")) {
				return "";
			}
			length = getDefaultLength(type);
		}

		if (type.equals("money")) {
			String precision = field.get("decimalPrecision");

			if (precision == null) {
				precision = "2";
			}
			return "(" + length + "," + precision + ")";

		} else if (type.equals("bit")) {
			return "(1)";
		}
		return length == null ? "" : "(" + length + ")";
	}

	@Override
	public String getNVL(final String column, final String value) {
		return "COALESCE(" + column + "," + value + ") ";
	}

	@Override
	public String getPaginacao(final Paginator p, final String[] sql, final String paginateBy) {
		return " " + sql[0] + " (" + sql[1] + ") LIMIT " + p.getLimit() + " OFFSET " + p.getStart() + " ";
	}

	public String getPesqObj1(final String mes1, final String ano1, final String mes2, final String ano2) {

		return "SELECT * FROM c_cvrd_prov_objeto WHERE mes='" + mes1 + "' AND ano='" + ano1
				+ "' AND (Id_processo_objeto,parcela) NOT IN "
				+ "(SELECT Id_processo_objeto, parcela FROM c_cvrd_prov_objeto WHERE mes='" + mes2 + "' AND ano='"
				+ ano2 + "' )";
	}

	@Override
	public String getSequenceName(final String tableName) {
		return util.shortenIdentifier("SQ_" + tableName);
	}

	@Override
	public String[] getSqlHierarquia(final String root, final String de, final String para, final String table,
			final Usuario user) {

		// Somente para postgre 4.3
		return new String[] { " WITH RECURSIVE        rows AS        " + "(        SELECT  *        FROM    (       "
				+ "         SELECT  *                FROM    MM_MEMBRO_MEMBRO         "
				+ "       WHERE   ID_UNIDADE_ORGANIZACIONAL in ( "
				+ String.valueOf(StringUtils.arrayToCommaDelimitedString(user.getUnidadesVisiveis()))
				+ "                            ) " + " ) q    "
				+ "   UNION ALL        SELECT  *        FROM    (                SELECT  c.*        "
				+ "        FROM    rows r                JOIN    MM_MEMBRO_MEMBRO c         "
				+ "       ON      c.ID_UNIDADE_ORGANIZACIONAL = r.ID_UNIDADE_FILHO             "
				+ "            ) q2        ) ", "SELECT  ID_UNIDADE_FILHO as ID FROM    rows" };
	}

	public String getSqlHierarquiaFilhos(final String root, final Usuario user) {
		return "select id_filho from mm_folha where   id_pai = " + root.toString() + " ";
	}

	public String getSqlHierarquiaPais(final String child, final Usuario user) {
		return "select id_pai from mm_folha where   id_filho = " + child.toString() + " ";
	}

	@Override
	public String getSysdate() {
		return " CURRENT_TIMESTAMP ";
	}

	@Override
	public String getToChar(final String data, final int type) {

		if (type == java.sql.Types.TIME) {
			return "TO_CHAR(" + data + ",'HH24:MI')";
		} else if (type == java.sql.Types.DATE) {
			return "TO_CHAR(" + data + ",'dd/MM/yyyy HH24:MI')";
		} else {
			return "TO_CHAR(" + data + ")";
		}
	}

	@Override
	public String getToDate(final String mascara, final String data) {
		String convert = "NULL";

		if (!data.equals("")) {
			convert = "TO_DATE('" + data + "','" + mascara + "')";
		}

		return convert;
	}

	public String getToDateField(final String mascara, final String data) {
		String convert = "NULL";

		if (!data.equals("")) {
			convert = "TO_DATE(" + data + ",'" + mascara + "')";
		}

		return convert;
	}

	@Override
	public String getTransactionID() {
		return "select txid_current()";
	}

	@Override
	public String getTrunc(final String data) {
		return "UPPER(" + data + ")";
	}

	@Override
	public String getTruncDate(final String data) {
		return " DATE_TRUNC('day'," + data + ") ";
	}

	@Override
	public boolean isCheckConstraintType(final String constraintType) {
		return constraintType.equals("CHECK");
	}

	@Override
	public boolean isGeneratedKeyBySequence() {
		return false;
	}

	@Override
	public boolean isPrimaryKeyConstraintType(final String constraintType) {
		return constraintType.equals("PRIMARY KEY");
	}

	private boolean isValueTrue(final Map<String, String> map, final String key) {
		if (map == null)
			return false;
		final String value = map.get(key);
		return value != null && value.equals(Constants.true_str);
	}

	@Override
	public String remaneColumn(final String tableName, final String columnName, final String newName) {

		return "ALTER TABLE " + tableName + " RENAME COLUMN " + columnName + " TO " + newName;
	}

	@Override
	public String renameTable(final String tableName, final String newtableName) {
		return "ALTER TABLE " + tableName + " RENAME TO " + newtableName;
	}

	@Override
	public String sqlAlterColumn(final String tableName, final String columnName) {

		return "ALTER TABLE " + tableName + " ALTER COLUMN " + columnName;
	}

	@Override
	public boolean suportaHierarquia(final Usuario user) {
		return false;
	}

	public boolean useAlias() {
		return true;
	}

	@Override
	public String getToNumber(String campo) {
		return "TO_NUMBER(" + campo + ",'999999999')";
	}

	@Override
	public boolean requireOrder() {
		return false;
	}

	@Override
	public String getAllSequences(List<String> tableNames) {
		return "SELECT  c.relname AS SEQUENCE_NAME FROM pg_class c WHERE (c.relkind = 'S')";
	}

	@Override
	public String dropSequence(String tableName, String indexName, final Usuario user) {
		DAOTemplate jt = null;
		String fname = user.getUtil().replace(indexName, new String[] { tableName + "_", "_seq" },
				new String[] { "", "" });
		if (fname.equals("iseq"))
			fname = "i" + tableName;
		try {
			jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user, null, user.getUtil());
		} catch (ConfigurationException e) {
			LogUtil.exception(e, user);
		} catch (SQLException e) {
			LogUtil.exception(e, user);
		}
		fname = String.valueOf(jt.queryForObject("select max(" + fname + " ) from " + tableName, Integer.class) + 1);

		return "ALTER SEQUENCE " + indexName + " RESTART WITH " + fname;
	}

	@Override
	public String subtractDaysFromDate(String string, String string2) {
		return " ( " + string + " - createinterval(" + string2 + ")) ";
		// return " " + string +" - INTERVAL "+string2+" DAY";
	}

	@Override
	public String getTruncInt(String dado) {
		return " TRUNC(" + dado + ") ";
	}

	@Override
	public String getConcatOperator() {
		return " || ";
	}

	@Override
	public String getSubStr(String c, String o, String f) {
		return "SUBSTR(" + c + "," + o + "," + f + ")";

	}

	@Override
	public String getSubStrName() {
		return "substring";
	}

	@Override
	public String getStringDatePart(String part, String field) {
		return "date_part('" + part + "', " + field + ")";
	}

	@Override
	public String getQuoteEscape() {
		return "''";
	}

}