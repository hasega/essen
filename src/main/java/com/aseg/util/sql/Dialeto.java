package com.aseg.util.sql;

import java.util.HashMap;
import java.util.Map;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.util.ConnectionManager;

public class Dialeto {

	private static Dialeto me = null;

	public static Dialeto getInstance() {

		if (me == null) {
			me = new Dialeto();
		}
		return me;
	}

	private final static Map<Object, InterfaceDialeto> dialetoCorrente = new HashMap<Object, InterfaceDialeto>() {
		public InterfaceDialeto put(Object key, InterfaceDialeto value) {
			return super.put(key, value);
		};
	};

	public InterfaceDialeto getDialeto(final Usuario user) {
		if (dialetoCorrente.get(user.getConnectionRoute()) == null) {
			load(user);
		}
		return dialetoCorrente.get(user.getConnectionRoute());
	}

	private void load(final Usuario user) {

		try {

			if (ConnectionManager.getInstance(user).getBancoConectado(user) == 0) {
				LogUtil.fatal("!!!ATENCAO!!!");
				LogUtil.fatal("Nao foi possivel carregar o tipo de banco de dados a se conectar. getBancoConectado=0");
				LogUtil.fatal("Verifique o config-sis.xml, e se o HOST que serve o banco de dados esta ACESSIVEL.");

			} else {

				try {

					if (ConnectionManager.getInstance(user).getBancoConectado(user) == ConnectionManager.ORACLE) {
						LogUtil.debug("Conectado ao banco Oracle", user);
						dialetoCorrente.put(user.getConnectionRoute(), dialetoOracle.getInstance());
					} else if (ConnectionManager.getInstance(user)
							.getBancoConectado(user) == ConnectionManager.SQLSERVER) {
						LogUtil.debug("Conectado ao banco Sql Server", user);
						dialetoCorrente.put(user.getConnectionRoute(), new dialetoSQLServer());
					} else if (ConnectionManager.getInstance(user)
							.getBancoConectado(user) == ConnectionManager.POSTGRESQL) {
						LogUtil.debug("Conectado ao banco Postgresql", user);
						dialetoCorrente.put(user.getConnectionRoute(), new dialetoPostgre());
					} else if (ConnectionManager.getInstance(user).getBancoConectado(user) == ConnectionManager.MYSQL) {
						LogUtil.debug("Conectado ao banco MySql", user);
						dialetoCorrente.put(user.getConnectionRoute(), new dialetoMySql());
					} else if (ConnectionManager.getInstance(user)
							.getBancoConectado(user) == ConnectionManager.FIREBIRD) {
						LogUtil.debug("Conectado ao banco Firebird", user);
						dialetoCorrente.put(user.getConnectionRoute(), new dialetoFireBird());
					}

				} catch (final Exception ex) {
					LogUtil.exception(ex, user);
				}

			}

		} catch (Exception e) {

		}

	}

	public static Map getDialetos() {
		// TODO Auto-generated method stub
		return dialetoCorrente;
	}
}