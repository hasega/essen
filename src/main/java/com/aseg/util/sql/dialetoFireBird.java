package com.aseg.util.sql;

import java.util.List;
import java.util.Map;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.Paginator;
import com.aseg.util.service.UtilServiceImpl;

public class dialetoFireBird implements InterfaceDialeto {
	// Standard paging query from http://asktom.oracle.com:
	//
	// select *
	// from ( select a.*, rownum rnum
	// from ( YOUR_QUERY_GOES_HERE -- including the order by ) a
	// where rownum <= MAX_ROWS )
	// where rnum >= MIN_ROWS
	private static dialetoFireBird me = null;
	public final UtilServiceImpl util = new UtilServiceImpl();

	public static dialetoFireBird getInstance() {
		if (me == null) {
			me = new dialetoFireBird();
		}

		return me;
	}

	dialetoFireBird() {
		LogUtil.debug("Dialeto de banco de dados FireBird utilizado            [OK]", null);
	}

	@Override
	public String alterColumn(final String tableName, final String columnName, final Map<String, String> field) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String alterColumn(final String tableName, final String columnName, final Map<String, String> field,
			final boolean nullable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean compatibleTypes(final String propertyType, final boolean limitedLength, final String columnType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String copyColumn(final String tableName, final String columnOrigin, final String columnDestiny) {

		return null;
	}

	@Override
	public String createCheckConstraint(final String tableName, final String fieldName, final String[] options) {

		return null;
	}

	@Override
	public String createColumn(final String tableName, final String columnName, final Map<String, String> field,
			final boolean ignoreNotNull) {

		return null;
	}

	@Override
	public String createForeignKey(final String tableName, final String relationalTable, final String field,
			String pkName) {

		return null;
	}

	@Override
	public String createIndex(final String tableName, final String constraintName, final String fieldName) {

		return null;
	}

	@Override
	public String createPrimaryKey(final String tableName, final String fieldName) {

		return null;
	}

	@Override
	public String createPrimaryKey(final String tableName, final String[] fieldNames) {

		return null;
	}

	@Override
	public String createSequence(final String tableName, final String pkName, final long initialValue) {

		return null;
	}

	@Override
	public String createTable(final String nome, final Map<String, Map<String, String>> Fields) {

		return null;
	}

	@Override
	public String createUser(final String user, final String pass) {

		return null;
	}

	@Override
	public String dropColumn(final String tableName, final String columnName) {

		return null;
	}

	@Override
	public String dropConstraint(final String tableName, final String constraintName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String dropIndex(final String tableName, final String constraintName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String dropTable(final String tableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String fromDual() {
		return "from RDB$DATABASE";
	}

	@Override
	public String generatedKeyColumnName(final String primaryKey) {

		return null;
	}

	@Override
	public String getAliasCampo() {

		return "";
	}

	@Override
	public String getAliasTabela(final boolean named) {

		return "";
	}

	@Override
	public String getAllConstraints(final List<String> tableNames) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAllIndexes(final List<String> tableNames) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAutoIncrementIdentifier() {

		return null;
	}

	@Override
	public String getCompareInsensitive(final String campo, final boolean cote) {

		if (cote) {
			return "'" + campo + "'";
		}
		return campo;
	}

	@Override
	public String getConcat(final String str1, final String str2) {
		return str1 + "||" + str2;
	}

	@Override
	public String getConstraintName(final String tableName, final String fieldName, final CONSTRAINT_TYPE type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDataFromDate(final String data) {

		return null;
	}

	@Override
	public String getDataHoraFromDate(final String data) {

		return null;
	}

	@Override
	public String getDataType(final String propertyType, final boolean limitedLength) {

		return null;
	}

	@Override
	public String getDefaultLength(final String propertyType) {

		return null;
	}

	@Override
	public String getDiffDate(final String columns) {

		final String[] c = columns.split(",");
		return "(" + c[0] + " - " + c[1] + ")";
	}

	@Override
	public String getHoraFromDate(final String data) {
		// LogUtil.debug("dialetoOracle >>TO_DATE('"+data+"','"+mascara+"')"
		// );
		return "CAST (" + data + " as TIME)";
	}

	@Override
	public String getNVL(final String column, final String value) {

		return "COALESCE(" + column + "," + value + ") ";
	}

	@Override
	public String getPaginacao(final Paginator p, final String[] sql, final String paginateBy) {
		return " ( select FIRST " + p.getLimit() + " SKIP " + p.getStart() + " * from " + sql + ")";
	}

	public String getPesqObj1(final String mes1, final String ano1, final String mes2, final String ano2) {

		return "select * from c_cvrd_prov_objeto where mes='" + mes1 + "' and ano='" + ano1
				+ "' and (Id_processo_objeto,parcela) not in "
				+ "(select Id_processo_objeto,parcela from c_cvrd_prov_objeto where mes='" + mes2 + "' and ano='" + ano2
				+ "' )";
	}

	@Override
	public String getSequenceName(final String tableName) {

		return null;
	}

	@Override
	public String[] getSqlHierarquia(final String root, final String de, final String para, final String table,
			final Usuario user) {

		return null;
	}

	public String getSqlHierarquiaFilhos(final String root, final Usuario user) {

		return "select id_filho from mm_folha where   id_pai = " + root.toString() + " ";
	}

	public String getSqlHierarquiaPais(final String child, final Usuario user) {

		return "select id_pai from mm_folha where   id_filho = " + child.toString() + " ";
	}

	@Override
	public String getSysdate() {
		return "CURRENT_DATE";
	}

	@Override
	public String getToChar(final String data, final int type) {

		return "CAST (" + data + " as VARCHAR)";
	}

	@Override
	public String getToDate(final String mascara, String data) {
		data = util.replace(data, "/", ".");
		// mascara = util.replace(mascara,"DD","%d");
		// mascara = util.replace(mascara,"MM","%m");
		// mascara = util.replace(mascara,"yyyy","%Y");
		// mascara = util.replace(mascara,"YYYY","%Y");
		// mascara = util.replace(mascara,"/mm/","/%m/");

		return "cast('" + data + "' as timestamp)";

		// LogUtil.debug("dialetoOracle >>TO_DATE('"+data+"','"+mascara+"')"
		// );
		// return "TO_DATE('"+data+"','"+mascara+"')";
	}

	public String getToDateField(final String mascara, final String data) {
		String convert = "NULL";

		if (!data.equals("")) {
			convert = "TO_DATE(" + data + ",'" + mascara + "')";
		}

		return convert;

		// LogUtil.debug("dialetoOracle >>TO_DATE('"+data+"','"+mascara+"')"
		// );
		// return "TO_DATE('"+data+"','"+mascara+"')";
	}

	@Override
	public String getTransactionID() {

		return null;
	}

	@Override
	public String getTrunc(final String data) {
		return " " + data + " ";
	}

	@Override
	public String getTruncDate(final String data) {
		return " cast(" + data + " as date) ";
	}

	@Override
	public boolean isCheckConstraintType(final String constraintType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGeneratedKeyBySequence() {

		return false;
	}

	@Override
	public boolean isPrimaryKeyConstraintType(final String constraintType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String remaneColumn(final String tableName, final String columnName, final String newName) {

		return null;
	}

	@Override
	public String renameTable(final String tableName, final String newtableName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean suportaHierarquia(final Usuario user) {

		return false;
	}

	public boolean useAlias() {
		return false;
	}

	@Override
	public String getToNumber(String campo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean requireOrder() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getAllSequences(List<String> tableNames) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String dropSequence(String tableName, String indexName, final Usuario user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String subtractDaysFromDate(String string, String string2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getMetaType(String propertyType, String decimal) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String sqlAlterColumn(String tableName, String columnName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTruncInt(String dado) {
		return " cast(" + dado + " as int) ";
	}

	@Override
	public String getConcatOperator() {
		// TODO Auto-generated method stub
		return " || ";
	}

	@Override
	public String getSubStr(String c, String o, String f) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSubStrName() {
		// TODO Auto-generated method stub
		return "substring";
	}

	@Override
	public String getStringDatePart(String part, String field) {
		if (part.equals("month")) {
			part = "Month";
		} else if (part.equals("year")) {
			part = "Year";
		} else if (part.equals("day")) {
			part = "Day";
		}
		return "Extract(" + part + " From " + field + ")";
	}

	@Override
	public String getQuoteEscape() {
		return "''";
	}

}