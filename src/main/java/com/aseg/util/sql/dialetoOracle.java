package com.aseg.util.sql;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.util.StringUtils;

import com.aseg.config.Constants;
import com.aseg.exceptions.PersistenceException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.Paginator;
import com.aseg.util.ConnectionManager;
import com.aseg.util.service.UtilServiceImpl;

public class dialetoOracle implements InterfaceDialeto {
	// Standard paging query from http://asktom.oracle.com:
	//
	// select *
	// from ( select a.*, rownum rnum
	// from ( YOUR_QUERY_GOES_HERE -- including the order by ) a
	// where rownum <= MAX_ROWS )
	// where rnum >= MIN_ROWS
	private static dialetoOracle me = null;
	public final UtilServiceImpl util = new UtilServiceImpl();

	public static dialetoOracle getInstance() {
		if (me == null) {
			me = new dialetoOracle();
		}

		return me;
	}

	private dialetoOracle() {
		LogUtil.debug("Dialeto de banco de dados Oracle utilizado            [OK]", null);
	}

	@Override
	public String alterColumn(final String tableName, final String columnName, final Map<String, String> field) {

		String sql = sqlAlterColumn(tableName, columnName);
		sql += " " + getDataType(field) + " " + getColumnDefinition(field);
		return sql;
	}

	@Override
	public String alterColumn(final String tableName, final String columnName, final Map<String, String> field,
			final boolean nullable) {

		String sql = sqlAlterColumn(tableName, columnName);
		sql += nullable ? " NULL" : " NOT NULL";
		return sql;
	}

	@Override
	public boolean compatibleTypes(final String propertyType, final boolean limitedLength, final String columnType) {

		final String dataType = getDataType(propertyType, limitedLength);

		return columnType.equals(dataType);
	}

	@Override
	public String copyColumn(final String tableName, final String columnOrigin, final String columnDestiny) {

		return "UPDATE " + tableName + " SET " + columnDestiny + " = " + columnOrigin;
	}

	@Override
	public String createCheckConstraint(final String tableName, final String fieldName, final String[] options) {
		return "ALTER TABLE " + tableName + " ADD ( CONSTRAINT "
				+ getConstraintName(tableName, fieldName, CONSTRAINT_TYPE.CHECK) + " CHECK (" + fieldName + " IN ('"
				+ StringUtils.arrayToDelimitedString(options, "','") + "')))";
	}

	@Override
	public String createColumn(final String tableName, final String columnName, final Map<String, String> field,
			final boolean ignoreNotNull) {

		final StringBuffer sql = new StringBuffer();

		sql.append("ALTER TABLE ");
		sql.append(tableName);
		sql.append(" ADD ( ");
		sql.append(columnName);
		sql.append(" ");
		sql.append(getDataType(field));

		if (!ignoreNotNull) {
			sql.append(" ");
			sql.append(getColumnDefinition(field));
		}
		sql.append(")");

		return sql.toString();
	}

	@Override
	public String createForeignKey(final String tableName, final String relationalTable, final String fieldName,
			String pkName) {
		return "ALTER TABLE " + relationalTable + " ADD ( CONSTRAINT "
				+ util.shortenIdentifier("FK_" + relationalTable + "_" + fieldName) + " FOREIGN KEY (" + fieldName
				+ ") REFERENCES " + tableName + ")";
	}

	@Override
	public String createIndex(final String tableName, final String constraintName, final String fieldName) {

		return "CREATE INDEX " + constraintName + " ON " + tableName + "(" + fieldName + ")";
	}

	@Override
	public String createPrimaryKey(final String tableName, final String fieldName) {
		return "ALTER TABLE " + tableName + " ADD ( CONSTRAINT " + util.shortenIdentifier("PK_" + tableName)
				+ " PRIMARY KEY (" + fieldName + ") USING INDEX)";
	}

	@Override
	public String createPrimaryKey(final String tableName, final String[] fieldNames) {
		return createPrimaryKey(tableName, StringUtils.arrayToCommaDelimitedString(fieldNames));
	}

	@Override
	public String createSequence(final String tableName, final String pkName, final long initialValue) {
		return "CREATE SEQUENCE " + getSequenceName(tableName) + " MINVALUE " + initialValue;

	}

	@Override
	public String createTable(final String tableName, final Map<String, Map<String, String>> fields) {

		Map<String, String> field;
		final StringBuffer sql = new StringBuffer("CREATE TABLE " + tableName + " ( ");

		for (final Entry<String, Map<String, String>> entry : fields.entrySet()) {
			field = entry.getValue();

			sql.append(entry.getKey());
			sql.append(" ");
			sql.append(getDataType(field));
			sql.append(" ");
			sql.append(getColumnDefinition(field));
			sql.append(',');
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.append(" )");
		return sql.toString();
	}

	@Override
	public String createUser(final String user, final String pass) {

		return "CREATE USER \"" + user + "\" IDENTIFIED BY \"" + pass + "\"" + "DEFAULT TABLESPACE \"USERS\""
				+ "TEMPORARY TABLESPACE \"TEMP\"" + "PROFILE \"DEFAULT\";" + "GRANT CREATE SEQUENCE TO " + user + ";"
				+ "GRANT CREATE SESSION TO " + user + ";" + "GRANT CREATE TABLE TO " + user + ";" + "ALTER USER " + user
				+ "	QUOTA UNLIMITED ON \"USERS\" ";

	}

	@Override
	public String dropColumn(final String tableName, final String columnName) {
		return "ALTER TABLE " + tableName + " DROP COLUMN " + columnName;
	}

	@Override
	public String dropConstraint(final String tableName, final String constraintName) {

		return "ALTER TABLE " + tableName + " DROP CONSTRAINT " + constraintName;
	}

	@Override
	public String dropIndex(final String tableName, final String indexName) {
		return "DROP INDEX " + indexName;

	}

	@Override
	public String dropTable(final String tableName) {
		return "DROP TABLE " + tableName;
	}

	@Override
	public String fromDual() {
		return " from dual ";
	}

	@Override
	public String generatedKeyColumnName(final String primaryKey) {
		return primaryKey;
	}

	@Override
	public String getAliasCampo() {
		return " ";
	}

	@Override
	public String getAliasTabela(final boolean named) {
		return " ";
	}

	@Override
	public String getAllConstraints(final List<String> tableNames) {

		final StringBuffer sb = new StringBuffer();

		for (int i = 0; i < tableNames.size(); i++) {
			if (i != 0) {
				sb.append(',');
			}
			sb.append("'");
			sb.append(tableNames.get(i));
			sb.append("'");
		}

		return "SELECT CONSTRAINT_NAME, TABLE_NAME, CONSTRAINT_TYPE from user_constraints WHERE TABLE_NAME IN ("
				+ sb.toString() + ")";
	}

	@Override
	public String getAllIndexes(final List<String> tableNames) {

		final StringBuffer sb = new StringBuffer();

		for (int i = 0; i < tableNames.size(); i++) {
			if (i != 0) {
				sb.append(',');
			}
			sb.append("'");
			sb.append(tableNames.get(i));
			sb.append("'");
		}

		return "SELECT INDEX_NAME, TABLE_NAME, INDEX_TYPE from user_indexes WHERE TABLE_NAME IN (" + sb.toString()
				+ ") AND INDEX_TYPE <> 'LOB' ";
	}

	@Override
	public String getAutoIncrementIdentifier() {
		throw new PersistenceException("Esse BD usa 'sequences' para gerar id automaticamente.");
	}

	private String getColumnDefinition(final Map<String, String> field) {

		if (isValueTrue(field, "required")) {
			return " NOT NULL";
		}
		return "";
	}

	@Override
	public String getCompareInsensitive(final String campo, final boolean cote) {
		if (cote) {
			return "'" + campo + "'";
		}
		return campo;
	}

	@Override
	public String getConcat(final String str1, final String str2) {
		return "CONCAT(" + str1 + "," + str2 + ")";
	}

	@Override
	public String getConstraintName(final String tableName, final String fieldName, final CONSTRAINT_TYPE type) {

		String pre;

		switch (type) {
		case CHECK:
			pre = "CK";
			break;
		case FOREIGNKEY_INDEX:
			pre = "XFK";
			break;
		default:
			pre = "X";
		}

		return util.shortenIdentifier(pre + "_" + tableName + "_" + fieldName);
	}

	@Override
	public String getDataFromDate(final String data) {
		return "TO_CHAR(" + data + ",'dd/mm/yyyy')";
	}

	@Override
	public String getDataHoraFromDate(final String data) {
		return "TO_CHAR(" + data + ",'dd/mm/yyyy hh24:mi:ss')";
	}

	private String getDataType(final Map<String, String> field) {
		return getDataType(field.get("type"), field.get("length") != null) + getLength(field);
	}

	@Override
	public String getDataType(final String propertyType, final boolean limitedLength) {

		if (propertyType == null || (propertyType.equals("num") || propertyType.equals("money"))) {
			return "NUMBER";
		} else if (propertyType.equals("alfa")) {
			return "VARCHAR2";
		} else if (propertyType.equals("date") || propertyType.equals("time")) {
			return "DATE";
		} else if (propertyType.equals("bit")) {
			return "CHAR";

		} else if (propertyType.equals("text")) {
			return "CLOB";
		} else if (propertyType.equals("file") || propertyType.equals("object")) {

			return "BLOB";
		}
		return null;
	}

	public String getMetaType(final String propertyType, String decimal) {
		if (decimal == null)
			decimal = "0";

		if (propertyType == null || (propertyType.equals("NUMBER") && decimal.equals("0"))) {
			return "num";
		} else if (propertyType.equals("NUMBER") && !decimal.equals("0")) {
			return "money";
		} else if (propertyType.equals("VARCHAR2")) {
			return "alfa";
		} else if (propertyType.equals("DATE")) {
			return "date";
		} else if (propertyType.equals("CHAR")) {
			return "bit";
		} else if (propertyType.equals("BLOB")) {

			return "file";
		}
		return null;
	}

	@Override
	public String getDefaultLength(final String propertyType) {

		if (propertyType == null || propertyType.equals("num") || propertyType.equals("array")) {
			return "9";
		} else if (propertyType.equals("alfa")) {
			return "100";
		} else if (propertyType.equals("money")) {
			return "16";
		}
		return null;
	}

	@Override
	public String getDiffDate(final String columns) {
		final String[] c = columns.split(",");
		return " TRUNC(" + c[0] + " - " + c[1] + ") "; // diferenca em dias
	}

	@Override
	public String getHoraFromDate(final String data) {
		// LogUtil.debug("dialetoOracle >>TO_DATE('"+data+"','"+mascara+"')"
		// );
		return "TO_CHAR(" + data + ",'HH24:MI:SS')";
	}

	private String getLength(final Map<String, String> field) {
		String length = field.get("length");

		if (length == null) {
			length = getDefaultLength(field.get("type"));
		}

		if (field.get("type") == null || field.get("type").equals("money")) {
			String precision = field.get("decimalPrecision");

			if (precision == null) {
				precision = "2";
			}
			return "(" + length + "," + precision + ")";
		}
		return length == null ? "" : "(" + length + ")";
	}

	@Override
	public String getNVL(final String column, final String value) {
		return "NVL(" + column + "," + value + ") ";
	}

	@Override
	public String getPaginacao(final Paginator p, final String[] sql, String paginateBy) {
		if (paginateBy == null || paginateBy.trim().length() == 0) {
			paginateBy = "rownum";
		}

		return "select * from (select q.*,  " + paginateBy + " rnum from " + "(" + sql[1] + ") "

				+ " q ) where rnum between " + p.getStart() + " and "
				+ (Integer.parseInt(p.getStart()) + Integer.parseInt(p.getLimit()));

	}

	@Override
	public String getSequenceName(final String tableName) {
		return util.shortenIdentifier("SQ_" + tableName);
	}

	@Override
	public String[] getSqlHierarquia(final String root, final String de, final String para, final String table,
			final Usuario user) {
		try {
			if (ConnectionManager.getInstance(user).getVersion(user) >= 10) {
				return new String[] { "",
						" select    distinct(" + table + "." + para + ") from  " + table + " start with " + table + "."
								+ de + " = " + root.toString() + " connect by nocycle  prior " + table + "." + para
								+ " = " + table + "." + de };
			} else {
				// return "select id_filho from mm_folha where id_pai = "
				// + root.toString() + " ";
				throw new RuntimeException("ORACLE SUPORTADO R 10 ou superior");
			}
		} catch (final SQLException e) {
			LogUtil.exception(e, user);
		} catch (final Exception e) {
			LogUtil.exception(e, user);
		}
		return new String[] { null, "" };
	}

	@Override
	public String getSysdate() {
		return "SYSDATE";
	}

	@Override
	public String getToChar(final String data, final int type) {

		if (type == java.sql.Types.DATE) {
			return "TO_CHAR(" + data + ",'dd/MM/yyyy HH24:MI')";
		} else {
			return "TO_CHAR(" + data + ")";
		}
	}

	@Override
	public String getToDate(final String mascara, final String data) {
		String convert = "NULL";

		if (!data.equals("")) {
			convert = "TO_DATE('" + data + "','" + mascara + "')";
		}

		return convert;

		// LogUtil.debug("dialetoOracle >>TO_DATE('"+data+"','"+mascara+"')"
		// );
		// return "TO_DATE('"+data+"','"+mascara+"')";
	}

	public String getToDateField(final String mascara, final String data) {
		String convert = "NULL";

		if (!data.equals("")) {
			convert = "TO_DATE(" + data + ",'" + mascara + "')";
		}

		return convert;

		// LogUtil.debug("dialetoOracle >>TO_DATE('"+data+"','"+mascara+"')"
		// );
		// return "TO_DATE('"+data+"','"+mascara+"')";
	}

	@Override
	public String getTransactionID() {
		return "SELECT dbms_transaction.local_transaction_id FROM dual";
	}

	@Override
	public String getTrunc(final String data) {
		return " UPPER(dbms_lob.substr(" + data + ",dbms_lob.getlength(" + data + "),1)) ";
	}

	@Override
	public String getTruncDate(final String data) {
		return " TRUNC(" + data + ") ";
	}

	@Override
	public boolean isCheckConstraintType(final String constraintType) {
		return constraintType.equals("C");
	}

	@Override
	public boolean isGeneratedKeyBySequence() {
		return true;
	}

	@Override
	public boolean isPrimaryKeyConstraintType(final String constraintType) {
		return constraintType.equals("P");
	}

	private boolean isValueTrue(final Map<String, String> map, final String key) {
		final String value = map.get(key);
		return value != null && value.equals(Constants.true_str);
	}

	@Override
	public String remaneColumn(final String tableName, final String columnName, final String newName) {
		return "ALTER TABLE " + tableName + " RENAME COLUMN " + columnName + " TO " + newName;
	}

	@Override
	public String renameTable(final String tableName, final String newtableName) {
		return "ALTER TABLE " + tableName + " RENAME TO " + newtableName;
	}

	/**
	 * Abrevia o identificador (oracle aceita ate 30 caracteres).<br>
	 * Abrevia cada palavra (para 3 caracteres) ate ficar no tamanho adequado
	 * (da ultima para a primeira).<br>
	 * Ex: PK_ID_TABELA_COLUNA_NOME_MUITO_GRANDE =><br>
	 * PK_ID_TABELA_COLUNA_NOME_MUI_GRA
	 * 
	 * @param identifier
	 * @return
	 */

	public String sqlAlterColumn(final String tableName, final String columnName) {
		return "ALTER TABLE " + tableName + " MODIFY " + columnName;
	}

	@Override
	public boolean suportaHierarquia(final Usuario user) {
		try {
			if (ConnectionManager.getInstance(user).getVersion(user) >= 10) {
				return true;
			} else {
				return false;
			}
		} catch (final SQLException e) {
			LogUtil.exception(e, user);
		} catch (final Exception e) {
			LogUtil.exception(e, user);
		}
		return false;
	}

	public boolean useAlias() {
		return false;
	}

	@Override
	public String getToNumber(String campo) {
		// TODO Auto-generated method stub
		return "TO_NUMBER(" + campo + ")";
	}

	@Override
	public boolean requireOrder() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getAllSequences(List<String> tableNames) {
		final StringBuffer sb = new StringBuffer();

		return "SELECT SEQUENCE_NAME from user_sequences  ";
	}

	@Override
	public String dropSequence(String tableName, String indexName, final Usuario user) {
		// TODO Auto-generated method stub
		return "DROP SEQUENCE " + indexName;
	}

	@Override
	public String subtractDaysFromDate(String string, String days) {
		// TODO Auto-generated method stub
		return string + " - " + days;
	}

	@Override
	public String getTruncInt(String dado) {
		// TODO Auto-generated method stub
		return (this.getTruncDate(dado));
	}

	@Override
	public String getConcatOperator() {
		// TODO Auto-generated method stub
		return " || ";
	}

	@Override
	public String getSubStr(String c, String o, String f) {
		// TODO Auto-generated method stub
		return "SUBSTR(" + c + "," + o + "," + f + ")";

	}

	@Override
	public String getSubStrName() {
		// TODO Auto-generated method stub
		return "substr";
	}

	@Override
	public String getStringDatePart(String part, String field) {
		if (part.equals("month")) {
			part = "MM";
		} else if (part.equals("year")) {
			part = "YYYY";
		} else if (part.equals("day")) {
			part = "DD";
		}
		return "to_char(" + field + ",'" + part + "')";
	}

	@Override
	public String getQuoteEscape() {
		return "''";
	}

}