package com.aseg.util.sql;

import java.util.List;
import java.util.Map;

import com.aseg.seguranca.Usuario;
import com.aseg.service.Paginator;

public interface InterfaceDialeto {

	public enum CONSTRAINT_TYPE {
		CHECK, FOREIGNKEY_INDEX, INDEX
	};

	public String alterColumn(String tableName, String columnName, Map<String, String> field);

	public String alterColumn(String tableName, String columnName, Map<String, String> field, boolean nullable);

	public boolean compatibleTypes(String propertyType, boolean limitedLength, String columnType);

	public String copyColumn(String tableName, String columnOrigin, String columnDestiny);

	public String createCheckConstraint(String tableName, String fieldName, String[] options);

	public String createColumn(String tableName, String columnName, Map<String, String> field, boolean ignoreNotNull);

	public String createForeignKey(String tableName, String relationalTable, String fieldName, String pkName);

	public String createIndex(String tableName, String constraintName, String fieldName);

	public String createPrimaryKey(String tableName, String fieldName);

	public String createPrimaryKey(String tableName, String[] fieldNames);

	public String createSequence(String tableName, String pkName, long initialValue);

	public String createTable(String tableName, Map<String, Map<String, String>> fields);

	public String createUser(String user, String pass);

	public String dropColumn(String tableName, String columnName);

	public String dropConstraint(String tableName, String constraintName);

	public String dropIndex(String tableName, String constraintName);

	public String dropTable(String tableName);

	public String fromDual();

	public String generatedKeyColumnName(String primaryKey);

	public String getAliasCampo();

	public String getConcatOperator();

	public String getAliasTabela(boolean named);

	public String getAllConstraints(List<String> tableNames);

	public String getAllIndexes(List<String> tableNames);

	public String getAutoIncrementIdentifier();

	public String getCompareInsensitive(String campo, boolean cote);

	public String getConcat(String str1, String str2);

	public String getConstraintName(final String tableName, final String fieldName, final CONSTRAINT_TYPE type);

	public String getDataFromDate(String data);

	public String getDataHoraFromDate(String data);

	public String getDataType(String propertyType, boolean limitedLength);

	public String getDefaultLength(String propertyType);

	public String getDiffDate(String columns);

	public String getHoraFromDate(String data);

	public String getNVL(String column, String value);

	public String getPaginacao(Paginator p, String[] sql, String paginateBy);

	public String getSequenceName(String tableName);

	public String[] getSqlHierarquia(String root, String de, String para, String table, Usuario user);

	public String getSysdate();

	public String getToChar(String string, int FromType);

	public String getToDate(String mascara, String data);

	public String getTransactionID();

	public String getTrunc(String data);

	public String getTruncDate(String data);

	public boolean isCheckConstraintType(String constraintType);

	public boolean isGeneratedKeyBySequence();

	public boolean isPrimaryKeyConstraintType(String constraintType);

	public String remaneColumn(String tableName, String columnName, String newName);

	public String renameTable(final String tableName, final String newTableName);

	public boolean suportaHierarquia(Usuario user);

	public String getToNumber(String campo);

	public boolean requireOrder();

	public String getAllSequences(List<String> tableNames);

	public String dropSequence(String tableName, String indexName, Usuario user);

	public String getToDateField(String string, String string2);

	public String getSubStr(String c, String o, String f);

	public String getSubStrName();

	public String subtractDaysFromDate(String string, String string2);

	public String getMetaType(final String propertyType, String decimal);

	public String sqlAlterColumn(String tableName, String columnName);

	public String getTruncInt(final String dado);

	public String getStringDatePart(String part, String field);

	public String getQuoteEscape();

}
