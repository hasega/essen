package com.aseg.util.sql;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.util.StringUtils;

import com.aseg.config.Constants;
import com.aseg.exceptions.PersistenceException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.Paginator;
import com.aseg.util.service.UtilServiceImpl;

public class dialetoSQLServer implements InterfaceDialeto {

	public dialetoSQLServer() {
		LogUtil.debug("Dialeto de banco de dados SQL Server utilizado            [OK]", null);
	}

	@Override
	public String alterColumn(final String tableName, final String columnName, final Map<String, String> field) {

		String sql = sqlAlterColumn(tableName, columnName);
		sql += " " + getDataType(field) + " " + getColumnDefinition(field, tableName.startsWith("MM_"));
		return sql;
	}

	@Override
	public String alterColumn(final String tableName, final String columnName, final Map<String, String> field,
			final boolean nullable) {

		String sql = sqlAlterColumn(tableName, columnName);
		sql += " " + getDataType(field);
		sql += nullable ? " NULL" : " NOT NULL";
		return sql;
	}

	@Override
	public boolean compatibleTypes(final String propertyType, final boolean limitedLength, final String columnType) {

		final String dataType = getDataType(propertyType, limitedLength);

		// Usando "startWith" porque o SQLServer retorna "INT IDENTITY"
		return columnType.startsWith(dataType);
	}

	@Override
	public String copyColumn(final String tableName, final String columnOrigin, final String columnDestiny) {

		return "UPDATE " + tableName + " SET " + columnDestiny + " = " + columnOrigin;
	}

	@Override
	public String createCheckConstraint(final String tableName, final String fieldName, final String[] options) {
		return "ALTER TABLE " + tableName + " ADD CONSTRAINT "
				+ getConstraintName(tableName, fieldName, CONSTRAINT_TYPE.CHECK) + " CHECK (" + fieldName + " IN ('"
				+ StringUtils.arrayToDelimitedString(options, "','") + "'))";
	}

	@Override
	public String createColumn(final String tableName, final String columnName, final Map<String, String> field,
			final boolean ignoreNotNull) {

		final StringBuffer sql = new StringBuffer();

		sql.append("ALTER TABLE ");
		sql.append(tableName);
		sql.append(" ADD ");
		sql.append(columnName);
		sql.append(" ");
		sql.append(getDataType(field));

		if (!ignoreNotNull) {
			sql.append(" ");
			sql.append(getColumnDefinition(field, tableName.startsWith("MM_")));
		}

		return sql.toString();
	}

	@Override
	public String createForeignKey(final String tableName, final String relationalTable, final String fieldName,
			final String pkName) {

		return "ALTER TABLE " + relationalTable + " ADD CONSTRAINT " + "FK_" + relationalTable + "_" + fieldName
				+ " FOREIGN KEY (" + fieldName + ") REFERENCES " + tableName + "(" + pkName + ")";
	}

	@Override
	public String createIndex(final String tableName, final String constraintName, final String fieldName) {

		return "CREATE INDEX " + constraintName + " ON " + tableName + "(" + fieldName + ")";
	}

	@Override
	public String createPrimaryKey(final String tableName, final String fieldName) {
		return "ALTER TABLE " + tableName + " ADD CONSTRAINT " + "PK_" + tableName + " PRIMARY KEY (" + fieldName + ")";
	}

	@Override
	public String createPrimaryKey(final String tableName, final String[] fieldNames) {
		return createPrimaryKey(tableName, StringUtils.arrayToCommaDelimitedString(fieldNames));
	}

	@Override
	public String createSequence(final String tableName, final String pkName, final long initialValue) {

		return "ALTER TABLE " + tableName + " ALTER COLUMN " + pkName + " IDENTITY ";
	}

	@Override
	public String createTable(final String tableName, final Map<String, Map<String, String>> fields) {

		Map<String, String> field;
		final StringBuffer sql = new StringBuffer("CREATE TABLE " + tableName + " ( ");

		for (final Entry<String, Map<String, String>> entry : fields.entrySet()) {
			field = entry.getValue();

			sql.append(entry.getKey());
			sql.append(" ");
			sql.append(getDataType(field));
			sql.append(" ");
			sql.append(getColumnDefinition(field, tableName.startsWith("MM_")));
			sql.append(',');
		}
		sql.deleteCharAt(sql.length() - 1);
		sql.append(" )");
		return sql.toString();
	}

	@Override
	public String createUser(final String user, final String pass) {

		return null;
	}

	@Override
	public String dropColumn(final String tableName, final String columnName) {
		return "ALTER TABLE " + tableName + " DROP COLUMN " + columnName;
	}

	@Override
	public String dropConstraint(final String tableName, final String constraintName) {

		return "ALTER TABLE " + tableName + " DROP CONSTRAINT " + constraintName;
	}

	@Override
	public String dropIndex(final String tableName, final String constraintName) {
		// TODO Auto-generated method stub
		return "DROP INDEX " + tableName + "." + constraintName;
	}

	@Override
	public String dropTable(final String tableName) {
		return "DROP TABLE " + tableName;
	}

	@Override
	public String fromDual() {
		return "";
	}

	@Override
	public String generatedKeyColumnName(final String primaryKey) {
		return "ID";
	}

	@Override
	public String getAliasCampo() {
		return " as ";
	}

	@Override
	public String getAliasTabela(final boolean named) {
		if (named) {
			return " as usealias ";
		} else {
			return " as ";
		}
	}

	@Override
	public String getAllConstraints(final List<String> tableNames) {

		final StringBuffer sb = new StringBuffer();

		sb.append("SELECT OBJECT_NAME(OBJECT_ID) AS CONSTRAINT_NAME, ");
		sb.append("OBJECT_NAME(PARENT_OBJECT_ID) AS TABLE_NAME, ");
		sb.append("TYPE_DESC AS CONSTRAINT_TYPE ");
		sb.append("FROM SYS.OBJECTS ");
		sb.append("WHERE TYPE_DESC LIKE '%CONSTRAINT' ");
		sb.append("AND OBJECT_NAME(parent_object_id) IN (");

		for (int i = 0; i < tableNames.size(); i++) {
			if (i != 0) {
				sb.append(',');
			}
			sb.append("'");
			sb.append(tableNames.get(i));
			sb.append("'");
		}
		sb.append(")");

		return sb.toString();
	}

	@Override
	public String getAllIndexes(final List<String> tableNames) {
		final StringBuffer sb = new StringBuffer();

		sb.append("SELECT distinct(I.NAME)  AS INDEX_NAME, OBJECT_NAME(I.OBJECT_ID) AS TABLE_NAME FROM  ");
		// sb.append("FROM SYS.DM_DB_INDEX_OPERATIONAL_STATS(NULL, NULL, NULL,
		// NULL) A ");
		sb.append(
				// "LEFT JOIN " +
				"SYS.INDEXES AS I "
		// +"ON I.OBJECT_ID = A.OBJECT_ID "
		);
		// sb.append("AND I.INDEX_ID = A.INDEX_ID ");
		sb.append("WHERE "
		// + " OBJECTPROPERTY(A.[OBJECT_ID],'IsUserTable') = 1 AND "
		);
		sb.append("I.NAME IS NOT NULL AND ");
		sb.append("OBJECT_NAME(I.OBJECT_ID) IN (");

		for (int i = 0; i < tableNames.size(); i++) {
			if (i != 0) {
				sb.append(',');
			}
			sb.append("'");
			sb.append(tableNames.get(i));
			sb.append("'");
		}

		sb.append(")");
		// System.out.println(sb.toString());
		return sb.toString();
	}

	@Override
	public String getAutoIncrementIdentifier() {
		return "IDENTITY";
	}

	private String getColumnDefinition(final Map<String, String> field, boolean ismm) {

		String a = "";
		if (isValueTrue(field, "required")) {
			a += " NOT NULL";
		}
		if (isValueTrue(field, "primaryKey") && !ismm)
			a += " " + getAutoIncrementIdentifier();
		return a;
	}

	@Override
	public String getCompareInsensitive(final String campo, final boolean cote) {

		if (cote) {
			return "  BINARY_CHECKSUM('" + campo + "') ";
		}
		return "  BINARY_CHECKSUM(" + campo + ") ";
	}

	@Override
	public String getConcat(final String str1, final String str2) {
		return "CAST (ISNULL(" + str1 + ",'') AS varchar(4000)) + CAST(ISNULL(" + str2 + ",'') AS varchar(4000))";
	}

	@Override
	public String getConstraintName(final String tableName, final String fieldName, final CONSTRAINT_TYPE type) {

		String pre;

		switch (type) {
		case CHECK:
			pre = "CK";
			break;
		case FOREIGNKEY_INDEX:
			pre = "XFK";
			break;
		default:
			pre = "X";
		}

		return pre + "_" + tableName + "_" + fieldName;
	}

	// Retorna data no formato DD/MM/YYYY
	@Override
	public String getDataFromDate(final String data) {
		String convert = "NULL";
		if (!data.equals("")) {
			convert = "convert (varchar," + data + ",103)";
		}

		return convert;
	}

	// Retorna data no formato DD/MM/YYYY HH24:MI:SS
	@Override
	public String getDataHoraFromDate(final String data) {
		String convert = "NULL";
		if (!data.equals("")) {
			convert = "convert (varchar," + data + ",103) + ' ' + convert (varchar," + data + ",108)";
		}

		return convert;
	}

	private String getDataType(final Map<String, String> field) {
		return getDataType(field.get("type"), field.get("length") != null) + getLength(field);
	}

	@Override
	public String getDataType(final String propertyType, final boolean limitedLength) {

		if (propertyType == null || propertyType.equals("num")) {
			if (limitedLength) {
				return "NUMERIC";
			}
			return "INT";
		} else if (propertyType.equals("alfa")) {
			return "VARCHAR";
		} else if (propertyType.equals("date") || propertyType.equals("time")) {
			return "DATETIME";
		} else if (propertyType.equals("bit")) {
			return "CHAR";
		} else if (propertyType.equals("money")) {
			return "NUMERIC";
		} else if (propertyType.equals("text")) {
			return "TEXT";
		} else if (propertyType.equals("file") || propertyType.equals("object")) {
			return "IMAGE";
		}
		return null;
	}

	public String getMetaType(final String propertyType, String decimal) {

		if (propertyType == null || (propertyType.equals("INT") && decimal.equals("0"))) {
			return "num";
		} else if ((propertyType.equals("NUMERIC") && !decimal.equals("0"))) {
			return "money";
		} else if (propertyType.equals("VARCHAR")) {
			return "alfa";
		} else if (propertyType.equals("DATETIME")) {
			return "date";
		} else if (propertyType.equals("CHAR")) {
			return "bit";
		} else if (propertyType.equals("IMAGE")) {

			return "file";
		}
		return null;
	}

	@Override
	public String getDefaultLength(final String propertyType) {
		if (propertyType == null)
			return null;
		if (propertyType.equals("alfa")) {
			return "100";
		} else if (propertyType.equals("money")) {
			return "16";
		}
		return null;
	}

	@Override
	public String getDiffDate(final String columns) {
		final String[] c = columns.split(",");
		return "datediff (day," + c[1] + " , " + c[0] + ")";
	}

	@Override
	public String getHoraFromDate(final String data) {
		String convert = "NULL";

		if (!data.equals("")) {
			convert = "convert (varchar," + data + ",108)";
		}

		return convert;

	}

	private String getLength(final Map<String, String> field) {
		String length = field.get("length");

		if (length == null) {
			length = getDefaultLength(field.get("type"));
		}

		if (field.get("type") == null || field.get("type").equals("money")) {
			String precision = field.get("decimalPrecision");

			if (precision == null) {
				precision = "2";
			}
			return length == null ? "" : "(" + length + "," + precision + ")";
		}
		return length == null ? "" : "(" + length + ")";
	}

	@Override
	public String getNVL(final String column, final String value) {
		return "ISNULL(" + column + "," + value + ") ";
	}

	@Override
	public String getPaginacao(final Paginator p, final String[] sql, final String paginateBy) {

		String subSQL = sql[1];
		UtilServiceImpl util = new UtilServiceImpl();

		String orderBy = subSQL.split(",")[0].split("as")[0].replaceAll("SELECT", "");
		String[] subSqlFields = null;
		if (subSQL.lastIndexOf("order by") > 0) {
			subSQL = sql[1].trim().replaceAll("^\\(", "").replaceAll("\\)$", "");

			subSqlFields = subSQL.replaceFirst("SELECT", "").trim().split("FROM")[0].split(",");

			orderBy = subSQL.substring(subSQL.lastIndexOf("order by") + 8);
			subSQL = subSQL.substring(0, subSQL.lastIndexOf("order by"));
			if (subSQL.indexOf("as  "
					+ util.replace(orderBy, new String[] { "ASC", "DESC" }, new String[] { "", "" }).trim()) > -1) {
				String[] b = subSQL.split(
						"as  " + util.replace(orderBy, new String[] { "ASC", "DESC" }, new String[] { "", "" }).trim());
				b = b[0].split(",");
				orderBy = util.replace(orderBy,
						util.replace(orderBy, new String[] { "ASC", "DESC" }, new String[] { "", "" }),
						" " + b[b.length - 1].replaceAll("SELECT", "").trim() + " ");
			}

		}

		subSQL = subSQL.replaceFirst("SELECT", "SELECT ROW_NUMBER() OVER (ORDER BY  " + orderBy + ") AS Row, ");

		String outerSqlFields = "*";
		if (subSQL.contains(" distinct ")) {
			subSQL = subSQL.replace(" distinct ", " ");
			outerSqlFields = "";
			for (String f : subSqlFields) {
				outerSqlFields += f.split("\\.")[1].trim() + ", ";
			}
			outerSqlFields = " distinct " + outerSqlFields.replaceFirst("(?s),(?!.*?,)", "") + " ";
		}
		final int start = Integer.parseInt(p.getStart());
		final int endRow = start + Integer.parseInt(p.getLimit());

		return " " + sql[0] + " SELECT " + outerSqlFields + " FROM (" + subSQL + ") AS paginado WHERE Row between "
				+ (start + 1) + " AND " + endRow + " ";
	}

	@Override
	public String getSequenceName(final String tableName) {
		throw new PersistenceException("No  utilizado sequncia para esse BD.");
	}

	@Override
	public String[] getSqlHierarquia(final String root, final String de, final String para, final String table,
			final Usuario user) {

		return new String[] {
				" with Hierachy(" + para + ", " + de + ",Level) as  " + "( select c." + de + ", c." + para
						+ " ,0 as Level  " + "  from " + table + "  c    where c." + de + " = " + root
						+ "  union all    select c2." + de + ", c2." + para + ", ch.Level + 1   " + "   from " + table
						+ " c2    inner join Hierachy ch       on c2." + de + " = ch." + de + ") ",
				" select distinct(" + de + ") as ID from Hierachy " };
	}

	@Override
	public String getSysdate() {
		return "GETDATE()";
	}

	@Override
	public String getToChar(final String data, final int type) {
		return " convert(varchar," + data + ")";
	}

	@Override
	public String getToDate(final String mascara, final String data) {
		String convert = "NULL";

		if (!data.equals("")) {
			convert = "convert (datetime,\'" + data + "\',103)";
		}

		return convert;
	}

	public String getToDateField(final String mascara, final String data) {
		String convert = "NULL";

		if (!data.equals("")) {
			convert = "convert (datetime," + data + ",103)";
		}

		return convert;
	}

	@Override
	public String getTransactionID() {
		return " select transaction_id from sys.dm_tran_current_transaction";
	}

	@Override
	public String getTrunc(final String data) {
		return " UPPER(SUBSTRING(" + data + ",1,DATALENGTH(" + data + "))) ";
	}

	@Override
	public String getTruncDate(final String data) {
		return " CAST ( CONVERT (varchar," + data + ",110) as datetime) ";
	}

	@Override
	public boolean isCheckConstraintType(final String constraintType) {
		return constraintType.equals("CHECK_CONSTRAINT");
	}

	@Override
	public boolean isGeneratedKeyBySequence() {
		return false;
	}

	@Override
	public boolean isPrimaryKeyConstraintType(final String constraintType) {
		return constraintType.equals("PRIMARY_KEY_CONSTRAINT");
	}

	private boolean isValueTrue(final Map<String, String> map, final String key) {
		final String value = map.get(key);
		return value != null && value.equals(Constants.true_str);
	}

	@Override
	public String remaneColumn(final String tableName, final String columnName, final String newName) {

		return "sp_RENAME  '[" + tableName + "].[" + columnName + "]' , '" + newName + "' , 'COLUMN'";
	}

	@Override
	public String renameTable(final String tableName, final String newtableName) {
		return "sp_RENAME '[" + tableName + "]', '[" + newtableName + "]'";
	}

	public String sqlAlterColumn(final String tableName, final String columnName) {
		return "ALTER TABLE " + tableName + " ALTER COLUMN " + columnName;
	}

	@Override
	public boolean suportaHierarquia(final Usuario user) {
		return false;
	}

	public boolean useAlias() {
		return true;
	}

	@Override
	public String getToNumber(String campo) {
		// TODO Auto-generated method stub
		return "CONVERT(int," + campo + ")";
	}

	@Override
	public boolean requireOrder() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getAllSequences(List<String> tableNames) {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String dropSequence(String tableName, String indexName, final Usuario user) {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String subtractDaysFromDate(String string, String string2) {
		// TODO Auto-generated method stub
		return " dateadd(d,-" + string2 + "," + string + ") ";
	}

	@Override
	public String getTruncInt(String dado) {
		// TODO Auto-generated method stub
		return " CAST ( CONVERT (varchar," + dado + ",110) as int) ";
	}

	@Override
	public String getConcatOperator() {
		// TODO Auto-generated method stub
		return " + ";
	}

	@Override
	public String getSubStr(String c, String o, String f) {
		// TODO Auto-generated method stub
		return "SUBSTRING(" + c + "," + o + "," + f + ")";
	}

	@Override
	public String getSubStrName() {
		// TODO Auto-generated method stub
		return "substring";
	}

	@Override
	public String getStringDatePart(String part, String field) {
		if (part.equals("month")) {
			part = "mm";
		} else if (part.equals("year")) {
			part = "yyyy";
		} else if (part.equals("day")) {
			part = "dd";
		}
		return "datepart(" + part + ", " + field + ")";
	}

	@Override
	public String getQuoteEscape() {
		return "''";
	}

}