package com.aseg.util.correcaomonetaria;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeSet;

import com.aseg.exceptions.BusinessException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.util.ConnectionManager;
import com.aseg.util.DAOTemplate;
import com.aseg.util.sql.Dialeto;
import com.aseg.vo.GenericVO;

/**
 * Classe responsvel pelos calculos da correcao monetaria.<br>
 * 
 * @author Haroldo Asega
 * @since 4.5
 */
public class CorrecaoMonetaria extends ServiceImpl {

	/**
	 * mesesEntreDatas<br>
	 * Mtodo utilitrio, calcula quantos meses existemd e diferena entre duas
	 * datas
	 * 
	 * @param Data1
	 *            Data inicial
	 * @param Data2
	 *            Data final
	 * @return Nmero de meses entre as datas
	 */
	private static int mesesEntreDatas(final Timestamp Data1, final Timestamp Data2) {
		final Calendar data1 = Calendar.getInstance();
		data1.setTime(Data1);
		final Calendar data2 = Calendar.getInstance();
		data2.setTime(Data2);

		return 12 * (data2.get(Calendar.YEAR) - data1.get(Calendar.YEAR)) + data2.get(Calendar.MONTH)
				- data1.get(Calendar.MONTH) + 1;
	}

	private final Calendar cal = Calendar.getInstance(new Locale("pt", "BR"));

	public GenericVO calcular(final GenericVO vo, final Usuario user) throws Exception, BusinessException {
		final StringBuffer sttmt = new StringBuffer();
		List myRset = null;

		double valorCM = 0.0, valorJuros = 0.0, valorMulta = 0.0, principal = 0.0;

		if (vo.get("DATA_INICIAL") == null) {
			throw new BusinessException(user.getLabel("lb.data_inicial_nula"));
		}

		if (vo.get("DATA_JUROS") == null) {
			throw new BusinessException(user.getLabel("lb.data_juros_nula"));
		}

		if (vo.get("DATA_FINAL") == null) {
			throw new BusinessException(user.getLabel("lb.data_final_nula"));
		}

		if (vo.get("VALOR") == null) {
			throw new BusinessException(user.getLabel("lb.valor_atualizado_nulo"));
		} else
			principal = ((BigDecimal) vo.get("VALOR")).doubleValue();

		if (vo.get("MULTA") == null) {
			vo.put("MULTA", new BigDecimal(0));
			// throw new BusinessException("percentual de MULTA nulo");
		}

		if (vo.get("ID_METODO_ATUALIZACAO") == null) {
			throw new BusinessException(user.getLabel("lb.metodo_atualizacao_indefinido"));
		}

		if (mesesEntreDatas((Timestamp) vo.get("DATA_INICIAL"), (Timestamp) vo.get("DATA_FINAL")) <= 0) {
			vo.put("VALOR_ATUAL", new BigDecimal(valorCM));
			vo.put("VALOR_JUROS", new BigDecimal(valorJuros));
			vo.put("VALOR_MULTA", new BigDecimal(valorMulta));
			vo.put("VALOR_CORRECAO", new BigDecimal(0));
			vo.put("VALOR_CM", new BigDecimal("0"));
			return vo;
		}

		if (sttmt.length() > 2) {
			sttmt.delete(0, sttmt.length());
		}

		String multaCM = "F";

		final DAOTemplate jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
				createVO(user), user.getUtil());

		String metodoAtualizacao = jt
				.queryForObject("SELECT METODO_ATUALIZACAO FROM lt_metodo_atualizacao WHERE ID_METODO_ATUALIZACAO ="
						+ vo.get("ID_METODO_ATUALIZACAO"), String.class)
				.toString();

		try {

			multaCM = jt
					.queryForObject("SELECT CALCULAR_MULTA_CM FROM lt_metodo_atualizacao WHERE ID_METODO_ATUALIZACAO ="
							+ vo.get("ID_METODO_ATUALIZACAO"), String.class)
					.toString();
		} catch (final RuntimeException e1) {
			throw new BusinessException(user.getLabel("lb.metodo_atualizacao_nao_encontrada",
					new Object[] { vo.get("ID_METODO_ATUALIZACAO") }));

		}
		String tipoJuros = "", periodicidade = "";

		long idIndice = 0;
		int count = 0;

		sttmt.append(
				"SELECT MAX(DATA_INICIO) AS dataPrimeiroPeriodo FROM d_metodo_atualizacao_periodo WHERE ID_METODO_ATUALIZACAO = ? AND DATA_INICIO <= ? ");

		List mystat = jt.queryForList(sttmt.toString(),
				new Object[] { vo.get("ID_METODO_ATUALIZACAO"), vo.get("DATA_INICIAL") });

		Timestamp datateste = null;

		if (mystat.size() > 0 && ((Map) mystat.get(0)).get("DATAPRIMEIROPERIODO") != null) {
			datateste = new Timestamp(((Timestamp) ((Map) mystat.get(0)).get("DATAPRIMEIROPERIODO")).getTime());
		}

		if (datateste == null) {

			vo.put("VALOR_ATUAL", new BigDecimal("0"));
			vo.put("VALOR_JUROS", new BigDecimal("0"));
			vo.put("VALOR_MULTA", new BigDecimal("0"));
			vo.put("VALOR_CORRECAO", new BigDecimal("0"));
			vo.put("VALOR_CM", new BigDecimal("0"));

			throw new BusinessException(
					user.getLabel("lb.periodo_metodo_atualizacao_nao_definido_para_data", new Object[] {
							metodoAtualizacao, user.getUtil().df.fromTimestamp((Timestamp) vo.get("DATA_INICIAL")) }));

		}

		sttmt.delete(0, sttmt.length());
		sttmt.append("SELECT DMAP.DATA_INICIO,  (select PERIODICIDADE from lt_indice where id_indice=DMAP.id_indice)"
				+ " as PERIODICIDADE, DMAP.ID_INDICE, DMAP.TIPO_JUROS as TIPO_JUROS , DMAP.ID_INDICE_JUROS, DMAP.TAXA_JUROS , "
				+ " DMAP.TAXA_JUROS_ULTIMO_MES_TABELA  FROM  d_metodo_atualizacao_periodo DMAP "
				+ " WHERE DMAP.ID_METODO_ATUALIZACAO = ? AND DMAP.DATA_INICIO >= ? AND DMAP.DATA_INICIO <= ?  order by DATA_INICIO");

		mystat = jt.queryForList(sttmt.toString(),
				new Object[] { vo.get("ID_METODO_ATUALIZACAO"), datateste, vo.get("DATA_FINAL") });

		myRset = new ArrayList<Map<String, Object>>(mystat);

		final TreeSet pontoDatas = new TreeSet();
		final TreeSet pontoDatasF = new TreeSet();
		final int o = 0;
		int i = 0;

		final Iterator in = mystat.iterator();
		while (in.hasNext()) {
			final Map m = (Map) in.next();
			pontoDatas.add(m.get("DATA_INICIO"));

		}

		pontoDatasF.add(vo.get("DATA_FINAL"));

		Timestamp myDataInicial = null, myDataFinal = null;
		final Iterator i2 = myRset.iterator();
		while (i2.hasNext()) {

			final Map m = (Map) i2.next();
			Timestamp dataUltimoIndice = (Timestamp) vo.get("DATA_FINAL");

			try {
				myDataInicial = new Timestamp(((Timestamp) pontoDatas.first()).getTime());
				pontoDatas.remove(myDataInicial);
			} catch (final Exception e) {
				LogUtil.exception(e, user);

				vo.put("VALOR_ATUAL", new BigDecimal("0"));
				vo.put("VALOR_JUROS", new BigDecimal("0"));
				vo.put("VALOR_MULTA", new BigDecimal("0"));
				vo.put("VALOR_CORRECAO", new BigDecimal("0"));
				vo.put("VALOR_CM", new BigDecimal("0"));

				throw new BusinessException(user.getLabel("lb.periodo_metodo_atualizacao_para_data_definido_duplicado",
						new Object[] { metodoAtualizacao,
								user.getUtil().df.fromTimestamp((Timestamp) vo.get("DATA_INICIAL")),
								user.getUtil().df.fromTimestamp(new Timestamp(myDataInicial.getTime())) }));
			}

			if (i > 0) {
				vo.put("DATA_INICIAL", myDataInicial);
			}

			// if (pontoDatasF.size() == 0) {
			// dataFinal=dataFinal;
			// } else {
			myDataFinal = (Timestamp) pontoDatasF.first();
			pontoDatasF.remove(myDataFinal);
			if (i != o) {
				vo.put("DATA_FINAL", myDataFinal);
			}
			// }
			// Setando as datas pela PERIODICIDADE
			count++;
			final Object teste = m.get("ID_INDICE");
			tipoJuros = m.get("TIPO_JUROS").toString();

			if (teste != null) {
				try {
					periodicidade = m.get("PERIODICIDADE").toString();
					if (periodicidade.equalsIgnoreCase("M")) {
						cal.setTimeInMillis(((Timestamp) vo.get("DATA_INICIAL")).getTime());
						cal.set(Calendar.DAY_OF_MONTH, 1);
						((Timestamp) vo.get("DATA_INICIAL")).setTime(cal.getTime().getTime());
						cal.setTimeInMillis(((Timestamp) vo.get("DATA_FINAL")).getTime());
						cal.set(Calendar.DAY_OF_MONTH, 1);
						((Timestamp) vo.get("DATA_FINAL")).setTime(cal.getTime().getTime());
					}
					idIndice = Long.parseLong(teste.toString());

					// Calcula Correo Monetria
					dataUltimoIndice = getDataUltimoIndice(idIndice, (Timestamp) vo.get("DATA_FINAL"), user);
					if (dataUltimoIndice != null) {
						if (!((Timestamp) vo.get("DATA_INICIAL")).after(dataUltimoIndice)) {
							final double indiceInicial = obterValorIndice(idIndice, (Timestamp) vo.get("DATA_INICIAL"),
									user);
							final double indiceFinal = obterValorIndice(idIndice, dataUltimoIndice, user);
							valorCM += (principal + valorCM) * (indiceFinal / indiceInicial - 1);
						}
					} else {
						valorCM = 0.00;
					}

				} catch (final Exception e) {
					vo.put("VALOR_ATUAL", new BigDecimal("0"));
					vo.put("VALOR_JUROS", new BigDecimal("0"));
					vo.put("VALOR_MULTA", new BigDecimal("0"));
					vo.put("VALOR_CORRECAO", new BigDecimal("0"));
					vo.put("VALOR_CM", new BigDecimal("0"));
					LogUtil.exception(e, user);
					throw e;
				}
			}

			// Calcula Juros

			if (dataUltimoIndice != null) {
				if (!((Timestamp) vo.get("DATA_JUROS")).after(dataUltimoIndice)) {
					if (tipoJuros.equalsIgnoreCase("S")) {
						valorJuros += jurosSimples(((BigDecimal) vo.get("VALOR")).doubleValue() + valorCM,
								((BigDecimal) m.get("TAXA_JUROS")).doubleValue(),
								mesesEntreDatas((Timestamp) vo.get("DATA_JUROS"), dataUltimoIndice));
					} else if (tipoJuros.equalsIgnoreCase("C")) {
						valorJuros += jurosCompostos(((BigDecimal) vo.get("VALOR")).doubleValue() + valorJuros,
								((BigDecimal) m.get("TAXA_JUROS")).doubleValue(),
								mesesEntreDatas((Timestamp) vo.get("DATA_JUROS"), dataUltimoIndice));
					} else if (tipoJuros.equalsIgnoreCase("T")) {

						valorJuros += jurosTabela(((BigDecimal) vo.get("VALOR")).doubleValue() + valorCM,
								(Timestamp) vo.get("DATA_JUROS"), dataUltimoIndice,
								new Long(m.get("ID_INDICE_JUROS").toString()).longValue(),
								((BigDecimal) m.get("TAXA_JUROS_ULTIMO_MES_TABELA")).doubleValue(), user);

					}
				}
			} else {
				valorJuros = 0.00;
			}

			// dataFinal.setTime(dataFinal.getTime());

			i++;
		}

		valorMulta = multaCM.equalsIgnoreCase("T")
				? (((BigDecimal) vo.get("VALOR")).doubleValue() + valorCM)
						* (((BigDecimal) vo.get("MULTA")).doubleValue() / 100)
				: ((BigDecimal) vo.get("VALOR")).doubleValue() * (((BigDecimal) vo.get("MULTA")).doubleValue() / 100);

		vo.put("VALOR_ATUAL", new BigDecimal(((BigDecimal) vo.get("VALOR")).doubleValue() + valorCM));
		vo.put("VALOR_JUROS", new BigDecimal(valorJuros));
		vo.put("VALOR_MULTA", new BigDecimal(valorMulta));
		vo.put("VALOR_CM", new BigDecimal(valorCM));
		return vo;
	}

	/**
	 * getDataUltimoIndice<br>
	 * Mtodo utilitrio para retornar a ultima data fque possue indice
	 * cadastrado.
	 * 
	 * @param idIndice
	 *            Id do indice
	 * @param data
	 *            Data final
	 * @return Valor do indice
	 * @throws Exception
	 */
	private Timestamp getDataUltimoIndice(final long idIndice, final Timestamp data, final Usuario user)
			throws Exception {
		Timestamp dataUltimoIndice = null;

		List pst = null;

		try {
			final DAOTemplate jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
					createVO(user), user.getUtil());

			pst = jt.queryForList(" SELECT MAX(DATA) AS DATA FROM m_valor_indice WHERE ID_INDICE = ? AND DATA <= ?",
					new Object[] { new Long(idIndice), data });

			if (pst.size() > 0) {
				dataUltimoIndice = new Timestamp(((Timestamp) ((Map) pst.get(0)).get("DATA")).getTime());
			}

		} catch (final Exception e) {
			throw new BusinessException(user.getLabel("lb.correcao_monetaria_indice_sem_valores"));

		}

		return dataUltimoIndice;
	}

	private double jurosCompostos(final double principal, final double taxaJuros, final int n) {
		return principal * Math.pow(1 + taxaJuros / 100, n) - principal;
	}

	private double jurosSimples(final double principal, final double taxaJuros, final int n) {
		return principal * taxaJuros / 100 * n;
	}

	private double jurosTabela(final double principal, final Timestamp dataInicial, final Timestamp dataFinal,
			final long idTabelaJuros, final double taxaJurosUltimoMesTabela, final Usuario user)
			throws Exception, BusinessException {

		final Calendar cal = Calendar.getInstance(new Locale("pt", "BR"));
		List pstate;
		double juros = 0;
		cal.setTime(dataFinal);
		if (cal.get(Calendar.MONTH) == 0) {
			cal.set(Calendar.MONTH, 11);
			cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 1);
		}
		// else
		// cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1);

		cal.set(Calendar.DAY_OF_MONTH, 1);

		final Timestamp df = new Timestamp(cal.getTime().getTime());
		// mes inicial +1
		cal.setTime(dataInicial);
		if (cal.get(Calendar.MONTH) == 11) {// dezembro?
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
		}
		// } else
		// cal.set(Calendar.MONTH,cal.get(Calendar.MONTH)+1);

		cal.set(Calendar.DAY_OF_MONTH, 1);
		final DAOTemplate jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
				createVO(user), user.getUtil());
		pstate = jt.queryForList(
				" SELECT SUM(VALOR) AS TAXA_JUROS " + "FROM m_valor_indice WHERE ID_INDICE = ? " + " AND "
						+ Dialeto.getInstance().getDialeto(user).getTruncDate("DATA") + " >= ? AND "
						+ Dialeto.getInstance().getDialeto(user).getTruncDate("DATA") + " <= ? ",
				new Object[] { idTabelaJuros, new Timestamp(cal.getTime().getTime()), df });
		if (pstate.size() > 0 && ((Map) pstate.get(0)).get("TAXA_JUROS") != null) {
			juros = ((BigDecimal) ((Map) pstate.get(0)).get("TAXA_JUROS")).doubleValue();
		}

		juros += taxaJurosUltimoMesTabela;
		return principal * juros / 100;
	}

	/**
	 * obterValorIndice<br>
	 * Mtodo utilitrio para retornar o indice de uma data especfica
	 * 
	 * @param idIndice
	 *            Id do indice
	 * @param data
	 *            Data final
	 * @return Valor do indice
	 * @throws Exception
	 *             ,BusinessException
	 */
	private double obterValorIndice(final long idIndice, final Timestamp data, final Usuario user)
			throws Exception, BusinessException {
		final ResultSet resset;
		Double indice = null;
		List pstate = null;
		String nomeIndice = "";

		final Timestamp data_caso_nula = data;
		final DAOTemplate jt = new DAOTemplate(ConnectionManager.getInstance(user).getDataSource(user), user,
				createVO(user), user.getUtil());
		pstate = jt.queryForList(" SELECT VALOR FROM m_valor_indice WHERE ID_INDICE = ? AND DATA = ?",
				new Object[] { new Long(idIndice), data });

		nomeIndice = jt.queryForObject(" SELECT INDICE FROM LT_INDICE WHERE ID_INDICE = " + idIndice, String.class)
				.toString();

		if (pstate.size() > 0) {
			indice = ((BigDecimal) ((Map) pstate.get(0)).get("VALOR")).doubleValue();
		}

		if (indice == null) {
			if (data != null) {

				throw new BusinessException(user.getLabel("lb.valor_em_data_para_indice_nao_registrado",
						new Object[] { user.getUtil().df.fromTimestamp(new Timestamp(data.getTime())), nomeIndice }));

			} else {

				throw new BusinessException(user.getLabel("lb.valor_em_data_nula_para_indice_nao_registrado",
						new Object[] { user.getUtil().df.fromTimestamp(new Timestamp(data_caso_nula.getTime())),
								nomeIndice }));
			}
		}

		return indice.doubleValue();
	}
}