package com.aseg.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;

public class DateIterator implements Iterator {
	private Calendar end = Calendar.getInstance();
	private Calendar current = Calendar.getInstance();

	int tap = Calendar.DATE;
	int interval = 1;

	@Override
	public boolean hasNext() {

		// TODO Auto-generated method stub
		return !current.after(end);

	}

	@Override
	public Object next() {
		current.add(tap, interval);
		return new Timestamp(current.getTime().getTime());
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Cannot remove");

	}

	private DateIterator() {
	}

	public DateIterator(Calendar a, Calendar b, int tap, int interval)

	{
		this.end = b;
		this.current = a;
		this.tap = tap;
		this.interval = interval;
	}

	public DateIterator(Calendar a, Calendar b)

	{
		this.end = b;
		this.current = a;

	}

	public DateIterator(Calendar a, Calendar b, int tap)

	{
		this.end = b;
		this.current = a;
		this.tap = tap;

	}
}
