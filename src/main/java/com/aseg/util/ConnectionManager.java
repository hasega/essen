package com.aseg.util;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.aseg.exceptions.ConfigurationException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.ServiceImpl;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


/**
 * Classe responsvel pelo fornecimento de conexes com o banco de dados.
 * <p>
 * Por exemplo:
 * <p>
 * <pre>
 * ConnectionManager cm = ConnectionManager.getInstance();
 *
 * Connection c = cm.getConexao();
 *
 * </pre>
 */
public class ConnectionManager {

    public static final String DRIVER = "POOL_DRIVER";
    public static final String URL = "POOL_URL";
    public static final String USER = "POOL_USER";
    public static final String PASSWORD = "POOL_PASSWORD";
    public static InitialContext context;
    public final UtilServiceImpl util = new UtilServiceImpl();
    List clientes = null;

    public String getDriver(final Usuario user) throws SQLException, Exception {
        // final Connection con = getInstance(user).getDataSource(user)
        // .getConnection();
        // final String r = con.getClass().getName();
        // con.close();
        return ((String) getPool().get(user.getConnectionRoute()).get(DRIVER));
    }

    public String getUrl(final Usuario user) throws SQLException, Exception {
        // final Connection con = getInstance(user).getDataSource(user)
        // .getConnection();
        // final String r = con.getClass().getName();
        // con.close();
        return ((String) getPool().get(user.getConnectionRoute()).get(URL));
    }

    public String getUserName(final Usuario user) throws SQLException, Exception {
        // final Connection con = getInstance(user).getDataSource(user)
        // .getConnection();
        // final String r = con.getClass().getName();
        // con.close();
        return ((String) getPool().get(user.getConnectionRoute()).get(USER));
    }

    public String getPassword(final Usuario user) throws SQLException, Exception {
        // final Connection con = getInstance(user).getDataSource(user)
        // .getConnection();
        // final String r = con.getClass().getName();
        // con.close();
        return ((String) getPool().get(user.getConnectionRoute()).get(PASSWORD));
    }

    /**
     * Retorna uma instncia de ConnectionManager.
     *
     * @param user
     * @return instncia de java.sql.ConnectionManager
     * @since 1.0
     */
    public static ConnectionManager getInstance(final Usuario user) throws ConfigurationException {

        if (instance.get(user.getConnectionRoute()) == null) {

            Connection con = null;
            if (instance.get(user.getConnectionRoute()) == null) {
                instance.put(user.getConnectionRoute(), new ConnectionManager().load(user));
            }

            try

            {

                con = instance.get(user.getConnectionRoute()).getConexao(user);
                if (con.getMetaData().getDatabaseProductName().equalsIgnoreCase("oracle")) {
                    instance.get(user.getConnectionRoute()).dataBaseConnected = ORACLE;
                } else if (con.getMetaData().getDatabaseProductName().toLowerCase().indexOf("microsoft") > -1) {
                    instance.get(user.getConnectionRoute()).dataBaseConnected = SQLSERVER;
                } else if (con.getMetaData().getDatabaseProductName().toLowerCase().indexOf("postgresql") > -1) {
                    instance.get(user.getConnectionRoute());
                    instance.get(user.getConnectionRoute()).dataBaseConnected = POSTGRESQL;
                } else if (con.getMetaData().getDatabaseProductName().toLowerCase().indexOf("mysql") > -1) {
                    instance.get(user.getConnectionRoute());
                    instance.get(user.getConnectionRoute()).dataBaseConnected = MYSQL;
                } else if (con.getMetaData().getDatabaseProductName().toLowerCase().indexOf("firebird") > -1) {
                    instance.get(user.getConnectionRoute());
                    instance.get(user.getConnectionRoute()).dataBaseConnected = FIREBIRD;
                }
            } catch (final SQLException e) {
                LogUtil.exception(e, user);
                return null;
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (final SQLException e) {
                        LogUtil.exception(e, user);
                    }
                }
            }
        }
        return instance.get(user.getConnectionRoute());
    }

    public String getjndiName() {
        return jndiName;
    }

    public int getVersion(final Usuario user) throws SQLException, Exception {
        final Connection con = getInstance(user).getDataSource(user).getConnection();
        final int r = con.getMetaData().getDatabaseMajorVersion();
        con.close();
        return r;
    }

    public void loadConnection(final Usuario user) {
        final ParseXmlService sx = (ParseXmlService) user.getSysParser();

        String driver = null, url = null, userName = null, password = null;
        try {
            driver = sx.getValueFromXQL("/systems/system[@name='" + user.getSchema() + "']", DRIVER, user, true, false);
        } catch (final Exception e) {
            final ServiceImpl service = (ServiceImpl) user.getFactory().getSpringBean("SystemService", user);
            final GenericVO vo = service.createVO(user);
            vo.setPaginate(false);

            List sysConfig = null;
            try {
                sysConfig = service.lista(vo, user);
            } catch (final Exception e1) {
                LogUtil.exception(e1, user);
            }
            try {
                driver = (String) ((Map) util.filter(sysConfig, "ID_CONFIG_SISTEMA", "GENERAL_DRIVER").get(0))
                        .get("VALOR_CONFIG_SISTEMA");
            } catch (final Exception e2) {
                LogUtil.exception(e2, user);
                if (driver == null || driver.toString().trim().equals("")) {
                    throw new ConfigurationException("Driver no definidao", ConfigurationException.TYPE.DB_PARAMS);
                }
            }
        }

        try {
            url = sx.getValueFromXQL("/systems/system[@name='" + user.getSchema() + "']", URL, user, true, false);
        } catch (final Exception e) {
            final ServiceImpl service = (ServiceImpl) user.getFactory().getSpringBean("SystemService", user);
            final GenericVO vo = service.createVO(user);
            vo.setPaginate(false);

            List sysConfig = null;
            try {
                sysConfig = service.lista(vo, user);
            } catch (final Exception e1) {
                LogUtil.exception(e1, user);
            }
            try {
                url = (String) ((Map) util.filter(sysConfig, "ID_CONFIG_SISTEMA", "GENERAL_URL").get(0))
                        .get("VALOR_CONFIG_SISTEMA");
            } catch (final Exception e2) {
                if (url == null || url.toString().trim().length() == 0) {
                    throw new ConfigurationException("URL no definida", ConfigurationException.TYPE.DB_PARAMS);
                }
            }
        }

        try {
            userName = sx.getValueFromXQL("/systems/system[@name='" + user.getSchema() + "']", USER, user, true, false);
        } catch (final Exception e) {
            final ServiceImpl s = (ServiceImpl) user.getFactory().getSpringBean("SystemService", user);
            final GenericVO vo = s.createVO(user);
            vo.setPaginate(false);

            List sysConfig = null;
            try {
                sysConfig = s.lista(vo, user);
            } catch (final Exception e1) {
                LogUtil.exception(e1, user);
            }
            try {
                userName = (String) ((Map) util.filter(sysConfig, "ID_CONFIG_SISTEMA", "GENERAL_USER").get(0))
                        .get("VALOR_CONFIG_SISTEMA");
            } catch (final Exception e2) {
                if (userName == null || userName.trim().equals("")) {
                    throw new ConfigurationException("Usuario do bd no definido",
                            ConfigurationException.TYPE.DB_PARAMS);
                }
            }
        }
        try {
            password = sx.getValueFromXQL("/systems/system[@name='" + user.getSchema() + "']", PASSWORD, user, true,
                    false);
        } catch (final Exception e) {
            final ServiceImpl s = (ServiceImpl) user.getFactory().getSpringBean("SystemService", user);
            final GenericVO vo = s.createVO(user);
            vo.setPaginate(false);

            List sysConfig = null;
            try {
                sysConfig = s.lista(vo, user);
            } catch (final Exception e1) {
                LogUtil.exception(e1, user);
            }
            try {
                password = (String) ((Map) util.filter(sysConfig, "ID_CONFIG_SISTEMA", "GENERAL_PASS").get(0))
                        .get("VALOR_CONFIG_SISTEMA");
            } catch (final Exception e2) {
                if (password == null || password.trim().equals("")) {
                    throw new ConfigurationException("Senha do db no definida", ConfigurationException.TYPE.DB_PARAMS);
                }
            }
        }
        if (util.valid(driver) && util.valid(url) && util.valid(userName) && util.valid(password)) {


            //	final BoneCPDataSource sp = new BoneCPDataSource();
            Map data = new HashMap();
            data.put(DRIVER, driver);
            data.put(URL, url);
            data.put(USER, userName);
            data.put(PASSWORD, password);

            //sp.setDriverClass(driver);
            //	sp.setJdbcUrl(url);
            //	sp.setUsername(userName);
            //	sp.setPassword(password);

            HikariConfig config = new HikariConfig();
            config.setJdbcUrl(url);
            config.setUsername(userName);
            config.setPassword(password);
            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");


            HikariDataSource ds = new HikariDataSource(config);
            data.put("DS", ds);
            getPool().put(user.getSystem(), data);

            try {
                loadSASConfiguration(user);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                LogUtil.exception(e, user);
            }
            configurePool(ds, user);

        }

    }

    private void configurePool(final HikariDataSource sp, Usuario user) {
        // if(LogUtil.debugEnabled())
        // sp.setCloseConnectionWatch(true);
        // sp.setMinConnectionsPerPartition(10);
        // sp.setDisableConnectionTracking(true);
        //sp.setPartitionCount(
        //	Integer.parseInt(user.getUtil().valid(user.getSystemProperty("POOL_PARTITION_COUNT"), "2")));
        //sp.setMaxConnectionsPerPartition(
        //	Integer.parseInt(user.getUtil().valid(user.getSystemProperty("POOL_MAX_CONNECTION"), "15")));
        // sp.setStatementsCacheSize(100);
        // sp.setIdleConnectionTestPeriodInMinutes(5);
        // sp.setIdleMaxAge(30,TimeUnit.MINUTES);
        sp.setPoolName(user.getSystem());
        ((HikariDataSource) sp).setConnectionTimeout(30000);
        ((HikariDataSource) sp).setAutoCommit(true);
        ((HikariDataSource) sp).setIdleTimeout(60000);
        ((HikariDataSource) sp).setMaxLifetime(1800000);
        ((HikariDataSource) sp).setLeakDetectionThreshold(15000);
        ((HikariDataSource) sp).setMaximumPoolSize(20);
        ((HikariDataSource) sp).setRegisterMbeans(true);

        ((HikariDataSource) sp).setInitializationFailFast(true);
        ((HikariDataSource) sp).setConnectionTestQuery("SELECT 1 from dual");
        //sp.setLogStatementsEnabled(true);
        //try {
        //		sp.setLogWriter(new PrintWriter(System.out, true));
//		} catch (SQLException e) {
        // TODO Auto-generated catch block
        //	e.printStackTrace();//
        //}
        // sp.setTestOnBorrow(true);
        // sp.setTestWhileIdle(true);
        // sp.setTestOnReturn(true);
        //if (sp.getDriverClassName().toLowerCase().indexOf("oracle") > -1)
        //	sp.setConnectionTestQuery("select 1 from dual");
        //	else
        //	sp.setConnectionTestQuery("select 1 ");

        // sp.setPreparedStatementsCacheSize(100);
        // sp.setMaxWait(1000 * 60 * 30); // meia hora esperando
        // sp.setMaxIdle(10);
        // sp.setTimeBetweenEvictionRunsMillis(1000 * 60 * 30); // de meia em
        // meia
        // hora valido
        // as conexoes
        // firewalls
        // geralmente
        // cortariam
        // conexoes
        // ociosas em
        // uma hora
        // sp.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis) deixamos
        // no default que o intervalo de execucoes do teste
    }

    public List loadSASConfiguration(final Usuario user) throws Exception {

        final ServiceImpl s = (ServiceImpl) user.getFactory().getSpringBean("ASPConfigService", user);
        if (s == null)
            return new ArrayList<>();
        if (clientes != null)
            return clientes;

        final GenericVO vo = s.createVO(user);
        vo.setPaginate(false);
        vo.setLazyList(true);

        try {
            clientes = s.lista(vo, user);
            final ServiceImpl si = (ServiceImpl) user.getFactory().getSpringBean("ASPConfigItemService", user);
            Map type;
            for (final Iterator iterator = clientes.iterator(); iterator.hasNext(); ) {
                type = (Map) iterator.next();

                final GenericVO vi = si.createVO(user);
                vi.put("ID_PAI", type.get("ID_CONFIG_SISTEMA"));
                vi.put("ID_CONFIG_SISTEMA", type.get("ID_CONFIG_SISTEMA"));
                vi.setPaginate(false);
                // vi.putKey("NOME_CONFIG_SISTEMA", DRIVER);
                vi.setLazyList(true);
                final List itemsC = si.lista(vi, user);
                type.put("CONFIG_ITEMS", itemsC);

                try {
                    Map data = new HashMap();
                    if (!type.get("VALOR_CONFIG_SISTEMA").equals(user.getSystem())) {
                        final HikariDataSource sp = new HikariDataSource();

                        LogUtil.debug("Resgatando DataSource para > " + type.get("VALOR_CONFIG_SISTEMA").toString(),
                                user);

                        data.put(DRIVER, (String) ((Map) util.match(itemsC, "NOME_CONFIG_SISTEMA", DRIVER).get(0))
                                .get("VALOR_CONFIG_SISTEMA"));
                        data.put(URL, (String) ((Map) util.match(itemsC, "NOME_CONFIG_SISTEMA", URL).get(0))
                                .get("VALOR_CONFIG_SISTEMA"));
                        data.put(USER, (String) ((Map) util.match(itemsC, "NOME_CONFIG_SISTEMA", USER).get(0))
                                .get("VALOR_CONFIG_SISTEMA"));
                        data.put(PASSWORD, (String) ((Map) util.match(itemsC, "NOME_CONFIG_SISTEMA", PASSWORD).get(0))
                                .get("VALOR_CONFIG_SISTEMA"));

                        //sp.setDriverClass((String) data.get(DRIVER));

                        sp.setJdbcUrl((String) data.get(URL));

                        sp.setUsername((String) data.get(USER));

                        sp.setPassword((String) data.get(PASSWORD));
                        configurePool(sp, user);
                        data.put("DS", sp);
                    }
                    if (!getPool().containsKey(type.get("VALOR_CONFIG_SISTEMA").toString()))
                        getPool().put(type.get("VALOR_CONFIG_SISTEMA").toString(), data);

                    user.getFactory().getSystems().put(type.get("VALOR_CONFIG_SISTEMA").toString(),
                            user.getSchema());
                    user.getFactory().getSystemProperties().put(type.get("VALOR_CONFIG_SISTEMA").toString(),
                            (List<Map<String, Object>>) type.get("CONFIG_ITEMS"));

                    LogUtil.debug(
                            "Cliente " + type.get("VALOR_CONFIG_SISTEMA").toString() + " configurado com sucesso.",
                            user);
                } catch (final Exception e) {
                    if (LogUtil.debugEnabled(user))
                        LogUtil.exception(e, user);
                    LogUtil.debug("Erro lendo banco de dados do cliente " + type.get("VALOR_CONFIG_SISTEMA").toString(),
                            user);
                    continue;
                }

            }
            return clientes;
        } catch (final Exception e) {
            LogUtil.exception(e, user);
        }
        return clientes;
    }

    public void setjndiName(final String jndiName) {
        ConnectionManager.jndiName = jndiName;
    }

    protected int dataBaseConnected = 0;
    // public final static String DEFAULT_DATASOURCE = "asp_admin";

    public final static int FIREBIRD = 5;

    private static volatile Map<Object, ConnectionManager> instance = new HashMap<Object, ConnectionManager>();

    private static String jndiName;

    public final static int MYSQL = 4;

    public final static int ORACLE = 1;

    private static Map<String, Map> pool = new HashMap<String, Map>() {
        public Map put(String key, Map value) {
            return super.put(key, value);
        }

        ;
    };

    public final static int POSTGRESQL = 3;

    public final static int SQLSERVER = 2;

    private ConnectionManager() {
    }

    private void addDataSource(final String lookup, Usuario user) throws NamingException {
        Map data = new HashMap();
        data.put("DS", (DataSource) ConnectionManager.context.lookup(lookup));
        getPool().put(user.getConnectionRoute(), data);
    }

    public int getBancoConectado(final Usuario user) {
        if (instance.get(user.getConnectionRoute()) == null) {
            try {
                getInstance(user);
            } catch (final Exception e) {
                LogUtil.exception(e, user);
            }
        }
        return instance.get(user.getConnectionRoute()).dataBaseConnected;
    }

    public String getStringBancoConectado(final Usuario user) {
        switch (getBancoConectado(user)) {
            case 1:
                return "oracle";
            case 2:
                return "sqlserver";
            case 3:
                return "postgresql";
            case 4:
                return "mysql";
            default:
                return "";
        }
    }

    /**
     * Retorna uma instncia de Connection do pool de conexo, possibilitando a
     * conexo com o banco.
     *
     * @param user
     * @return instncia de java.sql.Connection
     * @see java.sql.Connection
     * @since 1.0
     */
    public Connection getConexao(final Usuario user) throws java.sql.SQLException {
        if (instance == null) {
            try {
                getInstance(user);
            } catch (final Exception e) {
                LogUtil.exception(e, user);
            }
        }

        if (getPool().get(user.getConnectionRoute()) == null) {
            throw new ConfigurationException("Conexo no configurada",
                    ConfigurationException.TYPE.CANNOT_ROUT_CONNECTION);
        }

        return ((DataSource) getPool().get(user.getConnectionRoute()).get("DS")).getConnection();

    }

    public DataSource getDataSource(final Usuario user) throws java.sql.SQLException {
        if (user.getService("dataSouce") != null) {
            return (DataSource) user.getService("dataSouce");
        }
        if (instance == null) {
            try {
                getInstance(user);
            } catch (final Exception e) {
                LogUtil.exception(e, user);
            }
        }
        if (getPool().get(user.getConnectionRoute()) == null) {
            loadConnection(user);
        }
        // if (user == null) {
        // return (DataSource) getPool().get(DEFAULT_DATASOURCE).get("DS");
        // }

        final DataSource d = (DataSource) getPool().get(user.getConnectionRoute()).get("DS");
        // try {
        // final Connection c = d.getConnection();
        // c.close();
        // } catch (final Exception e) {
        // loadConnection(user);
        // }

        return d;
    }

    private ConnectionManager load(final Usuario user) throws ConfigurationException {

        if (getPool().containsKey(user.getConnectionRoute())) {
            return this;
        }

        try {

            jndiName = "DS_EssenWeb";

        } catch (final MissingResourceException e) {
            LogUtil.exception(e, user);
        }

        final String clientName = "Virtuem";

        try {

            context = new InitialContext();

            addDataSource("jdbc/" + jndiName + "_" + clientName, user);

            jndiName = "jdbc/" + jndiName + clientName;
        } catch (final Exception ne)

        {
            try {
                addDataSource("jdbc/" + jndiName, user);

                jndiName = "jdbc/" + jndiName;
            } catch (final Exception ne2)

            {
                try {
                    addDataSource("java:comp/env/" + jndiName + "_" + clientName, user);

                    jndiName = "java:comp/env/" + jndiName + "_" + clientName;
                } catch (final Exception ne3)

                {
                    try {
                        addDataSource("java:comp/env/" + jndiName, user);

                        jndiName = "java:comp/env/" + jndiName;

                    } catch (final Exception ne4)

                    {
                        try {
                            addDataSource("java:" + "jdbc/" + jndiName + "_" + clientName, user);

                            jndiName = "java:" + "jdbc/" + jndiName + "_" + clientName;
                        } catch (final Exception ne5) {

                            try {
                                addDataSource("java:" + "jdbc/" + jndiName, user);

                                jndiName = "java:" + "jdbc/" + jndiName;
                            } catch (final Exception ne6) {

                                try {
                                    addDataSource(jndiName + "_" + clientName, user);

                                    jndiName = jndiName + "_" + clientName;
                                } catch (final Exception ne7) {

                                    try {
                                        addDataSource(jndiName, user);

                                    } catch (final Exception ne8) {
                                        loadConnection(user);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return this;
    }

    public static Map<String, Map> getPool() {
        return pool;
    }

    public static void setPool(Map<String, Map> pool) {
        ConnectionManager.pool = pool;
    }

    public void createPool(String name, String driver, String url, String username, String pass, Usuario user) {
        Map data = new HashMap();

        final HikariDataSource sp = new HikariDataSource();

        data.put(DRIVER, driver);
        data.put(URL, url);
        data.put(USER, username);
        data.put(PASSWORD, pass);

        //sp.setDriverClass((String) data.get(DRIVER));

        sp.setJdbcUrl((String) data.get(URL));

        sp.setUsername((String) data.get(USER));

        sp.setPassword((String) data.get(PASSWORD));
        configurePool(sp, user);
        data.put("DS", sp);

        if (!getPool().containsKey(name))
            getPool().put(name, data);

    }

    public DataSource refreshDataSource(HikariDataSource datasource) {
        // TODO Auto-generated method stub
        HikariConfig cfg = new HikariConfig();
        datasource.copyState(cfg);
        datasource.close();
        return new HikariDataSource(cfg);

    }

}
