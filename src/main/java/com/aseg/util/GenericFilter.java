package com.aseg.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.aseg.config.Constants;
import com.aseg.exceptions.BusinessException;
import com.aseg.exceptions.SessionExpiredException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.service.protocol.RestProtocol;
import com.aseg.service.protocol.ServiceProtocol;
import com.aseg.service.protocol.SoapProtocol;
import com.aseg.util.apresentacao.BaseController;
import com.aseg.util.service.Inflector;
import com.aseg.util.service.ParseXml;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.wsdl.Bean2WSDL;
import com.aseg.vo.GenericVO;
//import com.tonbeller.wcf.component.component.ObjectsRepository;

@WebFilter
public class GenericFilter extends BaseController implements Filter {

	@Override
	public ModelAndView altera_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		return null;
	}

	@Override
	public void destroy() {
	}

	private Usuario env(final ServletRequest request, Usuario user
	// ,
	// int forceLoginReq
	) {
		String uri = ((HttpServletRequest) request).getRequestURI();
		uri = uri.substring(1, uri.length());
		if (uri.indexOf('/') > -1) {
			uri = uri.substring(0, uri.indexOf('/'));
		}

		user.setServerUrl(((HttpServletRequest) request).getRequestURL().toString());
		user.setSessionId((((HttpServletRequest) request).getSession()).getId());
		user.setSession((((HttpServletRequest) request).getSession()));

		if (uri.trim().length() > 0 && !uri.equalsIgnoreCase("css") && !uri.equalsIgnoreCase("images")
				&& !uri.equalsIgnoreCase("scripts") && uri.indexOf(",") == -1
				&& !uri.equalsIgnoreCase("Setup" + Constants.ACTION_BASE + Constants.ACTION_SUFIX)) {

			if (uri.indexOf('.') == -1 && !uri.equals("SeleniumTester")) {
				user.setSystem(uri);
			}

		}

		user.setIpOrigem(request.getRemoteAddr());
		user.setHostOrigem(request.getRemoteHost());
		user.setPort("" + request.getRemotePort());

		if(request.getAttribute(Constants.RAIN_USER)==null)
		{
			user.setSystem(uri);
			user.setSchema(uri);
			 final ParseXmlService sx = (ParseXmlService) user.getSysParser();
			 String ini = sx.getValueFromXQL(
			 "/systems/system[@name='" + user.getSchema() +
			 "']/module/bean[@initial='true']", Constants.name, user,
			 true, false);
			
			 user.setIndexAction(ini + Constants.ACTION_BASE +
			 Constants.ACTION_SUFIX);
			
			 ini = sx.getValueFromXQL(
			 "/systems/system[@name='" + getUser(request).getSchema() +
			 "']/module/bean[@main='true']",
			 Constants.name, getUser(request), true, false);
			
			 user.setMainAction(ini + Constants.ACTION_BASE +
			 Constants.ACTION_SUFIX);
			setReqAttr(request, Constants.RAIN_USER, user);
		}

		return user;
	}


	private boolean isRequiredSession(ServletRequest request, Usuario user) {
		if (request.getParameter("session") != null && request.getParameter("session").equals("false")) {
			// verifica se existe um mtodo definido no config-sis, que
			// realmente anula a necessidade de uma sesso registrada.
			// para evitando problemas de seguna.

			List<Map<String, String>> methods = (List<Map<String, String>>) user.getSysParser()
					.getListValuesFromXQL(
							"/systems/system[@name='" + user.getSchema() + "']//bean[@name='"
									+ getBeanNameOfUri(request, user) + "']/method[@name = '"
									+ request.getParameter(Constants.ActionParam) + "' and @requiredSession='false']",
							"order", user, true, false);
			if (methods.size() > 0) {
				return false;
			}
		}
		return true;
	}

	private String getBeanNameOfUri(ServletRequest request, Usuario user) {
		return ((HttpServletRequest) request).getRequestURI().replaceAll(user.getSystem() + "/", "").replaceAll("/", "")
				.replaceAll(Constants.ACTION_BASE + Constants.ACTION_SUFIX, "");
	}

	public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
			throws IOException, ServletException {
		// try {

		HttpServletRequest request = (HttpServletRequest) req;
		final HttpServletResponse response = (HttpServletResponse) res;

		if (getParameter(request, "session", false) != null
				&& getParameter(request, "session", false).toString().equalsIgnoreCase("false")) {
			requiredUser = false;
			NoRequiredUser(request);
		} else {
			requiredUser = true;
		}

		final String url = request.getRequestURL().toString();

		Usuario user = env(request, getUser(request));





		// if (!user.getSystem().equals(
		// user.getSchema())
		// && url.indexOf(Constants.ACTION_SUFIX) > -1) // sistema de
		// // redirecionamento
		// // verificar em
		// // webservices
		// {
		// request.getRequestDispatcher(
		// request.getRequestURI().replaceAll(
		// user.getSchema(),
		// user.getSystem()))
		// .forward(req, res);
		// return;
		// }

		// }

		// if (url.indexOf(Constants.ACTION_SUFIX) == -1) {
		//
		// if (url.matches(".+/services/.+WS$")) {
		// processSOAP(request, response, url);
		// return;
		//
		// } else if (url.matches(".+/services/rest/.+\\.xml$")) {
		// processRest(request, response, url);
		// return;
		//
		// } else if (url.endsWith("services")) {
		// try {
		// lista_imp(request, response); // getServices Avaliable
		// return;
		// } catch (final Exception e1) {
		// LogUtil.exception(e1, user);
		// }
		// } else if (url.endsWith(".wsdl")) {
		// try {
		// obtem_imp(request, response); // Describe Service
		// return;
		// } catch (final Exception e1) {
		// LogUtil.exception(e1, user);
		// }
		// }
		// /*else if (url.toLowerCase().matches(".+axis.+$") ||
		// url.toLowerCase().matches(".+servlet.+$")) {
		// processServlet(request, response, url, user);
		// return;
		//
		// }*/
		// } else if (user != null && user.getHostOrigem() == null &&
		// !getSession(request).isNew() && requiredUser
		// && getParameter(request, Constants.ActionParam, false) != null
		// && !getParameter(request, Constants.ActionParam,
		// false).toString().equalsIgnoreCase("icon")
		// && !getParameter(request, Constants.ActionParam,
		// false).toString().equalsIgnoreCase("download")) {
		// throw new SessionExpiredException(user);
		//
		// } else {
		// LogUtil.debug(url, user);
		// }

		// @SuppressWarnings("unchecked")
		// Enumeration<String> e = request.getParameterNames();
		// LogUtil.debug(" Encoding da Requisicao >>>"
		// + request.getCharacterEncoding());
		//
		// while (e.hasMoreElements()) {
		// String object = e.nextElement();
		// LogUtil.debug(" Parametros no filter " + object + " >>>"
		// + getParameter(request, object, false));
		//
		// }

		// e = request.getHeaderNames();
		// while (e.hasMoreElements()) {
		// String object = (String) e.nextElement();
		// if(!user.isMobile())
		// if(object.equalsIgnoreCase("user-agent") &&
		// request.getHeader(object).indexOf("iPhone")>-1)
		// user.setMobile(true);
		// LogUtil.debug(" Headers no filter "+ object+ " >>>" +
		// request.getHeader(object));
		//
		// }
		response.addHeader("Access-Control-Allow-Origin", "*");
		if (req.getParameter(Constants.ActionParam) != null
				&& (req.getParameter(Constants.ActionParam).equalsIgnoreCase("download")
						|| req.getParameter(Constants.ActionParam).equalsIgnoreCase("icon"))
				&& !url.endsWith(".jsp")) {

			response.setHeader("Cache-Control", "public");
			response.setHeader("Pragma", "!invalid");
			response.setDateHeader("Expires", System.currentTimeMillis() + 86400000 * 7);

		} else if (user != null) {
			response.setHeader("Content-Type", "text/html;charset=" + user.getEncode());

			// Arquivos estáticos
			if (url.indexOf(Constants.ACTION_BASE) == -1 && !url.endsWith(".jsp")) {
				// 86400000 milisegundos = 1 dias
				response.setHeader("Cache-Control", "public");
				response.setHeader("Pragma", "!invalid");
				response.setDateHeader("Expires", System.currentTimeMillis() + 86400000 * 7);
				// Arquivos dinâmicos
			} else {
				response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Pragma", "no-cache");
				response.setDateHeader("Expires", System.currentTimeMillis());
			}
		}

	
		chain.doFilter(req, res);

	}

	@Override
	public ModelAndView inclui_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		return null;
	}

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void init_imp(final ServletRequest request, final ServletResponse response) throws Exception {

	}

	private boolean isMethod(final ServiceImpl service, final String methodName, final Usuario user) {

		final List<Map<String, Object>> methods = service.getMethods(user);

		for (final Map<String, Object> method : methods) {

			if (method.get(Constants.name).equals(methodName)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void lista_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		((HttpServletResponse) response).addHeader("Content-Type", "text/html");

		final ParseXml ParseSys = getUser(request).getSysParser();
		final Collection<Map<String, String>> info = ParseSys.getListValuesFromXQL(
				"/systems/system[@name='" + getUser(request).getSchema() + "']/module/bean", Constants.name,
				getUser(request), true, true);

		response.getWriter().write("<ul>");
		String beanName;

		for (final Map<String, String> object : info) {
			beanName = object.get(Constants.name).toString();
			response.getWriter().write(
					"<li>" + beanName + "Service (<a href=\"services/" + beanName + "Service.wsdl\">wsdl</a>)</li>");
		}
		response.getWriter().write("</ul>");
		response.flushBuffer();
	}

	@Override
	public ModelAndView obtem_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		final String url = ((HttpServletRequest) request).getRequestURL().toString();
		final String location = url.substring(0, url.lastIndexOf('/'));
		final String beanName = url.substring(url.lastIndexOf('/') + 1, url.length() - 12);

		final Usuario user = getUser(request);

		new Bean2WSDL().create(response.getWriter(), beanName, (GenericService) getService(beanName + "Service", user),
				location, user.getEncode(), user);

		response.flushBuffer();

		return null;
	}

	private void processRest(final HttpServletRequest request, final HttpServletResponse response, final String url)
			throws IOException {

		LogUtil.debug(url, getUser(request));

		final String serviceName = url.replaceAll(".+/services/rest/(.+)\\.xml", "$1");
		final String[] serviceParams = serviceName.split("/");

		final Inflector inflector = Inflector.getInstance();

		final String beanName = inflector.camelCase(serviceParams[0], true, '_');

		final RestProtocol restProtocol = new RestProtocol(beanName, getUser(request).getEncode());

		ServiceImpl service;
		try {
			service = (ServiceImpl) getService(beanName + "Service", getUser(request));

			if (serviceParams.length > 1) {
				String param = inflector.camelCase(serviceParams[1], false, '_');

				if (isMethod(service, param, getUser(request))) {
					restProtocol.setServiceMethod(param);

					if (serviceParams.length > 2) {
						param = serviceParams[2];
					}
				}

				if (param.equals("list")) {
					restProtocol.setReturnList(true);

				} else {
					restProtocol.setId(service.getPrimaryKey().get(Constants.name).toString(), serviceParams[1]);
				}
			}
			processService(request, response, restProtocol, service);

		} catch (final IOException e) {
			throw e;
		} catch (final Exception e) {
			LogUtil.exception(e, getUser(request));
			response.getWriter().print(restProtocol.generateFault(e.getMessage()));
		}
	}

	private void processService(final HttpServletRequest request, final HttpServletResponse response,
			final ServiceProtocol serviceProtocol, final ServiceImpl service) throws IOException {

		String responseContent = null;

		try {
			serviceProtocol.processRequest(request);

			Usuario user = getUser(request);

			final GenericVO vo = service.createVO(user);

			LogUtil.debug("Method: " + serviceProtocol.getMethod(), user);

			switch (serviceProtocol.getMethod()) {
			case START_SESSION:
				user = service.login(serviceProtocol.getLogin(), serviceProtocol.getPassword(), user,
						serviceProtocol.isPasswordHash());
				responseContent = serviceProtocol.generateResponse(user.getSessionId());
				break;
			case END_SESSION:
				if (vo.get("LOGIN") != null) { // TODO springsession
					// usuarios.removerUsuariobyLogin((String) a.get("LOGIN"),
					// user);
					responseContent = serviceProtocol.generateResponse(Constants.true_str);
				} else {
					responseContent = serviceProtocol.generateResponse(Constants.false_str);
				}
				break;
			default:
				// Autentica o usurio.
				final String sessionId = serviceProtocol.getSessionId();
				if (sessionId != null && !sessionId.equals("")) {
					// Esse mtodo lana uma exception caso no encontre
					// GenericVO us = usuarios.getUsuariobySessionId(sessionId,
					// user);
					// user = ((GenericService) getService("UsuarioService",
					// user)).bindUser(user, us, true);
					throw new ServletException("label.login.timeout");
				}
			}

			GenericVO voReturn;

			switch (serviceProtocol.getMethod()) {
			case GET:
				serviceProtocol.populateVO(vo);

				if (serviceProtocol.getServiceMethod() != null) {
					voReturn = (GenericVO) service.invoke(serviceProtocol.getServiceMethod(), vo, user);

				} else {
					voReturn = service.obtem(vo, user);
				}

				if (voReturn == null) {
					voReturn = vo;
				}
				responseContent = serviceProtocol.generateResponse(voReturn);
				break;
			case INCLUDE:
				serviceProtocol.populateVO(vo);
				responseContent = serviceProtocol.generateResponse(service.inclui(vo, user));
				break;
			case UPDATE:
				serviceProtocol.populateVO(vo);
				responseContent = serviceProtocol.generateResponse(service.altera(vo, user));
				break;
			case REMOVE:
				serviceProtocol.populateVO(vo);
				String[] ids = vo.getMultipleId();
				final String primaryKey = (String) service.getPrimaryKey().get(Constants.name);

				if (ids.length == 0) {
					ids = new String[1];
					ids[0] = vo.get(primaryKey).toString();
				}
				vo.setMultipleId(ids);
				vo.setKey(primaryKey);
				responseContent = serviceProtocol.generateResponse(service.remove(vo, user));
				break;
			case LIST:
				serviceProtocol.populateVO(vo);
				List list;

				if (serviceProtocol.getServiceMethod() != null) {
					list = (List) service.invoke(serviceProtocol.getServiceMethod(), vo, user);

				} else {
					list = service.lista(vo, user);
				}
				responseContent = serviceProtocol.generateResponse(list);
				break;
			}

		} catch (final Exception e) {
			LogUtil.exception(e, getUser(request));
			responseContent = serviceProtocol.generateFault(e.getMessage());
		}

		if (responseContent == null) {
			responseContent = serviceProtocol.generateFault("Requisio invlida");
		}
		response.getWriter().print(responseContent);
		LogUtil.debug(responseContent, getUser(request));
	}

	private void processSOAP(final HttpServletRequest request, final HttpServletResponse response, final String url)
			throws IOException {
		// SOAP
		final String beanName = url.substring(url.lastIndexOf("/") + 1, url.length() - 2);

		try {

			final Usuario user = getUser(request);

			final ServiceImpl service = (ServiceImpl) getService(beanName + "Service", user);

			final SoapProtocol soapProtocol = new SoapProtocol(beanName, user.getEncode(), service.getMethods(user));

			processService(request, response, soapProtocol, service);

		} catch (final IOException e) {
			throw e;
		} catch (final Exception e) {
			LogUtil.exception(e, getUser(request));

			final SoapProtocol soapProtocol = new SoapProtocol(beanName, getUser(request).getEncode(), null);

			response.getWriter().print(soapProtocol.generateFault(e.getMessage()));
		}
	}

	private void removeOldCubeFromRequest(HttpServletRequest request, Usuario user) {
		request.removeAttribute("BIQUERY");

		user.getSession().removeAttribute("BIQUERY");
		// ObjectsRepository objRep = (ObjectsRepository)
		// user.getSession().getAttribute("olapObjRep");

		/*
		 * if (objRep != null) { user.getSession().removeAttribute("chart" +
		 * objRep.getIdCubo()); user.getSession().removeAttribute("chartform" +
		 * objRep.getIdCubo()); user.getSession().removeAttribute("table" +
		 * objRep.getIdCubo()); user.getSession().removeAttribute("navi" +
		 * objRep.getIdCubo()); user.getSession().removeAttribute("mdxedit" +
		 * objRep.getIdCubo()); user.getSession().removeAttribute("sortform" +
		 * objRep.getIdCubo()); user.getSession().removeAttribute("print" +
		 * objRep.getIdCubo()); user.getSession().removeAttribute("printform" +
		 * objRep.getIdCubo()); user.getSession().removeAttribute("query" +
		 * objRep.getIdCubo() + ".drillthroughtable");
		 * user.getSession().removeAttribute("query" + objRep.getIdCubo());
		 * user.getSession().removeAttribute("toolbar" + objRep.getIdCubo());
		 * user.getSession().removeAttribute(
		 * "com.tonbeller.wcf.bookmarks.BookmarkManager");
		 * user.getSession().removeAttribute(
		 * "com.tonbeller.wcf.controller.RequestContextFactory");
		 * user.getSession().removeAttribute("com.tonbeller.wcf.utils.XmlUtils")
		 * ; user.getSession().removeAttribute(
		 * "com.tonbeller.wcf.controller.RequestContextFactory"); }
		 */
		// user.getSession().setAttribute("olapObjRep", new
		// ObjectsRepository());
	}




}
