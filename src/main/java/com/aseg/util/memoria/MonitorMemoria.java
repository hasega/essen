package com.aseg.util.memoria;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeSet;

public class MonitorMemoria {
	private static Runtime myruntime = Runtime.getRuntime();
	private static HashMap dadosConsumo = new HashMap();
	private static TreeSet horas = new TreeSet();
	private static MonitorMemoria myself = null;
	private static int MAX_POINTS = 120;

	private MonitorMemoria() {
	}

	public static MonitorMemoria getInstance() {
		if (myself == null) {
			myself = new MonitorMemoria();
			return myself;
		} else {
			return myself;
		}
	}

	public static void atualizaDados() {
		Long memUsed = new Long(myruntime.totalMemory() - myruntime.freeMemory());
		Long memFree = new Long(myruntime.freeMemory());
		Long memMax = new Long(myruntime.totalMemory());
		ArrayList tupla = new ArrayList();
		Date tempo = Calendar.getInstance().getTime();
		tupla.add(memFree);
		tupla.add(memUsed);
		tupla.add(memMax);
		dadosConsumo.put(tempo, tupla);
		horas.add(tempo);
		if (dadosConsumo.keySet().size() > MAX_POINTS) {
			Date pos = (Date) horas.first();
			dadosConsumo.remove(pos);
			horas.remove(pos);
		}
	}

	public static HashMap getDados() {
		return dadosConsumo;
	}

	public static Long getMaxMem() {
		return new Long(myruntime.totalMemory());
	}
}