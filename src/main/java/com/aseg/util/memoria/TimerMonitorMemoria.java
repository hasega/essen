package com.aseg.util.memoria;

import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class TimerMonitorMemoria extends HttpServlet {
	private static Timer clock = new Timer(true);
	private int minutos = 1;
	TimerMonitorMemoria sitting = null;

	private TimerTask getTask() {
		return new TimerTask() {
			public void run() {

				// MonitorMemoria.atualizaDados();

			}
		};
	}

	public TimerMonitorMemoria() {

	}

	public void destroy() {
		System.err.println("Timer Memria    [FINALIZADO]");
	}

	public void init() throws ServletException {
		System.err.println("Timer Memria                                    [INICIALIZADO]");
		clock.schedule(getTask(), 0, minutos * 60000);
	}
}