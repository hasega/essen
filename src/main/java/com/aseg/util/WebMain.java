package com.aseg.util;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.util.factory.SpringFactory;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;

@Configuration
public class WebMain extends SpringBootServletInitializer implements ServletContextListener {
	public final UtilServiceImpl util = new UtilServiceImpl();
	private static Map<String, HttpSession> sessions = new HashMap<String, HttpSession>();
	public static boolean inicializado;
	@Autowired
	private ApplicationContext context;

	@Bean
	public ServletContextInitializer myInitializer() {
		return new ServletContextInitializer() {

			@Override
			public void onStartup(ServletContext servletContext) throws ServletException {
				System.out.println("Startup of " + servletContext);
				servletContext.addListener(new org.apache.commons.fileupload.servlet.FileCleanerCleanup());

			}
		};

	}

	@Override
	public void contextDestroyed(final ServletContextEvent sce) {
		LogUtil.error("#######################################");
		LogUtil.error(">> Essen finish <<");
		LogUtil.error("#######################################");
	}

	/**
	 * 
	 * @param sce
	 */

	@Override
	public void contextInitialized(final ServletContextEvent sce) {
		final ServletContext appContext = sce.getServletContext();
		SpringFactory.setContext(WebApplicationContextUtils.getRequiredWebApplicationContext(appContext));
		// SpringFactory.setServletContext(appContext);
		Locale.setDefault(util.getLocale());
		TimeZone.setDefault(TimeZone.getTimeZone("America/Sao_Paulo"));
		LogUtil.notify("**************************************");
		LogUtil.notify("**     Essen Booting     **");
		LogUtil.notify("**       " + util.formatDate(new Date(), "dd/MM/yyyy hh:mm:ss") + "        **");
		LogUtil.notify("**************************************");

	}
//
//	@Override
//	public void sessionCreated(HttpSessionEvent e) {
//		synchronized (sessions) {
//			List a = Arrays.asList(sessions.keySet().toArray());
//			for (Iterator iterator = a.iterator(); iterator.hasNext();) {
//				String object = (String) iterator.next();
//				try {
//					HttpSession session = sessions.get(object);
//					Usuario user = (Usuario) session.getAttribute(Constants.RAIN_USER);
//
//					if (!session.isNew() || user != null) {
//						if (user != null && user.getLogin() == null) {
//							sessions.remove(object);
//							session.invalidate();
//						}
//
//					}
//				} catch (Exception e2) {
//				}
//			}
//
//		}
//
//	}

//	@Override
//	public void sessionDestroyed(HttpSessionEvent e) {
//		Usuario user = (Usuario) e.getSession().getAttribute(Constants.RAIN_USER);
//		GenericService sessoes = (GenericService) user.getFactory().getSpringBean("SessoesService", user);
//		if (sessoes == null)
//			return;
//		GenericVO v = sessoes.createVO(user);
//
//		v.putKey("ID_SESSAO", e.getSession().getId());
//		synchronized (sessions) {
//			sessions.remove(user.getSystem() + "-" + user.getLogin() + "-" + e.getSession().getId());
//		}
//		try {
//			sessoes.remove(v, user);
//		} catch (Exception e1) {
//			LogUtil.exception(e1, user);
//		}
//	}

//	public static void clearSessions(String login, String sessionID) {
//		synchronized (sessions) {
//			List a = Arrays.asList(sessions.keySet().toArray());
//			for (Iterator iterator = a.iterator(); iterator.hasNext();) {
//				String object = (String) iterator.next();
//				HttpSession session = sessions.get(object);
//
//				try {
//					if (object.indexOf("-" + login + "-") > -1 && !object.endsWith(sessionID)) {
//
//						sessions.remove(object);
//						session.invalidate();
//
//					}
//				} catch (Exception e2) {
//					// TODO: handle exception
//					e2.printStackTrace();
//				}
//
//			}
//
//		}
//
//	}

//	public static void putSession(String system, String login, String sessionId, HttpSession session) {
//		sessions.put(system + "-" + login + "-" + sessionId, session);
//
//	}

//	public static HttpSession getSession(String system, String login, String sessionId) {
//		return sessions.get(system + "-" + login + "-" + sessionId);
//	}

	public static Usuario getUserbyLogin(String login, String system) {
		Map t = new HashMap(sessions);
		Iterator i = t.keySet().iterator();
		while (i.hasNext()) {
			String object = (String) i.next();
			if (object.indexOf(system + "-" + login) > -1)
				return (Usuario) ((HttpSession) t.get(object)).getAttribute(Constants.RAIN_USER);
		}

		return null;
	}
}