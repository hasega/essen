package com.aseg.util.apresentacao;

import java.util.Iterator;
import java.util.LinkedHashMap;

import com.aseg.logauditoria.service.LogUtil;
import com.aseg.util.service.UtilServiceImpl;

public class JSONBuilder extends LinkedHashMap implements Cloneable {

	final String START_KEY = "**_**";
	final String END_KEY = "__*__";
	final String KEY = "*_*_*";
	final String TAB = " \n";

	JSONBuilder master = null;
	private boolean text = false;

	public JSONBuilder addJson(final String string) {

		final JSONBuilder n = new JSONBuilder();
		n.Json();
		n.master = this;
		this.put(string, n);

		return n;
	}

	public JSONBuilder addList(final String string) {

		this.put(START_KEY + this.size(), string + ":[ " + TAB);
		return this;

	}

	public JSONBuilder addProperty(final String key, final String value) {

		this.put(key, "\"" + value + "\"");
		return this;

	}

	public JSONBuilder addPropertyType(final String key, final Object value) {
		this.put(key, value);

		return this;

	}

	public JSONBuilder addValue(final String string) {

		this.put(KEY + this.size(), "\"" + string.trim() + "\"");

		return this;

	}

	public JSONBuilder addValueType(final String string) {

		this.put(KEY + this.size(), string.trim());
		return this;

	}

	@Override
	public Object clone() {
		final JSONBuilder c = new JSONBuilder();
		c.putAll(this);
		c.text = this.text;
		if (this.master != null) {
			c.master = (JSONBuilder) this.master.clone();
		}

		return c;

	}

	public JSONBuilder closeJson() {
		if (this.master == null) {
			return this;
		} else {
			return this.master;
			// this.put(END_KEY + this.size(), "}\n");
			// return this;
			// } else {
			// JSONBuilder master = null;
			// try {
			// // this.master.this.put(END_KEY + this.size(), "}\n");
			// master = (JSONBuilder) this.master.clone();
			// // this.master = null;
			// return master;
			// } catch (Exception e) {
			//
			// return this;
			// }
			//
			// }
		}

	}

	public JSONBuilder closeList() {

		this.put(END_KEY + this.size(), "]" + TAB);
		return this;

	}

	public String getCleanProperty(final String p) {
		return this.get(p).toString().replaceAll("'", "").replaceAll("\"", "");
	}

	public String getProperty(final String p) {
		return this.get(p).toString().replaceAll("'", "");
	}

	public Object getPropertyType(final String p) {
		return this.get(p);
	}

	public String instance(final String string, final String additionalParams) {

		if (additionalParams == null) {
			return " new " + string + "(" + this.toString() + ")";
		} else {
			return " new " + string + "(" + this.toString() + ", " + additionalParams + ")";
		}
	}

	public boolean isText() {
		return text;
	}

	public JSONBuilder Json() {
		this.put(START_KEY + this.size(), " {");

		return this;
	}

	public JSONBuilder List() {
		this.put(START_KEY + this.size(), " [ " + TAB);
		return this;

	}

	public JSONBuilder newJson() {

		final JSONBuilder n = new JSONBuilder();
		n.Json();
		n.master = this;
		this.put(n, "");

		return n;
	}

	public JSONBuilder removeProperty(final String key) {
		if (this.containsKey(key)) {
			this.remove(key);
		}
		return this;
	}

	public JSONBuilder renameProperty(final String string, final String string2) {

		this.put(string2, this.get(string));
		this.remove(string);

		return this;
	}

	public void setIsText() {

		text = true;
	}

	public String toList() {

		return "[" + this.toString() + "]";
	}

	@Override
	public String toString() {

		StringBuilder s = new StringBuilder();

		final Iterator i = this.keySet().iterator();
		while (i.hasNext()) {
			final Object type = i.next();
			if (type instanceof JSONBuilder) {
				s.append(type + " ," + TAB);
			} else if (type.toString().startsWith(START_KEY)) {
				s.append(this.get(type) + TAB);
			} else if (type.toString().startsWith(END_KEY)) {
				if (s.indexOf(",") > -1) {
					s = s.deleteCharAt(s.lastIndexOf(","));
				}
				s.append(this.get(type) + "," + TAB);
			} else if (type.toString().startsWith(KEY)) {
				s.append(this.get(type) + ",");
			} else {
				try {
					s.append("" + type + "" + ":" + this.get(type) + "," + TAB);
				} catch (final Exception e) {
					LogUtil.exception(e, null);
				}
			}
		}
		if (s.length() == 0) {
			return "";
		}
		if (s.lastIndexOf(",") > -1) {
			s = s.deleteCharAt(s.lastIndexOf(","));
		}

		// INSERIDO PARA SUBSTITUIR ^ por \" no client ..

		String res = new UtilServiceImpl().replace(s.toString(), "^", "\\\"");
		if (text) {
			return res;
		}
		if (s.toString().trim().startsWith("{")) {
			return res + "}";
		} else {

			return "{" + res + "}";
		}

	}

}
