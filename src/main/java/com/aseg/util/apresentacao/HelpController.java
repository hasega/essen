package com.aseg.util.apresentacao;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.aseg.config.Constants;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.util.service.ParseXml;

public class HelpController extends BaseController {

	@Override
	public ModelAndView altera_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		return null;
	}

	public ModelAndView getHelp(final ServletRequest request, final ServletResponse response)
			throws IOException, ServletException {

		try {

			final String id = (String) getParameter(request, "id", false);
			final String label = (String) getParameter(request, "label", false);
			final String caminho = (String) getParameter(request, "caminho", false);

			final String[] idx = id.split("_");

			// verifica se o nodo possui propriedades e cria o array pra popular
			// o grid de propriedades
			final List<HashMap<String, Object>> listaProp = retornaPropriedadesHelp(request, idx[0], false);

			final StringBuilder sbProp = new StringBuilder();

			sbProp.append("[");
			if (listaProp != null && listaProp.size() > 0) {

				String aux = "";
				for (final Map<String, Object> m : listaProp) {
					final List<HashMap<String, Object>> listaP = (List<HashMap<String, Object>>) m.get("children");
					for (final Map<String, Object> mm : listaP) {

						String desc = getUser(request).getDescricao((String) mm.get("label"));
						if (desc == null) {
							desc = "";
						}
						aux = aux + ",[ '" + mm.get("id") + "', '" + mm.get("text") + "', '" + desc + "']";
					}
				}

				sbProp.append(aux.substring(1));
			}

			sbProp.append("]");

			// verifica se o nodo possui modulos e cria o array pra popular o
			// grid de modulos
			final List<HashMap<String, Object>> listaMod = retornaModulosHelp(request, idx[0], false);

			final StringBuilder sbmod = new StringBuilder();

			sbmod.append("[");
			if (listaMod != null && listaMod.size() > 0) {

				String aux = "";
				for (final Map<String, Object> m : listaMod) {

					final List<HashMap<String, Object>> listaM = (List<HashMap<String, Object>>) m.get("children");
					for (final Map<String, Object> mm : listaM) {

						String desc = getUser(request).getDescricao((String) mm.get("label"));

						if (desc == null) {
							desc = "";
						}

						aux = aux + ",[ '" + mm.get("id") + "', '" + mm.get("text") + "', '" + desc + "']";
					}
				}

				sbmod.append(aux.substring(1));
			}

			sbmod.append("]");

			final String l = getUser(request).getLabel(label);
			final String desc = getUser(request).getDescricao(label);
			final String help = getUser(request).getHelp(label);

			setReqAttr(request,"CAMINHO", caminho);
			setReqAttr(request,"LABEL", l);
			setReqAttr(request,"DESC", desc.replace("\n", "<br/>"));
			setReqAttr(request,"HELP", help.replace("\n", "<br/>"));
			setReqAttr(request,"ID", id);
			setReqAttr(request,"ARRAY_PROP", sbProp.toString());
			setReqAttr(request,"ARRAY_MOD", sbmod.toString());

		} catch (final Exception ex) {
			LogUtil.exception(ex, getUser(request));

			final PrintWriter out = response.getWriter();
			final StringWriter sw = new StringWriter();
			ex.printStackTrace(new PrintWriter(sw));
			final String suaExeption = sw.getBuffer().toString();
			sw.close();
			out.print(suaExeption);
			out.close();

			LogUtil.debug("Erro AJAX: " + ex.getMessage().toString(), getUser(request));
		}

		return new ModelAndView("help_content");
	}

	public ModelAndView getNodoArvoreHelp(final ServletRequest request, final ServletResponse response)
			throws Exception {

		try {
			final String check = (String) getParameter(request, "check", false);

			boolean is_check = false;
			if (check != null && check.equals(Constants.true_str)) {
				is_check = true;
			}

			final List<HashMap<String, Object>> lista = getNodoHelp(request, null, is_check);

			final String tree = util.JSONserialize(lista);

			final String sbAux = "{success: true, tree: " + tree + "}";

			// writeJson(request, response, sbAux);

		} catch (final Exception e) {
			LogUtil.exception(e, getUser(request));
		}
		return null;
	}

	public List<HashMap<String, Object>> getNodoHelp(final ServletRequest request, final HashMap<String, Object> nodo,
			final boolean is_check) {

		try {

			String query = null;

			List<Map<String, String>> c = new ArrayList<Map<String, String>>();

			final List<HashMap<String, Object>> nodos = new ArrayList<HashMap<String, Object>>();

			final Usuario user = getUser(request);
			// cria os nodos master
			if (nodo == null) {
				query = "/systems/system[@name='" + user.getSchema() + "']/module/bean[@type='M' and @master='SI']";
			} else {

				if (nodo.get("query") != null) {
					query = (String) nodo.get("query");
				} else {
					query = "/systems/system[@name='" + user.getSchema() + "']/module/bean[@type='M' and @master='"
							+ (String) nodo.get("id") + "']";
				}

			}

			c = (List) ((ParseXml) user.getSysParser()).getListValuesFromXQL(query, null, user, true, true);

			final LinkedHashSet<String> agrupados = new LinkedHashSet<String>();

			for (final Map<String, String> map : c) {
				final String id = map.get("id");

				final HashMap<String, Object> map_nodo = new HashMap<String, Object>();

				map_nodo.put("id", map.get("id"));
				map_nodo.put("master", map.get("master"));
				map_nodo.put("group", map.get("group"));
				map_nodo.put("query", map.get("query"));
				map_nodo.put("help", true);
				map_nodo.put("text", getUser(request).getLabel(map.get("label")));
				map_nodo.put("label", map.get("label"));
				map_nodo.put("type", "PROP");

				if (getUser(request).getIcon(map.get("label")) != null) {
					map_nodo.put("icon",
							"ItemLinguagemAction.do?" + Constants.ActionParam + "=download&amp;NOME_CONFIG_SISTEMA="
									+ getUser(request).getLang() + Constants.CACHE_SEPARATOR + map.get("label")
									+ "&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
				} else {
					map_nodo.put("icon", "images/ic_diretorio.gif");
				}

				final List<HashMap<String, Object>> filhos = new ArrayList<HashMap<String, Object>>();

				if (map.get("query") == null) {
					filhos.addAll(retornaModulosHelp(request, id, is_check));
					filhos.addAll(retornaPropriedadesHelp(request, id, is_check));

				}
				if (is_check) {
					filhos.addAll(retornaMetodosHelp(request, id, map.get("table"), is_check));
					filhos.addAll(retornaCrudHelp(request, id, map.get("table"), is_check, "BEAN"));

				}

				filhos.addAll(getNodoHelp(request, map_nodo, is_check));

				if (filhos != null && filhos.size() > 0) {
					map_nodo.put("children", (filhos != null && filhos.size() > 0 ? filhos : null));
				}
				map_nodo.put("tem_filho",
						(filhos != null && filhos.size() > 0 ? Constants.true_str : Constants.false_str));
				map_nodo.put("leaf", (filhos != null && filhos.size() > 0 ? false : true));

				nodos.add(map_nodo);

				if (nodo != null && nodo.get("group") != null) {
					agrupados.add(map.get(nodo.get("group")));
				}
			}

			if (nodo != null && nodo.get("group") != null) {

				return this.retornaAgrupadosHelp(request, agrupados, (String) nodo.get("group"), nodos, is_check);

			}

			return nodos;

		} catch (final Exception e) {
			LogUtil.exception(e, getUser(request));
		}
		return null;

	}

	@Override
	public ModelAndView inclui_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		return null;
	}

	@Override
	public void init_imp(final ServletRequest request, final ServletResponse response) throws Exception {

	}

	@Override
	public void lista_imp(final ServletRequest request, final ServletResponse response) throws Exception {

	}

	@Override
	public ModelAndView obtem_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		return null;
	}

	public List<HashMap<String, Object>> retornaAgrupadosHelp(final ServletRequest request,
			final LinkedHashSet<String> agrupados, final String grupo, final List<HashMap<String, Object>> lista,
			final boolean is_check) {

		final Usuario user = getUser(request);

		final List<HashMap<String, Object>> nova_lista = new ArrayList<HashMap<String, Object>>();
		for (final String id : agrupados) {

			// retorna o bean com id
			final List<Map<String, String>> c_pai = (List) ((ParseXml) user.getSysParser()).getListValuesFromXQL(
					"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + id + "']", null, user, true,
					true);

			final HashMap<String, Object> map_nodo = new HashMap<String, Object>();

			if (c_pai != null && c_pai.size() > 0) {

				map_nodo.put("id", c_pai.get(0).get("id") + "_QUERY");
				map_nodo.put("master", c_pai.get(0).get("master"));
				map_nodo.put("group", c_pai.get(0).get("group"));
				map_nodo.put("query", c_pai.get(0).get("query"));
				map_nodo.put("help", true);
				map_nodo.put("text", getUser(request).getLabel(c_pai.get(0).get("label")));
				map_nodo.put("label", c_pai.get(0).get("label"));
				map_nodo.put("type", "PROP");

				if (is_check) {
					// map_nodo.put("checked", true);
				}

				if (getUser(request).getIcon((String) map_nodo.get("label")) != null) {
					map_nodo.put("icon",
							"ItemLinguagemAction.do?" + Constants.ActionParam + "=download&amp;NOME_CONFIG_SISTEMA="
									+ getUser(request).getLang() + Constants.CACHE_SEPARATOR + map_nodo.get("label")
									+ "&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
				} else {
					map_nodo.put("icon", "images/ic_diretorio.gif");
				}

				final List<HashMap<String, Object>> filhos = util.match(lista, new String[] { grupo },
						new String[] { c_pai.get(0).get("id") });

				map_nodo.put("children", (filhos != null && filhos.size() > 0 ? filhos : null));
				map_nodo.put("tem_filho",
						(filhos != null && filhos.size() > 0 ? Constants.true_str : Constants.false_str));
				map_nodo.put("leaf", (filhos != null && filhos.size() > 0 ? false : true));

				nova_lista.add(map_nodo);

			}

		}

		return nova_lista;
	}

	public List<HashMap<String, Object>> retornaCrudHelp(final ServletRequest request, final String id,
			final String table, final boolean is_check, final String tipo) {

		final List<HashMap<String, Object>> lista = new ArrayList<HashMap<String, Object>>();

		HashMap<String, Object> map = new HashMap<String, Object>();

		if (tipo != null && (tipo.equals("PROP") || tipo.equals("BEAN"))) {
			map.put("id", table + "label.inclui");
			map.put("master", "");
			map.put("group", "");
			map.put("query", "");
			map.put("help", true);
			map.put("text", getUser(request).getLabel("label.inclui"));
			map.put("label", "label.inclui");
			map.put("type", "CRUD");
			map.put("tem_filho", Constants.false_str);
			map.put("leaf", true);
			map.put("checked", false);

			lista.add(map);

		}

		if (tipo != null && (tipo.equals("PROP") || tipo.equals("BEAN"))) {
			map = new HashMap<String, Object>();

			map.put("id", table + "label.altera");
			map.put("master", "");
			map.put("group", "");
			map.put("query", "");
			map.put("help", true);
			map.put("text", getUser(request).getLabel("label.altera"));
			map.put("label", "label.altera");
			map.put("type", "CRUD");
			map.put("tem_filho", Constants.false_str);
			map.put("leaf", true);
			map.put("checked", false);

			lista.add(map);
		}

		if (tipo != null && (tipo.equals("PROP") || tipo.equals("BEAN"))) {

			map = new HashMap<String, Object>();

			map.put("id", table + "label.ver");
			map.put("master", "");
			map.put("group", "");
			map.put("query", "");
			map.put("help", true);
			map.put("text", getUser(request).getLabel("label.ver"));
			map.put("label", "label.ver");
			map.put("type", "CRUD");
			map.put("tem_filho", Constants.false_str);
			map.put("leaf", true);
			map.put("checked", false);

			lista.add(map);
		}

		if (tipo != null && tipo.equals("BEAN")) {

			map = new HashMap<String, Object>();

			map.put("id", table + "label.listar");
			map.put("master", "");
			map.put("group", "");
			map.put("query", "");
			map.put("help", true);
			map.put("text", getUser(request).getLabel("label.listar"));
			map.put("label", "label.listar");
			map.put("type", "CRUD");
			map.put("tem_filho", Constants.false_str);
			map.put("leaf", true);
			map.put("checked", false);

			lista.add(map);
		}

		if (tipo != null && tipo.equals("BEAN")) {

			map = new HashMap<String, Object>();

			map.put("id", table + "label.remove");
			map.put("master", "");
			map.put("group", "");
			map.put("query", "");
			map.put("help", true);
			map.put("text", getUser(request).getLabel("label.remove"));
			map.put("label", "label.remove");
			map.put("type", "CRUD");
			map.put("tem_filho", Constants.false_str);
			map.put("leaf", true);
			map.put("checked", false);

			lista.add(map);
		}

		return lista;
	}

	public List<HashMap<String, Object>> retornaMetodosHelp(final ServletRequest request, final String id,
			final String table, final boolean is_check) {
		final String query = "/systems/system[@name='" + getUser(request).getSchema()
				+ "']/module/bean[@type!='A' and @master='" + id + "']";

		final Usuario user = getUser(request);

		// retorna as properties do bean
		final List<Map<String, String>> c = (List) ((ParseXml) user.getSysParser()).getListValuesFromXQL(
				"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + id + "']/method[@layers!='S']",
				null, user, true, true);

		final List<HashMap<String, Object>> lista = new ArrayList<HashMap<String, Object>>();

		for (final Map<String, String> mod : c) {
			final HashMap<String, Object> map_nodo = new HashMap<String, Object>();
			map_nodo.put("id", table + "." + mod.get("label"));
			map_nodo.put("master", "");
			map_nodo.put("group", "");
			map_nodo.put("query", "");
			map_nodo.put("help", false);
			map_nodo.put("text", getUser(request).getLabel(mod.get("label")));
			map_nodo.put("label", mod.get("label"));
			map_nodo.put("type", "MET");
			map_nodo.put("checked", false);

			// if(getUser(request).getIcon("label.module")!=null) {
			// map_nodo.put("icon",
			// "ItemLinguagemAction.do?"+Constants.ActionParam+"=download&amp;NOME_CONFIG_SISTEMA="+getUser(request).getLang()+Constants.CACHE_SEPARATOR+"label.module&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
			// } else {
			// map_nodo.put("icon", "images/ic_diretorio.gif");
			// }

			map_nodo.put("tem_filho", false);
			map_nodo.put("leaf", true);

			lista.add(map_nodo);

		}

		return lista;
	}

	public List<HashMap<String, Object>> retornaModulosHelp(final ServletRequest request, final String id,
			final boolean is_check) {

		final String query = "/systems/system[@name='" + getUser(request).getSchema()
				+ "']/module/bean[@type!='A' and @master='" + id + "']";

		final Usuario user = getUser(request);

		// retorna os modulos do bean
		final List<Map<String, String>> c = (List) ((ParseXml) user.getSysParser()).getListValuesFromXQL(query, null,
				user, true, true);

		final List<HashMap<String, Object>> lista = new ArrayList<HashMap<String, Object>>();
		final HashMap<String, Object> map_nodo = new HashMap<String, Object>();

		if (c != null && c.size() > 0) {

			final List<HashMap<String, Object>> lista_mod = new ArrayList<HashMap<String, Object>>();
			for (final Map<String, String> mod : c) {
				final HashMap<String, Object> map_mod = new HashMap<String, Object>();

				map_mod.put("id", mod.get("id") + "_MOD_" + id);
				map_mod.put("master", "");
				map_mod.put("group", "");
				map_mod.put("query", "");
				map_mod.put("help", true);
				map_mod.put("text", getUser(request).getLabel(mod.get("label")));
				map_mod.put("label", mod.get("label"));
				map_mod.put("type", "MOD");

				if (getUser(request).getIcon((String) map_mod.get("label")) != null) {
					map_nodo.put("icon",
							"ItemLinguagemAction.do?" + Constants.ActionParam + "=download&amp;NOME_CONFIG_SISTEMA="
									+ getUser(request).getLang() + Constants.CACHE_SEPARATOR + map_mod.get("label")
									+ "&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
				} else {
					map_nodo.put("icon", "images/ic_diretorio.gif");
				}

				LogUtil.debug("->   " + mod.get("id") + " : " + id, user);
				final List<HashMap<String, Object>> filhos = new ArrayList<HashMap<String, Object>>();

				filhos.addAll(retornaPropriedadesHelp(request, mod.get("id"), is_check));
				filhos.addAll(retornaMetodosHelp(request, mod.get("id"), mod.get("table"), is_check));
				filhos.addAll(retornaCrudHelp(request, mod.get("id"), mod.get("table"), is_check, "bean"));

				if (filhos != null && filhos.size() > 0) {
					map_mod.put("children", (filhos != null && filhos.size() > 0 ? filhos : null));
				}
				map_mod.put("tem_filho",
						(filhos != null && filhos.size() > 0 ? Constants.true_str : Constants.false_str));
				map_mod.put("leaf", (filhos != null && filhos.size() > 0 ? false : true));

				lista_mod.add(map_mod);

			}

			map_nodo.put("id", id + "_MOD");
			map_nodo.put("master", "");
			map_nodo.put("group", "");
			map_nodo.put("query", "");
			map_nodo.put("help", false);
			map_nodo.put("text", getUser(request).getLabel("label.module"));
			map_nodo.put("label", "label.module");
			map_nodo.put("type", "MOD");

			if (getUser(request).getIcon("label.module") != null) {
				map_nodo.put("icon",
						"ItemLinguagemAction.do?" + Constants.ActionParam + "=download&amp;NOME_CONFIG_SISTEMA="
								+ getUser(request).getLang() + Constants.CACHE_SEPARATOR
								+ "label.module&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
			} else {
				map_nodo.put("icon", "images/ic_diretorio.gif");
			}

			map_nodo.put("children", (lista_mod != null && lista_mod.size() > 0 ? lista_mod : null));
			map_nodo.put("tem_filho",
					(lista_mod != null && lista_mod.size() > 0 ? Constants.true_str : Constants.false_str));
			map_nodo.put("leaf", (lista_mod != null && lista_mod.size() > 0 ? false : true));

			lista.add(map_nodo);

		}

		return lista;

	}

	public List<HashMap<String, Object>> retornaPropriedadesHelp(final ServletRequest request, final String id,
			final boolean is_check) {

		final Usuario user = getUser(request);

		// retorna as properties do bean
		final List<Map<String, String>> c = util.match((List) ((ParseXml) user.getSysParser()).getListValuesFromXQL(
				"/systems/system[@name='" + user.getSchema() + "']/module/bean[@id='" + id + "']/property", null, user,
				true, true), "primaryKey", null);

		final List<HashMap<String, Object>> lista = new ArrayList<HashMap<String, Object>>();

		final HashMap<String, Object> map_nodo = new HashMap<String, Object>();
		if (c != null && c.size() > 0) {

			final List<HashMap<String, Object>> lista_prop = new ArrayList<HashMap<String, Object>>();
			for (final Map<String, String> prop : c) {
				final HashMap<String, Object> map_prop = new HashMap<String, Object>();

				map_prop.put("id", prop.get(Constants.name) + "_PROP_" + id);
				map_prop.put("master", "");
				map_prop.put("group", "");
				map_prop.put("query", "");
				map_prop.put("help", true);
				map_prop.put("text", getUser(request).getLabel(prop.get("label")));
				map_prop.put("label", prop.get("label"));
				map_prop.put("type", "PROP");

				if (is_check) {
					final List<HashMap<String, Object>> lista_crud = retornaCrudHelp(request, "", prop.get("table"),
							true, "PROP");
					map_prop.put("children", (lista_crud != null && lista_crud.size() > 0 ? lista_crud : null));

					map_prop.put("tem_filho", Constants.true_str);
					map_prop.put("leaf", false);
				} else {

					map_prop.put("tem_filho", Constants.false_str);
					map_prop.put("leaf", true);
				}

				if (getUser(request).getIcon((String) map_prop.get("label")) != null) {
					map_nodo.put("icon",
							"ItemLinguagemAction.do?" + Constants.ActionParam + "=download&amp;NOME_CONFIG_SISTEMA="
									+ getUser(request).getLang() + Constants.CACHE_SEPARATOR + map_prop.get("label")
									+ "&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
				} else {
					map_nodo.put("icon", "images/ic_diretorio.gif");
				}

				if (prop.get("type") != null && !prop.get("type").toString().contains("Fieldset")) {
					lista_prop.add(map_prop);
				}

			}

			map_nodo.put("id", id + "_PROP");
			map_nodo.put("master", "");
			map_nodo.put("group", "");
			map_nodo.put("query", "");
			map_nodo.put("help", false);
			map_nodo.put("text", getUser(request).getLabel("label.property"));
			map_nodo.put("label", "label.property");
			map_nodo.put("type", "PROP");

			if (getUser(request).getIcon("label.property") != null) {
				map_nodo.put("icon",
						"ItemLinguagemAction.do?" + Constants.ActionParam + "=download&amp;NOME_CONFIG_SISTEMA="
								+ getUser(request).getLang() + Constants.CACHE_SEPARATOR
								+ "label.property&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache");
			} else {
				map_nodo.put("icon", "images/ic_diretorio.gif");
			}

			map_nodo.put("children", (lista_prop != null && lista_prop.size() > 0 ? lista_prop : null));
			map_nodo.put("tem_filho",
					(lista_prop != null && lista_prop.size() > 0 ? Constants.true_str : Constants.false_str));
			map_nodo.put("leaf", (lista_prop != null && lista_prop.size() > 0 ? false : true));

			lista.add(map_nodo);

		}

		return lista;
	}

}
