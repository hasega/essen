package com.aseg.util.apresentacao;


import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.*;

import javax.script.ScriptException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.aseg.config.Constants;
import com.aseg.dao.DAOComparator;
import com.aseg.exceptions.BusinessException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.util.ConnectionManager;
import com.aseg.util.service.SetupService;
import com.aseg.vo.GenericVO;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.script.ScriptException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.*;
@Controller
@RequestMapping("/SetupAction.do")
public class SetupController extends BaseController {

    static Usuario setupUser = new Usuario();

	private static enum STEP {
		// CONNECTION("setupConnection")
		CHOSECONNECTION("choseConnection"), VALIDATION("validateSystem"), DATABASE("setupDataBase"), USER(
				"setupUser"), LANGUAGE(
						"setupLanguage"), CREATE_CONNECTION("createConnection"), CLONE_DATABASE("cloneDatabase");

		private final String method;

		STEP(final String method) {
			this.method = method;
		}

		public String getMethod() {
			return this.method;
		}
	}

	private static final String STEP_REQUIRED = "STEP_REQUIRED";

	private void addApplyButton(final ServletRequest request, final STEP currentStep) {
		addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.aplicar", currentStep.getMethod(),
				"functionvalidation.validate", BUTTON.STATIC, null, null, false, request);
	}

	private void disableSetup(ServletRequest request, Usuario user) throws Exception {
		try {

			getSetupService().disableSetup(getSetupUser());

			final Map<String, String> field = new HashMap<String, String>();
			String mensagem = user.getLabel("label.setup.disable.setup.sucesso");
			field.put(Constants.name, "MSG");
			field.put("MSG", mensagem);
			field.put("value", mensagem);
			field.put("type", "alfa");
			field.put(Constants.component, "message");
			field.put("preview", "label");
			field.put("colspan", "4");
			field.put("volatile", Constants.true_str);
			field.put("header", "0;4");
			addCampo(request, field);
		} catch (Exception e) {
			LogUtil.exception(e, user);
			throw new BusinessException("label.setup.disable.setup.error");
		}
	}

	private void addStepButtons(final ServletRequest request, final STEP currentStep, final boolean deepStep) {

		final STEP[] steps = STEP.values();
		String backStep = null, nextStep = null;
		final int index = currentStep.ordinal();

		// Se no for o ltimo
		if (index != steps.length - 1) {
			nextStep = steps[index + 1].getMethod();
		}

		// Se no for o primeiro
		if (index != 0) {
			final int backStepIndex = deepStep ? index : index - 1;
			backStep = steps[backStepIndex].getMethod();
		}

		if (backStep != null) {
			addButton(Constants.MAIN_ACTION, "", BUTTON.METHOD, "label.voltar", "javascript:" + getLoadPageJS(backStep),
					"", BUTTON.STATIC, null, null, false, request);
		}

		if (nextStep != null && stepIsRequired(request)) {
			addButton(Constants.MAIN_ACTION, "", BUTTON.METHOD, "label.pular", "javascript:" + getLoadPageJS(nextStep),
					"", BUTTON.STATIC, null, null, false, request);
		}

		addButton(Constants.MAIN_ACTION, "", BUTTON.CANCEL, "label.concluir", "", "", BUTTON.STATIC, null, null, false,
				request);

	}

	private boolean stepIsRequired(final ServletRequest request) {
		return request.getAttribute(STEP_REQUIRED) == null;
	}

	@Override
	public ModelAndView altera_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		if (process(request)) {
			service.altera(getVO(request), getUser(request));
		} else {

			getVO(request).putAll(service.obtem(getVO(request), getUser(request)));
		}
		return finish(request, response, null);
	}

	public ModelAndView refreshProblems(final ServletRequest request, final ServletResponse response) throws Exception {
		getSetupService().initSetup(getSetupUser());

		final String extra = "loadPage(\"Setup" + Constants.ACTION_BASE + Constants.ACTION_SUFIX + "?"
				+ Constants.ActionParam + "=" + STEP.DATABASE.getMethod() + "&system=" + getUser(request).getSystem()
				+ "&session=false&CHOICE=" + SetupService.DataBaseOptions.LIST_PROBLEMS.name()
				+ "\",\"window_modal\",\"null\",\"\",true,null,null)";

		return finish(request, response, extra);

	}

	public ModelAndView corrigirGrupoErro(final ServletRequest request, final ServletResponse response)
			throws Exception {

		clearFields(request);
		clearButtons(request);
		setMainField("TABLE");
		setReqAttr(request,Constants.WINDOW_SIZE, "{w:500,h:270}");

		final GenericVO vo = getVO(request);

		final SetupService service = getSetupService();

		final int id = Integer.parseInt(vo.get("ID").toString());

		final Map<String, Object> problem = service.getDataBaseProblem(id, getSetupUser());

		final boolean onlyLogDDL = !vo.get("EXECUTE_DLL").equals("T");
		service.amendDatabase(problem.get("ERROR_CODE").toString(), onlyLogDDL, getSetupUser());

		return finish(request, response, null);
	}

	public ModelAndView lista_start(final ServletRequest request, final ServletResponse response) throws Exception {
		setReqAttr(request,Constants.MAIN_ACTION, true);
		return lista(request, response);
	}

	private void createChoiceField(final ServletRequest request, final String label) {

		createChoiceField(request, label, "Setup", "getTrueOrFalse", "CHOICE");
		getVO(request).put("CHOICE", "T");
	}

	private void createChoiceField(final ServletRequest request, final String label, final String serviceName,
			final String methodName, final String fieldName) {

		final Map<String, String> field = new HashMap<String, String>();
		field.put(Constants.name, fieldName);
		field.put("label", label);
		field.put("type", "alfa");
		field.put("size", "2");
		field.put(Constants.component, "radioGroup");
		field.put("colspan", "4");
		field.put("required", Constants.true_str);
		field.put("service", serviceName);
		field.put("serviceMethod", methodName);
		field.put("labelValue", Constants.name);
		field.put("key", "ID");
		addCampo(request, field);
	}

	private void createDatabase(final boolean onlyLogDDL, final Usuario user) throws BusinessException {

		try {
			getSetupService().createDataBase(onlyLogDDL, getSetupUser());
		} catch (final SQLException e) {
			throw new BusinessException("label.setup.bancodados.erro.criacao", e);
		}
	}

	private void createDataBaseDiffList(final ServletRequest request, final String system) {

		final Map<String, String> grid = new HashMap<String, String>();
		grid.put(Constants.component, "grid");
		grid.put("colspan", "4");
		grid.put("service", "Setup");
		grid.put("serviceMethod", "dataBaseDiff");
		grid.put("serviceParams", "{system:'" + system + "'}");
		grid.put("linkMethod", "editDataBaseDiff");
		addCampo(request, grid);

		noFilter(request);

		addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.setup.bancodados.removerTabelaOuColuna",
				"removeDataBaseObject", "functionvalidation.validate", BUTTON.FORALL, null, null, false, request);
	}

	private void createDataBaseList(final ServletRequest request, final String system) {
		// criando o grid principal de listagens
		final Map<String, String> grid = new HashMap<String, String>();
		grid.put(Constants.component, "grid");
		grid.put("colspan", "4");
		grid.put("service", "Setup");
		grid.put("serviceMethod", "dataBaseAnalys");
		grid.put("serviceParams", "{system:'" + system + "'}");
		grid.put("linkMethod", "inclui");
		addCampo(request, grid);

		noFilter(request);

		addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.setup.bancodados.corrigirBancoDados", "inclui",
				"functionvalidation.validate", BUTTON.FORALL, null, null, false, request);

		addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.setup.bancodados.corrigirGrupoDeErro",
				"corrigirGrupoErro", "functionvalidation.validate", BUTTON.FORONE, null, null, false, request);

		addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.setup.bancodados.refreshProblems", "refreshProblems",
				"functionvalidation.validate", BUTTON.STATIC, null, null, false, request);

	}

	private void createExecuteSQLForm(final ServletRequest request) {
		setMainField("SQL");

		final Map<String, String> field = new HashMap<String, String>();
		field.put(Constants.name, "SQL");
		field.put("label", "label.setup.bancodados.sql");
		field.put("type", "alfa");
		field.put(Constants.component, "memo");
		field.put("colspan", "4");
		addCampo(request, field);

		addApplyButton(request, STEP.DATABASE);
	}

	public void cloneDatabase(final ServletRequest request, final ServletResponse response) {
		initStep(request, "is_insert", "CONNECTION_FROM", STEP.CLONE_DATABASE);

		final boolean processed = process(request);

		if (processed) {
			Usuario from = new Usuario(getSetupUser());
			from.setConnectionRoute((String) getVO(request).get("CONNECTION_FROM"));
			Usuario to = new Usuario(getSetupUser());
			to.setConnectionRoute((String) getVO(request).get("CONNECTION_TO"));
			try {
				String t = (String) getVO(request).get("RESTRICT_TABLES");
				if (t == null)
					t = "";
				getSetupService().cloneDatabase(from, to, "T".equals(getVO(request).get("CREATE_DB")),
						"T".equals(getVO(request).get("POPULATE_DB")), Arrays.asList(t.split(",")));
			} catch (MetaDataAccessException e) {
				LogUtil.exception(e, getUser(request));
			} catch (SQLException e) {
				LogUtil.exception(e, getUser(request));
			}
		} else {
			setReqAttr(request,Constants.WINDOW_SIZE, "{w:500,h:350}");

			final Map<String, String> field = new HashMap<String, String>();
			field.put(Constants.name, "CONNECTION_FROM");
			field.put("label", "label.setup.conexaoOrigem");
			field.put("type", "alfa");
			field.put(Constants.component, "select");
			field.put("service", "Setup");
			field.put("serviceMethod", "getConnections");
			field.put("key", "ID");
			field.put("labelValue", "NAME");
			field.put("required", "true");
			field.put("colspan", "2");
			addCampo(request, field);

			final Map<String, String> field1 = new HashMap<String, String>();

			field1.put(Constants.name, "CONNECTION_TO");
			field1.put("label", "label.setup.conexaoDestino");
			field1.put("type", "alfa");
			field1.put(Constants.component, "select");
			field1.put("service", "Setup");
			field1.put("serviceMethod", "getConnections");
			field1.put("key", "ID");
			field1.put("labelValue", "NAME");
			field1.put("required", "true");
			field1.put("colspan", "2");
			addCampo(request, field1);

			final Map<String, String> field3 = new HashMap<String, String>();
			field3.put(Constants.name, "CREATE_DB");
			field3.put("label", "label.setup.bancodados.criarBanco");
			field3.put("type", "alfa");
			field3.put("value", "T");
			field3.put(Constants.component, "checkbox");
			field3.put("colspan", "2");
			addCampo(request, field3);

			final Map<String, String> field4 = new HashMap<String, String>();
			field4.put(Constants.name, "POPULATE_DB");
			field4.put("label", "label.setup.bancodados.popularBanco");
			field4.put("type", "alfa");
			field4.put("value", "F");
			field4.put(Constants.component, "checkbox");
			field4.put("colspan", "2");
			addCampo(request, field4);

			//
			Map<String, String> field5 = new HashMap<String, String>();
			field5.put(Constants.name, "RESTRICT_TABLES");
			field5.put("label", "label.setup.bancodados.restringir_tables");

			field5.put(Constants.type.toString(), "alfa");
			field5.put("component", "memo");
			field5.put("colspan", "4");
			field5.put("required", "false");
			addCampo(request, field5);
			//

			addApplyButton(request, STEP.CLONE_DATABASE);
		}
	}

	private void createFileField(final ServletRequest request) {
		final Map<String, String> field = new HashMap<String, String>();
		field.put(Constants.name, "FILE");
		field.put("label", "label.setup.file");
		field.put("type", "alfa");
		field.put(Constants.component, "file");
		field.put("colspan", "4");
		addCampo(request, field);
	}

	private void createProblemForm(final ServletRequest request, final DAOComparator.PROBLEM_TYPE problemType) {

		String label = "erro";
		switch (problemType) {
		case TABLE_NOT_EXISTS:
			label = "criarTabela";
			break;
		case COLUMN_NOT_EXISTS:
			label = "criarColuna";
			break;
		case SEQUENCE_NOT_EXISTS:
			label = "criarSequencia";
			break;
		case COLUMN_NOT_NULL:
			label = "permitirNulo";
			break;
		case COLUMN_TYPE:
			label = "alterarTipo";
			break;
		case PRIMARY_NOT_EXISTS:
			label = "criarChavePrimaria";
			break;
		case FOREIGN_KEY_NOT_EXISTS:
			label = "criarChaveEstrangeira";
			break;
		}
		createChoiceField(request, "label.setup.bancodados." + label);
	}

	public ModelAndView editDataBaseDiff(final ServletRequest request, final ServletResponse response)
			throws Exception {

		clearFields(request);
		clearButtons(request);
		setMainField("TABLE");
		setReqAttr(request,Constants.WINDOW_SIZE, "{w:500,h:270}");

		final GenericVO vo = getVO(request);

		if (vo.get("ID") == null) {
			throw new BusinessException("label.setup.bancodados.erro.registroInvalido");
		}

		final SetupService service = getSetupService();

		final Map<String, Object> problem = service.getDataBaseDiff(Integer.parseInt(vo.get("ID").toString()),
				getUser(request));

		if (problem == null) {
			throw new BusinessException("label.setup.bancodados.erro.carregandoLista");
		}

		vo.putAll(problem);

		final boolean entireTable = vo.get("OBJECT") == null;

		Map<String, String> field = new HashMap<String, String>();
		field.put(Constants.name, "TABLE");
		field.put("label", "label.setup.bancodados.tabela");
		field.put("type", "alfa");
		field.put(Constants.component, "alfa");
		field.put("colspan", entireTable ? "2" : "4");
		addCampo(request, field);

		final String label = entireTable ? "Tabela" : "Coluna";

		if (entireTable) {
			field = new HashMap<String, String>();
			field.put(Constants.name, "RENAME_TABLE");
			field.put("label", "label.setup.bancodados.novaTabela");
			field.put("type", "alfa");
			field.put(Constants.component, "alfa");
			field.put("colspan", "2");
			addCampo(request, field);

		} else {
			field = new HashMap<String, String>();
			field.put(Constants.name, "OBJECT");
			field.put("label", "label.setup.bancodados.coluna");
			field.put("type", "alfa");
			field.put(Constants.component, "alfa");
			field.put("colspan", "2");
			addCampo(request, field);

			field = new HashMap<String, String>();
			field.put(Constants.name, "RENAME_COLUMN");
			field.put("label", "label.setup.bancodados.novaColuna");
			field.put("type", "alfa");
			field.put(Constants.component, "alfa");
			field.put("colspan", "2");
			addCampo(request, field);
		}
		createChoiceField(request, "label.setup.bancodados.logOnly", "Setup", "getTrueOrFalse", "EXECUTE_DLL");

		addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.setup.bancodados.renomear" + label,
				"renameDataBaseObject", "functionvalidation.validate", BUTTON.STATIC, null, null, false, request);

		addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.setup.bancodados.remover" + label,
				"removeDataBaseObject", "functionvalidation.validate", BUTTON.STATIC, null, null, false, request);

		final String javascript = getLoadPageJS(STEP.DATABASE.getMethod(),
				"&CHOICE=" + SetupService.DataBaseOptions.NOT_CONFIGURED.name());

		addButton(Constants.MAIN_ACTION, "", BUTTON.METHOD, "label.voltar", "javascript:" + javascript, "",
				BUTTON.STATIC, null, null, false, request);

		return finish(request, response, null);
	}

	private void executeSQL(final String sql, final boolean onlyLogDDL, final Usuario user) throws Exception {

		getSetupService().executeSQL(sql, onlyLogDDL, getSetupUser());
	}

	private ModelAndView finishStep(final ServletRequest request, final ServletResponse response, final STEP step,
			final boolean processed) throws Exception {

		String extra = null;
		final STEP[] steps = STEP.values();

		if (processed) {
			String nextMethod;

			if (step.ordinal() != steps.length - 1) {
				nextMethod = steps[step.ordinal() + 1].getMethod();

			} else {
				nextMethod = steps[step.ordinal()].getMethod();
			}
			extra = getLoadPageJS(nextMethod);
		}
		return finish(request, response, extra);
	}

	private String getLoadPageJS(final String nextMethod) {
		return getLoadPageJS(nextMethod, null);
	}

	private String getLoadPageJS(final String nextMethod, final String params) {

		final String urlParam = params == null ? "" : params;

		final String extra = "loadPage(\"Setup" + Constants.ACTION_BASE + Constants.ACTION_SUFIX + "?"
				+ Constants.ActionParam + "=" + nextMethod + urlParam
				+ "&session=false\",\"window_modal\",\"null\",\"\",true,null,null)";
		return extra;
	}

	private SetupService getSetupService() {
		return (SetupService) getService();
	}

	private int[] getVoIds(final GenericVO vo) {
		int[] ids = null;
		final String[] multipleId = vo.getMultipleId();

		if (multipleId.length == 0 || multipleId[0].equals("")) {
			ids = new int[1];
			ids[0] = Integer.parseInt((String) vo.get("ID"));

		} else {
			ids = new int[multipleId.length];

			for (int i = 0; i < multipleId.length; i++) {
				ids[i] = Integer.parseInt(multipleId[i]);
			}
		}
		return ids;
	}

	@Override
	public ModelAndView inclui_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		clearFields(request);
		clearButtons(request);
		setMainField("TABLE");
		setReqAttr(request,Constants.WINDOW_SIZE, "{w:500,h:500}");

		final GenericVO vo = getVO(request);

		final SetupService service = getSetupService();

		//
		if (process(request) || vo.getMultipleId().length > 0 && !vo.getMultipleId()[0].equals("")) {

			final int[] ids = getVoIds(vo);

			if (ids.length == 0) {
				throw new BusinessException("label.setup.bancodados.erro.registroInvalido");
			}

			final boolean onlyLogDDL = false; // !vo.get("EXECUTE_DLL").equals("T");

			final List<Map<String, Object>> problems = new ArrayList<Map<String, Object>>();

			if (vo.get("CHOICE") != null) {

				if (vo.get("CHOICE").equals("T")) {
					problems.add(service.getDataBaseProblem(ids[0], getUser(request)));
					getSetupService().amendDatabase(problems, onlyLogDDL, vo, getSetupUser());
				}

			} else {

				for (final int id : ids) {
					problems.add(service.getDataBaseProblem(id, getUser(request)));
				}
				getSetupService().amendDatabase(problems, onlyLogDDL, getSetupUser());
			}
			// Volta para a lista de problemas
			final String extra = "loadPage(\"Setup" + Constants.ACTION_BASE + Constants.ACTION_SUFIX + "?"
					+ Constants.ActionParam + "=" + STEP.DATABASE.getMethod() + "&system="
					+ getUser(request).getSystem() + "&session=false&CHOICE="
					+ SetupService.DataBaseOptions.LIST_PROBLEMS.name()
					+ "\",\"window_modal\",\"null\",\"\",true,null,null)";

			return finish(request, response, extra);

		} else if (vo.get("ID") != null && !vo.get("ID").toString().equals("0")
				&& !vo.get("ID").toString().equals("")) {
			final Map<String, Object> problem = service.getDataBaseProblem(Integer.parseInt(vo.get("ID").toString()),
					getUser(request));

			if (problem == null) {
				throw new BusinessException("label.setup.bancodados.erro.carregandoLista");
			}

			vo.putAll(problem);
			Map<String, String> field = new HashMap<String, String>();
			field.put(Constants.name, "TABLE");
			field.put("label", "label.setup.bancodados.tabela");
			field.put("type", "alfa");
			field.put(Constants.component, "alfa");
			field.put("colspan", "2");
			addCampo(request, field);
			field = new HashMap<String, String>();
			field.put(Constants.name, "OBJECT");
			field.put("label", "label.setup.bancodados.objeto");
			field.put("type", "alfa");
			field.put(Constants.component, "alfa");
			field.put("colspan", "2");
			addCampo(request, field);

			if (vo.get("CONFIG_VALUE") != null) {
				field = new HashMap<String, String>();
				field.put(Constants.name, "CONFIG_VALUE");
				field.put("label", "label.setup.bancodados.configurado");
				field.put("type", "alfa");
				field.put(Constants.component, "alfa");
				field.put("colspan", "2");
				addCampo(request, field);
			}

			if (vo.get("VALUE_FOUND") != null) {
				field = new HashMap<String, String>();
				field.put(Constants.name, "VALUE_FOUND");
				field.put("label", "label.setup.bancodados.encontrado");
				field.put("type", "alfa");
				field.put(Constants.component, "alfa");
				field.put("colspan", "2");
				addCampo(request, field);
			}

			field = new HashMap<String, String>();
			field.put(Constants.name, "ERROR_TYPE");
			field.put("label", "label.setup.bancodados.erro");
			field.put("type", "alfa");
			field.put(Constants.component, "alfa");
			field.put("colspan", "4");
			addCampo(request, field);

			final DAOComparator.PROBLEM_TYPE problemType = DAOComparator.PROBLEM_TYPE
					.valueOf((String) problem.get("ERROR_CODE"));

			if (problemType == DAOComparator.PROBLEM_TYPE.COLUMN_NULL
					|| problemType == DAOComparator.PROBLEM_TYPE.FOREIGN_KEY_NOT_EXISTS
					|| problemType == DAOComparator.PROBLEM_TYPE.CHECK_CONSTRAINT_NOT_EXISTS) {
				field = new HashMap<String, String>();
				field.put(Constants.name, "DEFAULT_VALUE");
				field.put("label", "label.setup.bancodados.defaultValue");
				field.put("type", "alfa");
				field.put(Constants.component, "alfa");
				field.put("colspan", "2");
				addCampo(request, field);
				if (problemType == DAOComparator.PROBLEM_TYPE.COLUMN_NULL
						|| problemType == DAOComparator.PROBLEM_TYPE.CHECK_CONSTRAINT_NOT_EXISTS) {
					field = new HashMap<String, String>();
					field.put(Constants.name, "ESCAPED");
					field.put("label", "label.setup.bancodados.escaped");
					field.put("type", "alfa");
					field.put("value", "F");
					field.put(Constants.component, "checkbox");
					field.put("colspan", "2");
					addCampo(request, field);

					field = new HashMap<String, String>();
					field.put(Constants.name, "ALL_TYPES");
					field.put("label", "label.setup.bancodados.allTypes");
					field.put("type", "alfa");
					field.put("value", "F");
					field.put(Constants.component, "checkbox");
					field.put("colspan", "2");
					addCampo(request, field);

				}

				field = new HashMap<String, String>();
				field.put(Constants.name, "ALL_NAMES");
				field.put("label", "label.setup.bancodados.allNames");
				field.put("type", "alfa");
				field.put("value", "F");
				field.put(Constants.component, "checkbox");
				field.put("colspan", "2");
				addCampo(request, field);

			}
			createProblemForm(request, problemType);

			addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.aplicar", "inclui",
					"functionvalidation.validate", BUTTON.STATIC, null, null, false, request);

		}
		final String javascript = getLoadPageJS(STEP.DATABASE.getMethod(),
				"&CHOICE=" + SetupService.DataBaseOptions.LIST_PROBLEMS.name());

		addButton(Constants.MAIN_ACTION, "", BUTTON.METHOD, "label.voltar", "javascript:" + javascript, "",
				BUTTON.STATIC, null, null, false, request);

		return finish(request, response, null);
	}

	@Override
	public void init_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		NoRequiredUser(request);
		addKey(getPrimaryKey(), null, request);
		final Map<String, Object> properties = getService().getProperties();

		if (properties != null) {

			if ((properties.containsKey("initial") || properties.containsKey("main")) && isDefaultCRUD(request)) {
				setReqAttr(request,Constants.MAIN_ACTION, true);
			}
		}
	}

	private void initStep(final ServletRequest request, final String method, final String mainField,
			final STEP currentStep) {

		final Usuario user = getUser(request);
		final String label = user.getLabel("label.setup.title." + currentStep.name().toLowerCase());
		final int step = currentStep.ordinal() + 1;
		final int total = STEP.values().length;

		clearFields(request);
		setMainField(mainField);
		setReqAttr(request,method, Boolean.TRUE);
		String str = null;
		try {
			str = (String) ConnectionManager.getInstance(user).getPool().get(user.getConnectionRoute())
					.get(ConnectionManager.USER);
		} catch (Exception e) {
		}

		setTitulo("Conectado a " + user.getConnectionRoute() + " (dbUser = " + str + ") (sigla = "
				+ user.getSystemProperty("SIGLA_CLIENTE") + ") > " + label + " ( " + step + " / " + total + " )",
				request);
		setReqAttr(request,"is_win", Boolean.TRUE);
		if (method.equals("is_list")) {
			setReqAttr(request,Constants.WINDOW_SIZE, "{h:580}");
		} else {
			setReqAttr(request,Constants.WINDOW_SIZE, "{w:580,h:230}");
		}
	}

	@Override
	public void lista_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		noFilter(request);
		getVO(request).setFiltro("/systems/system");
		getVO(request).setPaginate(false);
		overrideLink("javascript:loadPage(\"Setup" + Constants.ACTION_BASE + Constants.ACTION_SUFIX + "?"
				+ Constants.ActionParam + "=" + STEP.CHOSECONNECTION.getMethod()
				+ "&system=$NAME$&session=false\",\"window_modal\",\"null\",\"\", true, null, null)", request);

	}



	@Override
	public ModelAndView obtem_imp(final ServletRequest request, final ServletResponse response) throws Exception {
		try {
			getVO(request).putAll((Map) service.invoke(getVO(request).getMethod(), getVO(request), getUser(request)));
		} catch (final Exception e) {
			throw new BusinessException("error.naoEncontrado");
		}
		return finish(request, response, null);
	}

	@Override
	public ModelAndView obtemLista(final ServletRequest request, final ServletResponse response)
			throws IOException, ServletException {

		getVO(request).setMapping(getUser(request).getUtil().encode("{'id':'ID','name':'NAME'}", getUser(request)));
		setService(getSetupService());
        setReqAttr(request, "is_list", Boolean.TRUE);
        List<Map> items = new ArrayList<Map>();
        GenericService service = null;
        List<Map> nd = new ArrayList();
        if (request.getAttribute(Constants.LIST_DATA) == null) {
            String ser = (String) util.Evaluate((String) getParameter(request, "service", false),
                    super.createEvaluationContext(request));
            if (ser == null) {
                ser = (String) getParameter(request, "service", false);
            }
            service = (GenericService) getUser(request).getService(ser);
            final String method = (String) getParameter(request, "serviceMethod", false);

            if (service == null)
                service = getService();
            try {

                if (!method.equalsIgnoreCase("dummy")) {
                    if (method == null || method.toString().length() == 0 || method.equalsIgnoreCase("null")
                            || method.equalsIgnoreCase("lista")) {
                        getVO(request).setLazyList(true);
                        // if("D".equals(getService().getProperties().get("type")))
                        // {
                        // String mpk = (String)
                        // getService().getParent(getUser(request)).getPrimaryKey().get(Constants.name);
                        // if(!getVO(request).containsKey(mpk) ||
                        // getVO(request).get(mpk)==null )
                        // throw new BusinessException("label.reabrir.pasta");
                        // }
                        //
                        items.addAll(service.lista(getVO(request),getSetupUser()));
                        // getVO(request).cloneConfig(mvo);
                    } else {
                        getVO(request).put(Constants.RequestProcessed, request);
						getVO(request).setPaginate(false);
                        items = (List<Map>) service.invoke(method, getVO(request), getSetupUser());
                        if (items == null && getVO(request).get(Constants.LIST_DATA) != null)
                            items = (List<Map>) getVO(request).get(Constants.LIST_DATA);
                    }
                }
            } catch (final Exception e1) {
                LogUtil.exception(e1, (Usuario) request.getAttribute(Constants.RAIN_USER));
                throw new BusinessException(e1);
            }
            List f = null;

            if (method != null && method.toString().trim().length() > 0 && ReflectionUtils
                    .findMethod(service.getClass(), method, new Class[] { GenericVO.class, Usuario.class }) == null) {
                f = (List) service.getMethod(method, getUser(request)).get(Constants.ALL_FIELDS);
            } else {

                f = new ArrayList((List) service.getProperties().get(Constants.ALL_FIELDS));
                if (f != null) {
                    f.addAll(f = service.getDAO().getProperties());
                }
            }

            final List pk = util.match(f, "primaryKey", Constants.true_str);
            f = util.filter(f, "listable", null);
            pk.addAll(f);
            final List names = java.util.Arrays.asList(util.extractArrayFromList(pk, Constants.name, true));
            for (final Object element : items) {
                final HashMap object = (HashMap) element;
                LogUtil.debug("DATA >>> " + object, getUser(request));

                final Iterator i = object.keySet().iterator();
                final GenericVO n = service.createVO(getUser(request));
                if (getVO(request).containsKey("FORCE_*_TEXT")) {
                    n.put("FORCE_*_TEXT", true);
                }

                for (final Iterator iterator2 = i; iterator2.hasNext();) {

                    final String object2 = (String) iterator2.next();
                    if (names.contains(object2.toUpperCase())) {
                        final HashMap metadata = (HashMap) ((HashMap) util
                                .match(pk, Constants.name, object2.toUpperCase()).get(0)).clone();

                        final Map d = new HashMap(object);
                        d.putAll(metadata);

                        n.put(object2, object.get(object2));

                        if (
                            // !object2.toString().equalsIgnoreCase(auxCol)
                            // && !n.containsKey(auxCol)
                            // &&
                                metadata.get("serviceMethod") != null
                                        && !getService().isDefaultCRUD((String) metadata.get("serviceMethod"))) { // TODO

                            final String auxCol = util.defineColumn(metadata, getUser(request), true, 0)
                                    .get(Constants.COLUMN_NAME);
                            if (auxCol == null)
                                continue;
                            // corrigir
                            // bug na
                            // listagem
                            // para
                            // isDefaulCrud
                            // false
                            // **MATCHSER
                            // util.matchService(d, object2,
                            // object.get(object2), getUser(request));
                            if (getService().equals(util.evalVOService(d, getUser(request)))) {
                                d.putAll(metadata); // recolocando os filtros
                            }
                            // originais
                            // **EVSER

                            n.put(auxCol, util.evalService(d, object2, getUser(request)));
                        }

                    } else if (n.get(object2) == null) {
                        n.put(object2, object.get(object2));

                    }

                }

                if (getParameter(request, "labelValue", false) != null
                        && getParameter(request, "labelValue", false).toString().trim().length() > 0) {
                    if (!n.containsKey(getParameter(request, "labelValue", false))) {
                        final String[] labels = getParameter(request, "labelValue", false).toString().split(",");
                        String v = "";
                        for (final String label : labels) {
                            HashMap metadata = null;

                            try {
                                metadata = (HashMap) ((HashMap) util.match(pk, Constants.name, label.toUpperCase())
                                        .get(0)).clone();
                            } catch (final Exception e) {
                                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                            }
                            v = v + n.get(label) + ", ";
                        }
                        n.put(getParameter(request, "labelValue", false).toString().replaceAll(",", "_"),
                                v.substring(0, v.lastIndexOf(",")));
                    }
                }
                nd.add(n.format(getUser(request)));
            }
        } else {
            // Quando o service representa um objeto no request
            try {
                if (request.getAttribute(Constants.LIST_DATA) != null) {
                    nd = (List<Map>) request.getAttribute(Constants.LIST_DATA);
                } else {
                    nd = (List<Map>) util
                            .Evaluate("${" + util.Evaluate((String) getParameter(request, "service", false),
                                    createEvaluationContext(request)) + "}", createEvaluationContext(request));
                }
            } catch (final Exception e2) {
                throw new BusinessException(e2);
            }
        }
        return super.serializeJSOnResponse(request, response, nd, getVO(request).isEncapsulate());
	}

	private void populateVo(final GenericVO vo, final Usuario user) throws BusinessException {
		try {
			vo.putAll((Map) getSetupService().invoke(vo.getMethod(), vo, getSetupUser()));
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			throw new BusinessException("error.naoEncontrado");
		}
	}

	private ModelAndView processDataBaseOption(final ServletRequest request, final ServletResponse response,
			final GenericVO vo, final String system, final boolean logOnlyDLL, final Usuario user) throws Exception {

		final SetupService.DataBaseOptions option = SetupService.DataBaseOptions.valueOf(vo.get("CHOICE").toString());

		switch (option) {
		case NOT_CONFIGURED:
			// if(user.getSystemProperty("SIGLA_CLIENTE")==null)
			// throw new BusinessException("Sigla no Configurada");
			initStep(request, "is_list", Constants.name, STEP.DATABASE);
			createDataBaseDiffList(request, system);
			break;

		case LIST_PROBLEMS:
			// if(user.getSystemProperty("SIGLA_CLIENTE")==null)
			// throw new BusinessException("Sigla no Configurada");
			initStep(request, "is_list", Constants.name, STEP.DATABASE);
			createDataBaseList(request, system);
			break;

		case REMOVE_CONSTRAINTS:
			removeAllConstraints(logOnlyDLL, getSetupUser());
			break;

		case REMOVE_INDEXES:
			removeAllIndexes(logOnlyDLL, getSetupUser());
			break;

		case REMOVE_SEQUENCES:
			removeAllSequences(logOnlyDLL, getSetupUser());
			break;

		case EXECUTE_SQL:
			createExecuteSQLForm(request);
			break;

		case CREATE_DATA_BASE:
			createDatabase(logOnlyDLL, getSetupUser());
			break;

		case CLONE_DATA_BASE:
			cloneDatabase(request, response);
			break;

		case FONETIC_INDEX:
			foneticIndex(request, response, getSetupUser());
			break;

		case PROCESS_LABELS:
			processLabels(request, response);
			break;

		case CREATE_CONNECTION:
			createConnection(request, response);
			break;

		case DISABLE_SETUP:
			disableSetup(request, getSetupUser());
			break;

		case EXECUTAR_SCRIPT_RULE:
			executarScriptRule(request, response);
			break;

		}
		return null;

	}

	public void executarScriptRule(ServletRequest request, ServletResponse response) {
		Map<String, String> field = new HashMap<String, String>();
		field.put(Constants.name, "script");
		field.put(Constants.type.toString(), "alfa");
		field.put("component", "memo");
		field.put("colspan", "4");
		field.put("required", "true");
		addCampo(request, field);
		addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, getUser(request).getLabel("label.executar"),
				"executarScript", "functionvalidation.validate", BUTTON.STATIC, null, null, false, request);
	}

	public void executarScript(ServletRequest request, ServletResponse response) {
		getVO(request).put("SCRIPT", request.getParameter("script"));
		try {
			util.executeScript(getVO(request), getSetupUser());
		} catch (ScriptException ex) {
			throw new BusinessException(ex.toString());
		}
	}

	public ModelAndView processLabels(ServletRequest request, ServletResponse response) throws Exception {

		if (request.getParameter("LANG") == null) {
			Map<String, String> field = new HashMap<String, String>();
			field.put("colspan", "4");
			field.put("component", "select");
			field.put("key", "ID");
			field.put("labelValue", "NAME");
			field.put("name", "LANG");
			field.put("required", "true");
			field.put("service", "Setup");
			field.put("serviceMethod", "getLanguages");
			field.put("type", "alfa");
			field.put("volatile", "true");

			addButton(Constants.MAIN_ACTION, "", BUTTON.DOWNLOAD,
					getUser(request).getLabel("label.setup.process.labels"), "processLabels",
					"functionvalidation.validate", BUTTON.FORALL, null, null, false, request);

			addCampo(request, field);

		} else {

			getUser(request).setLang(Long.valueOf(request.getParameter("LANG")));
			Map<String, String> labelasAbsent = ((SetupService) getService("Setup", getUser(request)))
					.processLabels(getUser(request));
			List<GenericVO> vos = new ArrayList<GenericVO>();
			GenericService service = (GenericService) getService("ItemLinguagemService", getUser(request));
			GenericVO vo = null;
			Integer count = 1;

			for (final String label : labelasAbsent.keySet()) {
				vo = service.createVO(getUser(request));
				vo.put("NOME_CONFIG_SISTEMA", label);
				LogUtil.debug(label, getUser(request));
				vo.put("ID_CONFIG_SISTEMA", (count++).toString());
				vo.put("ID_PAI", getUser(request).getLang().toString());
				vo.put("VALOR_CONFIG_SISTEMA", " ");
				vos.add(vo);
			}

			configureDownload((HttpServletResponse) response, "text/xml", service.getName());
			service.exportListBean(vos, getSetupUser(), response.getWriter());
		}
		return null;
	}

	public void createConnection(final ServletRequest request, final ServletResponse response) throws Exception {

		initStep(request, "is_insert", "POOL_NAME", STEP.CREATE_CONNECTION);

		final GenericVO vo = getVO(request);
		final String system = getUser(request).getSystem();

		final boolean processed = process(request);

		if (processed) {

			getSetupService().createConnection(vo.get("POOL_NAME").toString(), vo.get("POOL_DRIVER").toString(),
					vo.get("POOL_URL").toString(), vo.get("POOL_USER").toString(), vo.get("POOL_PASSWORD").toString(),
                    getSetupUser());

		} else {

			Map<String, String> field4 = new HashMap<String, String>();
			field4.put(Constants.name, "POOL_NAME");
			field4.put("label", "label.setup.bancodados.name");
			field4.put("type", "alfa");
			field4.put("required", "true");
			field4.put(Constants.component, "alfa");
			field4.put("colspan", "2");
			addCampo(request, field4);

			Map<String, String> field = new HashMap<String, String>();
			field.put(Constants.name, "POOL_DRIVER");
			field.put("label", "label.setup.bancodados.driver");
			field.put("type", "alfa");
			field.put(Constants.component, "select");
			field.put("service", "Setup");
			field.put("serviceMethod", "getDrivers");
			field.put("key", "ID");
			field.put("labelValue", "NAME");
			field.put("required", "true");
			field.put("colspan", "2");
			addCampo(request, field);

			Map<String, String> field2 = new HashMap<String, String>();
			field2.put(Constants.name, "POOL_URL");
			field2.put("label", "label.setup.bancodados.url");
			field2.put("type", "alfa");
			field2.put(Constants.component, "alfa");
			field2.put("required", "true");
			field2.put("bind", "POOL_DRIVER:tem|value");
			field2.put("colspan", "2");
			addCampo(request, field2);

			Map<String, String> field3 = new HashMap<String, String>();
			field3.put(Constants.name, "POOL_USER");
			field3.put("label", "label.setup.bancodados.user");
			field3.put("type", "alfa");
			field3.put(Constants.component, "alfa");
			field3.put("required", "true");
			field3.put("colspan", "1");
			addCampo(request, field3);

			Map<String, String> field5 = new HashMap<String, String>();
			field5.put(Constants.name, "POOL_PASSWORD");
			field5.put("label", "label.setup.bancodados.pass");
			field5.put("type", "alfa");
			field5.put(Constants.component, "password");
			field5.put("required", "true");
			field5.put("colspan", "1");
			addCampo(request, field5);
			addApplyButton(request, STEP.CREATE_CONNECTION);
		}

	}

	private void foneticIndex(final ServletRequest request, final ServletResponse response, Usuario user) {
		try {
			getSetupService().foneticIndex(getSetupUser());

		} catch (final Exception e) {
			throw new BusinessException("label.setup.bancodados.erro.foneticIndex", e);
		}
	}

	private void removeAllConstraints(final boolean logOnlyDLL, final Usuario user) throws Exception {
		try {
			getSetupService().dropAllConstraints(logOnlyDLL, getSetupUser());
		} catch (final SQLException e) {
			throw new BusinessException("label.setup.bancodados.erro.remocaoRestricoes", e);
		}
	}

	private void removeAllSequences(final boolean logOnlyDLL, final Usuario user) throws Exception {
		try {
			getSetupService().dropAllSequences(logOnlyDLL, getSetupUser());
		} catch (final SQLException e) {
			throw new BusinessException("label.setup.bancodados.erro.remocaoSequencias", e);
		}
	}

	private void removeAllIndexes(final boolean logOnlyDLL, final Usuario user) throws Exception {
		try {
			getSetupService().dropAllIndexes(logOnlyDLL, getSetupUser());
		} catch (final SQLException e) {
			throw new BusinessException("label.setup.bancodados.erro.remocaoRestricoes", e);
		}
	}

	public ModelAndView removeDataBaseObject(final ServletRequest request, final ServletResponse response)
			throws Exception {

		final GenericVO vo = getVO(request);

		final int[] multipleId = getVoIds(vo);

		if (multipleId.length == 0) {
			throw new BusinessException("label.setup.bancodados.erro.registroInvalido");
		}

		final SetupService service = getSetupService();
		Map<String, Object> problem;
		boolean entireTable;

		final boolean onlyLogDDL = !vo.get("EXECUTE_DLL").toString().equals("T");

		for (final int element : multipleId) {
			problem = service.getDataBaseDiff(element, getSetupUser());
			entireTable = problem.get("OBJECT") == null;

			try {

				if (entireTable) {
					service.dropTable((String) problem.get("TABLE"), onlyLogDDL,getSetupUser());
				} else {
					service.dropColumn((String) problem.get("TABLE"), (String) problem.get("OBJECT"), onlyLogDDL,
                            getSetupUser());
				}
			} catch (Exception e) {
				LogUtil.exception(e, getUser(request));
			}
		}

		final String javascript = getLoadPageJS(STEP.DATABASE.getMethod(),
				"&CHOICE=" + SetupService.DataBaseOptions.NOT_CONFIGURED.name());

		return finish(request, response, javascript);
	}

	public ModelAndView renameDataBaseObject(final ServletRequest request, final ServletResponse response)
			throws Exception {

		final GenericVO vo = getVO(request);

		if (vo.get("ID") == null) {
			throw new BusinessException("label.setup.bancodados.erro.registroInvalido");
		}

		final boolean onlyLogDDL = !vo.get("EXECUTE_DLL").toString().equals("T");

		final SetupService service = getSetupService();

		final Map<String, Object> problem = service.getDataBaseDiff(Integer.parseInt(vo.get("ID").toString()),
                getSetupUser());

		if (problem == null) {
			throw new BusinessException("label.setup.bancodados.erro.carregandoLista");
		}

		final boolean entireTable = vo.get("OBJECT") == null;

		if (entireTable) {

			if (vo.get("RENAME_TABLE") == null || vo.get("RENAME_TABLE").equals("")) {
				throw new BusinessException("label.setup.bancodados.erro.novoNomeTabela");
			}
			service.renameTable((String) problem.get("TABLE"), (String) vo.get("RENAME_TABLE"), onlyLogDDL,
					getUser(request));
		} else {

			if (vo.get("RENAME_COLUMN") == null || vo.get("RENAME_COLUMN").equals("")) {
				throw new BusinessException("label.setup.bancodados.erro.novoNomeColuna");
			}
			service.renameColumn((String) problem.get("TABLE"), (String) vo.get("OBJECT"),
					(String) vo.get("RENAME_COLUMN"), onlyLogDDL, getUser(request));
		}

		final String javascript = getLoadPageJS(STEP.DATABASE.getMethod(),
				"&CHOICE=" + SetupService.DataBaseOptions.NOT_CONFIGURED.name());

		return finish(request, response, javascript);
	}

	public ModelAndView setupConnection(final ServletRequest request, final ServletResponse response) throws Exception {

		// initStep(request, "is_insert", "DATAURL", STEP.CONNECTION);

		final GenericVO vo = getVO(request);

		final boolean processed = process(request);

		if (processed) {
			getSetupService().saveConnectionParams(vo, getSetupUser());

		} else {
			Map<String, String> field = new HashMap<String, String>();
			field.put(Constants.name, "DATAURL");
			field.put("label", "label.setup.conexao.url");
			field.put("type", "alfa");
			field.put(Constants.component, "alfa");
			field.put("colspan", "2");
			addCampo(request, field);

			field = new HashMap<String, String>();
			field.put(Constants.name, "DRIVER");
			field.put("label", "label.setup.conexao.driver");
			field.put("type", "alfa");
			field.put(Constants.component, "alfa");
			field.put("colspan", "2");
			addCampo(request, field);

			field = new HashMap<String, String>();
			field.put(Constants.name, "USERNAME");
			field.put("label", "label.setup.conexao.usuario");
			field.put("type", "alfa");
			field.put(Constants.component, "alfa");
			field.put("colspan", "2");
			addCampo(request, field);

			field = new HashMap<String, String>();
			field.put(Constants.name, "PASSWORD");
			field.put("label", "label.setup.conexao.senha");
			field.put("type", "alfa");
			field.put(Constants.component, "password");
			field.put("colspan", "2");
			addCampo(request, field);

			populateVo(vo, getUser(request));

			// addApplyButton(request, STEP.CONNECTION);
			// addStepButtons(request, STEP.CONNECTION, false);
		}
		return finishStep(request, response, STEP.CHOSECONNECTION, processed);
	}

	public ModelAndView choseConnection(final ServletRequest request, final ServletResponse response) throws Exception {

		initStep(request, "is_insert", "CONNECTION", STEP.CHOSECONNECTION);

		final GenericVO vo = getVO(request);

		final boolean processed = process(request);

		if (processed) {
			String p = (String) getParameter(request, "CONNECTION", false);
			getSetupUser().setConnectionRoute(p);
            getSetupUser().setSystem(p);
            getSetupUser().setSchema(p);
			getUser(request).setConnectionRoute(p);
			getSetupService().initSetup(getSetupUser());

		} else {
			getVO(request).put("CONNECTION", (String) getUser(request).getConnectionRoute());
			Map<String, String> field = new HashMap<String, String>();
			field.put(Constants.name, "CONNECTION");
			field.put("label", "label.setup.conexao");
			field.put("type", "alfa");
			field.put(Constants.component, "select");
			field.put("service", "Setup");
			field.put("serviceMethod", "getConnections");
			field.put("key", "ID");
			field.put("labelValue", "NAME");
			field.put("required", "true");
			field.put("colspan", "4");
			addCampo(request, field);

			// populateVo(vo, user);

			addApplyButton(request, STEP.CHOSECONNECTION);
			setStepRequired(request);
			addStepButtons(request, STEP.CHOSECONNECTION, false);
		}
		return finishStep(request, response, STEP.CHOSECONNECTION, processed);
	}

    private Usuario getSetupUser() {
        return setupUser;
    }

    private void setStepRequired(ServletRequest request) {
		setReqAttr(request,STEP_REQUIRED, true);

	}

	public ModelAndView setupDataBase(final ServletRequest request, final ServletResponse response) throws Exception {

		initStep(request, "is_insert", "CHOICE", STEP.DATABASE);

		final GenericVO vo = getVO(request);

		getSetupService().createDataBaseLogTable(getSetupUser());

		boolean processed = process(request);
		boolean deepStep = false;

		if (processed) {
			processed = false;
			setReqAttr(request,Constants.RequestProcessed, null);

			final String executeDLL = vo.get("EXECUTE_DLL") == null ? "T" : vo.get("EXECUTE_DLL").toString();
			addKey("EXECUTE_DLL", executeDLL, request);

			final boolean onlyLogDDL = !executeDLL.equals("T");

			if (vo.get("SQL") != null) {

				executeSQL((String) vo.get("SQL"), onlyLogDDL, getUser(request));
				vo.put("CHOICE", SetupService.DataBaseOptions.EXECUTE_SQL);
			}
			request.removeAttribute(Constants.PROCESS_CALCULATED);
			ModelAndView v = processDataBaseOption(request, response, vo, getUser(request).getSchema(), onlyLogDDL,
					getUser(request));

			deepStep = true;
			if (v != null)
				return v;

		} else {
			setReqAttr(request,Constants.WINDOW_SIZE, "{w:580,h:330}");

			createChoiceField(request, "label.setup.bancodados.opcoes", "Setup", "getDataBaseOptions", "CHOICE");
			// createChoiceField(request, "label.setup.bancodados.logOnly",
			// "Setup", "getTrueOrFalse", "EXECUTE_DLL");
			vo.put("EXECUTE_DLL", "T");
			addApplyButton(request, STEP.DATABASE);
		}
		addStepButtons(request, STEP.DATABASE, deepStep);

		return finishStep(request, response, STEP.DATABASE, processed);
	}

	public ModelAndView setupLanguage(final ServletRequest request, final ServletResponse response) throws Exception {

		initStep(request, "is_insert", "FILE", STEP.LANGUAGE);

		final GenericVO vo = getVO(request);

		final boolean processed = process(request);

		if (processed) {
			final Object file = vo.get("FILE");

			if (file != null) {
				getSetupService().importFile((InputStream) file, getSetupUser());
			}

		} else {
			createFileField(request);
			addApplyButton(request, STEP.LANGUAGE);
			addStepButtons(request, STEP.LANGUAGE, false);
		}
		return finishStep(request, response, STEP.LANGUAGE, processed);
	}

	public ModelAndView setupUser(final ServletRequest request, final ServletResponse response) throws Exception {

		initStep(request, "is_insert", "FILE", STEP.USER);

		final GenericVO vo = getVO(request);

		final boolean processed = process(request);

		if (processed) {
			final Object file = vo.get("FILE");

			if (file != null) {
				getSetupService().importFile((InputStream) file, getSetupUser());
			}

		} else {
			createFileField(request);
			addApplyButton(request, STEP.USER);
			addStepButtons(request, STEP.USER, false);
		}
		return finishStep(request, response, STEP.USER, processed);
	}

	public ModelAndView validateSystem(final ServletRequest request, final ServletResponse response) throws Exception {

		initStep(request, "is_insert", "CHOICE", STEP.VALIDATION);

		final GenericVO vo = getVO(request);

		boolean processed = process(request);
		boolean deepStep = false;

		if (processed) {

			if (vo.get("CHOICE") != null && vo.get("CHOICE").equals("T")) {
				initStep(request, "is_list", Constants.name, STEP.VALIDATION);

				final Map<String, String> grid = new HashMap<String, String>();
				grid.put(Constants.component, "grid");
				grid.put("colspan", "4");
				grid.put("service", "Setup");
				grid.put("serviceMethod", "semanticValidation");
				grid.put("serviceParams", "{system:'" + getUser(request).getSchema() + "'}");
				grid.put("sm", Constants.false_str);
				addCampo(request, grid);

				noFilter(request);

				processed = false;
				deepStep = true;
			}

		} else {
			createChoiceField(request, "label.setup.validacao.validar");
			addApplyButton(request, STEP.VALIDATION);
		}
		addStepButtons(request, STEP.VALIDATION, deepStep);

		return finishStep(request, response, STEP.VALIDATION, processed);
	}
}
