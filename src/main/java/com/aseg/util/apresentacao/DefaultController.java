package com.aseg.util.apresentacao;

import com.aseg.config.Constants;
import com.aseg.vo.GenericVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class DefaultController extends BaseController {

	@Override
	public ModelAndView altera_imp(final ServletRequest request, final ServletResponse response) throws Exception {
		if (process(request)) {
			getVO(request).setOld_VO((GenericVO) getVO(request).clone());
			service.altera(getVO(request), getUser(request));

		}
		return finish(request, response, null);
	}

	@Override
	public ModelAndView inclui_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		if (process(request) && isTokenValid((HttpServletRequest) request)) {
			getVO(request).setOld_VO((GenericVO) getVO(request).clone());
			service.inclui(getVO(request), getUser(request));

		} else {
			addKey(Constants.TOKEN, nextToken((HttpServletRequest) request), request);
			if (!service.prepare(getVO(request), getUser(request))) {
				return refresh(request, response);
			}
		}
		return finish(request, response, null);
	}

	@Override
	public void lista_imp(final ServletRequest request, final ServletResponse response) throws Exception {
		getVO(request).setLazyList(true);
		if (processLista(request)) {
			setReqAttr(request,"dadosLista", getService().lista(getVO(request), getUser(request)));
		}

	}

	
	@Override
	public ModelAndView obtem_imp(final ServletRequest request, final ServletResponse response) throws Exception {

		GenericVO old = (GenericVO) getVO(request).clone();
		getVO(request).putAllAndGet((Map) service.invoke(getVO(request).getMethod(), getVO(request), getUser(request)),
				true);
		getVO(request).setMethod(old.getMethod());
		return finish(request, response, null);

	}

}

