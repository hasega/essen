package com.aseg.util.apresentacao;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.util.WebUtils;

import com.aseg.config.Constants;
import com.aseg.exceptions.BusinessException;
import com.aseg.exceptions.MessageException;
import com.aseg.exceptions.PermissionException;
import com.aseg.exceptions.SessionExpiredException;
import com.aseg.exceptions.StreamingException;
import com.aseg.logauditoria.service.LogUtil;
import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.service.ServiceImpl;
import com.aseg.service.importation.XMLImporter;
import com.aseg.util.service.ParseXmlService;
import com.aseg.util.service.UtilServiceImpl;
import com.aseg.vo.GenericVO;

/**
 * @author asega
 */
public abstract class BaseController extends MultiActionController implements Controller {
    ScriptEngineManager manager = new ScriptEngineManager();
    final ScriptEngine engine = manager.getEngineByName("js");

    public String encode(String url, final String id, final String text, final boolean urlRewrite, Usuario user) {

        while (url.indexOf("encode(") > -1) {
            String aux = url.substring(url.indexOf("encode(") + 7);
            aux = aux.substring(0, aux.indexOf(")"));
            url = util.replace(url, "encode(" + aux + ")", util.encode(aux, user));
        }
        if (!urlRewrite) {
            return url;
        }
        if (url.startsWith("javascript") || url.indexOf("jsessionid") > -1) {
            return url;
        }
        if (url.indexOf("?") > -1) {
            if (text == null || text.length() == 0) {
                return util.replace(util.replace(url, "?", ";jsessionid=" + id + "$$"), "$$", "?");
            } else {
                return util.replace(util.replace(url, "?", ";jsessionid=" + id + "$$"), "$$", "?texto=" + text + "&");
            }

        }
        if (text == null || text.length() == 0) {
            return url + ";jsessionid=" + id;
        } else {
            return url + ";jsessionid=" + id + "?texto=" + text;
        }

    }

    /**
     * @author asega NAO UTILIZAR _ PARA O VALOR DA enum
     */
    public enum BUTTON {
        ACTION, ACTIONNEXT, CANCEL, CONDITION, CONFIRM, DOWNLOAD, FORALL, FORONE, METHOD, SEPARATOR, STATIC
    }

    ;

    public boolean filterUrl(final String el) {
        final List p = new ArrayList();
        p.add("key_");
        p.add("multipleId");
        p.add("senha");
        p.add("callback");
        p.add("_dc");
        for (final Iterator iterator = p.iterator(); iterator.hasNext(); ) {
            final String name = (String) iterator.next();
            if (el.toUpperCase().startsWith(name.toUpperCase())) {
                return false;
            }
        }
        return true;
    }

    // private String ActionId = null;
    private boolean dynamic_redirect = false;
    public final UtilServiceImpl util = new UtilServiceImpl();
    private boolean master = false;
    public boolean requiredUser = true;
    protected GenericService service = new ServiceImpl();
    String mf = null;

    public UtilServiceImpl getUtil() {
        return util;
    }

    public void addButton(final String PANEL_NAME, String elExpr, final BUTTON tipo, String label, final String value,
                          final String msg, final BUTTON condititon, final String align, final String iconCls,
                          final boolean hideLabel, final ServletRequest request) {
        HashMap<String, ArrayList<Map>> d = null;
        Map mmap = null;

        if (getActionProperty(request) != null) {
            if (value == null) {
                return;
            }
            if (value.startsWith("inclui")) {
                mmap = (Map) getActionProperty(request).get("C");
                if (mmap == null)
                    mmap = (Map) getActionProperty(request).get("inclui");
                if (!evalPrintButton(mmap, request)) {
                    return;
                }
            }
            if (value.startsWith("altera")) {
                mmap = (Map) getActionProperty(request).get("U");
                if (mmap == null)
                    mmap = (Map) getActionProperty(request).get("altera");
                if (!evalPrintButton(mmap, request)) {
                    return;
                }
            }
            if (value.startsWith("remove")) {
                mmap = (Map) getActionProperty(request).get("D");
                if (mmap == null)
                    mmap = (Map) getActionProperty(request).get("remove");
                if (!evalPrintButton(mmap, request)) {
                    return;
                }
            }

            if (!getActionProperty(request).containsKey(value) && !value.startsWith("javascript")
                    && !isDefaultCRUD(request)) {
                return;
            }
        }
        // (java.lang.String)
        // M_PALAVRA_CHAVE_add-STATIC_essen/PalavraChaveVO_PV_lista
        if ((elExpr == null || elExpr.trim().length() == 0) && mmap != null) {
            if (mmap.get("disableWhenExpression") != null) {
                Map t = util.JSONtoMap(mmap.get("disableWhenExpression").toString());
                if (t.get("c") != null) {
                    elExpr = (String) t.get("c");

                }
            }

        }

        d = (HashMap<String, ArrayList<Map>>) request.getAttribute(Constants.BUTTONS);

        final Map a = new HashMap();
        if (label.indexOf(">") > -1) {
            String[] ln = label.split(">");
            label = ln[1];
            a.put("KEYLABEL", ln[0]);
        }
        if (d.containsKey(PANEL_NAME) && util
                .match(d.get(PANEL_NAME), "id", getId(util.replace(label, ".", "_") + "-" + condititon + "_", request))
                .size() > 0) {
            return;
        }

        String t = (String) getService().getProperties().get("table");
        if (t == null) {
            t = "";
        }
        Map p = new HashMap();
        p.put("label", label.replaceAll("label", t));
        if (!ParseXmlService.canAccess(p, getUser(request), true)) {
            return;
        }

        a.put("TIPO", tipo);
        if (hideLabel) {
            a.put("LABEL", "");
        } else {
            a.put("LABEL", getUser(request).getLabel(label));
        }
        a.put("VALUE", value);
        if (align != null) {
            a.put("ALIGN", align);
        }
        if (iconCls != null) {
            a.put("ICONCLS", iconCls);
        }
        // a.put("PERMISSAO",
        // getUser(request).verificaPermisao(label.hashCode()));
        if (request.getAttribute("button_for_detail") != null)
            a.put("is_detail", true);

        a.put("COMP", condititon);
        a.put("ELEXPR", util.EvaluateInString(elExpr, createEvaluationContext(request)));
        if (!a.containsKey("KEYLABEL"))
            a.put("KEYLABEL", label);
        if (a.get("ELEXPR") != null && !a.get("ELEXPR").toString().startsWith("$")
                && a.get("ELEXPR").toString().toString().length() > 0) {
            a.put("RULE", util.replace(label, ".", "_") + "-" + a.get("COMP") + "_" + getId("", request) + "#"
                    + a.get("ELEXPR").toString().toString());
        }
        // util.replace(a.get("KEYLABEL").toString(), ".", "_")
        a.put("id", getId(util.replace(a.get("KEYLABEL").toString(), ".", "_") + "-" + a.get("COMP") + "_", request));
        if (d.get(PANEL_NAME) == null) {
            final ArrayList<Map> d1 = new ArrayList<Map>();
            d1.add(a);
            d.put(PANEL_NAME, d1);
        } else {
            d.get(PANEL_NAME).add(a);
        }
        setReqAttr(request, Constants.BUTTONS, d);
    //    setReqAttr(request, "BUTTONS_MAIN_ACTION", getUser(request).getUtil().replace(d.toString(), new String[]{"[", "]"}
     //           , new String[]{"", ""}));

        // if (tipo != BUTTON.SEPARATOR) {
        // addButton(Constants.MAIN_ACTION, "",
        // BaseController.BUTTON.SEPARATOR, "", "", "",
        // BaseController.BUTTON.STATIC, "right", null, false, request);
        // }
    }

    public void setReqAttr(ServletRequest request, String key, Object value) {
        // TODO Auto-generated method stub
        getUser(request).getRequestEvaluationContext(request).setVariable(key, value);
        request.setAttribute(key, value);
    }

    private boolean evalPrintButton(Map campo, ServletRequest request) {
        // TODO Auto-generated method stub

        if (campo != null) {
            if (campo.get("printWhenExpression") != null) {
                return util.EvaluateBoolean((String) campo.get("printWhenExpression"),
                        createEvaluationContext(request));
            } else {
                if (campo.get("disableWhenExpression") != null && !is_list(request)) {
                    Map t = util.JSONtoMap(campo.get("disableWhenExpression").toString());
                    if (t.get("s") != null) {
                        return !util.EvaluateBoolean((String) t.get("s"), createEvaluationContext(request));

                    }


                }

                return true;
            }
        } else {
            return true;
        }

    }

    private void resolveAttributeOverwriteOfContextParams(Object ctxPrms, List contextFields) {

        if (ctxPrms == null || contextFields == null) {
            return;
        }

        Map contextParams = util.JSONtoMap(ctxPrms.toString().replaceAll("\\^", "\\\\\""));
        if (contextParams.get("@OVERRIDE_ATTR") != null) {
            Map<String, Object> injectItems = util.JSONtoMap(contextParams.get("@OVERRIDE_ATTR").toString());
            for (Entry<String, Object> itInj : injectItems.entrySet()) {
                if (itInj.getValue() != null) {
                    List matchField = util.match(contextFields, "name", itInj.getKey());
                    Map contextField = null;
                    if (matchField != null && matchField.size() > 0) {
                        contextField = (Map) matchField.get(0);
                    }
                    Map<String, Object> injectValues = util.JSONtoMap(itInj.getValue().toString());

                    for (Entry<String, Object> injVal : injectValues.entrySet()) {
                        contextField.put(injVal.getKey(), injVal.getValue());
                    }
                }
            }

        }
    }

    public void addCampo(final ServletRequest request, final Map campo) {
        if (is_detail(request) && campo.get("header") != null) {
            return;
        } else if ("volatile".equals(campo.get("nature"))) {
            return;
        }

        if (!ParseXmlService.canAccess(campo, getUser(request), true)) {
            return;
        }

        if (campo.get(Constants.component) != null
                && campo.get(Constants.component).toString().equalsIgnoreCase("imageSelector")) {
            if (campo.get("printWhenExpression") != null && util
                    .EvaluateBoolean((String) campo.get("printWhenExpression"), createEvaluationContext(request))) {

                addKey(campo.get(Constants.name).toString(), (String) campo.get("value"), request);

                return;
            } else {
                if (campo.get("printWhenExpression") == null) {
                    addKey(campo.get(Constants.name).toString(), (String) campo.get("value"), request);
                    return;
                }
                return;
            }
        } else {
            // Guardando o nome original do campo, caso seja alterado com o
            // prefixo do contexto,
            // para facilitar a identificao dos campos quando chegarem as
            // classes de componenetes. Para bind's, etc..
            if (campo.get(Constants.ORIGINAL_NAME) == null && !("context".equals(campo.get(Constants.component)))) {
                campo.put(Constants.ORIGINAL_NAME, campo.get(Constants.name));
            }

            if (campo.get(Constants.component) != null
                    && campo.get(Constants.component).toString().equalsIgnoreCase("context")) {
                setReqAttr(request, "is_context", true);
                // se a nature o volatil devo enviar os prints para os fields
                // pois eles vao ser usandos na view
                if (campo.get("printWhenExpression") != null && !campo.get("nature").equals("volatile")) {
                    Object v = util.Evaluate(campo.get("printWhenExpression").toString(),
                            createEvaluationContext(request));
                    if (v == null) {
                        v = Constants.false_str;
                    }
                    if (!new Boolean(v.toString())) {
                        return;
                    }
                }
                final Map par = new HashMap();

                if (campo.get("volatile") == null) {
                    addKey((String) campo.get(Constants.name), (String) campo.get("value"), request);
                }

                String ctxPar = null;
                Map p = null;
                Map bind = new HashMap();

                if (campo.get("bind") != null) {
                    String b = (String) campo.get("bind");
                    bind = util.JSONtoMap(b);
                }

                if (campo.get("contextParams") != null) {
                    ctxPar = getUser(request).getUtil().replace(
                            util.removeInvalidItemsFromContextParams(campo.get("contextParams").toString()), "!", "");

                    p = util.JSONtoMap(util.EvaluateInString(ctxPar, createEvaluationContext(request)));
                    // final Iterator xi = p.keySet().iterator();
                    //
                    // while (xi.hasNext()) {
                    // String object = (String) xi.next();
                    // par.put(campo.get(Constants.name) + "_" + object,
                    // p.get(object));
                    // }

                    getVO(request).putAll(p);
                }
                final List cont = util
                        .filterAndClone(
                                (List) ((GenericService) getService().getService(campo.get("service").toString(),
                                        getUser(request))).getProperties().get(Constants.ALL_FIELDS),
                                "type", "sqlrule"); // order

                resolveAttributeOverwriteOfContextParams(campo.get("contextParams"), cont);

                for (final Iterator iterator2 = cont.iterator(); iterator2.hasNext(); ) {
                    final Map object = (Map) ((HashMap) iterator2.next()).clone();
                    object.put(Constants.ORIGINAL_NAME, object.get(Constants.name));
                    object.put("parent_id", campo.get("parent_id"));

                    if (p != null && p.containsKey(object.get(Constants.name))) {
                        if (object.get(Constants.component) != null
                                && (object.get(Constants.component).equals("checkbox")
                                || p.get(object.get(Constants.name)).equals("hide"))) {

                            object.put(Constants.component, "hidden");
                        }

                        object.put("value", p.get(object.get(Constants.name)));

                        if (bind.containsKey((object.get(Constants.name)))) {

                            String b = bind.get((object.get(Constants.name))).toString();

                            if (campo.get("is_ctx") != null) {
                                b = util.replace(b, "%ctx%", "%ctx%" + campo.get("is_ctx") + "_");
                            }
                            object.put("bind", b);
                        }

                    }

                    String value = "";

                    if (!isDefaultCRUD(request) && object.get("value") != null
                            && getVO(request).get(object.get(Constants.name)) == null) {
                        if (object.get("value").toString().startsWith("${")) {
                            value = util.EvaluateInString(object.get("value").toString(),
                                    createEvaluationContext(request));
                        } else {
                            value = object.get("value").toString();
                        }
                    } else if (getVO(request).get(object.get(Constants.name)) != null) {
                        value = getVO(request).get(object.get(Constants.name)).toString();
                    }

                    object.put(Constants.FIELD_CONTEXT_PREFIX, campo.get(Constants.name) + "_");
                    object.put(Constants.IS_CONTEXT_FIELD, true);
                    setReqAttr(request, Constants.FIELD_CONTEXT_PREFIX, campo.get(Constants.name) + "_");

                    getVO(request).put(campo.get(Constants.name) + "_" + object.get(Constants.name), value);

                    object.put("is_ctx", campo.get(Constants.name));

                    object.put(Constants.name, campo.get(Constants.name) + "_" + object.get(Constants.name));

                    if (object.get("bind") != null && object.get("bind").toString().indexOf("%ctx%") == -1) {

                        String nb = "";
                        String[] b = object.get("bind").toString().split("$");
                        for (int i = 0; i < b.length; i++) {
                            if (b[i].indexOf("*") == -1) {
                                nb += campo.get(Constants.name) + "_" + b[i] + "$";
                            } else {
                                if (campo.get("is_ctx") != null && object.get("is_ctx") != null
                                        && !campo.get("is_ctx").equals(object.get("is_ctx")))
                                    nb += campo.get("is_ctx") + "_" + b[i] + "$";
                                else
                                    nb += b[i] + "$";
                            }
                        }
                        object.put("bind", nb.substring(0, nb.length() - 1));
                    }

                    if (object.get(Constants.component) != null && object.get(Constants.component).equals("file")
                            && is_detail(request)) {

                        object.put("key", campo.get("service") + "*" + campo.get(Constants.name) + "*"
                                + campo.get(Constants.name) + "_" + campo.get("key"));

                    }

					/*
                     * if (par.containsKey(campo.get(Constants.name) + "_" +
					 * object.get(Constants.name))) {
					 * addKey(campo.get(Constants.name) + "_" +
					 * object.get(Constants.name), (String)
					 * par.get(ex.get(Constants.name) + "_" +
					 * object.get(Constants.name)), request); continue; }
					 */
                    if (ctxPar != null) {
                        object.put("contextParams", ctxPar);
                    }
                    // somente se o contexto for volatil propaga as contraints
                    // para usar na view
                    if (campo.get("printWhenExpression") != null && campo.get("nature").equals("volatile")) {
                        object.put("printWhenExpression", campo.get("printWhenExpression"));
                    }

                    try {
                        if (object.get("bind") != null && object.get("bind").toString().indexOf(":") > -1) {
                            final int size = util.match(cont, Constants.name, object.get("bind").toString().substring(0,
                                    object.get("bind").toString().indexOf(":"))).size();
                            if (size > 0) {
                                // atualizando valores do contexto bind
                                // e
                                // acima name
                                object.put("bind", campo.get(Constants.name) + "_" + object.get("bind"));
                            }
                        }
                    } catch (final Exception exp) {
                        LogUtil.exception(exp, (Usuario) request.getAttribute(Constants.RAIN_USER));
                    }
                    addCampo(request, object);
                }
                setReqAttr(request, "is_context", false);
            } else if (campo.get(Constants.component) != null
                    && campo.get(Constants.component).toString().equalsIgnoreCase("propertygrid")) {

                if (campo.get("printWhenExpression") != null && !util
                        .EvaluateBoolean((String) campo.get("printWhenExpression"), createEvaluationContext(request))) {
                    return;
                } else {
                    addKey(campo.get(Constants.name).toString(), (String) campo.get("value"), request);
                }
            }

            if (request.getAttribute(Constants.LIST_FIELDS) == null) {
                setReqAttr(request, Constants.LIST_FIELDS, new ArrayList<Map>());
            }

            if (getService().getServiceId() == null || getService().getServiceId().equals(campo.get("parent_id"))
                    || campo.get("parent_id") == null
                    || (!isDefaultCRUD(request) && campo.get("parent_id").equals(getVO(request).getMethod()))) {

                final ArrayList<Map> cam = (ArrayList<Map>) request.getAttribute(Constants.LIST_FIELDS);

                cam.add(campo);

            }
        }
    }

    public StandardEvaluationContext createEvaluationContext(ServletRequest request) {
        // TODO Auto-generated method stub
        return getUser(request).getRequestEvaluationContext(request);
    }

    private boolean isContextCmpField(String name, List fields) {
        List res = util.match(fields, "name", name);
        if (res != null && res.size() > 0) {
            return true;
        }
        return false;
    }

    public void addKey(String chave, final String value, final ServletRequest request) {

        if (value != null && value.toString().length() > 0 && (getVO(request).get(chave) == null
                || getVO(request).get(chave).toString().startsWith("${") || value.startsWith("${"))) {
            chave = chave + ":" + value;
        }
        if (chave != null && chave.length() > 0 && !getRequestKeys(request).contains(chave)) {
            getRequestKeys(request).add(chave);
        }

    }

    private List getRequestKeys(ServletRequest request) {
        if (request.getAttribute(Constants.REQUEST_KEYS) == null)
            setReqAttr(request, Constants.REQUEST_KEYS, new ArrayList());

        return (List) request.getAttribute(Constants.REQUEST_KEYS);

    }

    public ModelAndView processDif(final ServletRequest request, final ServletResponse response) throws Exception {

        GenericVO old = service.obtem(getVO(request), getUser(request));
        old.putDiff(getVO(request));
        old.setMethod("altera");
        service.altera(old, getUser(request));
        setProcessed(request);
        return finish(request, response, null);
    }

    public final ModelAndView altera(final ServletRequest request, final ServletResponse response) throws Exception {
        setReqAttr(request, "is_update", Boolean.TRUE);
        setReqAttr(request, Constants.CRUD_NATURE, "U");
        addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.salvar", getVO(request).getMethod(), "",
                BUTTON.STATIC, null, null, false, request);
        addButton(Constants.MAIN_ACTION, "", BUTTON.CANCEL, "label.cancelar", "cancel", "", BUTTON.STATIC, null, null,
                false, request);
        getVO(request).setFiltro("");
        request.removeAttribute(Constants.FILTER_KEY);
        return altera_imp(request, response);
    }

    public abstract ModelAndView altera_imp(final ServletRequest request, final ServletResponse response)
            throws Exception;

    public void clearButtons(final ServletRequest request) {
        setReqAttr(request, Constants.BUTTONS, new HashMap<String, ArrayList<Map>>());
    }

    public void clearFields(final ServletRequest request) {
        setReqAttr(request, Constants.LIST_FIELDS, null);
    }

    protected void configureDownload(final HttpServletResponse response, final String contentType,
                                     final String fileName) {
        response.setContentType(contentType);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
    }

    public boolean containErrors(final ServletRequest request) {
        return false;
    }

    public boolean contains_filter(final ServletRequest request) {
        return request != null && request.getAttribute(Constants.NO_FILTER) == null
                && request.getParameter(Constants.NO_FILTER) == null;
    }

    public String createUrl(final ServletRequest request) {
        String url = ((HttpServletRequest) request).getRequestURL().toString();
        url = url.substring(url.indexOf(((HttpServletRequest) request).getContextPath()));
        if (url.indexOf(".") == -1) {
            return url;
        }
        setReqAttr(request, Constants.ActionID,
                url.substring(
                        url.lastIndexOf(getUser(request).getSystem()) == -1 ? url.lastIndexOf("/") + 1
                                : url.lastIndexOf(getUser(request).getSystem()),
                        url.lastIndexOf(".") == -1 ? url.length() : url.lastIndexOf(".")) + Constants.ACTION_SUFIX);
        final Enumeration e = getParameterNames(request);
        while (e.hasMoreElements()) {

            final String element = (String) e.nextElement();

            if (filterUrl(element)) {
                if (url.indexOf("?") < 0) {
                    url += "?";
                } else {
                    url += "&";
                }
                try {

                    url += element + "=" + (String) getParameter(request, element, false);
                } catch (final Exception ex) {
                }
            }
        }
        return util.encode(url, getUser(request));
    }

    public ModelAndView describe(final ServletRequest request, final ServletResponse response) throws Exception {
        getVO(request).setEncapsulate(false);
        getVO(request).put(Constants.RequestProcessed, request);

        return serializeJSOnResponse(request, response, getService().describe(getVO(request), getUser(request)), false);
    }

    public ModelAndView doProcess(final ServletRequest request, final ServletResponse response) throws Exception {
        requiredUser = true;
        FileUploadWrapper fuw = null;

        if (ServletFileUpload.isMultipartContent((HttpServletRequest) request)) {
            fuw = new FileUploadWrapper(request);
            fuw.setCharacterEncoding(getUser(request).getEncode());
            setReqAttr(request, Constants.FUW, fuw);
        }
        String method = (String) getParameter(request, Constants.ActionParam, false);
        getVO(request).setMethod(method);

        setReqAttr(request, Constants.ActionParam, method);

        try {

            if (ReflectionUtils.findMethod(this.getClass(), method,
                    new Class[]{ServletRequest.class, ServletResponse.class}) != null) {
                setReqAttr(request, Constants.DEFAULT_CRUD, true);

            } else {
                setReqAttr(request, Constants.DEFAULT_CRUD, false);
            }

            init(request, response);
            clearButtons(request);

            String m = getVO(request).getMethod();

            if (m == null) {
                throw new RuntimeException("Invalid Request Method");
            }
            getVO(request).put(Constants.CONTEXT_KEY, request.getAttribute(Constants.CONTEXT_KEY));
            // verificaPermissao("0", getUser(request));

            ModelAndView f = null;

            f = processRequestMethod(m, request, response);

            if (getParameter(request, "texto", false) != null && getVO(request).getMethod().startsWith("lista")) {
                setCabecalho((String) getParameter(request, "texto", false), request);
            }
            if (!processLista(request) && f != null) {
                // saveToken(arg2);
                return f;
            }

        } catch (final BusinessException ex) {
            LogUtil.exception(ex, (Usuario) request.getAttribute(Constants.RAIN_USER));
            clearButtons(request);
            String m = getVO(request).getMethod();
            if (m.equalsIgnoreCase("filtro") && process(request)) {
                m = "lista";
            }
            if (getService() != null && getService().getProperties() != null
                    && getService().getProperties().get("P_" + m.toUpperCase()) != null) {
                setHelpId((String) getService().getProperties().get("P_" + m.toUpperCase()), request);
            }

            final StringBuilder sb = new StringBuilder();
            String cl = "";
            if (ex.isClose()) {
                cl = "closeWindow(\"" + getFormName(request) + "\",\"" + getVO(request).getMethod() + "\")";
            }
            response.setContentType("application/javascript; charset=" + getUser(request).getEncode());

            sb.append("({success:false," + Constants.TOKEN + ":'" + nextToken((HttpServletRequest) request)
                    + "', error: { form_id:'" + getId("", request) + "' ,  extra:'" + cl + ex.getExtra()
                    + "',reason:'");

            String key = "";
            try {
                key = ex.getCause().getMessage();
            } catch (Exception e) {
                // TODO: handle exception
                key = ex.getMessage();
            }

            sb.append(getUser(request).getLabel(key, ex.getValues()));
            sb.append("'}})");
            // response.setContentType("application/x-json; charset="
            // + Constants.encode);
            response.getWriter().write(sb.toString());
            // setReqAttr(request,"forward", getParameter(request,
            // "forward"));
            if (getVO(request).getMethod().equalsIgnoreCase("remove")) {
                getVO(request).setMethod("lista");
            }

            if (!processLista(request)) {
                return null;
                // return super.dispatchMethod(arg0, arg1, arg2, arg3, method);
            }

        } catch (final MessageException ex) {
            LogUtil.exception(ex, (Usuario) request.getAttribute(Constants.RAIN_USER));
            String m = getVO(request).getMethod();
            if (m.equalsIgnoreCase("remove") || m.equalsIgnoreCase("filtro")
                    || m.equalsIgnoreCase("chaves") && process(request)) {
                m = "lista";
            }
            if (getService() != null && getService().getProperties() != null
                    && getService().getProperties().get("P_" + m.toUpperCase()) != null) {
                setHelpId((String) getService().getProperties().get("P_" + m.toUpperCase()), request);
            }

            // saveErrors(arg2, erros);
            // setReqAttr(request,"forward", getParameter(request,
            // "forward"));
            if (getVO(request).getMethod().equalsIgnoreCase("remove")) {
                getVO(request).setMethod("lista");
            }
            if (!processLista(request)) {
                return processRequestMethod(getVO(request).getMethod(), request, response);
            }

        } catch (final PermissionException ex) {
            LogUtil.exception(ex, (Usuario) request.getAttribute(Constants.RAIN_USER));
            clearButtons(request);
            return new ModelAndView("permissao");

        } catch (final SessionExpiredException ex) {
            LogUtil.exception(ex, (Usuario) request.getAttribute(Constants.RAIN_USER));
            clearButtons(request);
            requiredUser = false;

            throw ex;
        } catch (StreamingException re) {
            LogUtil.exception(re, (Usuario) request.getAttribute(Constants.RAIN_USER));
            String msg = "";
            if (re.getExtra() == null && re.getCause() != null) {
                msg = ((StreamingException) re.getCause()).getExtra();
            } else if (re.getExtra() != null) {
                msg = re.getExtra();
            } else {
                msg = re.getMessage();
            }

            String out = "<script>" + "(function(){" + "window.top.sysAlert('" + msg + "');" + "})()" + "</script>";
            response.getWriter().write(out);

        } catch (final Exception e) {
            LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
            final StringBuilder sb = new StringBuilder();
            sb.append("({failure:true , error:{extra:'',reason:'");
            String key = "";
            try {
                key = e.getCause().getMessage();
            } catch (Exception ex) {
                // TODO: handle exception
                key = e.getMessage();
            }

            sb.append(getUser(request).getLabel(key, new ArrayList<String>().toArray()));
            sb.append("'}})");
            LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
            response.getWriter().write(sb.toString());
        }
        return null;
    }

    public ModelAndView download(final ServletRequest request, final ServletResponse response) throws Exception {

        if (getParameter(request, "key", false) == null) {
            getVO(request).putKey("NOME_CONFIG_SISTEMA", getVO(request).get("NOME_CONFIG_SISTEMA"));
        } else {
            getVO(request).putKey(getParameter(request, "key", false),
                    getVO(request).get(getParameter(request, "key", false)));
        }
        getVO(request).setLazyList(true);
        getVO(request).setMethod("obtem");
        final Map r = getService().obtem(getVO(request), getUser(request));
        getVO(request).setMethod("download");
        final byte[] br = (byte[]) r.get(getParameter(request, "download_property", false));

        final ByteArrayInputStream fileInputStream = new ByteArrayInputStream(br);

        if (getParameter(request, "fileNameDescriptor", false) != null) {
            ((HttpServletResponse) response).setHeader("Cache-Control", "no-cache");
            ((HttpServletResponse) response).setHeader("Pragma", "no-cache");
            ((HttpServletResponse) response).setDateHeader("Expires", System.currentTimeMillis());
            configureDownload((HttpServletResponse) response, "application/octet-stream",
                    (String) r.get(getParameter(request, "fileNameDescriptor", false)));

        } else {
			/*
			 * File f = util.writeToTmp(br,
			 * r.get(getVO(request).getKey()).toString() ); String ct = new
			 * MimetypesFileTypeMap().getContentType(f); f.delete();
			 */
            String ct = URLConnection.guessContentTypeFromStream(fileInputStream);
            response.setContentType(ct);
        }

        int i;

        if (request.getParameter("enc") != null) {
            if (request.getParameter("enc").equalsIgnoreCase("PNG")) {
                response.setContentType("image/png");
            }
            if (request.getParameter("enc").equalsIgnoreCase("FILE")) {
                ((HttpServletResponse) response).setContentLength(br.length);
                ((HttpServletResponse) response).setHeader("Cache-Control", "no-cache");
                ((HttpServletResponse) response).setHeader("Pragma", "no-cache");
                ((HttpServletResponse) response).setDateHeader("Expires", System.currentTimeMillis());
            }
        }

        while ((i = fileInputStream.read()) != -1) {
            response.getOutputStream().write(i);
        }
        fileInputStream.close();
        response.getOutputStream().flush();
        return null;
    }

    public final ModelAndView exporta(final ServletRequest request, final ServletResponse response) throws Exception {
        final ServiceImpl service = (ServiceImpl) getService();
        final String fileName = service.getName().replaceAll("Service", "");
        configureDownload((HttpServletResponse) response, "text/xml", fileName + ".xml");
        service.exportBean(getVO(request), getUser(request), response.getWriter());
        return null;
    }

    public ModelAndView finish(final ServletRequest request, final ServletResponse response, final String extra)
            throws Exception {

        try {
            if (extra != null) {
                response.setCharacterEncoding(getUser(request).getEncode());
                // ("application/x-json; charset="
                // + Constants.encode);
                response.setContentType("application/javascript; charset=" + getUser(request).getEncode());

                response.getWriter()
                        .write("({success: true, " + Constants.TOKEN + ":'" + nextToken((HttpServletRequest) request)
                                + "',  extra:'" + util.clearForJSON(util.encode(extra, getUser(request))) + "'})");
                return null;
            } else if (processed(request)) {

                return refresh(request, response);

            }

        } catch (final IOException e) {
            LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
        }
        return new ModelAndView("/editar");
        // return lista(request, response);
    }

    public String nextToken(HttpServletRequest request) {
        long seed = System.currentTimeMillis();
        Random r = new Random();
        r.setSeed(seed);
        String t = Long.toString(seed) + Long.toString(Math.abs(r.nextLong()));
        request.getSession().setAttribute(Constants.TOKEN, t);
        return t;
    }

    protected boolean isTokenValid(HttpServletRequest request) {

        HttpSession session = request.getSession();
        String sessionToken = (String) session.getAttribute(Constants.TOKEN);
        String requestToken = (String) getParameter(request, Constants.TOKEN, false);
        if (requestToken == null) {
            // The hidden field wasn't provided
            throw new BusinessException("label.invalidRequestToken");
        }
        if (sessionToken == null) {
            // The session has lost the token.
            throw new BusinessException("label.invalidToken");
        }
        if (sessionToken.equals(requestToken)) {
            session.removeAttribute(Constants.TOKEN);
            return true;
        } else {
            getUser(request).addActionMessage(request, "label.tokenProcessed", null, false);
        }
        return false;

    }

    protected ModelAndView refresh(final ServletRequest request, final ServletResponse response) throws IOException {

        final boolean b = refreshMaster(request);
        String cl = "";
        String closeWindow = "";
        if (!isNext(request) && getVO(request).isProcess()) {
            closeWindow = "closeWindow(\"" + getFormName(request) + "\",\"" + getVO(request).getMethod() + "\");";
        }

        LinkedHashSet userMessages = getUser(request).getActionMessages(request);
        String messagesStr = "";
        if(userMessages!=null)
        for (Iterator iterator = userMessages.iterator(); iterator.hasNext(); ) {
            Map ms = (Map) iterator.next();
            if (ms.get("CLOSE") != null && !new Boolean("" + ms.get("CLOSE"))) {
                closeWindow = "";
            }
            messagesStr += getUser(request).getLabel((String) ms.get("MESSAGE")) + "</br>";
        }
        messagesStr = util.replaceLastString(messagesStr, "</br>", "");
        if (messagesStr != null && !messagesStr.isEmpty()) {
            cl += " sysAlert(\"" + messagesStr + "\"); ";
        }
        Object pk = "";
        if (getVO(request).getOld_VO() != null) {
            pk = getVO(request).getOld_VO().get(getPrimaryKey());
        }
        Object mpk = "";

        ServiceImpl ms = getService().getParent(getUser(request));
        if (is_sharing(request))
            ms = (ServiceImpl) getService().getServiceById((String) getParameter(request, Constants.CONTEXT_KEY, false),
                    getUser(request));
        if (getVO(request).getOld_VO() != null
                && (getService().getProperties().get("type").equals("D") || is_sharing(request))) {

            if (is_sharing(request))
                mpk = ms.getName() + "_" + getVO(request).get("VALORCHAVE");
            else
                mpk = ms.getName() + "_" + getVO(request).getOld_VO().get(ms.getPrimaryKey().get(Constants.name));
        } else {
            mpk = getService().getName() + "_" + pk;
        }

        String cn = (String) request.getAttribute(Constants.CRUD_NATURE);
        if (cn != null && cn.equals("C")) {
            if ((getActionProperty(request) != null && (getActionProperty(request).get("detailAfterInsert") != null
                    && getActionProperty(request).get("detailAfterInsert").toString().equals(Constants.false_str)))) {
                cn = "N";
            }
        } else if (cn != null && cn.equals("U")) {
            if (!((getActionProperty(request) != null && (getActionProperty(request).get("detailAfterUpdate") != null
                    && getActionProperty(request).get("detailAfterUpdate").toString().equals(Constants.true_str))))) {
                cn = "N";
            }
        }
        if (!isNext(request) && (!getVO(request).isProcess() || (closeWindow == null || closeWindow.isEmpty())))
            cn = "F"; // nao processado

        response.getWriter()
                .write("({success: true, " + Constants.TOKEN + ":'" + nextToken((HttpServletRequest) request)
                        + "',  extra:'" + closeWindow + "refresh(\"" + getFormName(request) + "\", \""
                        + util.replace(getUser(request).getLabel(getService().getProperties().get("label").toString()),
                        " ", "_")
                        + "\",\"" + getVO(request).getMethod() + "\", \"" + mpk + "\"" + ",  " + b + " ,\"" + pk
                        + "\",\"" + cn + "\");" + cl + " '})");

        return null;
    }

    public String getActionId(final ServletRequest request) {
        return (String) request.getAttribute(Constants.ActionID);
    }

    public String getActionName(final ServletRequest request) {
        return getActionId(request).substring(0, getActionId(request).lastIndexOf("."));
    }

    public String getBaseName(final ServletRequest request) {
        return getActionName(request).replaceAll(Constants.ACTION_BASE, "");
    }

    public String getChave(final ServletRequest request) {
        String ret = null;
        final String[] multipleId = getVO(request).getMultipleId();
        if (multipleId != null && multipleId.length == 0) {
            if (getPrimaryKey() == null || getVO(request).get(getPrimaryKey()) == null) {
                ret = null;
            } else {
                return getVO(request).get(getPrimaryKey()).toString();
            }
        } else {
            if (multipleId != null && multipleId.length > 0 && multipleId[0] != null
                    && multipleId[0].toString().trim().length() > 0 && !"null".equals(multipleId[0])) {
                getVO(request).put(getPrimaryKey(), new Long(multipleId[0]));
                return multipleId[0].toString();
            } else {
                try {
                    return getVO(request).get(getPrimaryKey()).toString();
                } catch (final Exception e) {
                    ret = null;
                }
            }
            if (ret == null) {
                ret = (String) getVO(request).get(getMainField(request));
            }
            return ret;
        }
        return ret;

    }

    public String[] getChaves(final ServletRequest request) {
        final String[] multipleId = getVO(request).getMultipleId();
        if (multipleId.length == 0 || multipleId[0].toString().trim().length() == 0) {
            setReqAttr(request, "cont_chaves", "1");
            return new String[]{getVO(request).get(getPrimaryKey()).toString()};
        } else {
            setReqAttr(request, "cont_chaves", "" + multipleId.length);
            return getVO(request).getMultipleId();
        }

    }

    public Object getCRUDKey(final ServletRequest request) {
        if (is_list(request)) {
            return "L";
        }
        if (is_insert(request)) {
            return "C";
        }
        if (is_update(request)) {
            return "U";
        }
        if (is_detail(request)) {
            return "R";
        }
        if (is_remove(request)) {
            return "D";
        }
        return "";

    }

    public String getFormName(final ServletRequest request) {
        return request.getAttribute("FormName").toString();
    }

    public String getIcon(final String label, final ServletRequest request) {
        return getUser(request).getSystemPath() + "/ItemLinguagemAction.do?" + Constants.ActionParam
                + "=icon&amp;NOME_CONFIG_SISTEMA=" + getUser(request).getLang() + "*" + label
                + "&amp;download_property=ARQUIVO_CONFIG_SISTEMA&searchBy=cache";
    }

    public String getId(String id, final ServletRequest request) {

        String str = "*";
        if (is_sharing(request)) {
            str = getId_contexto();
        }

        try {
            str = getVO(request).get(getService().getPrimaryKey().get(Constants.name)).toString();
        } catch (final Exception e) {
            if (getVO(request).get("VALORCHAVE") != null) {
                str = "" + getVO(request).get("VALORCHAVE");
            }
        }

        if (str.equalsIgnoreCase("*") || str.equalsIgnoreCase("")) {
            try {
                str = getVO(request).get(getService().getParent(getUser(request)).getPrimaryKey().get(Constants.name))
                        .toString();
            } catch (final Exception e) {
                // LogUtil.exception(e, (Usuario)
                // request.getAttribute(Constants.RAIN_USER));
            }
        }
        String pre = "";
        // if (container != null)
        // pre = container.getName() ;
        // else
        // {
        pre = (String) request.getAttribute(Constants.VOName);
        // }

        if (id.toString().equalsIgnoreCase(pre)) {
            pre = "";
        }
        if (str.trim().length() == 0) {
            str = "*";
        }

        id = id + pre + "_" + str + "_" + getVO(request).getMethod();

        return id;

    }

    protected String getId_contexto() {
        try {
            return getService().getProperties().get("id").toString();
        } catch (final Exception e) {
            return "";
        }
    }

    private Object getId_contexto_master(final Usuario user) {
        try {
            return getService().getParent(user).getProperties().get("id").toString();
        } catch (final Exception e) {
            return "";
        }
    }

    public String getMainField(final ServletRequest request) {

        if (mf != null) {
            return mf;
        }

        if (isDefaultCRUD(request)) {

            if (getActionProperty(request) != null && getActionProperty(request).get("mainFieldEvaluator") != null) {
                final Map evals = (Map) getActionProperty(request).get("mainFieldEvaluator");
                final Iterator iterator = evals.keySet().iterator();

                while (iterator.hasNext()) {
                    final String object = (String) iterator.next();
                    if (util.EvaluateBoolean(object, createEvaluationContext(request))) {
                        return (String) evals.get(object);
                    }
                }

            }
            Map mfg = null;
            try {
                mfg = getService().getMainField(getVO(request).getMethod(), getUser(request));
            } catch (final Exception e) {
                return null;
            }

            if (mfg != null) {
                try {
                    if (mfg.get("calculated") == null) {
                        return (String) mfg.get(Constants.name);
                    } else {
                        return ((Map) util.match((List) getService().getProperties().get(Constants.ALL_FIELDS),
                                "required", Constants.true_str).get(0)).get(Constants.name).toString();
                    }
                } catch (final Exception e) {
                    LogUtil.fatal("Service " + getService().getProperties().get(Constants.name) + " sem MainField.");
                }

            }
            return null;

        } else {
            try {
                final Map m = (Map) util
                        .match((List) getService().getMethod(getVO(request).getMethod(), getUser(request))
                                .get(Constants.ALL_FIELDS), "required", Constants.true_str)
                        .get(0);
                if (m.get(Constants.component) != null
                        && m.get(Constants.component).toString().equalsIgnoreCase("context")) {

                    final GenericService d = (GenericService) getService().getService(m.get("service").toString(),
                            getUser(request));
                    Map c = d.getMainField(getVO(request).getMethod(), getUser(request));
                    if (c.get("calculated") != null) {
                        c = (Map) util.match((List) d.getProperties().get(Constants.ALL_FIELDS), "required",
                                Constants.true_str).get(0);
                    }
                    d.unBindVOContext((GenericVO) getVO(request).clone(), getVO(request),
                            (String) m.get(Constants.name), getUser(request));

                    return m.get(Constants.name) + "_" + c.get(Constants.name);
                }
                return m.get(Constants.name).toString();
            } catch (final Exception e) {
                return (String) getService().getPrimaryKey().get(Constants.name);
            }
        }
    }

    public Object getParameter(final ServletRequest request, final String actionParam, final boolean isArray) {

        if (request == null) {
            return null;
        }
        // if (actionParam != null && actionParam.equalsIgnoreCase("session")) {
        // return request.getParameter("session");
        // }

        if (request.getAttribute(Constants.FUW) != null) {
            try {
                FileUploadWrapper fuw = ((FileUploadWrapper) request.getAttribute(Constants.FUW));
                if (fuw.getFileItem(actionParam) == null) {
                    if (isArray) {
                        return fuw.getParameterValues(actionParam);
                    }
                    return util.decode(fuw.getParameter(actionParam), getUser(request));
                } else {
                    String name = new String(
                            fuw.getFileItem(actionParam).getName().getBytes(request.getCharacterEncoding()),
                            getUser(request).getEncode());
                    String ct = fuw.getFileItem(actionParam).getContentType();
                    if (name.lastIndexOf("/") > -1) {
                        name = name.substring(name.lastIndexOf("/") + 1);
                    }
                    if (name.lastIndexOf("\\") > -1) {
                        name = name.substring(name.lastIndexOf("\\") + 1);
                    }
                    getVO(request).put("FILE_NAME_" + actionParam, name);
                    getVO(request).put("CONTENT_TYPE_" + actionParam, ct);
                    InputStream is = fuw.getFileItem(actionParam).getInputStream();
                    String st = getUser(request).getSystemProperty("LOCALIZACAO_MAX_UPLOAD_SIZE");
                    if (st != null && Integer.parseInt(st) < is.available()) {
                        throw new BusinessException(getUser(request).getLabel("MAX_UPLOAD_SIZE_EXEDED"));
                    }
                    return is;
                }

            } catch (final IOException e) {
                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                return null;
            }

        } else {
            if (isArray) {
                String[] s = request.getParameterValues(actionParam);
                if (s.length == 1 && s[0].indexOf(',') > -1) {
                    s = util.decode(s[0], getUser(request)).split(",");
                }

                return util.decode(s, getUser(request));
            } else {
                // if ( false && request.getCharacterEncoding() != null &&
                // request.getCharacterEncoding() != Constants.encode)
                // try {
                // if(request.getParameter(
                // actionParam)==null)
                // return null;
                //
                // return util.decode(new String(request.getParameter(
                // actionParam).getBytes(
                // request.getCharacterEncoding()),
                // Constants.encode));
                // } catch (UnsupportedEncodingException e) {
                // e.printStackTrace();
                // return util.decode(request.getParameter(actionParam));
                // }
                // else
                try {
                    return util.decode(request.getParameter(actionParam), getUser(request));
                } catch (Exception e) {
                    return request.getParameter(actionParam);
                }
            }
        }
    }

    public Enumeration getParameterNames(final ServletRequest request) {
        if (request.getAttribute(Constants.FUW) != null) {
            try {
                return ((FileUploadWrapper) request.getAttribute(Constants.FUW)).getParameterNames();
            } catch (final Exception e) {
                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                return null;
            }
        } else {
            return request.getParameterNames();
        }
    }

    public String getPrimaryKey() {
        return (String) getService().getProperties().get(Constants.NOME_CHAVE);
    }

    public String getRelationKey(final ServletRequest request) {
        return (String) getService().getProperties().get(Constants.RELATION_KEY);
    }

    public GenericService getService() {
        return service;
    }

    public Object getService(final String service2, final Usuario user) {
        return getService().getService(service2, user);
    }

    public HttpSession getSession(final ServletRequest request) {
        return ((HttpServletRequest) request).getSession();
    }

    public Usuario getUser(final ServletRequest request) {
        Usuario user = null;

        if (request != null && request.getAttribute(Constants.RAIN_USER) != null) {
            return (Usuario) request.getAttribute(Constants.RAIN_USER);
        }

        if (request != null && getSession(request) != null) {
            user = (Usuario) getSession(request).getAttribute(Constants.RAIN_USER);
        }
        if (user == null) {
            user = new Usuario();

            if (request != null) {
                // getSession(request).removeAttribute(Constants.RAIN_USER);
                getSession(request).setAttribute(Constants.RAIN_USER, user);
            }

        } else if (requiredUser() && request.getParameter(Constants.ActionParam) != null
                && !"login".equals(request.getParameter(Constants.ActionParam))) {
            request.setAttribute(Constants.RAIN_USER, user);
            // Usuarios log = (Usuarios) getService("Usuarios", user);

            if (user.getIdUsuario() != null && request.getAttribute("USER_REQUEST_UPDATED") == null) {
                // log.update(user);
                request.setAttribute("USER_REQUEST_UPDATED", true);
            }
        }
        Object  p = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(p instanceof  Usuario)
        {
            user.setNome(((Usuario) p).getNome());
                user.setIdUsuario(((Usuario) p).getIdUsuario());
                user.setLogin(((Usuario) p).getLogin());

        }
        if (request.getParameter("LANG") != null) {
            user.setLang(new Long(request.getParameter("LANG").toString()));
        }


        return user;

    }

    public GenericVO getVO(final ServletRequest request) {
        try {
            if (isSessionForm()) {
                return (GenericVO) WebUtils.getOrCreateSessionAttribute(getSession(request), Constants.VOKey,
                        GenericVO.class);
            } else {
                final GenericVO vo = null;
                if (request.getAttribute(Constants.VOKey) == null) {
                    {
                        setReqAttr(request, Constants.VOKey, getService().createVO(getUser(request)));
                    }
                }
                return (GenericVO) request.getAttribute(Constants.VOKey);
            }
        } catch (final Exception e) {
            LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER)); // nao
            // temos
            // sessao
            // valida
            setReqAttr(request, Constants.VOKey, new GenericVO());
            return (GenericVO) request.getAttribute(Constants.VOKey);

        }
    }

    @Override
    public ModelAndView handleRequest(final HttpServletRequest arg0, final HttpServletResponse arg1) throws Exception {
        arg0.getSession().getLastAccessedTime();
        return doProcess(arg0, arg1);
    }

    public ModelAndView icon(final ServletRequest request, final ServletResponse response) throws Exception {
        try {
            if (getParameter(request, "key", false) == null) {
                getVO(request).putKey("NOME_CONFIG_SISTEMA", getVO(request).get("NOME_CONFIG_SISTEMA"));
            } else {
                getVO(request).putKey(getParameter(request, "key", false),
                        getVO(request).get(getParameter(request, "key", false)));
            }
            getVO(request).setLazyList(true);
            final Map r = getService().obtem(getVO(request), getUser(request));
            final ByteArrayInputStream fileInputStream = new ByteArrayInputStream(
                    (byte[]) r.get(getParameter(request, "download_property", false)));
            if (getParameter(request, "sz", false) == null) {
                ImageIO.write(util.resize(ImageIO.read(fileInputStream), 30, 30), "png", response.getOutputStream());
            } else if (getParameter(request, "sz", false).toString().equalsIgnoreCase("none")) {
                if (request.getParameter("enc") != null) {
                    if (request.getParameter("enc").equalsIgnoreCase("PNG")) {
                        response.setContentType("image/png");
                    }
                }
                int i;
                while ((i = fileInputStream.read()) != -1) {
                    response.getOutputStream().write(i);
                }
                fileInputStream.close();
                response.getOutputStream().flush();

            } else {
                ImageIO.write(
                        util.resize(ImageIO.read(fileInputStream),
                                Integer.parseInt(getParameter(request, "sz", false).toString()),
                                Integer.parseInt(getParameter(request, "sz", false).toString())),
                        "png", response.getOutputStream());
            }

            response.getOutputStream().flush();
            return null;
        } catch (final Exception e) {
            LogUtil.debug("Erro pegando icon:" + getVO(request).get(getVO(request).getKey()), getUser(request));
            return null;
        }
    }

    public final ModelAndView importa(final ServletRequest request, final ServletResponse response) throws Exception {

        final Map cmp = new HashMap(); // criando oo file

        if (getParameter(request, "UP_FILE", false) == null) {
            clearFields(request);
            setReqAttr(request, "is_insert", Boolean.TRUE);
            setReqAttr(request, "is_win", Boolean.TRUE);
            if (getParameter(request, "editar_form", false) != null) {
                setReqAttr(request, "editar_form", true);
            }
            addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.salvar", "importa",
                    "functionvalidation.validate", BUTTON.STATIC, null, null, false, request);
            addButton(Constants.MAIN_ACTION, "", BUTTON.CANCEL, "label.cancelar", "", "", BUTTON.STATIC, null, null,
                    false, request);
            cmp.put(Constants.component, "file");
            cmp.put(Constants.name, "UP_FILE");
            cmp.put("label", "UP_FILE");
            cmp.put("required", Constants.true_str);
            cmp.put("colspan", "4");
            addCampo(request, cmp);
            setReqAttr(request, Constants.WINDOW_SIZE, "{w:500,h:200}");
            return finish(request, response, null);
        }
        GenericVO vo = getVO(request);
        vo.setImport(true);
        new XMLImporter((ServiceImpl) getService(), getUser(request))
                .process((InputStream) getParameter(request, "UP_FILE", false), vo);

        return finish(request, response,
                "closeWindow(\"" + getFormName(request) + "\",\"" + getVO(request).getMethod() + "\");");
    }

    public final ModelAndView inclui(final ServletRequest request, final ServletResponse response) throws Exception {
        setReqAttr(request, "is_insert", Boolean.TRUE);
        setReqAttr(request, "is_win", Boolean.TRUE);
        setReqAttr(request, Constants.CRUD_NATURE, "C");
        if (getParameter(request, "editar_form", false) != null) {
            setReqAttr(request, "editar_form", true);
        }

        Map m = new HashMap();
        String label = "label.salvar";
        String label2 = "label.salvareproximo";
        String label3 = "label.cancelar";

        if (getActionProperty(request) != null) {
            m = (Map) getActionProperty(request).get(getVO(request).getMethod());
            if (m == null)
                m = new HashMap();
            if (m.containsKey("saveLabel")) {
                label = (String) m.get("saveLabel");

            }
            if (m.containsKey("saveNextLabel")) {
                label2 = (String) m.get("saveNextLabel");

            }
            if (m.containsKey("cancelLabel")) {
                label3 = (String) m.get("cancelLabel");

            }
        }

        addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, label, getVO(request).getMethod(),
                "functionvalidation.validate", BUTTON.STATIC, null, null, false, request);
        if (isDefaultCRUD(request)
                && (getActionProperty(request) == null || (getActionProperty(request).get("actionNext") == null
                || getActionProperty(request).get("actionNext").toString().equals(Constants.true_str)))) {

            addButton(Constants.MAIN_ACTION, "", BUTTON.ACTIONNEXT, label2, getVO(request).getMethod(),
                    "functionvalidation.validate", BUTTON.STATIC, null, null, false, request);
        }

        addButton(Constants.MAIN_ACTION, "", BUTTON.CANCEL, label3, getVO(request).getMethod(), "", BUTTON.STATIC, null,
                null, false, request);
        getVO(request).setFiltro("");
        request.removeAttribute(Constants.FILTER_KEY);
        return inclui_imp(request, response);
    }

    public abstract ModelAndView inclui_imp(final ServletRequest request, final ServletResponse response)
            throws Exception;


    private final void init(final ServletRequest request, final ServletResponse response) throws Exception {

        final String action = createUrl(request);
        // LogUtil.debug(action);
        setReqAttr(request, Constants.ACTION_BASE, action);
        setReqAttr(request, Constants.BUTTONS, new HashMap<String, ArrayList<Map>>());

        try {
            ((HttpServletRequest) request).getSession().setMaxInactiveInterval(
                    Integer.parseInt(getUser(request).getSystemProperty("SESSION_TIMEOUT")) * 60);
        } catch (Exception ex) {
            ((HttpServletRequest) request).getSession().setMaxInactiveInterval(60 * 60);
        }

        setReqAttr(request, "sessionTime", util.getSessionTimeOutl(request));
        setReqAttr(request, "NOMECHAVE", getPrimaryKey());
        setReqAttr(request, "CONTEXTO", getId_contexto());
        setReqAttr(request, "SERVICE_NAME", getService().getName());
        setReqAttr(request, Constants.RESPONSE, response);
        setReqAttr(request, Constants.REQUEST, request);
        final String m = request.getParameter(Constants.ActionParam);
        setReqAttr(request, "is_list", Boolean.FALSE);
        setReqAttr(request, "is_detail", Boolean.FALSE);
        setReqAttr(request, "is_update", Boolean.FALSE);
        setReqAttr(request, "is_win", Boolean.FALSE);
        setReqAttr(request, "is_remove", Boolean.FALSE);



        if (m != null && m.startsWith("obtemLista")) {
            setReqAttr(request, "is_list", Boolean.TRUE);

        } else if (m != null && m.startsWith("obtem")) {
            setReqAttr(request, "is_detail", Boolean.TRUE);
        } else if (m != null && m.startsWith("altera")) {
            setReqAttr(request, "is_update", Boolean.TRUE);
            setReqAttr(request, "is_win", Boolean.TRUE);
        } else if (m != null && m.startsWith("inclui")) {
            setReqAttr(request, "is_insert", Boolean.TRUE);
            setReqAttr(request, "is_win", Boolean.TRUE);
        } else if (m != null && m.startsWith("lista")) {
            setReqAttr(request, "is_list", Boolean.TRUE);

        }

        if (!(getParameter(request, "dir", false) == null)) {
            getVO(request).setDir((String) getParameter(request, "dir", false));
        }
        if (getParameter(request, "sort", false) != null) {

            getVO(request).setSort((String) getParameter(request, "sort", false));
        }
        if (getParameter(request, "search_by", false) != null) {
            getVO(request).searchBy(getParameter(request, "search_by", false));
        }
        if (getParameter(request, "start", false) == null) {
            getVO(request).setStart("0");
        } else {
            getVO(request).setStart((String) getParameter(request, "start", false));
        }
        if (getParameter(request, "limit", false) == null) {
            getVO(request).setLimit("10");
        } else {
            getVO(request).setLimit((String) getParameter(request, "limit", false));
        }

        init_imp(request, response);

        if (request.getParameter("VALORCHAVE") != null) {
            setReqAttr(request, "VALORCHAVE", request.getParameter("VALORCHAVE"));
        } else {
            setReqAttr(request, "VALORCHAVE", getService().getServiceId());
        }

        if (request.getParameter("is_sharing") != null) {
            setReqAttr(request, "is_sharing", Boolean.TRUE);
            addKey(Constants.CONTEXT_KEY, request.getParameter(Constants.CONTEXT_KEY), request);
            setReqAttr(request, Constants.CONTEXT_KEY, request.getParameter(Constants.CONTEXT_KEY));
            getSession(request).setAttribute(Constants.CONTEXT_KEY, request.getParameter(Constants.CONTEXT_KEY));
            addKey("VALORCHAVE", request.getAttribute("VALORCHAVE").toString(), request);
            addKey("is_sharing", "true", request);
        } else {
            setReqAttr(request, "is_sharing", Boolean.FALSE);
            setReqAttr(request, Constants.CONTEXT_KEY, getId_contexto());
            getSession(request).setAttribute(Constants.CONTEXT_KEY, getId_contexto());
        }

        setReqAttr(request, "user", getUser(request));
        setReqAttr(request, "LANG", getUser(request).getLang());
        if (requiredUser) {// valores de usuarios no request e outros valores
            // para substituicao via regexp

            setReqAttr(request, "nome_user", getUser(request).getNome());
            setReqAttr(request, "id_user", getUser(request).getIdUsuario());
            setReqAttr(request, "atual_date",
                    util.df.fromTimestampcomHoraptBR(new Timestamp(new java.util.Date().getTime())));

        }
        setReqAttr(request, "chave", getPrimaryKey());
        setReqAttr(request, Constants.VOName,
                getActionName(request).replaceAll(Constants.ACTION_BASE, Constants.VOKey));
        // setReqAttr(request,Constants.ActionID, getActionId(request));
        // setReqAttr(request,Constants.ActionName, getActionName(request));
        setReqAttr(request, Constants.REQUEST_KEYS, getRequestKeys(request));

        setReqAttr(request, Constants.CONTROLER, this);

        setReqAttr(request, Constants.CONTEXT_MASTER_KEY, getId_contexto_master(getUser(request)));
        getSession(request).setAttribute(Constants.CONTEXT_MASTER_KEY, getId_contexto_master(getUser(request)));

        final StringBuffer url = ((HttpServletRequest) request).getRequestURL();
        setReqAttr(request, Constants.SERVER_URL,
                url.substring(0, (url.indexOf(((HttpServletRequest) request).getContextPath())
                        + ((HttpServletRequest) request).getContextPath().length())));

        if (!getVO(request).getErrors().isEmpty()) {
            for (final Object element : getVO(request).getErrors()) {
                final Map object = (Map) element;
                throw new BusinessException("Formato do campo " + object.get(Constants.name).toString() + " invlido!",
                        "Ext.getCmp(\\\"" + object.get(Constants.name) + getFormName(request)
                                + "\\\").markInvalid(\\\"valor Invlido\\\");");
            }
        }
        addKey(Constants.KeyCollection, "", request);
        addKey(Constants.ActionParam, "", request);
        addKey(Constants.FILTER_KEY, "", request);
        if (is_insert(request)) {
            addKey(Constants.NEXT_KEY, "", request);
        }

    }

    public void init_imp(final ServletRequest request, final ServletResponse response) throws Exception {

        addKey(getPrimaryKey(), null, request);

        if (getService().getProperties() != null) {
            String l = "";
            if (getService().getProperties().get("label") != null) {
                l = (String) getService().getProperties().get("label");
            } else if (getService().getProperties().get("table") != null) {
                l = (String) getService().getProperties().get("table");
            }
            // setTitulo(getUser(request).getLabel(l), request);
        }
    }

    public boolean is_detail(final ServletRequest request) {
        return request.getAttribute("is_detail") != null && request.getAttribute("is_detail").equals(Boolean.TRUE);
    }

    public boolean is_sharing(final ServletRequest request) {
        return request.getAttribute("is_sharing") != null && request.getAttribute("is_sharing").equals(Boolean.TRUE)
                || request.getParameter("is_sharing") != null;
    }

    public boolean is_insert(final ServletRequest request) {
        return request.getAttribute("is_insert") != null && request.getAttribute("is_insert").equals(Boolean.TRUE);
    }

    public boolean is_list(final ServletRequest request) {
        return request.getAttribute("is_list") != null && request.getAttribute("is_list").equals(Boolean.TRUE);
    }

    public boolean is_popup(final ServletRequest request) {
        return request.getAttribute("is_popup") != null
                && request.getAttribute("is_popup").toString().equalsIgnoreCase(String.valueOf(Boolean.TRUE))
                || request.getParameter("is_popup") != null
                && request.getParameter("is_popup").equalsIgnoreCase(String.valueOf(Boolean.TRUE));
    }

    private boolean is_remove(final ServletRequest request) {
        return request.getAttribute("is_remove") != null && request.getAttribute("is_remove").equals(Boolean.TRUE);
    }

    public boolean is_update(final ServletRequest request) {
        return request.getAttribute("is_update") != null && request.getAttribute("is_update").equals(Boolean.TRUE);
    }

    public boolean isDefaultCRUD(final ServletRequest request) {
        return request.getAttribute(Constants.DEFAULT_CRUD).equals(true);
    }

    public boolean isMaster() {
        return master;
    }

    public void clearRequest(final ServletRequest request) {
        Enumeration r = request.getAttributeNames();
        List list = Collections.list(r);
        for (Object object : list) {
            request.removeAttribute((String) object);
        }
    }

    public boolean isNext(final ServletRequest request) {
        if (getParameter(request, Constants.NEXT_KEY, false) != null
                && ((String) getParameter(request, Constants.NEXT_KEY, false)).equalsIgnoreCase(Constants.true_str)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isSessionForm() {
        return false;
    }

    public ModelAndView lista(final ServletRequest request, final ServletResponse response) throws Exception {
        setReqAttr(request, "is_list", Boolean.TRUE);
        if (isDefaultCRUD(request)) {
            if (!is_popup(request)) {
                addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.inclui", "inclui", "label.embranco",
                        BUTTON.STATIC, null, null, false, request);

                addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.altera", "altera", "label.embranco",
                        BUTTON.FORONE, null, null, false, request);

                if (getUser(request).isSuperUser()) {

                    if ((getService().getProperties().get("actions") != null
                            && !getService().getProperties().get("actions").toString().contains("importar"))
                            || getService().getProperties().get("actions") == null
                            || (getService().getProperties().get("actions") != null
                            && util.JSONtoMap(getService().getProperties().get("actions").toString())
                            .get("importar") != null
                            && util.JSONtoMap(getService().getProperties().get("actions").toString())
                            .get("importar").toString().equals("true"))) {
                        addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.importa", "importa",
                                "label.embranco", BUTTON.STATIC, null, null, false, request);
                    }

                    if ((getService().getProperties().get("actions") != null
                            && !getService().getProperties().get("actions").toString().contains("exportar"))
                            || getService().getProperties().get("actions") == null
                            || (getService().getProperties().get("actions") != null
                            && util.JSONtoMap(getService().getProperties().get("actions").toString())
                            .get("exportar") != null
                            && util.JSONtoMap(getService().getProperties().get("actions").toString())
                            .get("exportar").toString().equals("true"))) {
                        addButton(Constants.MAIN_ACTION, "", BUTTON.DOWNLOAD, "label.exporta", "exporta",
                                "label.embranco", BUTTON.STATIC, null, null, false, request);
                    }

                }

                addButton(Constants.MAIN_ACTION, "", BUTTON.CONFIRM, "label.remove", "remove", "label.confirmar_exclui",
                        BUTTON.FORALL, null, null, false, request);

            } else {
                noHelp(request);
                addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.inclui", "inclui", "label.embranco",
                        BUTTON.STATIC, null, null, false, request);
                addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.altera", "altera", "label.embranco",
                        BUTTON.FORONE, null, null, false, request);
                addButton(Constants.MAIN_ACTION, "", BUTTON.CONFIRM, "label.remove", "remove", "label.confirmar_exclui",
                        BUTTON.FORALL, null, null, false, request);
                addButton(Constants.MAIN_ACTION, "", BUTTON.CANCEL, "label.cancelar", "cancel", "", BUTTON.STATIC, null,
                        null, false, request);
            }
        }
        processMethods(request, response);
        lista_imp(request, response);
        if (getVO(request).getFiltro() != null && getVO(request).getFiltro().toString().length() > 0) {
            setReqAttr(request, Constants.FILTER_KEY, getVO(request).getFiltro());
        }
        if (getService().getProperties().get("delegateViewList") == null
                && request.getAttribute("delegateViewList") == null) {
            final Map grid = new HashMap(); // criando o grid principal de
            // listagens
            if (getActionProperty(request) != null && getActionProperty(request).get("L") != null) {

                grid.putAll((Map) getActionProperty(request).get("L"));
            }
            grid.put(Constants.component, "grid");
            grid.put("service", getService().getProperties().get(Constants.name) + "Service");
            grid.put("method", "lista");
            grid.put("property", "lista_grid");
            grid.put("filtro", getVO(request).getFiltro());
            Map p1 = util.getParams("lista", request, true, true);
            p1.remove(Constants.ActionParam);

            grid.put("serviceParams", util.JSONserialize(p1));

            if (grid.get("linkMethod") == null) {
                grid.put("linkMethod", "obtem");
            }
            // grid.put("group", getMainField());
            addCampo(request, grid);
        }
        return finish(request, response, null);
    }

    public abstract void lista_imp(final ServletRequest request, final ServletResponse response) throws Exception;

    public void noFilter(final ServletRequest request) {
        setReqAttr(request, Constants.NO_FILTER, Boolean.TRUE);
    }

    public void noHelp(final ServletRequest request) {
        setReqAttr(request, Constants.NO_HELP, Boolean.TRUE);
    }

    public void NoRequiredUser(final ServletRequest request) {
        setReqAttr(request, "session", Constants.false_str);
        addKey("session", "", request);
        requiredUser = false;
    }

    public final ModelAndView obtem(final ServletRequest request, final ServletResponse response) throws Exception {
        setReqAttr(request, "is_detail", Boolean.TRUE);
        getVO(request).setMultipleId(new String[]{getChave(request)});

        final ModelAndView o = obtem_imp(request, response);
        processMethods(request, response);
        final List m = getService().getMethods(getUser(request));

        for (final Iterator iterator2 = m.iterator(); iterator2.hasNext(); ) {
            final Map method = (Map) iterator2.next();

            if (method.get("type").equals("U") || method.get("type").equals("C")) {
                if (method.get("disableWhenExpression") != null || method.get("includeWhenExpression") != null) {
                    String exp = "";
                    if (method.get("includeWhenExpression") != null) {
                        exp = (String) method.get("includeWhenExpression");
                    } else {
                        Map ser = util.JSONtoMap((String) method.get("disableWhenExpression"));
                        if (ser.get("s") == null) {
                            continue;
                        }
                        exp = (String) ser.get("s");
                    }

                    Object v = util.Evaluate(exp.toString(), createEvaluationContext(request));
                    if (v == null) {
                        v = Constants.false_str;
                    }
                    if (!new Boolean(v.toString())) {
                        continue;
                    } else {
                        final List f = (List) method.get(Constants.ALL_FIELDS);
                        for (final Iterator iterator3 = f.iterator(); iterator3.hasNext(); ) {
                            final Map object2 = (Map) iterator3.next();
                            object2.put("parent_id", getService().getServiceId());
                            if (object2.get(Constants.component) != null
                                    && !object2.get(Constants.component).equals("context")) {
                                addCampo(request, object2);
                            }

                        }

                    }
                }
            }

        }

        // Adicionando a view das informacoes de CONTROLE
        if (getVO(request).getMethod().equalsIgnoreCase("obtem")) {
            final Map cmp = new HashMap();
            cmp.put(Constants.name, "CONTROL");
            cmp.put("label", "CONTROL");
            cmp.put("colspan", "4");
            cmp.put("type", "openFieldset");
            cmp.put(Constants.component, "openFieldset");
            addCampo(request, cmp);

            final Map cmp1 = new HashMap();
            cmp1.put(Constants.name, "ID_USUARIO_CADASTRO");
            cmp1.put("colspan", "2");
            cmp1.put("label", "ID_USUARIO_CADASTRO");
            cmp1.put(Constants.component, "select");
            cmp1.put("key", "ID_USUARIO");
            cmp1.put("labelValue", "NOME");
            cmp1.put("service", "UsuarioService");
            cmp1.put("serviceParams", "{SEGMENTBY:'false'}");
            cmp1.put("type", "num");
            addCampo(request, cmp1);

            if (getVO(request).get("DATA_CADASTRO") != null) {
                final Map cmp2 = new HashMap();
                cmp2.put(Constants.name, "DATA_CADASTRO");
                cmp2.put("label", "DATA_CADASTRO");
                cmp2.put("type", "date");
                cmp2.put(Constants.component, "date");
                addCampo(request, cmp2);
            }

            final Map cmp3 = new HashMap();
            cmp3.put(Constants.name, "ULTIMA_MODIFICACAO");
            cmp3.put("label", "ULTIMA_MODIFICACAO");
            cmp3.put("type", "date");
            cmp3.put(Constants.component, "date");
            addCampo(request, cmp3);

            final Map cmp4 = new HashMap();
            cmp4.put(Constants.name, "CONTROL");
            cmp4.put("label", "CONTROL");
            cmp4.put("type", "closeFieldset");
            cmp4.put("colspan", "4");
            cmp4.put(Constants.component, "closeFieldset");
            addCampo(request, cmp4);
        }
        final List cam2 = (List) getUser(request).getSysParser()
                .getListValuesFromXQL("/systems/system[@name='" + getUser(request).getSchema()
                                + "']/module/bean[@type='D' and @master='" + getService().getProperties().get("id") + "' ]",
                        null, getUser(request), true, true);
        getVO(request).setFiltro("");
        // Iterator iteratorTabRoot = cam_root.iterator();
        final Iterator iteratorTab = cam2.iterator();
        Map<String, Object> p1 = null;

        while (iteratorTab.hasNext()) {
            final Map ee = (Map) iteratorTab.next();
            if (!ParseXmlService.canAccess(ee, getUser(request), true) || ee.get("ignoreLayers") != null
                    && ee.get("ignoreLayers").toString().toUpperCase().contains("V")) {
                continue;
            }

            if (ee.get("detailWhenExpression") != null && !getUtil()
                    .EvaluateBoolean(ee.get("detailWhenExpression").toString(), createEvaluationContext(request))) {
                continue;
            }

            final Object[] action = util.getActionStringAndParams(ee, false, true, request);

            if (p1 == null) {
                p1 = (Map<String, Object>) action[1];
                if (!p1.containsKey(getPrimaryKey()) || p1.get(getPrimaryKey()) == null) {
                    throw new BusinessException(
                            getUser(request).getLabel("label." + getService().getMainField("inclui", getUser(request))
                                    .get(Constants.name).toString().toLowerCase() + ".nao.encontrado(a)"));
                }
            }

            p1.put(getPrimaryKey(), getChave(request));
            final Map t3 = new HashMap(ee);

            t3.put(Constants.component, "tab");
            t3.put("type", "tab");
            t3.put("action", action[0]);
            t3.put("serviceParams", util.JSONserialize(p1));
            addCampo(request, t3);

        }

        final List<Map<String, String>> c = (List) getUser(request).getSysParser().getListValuesFromXQL(
                util.EvaluateInString("/systems/system[@name='" + getUser(request).getSchema()
                        + "']/module/bean[contains(@sharing,'item')]", createEvaluationContext(request)),
                "order", getUser(request), true, true);

        final Iterator iSha = c.iterator();
        Map ac = util.JSONtoMap((String) getService().getProperties().get("acceptSharing"));
        while (iSha.hasNext()) {

            Map ee = (Map) iSha.next();

            if (ac.size() > 0) {

                if (!ac.containsKey(ee.get("id"))) {
                    continue;
                }
            } else {
                continue;
            }

            addKey(Constants.CONTEXT_KEY, (String) getService().getProperties().get("id"), request);
            if (request.getAttribute("VALORCHAVE") != null) {
                addKey("VALORCHAVE", (String) request.getAttribute("VALORCHAVE"), request);
            }
            addKey("is_sharing", "true", request);
            ServiceImpl sservice = (ServiceImpl) getService().getServiceById(ee.get("id").toString(), getUser(request));
            List p = (List) sservice.getAllFields(getUser(request));

            List keyP = getUser(request).getUtil().filter(p, "sharingKey", null);

            if (keyP.size() > 0) {
                for (Iterator iterator = keyP.iterator(); iterator.hasNext(); ) {
                    Map object = (Map) iterator.next();
                    object.put("sharing", true);
                    addCampoHeader(request, object);
                }

            }

            final Object[] action = util.getActionStringAndParams(ee, false, true, request);

            p1 = (Map<String, Object>) action[1];

            if (request.getAttribute("VALORCHAVE") == null) {
                setReqAttr(request, "VALORCHAVE", "" + getVO(request).get(getService().getPrimaryKey().get("name")));
            }

            Map sh = util.JSONtoMap((String) ee.get("sharing"));
            if (sh.get(Constants.FILTER_KEY) != null) {
                StandardEvaluationContext ctx = createEvaluationContext(request);
                ctx.setVariables(p1);
                p1.put(Constants.FILTER_KEY,
                        util.replace(util.EvaluateInString((String) sh.get(Constants.FILTER_KEY), ctx), "\\", ""));
            }
            final Map t3 = new HashMap(ee);

            t3.put(Constants.component, "tab");
            t3.put("type", "tab");
            t3.put("action", action[0]);
            t3.put("serviceParams", util.JSONserialize(p1));
            addCampo(request, t3);

        }
        addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.altera", "altera", "label.embranco", BUTTON.FORONE,
                null, null, false, request);
        addButton(Constants.MAIN_ACTION, "", BUTTON.CONFIRM, "label.remove", "remove", "label.confirmar_exclui",
                BUTTON.FORALL, null, null, false, request);
        if (getUser(request).getLogin() != null) {
            addButton(Constants.MAIN_ACTION, "", BUTTON.DOWNLOAD, "label.exporta", "exporta", "label.embranco",
                    BUTTON.FORONE, null, null, false, request);
            addButton(Constants.MAIN_ACTION, "", BUTTON.ACTION, "label.importa", "importa", "label.embranco",
                    BUTTON.FORONE, null, null, false, request);
        }
        return o;

    }

    private void addCampoHeader(ServletRequest request, Map object) {
        // TODO Auto-generated method stub
        if (request.getAttribute(Constants.LIST_HEADER_FIELDS) == null) {
            setReqAttr(request, Constants.LIST_HEADER_FIELDS, new ArrayList<Map>());
        }
        List hf = (List) request.getAttribute(Constants.LIST_HEADER_FIELDS);
        hf.add(object);
    }

    public abstract ModelAndView obtem_imp(final ServletRequest request, final ServletResponse response)
            throws Exception;

    public ModelAndView obtemData(final ServletRequest request, final ServletResponse response)
            throws IOException, ServletException {
        GenericVO ret = null;
        try {
            ret = (GenericVO) getService().invoke(getVO(request).getMethod(), getVO(request), getUser(request));
        } catch (final Exception e) {
            LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
        }

        return serializeJSOnResponse(request, response, ret, ret.isEncapsulate());
    }

    public ModelAndView obtemLista(final ServletRequest request, final ServletResponse response)
            throws IOException, ServletException {
        setReqAttr(request, "is_list", Boolean.TRUE);
        List<Map> items = new ArrayList<Map>();
        GenericService service = null;
        List<Map> nd = new ArrayList();
        if (request.getAttribute(Constants.LIST_DATA) == null) {
            String ser = (String) util.Evaluate((String) getParameter(request, "service", false),
                    createEvaluationContext(request));
            if (ser == null) {
                ser = (String) getParameter(request, "service", false);
            }
            service = (GenericService) getUser(request).getService(ser);
            final String method = (String) getParameter(request, "serviceMethod", false);

            if (service == null)
                service = getService();
            try {

                if (!method.equalsIgnoreCase("dummy")) {
                    if (method == null || method.toString().length() == 0 || method.equalsIgnoreCase("null")
                            || method.equalsIgnoreCase("lista")) {
                        getVO(request).setLazyList(true);
                        // if("D".equals(getService().getProperties().get("type")))
                        // {
                        // String mpk = (String)
                        // getService().getParent(getUser(request)).getPrimaryKey().get(Constants.name);
                        // if(!getVO(request).containsKey(mpk) ||
                        // getVO(request).get(mpk)==null )
                        // throw new BusinessException("label.reabrir.pasta");
                        // }
                        //
                        items.addAll(service.lista(getVO(request), getUser(request)));
                        // getVO(request).cloneConfig(mvo);
                    } else {
                        getVO(request).put(Constants.RequestProcessed, request);
                        items = (List<Map>) service.invoke(method, getVO(request), getUser(request));
                        if (items == null && getVO(request).get(Constants.LIST_DATA) != null)
                            items = (List<Map>) getVO(request).get(Constants.LIST_DATA);
                    }
                }
            } catch (final Exception e1) {
                LogUtil.exception(e1, (Usuario) request.getAttribute(Constants.RAIN_USER));
                throw new BusinessException(e1);
            }
            List f = null;

            if (method != null && method.toString().trim().length() > 0 && ReflectionUtils
                    .findMethod(service.getClass(), method, new Class[]{GenericVO.class, Usuario.class}) == null) {
                f = (List) service.getMethod(method, getUser(request)).get(Constants.ALL_FIELDS);
            } else {

                f = new ArrayList((List) service.getProperties().get(Constants.ALL_FIELDS));
                if (f != null) {
                    f.addAll(f = service.getDAO().getProperties());
                }
            }

            final List pk = util.match(f, "primaryKey", Constants.true_str);
            f = util.filter(f, "listable", null);
            pk.addAll(f);
            final List names = java.util.Arrays.asList(util.extractArrayFromList(pk, Constants.name, true));
            for (final Object element : items) {
                final HashMap object = (HashMap) element;
                LogUtil.debug("DATA >>> " + object, getUser(request));

                final Iterator i = object.keySet().iterator();
                final GenericVO n = service.createVO(getUser(request));
                if (getVO(request).containsKey("FORCE_*_TEXT")) {
                    n.put("FORCE_*_TEXT", true);
                }

                for (final Iterator iterator2 = i; iterator2.hasNext(); ) {
                    final String object2 = (String) iterator2.next();
                    if (names.contains(object2.toUpperCase())) {
                        final HashMap metadata = (HashMap) ((HashMap) util
                                .match(pk, Constants.name, object2.toUpperCase()).get(0)).clone();

                        final Map d = (Map) object.clone();
                        d.putAll(metadata);

                        n.put(object2, object.get(object2));

                        if (
                            // !object2.toString().equalsIgnoreCase(auxCol)
                            // && !n.containsKey(auxCol)
                            // &&
                                metadata.get("serviceMethod") != null
                                        && !getService().isDefaultCRUD((String) metadata.get("serviceMethod"))) { // TODO

                            final String auxCol = util.defineColumn(metadata, getUser(request), true, 0)
                                    .get(Constants.COLUMN_NAME);
                            if (auxCol == null)
                                continue;
                            // corrigir
                            // bug na
                            // listagem
                            // para
                            // isDefaulCrud
                            // false
                            // **MATCHSER
                            // util.matchService(d, object2,
                            // object.get(object2), getUser(request));
                            if (getService().equals(util.evalVOService(d, getUser(request)))) {
                                d.putAll(metadata); // recolocando os filtros
                            }
                            // originais
                            // **EVSER

                            n.put(auxCol, util.evalService(d, object2, getUser(request)));
                        }

                    } else if (n.get(object2) == null) {
                        n.put(object2, object.get(object2));

                    }

                }

                if (getParameter(request, "labelValue", false) != null
                        && getParameter(request, "labelValue", false).toString().trim().length() > 0) {
                    if (!n.containsKey(getParameter(request, "labelValue", false))) {
                        final String[] labels = getParameter(request, "labelValue", false).toString().split(",");
                        String v = "";
                        for (final String label : labels) {
                            HashMap metadata = null;

                            try {
                                metadata = (HashMap) ((HashMap) util.match(pk, Constants.name, label.toUpperCase())
                                        .get(0)).clone();
                            } catch (final Exception e) {
                                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                            }
                            v = v + n.get(label) + ", ";
                        }
                        n.put(getParameter(request, "labelValue", false).toString().replaceAll(",", "_"),
                                v.substring(0, v.lastIndexOf(",")));
                    }
                }
                nd.add(n.format(getUser(request)));
            }
        } else {
            // Quando o service representa um objeto no request
            try {
                if (request.getAttribute(Constants.LIST_DATA) != null) {
                    nd = (List<Map>) request.getAttribute(Constants.LIST_DATA);
                } else {
                    nd = (List<Map>) util
                            .Evaluate("${" + util.Evaluate((String) getParameter(request, "service", false),
                                    createEvaluationContext(request)) + "}", createEvaluationContext(request));
                }
            } catch (final Exception e2) {
                throw new BusinessException(e2);
            }
        }
        return serializeJSOnResponse(request, response, nd, getVO(request).isEncapsulate());
    }

    public void overrideLink(final String chave, final ServletRequest request) {
        setReqAttr(request, "list_link", chave);
    }

    private void populateDefaultValuesInVO(final ServletRequest request, List cam3, boolean evaluated) {

        if (process(request))
            return;
        final Iterator i = cam3.iterator();
        String idx;
        while (i.hasNext()) {
            final Map n = (Map) i.next();
            idx = n.get(Constants.name).toString();
            if (n.get("value") != null && (getVO(request).get(idx) == null)) {
                if (n.get("value").toString().startsWith("${")) {
                    if (!evaluated)
                        continue;
                    try {
                        getVO(request).put(idx,
                                getVO(request).convert(idx,
                                        util.Evaluate((String) n.get("value"), createEvaluationContext(request)),
                                        getUser(request)));
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                    }
                } else {
                    try {

                        if (n.get(Constants.type) != null && n.get(Constants.type).toString().equalsIgnoreCase("bit")) {
                            if ((getVO(request).get(idx) == null))
                                getVO(request).put(idx, "F");

                        }

                        // default value in methods like bloquear desbloquear
                        // usuario
                        getVO(request).put(idx, getVO(request).convert(idx, n.get("value"), getUser(request)));

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                    }
                }
            } else {
                // getVO(request).put(idx, null); forcando populate padrao do
                // frmWork
                if (n.get(Constants.component) != null
                        && n.get(Constants.component).toString().equalsIgnoreCase("checkbox")) {
                    if ((getVO(request).get(idx) == null))
                        getVO(request).put(idx, "F");
                }
            }

        }
    }

    private void populateVO(final ServletRequest request, List cam3) {
        cam3 = util.filter(cam3,
                new String[]{Constants.component, Constants.component, Constants.component, Constants.component,
                        Constants.component, Constants.component, Constants.component, Constants.component},
                new String[]{"openStep", "openFieldSet", "openTab", "closeStep", "closeFieldSet", "closeTab",
                        "scriptRule", "sqlRule"});
        if (process(request) && !is_forward(request)) {
            getVO(request).clear();
        }

        if (getService().getProperties().get("sharing") != null) {
            getVO(request).put("CONTEXTO", getParameter(request, "CONTEXTO", false) == null
                    ? getParameter(request, "MODEL", false) : getParameter(request, "CONTEXTO", false));
        }

        final Enumeration e = getParameterNames(request);
        while (e.hasMoreElements()) {
            final String element = (String) e.nextElement();
            if (is_forward(request) && element.toString().toUpperCase().indexOf("MULTIPLEID") > -1)
                continue;
            LogUtil.debug(element + " >>" + getParameter(request, element, false), getUser(request));
            final List c = util.match(cam3, Constants.name, element);
            final Map ca = c.size() > 0 ? (Map) c.get(0) : new HashMap();
            if (element.toString().toUpperCase().indexOf("MULTIPLEID") > -1
                    || !ca.isEmpty() && ca.get("type") != null && ca.get("type").toString().equalsIgnoreCase("array")) {
                getVO(request).put(element, getParameter(request, element, true));
            } else {
                getVO(request).put(element, getParameter(request, element, false));
            }

        }

        if (!isDefaultCRUD(request)) { // unbind em context imports full
            // precadastro sample
            try {
                final Map mt = getService().getMethod(getVO(request).getMethod(), getUser(request));
                List t = util.match((List) mt.get(Constants.ALL_FIELDS), "required", Constants.true_str);

                Map m = null;
                if (t.size() > 0)
                    m = (Map) t.get(0);

                if (m != null && m.get(Constants.component) != null
                        && m.get(Constants.component).toString().equalsIgnoreCase("context")) {

                    final GenericService d = (GenericService) getService().getService(m.get("service").toString(),
                            getUser(request));
                    Map c = d.getMainField(getVO(request).getMethod(), getUser(request));
                    if (c.get("calculated") != null) {
                        c = (Map) util.match((List) d.getProperties().get(Constants.ALL_FIELDS), "required",
                                Constants.true_str).get(0);
                    }
                    GenericVO n = d.createVO(getUser(request));
                    d.unBindVOContext((GenericVO) getVO(request).clone(), n, (String) m.get(Constants.name),
                            getUser(request));
                    n.cloneConfig(getVO(request));

                    getVO(request).clear();

                    getVO(request).putAll(n);
                }

            } catch (Exception e2) {
                // TODO: handle exception
                e2.printStackTrace();
            }

        }

        populateDefaultValuesInVO(request, cam3, false);

        if (is_update(request) && !process(request))
            try {
                getVO(request).putAllAndGet(service.obtem(getVO(request), getUser(request)), true);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                LogUtil.exception(e1, (Usuario) request.getAttribute(Constants.RAIN_USER));
            }

        populateDefaultValuesInVO(request, cam3, true);

        getVO(request).setFiltro(util.EvaluateInString(getVO(request).getFiltro(), createEvaluationContext(request)));

        ((ServiceImpl) getService()).processScripts(getVO(request), getUser(request),
                ((ServiceImpl) getService()).getScripts(getUser(request), getVO(request).getMethod()), "V",
                "precedent");

    }

    private boolean is_forward(ServletRequest request) {
        return request.getAttribute("is_forward") != null && request.getAttribute("is_forward").equals(Boolean.TRUE);
    }

    private void processActionsMethods(final ServletRequest request) {
        Map actions = getActionProperty(request);
        if (actions == null) {
            return;
        }
        Iterator i = actions.keySet().iterator();
        while (i.hasNext()) {
            String tx = (String) i.next();
            if (getCRUDKey(request).toString().equalsIgnoreCase(tx)) {
                continue;
            }
            Map object = null;
            try {
                object = (Map) actions.get(tx);

            } catch (Exception e) {
                continue;
            }

            String printWhenExpression = "" + String.valueOf(object.get("printWhenExpression"));
            if (!printWhenExpression.equals("null")
                    && !util.EvaluateBoolean(printWhenExpression, createEvaluationContext(request))) {
                continue;
            }

            final Map d = util.JSONtoMap((String) object.get("disableWhenExpression"));

            BUTTON t = BUTTON.FORALL;
            BUTTON t0 = BUTTON.ACTION;
            if (object.get("type") != null) {
                if (object.get("type").toString().equalsIgnoreCase("C")) {
                    t = BUTTON.STATIC;
                    if (object.get("nature") != null
                            && object.get("nature").toString().equalsIgnoreCase("persistent")) {
                        t = BUTTON.FORONE;
                    }
                }

                if (object.get("nature") != null && object.get("nature").toString().equalsIgnoreCase("U")) {
                    t = BUTTON.FORONE;

                }

                if (object.get("type").toString().equalsIgnoreCase("A")
                        || object.get("type").toString().equalsIgnoreCase("D")) {
                    t0 = BUTTON.CONFIRM;
                    t = BUTTON.CONDITION;
                }
                if (object.get("type").toString().equalsIgnoreCase("S")) {
                    t0 = BUTTON.DOWNLOAD;
                }

                if (object.get("type").toString().equalsIgnoreCase("R")
                        || object.get("type").toString().equalsIgnoreCase("L")) {
                    t0 = BUTTON.METHOD;
                    t = BUTTON.STATIC;
                }
            }
            if (object.get("javascript") != null) {
                addButton(Constants.MAIN_ACTION, (String) d.get("c"), BUTTON.METHOD,
                        (String) getService().getProperties().get("table") + "." + tx,
                        "javascript:" + util.replace((String) object.get("javascript"), "^", "\""), null, t, null, null,
                        false, request);
            }
            if (is_detail(request)) {
                if (t == BUTTON.STATIC) {
                    continue;
                }
            }
            if (!is_list(request) && d.get("s") != null
                    && util.EvaluateBoolean((String) d.get("s"), createEvaluationContext(request))) {
                // validationr
                continue;
            }
            String l = ((String) getService().getProperties().get("table") + "." + object.get("method"));

            if (object.get("label") != null)
                l = l + ">" + object.get("label").toString();

            addButton(Constants.MAIN_ACTION, (String) d.get("c"), t0, l, (String) object.get("method"), null, t, null,
                    null, false, request);

        }
    }

    public ModelAndView processRequestMethod(final String method, final ServletRequest request,
                                             final ServletResponse response) throws BusinessException {
        request.removeAttribute(Constants.RequestProcessed);


        if ((method.equalsIgnoreCase((String) getService().getProperties().get("initial"))
                || method.equalsIgnoreCase((String) getService().getProperties().get("main")))) {
            setReqAttr((HttpServletRequest) request, Constants.MAIN_REQUEST_ACTION, true);


        }

        ArrayList cam = new ArrayList((List) getService().getProperties().get(Constants.ALL_FIELDS));
        // force redirect8 CALLS DO TYPE = F devem funcionar assim tambem para o
        // redirect da senha
        if (!getService().getProperties().containsKey("main") && false
                && !getService().getName().equals("SystemService")
                && !getService().getProperties().containsKey("loadOnStartup")
                && getService().getProperties().get("master") != null && getUser(request).getTempoValidadeSenha() <= 0
                && request.getAttribute("FORCE_REDIRECT") == null && isDefaultCRUD(request)) {
            try {
                getVO(request).setMethod("alteraSenha");
                setReqAttr(request, "is_update", Boolean.TRUE);
                setReqAttr(request, "FORCE_REDIRECT", true);
                request.getRequestDispatcher(util.encode("UsuarioAction.do?METHOD=alteraSenha&TROCA_SENHA=F",
                        getUser(request).getSessionId(), "", false, getUser(request))).forward(request, response);
            } catch (SessionExpiredException e) {
                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
            } catch (ServletException e) {
                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
            } catch (IOException e) {
                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
            }

            return null;
        }
        processKeys(request, response, cam);

        if (isDefaultCRUD(request)) {
            getVO(request).put("METHOD_NAME", getVO(request).getMethod());
            String f = forward(cam, (HttpServletRequest) request, (HttpServletResponse) response);
            if (f != null && f.trim().length() > 0) {
                try {
                    request.getRequestDispatcher(
                            util.encode(f, getUser(request).getSessionId(), "", false, getUser(request)))
                            .forward(request, response);
                    return null;
                } catch (SessionExpiredException e) {
                    LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                } catch (ServletException e) {
                    LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                } catch (IOException e) {
                    LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                }
            }

            processFields(request, response, cam);

            processActionsMethods(request); // ADD TO OVERRIDE DEFAULT CRUD METHODS BY ACTIONS
            try {
                final ModelAndView res = (ModelAndView) ReflectionUtils.invokeMethod(
                        ReflectionUtils.findMethod(this.getClass(), method,
                                new Class[]{ServletRequest.class, ServletResponse.class}),
                        this, new Object[]{request, response});

                // if (is_detail(request)) {
                // processFields(request, response, cam);
                // }
                setReqAttr(request, Constants.MODEL_AND_VIEW, res);
                return res;
            } catch (final BusinessException e2) {
                if (LogUtil.debugEnabled(getUser(request))) {
                    LogUtil.exception(e2, (Usuario) request.getAttribute(Constants.RAIN_USER));
                }
                throw e2;
            }

        } else {
            Map im = null;
            getVO(request).put("METHOD_NAME", getVO(request).getMethod());
            im = (Map) util.match(getService().getMethods(getUser(request)), Constants.name, getVO(request).getMethod())
                    .get(0);

            cam = new ArrayList((List) im.get(Constants.ALL_FIELDS));
            setReqAttr(request, (String) im.get(Constants.name), Boolean.TRUE);
            final String tp = (String) im.get("type");
            final String nature = (String) im.get("nature");
            if (tp != null) {
                if (tp.equalsIgnoreCase("C")) {
                    setReqAttr(request, "is_insert", Boolean.TRUE);
                } else if (tp.equalsIgnoreCase("R")) {
                    setReqAttr(request, "is_detail", Boolean.TRUE);
                } else if (tp.equalsIgnoreCase("U") || tp.equalsIgnoreCase("A")) {
                    setReqAttr(request, "is_update", Boolean.TRUE);
                } else if (tp.equalsIgnoreCase("D")) {
                    setReqAttr(request, "is_remove", Boolean.TRUE);
                } else if (tp.equalsIgnoreCase("S")) {
                    setReqAttr(request, "is_stream", Boolean.TRUE);
                } else if (tp.equalsIgnoreCase("L")) {
                    setReqAttr(request, "is_list", Boolean.TRUE);
                } else if (tp.equalsIgnoreCase("F")) {
                    setReqAttr(request, "is_forward", Boolean.TRUE);
                }
            }
            processKeys(request, response, cam);

            String f = forward(cam, (HttpServletRequest) request, (HttpServletResponse) response);
            if (f != null) {
                try {
                    clearButtons(request);
                    request.getRequestDispatcher(
                            util.encode(f, getUser(request).getSessionId(), "", false, getUser(request)))
                            .forward(request, response);

                    return (ModelAndView) request.getAttribute(Constants.MODEL_AND_VIEW);
                } catch (SessionExpiredException e) {
                    LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                } catch (ServletException e) {
                    LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                } catch (IOException e) {
                    LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                }
            }

            if (im.get("size") != null) {
                setReqAttr(request, Constants.WINDOW_SIZE, im.get("size"));
            }
            if (im.get("requiredSession") != null && im.get("requiredSession").toString().equalsIgnoreCase("false")) {
                addKey("session", "false", request);
            }

            if (nature != null) {
                if (nature.equalsIgnoreCase("C") || tp.equalsIgnoreCase("U") || tp.equalsIgnoreCase("A")) {
                    setReqAttr(request, "is_win", Boolean.TRUE);
                }

            }

            try {
                if (tp != null) {
                    if (tp.equalsIgnoreCase("C")
                            && getService().getMethod(getVO(request).getMethod(), getUser(request)) != null) {
                        processActionsMethods(request);
                        processFields(request, response, new ArrayList((List) getService()
                                .getMethod(getVO(request).getMethod(), getUser(request)).get(Constants.ALL_FIELDS)));
                        return inclui(request, response);
                    } else if (tp.equalsIgnoreCase("R")) {

                        processFields(request, response, new ArrayList((List) getService()
                                .getMethod(getVO(request).getMethod(), getUser(request)).get(Constants.ALL_FIELDS)));
                        if (im.get("nature") != null && im.get("nature").equals("D")) {
                            return obtemData(request, response);
                        }
                        return obtem(request, response);
                    } else if (tp.equalsIgnoreCase("U") || tp.equalsIgnoreCase("A")) {
                        final String m = getVO(request).getMethod();
                        processActionsMethods(request);
                        final ModelAndView r = altera(request, response);

                        getVO(request).setMethod(m);

                        processFields(request, response, new ArrayList(
                                (List) getService().getMethod(m, getUser(request)).get(Constants.ALL_FIELDS)));

                        setReqAttr(request, Constants.MODEL_AND_VIEW, r);
                        return r;
                    } else if (tp.equalsIgnoreCase("D")) {
                        processActionsMethods(request);
                        processFields(request, response, new ArrayList((List) getService()
                                .getMethod(getVO(request).getMethod(), getUser(request)).get(Constants.ALL_FIELDS)));
                        return remove(request, response);
                    } else if (tp.equalsIgnoreCase("S")) { // stream
                        processActionsMethods(request);
                        processFields(request, response, new ArrayList((List) getService()
                                .getMethod(getVO(request).getMethod(), getUser(request)).get(Constants.ALL_FIELDS)));
                        ModelAndView res = stream(request, response, im);
                        setReqAttr(request, Constants.MODEL_AND_VIEW, res);
                        return res;
                    } else if (tp.equalsIgnoreCase("L")) { // List
                        if (im.get("delegateViewList") != null) {
                            setReqAttr(request, "delegateViewList", true);
                            processFields(request, response, cam);
                            try {
                                im = (Map) util.match(getService().getMethods(getUser(request)), Constants.name,
                                        getVO(request).getMethod()).get(0);
                                if (im.get("layers").toString().indexOf("S") > -1)
                                    getService().invoke(getVO(request).getMethod(), getVO(request), getUser(request));
                            } catch (Exception e) {
                                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                                LogUtil.debug("Method de listagem  chamado nao existente" + getVO(request).getMethod(),
                                        getUser(request));
                                return null;
                            }

                        }
                        return lista(request, response);
                    } else if (tp.equalsIgnoreCase("F")) { // Forward

                        getVO(request).putAll(getService().obtem(getVO(request), getUser(request)));

                        if (!response.isCommitted()) {

                            request.getRequestDispatcher(util.encode((String) getVO(request).get("ACTION"),
                                    getUser(request).getSessionId(), "", false, getUser(request)))
                                    .forward(request, response);

                        }

                    }

                }
            } catch (final BusinessException e4) {
                if (LogUtil.exceptionEnabled(getUser(request))) {
                    LogUtil.exception(e4, (Usuario) request.getAttribute(Constants.RAIN_USER));
                }
                throw new BusinessException(e4);
            } catch (final Exception e5) {
                if (e5.getCause() instanceof StreamingException) {
                    throw new StreamingException(e5.getCause());
                } else {
                    if (LogUtil.exceptionEnabled(getUser(request))) {
                        LogUtil.exception(e5, (Usuario) request.getAttribute(Constants.RAIN_USER));
                    }
                    throw new BusinessException(e5);
                }
            }
        }

        return null;

    }

    public void setSessionAttr(HttpServletRequest request, String key, Object value) {
        request.getSession().setAttribute(key, value);
    }

    public String forward(List cam, HttpServletRequest request, HttpServletResponse response) {
        List r = util.match(cam, "type", "forwardRule");

        if (r.size() > 0) {
            setReqAttr(request, "is_win", Boolean.TRUE);

            try {
                Map t = (Map) r.get(0);
                if (t.get("nature") != null && t.get("nature").equals("volatile") && !process(request))
                    return null;
                setReqAttr(request, "is_forward", Boolean.TRUE);
                return (String) evalRhino(getVO(request), getUser(request), t, request, response);
            } catch (ScriptException e) {
                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
                // TODO Auto-generated catch block
                try {
                    throw new BusinessException(e.getCause().getCause().getMessage());
                } catch (Exception de) {
                    throw new BusinessException(e.getCause().getMessage());
                }
            }
        }
        return null;

    }

    public Object evalRhino(GenericVO vo, Usuario user, Map field, HttpServletRequest request,
                            HttpServletResponse response) throws ScriptException {
        CompiledScript eng = null;
        try {
            eng = ((Compilable) engine).compile((String) field.get("CDATA"));
        } catch (ScriptException e1) {
            LogUtil.exception(e1, (Usuario) request.getAttribute(Constants.RAIN_USER));
        }

        if (eng == null) {
            LogUtil.error("RhinoScript nao compilavel.");
        }

        final Bindings bindings = engine.createBindings();
        bindings.put("user", user);
        bindings.put("dm", this);
        bindings.put("sm", getService());
        bindings.put(Constants.VOKey, vo);
        bindings.put("util", user.getUtil());
        bindings.put("request", request);
        bindings.put("session", request.getSession());
        bindings.put("response", response);
        bindings.put("df", user.getUtil().df);
        // bindings.put("cmp", new TagInputExt());
        bindings.put("result", new Object());
        Object r = null;

        return eng.eval(bindings);

    }

    public boolean process(final ServletRequest request) {

        if (is_detail(request) || is_forward(request))
            setReqAttr(request, Constants.PROCESS_CALCULATED, false);

        if (request.getAttribute(Constants.PROCESS_CALCULATED) != null)
            return (Boolean) request.getAttribute(Constants.PROCESS_CALCULATED);

        if (getActionProperty(request) != null && getActionProperty(request).get(getCRUDKey(request)) != null
                && ((Map) getActionProperty(request).get(getCRUDKey(request))).containsKey("autoExecute")
                && (Boolean) ((Map) getActionProperty(request).get(getCRUDKey(request))).get("autoExecute")) {

            setProcessed(request);
            setReqAttr(request, Constants.PROCESS_CALCULATED, true);

        }
        if (getActionProperty(request) != null && getActionProperty(request).get(getCRUDKey(request)) != null
                && ((Map) getActionProperty(request).get(getCRUDKey(request))).containsKey("autoExecute")
                && !(Boolean) ((Map) getActionProperty(request).get(getCRUDKey(request))).get("autoExecute")) {
            setReqAttr(request, Constants.PROCESS_CALCULATED, false);
            return false;
        }
        if (!processed(request)) {
            if (getParameter(request, "multipleId", false) != null
                    && getParameter(request, "PROCCESS_FOREING_KEYS", false) != null) {
                setProcessed(request);
                setReqAttr(request, Constants.PROCESS_CALCULATED, true);
            }
            if (getMainField(request) != null && getParameter(request, getMainField(request), false) != null
                    && getParameter(request, getMainField(request), false).toString().length() > 0
                    && !getParameter(request, getMainField(request), false).toString().equals("null")) {
                setProcessed(request);
                setReqAttr(request, Constants.PROCESS_CALCULATED, true);
            } else {
                setReqAttr(request, Constants.PROCESS_CALCULATED, false);
            }
        } else {
            setReqAttr(request, Constants.PROCESS_CALCULATED, true);
        }
        return (Boolean) request.getAttribute(Constants.PROCESS_CALCULATED);
    }

    private Map getActionProperty(ServletRequest request) {
        // TODO Auto-generated method stub
        Map s = getActionProperty(getVO(request).getMethod(), request, getUser(request));

        return s;
    }

    public Map<String, Object> getActionProperty(String m, ServletRequest ctx, Usuario user) {
        if (getService().isDefaultCRUD(m)) {
            if (getService().getProperties().get("actions") != null) {
                return util.JSONtoMap(util.EvaluateInString(getService().getProperties().get("actions").toString(),
                        createEvaluationContext(ctx)));
            }
        } else {
            if (getService().getMethod(m, user) != null && getService().getMethod(m, user).get("actions") != null) {
                return util.JSONtoMap(getService().getMethod(m, user).get("actions").toString());
            }

        }

        return null;

    }

    private boolean processed(final ServletRequest request) {
        return request.getAttribute(Constants.RequestProcessed) != null && !is_list(request) && !is_detail(request);
    }

    private void processFields(final ServletRequest request, final ServletResponse response, List cam) {
        // clearFields(request);

        // busca Rapida

        // Adiciona como key 'addKey(String, String, ServletRequest)' as
        // properties marcadas como 'requestKey=true' no config
        for (Object it : cam) {
            Map<String, String> m = (Map<String, String>) it;
            if (m.get("requestKey") != null && m.get("requestKey").equals(Constants.true_str) && !is_insert(request)) {
                addKey((String) m.get("name"), null, request);
            }
        }

        if (getParameter(request, Constants.QUERY_KEY, false) != null
                && getParameter(request, Constants.QUERY_KEY, false).toString().trim().length() > 0) {

            String faux = "";
            if (getParameter(request, "fields", false) != null) {
                String fil = (String) getParameter(request, "fields", false);
                fil = util.replace(fil, new String[]{"[", "]", "\""}, new String[]{"", "", ""});
                final String[] fields = fil.split(",");

                faux = "{gO:[";
                for (final String field : fields) {
                    final Map c = util.getCmp(field.split("#")[0], getService(), getUser(request));
                    if (c == null || c.size() == 0 || c.get(Constants.component) == null) {
                        continue;
                    }

                    if (c.get(Constants.component).toString().indexOf("image") == -1
                            && c.get(Constants.component).toString().indexOf("radio") == -1) { // TODO
                        // retirado
                        // os radios pois geralmente estao com services
                        // INJETADOS pelo config-sis tratar no DAO com uso de
                        // ServiceMethod
                        faux += "{c:{f:'" + field.split("#")[0] + "',o:'%?' ,v1:'"
                                + getParameter(request, Constants.QUERY_KEY, false) + "'}},";
                    }

                }
                if (faux.trim().length() > 0 && faux.lastIndexOf(',') != -1) {
                    faux = faux.trim().substring(0, faux.lastIndexOf(',')) + "]}";
                }
            } else {
                faux = (String) getParameter(request, Constants.QUERY_KEY, false);
            }

            String f = getVO(request).getFiltro();
            if (f == null && getParameter(request, Constants.FILTER_KEY, false) != null) {
                f = getParameter(request, Constants.FILTER_KEY, false).toString();
            }
            if (f != null && f.length() > 2) {
                f = util.mergeFiltro(util.decode(f, getUser(request)), faux);
            } else {
                f = faux;
            }
            getVO(request).setFiltro(f);
        }

        if (!process(request) || request.getAttribute("delegateViewList") != null) {
            if (!is_list(request) || request.getAttribute("delegateViewList") != null) {
                // order
                cam = util.filter(cam, new String[]{"type"}, new String[]{"imageSelector"});

                cam.removeAll(CollectionUtils.intersection(
                        util.match(cam, new String[]{"type"}, new String[]{"sqlrule"}),
                        util.match(cam, new String[]{Constants.component}, new String[]{null})));

            } else {
                cam = util.filter(getService().getDAO().getProperties(), "listable", null);
                cam.addAll(util.match(getService().getDAO().getProperties(), "relationalKey", Constants.true_str));
                util.SortCollection(cam, "listable");
            }
            final Iterator iterator = cam.iterator();
            while (iterator.hasNext()) {
                final Map ex = (Map) iterator.next();

                if (!is_list(request) || request.getAttribute("delegateViewList") != null) {
                    addCampo(request, ex);
                } else if (ex.get("listable") != null) {
                    if (getService().getProperties().get("delegateViewList") != null) {
                        addCampo(request, ex);
                        // interacao de campos na lista passou para a grid
                    }
                } else if (ex.get("relationKey") != null) {
                    addKey((String) ex.get(Constants.name), (String) ex.get("value"), request);
                }
            }
        }

    }

    public ModelAndView getPage(final ServletRequest request, final ServletResponse response) throws Exception {
        return new ModelAndView("/" + getParameter(request, "page", false));
    }

    private void processKeys(final ServletRequest request, final ServletResponse response, final List cam) {
        List cam3 = null;
        if (isDefaultCRUD(request)) {
            cam3 = (List) getService().getProperties().get(Constants.ALL_FIELDS);
        } else {
            try {
                cam3 = (List) getService().getMethod(getVO(request).getMethod(), getUser(request))
                        .get(Constants.ALL_FIELDS);

            } catch (final Exception e) {
                return;
            }
        }

        String keyname = null;
        Object key = null;
        final GenericService s = getService().getParent(getUser(request));
        if (s != null && s.getPrimaryKey() != null) {

            if (getRelationKey(request) != null) {
                keyname = getRelationKey(request);
            } else {
                keyname = (String) s.getPrimaryKey().get(Constants.name);
            }
            key = getParameter(request, keyname, false);
            if (key != null) {
                key = key.toString();
            } else {
                key = getParameter(request, (String) s.getPrimaryKey().get(Constants.name), false);
            }
            final GenericVO vo = s.createVO(getUser(request));
            if (key != null) {
                try {
                    vo.putKey(vo.getKey(), key);
                    vo.setLazyList(true);
                    vo.put("NO_SCRIPT_PROCESS", true);
                    vo.putAllAndGet(s.obtem(vo, getUser(request)), true);
                } catch (final Exception e1) {
                    throw new BusinessException(
                            "Operao no realizada, envie o print da tela ao Administrador " + util.getDataAtual());
                }
            }
            setReqAttr(request, Constants.MASTERVOKey, vo);
            if (key != null) {
                addKey(keyname, key.toString(), request);
            }

        }

        populateVO(request, processContexts(cam3, request));
        if (!is_sharing(request)) {
            setReqAttr(request, "VALORCHAVE", getVO(request).get(getPrimaryKey()));
        }

    }

    private List processContexts(List cam3, final ServletRequest request) {
        List r = new ArrayList();
        for (Iterator iterator = cam3.iterator(); iterator.hasNext(); ) {
            Map campo = (Map) iterator.next();
            if (campo.get(Constants.component) != null
                    && campo.get(Constants.component).toString().equalsIgnoreCase("context")) {

                // se a nature volatil devo enviar os prints para os fields
                // pois eles vo ser usandos na view
                if (campo.get("printWhenExpression") != null && !campo.get("nature").equals("volatile")) {
                    Object v = util.Evaluate(campo.get("printWhenExpression").toString(),
                            createEvaluationContext(request));
                    if (v == null) {
                        v = Constants.false_str;
                    }
                    if (!new Boolean(v.toString())) {
                        continue;
                    }
                }

                final Map par = new HashMap();

                final List cont = util
                        .filter((List) ((GenericService) getService().getService(campo.get("service").toString(),
                                getUser(request))).getProperties().get(Constants.ALL_FIELDS), "type", "sqlrule"); // order
                for (final Iterator iterator2 = cont.iterator(); iterator2.hasNext(); ) {
                    final Map object = (Map) ((HashMap) iterator2.next()).clone();

                    object.put("parent_id", campo.get("parent_id"));

                    object.put(Constants.name, campo.get(Constants.name) + "_" + object.get(Constants.name));
                    r.add(object);
                }
            } else {
                r.add(campo);
            }

        }
        return r;
    }

    public boolean processLista(final ServletRequest request) {
        return request != null && getParameter(request, "processLista", false) != null;
    }

    private void processMethods(final ServletRequest request, final ServletResponse response) {
        List met = getService().getMethods(getUser(request));
        met = util.filter(met, "type", "L");
        // met = util.filter(met, "type", "R");
        met = util.filter(met, "type", "F");
        for (final Iterator iterator = met.iterator(); iterator.hasNext(); ) {
            final Map object = (Map) iterator.next();
            if (object.get("type").equals("R") && (object.get("nature") == null || object.get("nature").equals("R")))
                continue;
            if (object.get("printWhenExpression") != null) {
                Object v = util.Evaluate((String) object.get("printWhenExpression"), createEvaluationContext(request));
                if (v == null) {
                    v = Constants.false_str;
                }
                if (!new Boolean(v.toString())) {
                    continue;
                }
            }
            final Map d = util.JSONtoMap((String) object.get("disableWhenExpression"));
            BUTTON t = BUTTON.FORALL;
            BUTTON t0 = BUTTON.METHOD;
            if (object.get("type").toString().equalsIgnoreCase("C")) {
                t = BUTTON.STATIC;
                if (object.get("nature") != null && object.get("nature").toString().equalsIgnoreCase("persistent")) {
                    t = BUTTON.FORONE;
                }
            }
            if (object.get("type").toString().equalsIgnoreCase("A")
                    || object.get("type").toString().equalsIgnoreCase("D")
                    || (object.get("nature") != null && object.get("nature").toString().equalsIgnoreCase("D"))) {
                t0 = BUTTON.CONFIRM;
                t = BUTTON.CONDITION;
            }
            if (object.get("type").toString().equalsIgnoreCase("S")) {
                t0 = BUTTON.DOWNLOAD;
            }
            if (is_detail(request)) {
                if (t == BUTTON.STATIC) {
                    continue;
                }
            }
            if (object.get("nature") != null && object.get("nature").toString().equalsIgnoreCase("D")) {
                t = BUTTON.CONDITION;
            }
            if (object.get("nature") != null && object.get("nature").toString().equalsIgnoreCase("U")) {
                t = BUTTON.FORONE;

            }
            if (!is_list(request) && d.get("s") != null
                    && util.EvaluateBoolean((String) d.get("s"), createEvaluationContext(request))) {
                // validation
                continue;
            }
            if (object.get("type").equals("R"))
                setReqAttr(request, "button_for_detail", true);
            // if(getActionProperty(request)!=null)

            if (object.get("params") != null && util.JSONtoMap("" + object.get("params")).get("javascript") != null) {

                addButton(Constants.MAIN_ACTION, null, BUTTON.METHOD,
                        (String) getService().getProperties().get("table") + "."
                                + object.get(Constants.name),
                        "javascript:" + util.EvaluateInString(util.replace(
                                util.replace((String) util.JSONtoMap("" + object.get("params")).get("javascript"), "^^",
                                        "\""),
                                "^", "\\'"), createEvaluationContext(request)),
                        null, t, null, null, false, request);
            } else {

                addButton(Constants.MAIN_ACTION, (String) d.get("c"), t0, (String) object.get("label"),
                        (String) object.get(Constants.name), null, t, null, null, false, request);
            }
            request.removeAttribute("button_for_detail");
        }
        processActionsMethods(request);

    }

    public ModelAndView proxy(final ServletRequest request, final ServletResponse response) throws Exception {
        BufferedReader in = null;

        final URL url = new URL(request.getParameter("ref"));
        final URLConnection connection = url.openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line = null;
        while ((line = in.readLine()) != null) {
            response.getOutputStream().print(line);
        }

        if (in != null) {
            try {
                in.close();
            } catch (final IOException ex) {
            }
        }
        return null;

    }

    private boolean refreshMaster(final ServletRequest request) {
        if (getService().getParent(getUser(request)) != null) {
            final List u = util.match(
                    (List) getService().getParent(getUser(request)).getProperties().get(Constants.ALL_FIELDS),
                    "service", getService().getName());
            if (u.size() > 0 || is_sharing(request)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public ModelAndView remove(final ServletRequest request, final ServletResponse response)
            throws Exception, BusinessException {
        final boolean b = refreshMaster(request);
        getVO(request).setMultipleId(getChaves(request));
        getVO(request).setKey(getPrimaryKey());

        getService().remove(getVO(request), getUser(request));

        response.setContentType("application/javascript; charset=" + getUser(request).getEncode());
        String pk = "";
        if (getVO(request).get(getService().getParent(getUser(request)).getPrimaryKey()) != null) {
            pk = (String) getVO(request)
                    .get(getService().getParent(getUser(request)).getPrimaryKey().get(Constants.name));

        }

        if (getUser(request).getActionMessages(request).isEmpty()) {
            response.getWriter().write("{success: true, " + Constants.TOKEN + ":'"
                    + nextToken((HttpServletRequest) request) + "',  extra:' closeWindow(\"" + getFormName(request)
                    + "\",\"remove\") ; refresh(\"" + getFormName(request) + "\", \""
                    + getUser(request).getLabel(getService().getProperties().get("label").toString())
                    + "\",\"remove\",\"" + (getService().getParent(getUser(request)).getName() + "_" + pk + "\"") + ","
                    + b + ");'}");
        } else {
            refresh(request, response);
        }

        return null;
    }

    public void removeButton(final String Panel, final String label, final ServletRequest request) {

        final List a = (List) ((Map) request.getAttribute(Constants.BUTTONS)).get(Panel);
        if (a == null) {
            return;
        }
        a.removeAll(util.match(a, "KEYLABEL", label));

    }

    private boolean requiredUser() {
        return this.requiredUser;
    }

    public ModelAndView serializeJSOnResponse(final ServletRequest request, final ServletResponse response,
                                              final Object nd, final boolean encapsulate) {

        try {

            boolean scriptTag = false;
            final String cb = (String) getParameter(request, "callback", false);
            if (cb != null) {
                scriptTag = true;
                response.setContentType("text/javascript; charset=" + getUser(request).getEncode());
            } else {
                response.setContentType("application/javascript; charset=" + getUser(request).getEncode());
            }

            if (scriptTag) {
                response.getWriter().write(cb + "(");
            }
            if (encapsulate) {
                response.getWriter().write("{");
                response.getWriter().write("	\"total\": " + getVO(request).getTotal() + "," + "	\"stime\": '"
                        + getVO(request).getSTime() + "',");
                response.getWriter().write("	\"rows\":");
            }

            final String out = util.JSONserialize(nd);
            LogUtil.debug(out, getUser(request));
            response.getWriter().write(out);
            if (encapsulate && scriptTag) {
                String msgs = "";
                if (getUser(request).getActionMessages(request) != null && !getUser(request).getActionMessages(request).isEmpty()) {
                    for (Iterator iterator = getUser(request).getActionMessages(request).iterator(); iterator.hasNext(); ) {
                        Map ms = (Map) iterator.next();
                        msgs += getUser(request).getLabel((String) ms.get("MESSAGE")) + "</br>";
                    }
                }
                response.getWriter().write(",\"msgs\": \"" + msgs.trim() + "\"");
            }

            if (encapsulate) {
                response.getWriter().write("	}");
            }
            if (scriptTag) {
                response.getWriter().write(");");
            }
        } catch (final Exception ex) {
            LogUtil.exception(ex, (Usuario) request.getAttribute(Constants.RAIN_USER));
            // response.setContentType(CONTENT_TYPE);
            PrintWriter out = null;
            try {
                out = response.getWriter();
            } catch (final IOException e) {
                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
            }
            final StringWriter sw = new StringWriter();
            LogUtil.exception(ex, (Usuario) request.getAttribute(Constants.RAIN_USER));
            final String suaExeption = sw.getBuffer().toString();
            try {
                sw.close();
            } catch (final IOException e) {
                LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
            }
            out.print(suaExeption);
            out.close();
            LogUtil.debug("Erro AJAX: " + ex.getMessage().toString(), getUser(request));
        }
        return null;
    }

    public void set_master(final boolean is_master) {
        this.master = is_master;
    }

    private void setCabecalho(final String cab, final ServletRequest request) {
        setReqAttr(request, "cab_titulo", util.decode(cab, getUser(request)));
    }

    public void setDinamic(final boolean is_dinamic) {
        this.dynamic_redirect = is_dinamic;
    }

    public void setHelpId(final String cab, final ServletRequest request) {
        if (cab != null) {
            setReqAttr(request, "help_id", cab);
        }
    }

    public void setMainField(final String f) {
        this.mf = f;
    }

    public void setMethod(final String method, final ServletRequest req) {
        getVO(req).setMethod(method);
        req.setAttribute(Constants.ActionParam, method);
    }

    public void setProcessed(final ServletRequest request) {
        setReqAttr(request, Constants.RequestProcessed, Boolean.TRUE);
    }

    public void setService(final GenericService s) {
        service = s;
    }

    private void setSubCabecalho(final String cab, final ServletRequest request) {
        setReqAttr(request, "cab_subtitulo", cab);
    }

    public void setTitulo(final String cab, final ServletRequest request) {

        setReqAttr(request, Constants.ACTION_TITLE, cab);
    }

    protected void setUser(final Usuario user, final ServletRequest request) {
        WebUtils.setSessionAttribute((HttpServletRequest) request, Constants.RAIN_USER, user);
    }

    public ModelAndView show(final ServletRequest request, final ServletResponse response) throws Exception {
        clearFields(request);
        final GenericService s = (GenericService) getService()
                .getServiceById(getVO(request).get("NODE").toString().split("_")[0], getUser(request));

        final GenericService starget = (GenericService) getService()
                .getServiceById(getVO(request).get("MODEL").toString(), getUser(request));

        if (s.getProperties().get("sharing") != null) {
            setReqAttr(request, "is_win", Boolean.TRUE);
            final Map p = new HashMap(s.getProperties());
            final Map ps = new HashMap(
                    util.JSONtoMap(util.EvaluateInString((String) p.get("sharing"), createEvaluationContext(request))));
            if (ps.get("action") == null) {
                p.put(Constants.ActionParam, "lista");
            } else {
                p.put(Constants.ActionParam, ps.get("action"));
            }

            if (p.get("action") == null) {
                p.put("action", "lista");
            }

            final String form_name = Constants.FILTER_KEY
                    + util.replace(request.getAttribute(Constants.VOName).toString(),
                    getService().getProperties().get(Constants.name).toString(),
                    starget.getProperties().get(Constants.name).toString())
                    + "_*_" + p.get("action") + "_tree";

            final Object[] ac = util.getActionStringAndParams(p, false, false, request);
            clearButtons(request);

            request.getRequestDispatcher(util.encode(ac[0].toString() + "&form_name=" + form_name,
                    getUser(request).getSessionId(), "", false, getUser(request))).forward(request, response);
            return null;
        } else {
            for (final Iterator iterator = s.getExplorerPropertyes(getUser(request)).iterator(); iterator.hasNext(); ) {
                final Map field = (Map) iterator.next();
                addCampo(request, field);
            }
        }
        return finish(request, response, null);
    }

    public final ModelAndView stream(final ServletRequest request, final ServletResponse response, final Map method)
            throws Exception {
        final ServiceImpl service = (ServiceImpl) getService();

        final String fm = getUser(request).getLabel((String) method.get("label"));
        String ext = "xml";
        try {
            final Map c = util
                    .JSONtoMap(getUser(request).getUtil().replace((String) method.get("contextParams"), "!", ""));
            ext = (String) c.get("EXTENSION");
        } catch (final Exception e) {
            LogUtil.exception(e, (Usuario) request.getAttribute(Constants.RAIN_USER));
        }

        getVO(request).put("RESPONSE", response);
        getVO(request).put("REQUEST", request);
        if (ext != null) {
            getVO(request).put("WRITER", response.getWriter());
            configureDownload((HttpServletResponse) response, "text/xml", fm + "." + ext);
        }

        service.invoke(method.get(Constants.name).toString(), getVO(request), getUser(request));
        return null;
    }

    public String urlKeys(final ServletRequest request) {
        String a = "";
        for (int i = 0; i < getRequestKeys(request).size(); i++) {
            final String var = (String) getRequestKeys(request).get(i);
            final String value = (String) getParameter(request, var, false);
            a += var + "=" + value + "&";
        }
        return a;
    }

    public ModelAndView XmlConfig(final ServletRequest request, final ServletResponse response) throws Exception {
        response.setContentType("text/xml");
        final InputStream fileInputStream = getUser(request).getSysParser().getStreamConfig();
        int i;
        while ((i = fileInputStream.read()) != -1) {
            response.getOutputStream().write(i);
        }
        fileInputStream.close();
        response.getOutputStream().flush();
        return null;
    }

}
