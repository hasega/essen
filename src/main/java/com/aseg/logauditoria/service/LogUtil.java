package com.aseg.logauditoria.service;

/* Para visualizar debug, ir em instncias:
 * 
 * aba: m_config_sistema_logasp
 * 
 * Criar itens:
 * 
 * LOG_DEBUG_ENABLE : true
 * LOG_INFO_ENABLE : true
 * LOG_WARN_ENABLE : true
 * 
 * */

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aseg.seguranca.Usuario;
import com.aseg.service.GenericService;
import com.aseg.vo.GenericVO;

public class LogUtil implements Serializable {

	  private static final Logger log = LoggerFactory.getLogger(LogUtil.class);


	public Map ArraysToList(final Map m, Usuario user) {
		final Map retorno = new GenericVO();
		if (m != null) {
			final Iterator i = m.keySet().iterator();
			while (i.hasNext()) {
				final String elem = (String) i.next();
				if (m.get(elem) instanceof Object[]) {
					retorno.put(elem, Arrays.asList((Object[]) m.get(elem)));
				} else {
					retorno.put(elem, m.get(elem));
				}

			}

		} else {
			return null;
		}
		return retorno;

	}

	public static void debug(final Object object, Usuario user) {

		if (debugEnabled(user)) {
			final String cl = Thread.currentThread().getStackTrace()[2].getClassName();
			if (getDebugLevels(user) != null && Arrays.asList(getDebugLevels(user))
					.contains(cl.substring(cl.lastIndexOf('.') + 1).replaceAll("\\$.+", ""))) {
				if (object == null) {
					log.debug(getPrefixLog(user) + " DEBUG >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> "
							+ null);
				} else {
					log.debug(getPrefixLog(user) + "DEBUG >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> "
							+ object.toString());
				}
			} else if (getDebugLevels(user) == null) {
				if (object == null) {
					log.debug(getPrefixLog(user) + "DEBUG >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> "
							+ null);
				} else {
					log.debug(getPrefixLog(user) + "DEBUG >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> "
							+ object.toString());
				}
			}
		}
	}

	private static Object getDebugLevels(Usuario user) {
		try {
			return user.getSystemProperty("LOG_DEBUG_CLASSES").split(",");
		} catch (Exception e) {
			return null;

		}
	}

	public static boolean debugEnabled(Usuario user) {
		if (user == null)
			return true;
		if (user.isSuperUser())
			return true;
		if (user == null )
			return true;
		return user.getUtil().nullOrTrue(user.getSystemProperty("LOG_DEBUG_ENABLED"));

	}

	public static void error(final String string) {

		final String cl = Thread.currentThread().getStackTrace()[2].getClassName();
		if (string == null) {
			log.error(getPrefixLog(null) + "ERROR >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> " + null);
		} else {
			log.error(getPrefixLog(null) + "ERROR >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> "
					+ string.toString());
		}

	}

	public static void fatal(final String string) {

		final String cl = Thread.currentThread().getStackTrace()[2].getClassName();
		if (string == null) {
			log.error(getPrefixLog(null) + "FATAL >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> " + null);
		} else {
			log.error(getPrefixLog(null) + "FATAL >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> "
					+ string.toString());
		}

	}

	public static void exception(Throwable ex, Usuario user) {
		if (user != null) {
			if (exceptionEnabled(user)) {
				log.error(ex.toString());
				ex.printStackTrace();
			}
		}
	}

	public static void fatal(final String string, final Exception e) {
		log.error(e.getMessage());
		fatal(string);
	}

	public static void info(final Object object, Usuario user) {
		if (infoEnabled(user)) {
			final String cl = Thread.currentThread().getStackTrace()[2].getClassName();
			if (getInfoLevels(user) != null && Arrays.asList(getInfoLevels(user))
					.contains(cl.substring(cl.lastIndexOf('.') + 1).replaceAll("\\$.+", ""))) {
				if (object == null) {
					log.info(
							getPrefixLog(user) + "INFO >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> " + null);
				} else {
					log.info(getPrefixLog(user) + "INFO >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> "
							+ object.toString());
				}
			} else if (getInfoLevels(user) == null) {
				if (object == null) {
					log.info(
							getPrefixLog(user) + "INFO >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> " + null);
				} else {
					log.info(getPrefixLog(user) + "INFO >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> "
							+ object.toString());
				}
			}
		}

	}

	public void log(final Usuario usuario, final Map chave, final String clazz, String informacaoAdicional,
			Map informacaoAntiga, Map informacaoNova, final String idEvento, final String idModuloSistema,
			final String idTransacao) {
		if (usuario.getIdUsuario() == null) {
			return;
		}
		informacaoAntiga = ArraysToList(informacaoAntiga, usuario);
		informacaoNova = ArraysToList(informacaoNova, usuario);

		// Padroniza a entrada de dados na informao nova e antiga.

		String key = "";
		final GenericVO itAntiga = informacaoAntiga != null
				? new GenericVO().putAllAndGet(informacaoAntiga, false)
				: new GenericVO();
		// Para os casos de "ALTERACAO" os campos "nova" e "antiga" recebe
		// apenas a os campos que estiverem diferentes.
		final Iterator iterator = itAntiga.keySet().iterator();
		while (iterator.hasNext()) {
			key = (String) iterator.next();
			if (informacaoAntiga.get(key) != null) {
				if (informacaoAntiga.get(key).equals(informacaoNova.get(key))) {
					informacaoNova.remove(key);
					informacaoAntiga.remove(key);
				}
			} else if (informacaoNova.get(key) != null) {
				if (informacaoNova.get(key).equals(informacaoAntiga.get(key))) {
					informacaoNova.remove(key);
					informacaoAntiga.remove(key);
				}
			} else {
				informacaoNova.remove(key);
				informacaoAntiga.remove(key);
			}
		}

		if (informacaoAdicional != null) {
			informacaoAdicional = informacaoAdicional.toUpperCase();
		}

		// Trunca a mensagem caso ela seja maior que o tamanho do campo
		// (4000).
		String antiga = informacaoAntiga != null ? informacaoAntiga.toString() : "";
		if (antiga.length() > 3999) {
			antiga = antiga.substring(0, 3999);
		}

		// Trunca a mensagem caso ela seja maior que o tamanho do campo (4000).
		String nova = informacaoNova != null ? informacaoNova.toString() : "";
		if (nova.length() > 3999) {
			nova = nova.substring(0, 3999);
		}
		// Nos casos de alterao onde nenhum registro foi alterado no deve
		// registrar o log.
		if (informacaoAntiga != null && informacaoAntiga.size() != 0
				|| informacaoNova != null && informacaoNova.size() != 0) {
			if (usuario != null) {
				logDataBase(chave.toString(), clazz, informacaoAdicional, antiga, nova, idEvento, idModuloSistema,
						idTransacao, usuario);
			}
		}
	}

	public void log(final Usuario usuario, final String clazz, Throwable exp, final String idModuloSistema,
			final String idTransacao) {
		try {
			String expMsg = "";
			if (exp instanceof InvocationTargetException) {
				if (((InvocationTargetException) exp).getTargetException() != null) {
					exp = ((InvocationTargetException) exp).getTargetException();
				} else if (((InvocationTargetException) exp).getCause() != null) {
					exp = ((InvocationTargetException) exp).getCause();
				}
				expMsg = exp.getMessage();
			} else if (exp instanceof RuntimeException) {
				if (((RuntimeException) exp).getCause() != null) {
					exp = ((RuntimeException) exp).getCause();
				}

				expMsg = exp.toString();
			} else if (exp instanceof NullPointerException) {
				exp = ((RuntimeException) exp).getCause();
				expMsg = exp.toString();
			} else {
				expMsg = exp.getMessage();
			}

			// Trunca a mensagem de erro caso ela seja maior que o tamanho do
			// campo (4000).
			if (expMsg != null) {
				if (expMsg.length() > 3999) {
					expMsg = expMsg.substring(0, 3999);
				}
			} else {
				expMsg = "";
			}

			log(usuario, new GenericVO(), clazz, expMsg, new GenericVO(),
					new GenericVO(), "ERROEXECUCAO", idModuloSistema, idTransacao);
		} catch (final Exception e) {
			throw new RuntimeException("[LogUtil] Problema no mtodo de log ", e);
		} finally {

		}
	}

	protected void logConsole(final String sequenciaSessao, final String ip, final String chave, final String clazz,
			final String informacaoAdicional, final String informacaoAntiga, final String informacaoNova,
			final String idEvento, final String idModuloSistema, final Long idUsuario, Usuario user) {
		final StringBuffer out = new StringBuffer("");
		try {
			out.append("------------Inicio Log---------------------------- /n");
			out.append("Data: "
					+ user.getUtil().df.stringToTimestamp(new Timestamp(Calendar.getInstance().getTime().getTime()))
					+ "/n");
			out.append("Sequencial Sessao: " + sequenciaSessao + "/n");
			out.append("IP: " + ip + "/n");
			out.append("Chave: " + chave + "/n");
			out.append("Classe: " + clazz + "/n");
			out.append("Informacao Adicional: " + informacaoAdicional + "/n");
			out.append("Informacao Antiga: " + informacaoAntiga + "/n");
			out.append("Informacao Nova: " + informacaoNova + "/n");
			out.append("Evento: " + idEvento + "/n");
			out.append("Modulo: " + idModuloSistema + "/n");
			out.append("Usuario: " + idUsuario + "/n");
			out.append("------------Fim Log------------------------------ /n");
			debug(out.toString(), user);
		} catch (final Exception e) {
			LogUtil.exception(e, user);
			throw new RuntimeException("[LogUtil] Problema no mtodo de logConsole", e);
		} finally {

		}
	}

	protected void logDataBase(final String chave, final String clazz, final String informacaoAdicional,
			final String informacaoAntiga, final String informacaoNova, final String idEvento,
			final String idModuloSistema, String idTransacao, final Usuario user) {

		try {

			GenericService l = (GenericService) user.getFactory().getSpringBean("LogAuditoriaService", user);

			GenericVO vo = l.createVO(user);

			vo.put("SEQUENCIAL_SESSAO", user.getSessionId());
			vo.put("IP", user.getIp_origem());
			vo.put("CHAVE", chave);
			vo.put("CLASSE", clazz);
			vo.put("INFORMACAO_ADICIONAL", force4000(informacaoAdicional));
			vo.put("INFORMACAO_ANTIGA", force4000(informacaoAntiga));
			vo.put("INFORMACAO_NOVA", force4000(informacaoNova));
			vo.put("ID_EVENTO", idEvento);
			vo.put("ID_MODULO_SISTEMA", idModuloSistema);
			vo.put("ID_TRANSACAO", idTransacao);

			l.inclui(vo, user);

		} catch (final Exception e) {
			LogUtil.exception(e, user);
			throw new RuntimeException("[LogUtil] Problema no metodo de logDataBase", e);
		}
	}

	private Object force4000(String informacaoAdicional) {
		// TODO Auto-generated method stub
		String s = informacaoAdicional;
		if (s != null && s.toString().length() > 4000)
			s = s.substring(0, 4000);
		return s;
	}

	public static void warn(final Object object, Usuario user) {
		if (warnEnabled(user)) {
			final String cl = Thread.currentThread().getStackTrace()[2].getClassName();
			if (getWarnLevels(user) != null && Arrays.asList(getWarnLevels(user))
					.contains(cl.substring(cl.lastIndexOf('.') + 1).replaceAll("\\$.+", ""))) {
				if (object == null) {
					log.warn(
							getPrefixLog(user) + "WARN >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> " + null);
				} else {
					log.warn(getPrefixLog(user) + "WARN >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> "
							+ object.toString());
				}
			} else if (getWarnLevels(user) == null) {
				if (object == null) {
					log.warn(
							getPrefixLog(user) + "WARN >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> " + null);
				} else {
					log.warn(getPrefixLog(user) + "WARN >>>> " + cl.substring(cl.lastIndexOf('.') + 1) + " >>> "
							+ object.toString());
				}
			}
		}

	}

	private static Object getWarnLevels(Usuario user) {
		try {
			return user.getSystemProperty("LOG_WARN_CLASSES").split(",");
		} catch (Exception e) {
			return null;

		}
	}

	public static boolean warnEnabled(Usuario user) {

		return user.getUtil().nullOrTrue(user.getSystemProperty("LOG_WARN_ENABLED"));

	}

	public static boolean exceptionEnabled(Usuario user) {
		if (user.getSystemProperty("LOG_EXCEPTION") == null || user.isSuperUser())
			return true;

		return user.getUtil().nullOrTrue(user.getSystemProperty("LOG_EXCEPTION"));
	}

	private static Object getInfoLevels(Usuario user) {
		try {
			return user.getSystemProperty("LOG_DEBUG_CLASSES").split(",");
		} catch (Exception e) {
			return null;

		}
	}

	public static boolean infoEnabled(Usuario user) {
		return user.getUtil().nullOrTrue(user.getSystemProperty("LOG_INFO_ENABLED"));

	}

	public static void notify(String string) {
		System.out.println(string);
	}

	private static String getPrefixLog(Usuario user) {
		String id = " {null} ";
		if (user != null) {
			id = " { system:'" + user.getSystem() + "', login:'" + user.getLogin() + "' } ";
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
		String data = dateFormat.format(new Date());
		return data + id;

	}

}