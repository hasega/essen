//package com.aseg.webservices;
//
//import java.io.ByteArrayInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.io.OutputStreamWriter;
//import java.io.StringWriter;
//import java.io.Writer;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.net.URLConnection;
//import java.security.cert.X509Certificate;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
//import javax.net.ssl.HostnameVerifier;
//import javax.net.ssl.HttpsURLConnection;
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.SSLSession;
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.X509TrustManager;
//
//import com.aseg.exceptions.BusinessException;
//import com.aseg.logauditoria.service.LogUtil;
//import com.aseg.seguranca.Usuario;
//import com.aseg.service.ServiceImpl;
//import com.aseg.util.service.ParseXmlService;
//import com.aseg.vo.GenericVO;
//import com.predic8.wsdl.Definitions;
//import com.predic8.wsdl.Operation;
//import com.predic8.wsdl.Port;
//import com.predic8.wsdl.PortType;
//import com.predic8.wsdl.Service;
//import com.predic8.wsdl.WSDLParser;
//import com.predic8.wstool.creator.RequestCreator;
//import com.predic8.wstool.creator.SOARequestCreator;
//
//import groovy.xml.MarkupBuilder;
//
//public class WSUtil extends ServiceImpl {
//
//	static WSUtil instance = null;
//
//	public static WSUtil getInstance(final Usuario user) {
//		if (instance == null) {
//			instance = new WSUtil();
//		}
//		return instance;
//
//	}
//
//	private WSUtil() {
//
//	}
//
//	public boolean pageSetup(final GenericVO mVO, final Usuario user) throws Exception {
//
//		mVO.preservOld();
//
//		return true;
//	}
//
//	public boolean callWSDL(final GenericVO VO, final Usuario user) throws Exception {
//		WSDLParser parser = new WSDLParser();
//		if (VO.get("WS_OPERATION") == null)
//			return false;
//
//		Definitions wsdl = null;
//		if (VO.get("DESCRITOR").toString().startsWith("https")) {
//			System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
//			// Security.addProvider(new
//			// com.sun.net.ssl.internal.ssl.Provider());
//
//			// Create a trust manager that does not validate certificate chains
//			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
//				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//					return null;
//				}
//
//				public void checkClientTrusted(X509Certificate[] certs, String authType) {
//				}
//
//				public void checkServerTrusted(X509Certificate[] certs, String authType) {
//				}
//
//				public boolean isClientTrusted(X509Certificate[] arg0) {
//					// TODO Auto-generated method stub
//					return true;
//				}
//
//				public boolean isServerTrusted(X509Certificate[] arg0) {
//					// TODO Auto-generated method stub
//					return true;
//				}
//			} };
//
//			// Install the all-trusting trust manager
//			SSLContext sc = SSLContext.getInstance("SSL");
//			sc.init(null, trustAllCerts, new java.security.SecureRandom());
//			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//
//			// Create all-trusting host name verifier
//			HostnameVerifier allHostsValid = new HostnameVerifier() {
//				public boolean verify(String hostname, SSLSession session) {
//					return true;
//				}
//
//				public boolean verify(String arg0, String arg1) {
//					// TODO Auto-generated method stub
//					return true;
//				}
//			};
//
//			// Install the all-trusting host verifier
//			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
//
//			URL url = new URL(VO.get("DESCRITOR").toString());
//			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
//			con.setRequestProperty("Accept-Encoding", "");
//			// SSLException thrown here if server certificate is invalid
//			// ;
//
//			wsdl = parser.parse(
//					user.getUtil().writeToTmp(con.getInputStream(), VO.get("NOME").toString()).getAbsolutePath());
//		} else {
//			wsdl = parser.parse(VO.get("DESCRITOR").toString());
//		}
//		StringWriter writer = null;
//
//		Map a = user.getUtil().JSONtoMap((String) VO.get("PARAMS"));
//		String operation = VO.get("WS_OPERATION").toString();
//		if (a.size() == 0 || a.get(operation) == null)
//			return true;
//
//		Map op = (Map) a.get(operation);
//		HashMap<String, Object> formParams = new HashMap<String, Object>();
//		Iterator i = VO.keySet().iterator();
//		String opr = operation + "/";
//
//		while (i.hasNext()) {
//			String k = (String) i.next();
//
//			if (VO.isIgnoreCase() && VO.getICase(k) != null)
//				formParams.put("xpath:/" + opr + k.toLowerCase(), VO.getICase(k));
//			else if (VO.getICase(k) != null)
//				formParams.put("xpath:/" + opr + k, VO.getICase(k));
//
//		}
//
//		// formParams.put("xpath:/filtro", "teste");
//		String pt = "";
//		String binding = "";
//		String adres = "";
//
//		Iterator ix = op.keySet().iterator();
//		SOARequestCreator creator = new SOARequestCreator(wsdl, true, null);
//
//		creator.setDefinitions(wsdl);
//		creator.setFormParams(formParams);
//		creator.setCreator(new RequestCreator());
//
//		while (ix.hasNext()) {
//			try {
//				writer = new StringWriter();
//				creator.setBuilder(new MarkupBuilder(writer));
//				pt = (String) ix.next();
//				binding = (String) ((Map) op.get(pt)).keySet().toArray()[0];
//				adres = (String) ((Map) op.get(pt)).get(binding).toString();
//				creator.createRequest(pt, operation, binding);
//				break;
//			} catch (Exception e) {
//				continue;
//			}
//
//		}
//
//		if (writer.toString().length() == 0)
//			throw new BusinessException("Nao  possivel criar um request para seu servico");
//
//		URLConnection connection = null;
//		// System.out.println(writer);
//		try {
//			if (adres.toString().startsWith("https")) {
//				System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
//				// Security.addProvider(new
//				// javax.net.ssl.internal.ssl.Provider());
//
//				// Create a trust manager that does not validate certificate
//				// chains
//				TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
//					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//						return null;
//					}
//
//					public void checkClientTrusted(X509Certificate[] certs, String authType) {
//					}
//
//					public void checkServerTrusted(X509Certificate[] certs, String authType) {
//					}
//
//				} };
//
//				// Install the all-trusting trust manager
//				SSLContext sc = SSLContext.getInstance("SSL");
//				sc.init(null, trustAllCerts, new java.security.SecureRandom());
//				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//
//				// Create all-trusting host name verifier
//				HostnameVerifier allHostsValid = new HostnameVerifier() {
//					public boolean verify(String hostname, SSLSession session) {
//						return true;
//					}
//
//					public boolean verify(String arg0, String arg1) {
//						// TODO Auto-generated method stub
//						return true;
//					}
//				};
//
//				// Install the all-trusting host verifier
//				HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
//				URL u = new URL(adres);
//				URLConnection uc = u.openConnection();
//				connection = (HttpsURLConnection) uc;
//				((HttpsURLConnection) connection).setRequestMethod("POST");
//			} else {
//				URL u = new URL(adres);
//				URLConnection uc = u.openConnection();
//				connection = (HttpURLConnection) uc;
//				((HttpURLConnection) connection).setRequestMethod("POST");
//			}
//			connection.setRequestProperty("Content-Length", String.valueOf(writer.toString().getBytes().length));
//			connection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
//
//			connection.setDoOutput(true);
//			connection.setDoInput(true);
//			// connection.setUseCaches(false);
//
//			String tn = wsdl.getTargetNamespace();
//			// if (!tn.endsWith("/"))
//			// tn += "/";
//
//			connection.setRequestProperty("SOAPAction", tn + "/" + operation);
//
//			OutputStream out = connection.getOutputStream();
//			Writer wout = new OutputStreamWriter(out);
//
//			out.write(writer.toString().getBytes());
//
//			out.flush();
//			out.close();
//
//			ParseXmlService d = new ParseXmlService();
//
//			InputStream dx = null;
//			boolean fault = false;
//
//			try {
//				dx = connection.getInputStream();
//			} catch (Exception e) {
//				LogUtil.exception(e, user);
//				if (adres.toString().startsWith("https"))
//					dx = ((HttpsURLConnection) connection).getErrorStream();
//				else
//					dx = ((HttpURLConnection) connection).getErrorStream();
//				fault = true;
//			}
//
//			String in = user.getUtil().convertStreamToString(dx, user);
//			VO.put("WS_RESULT_STRING", in);
//			VO.put("IS_FAULT", fault);
//
//			LogUtil.debug(in, user);
//
//			if (fault)
//				return false;
//
//			d.setXMLStream(new ByteArrayInputStream(in.getBytes()));
//			String q = "/Envelope/Body/" + operation + "Response/" + operation + "Result";
//			if (VO.get("WS_QUERY_RESULT") != null)
//				q = (String) VO.get("WS_QUERY_RESULT");
//
//			String n = d.getStrinNodeFromXQL(q, user);
//			// d.setXMLStream();
//			if (n.startsWith("<"))
//				VO.put("WS_PARSER", user.getUtil().createXMLParser(new ByteArrayInputStream(n.getBytes())));
//
//			VO.put("WS_RESPONSE_STRING", n);
//
//			LogUtil.debug(in, user);
//
//		} catch (IOException e) {
//			LogUtil.exception(e, user);
//		}
//
//		return false;
//	}
//
//	public boolean parseWSDL(final GenericVO VO, final Usuario user) throws Exception {
//		WSDLParser parser = new WSDLParser();
//		HashMap<String, HashMap<String, HashMap<String, String>>> operations = new HashMap<String, HashMap<String, HashMap<String, String>>>();
//		HashMap<String, HashMap<String, String>> ptt = new HashMap<String, HashMap<String, String>>();
//
//		// https parser
//
//		Definitions wsdl = null;
//		if (VO.get("DESCRITOR").toString().startsWith("https")) {
//			System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
//			// Security.addProvider(new
//			// com.sun.net.ssl.internal.ssl.Provider());
//
//			// Create a trust manager that does not validate certificate chains
//			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
//				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//					return null;
//				}
//
//				public void checkClientTrusted(X509Certificate[] certs, String authType) {
//				}
//
//				public void checkServerTrusted(X509Certificate[] certs, String authType) {
//				}
//
//			} };
//
//			// Install the all-trusting trust manager
//			SSLContext sc = SSLContext.getInstance("SSL");
//			sc.init(null, trustAllCerts, new java.security.SecureRandom());
//			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//
//			// Create all-trusting host name verifier
//			HostnameVerifier allHostsValid = new HostnameVerifier() {
//				public boolean verify(String hostname, SSLSession session) {
//					return true;
//				}
//
//				public boolean verify(String arg0, String arg1) {
//					// TODO Auto-generated method stub
//					return true;
//				}
//			};
//
//			// Install the all-trusting host verifier
//			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
//
//			URL url = new URL(VO.get("DESCRITOR").toString());
//			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
//			// SSLException thrown here if server certificate is invalid
//			// ;
//
//			wsdl = parser.parse(
//					user.getUtil().writeToTmp(con.getInputStream(), VO.get("NOME").toString()).getAbsolutePath());
//		} else
//			wsdl = parser.parse(VO.get("DESCRITOR").toString());
//
//		for (PortType pt : wsdl.getPortTypes()) {
//			ptt.put(pt.getName(), new HashMap<String, String>());
//			// System.out.println(pt.getName() + pt.get);
//			for (Operation op : pt.getOperations()) {
//				// op.get
//				LogUtil.debug(op.getName(), user);
//				operations.put(op.getName(), ptt);
//			}
//		}
//
//		for (Service service : wsdl.getServices()) {
//			LogUtil.debug(service.getName(), user);
//			for (Port port : service.getPorts()) {
//				// Aport = port.getAddress().getLocation();
//				if (ptt.get(port.getName()) == null)
//					ptt.put(port.getName(), new HashMap<String, String>());
//				ptt.get(port.getName()).put(port.getBinding().getName(), port.getAddress().getLocation());
//				LogUtil.debug(port.getName() + "( " + port.getBinding().getName() + ", "
//						+ port.getAddress().getLocation() + " )", user);
//			}
//		}
//
//		VO.put("PARAMS", user.getUtil().JSONserialize(operations));
//
//		return true;
//	}
//
//}